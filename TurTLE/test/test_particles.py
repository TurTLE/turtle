#! /usr/bin/env python

import numpy as np
import h5py
import sys

import TurTLE
from TurTLE import DNS

try:
    import matplotlib.pyplot as plt
    matplotlib_on = True
except ImportError:
    matplotlib_on = False


def main():
    assert(sys.argv[1] in ['p2p_sampling'])
    assert(sys.argv[2] in ['on', 'off'])
    niterations = 32
    nparticles = 1000
    njobs = 1
    if sys.argv[2] == 'on':
        c = DNS()
        c.launch(
                ['NSVEcomplex_particles',
                 '-n', '32',
                 '--src-simname', 'B32p1e4',
                 '--src-wd', TurTLE.data_dir,
                 '--src-iteration', '0',
                 '--np', '4',
                 '--ntpp', '1',
                 '--niter_todo', '{0}'.format(niterations),
                 '--niter_out', '{0}'.format(niterations),
                 '--niter_stat', '1',
                 '--checkpoints_per_file', '{0}'.format(3),
                 '--nparticles', '{0}'.format(nparticles),
                 '--particle-rand-seed', '2',
                 '--cpp_random_particles', '0',
                 '--njobs', '{0}'.format(njobs),
                 '--wd', './'] +
                 sys.argv[3:])
    if sys.argv[1] == 'p2p_sampling':
        cf = h5py.File(
                'test_checkpoint_0.h5',
                'r')
        pf = h5py.File(
                'test_particles.h5',
                'r')
        if matplotlib_on:
            # initial condition:
            # show a histogram of the orientations
            f = plt.figure()
            a = f.add_subplot(111)
            for iteration in range(1):
                x = cf['tracers0/state/{0}'.format(iteration)][:, 3:]
                hist, bins = np.histogram(
                        np.sum(x**2, axis = -1).flatten()**.5,
                        bins = np.linspace(0, 2, 40))
                bb = (bins[:-1] + bins[1:])/2
                pp = hist.astype(float) / (np.sum(hist) * (bb[1] - bb[0]))
                a.plot(bb, pp, label = '{0}'.format(iteration))
            a.legend(loc = 'best')
            f.tight_layout()
            f.savefig('orientation_histogram.pdf')
            plt.close(f)
            # show a histogram of the positions
            f = plt.figure()
            a = f.add_subplot(111)
            for iteration in range(0, niterations*njobs+1, niterations//2):
                x = pf['tracers0/position/{0}'.format(iteration)][...]
                hist, bins = np.histogram(
                        np.sum(x**2, axis = -1).flatten()**.5,
                        bins = 40)
                bb = (bins[:-1] + bins[1:])/2
                pp = hist.astype(float) / (np.sum(hist) * (bb[1] - bb[0]))
                a.plot(bb, pp, label = '{0}'.format(iteration))
            a.legend(loc = 'best')
            f.tight_layout()
            f.savefig('position_histogram.pdf')
            plt.close(f)
            # show a histogram of the orientations
            f = plt.figure()
            a = f.add_subplot(111)
            for iteration in range(0, niterations*njobs+1, niterations//2):
                x = pf['tracers0/orientation/{0}'.format(iteration)][...]
                hist, bins = np.histogram(
                        np.sum(x**2, axis = -1).flatten()**.5,
                        bins = np.linspace(0, 2, 40))
                bb = (bins[:-1] + bins[1:])/2
                pp = hist.astype(float) / (np.sum(hist) * (bb[1] - bb[0]))
                a.plot(bb, pp, label = '{0}'.format(iteration))
            a.legend(loc = 'best')
            f.tight_layout()
            f.savefig('orientation_histogram.pdf')
            plt.close(f)
            # compared sampled positions with checkpoint positions
            for iteration in range(0, niterations*njobs+1, niterations):
                x = pf['tracers0/position/{0}'.format(iteration)][...]
                s = cf['tracers0/state/{0}'.format(iteration)][...]
                distance = (np.max(np.abs(x - s[..., :3]) /
                                   np.maximum(np.ones(x.shape),
                                              np.maximum(np.abs(x),
                                                         np.abs(s[..., :3])))))
                assert(distance < 1e-14)
                x = pf['tracers0/orientation/{0}'.format(iteration)][...]
                distance = (np.max(np.abs(x - s[..., 3:]) /
                                   np.maximum(np.ones(x.shape),
                                              np.maximum(np.abs(x),
                                                         np.abs(s[..., 3:])))))
                assert(distance < 1e-14)
            # code relevant when velocity field is 0 everywhere.
            # we check to see what happens to the orientation of the particles
            # show a histogram of the orientations
            f = plt.figure()
            a = f.add_subplot(111)
            for iteration in range(0, niterations*njobs+1, niterations//4):
                x = pf['tracers0/orientation/{0}'.format(iteration)][...]
                hist, bins = np.histogram(
                        x.flatten(),
                        bins = 100)
                bb = (bins[:-1] + bins[1:])/2
                pp = hist.astype(float) / (np.sum(hist) * (bb[1] - bb[0]))
                a.plot(bb, pp, label = '{0}'.format(iteration))
            a.legend(loc = 'best')
            f.tight_layout()
            f.savefig('full_orientation_histogram.pdf')
            plt.close(f)
    return None

if __name__ == '__main__':
    main()

