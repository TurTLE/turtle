#! /usr/bin/env python

import numpy as np
import sys

from TurTLE import DNS

def main():
    niterations = 10
    nlist = [16, 32, 48, 24, 64, 12]
    for ii in range(len(nlist)):
        c = DNS()
        c.launch(
                ['NSVE',
                 '--nx', str(nlist[ii]),
                 '--ny', str(nlist[(ii+1)%(len(nlist))]),
                 '--nz', str(nlist[(ii+2)%(len(nlist))]),
                 '--Lx', str(2+np.random.random()),
                 '--Ly', str(2+np.random.random()),
                 '--Lz', str(2+np.random.random()),
                 '--simname', 'test_Parseval_{0}'.format(ii),
                 '--np', '4',
                 '--ntpp', '1',
                 '--niter_todo', '{0}'.format(niterations),
                 '--niter_out', '{0}'.format(niterations),
                 '--niter_stat', '1',
                 '--field_random_seed', '1',
                 '--wd', './'] +
                 sys.argv[1:])
        c.compute_statistics()
        Parseval_error = np.abs((c.statistics['energy(t)'] - c.statistics['renergy(t)']) / c.statistics['renergy(t)'])
        assert(np.max(Parseval_error) < 1e-6)
    print('SUCCESS!!! Parseval test passed for unequal nx, ny, nz and random Lx, Ly, Lz')
    return None

if __name__ == '__main__':
    main()

