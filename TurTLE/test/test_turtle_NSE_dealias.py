#! /usr/bin/env python
#######################################################################
#                                                                     #
#  Copyright 2021 Max Planck Institute                                #
#                 for Dynamics and Self-Organization                  #
#                                                                     #
#  This file is part of TurTLE.                                       #
#                                                                     #
#  TurTLE is free software: you can redistribute it and/or modify     #
#  it under the terms of the GNU General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License,  #
#  or (at your option) any later version.                             #
#                                                                     #
#  TurTLE is distributed in the hope that it will be useful,          #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of     #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      #
#  GNU General Public License for more details.                       #
#                                                                     #
#  You should have received a copy of the GNU General Public License  #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     #
#                                                                     #
# Contact: Cristian.Lalescu@mpcdf.mpg.de                              #
#                                                                     #
#######################################################################



import os
import numpy as np
import h5py
import sys

import TurTLE
from TurTLE import DNS, PP

from TurTLE_addons import NSReader

try:
    import matplotlib.pyplot as plt
except:
    plt = None


def main_basic(
        wd = './'
        ):
    niterations = 256
    njobs = 2
    c0 = DNS()
    c0.launch(
            ['NSVE',
             '-n', '32',
             '--src-simname', 'B32p1e4',
             '--src-wd', TurTLE.data_dir,
             '--src-iteration', '0',
             '--simname', 'nsve_for_comparison',
             '--np', '4',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--forcing_type', 'linear',
             '--niter_stat', '1',
             '--checkpoints_per_file', '{0}'.format(3),
             '--njobs', '{0}'.format(njobs),
             '--wd', wd] +
             sys.argv[1:])
    cc = PP()
    cc.launch(
            ['get_velocity',
             '--simname', 'nsve_for_comparison',
             '--np', '4',
             '--ntpp', '1',
             '--iter0', '0',
             '--iter1', '{0}'.format(niterations),
             '--wd', wd] +
             sys.argv[1:])
    c1 = DNS()
    c1.launch(
            ['NSE',
             '-n', '32',
             '--src-simname', 'nsve_for_comparison',
             '--src-wd', wd,
             '--src-iteration', '{0}'.format(niterations),
             '--simname', 'nse1',
             '--np', '4',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--niter_stat', '1',
             '--forcing_type', 'linear',
             '--checkpoints_per_file', '{0}'.format(3),
             '--njobs', '{0}'.format(njobs),
             '--wd', wd] +
             sys.argv[1:])
    c2 = DNS()
    c2.launch(
            ['NSE_alt_dealias',
             '-n', '32',
             '--src-simname', 'nsve_for_comparison',
             '--src-wd', wd,
             '--src-iteration', '{0}'.format(niterations),
             '--simname', 'nse2',
             '--np', '4',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--niter_stat', '1',
             '--forcing_type', 'linear',
             '--checkpoints_per_file', '{0}'.format(3),
             '--njobs', '{0}'.format(njobs),
             '--wd', wd] +
             sys.argv[1:])
    f1 = h5py.File(
                os.path.join(wd, 'nse1.h5'), 'r')
    f2 = h5py.File(
                os.path.join(wd, 'nse2.h5'), 'r')
    if not type(plt) == type(None):
        e1 = 0.5*f1['statistics/moments/velocity'][:, 2, 3]
        e2 = 0.5*f2['statistics/moments/velocity'][:, 2, 3]
        fig = plt.figure()
        a = fig.add_subplot(211)
        a.set_title('energy')
        a.plot(e1)
        a.plot(e2)
        e1 = 0.5*f1['statistics/moments/vorticity'][:, 2, 3]
        e2 = 0.5*f2['statistics/moments/vorticity'][:, 2, 3]
        a = fig.add_subplot(212)
        a.set_title('enstrophy')
        a.plot(e1)
        a.plot(e2)
        fig.tight_layout()
        fig.savefig('comparison.pdf')
        plt.close(fig)
    f1.close()
    f2.close()
    print('SUCCESS! Basic test passed.')
    return None


def main_long():
    niterations = 2048
    N = 64
    njobs = 4
    c0 = DNS()
    c0.launch(
            ['NSVE',
             '-n', '{0}'.format(N),
             '--src-simname', 'B32p1e4',
             '--src-wd', TurTLE.data_dir,
             '--src-iteration', '0',
             '--simname', 'nsve_for_comparison',
             '--np', '4',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--forcing_type', 'linear',
             '--niter_stat', '1',
             '--checkpoints_per_file', '{0}'.format(3),
             '--wd', './'] +
             sys.argv[1:])
    cc = PP()
    cc.launch(
            ['get_velocity',
             '--simname', 'nsve_for_comparison',
             '--np', '4',
             '--ntpp', '1',
             '--iter0', '0',
             '--iter1', '{0}'.format(niterations)] +
             sys.argv[1:])
    c1 = DNS()
    c1.launch(
            ['NSE',
             '-n', '{0}'.format(N),
             '--src-simname', 'nsve_for_comparison',
             '--src-wd', './',
             '--src-iteration', '{0}'.format(niterations),
             '--simname', 'nse1',
             '--np', '4',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations//64),
             '--niter_stat', '1',
             '--forcing_type', 'linear',
             '--njobs', '{0}'.format(njobs),
             '--wd', './'] +
             sys.argv[1:])
    c2 = DNS()
    c2.launch(
            ['NSE_alt_dealias',
             '-n', '{0}'.format(N),
             '--src-simname', 'nsve_for_comparison',
             '--src-wd', './',
             '--src-iteration', '{0}'.format(niterations),
             '--simname', 'nse2',
             '--np', '4',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations//64),
             '--niter_stat', '1',
             '--forcing_type', 'linear',
             '--njobs', '{0}'.format(njobs),
             '--wd', './'] +
             sys.argv[1:])
    cc1 = NSReader(simname = 'nse1')
    cc2 = NSReader(simname = 'nse2')
    cc1.read_full_tk_spectrum()
    cc2.read_full_tk_spectrum()
    if not type(plt) == type(None):
        fig = plt.figure()
        a = fig.add_subplot(211)
        a.set_ylabel('energy')
        a.set_xlabel('iteration')
        a.plot(cc1.statistics['energy(t)'])
        a.plot(cc2.statistics['energy(t)'])
        a = fig.add_subplot(212)
        a.set_ylabel('enstrophy')
        a.set_xlabel('iteration')
        a.plot(cc1.statistics['enstrophy(t)'])
        a.plot(cc2.statistics['enstrophy(t)'])
        fig.suptitle('total physical times are {0:.1f} and {1:.1f} Tint'.format(
                cc1.statistics['t'][-1] / cc1.statistics['Tint'],
                cc2.statistics['t'][-1] / cc2.statistics['Tint']
                ))
        fig.tight_layout()
        fig.savefig('comparison_averages.pdf')
        plt.close(fig)
        fig = plt.figure()
        a = fig.add_subplot(111)
        a.plot(cc1.statistics['kshell'], cc1.statistics['energy(t, k)'][0], color = 'C0', label = 'dealias after')
        a.plot(cc2.statistics['kshell'], cc2.statistics['energy(t, k)'][0], color = 'C1', label = 'dealias before')
        a.plot(cc1.statistics['kshell'], cc1.statistics['energy(t, k)'][2048], color = 'C0', dashes = (1, 1))
        a.plot(cc2.statistics['kshell'], cc2.statistics['energy(t, k)'][2048], color = 'C1', dashes = (1, 1))
        a.plot(cc1.statistics['kshell'], cc1.statistics['energy(t, k)'][4096], color = 'C0', dashes = (2, 3))
        a.plot(cc2.statistics['kshell'], cc2.statistics['energy(t, k)'][4096], color = 'C1', dashes = (2, 3))
        a.plot(cc1.statistics['kshell'], cc1.statistics['energy(t, k)'][8192], color = 'C0', dashes = (3, 5))
        a.plot(cc2.statistics['kshell'], cc2.statistics['energy(t, k)'][8192], color = 'C1', dashes = (3, 5))
        a.set_xscale('log')
        a.set_yscale('log')
        a.legend(loc = 'best')
        fig.tight_layout()
        fig.savefig('comparison_spectra.pdf')
        plt.close(fig)
    return None

if __name__ == '__main__':
    #main_basic()
    main_long()

