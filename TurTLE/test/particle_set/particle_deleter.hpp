/**********************************************************************
*                                                                     *
*  Copyright 2021 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef PARTICLE_DELETER_HPP
#define PARTICLE_DELETER_HPP



#include <cstdlib>
#include "base.hpp"
#include "vorticity_equation.hpp"
#include "full_code/NSVE.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_output_hdf5.hpp"
#include "particles/particles_sampling.hpp"
#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/particle_solver.hpp"
#include "particles/rhs/tracer_with_collision_counter_rhs.hpp"

/** \brief Test of particle deletion functionality.
 *
 *  Child of Navier Stokes vorticity equation solver, this class calls all the
 *  methods from `NSVE`, and in addition:
 *   --- integrates simple Lagrangian tracers in the resulting velocity field.
 *   --- removes arbitrary particles from the set of tracers at each time step.
 *  It also executes a p2p kernel that doesn't affect the particle trajectories
 *  in any way.
 *
 */

template <typename rnumber>
class particle_deleter: public NSVE<rnumber>
{
    public:

        /* parameters that are read in read_parameters */
        int niter_part;
        int niter_part_fine_period;
        int niter_part_fine_duration;
        int nparticles;
        int tracers0_integration_steps;
        int tracers0_neighbours;
        int tracers0_smoothness;

        /* other stuff */
        field_tinterpolator<rnumber, FFTW, THREE, LINEAR> *fti;
        tracer_with_collision_counter_rhs<rnumber, FFTW, LINEAR> *trhs;
        particle_set<3, 3, 2> *pset;
        particle_solver *psolver;

        field<rnumber, FFTW, THREE> *previous_velocity;

        particles_output_hdf5<long long int, double, 3> *particles_output_writer_mpi;
        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;

        particle_deleter(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        ~particle_deleter(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//PARTICLE_DELETER_HPP

