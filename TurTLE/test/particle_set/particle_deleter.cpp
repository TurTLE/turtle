/**********************************************************************
*                                                                     *
*  Copyright 2021 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#include <string>
#include <cmath>
//#include "particle_deleter.hpp"
#include "scope_timer.hpp"

template <typename rnumber>
int particle_deleter<rnumber>::initialize(void)
{
    TIMEZONE("particle_deleter::intialize");
    this->NSVE<rnumber>::initialize();


    // allocate previous velocity
    this->previous_velocity = new field<rnumber, FFTW, THREE>(
            this->fs->cvelocity->rlayout->sizes[2],
            this->fs->cvelocity->rlayout->sizes[1],
            this->fs->cvelocity->rlayout->sizes[0],
            this->fs->cvelocity->rlayout->comm,
            this->fs->cvelocity->fftw_plan_rigor);
    this->fs->compute_velocity(this->fs->cvorticity);
    *this->previous_velocity = *this->fs->cvelocity;
    this->previous_velocity->ift();

    this->fti = new field_tinterpolator<rnumber, FFTW, THREE, LINEAR>();
    this->trhs = new tracer_with_collision_counter_rhs<rnumber, FFTW, LINEAR>();

    // We're not using Adams-Bashforth in this code.
    // This assert is there to ensure
    // user knows what's happening.
    assert(tracers0_integration_steps == 0);
    // neighbours and smoothness are second and third template parameters of particle_set
    assert(tracers0_neighbours == 3);
    assert(tracers0_smoothness == 2);
    this->pset = new particle_set<3, 3, 2>(
            this->fs->cvelocity->rlayout,
            this->dkx,
            this->dky,
            this->dkz,
            0.5);

    // set two fields for the temporal interpolation
    this->fti->set_field(this->previous_velocity, 0);
    this->fti->set_field(this->fs->cvelocity, 1);
    this->trhs->setVelocity(this->fti);

    particles_input_hdf5<long long int, double, 3, 3> particle_reader(
            this->comm,
            this->fs->get_current_fname(),
            std::string("/tracers0/state/") + std::to_string(this->fs->iteration), // dataset name for initial input
            std::string("/tracers0/rhs/")  + std::to_string(this->fs->iteration),  // dataset name for initial input
            this->pset->getSpatialLowLimitZ(),
            this->pset->getSpatialUpLimitZ());
    this->pset->init(particle_reader);

    this->psolver = new particle_solver(*(this->pset), 0);

    this->particles_output_writer_mpi = new particles_output_hdf5<
        long long int, double, 3>(
                MPI_COMM_WORLD,
                "tracers0",
                nparticles,
                tracers0_integration_steps);
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                MPI_COMM_WORLD,
                this->pset->getTotalNumberOfParticles(),
                (this->simname + "_particles.h5"),
                "tracers0",
                "position/0");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int particle_deleter<rnumber>::step(void)
{
    TIMEZONE("particle_deleter::step");
    // compute next step Navier-Stokes
    this->NSVE<rnumber>::step();
    // update velocity 1
    this->fs->compute_velocity(this->fs->cvorticity);
    this->fs->cvelocity->ift();
    // compute particle step using velocity 0 and velocity 1
    this->psolver->Heun(this->dt, *(this->trhs));
    // update velocity 0
    *this->previous_velocity = *this->fs->cvelocity;
    this->psolver->setIteration(this->fs->iteration);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int particle_deleter<rnumber>::write_checkpoint(void)
{
    TIMEZONE("particle_deleter::write_checkpoint");
    this->NSVE<rnumber>::write_checkpoint();
    this->psolver->setIteration(this->fs->iteration);
    this->particles_output_writer_mpi->open_file(this->fs->get_current_fname());
    this->psolver->template writeCheckpoint<3>(this->particles_output_writer_mpi);
    this->particles_output_writer_mpi->close_file();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int particle_deleter<rnumber>::finalize(void)
{
    TIMEZONE("particle_deleter::finalize");
    delete this->particles_sample_writer_mpi;
    delete this->particles_output_writer_mpi;
    delete this->psolver;
    delete this->pset;
    delete this->trhs;
    delete this->fti;
    delete this->previous_velocity;
    this->NSVE<rnumber>::finalize();
    return EXIT_SUCCESS;
}

/** \brief Compute fluid stats and sample fields at particle locations.
 */

template <typename rnumber>
int particle_deleter<rnumber>::do_stats()
{
    TIMEZONE("particle_deleter::do_stats");
    /// fluid stats go here
    this->NSVE<rnumber>::do_stats();


    /// either one of two conditions suffices to compute statistics:
    /// 1) current iteration is a multiple of niter_part
    /// 2) we are within niter_part_fine_duration/2 of a multiple of niter_part_fine_period
    if (!(this->iteration % this->niter_part == 0 ||
          ((this->iteration + this->niter_part_fine_duration/2) % this->niter_part_fine_period <=
           this->niter_part_fine_duration)))
        return EXIT_SUCCESS;

    // sample position
    this->pset->writeStateTriplet(
            0,
            this->particles_sample_writer_mpi,
            "tracers0",
            "position",
            psolver->getIteration());

    // sample velocity
    // first ensure velocity field is computed, and is in real space
    if (!(this->iteration % this->niter_stat == 0))
    {
        // we need to compute velocity field manually, because it didn't happen in NSVE::do_stats()
        this->fs->compute_velocity(this->fs->cvorticity);
        *this->tmp_vec_field = this->fs->cvelocity->get_cdata();
        this->tmp_vec_field->ift();
    }
    this->pset->writeSample(
            this->tmp_vec_field,
            this->particles_sample_writer_mpi,
            "tracers0",
            "velocity",
            psolver->getIteration());

    if (this->pset->getTotalNumberOfParticles() > 3)
    {
        this->pset->writeParticleLabels(
                "particle_index_before.h5",
                "tracers0",
                "index",
                psolver->getIteration());
        std::vector<long long int> indices_to_delete(2);
        indices_to_delete[0] = 1;
        indices_to_delete[1] = (this->iteration*3)%this->pset->getTotalNumberOfParticles();
        if (indices_to_delete[1] == indices_to_delete[0])
            indices_to_delete[1] = 3;
        std::sort(indices_to_delete.begin(), indices_to_delete.end());

        DEBUG_MSG("computed indices_to_delete:\n");
        DEBUG_MSG("indices_to_delete[0] = %d\n",
                indices_to_delete[0]);
        DEBUG_MSG("indices_to_delete[1] = %d\n",
                indices_to_delete[1]);
        DEBUG_MSG("before delete particles, iteration %d\n",
                this->iteration);
        this->pset->_print_debug_info();
        particle_set<3, 3, 2> *tmp_pset = new particle_set<3, 3, 2>(
                this->fs->cvelocity->rlayout,
                this->dkx,
                this->dky,
                this->dkz,
                0.5);
        tmp_pset->init_as_subset_of(
                *(this->pset),
                indices_to_delete,
                true);
        *(this->pset) = tmp_pset;
        delete tmp_pset;

        DEBUG_MSG("after delete particles\n");
        this->pset->_print_debug_info();

        delete this->particles_output_writer_mpi;
        delete this->particles_sample_writer_mpi;
        this->particles_output_writer_mpi = new particles_output_hdf5<
            long long int, double, 3>(
                    MPI_COMM_WORLD,
                    "tracers0",
                    this->pset->getTotalNumberOfParticles(),
                    tracers0_integration_steps);
        this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
            long long int, double, double, 3>(
                    MPI_COMM_WORLD,
                    this->pset->getTotalNumberOfParticles(),
                    (this->simname + "_particles.h5"),
                    "tracers0",
                    "position/0");

        DEBUG_MSG("before writeParticleIndex\n");
        this->pset->writeParticleLabels(
                "particle_index_after.h5",
                "tracers0",
                "index",
                psolver->getIteration());
        DEBUG_MSG("after writeParticleIndex\n");
    }

    // test particle subset extraction
    if (this->pset->getTotalNumberOfParticles() > 4)
    {
        particle_set<3, 1, 0> temp_pset(
            this->fs->cvelocity->rlayout,
            this->dkx,
            this->dky,
            this->dkz);
        std::vector<long long int> indices_to_extract(2);
        // TODO: possibly use a more complicated pattern, but remember to keep
        // indices smaller than current total number of particles
        indices_to_extract[0] = 2;
        indices_to_extract[1] = 4;
        temp_pset.init_as_subset_of(*(this->pset), indices_to_extract, false);
        DEBUG_MSG("before writeParticleLabels for extracted subset\n");
        temp_pset.writeParticleLabels(
                "particle_subset_index.h5",
                "tracers_subset",
                "index",
                psolver->getIteration());
        DEBUG_MSG("after writeParticleLabels for extracted subset\n");
    }

    return EXIT_SUCCESS;
}

template <typename rnumber>
int particle_deleter<rnumber>::read_parameters(void)
{
    TIMEZONE("particle_deleter::read_parameters");
    this->NSVE<rnumber>::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->niter_part = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part");
    this->niter_part_fine_period = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_period");
    this->niter_part_fine_duration = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_duration");
    this->nparticles = hdf5_tools::read_value<int>(parameter_file, "parameters/nparticles");
    this->tracers0_integration_steps = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_integration_steps");
    this->tracers0_neighbours = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_neighbours");
    this->tracers0_smoothness = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_smoothness");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class particle_deleter<float>;
template class particle_deleter<double>;


