/******************************************************************************
*                                                                             *
*  Copyright 2022 Max Planck Computing and Data Facility                      *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef TEST_PARTICLE_SET_INIT_HPP
#define TEST_PARTICLE_SET_INIT_HPP



#include "full_code/test.hpp"

template <typename rnumber>
class test_particle_set_init: public test
{
    public:
        test_particle_set_init(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            test(
                    COMMUNICATOR,
                    simulation_name){}
        ~test_particle_set_init(){}

        int initialize(void);
        int do_work(void);
        int finalize(void);
        int read_parameters(void);
};

#endif//TEST_PARTICLE_SET_INIT_HPP
