/**********************************************************************
*                                                                     *
*  Copyright 2022 Max Planck Computing and Data Facility              *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



//#include "NSVE_tracers_test.hpp"

#include <string>
#include <cmath>

using std::make_pair;

template <typename rnumber>
template <int neighbours, int smoothness>
int NSVE_tracers_test<rnumber>::create_tracers()
{
    TIMEZONE("NSVE_tracers_test::create_solver");
    assert(this->pset[make_pair(0, 0)] != nullptr);
    this->pset[make_pair(neighbours, smoothness)] = new particle_set<3, neighbours, smoothness>(
        this->fs->cvelocity->rlayout,
        this->dkx,
        this->dky,
        this->dkz);
    *(this->pset[make_pair(neighbours, smoothness)]) = this->pset[make_pair(0, 0)];
    this->psolver[make_pair(neighbours, smoothness)] = new particle_solver(
        *(this->pset[make_pair(neighbours, smoothness)]),
        0);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE_tracers_test<rnumber>::initialize(void)
{
    TIMEZONE("NSVE_tracers_test::intialize");
    this->NSVE<rnumber>::initialize();

    // allocate field for acceleration
    this->acceleration = new field<rnumber, FFTW, THREE>(
            this->fs->cvelocity->rlayout->sizes[2],
            this->fs->cvelocity->rlayout->sizes[1],
            this->fs->cvelocity->rlayout->sizes[0],
            this->fs->cvelocity->rlayout->comm,
            this->fs->cvelocity->fftw_plan_rigor);

    // allocate previous velocity
    this->previous_velocity = new field<rnumber, FFTW, THREE>(
            this->fs->cvelocity->rlayout->sizes[2],
            this->fs->cvelocity->rlayout->sizes[1],
            this->fs->cvelocity->rlayout->sizes[0],
            this->fs->cvelocity->rlayout->comm,
            this->fs->cvelocity->fftw_plan_rigor);
    this->fs->compute_velocity(this->fs->cvorticity);
    *this->previous_velocity = *this->fs->cvelocity;
    this->previous_velocity->ift();

    this->fti = new field_tinterpolator<rnumber, FFTW, THREE, LINEAR>();
    this->trhs = new tracer_with_collision_counter_rhs<rnumber, FFTW, LINEAR>();

    // set two fields for the temporal interpolation
    this->fti->set_field(this->previous_velocity, 0);
    this->fti->set_field(this->fs->cvelocity, 1);
    this->trhs->setVelocity(this->fti);

    // create linear interpolation particle set
    this->pset[make_pair(0, 0)] = new particle_set<3, 0, 0>(
            this->fs->cvelocity->rlayout,
            this->dkx,
            this->dky,
            this->dkz);

    // set up initial condition reader
    particles_input_hdf5<long long int, double, 3, 3> particle_reader(
            this->comm,
            this->fs->get_current_fname(),
            std::string("/tracers0/state/") + std::to_string(this->fs->iteration), // dataset name for initial input
            std::string("/tracers0/rhs/")  + std::to_string(this->fs->iteration),  // dataset name for initial input
            this->pset[make_pair(0, 0)]->getSpatialLowLimitZ(),
            this->pset[make_pair(0, 0)]->getSpatialUpLimitZ());

    // read initial condition with linear interpolation particle set
    this->pset[make_pair(0, 0)]->init(particle_reader);

    // create solver for linear interpolation particle set
    this->psolver[make_pair(0, 0)] = new particle_solver(
            *(this->pset[make_pair(0, 0)]),
            0);

    // create Lagrange interpolation particles
    this->template create_tracers<1, 0>();
    this->template create_tracers<2, 0>();
    this->template create_tracers<3, 0>();
    this->template create_tracers<1, 1>();
    this->template create_tracers<2, 1>();
    this->template create_tracers<3, 1>();
    this->template create_tracers<1, 2>();
    this->template create_tracers<2, 2>();
    this->template create_tracers<3, 2>();
    this->template create_tracers<1, 3>();
    this->template create_tracers<2, 3>();


    this->particles_output_writer_mpi = new particles_output_hdf5<
        long long int, double, 3>(
                MPI_COMM_WORLD,
                "tracers0",
                nparticles,
                0);
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                MPI_COMM_WORLD,
                this->pset[make_pair(0, 0)]->getTotalNumberOfParticles(),
                (this->simname + "_particles.h5"),
                "tracers_n0_m0",
                "position/0");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE_tracers_test<rnumber>::step(void)
{
    TIMEZONE("NSVE_tracers_test::step");
    // compute next step Navier-Stokes
    this->NSVE<rnumber>::step();
    // update velocity 1
    this->fs->compute_velocity(this->fs->cvorticity);
    this->fs->cvelocity->ift();
    // compute particle step using velocity 0 and velocity 1
    for (auto it=this->psolver.begin(); it != this->psolver.end(); ++it)
    {
        it->second->cRK4(this->dt, *(this->trhs));
        it->second->setIteration(this->fs->iteration);
    }
    // update velocity 0
    *this->previous_velocity = *this->fs->cvelocity;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE_tracers_test<rnumber>::write_checkpoint(void)
{
    TIMEZONE("NSVE_tracers_test::write_checkpoint");
    this->NSVE<rnumber>::write_checkpoint();

    for (auto it=this->psolver.begin(); it != this->psolver.end(); ++it)
    {
        this->particles_output_writer_mpi->open_file(this->fs->get_current_fname());
        this->particles_output_writer_mpi->update_particle_species_name(
                "tracers_n" +
                std::to_string(it->first.first) +
                "_m" +
                std::to_string(it->first.second));
        it->second->setIteration(this->fs->iteration);
        it->second->template writeCheckpoint<3>(this->particles_output_writer_mpi);
    }
    this->particles_output_writer_mpi->close_file();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE_tracers_test<rnumber>::finalize(void)
{
    TIMEZONE("NSVE_tracers_test::finalize");
    delete this->particles_sample_writer_mpi;
    delete this->particles_output_writer_mpi;
    for (auto it=this->psolver.begin(); it != this->psolver.end(); ++it)
    {
        delete it->second;
    }
    for (auto it=this->pset.begin(); it != this->pset.end(); ++it)
    {
        delete it->second;
    }
    delete this->trhs;
    delete this->fti;
    delete this->previous_velocity;
    delete this->acceleration;
    this->NSVE<rnumber>::finalize();
    return EXIT_SUCCESS;
}

/** \brief Compute fluid stats and sample fields at particle locations.
 */

template <typename rnumber>
int NSVE_tracers_test<rnumber>::do_stats()
{
    TIMEZONE("NSVE_tracers_test::do_stats");
    /// fluid stats go here
    this->NSVE<rnumber>::do_stats();


    /// either one of two conditions suffices to compute statistics:
    /// 1) current iteration is a multiple of niter_part
    /// 2) we are within niter_part_fine_duration/2 of a multiple of niter_part_fine_period
    if (!(this->iteration % this->niter_part == 0 ||
          ((this->iteration + this->niter_part_fine_duration/2) % this->niter_part_fine_period <=
           this->niter_part_fine_duration)))
        return EXIT_SUCCESS;

    // ensure velocity field is computed, and is in real space
    if (!(this->iteration % this->niter_stat == 0))
    {
        // we need to compute velocity field manually, because it didn't happen in NSVE::do_stats()
        this->fs->compute_velocity(this->fs->cvorticity);
        *this->tmp_vec_field = *(this->fs->cvelocity);
        this->tmp_vec_field->ift();
    }

    // compute acceleration
    this->fs->compute_Lagrangian_acceleration(this->acceleration);
    this->acceleration->ift();
    DEBUG_MSG("hello, finished with acceleration\n");

    for (auto it=this->pset.begin(); it != this->pset.end(); ++it)
    {
        const int neighbours = it->first.first;
        const int smoothness = it->first.second;
        abstract_particle_set *ps = it->second;
        const std::string species_name = (
                "tracers_n" +
                std::to_string(neighbours) +
                "_m" +
                std::to_string(smoothness));
        DEBUG_MSG("hello, doing %s\n", species_name.c_str());

        // sample position
        ps->writeStateTriplet(
                0,
                this->particles_sample_writer_mpi,
                species_name,
                "position",
                this->iteration);
        MPI_Barrier(this->comm);

        // sample velocity
        ps->writeSample(
                this->tmp_vec_field,
                this->particles_sample_writer_mpi,
                species_name,
                "velocity",
                this->iteration);
        MPI_Barrier(this->comm);

        // sample velocity
        ps->writeSample(
                this->acceleration,
                this->particles_sample_writer_mpi,
                species_name,
                "acceleration",
                this->iteration);
        MPI_Barrier(this->comm);
    }

    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE_tracers_test<rnumber>::read_parameters(void)
{
    TIMEZONE("NSVE_tracers_test::read_parameters");
    this->NSVE<rnumber>::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->niter_part = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part");
    this->niter_part_fine_period = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_period");
    this->niter_part_fine_duration = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_duration");
    this->nparticles = hdf5_tools::read_value<int>(parameter_file, "parameters/nparticles");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class NSVE_tracers_test<float>;
template class NSVE_tracers_test<double>;

