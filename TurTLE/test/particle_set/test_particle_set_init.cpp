/******************************************************************************
*                                                                             *
*  Copyright 2022 Max Planck Computing and Data Facility                      *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



//#include "test_particle_set_init.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/particle_set_input_random.hpp"
#include "particles/particle_set_input_hdf5.hpp"

template <typename rnumber>
int test_particle_set_init<rnumber>::initialize(void)
{
    TIMEZONE("test_particle_set_init::initialize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int test_particle_set_init<rnumber>::do_work(void)
{
    TIMEZONE("test_particle_set_init::do_work");
    const double twopi = 4*acos(0);
    field<rnumber, FFTW, THREE> vel(this->nx, this->ny, this->nz, this->comm, FFTW_ESTIMATE);

    // generate particle set
    particle_set<3, 2, 1> pset0(vel.rlayout, this->dkx, this->dky, this->dkz);
    particle_set_input_random<long long int, double, 3> pir(this->comm, 10, 1, 0, twopi);
    pset0.init(pir);

    long long int *tmp_label = new long long int[pset0.getLocalNumberOfParticles()];
    std::copy(pset0.getParticleLabels(),
              pset0.getParticleLabels()+pset0.getLocalNumberOfParticles(),
              tmp_label);
    tmp_label[2] = 4;
    tmp_label[3] = 4;

    pset0.setParticleLabels(tmp_label);
    pset0.writeParticleLabels(
            this->simname + std::string("_particles.h5"),
            "tracers0",
            "label",
            0);
    pset0.writeParticleStates<3>(
            this->simname + std::string("_particles.h5"),
            "tracers0",
            "state",
            0);

    particle_set<3, 1, 1> pset1(vel.rlayout, this->dkx, this->dky, this->dkz);
    particle_set_input_hdf5<long long int, double, 3> pif(
            this->comm,
            this->simname + std::string("_particles.h5"),
            "tracers0",
            0,
            0,
            twopi);
    pset1.init(pif);
    pset1.writeParticleLabels(
            this->simname + std::string("_particles.h5"),
            "tracers1",
            "label",
            0);
    pset1.writeParticleStates<3>(
            this->simname + std::string("_particles.h5"),
            "tracers1",
            "state",
            0);


    return EXIT_SUCCESS;
}

template <typename rnumber>
int test_particle_set_init<rnumber>::finalize(void)
{
    TIMEZONE("test_particle_set_init::finalize");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int test_particle_set_init<rnumber>::read_parameters(void)
{
    TIMEZONE("test_particle_set_init::read_parameters");
    this->test::read_parameters();
    return EXIT_SUCCESS;
}

template class test_particle_set_init<float>;
template class test_particle_set_init<double>;
