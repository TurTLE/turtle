/**********************************************************************
*                                                                     *
*  Copyright 2019 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#include <string>
#include <cmath>
//#include "NSVEparticle_set.hpp"
#include "scope_timer.hpp"

template <typename rnumber>
int NSVEparticle_set<rnumber>::initialize(void)
{
    TIMEZONE("NSVEparticle_set::intialize");
    this->NSVE<rnumber>::initialize();


    // allocate previous velocity
    this->previous_velocity = new field<rnumber, FFTW, THREE>(
            this->fs->cvelocity->rlayout->sizes[2],
            this->fs->cvelocity->rlayout->sizes[1],
            this->fs->cvelocity->rlayout->sizes[0],
            this->fs->cvelocity->rlayout->comm,
            this->fs->cvelocity->fftw_plan_rigor);
    this->fs->compute_velocity(this->fs->cvorticity);
    *this->previous_velocity = *this->fs->cvelocity;
    this->previous_velocity->ift();

    this->fti = new field_tinterpolator<rnumber, FFTW, THREE, LINEAR>();
    this->trhs = new tracer_with_collision_counter_rhs<rnumber, FFTW, LINEAR>();

    // We're not using Adams-Bashforth in this code.
    // This assert is there to ensure
    // user knows what's happening.
    assert(tracers0_integration_steps == 0);
    // neighbours and smoothness are second and third template parameters of particle_set
    assert(tracers0_neighbours == 3);
    assert(tracers0_smoothness == 2);
    this->pset = new particle_set<3, 3, 2>(
            this->fs->cvelocity->rlayout,
            this->dkx,
            this->dky,
            this->dkz,
            0.5);

    // technical setting helpful when more particle sets are used
    // if more particle species are present, species number `s` should receive
    // the value `s * ninterpolations` where `ninterpolations` is the total
    // number of interpolations performed in the ::step() and ::do_stats()
    // methods.
    this->pset->setMPITagShiftCounter(0);

    // set two fields for the temporal interpolation
    this->fti->set_field(this->previous_velocity, 0);
    this->fti->set_field(this->fs->cvelocity, 1);
    this->trhs->setVelocity(this->fti);

    particles_input_hdf5<long long int, double, 3, 3> particle_reader(
            this->comm,
            this->fs->get_current_fname(),
            std::string("/tracers0/state/") + std::to_string(this->fs->iteration), // dataset name for initial input
            std::string("/tracers0/rhs/")  + std::to_string(this->fs->iteration),  // dataset name for initial input
            this->pset->getSpatialLowLimitZ(),
            this->pset->getSpatialUpLimitZ());
    this->pset->init(particle_reader);

    this->psolver = new particle_solver(*(this->pset), 0);

    this->particles_output_writer_mpi = new particles_output_hdf5<
        long long int, double, 3>(
                MPI_COMM_WORLD,
                "tracers0",
                nparticles,
                tracers0_integration_steps);
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                MPI_COMM_WORLD,
                this->pset->getTotalNumberOfParticles(),
                (this->simname + "_particles.h5"),
                "tracers0",
                "position/0");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticle_set<rnumber>::step(void)
{
    TIMEZONE("NSVEparticle_set::step");
    // compute next step Navier-Stokes
    this->NSVE<rnumber>::step();
    // update velocity 1
    this->fs->compute_velocity(this->fs->cvorticity);
    this->fs->cvelocity->ift();
    // compute particle step using velocity 0 and velocity 1
    this->psolver->Heun(this->dt, *(this->trhs));
    // technical step needed to prevent MPI tags from growing too large
    this->pset->resetMPITagShiftCounter();
    // update velocity 0
    *this->previous_velocity = *this->fs->cvelocity;
    this->psolver->setIteration(this->fs->iteration);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticle_set<rnumber>::write_checkpoint(void)
{
    TIMEZONE("NSVEparticle_set::write_checkpoint");
    this->NSVE<rnumber>::write_checkpoint();
    this->psolver->setIteration(this->fs->iteration);
    this->particles_output_writer_mpi->open_file(this->fs->get_current_fname());
    this->psolver->template writeCheckpoint<3>(this->particles_output_writer_mpi);
    this->particles_output_writer_mpi->close_file();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticle_set<rnumber>::finalize(void)
{
    TIMEZONE("NSVEparticle_set::finalize");
    delete this->particles_sample_writer_mpi;
    delete this->particles_output_writer_mpi;
    delete this->psolver;
    delete this->pset;
    delete this->trhs;
    delete this->fti;
    delete this->previous_velocity;
    this->NSVE<rnumber>::finalize();
    return EXIT_SUCCESS;
}

/** \brief Compute fluid stats and sample fields at particle locations.
 */

template <typename rnumber>
int NSVEparticle_set<rnumber>::do_stats()
{
    TIMEZONE("NSVEparticle_set::do_stats");
    /// fluid stats go here
    this->NSVE<rnumber>::do_stats();


    /// either one of two conditions suffices to compute statistics:
    /// 1) current iteration is a multiple of niter_part
    /// 2) we are within niter_part_fine_duration/2 of a multiple of niter_part_fine_period
    if (!(this->iteration % this->niter_part == 0 ||
          ((this->iteration + this->niter_part_fine_duration/2) % this->niter_part_fine_period <=
           this->niter_part_fine_duration)))
        return EXIT_SUCCESS;

    // sample position
    this->pset->writeStateTriplet(
            0,
            this->particles_sample_writer_mpi,
            "tracers0",
            "position",
            psolver->getIteration());

    MPI_Barrier(this->comm);

    // sample velocity
    // first ensure velocity field is computed, and is in real space
    DEBUG_MSG("test 00\n");
    if (!(this->iteration % this->niter_stat == 0))
    {
        // we need to compute velocity field manually, because it didn't happen in NSVE::do_stats()
        this->fs->compute_velocity(this->fs->cvorticity);
        *this->tmp_vec_field = *(this->fs->cvelocity);
        this->tmp_vec_field->ift();
    }
    DEBUG_MSG("test 01\n");
    MPI_Barrier(this->comm);
    this->pset->writeSample(
            this->tmp_vec_field,
            this->particles_sample_writer_mpi,
            "tracers0",
            "velocity",
            psolver->getIteration());

    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticle_set<rnumber>::read_parameters(void)
{
    TIMEZONE("NSVEparticle_set::read_parameters");
    this->NSVE<rnumber>::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->niter_part = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part");
    this->niter_part_fine_period = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_period");
    this->niter_part_fine_duration = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_duration");
    this->nparticles = hdf5_tools::read_value<int>(parameter_file, "parameters/nparticles");
    this->tracers0_integration_steps = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_integration_steps");
    this->tracers0_neighbours = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_neighbours");
    this->tracers0_smoothness = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_smoothness");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class NSVEparticle_set<float>;
template class NSVEparticle_set<double>;

