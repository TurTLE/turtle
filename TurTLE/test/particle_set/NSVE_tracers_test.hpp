/**********************************************************************
*                                                                     *
*  Copyright 2022 Max Planck Computing and Data Facility              *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#ifndef NSVE_TRACERS_TEST_HPP
#define NSVE_TRACERS_TEST_HPP



#include "base.hpp"
#include "vorticity_equation.hpp"
#include "full_code/NSVE.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_output_hdf5.hpp"
#include "particles/particles_sampling.hpp"
#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/particle_solver.hpp"
#include "particles/rhs/tracer_with_collision_counter_rhs.hpp"

#include <cstdlib>
#include <map>

/** \brief Test of tracer integration.
 *
 *  Child of Navier Stokes vorticity equation solver, this class calls all the
 *  methods from `NSVE`, and in addition integrates simple Lagrangian tracers
 *  in the resulting velocity field.
 *
 *  The code is meant to compare results of using different interpolation methods.
 */

template <typename rnumber>
class NSVE_tracers_test: public NSVE<rnumber>
{
    public:

        /* parameters that are read in read_parameters */
        int niter_part;
        int niter_part_fine_period;
        int niter_part_fine_duration;
        long long int nparticles;

        /* other stuff */
        field_tinterpolator<rnumber, FFTW, THREE, LINEAR> *fti;
        tracer_rhs<rnumber, FFTW, LINEAR> *trhs;

        std::map<std::pair<int, int>, abstract_particle_set *> pset;
        std::map<std::pair<int, int>, particle_solver *> psolver;

        field<rnumber, FFTW, THREE> *previous_velocity;
        field<rnumber, FFTW, THREE> *acceleration;

        particles_output_hdf5<long long int, double, 3> *particles_output_writer_mpi;
        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;

        NSVE_tracers_test(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        ~NSVE_tracers_test(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);

        template <int neighbours, int smoothness>
            int create_tracers();
};

#endif//NSVE_TRACERS_TEST_HPP

