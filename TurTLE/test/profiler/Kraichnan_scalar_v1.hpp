/**********************************************************************
*                                                                     *
*  Copyright 2017 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef KRAICHNAN_SCALAR_V1
#define KRAICHNAN_SCALAR_V1



#include <cstdlib>
#include "base.hpp"
#include "full_code/direct_numerical_simulation.hpp"

template <typename rnumber>
class Kraichnan_scalar_v1: public direct_numerical_simulation
{
    public:

        /* parameters that are read in read_parameters */
        double dt;
        int histogram_bins;
        double max_value_estimate;
        double nu;
        std::string fftw_plan_rigor;

        int random_seed;
        double spectrum_dissipation;
        double spectrum_Lint;
        double spectrum_etaK;
        double spectrum_large_scale_const;
        double spectrum_small_scale_const;

        /* other stuff */
        field<rnumber, FFTW, ONE> *scalar;
        field<rnumber, FFTW, ONE> *tscal0;
        field<rnumber, FFTW, ONE> *ux, *uy, *uz;
        kspace<FFTW, SMOOTH> *kk;


        Kraichnan_scalar_v1(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            direct_numerical_simulation(
                    COMMUNICATOR,
                    simulation_name){}
        ~Kraichnan_scalar_v1(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        virtual int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//KRAICHNAN_SCALAR_V1

