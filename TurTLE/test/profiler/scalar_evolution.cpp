/******************************************************************************
*                                                                             *
*  Copyright 2019 Max Planck Institute for Dynamics and Self-Organization     *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include <string>
#include <cmath>
//#include "scalar_evolution.hpp"
#include "scope_timer.hpp"
#include "fftw_tools.hpp"


template <typename rnumber>
int scalar_evolution<rnumber>::initialize(void)
{
    TIMEZONE("scalar_evolution::initialize");
    this->read_iteration();
    this->read_parameters();
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    this->grow_file_datasets();

    this->scalar = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tscal0 = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    this->tscal1 = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->kk = new kspace<FFTW, SMOOTH>(
            this->scalar->clayout,
            this->dkx, this->dky, this->dkz);

    make_gaussian_random_field<rnumber, FFTW, ONE, SMOOTH>(
            this->kk,
            this->scalar,
            this->random_seed,
            this->spectrum_dissipation,
            this->spectrum_Lint,
            this->spectrum_etaK,
            this->spectrum_large_scale_const,
            this->spectrum_small_scale_const);

    this->scalar->symmetrize();
    this->kk->template dealias<rnumber, ONE>(this->scalar->get_cdata());
    this->scalar->ift();

    if (this->myrank == 0 && this->iteration == 0)
        this->kk->store(stat_file);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int scalar_evolution<rnumber>::step(void)
{
    TIMEZONE("scalar_evolution::step");
    this->iteration++;

    // store real space field
    *(this->tscal0) = *(this->scalar);

    // go to Fourier space
    this->scalar->dft();

    make_gaussian_random_field<rnumber, FFTW, ONE, SMOOTH>(
            this->kk,
            this->tscal1,
            this->random_seed+this->iteration,
            this->spectrum_dissipation,
            this->spectrum_Lint,
            this->spectrum_etaK,
            this->spectrum_large_scale_const,
            this->spectrum_small_scale_const);

    // some nontrivial Fourier space evolution
    this->kk->CLOOP(
                [&](const ptrdiff_t cindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex,
                    const double k2){
        const double kval = sqrt(k2);
        if (k2 <= this->kk->kM2)
        {
            this->scalar->cval(cindex,0) += this->dt*(
                    (-this->nu*k2 + this->mu*kval) * this->scalar->cval(cindex, 0) + // linear term
                    (kval / this->kk->kM2) * this->tscal1->cval(cindex, 0) // random change, larger for larger wavenumbers
                    );
            this->scalar->cval(cindex,1) += this->dt*(
                    (-this->nu*k2 + this->mu*kval) * this->scalar->cval(cindex, 1) + // linear term
                    (kval / this->kk->kM2) * this->tscal1->cval(cindex, 1) // random change, larger for larger wavenumbers
                    );
        }
        else
            std::fill_n((rnumber*)(this->scalar->get_cdata()+cindex), 2, 0.0);
    });

    // take random field to real space
    this->tscal1->ift();

    this->tscal0->RLOOP (
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
                this->tscal0->rval(rindex) *= this->tscal1->rval(rindex) / this->scalar->npoints;
        }
        );

    this->tscal0->dft();
    // dealias
    this->kk->template dealias<rnumber, ONE>(this->tscal0->get_cdata());
    // another nontrivial Fourier space evolution
    this->kk->CLOOP(
                [&](const ptrdiff_t cindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex,
                    const double k2){
        const double kval = 1 + sqrt(k2);
        const double scal_val = sqrt(
                this->scalar->cval(cindex, 0)*this->scalar->cval(cindex, 0) +
                this->scalar->cval(cindex, 1)*this->scalar->cval(cindex, 1));
        if (k2 <= this->kk->kM2)
        {
            this->scalar->cval(cindex,0) /= this->scalar->npoints;
            this->scalar->cval(cindex,0) -= this->dt*(
                    this->tscal0->cval(cindex, 1)*scal_val / kval
                    );
            this->scalar->cval(cindex,1) /= this->scalar->npoints;
            this->scalar->cval(cindex,1) += this->dt*(
                    this->tscal0->cval(cindex, 0)*scal_val / kval
                    );
        }
        else
            std::fill_n((rnumber*)(this->scalar->get_cdata()+cindex), 2, 0.0);
    });

    // symmetrize
    this->scalar->symmetrize();

    // go back to real space
    this->scalar->ift();

    return EXIT_SUCCESS;
}

template <typename rnumber>
int scalar_evolution<rnumber>::write_checkpoint(void)
{
    TIMEZONE("scalar_evolution::write_checkpoint");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int scalar_evolution<rnumber>::finalize(void)
{
    TIMEZONE("scalar_evolution::finalize");
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    delete this->tscal0;
    delete this->tscal1;
    delete this->scalar;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int scalar_evolution<rnumber>::do_stats()
{
    TIMEZONE("scalar_evolution::do_stats");
    if (!(this->iteration % this->niter_stat == 0))
        return EXIT_SUCCESS;
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "statistics",
                H5P_DEFAULT);
    else
        stat_group = 0;

    *(this->tscal0) = *(this->scalar);

    this->tscal0->compute_stats(
            this->kk,
            stat_group,
            "scalar",
            this->iteration / niter_stat,
            max_value_estimate);

    if (this->myrank == 0)
        H5Gclose(stat_group);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int scalar_evolution<rnumber>::read_parameters(void)
{
    TIMEZONE("scalar_evolution::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->nu = hdf5_tools::read_value<double>(parameter_file, "parameters/nu");
    this->mu = hdf5_tools::read_value<double>(parameter_file, "parameters/mu");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "parameters/dt");
    this->injection_rate = hdf5_tools::read_value<double>(parameter_file, "parameters/injection_rate");
    this->fk0 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk0");
    this->fk1 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk1");
    this->histogram_bins = hdf5_tools::read_value<int>(parameter_file, "parameters/histogram_bins");
    this->max_value_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_value_estimate");
    this->fftw_plan_rigor = hdf5_tools::read_string(parameter_file, "parameters/fftw_plan_rigor");

    this->spectrum_dissipation = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_dissipation");
    this->spectrum_Lint = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_Lint");
    this->spectrum_etaK = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_etaK");
    this->spectrum_large_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_large_scale_const");
    this->spectrum_small_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_small_scale_const");
    this->random_seed = hdf5_tools::read_value<int>(parameter_file, "/parameters/field_random_seed");
    H5Fclose(parameter_file);
    return EXIT_SUCCESS;
}

template class scalar_evolution<float>;
template class scalar_evolution<double>;

