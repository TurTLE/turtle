import os
import h5py
import random

from TurTLE import DNS

def main():
    for simulation_type in ['NSVE', 'NSE']:
        # generate random parameters
        # this list should correspond to the list output by the message "DNS will use physical parameters from source simulation."
        # as of right now, line 1158 of TurTLE/DNS.py
        random_parameters = [
                '--energy', '{0}'.format(random.random()),
                '--famplitude', '{0}'.format(random.random()),
                '--fk0', '{0}'.format(random.random()*4),
                '--fk1', '{0}'.format(4 + random.random()*4),
                '--fmode', '{0}'.format(1+int(random.random()*3)),
                '--friction_coefficient', '{0}'.format(random.random()),
                '--injection_rate', '{0}'.format(random.random()),
                '--nu', '{0}'.format(random.random()),
                '--forcing_type', 'fixed_energy']

        # generate parent simulation with the above parameters
        c = DNS()
        print('simulation type is ', simulation_type)
        c.launch([
            simulation_type,
            '-n', '48',
            '--wd', 'test_parameter_creation',
            '--simname', 'test_' + simulation_type + '_source',
            '--niter_todo', '1',
            '--niter_stat', '1',
            '--niter_out', '1',
            '--no-submit',
            '--field_random_seed', '0',
            ] + random_parameters)

        # generate fields for child simulation
        cp_fname = os.path.join(
                c.work_dir,
                c.simname + '_checkpoint_0.h5')
        with h5py.File(cp_fname, 'a') as ofile:
            data = c.generate_vector_field(
                    write_to_file = False,
                    spectra_slope = 2.0,
                    rseed = c.parameters['field_random_seed'],
                    amplitude = 0.05)
            if simulation_type == 'NSVE':
                ofile['vorticity/complex/0'] = data
            elif simulation_type == 'NSE':
                ofile['velocity/complex/0'] = data
            else:
                assert 0

        # generate child simulation
        c = DNS()
        c.launch([
            simulation_type,
            '-n', '48',
            '--wd', 'test_parameter_creation',
            '--simname', 'test_' + simulation_type + '_destination',
            '--niter_todo', '1',
            '--niter_stat', '1',
            '--niter_out', '1',
            '--src-simname', 'test_' + simulation_type + '_source',
            '--src-iteration', '0',
            '--no-submit',
            ])

        # compare parameters between the two simulations
        f1 = h5py.File(os.path.join(
            'test_parameter_creation',
            'test_' + simulation_type + '_source.h5'), 'r')
        f2 = h5py.File(os.path.join(
            'test_parameter_creation',
            'test_' + simulation_type + '_destination.h5'), 'r')

        # list MUST agree with parameters from "random_parameters"
        for kk in ['energy', 'famplitude', 'fk0', 'fk1', 'fmode', 'friction_coefficient', 'injection_rate', 'nu']:
            v1 = f1['parameters/' + kk][()]
            v2 = f2['parameters/' + kk][()]
            print(kk, v1, v2)
            assert(v1 == v2)
    return None

if __name__ == '__main__':
    main()

