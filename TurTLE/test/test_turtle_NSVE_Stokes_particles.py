#! /usr/bin/env python
#######################################################################
#                                                                     #
#  Copyright 2019 Max Planck Institute                                #
#                 for Dynamics and Self-Organization                  #
#                                                                     #
#  This file is part of TurTLE.                                       #
#                                                                     #
#  TurTLE is free software: you can redistribute it and/or modify     #
#  it under the terms of the GNU General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License,  #
#  or (at your option) any later version.                             #
#                                                                     #
#  TurTLE is distributed in the hope that it will be useful,          #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of     #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      #
#  GNU General Public License for more details.                       #
#                                                                     #
#  You should have received a copy of the GNU General Public License  #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     #
#                                                                     #
# Contact: Cristian.Lalescu@ds.mpg.de                                 #
#                                                                     #
#######################################################################



import os
import numpy as np
import h5py
import sys
import matplotlib.pyplot as plt


import TurTLE
from TurTLE import DNS

def theory_exponential(t,initial_value,drag_coeff):
    return initial_value*np.exp(-t*drag_coeff)

def main():
    """
        Run test where the trajectory of initially moving particles with varying drag coefficient in a
	quiescent flow is compared to the analytically expected exponential. The generated plot shows the accuracy of the corresponding particles for a certain fixed timestep.  
    """
    drag_coefficients = ['30','20','10','1']
    niterations = 100
    nparticles = 1
    njobs = 1
    dt=0.01
    fig = plt.figure()
    plt.ylabel('$v$')
    plt.xlabel('$t$')
    plt.yscale('log')
    j=0
    for drag_coeff in drag_coefficients:
        c = DNS()
        c.launch(
            ['NSVE_Stokes_particles',
             '-n', '32',
             '--simname', 'quiescent_nsve_stokes_particles_drag'+str(j),
             '--np', '4',
             '--ntpp', '1',
             '--dt', '{0}'.format(dt),
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(1),
             '--niter_stat', '1',
             '--nparticles', '{0}'.format(nparticles),
             '--initial_field_amplitude', '0',
             '--initial_particle_vel', '0.05',
             '--drag_coefficient', drag_coeff, 
             '--famplitude', '0',
             '--njobs', '{0}'.format(njobs),
             '--wd', './'] +
             sys.argv[1:])
        f = h5py.File('quiescent_nsve_stokes_particles_drag'+str(j)+'_particles.h5', 'r')
        f2 = h5py.File('quiescent_nsve_stokes_particles_drag'+str(j)+'.h5', 'r')
        particle_momentum = [np.sqrt(f['tracers0/momentum/'+'{}'.format(i)][0][0]**2
                                     +f['tracers0/momentum/'+'{}'.format(i)][0][1]**2
                                     +f['tracers0/momentum/'+'{}'.format(i)][0][2]**2)
                             for i in range(niterations)]
        time_values = np.arange(0, niterations*dt, dt)
        numerics, = plt.plot(time_values, particle_momentum, alpha = 1, lw=2)
        plt.plot(time_values, 
            theory_exponential(time_values, f2['parameters/initial_particle_vel'],float(drag_coeff)),
            color='black', ls = '--')
        plt.plot(0,0,color=numerics.get_color(),label=r'drag coefficient '+drag_coeff)
        j+=1
    plt.plot(0, 
            0,
            color='black', ls = '--',
            label='analytic sol.')
    plt.legend(loc='lower left')
    plt.savefig('Stokes_quiescent_test.pdf')
    return None

if __name__ == '__main__':
    main()

