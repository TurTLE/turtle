#! /usr/bin/env python
#######################################################################
#                                                                     #
#  Copyright 2021 the TurTLE team                                     #
#                                                                     #
#  This file is part of TurTLE.                                       #
#                                                                     #
#  TurTLE is free software: you can redistribute it and/or modify     #
#  it under the terms of the GNU General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License,  #
#  or (at your option) any later version.                             #
#                                                                     #
#  TurTLE is distributed in the hope that it will be useful,          #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of     #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      #
#  GNU General Public License for more details.                       #
#                                                                     #
#  You should have received a copy of the GNU General Public License  #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     #
#                                                                     #
# Contact: Cristian.Lalescu@mpcdf.mpg.de                              #
#                                                                     #
#######################################################################



import os
import numpy as np
import h5py
import sys
import argparse

import TurTLE
from TurTLE import DNS, PP


try:
    import matplotlib.pyplot as plt
except:
    plt = None


def main_basic():
    niterations = 32
    njobs = 2
    c0 = DNS()
    c0.launch(
            ['NSVE',
             '-n', '32',
             '--src-simname', 'B32p1e4',
             '--src-wd', TurTLE.data_dir,
             '--src-iteration', '0',
             '--simname', 'nsve_for_comparison',
             '--np', '4',
             '--ntpp', '2',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--forcing_type', 'linear',
             '--niter_stat', '1',
             '--checkpoints_per_file', '{0}'.format(3),
             '--njobs', '{0}'.format(njobs),
             '--wd', './'])
    cc = PP()
    cc.launch(
            ['get_velocity',
             '--simname', 'nsve_for_comparison',
             '--np', '4',
             '--ntpp', '2',
             '--iter0', '0',
             '--iter1', '64'])
    c1 = DNS()
    c1.launch(
            ['NSE',
             '-n', '32',
             '--src-simname', 'nsve_for_comparison',
             '--src-wd', './',
             '--src-iteration', '0',
             '--simname', 'nse_for_comparison',
             '--np', '4',
             '--ntpp', '2',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--niter_stat', '1',
             '--forcing_type', 'linear',
             '--checkpoints_per_file', '{0}'.format(3),
             '--njobs', '{0}'.format(njobs),
             '--wd', './'])
    if not type(plt) == type(None):
        c0 = DNS(simname = 'nsve_for_comparison')
        c1 = DNS(simname = 'nse_for_comparison')
        for cc in [c0, c1]:
            cc.compute_statistics()
            cc.plot_basic_stats()
    f0 = h5py.File(
                'nsve_for_comparison.h5', 'r')
    f1 = h5py.File(
                'nse_for_comparison.h5', 'r')
    if not type(plt) == type(None):
        e0 = 0.5*f0['statistics/moments/velocity'][:, 2, 3]
        e1 = 0.5*f1['statistics/moments/velocity'][:, 2, 3]
        fig = plt.figure()
        a = fig.add_subplot(111)
        a.plot(e0)
        a.plot(e1)
        fig.tight_layout()
        fig.savefig('comparison_energy.pdf')
        plt.close(fig)
    f0.close()
    f1.close()
    f0 = h5py.File(
                'nsve_for_comparison_checkpoint_0.h5', 'r')
    f1 = h5py.File(
                'nse_for_comparison_checkpoint_0.h5', 'r')
    for iteration in [0, 32, 64]:
        field0 = f0['velocity/complex/{0}'.format(iteration)][...]
        field1 = f1['velocity/complex/{0}'.format(iteration)][...]
        field_error = np.max(np.abs(field0 - field1))
        print(field_error)
        assert(field_error < 1e-1)
    f0.close()
    f1.close()
    print('SUCCESS! Basic test passed.')
    return None

def main_long():
    niterations = 2048
    njobs = 2
    c0 = DNS()
    c0.launch(
            ['NSVE',
             '-n', '64',
             '--src-simname', 'B32p1e4',
             '--src-wd', TurTLE.data_dir,
             '--src-iteration', '0',
             '--simname', 'nsve_for_long_comparison',
             '--np', '16',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--forcing_type', 'linear',
             '--niter_stat', '1',
             '--checkpoints_per_file', '{0}'.format(3),
             '--njobs', '{0}'.format(njobs),
             '--wd', './'])
    cc = PP()
    cc.launch(
            ['get_velocity',
             '--simname', 'nsve_for_long_comparison',
             '--np', '16',
             '--ntpp', '1',
             '--iter0', '0',
             '--iter1', '{0}'.format(niterations)])
    c1 = DNS()
    c1.launch(
            ['NSE',
             '-n', '64',
             '--src-simname', 'nsve_for_long_comparison',
             '--src-wd', './',
             '--src-iteration', '0',
             '--simname', 'nse_for_long_comparison',
             '--np', '16',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations),
             '--niter_stat', '1',
             '--forcing_type', 'linear',
             '--checkpoints_per_file', '{0}'.format(3),
             '--njobs', '{0}'.format(njobs),
             '--wd', './'])
    def get_err(a, b):
        return np.abs(a - b) / (0.5 * (a + b))
    if not type(plt) == type(None):
        c0 = DNS(simname = 'nsve_for_long_comparison')
        c1 = DNS(simname = 'nse_for_long_comparison')
        for cc in [c0, c1]:
            cc.compute_statistics()
            cc.plot_basic_stats()
        ##########
        fig = plt.figure()
        a = fig.add_subplot(211)
        a.plot(c0.statistics['energy(t)'],
                label = 'NSVE')
        a.plot(c1.statistics['energy(t)'],
                label = 'NSE')
        a.legend(loc = 'best')
        a = fig.add_subplot(212)
        a.plot(c0.statistics['kshell'],
               c0.statistics['energy(k)'],
               label = 'NSVE')
        a.plot(c1.statistics['kshell'],
               c1.statistics['energy(k)'],
               label = 'NSE')
        a.set_xscale('log')
        a.set_yscale('log')
        a.set_ylim(1e-7, 10)
        fig.tight_layout()
        fig.savefig('comparison_energy_long.pdf')
        plt.close(fig)
        ##########
        fig = plt.figure()
        a = fig.add_subplot(211)
        a.plot(c0.statistics['vel_max(t)'],
                label = 'NSVE')
        a.plot(c1.statistics['vel_max(t)'],
                label = 'NSE')
        a.legend(loc = 'best')
        a = fig.add_subplot(212)
        err = get_err(
                c0.statistics['vel_max(t)'],
                c1.statistics['vel_max(t)'])
        a.plot(c0.statistics['t'] / c0.statistics['tauK'],
               err,
               label = 'velocity')
        vort0_max = c0.get_data_file()['statistics/moments/vorticity'][:, 9, 3]
        vort1_max = c1.get_data_file()['statistics/moments/vorticity'][:, 9, 3]
        a.plot(c0.statistics['t'] / c0.statistics['tauK'],
               get_err(vort0_max, vort1_max),
               label = 'vorticity')
        a.legend(loc = 'best')
        a.set_yscale('log')
        a.set_ylim(bottom = 1e-6)
        fig.tight_layout()
        fig.savefig('comparison_velmax_long.pdf')
        plt.close(fig)
    return None

if __name__ == '__main__':
    parser = argparse.ArgumentParser('compare NSE with NSVE')
    parser.add_argument('--long', dest = 'long', action = 'store_true')
    opt = parser.parse_args()
    if opt.long:
        main_long()
    else:
        main_basic()

