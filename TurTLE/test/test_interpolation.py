#! /usr/bin/env python

import os
import numpy as np
import h5py
import sys

import TurTLE
from TurTLE import TEST

try:
    import matplotlib.pyplot as plt
    matplotlib_on = True
except ImportError:
    matplotlib_on = False


def main():
    nparticles = 100
    c = TEST()
    c.launch(
            ['test_interpolation',
             '-n', '32',
             '--np', '4',
             '--ntpp', '1',
             #'--nparticles', '{0}'.format(nparticles),
             '--wd', './'] +
             sys.argv[3:])
    ifile = h5py.File(
            'test_input.h5',
            'r')
    ofile = h5py.File(
            'test_output.h5',
            'r')
    pos0 = ifile['tracers0/state/0'][...]
    pos1 = ofile['tracers0/position/0'][...]
    assert(np.max(np.abs(pos0-pos1) / np.abs(pos0)) <= 1e-5)
    vort0 = ofile['tracers0/vorticity/0'][...]
    vel_gradient = ofile['tracers0/velocity_gradient/0'][...]
    vort1 = vort0.copy()
    vort1[:, 0] = vel_gradient[:, 5] - vel_gradient[:, 7]
    vort1[:, 1] = vel_gradient[:, 6] - vel_gradient[:, 2]
    vort1[:, 2] = vel_gradient[:, 1] - vel_gradient[:, 3]
    assert(np.max(np.abs(vort0-vort1) / np.abs(vort0)) <= 1e-5)
    divergence = vel_gradient[:, 0] + vel_gradient[:, 4] + vel_gradient[:, 8]
    divergence_error = np.abs(divergence) / (vel_gradient[:, 0]**2 + vel_gradient[:, 1]**2 + vel_gradient[:, 2]**2)**.5
    print('mean divergence error is ', np.mean(divergence_error))
    print('maximum divergence error is ', np.max(divergence_error))
    print('SUCCESS! Interpolated vorticity agrees with vorticity from interpolated velocity gradient.')
    return None

if __name__ == '__main__':
    main()

