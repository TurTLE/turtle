import TurTLE
from TurTLE import DNS

def main():
    c = DNS()
    c.launch([
        'NSVE',
        '--simname', 'test_statistics',
        '--src-simname', 'B32p1e4',
        '--src-wd', TurTLE.data_dir,
        '--niter_todo', '32',
        '--niter_stat', '1',
        '--max_velocity_estimate', '10',
        '--max_vorticity_estimate', '20'])
    c.compute_statistics()

    # velocity
    mm = c.read_moments()
    bb, hh, pdf = c.read_histogram()
    print(c.compute_statistics_error())
    print(mm[2, 3]*0.5, c.statistics['energy'])

    # vorticity
    mm = c.read_moments(quantity = 'vorticity')
    bb, hh, pdf = c.read_histogram(quantity = 'vorticity')
    print(c.compute_statistics_error(quantity = 'vorticity'))
    print(mm[2, 3]*0.5, c.statistics['enstrophy'])
    return None

if __name__ == '__main__':
    main()
