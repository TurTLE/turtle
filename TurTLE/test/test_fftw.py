#! /usr/bin/env python

import numpy as np
import h5py
import sys
import argparse

from TurTLE import TEST

try:
    import matplotlib.pyplot as plt
except:
    plt = None

try:
    import cmocean
except:
    cmocean = None

def main():
    parser = argparse.ArgumentParser('Compare FFTW with `numpy.fft`.')
    parser.add_argument('-p', '--plot', dest = 'plot_on', action = 'store_true')
    opt = parser.parse_args()

    niterations = 10
    nlist = [8, 24, 32, 16, 12]
    for ii in range(len(nlist)):
        c = TEST()
        c.launch(
                ['symmetrize_test',
                 '--nx', str(nlist[ii]),
                 '--ny', str(nlist[(ii+1)%(len(nlist))]),
                 '--nz', str(nlist[(ii+2)%(len(nlist))]),
                 '--Lx', str(2+np.random.random()),
                 '--Ly', str(2+np.random.random()),
                 '--Lz', str(2+np.random.random()),
                 '--simname', 'fftw_vs_numpy_{0}'.format(ii),
                 '--np', '4',
                 '--ntpp', '1',
                 '--wd', './'] +
                 sys.argv[1:])
        df = h5py.File(c.simname + '_fields.h5', 'r')
        field0_complex = df['field0/complex/0'][...]
        field1_complex = df['field1/complex/0'][...]
        field1_real = df['field1/real/0'][...]
        npoints = field1_real.size//3

        # first test symmetrize
        # field0 is subject to TurTLE symmetrize
        # field1 has additionally been through ift+dft+normalize
        # ideally they should be identical. otherwise it means
        # the TurTLE symmetrize is broken.
        f0 = field0_complex[:, :, 0, :]
        f1 = field1_complex[:, :, 0, :]
        diff = np.abs(f0 - f1)
        ii = np.unravel_index(np.argmax(diff), diff.shape)
        print('found maximum difference {0} between field0 and field1 at {1}'.format(diff[ii[0], ii[1], ii[2]], ii))
        assert(np.max(np.abs(diff)) < 1e-5)


        def L2norm_Fourier(ff):
            return np.sqrt(
                    np.sum(ff[:, :, 0 ] * np.conj(ff[:, :, 0 ]))
                + 2*np.sum(ff[:, :, 1:] * np.conj(ff[:, :, 1:])))

        # now compare numpy with fftw
        np_field1_real = np.fft.irfftn(field1_complex, axes = (0, 1, 2)).transpose(1, 0, 2, 3)
        L2normr    = np.sqrt(np.mean(np.sum(   field1_real**2, axis = 3)))
        np_L2normr = np.sqrt(np.mean(np.sum(np_field1_real**2, axis = 3)))
        err = np.max(np.abs(field1_real - np_field1_real*npoints)) / L2normr
        print('found maximum difference {0} between field1_real and np_field1_real'.format(err))
        assert(err < 1e-5)

        np_field1_complex = np.fft.rfftn(field1_real.transpose(1, 0, 2, 3), axes = (0, 1, 2)) / npoints

        L2norm0 = L2norm_Fourier(field1_complex)
        L2norm1 = L2norm_Fourier(np_field1_complex)
        err = np.max(np.abs(np_field1_complex - field1_complex)) / L2norm0
        assert(err < 1e-5)

        err = abs(L2normr - L2norm0) / L2norm0
        assert(err < 1e-5)

        if ((not type(plt) == type(None)) and (opt.plot_on)):
            f = plt.figure()
            a = f.add_subplot(221)
            a.imshow(np.log(np.abs(np_field1_complex[:, :, 0, 0])), interpolation = 'nearest')
            a.set_title('numpy')
            a = f.add_subplot(222)
            a.imshow(np.log(np.abs(field1_complex[:, :, 0, 0])), interpolation = 'nearest')
            a.set_title('TurTLE')
            if not type(cmocean)  == type(None):
                a = f.add_subplot(223)
                a.imshow(
                        np.log(np.angle(np_field1_complex[:, :, 0, 0])),
                        interpolation = 'nearest',
                        cmap  = cmocean.cm.phase)
                a = f.add_subplot(224)
                a.imshow(
                        np.log(np.angle(field1_complex[:, :, 0, 0])),
                        interpolation = 'nearest',
                        cmap  = cmocean.cm.phase)
            f.tight_layout()
            f.savefig(c.simname + '_complex_slice_kx0.pdf')
            plt.close(f)

        # hard test.
        # generate random rseed2_field
        # field0 is rseed2_field through TurTLE symmetrize
        # field1 is rseed2_field through ift+dft+normalize
        # compare field0 with field1
        field0_complex = df['rseed2_field0/complex/0'][...]
        field1_complex = df['rseed2_field1/complex/0'][...]
        f0 = field0_complex[:, :, 0, :]
        f1 = field1_complex[:, :, 0, :]
        diff = np.abs(f0 - f1)
        ii = np.unravel_index(np.argmax(diff), diff.shape)
        print('found maximum difference {0} between rseed2_field0 and rseed2_field1 at {1}'.format(diff[ii[0], ii[1], ii[2]], ii))
        assert(np.max(np.abs(diff)) < 1e-5)
    return None

if __name__ == '__main__':
    main()

