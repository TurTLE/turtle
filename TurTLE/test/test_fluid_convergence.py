#! /usr/bin/env python
#######################################################################
#                                                                     #
#  Copyright 2021 Max Planck Institute                                #
#                 for Dynamics and Self-Organization                  #
#                                                                     #
#  This file is part of TurTLE.                                       #
#                                                                     #
#  TurTLE is free software: you can redistribute it and/or modify     #
#  it under the terms of the GNU General Public License as published  #
#  by the Free Software Foundation, either version 3 of the License,  #
#  or (at your option) any later version.                             #
#                                                                     #
#  TurTLE is distributed in the hope that it will be useful,          #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of     #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      #
#  GNU General Public License for more details.                       #
#                                                                     #
#  You should have received a copy of the GNU General Public License  #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     #
#                                                                     #
# Contact: Cristian.Lalescu@mpcdf.mpg.de                              #
#                                                                     #
#######################################################################



import os
import numpy as np
import h5py
import sys
import argparse

import TurTLE
from TurTLE import DNS, PP


try:
    import matplotlib.pyplot as plt
except:
    plt = None

def main_NSE():
    niterations = 32
    c0 = DNS()
    c0.launch(
            ['NSE',
             '-n', '64',
             '--simname', 'nse0',
             '--np', '16',
             '--ntpp', '1',
             '--fftw_plan_rigor', 'FFTW_PATIENT',
             '--niter_todo', '{0}'.format(niterations),
             '--niter_out', '{0}'.format(niterations//16),
             '--niter_stat', '1',
             '--checkpoints_per_file', '{0}'.format(3),
             '--wd', './'])
    c0.compute_statistics()
    c0.plot_basic_stats()
    #cc = PP()
    #cc.launch(
    #        ['get_velocity',
    #         '--simname', 'nsve_for_long_comparison',
    #         '--np', '16',
    #         '--ntpp', '1',
    #         '--iter0', '0',
    #         '--iter1', '{0}'.format(niterations)])
    #c1 = DNS()
    #c1.launch(
    #        ['NSE',
    #         '-n', '64',
    #         '--src-simname', 'nsve_for_long_comparison',
    #         '--src-wd', './',
    #         '--src-iteration', '0',
    #         '--simname', 'nse_for_long_comparison',
    #         '--np', '16',
    #         '--ntpp', '1',
    #         '--fftw_plan_rigor', 'FFTW_PATIENT',
    #         '--niter_todo', '{0}'.format(niterations),
    #         '--niter_out', '{0}'.format(niterations),
    #         '--niter_stat', '1',
    #         '--forcing_type', 'linear',
    #         '--checkpoints_per_file', '{0}'.format(3),
    #         '--njobs', '{0}'.format(njobs),
    #         '--wd', './'])
    #def get_err(a, b):
    #    return np.abs(a - b) / (0.5 * (a + b))
    #if not type(plt) == type(None):
    #    c0 = DNS(simname = 'nsve_for_long_comparison')
    #    c1 = DNS(simname = 'nse_for_long_comparison')
    #    for cc in [c0, c1]:
    #        cc.compute_statistics()
    #        cc.plot_basic_stats()
    #    ##########
    #    fig = plt.figure()
    #    a = fig.add_subplot(211)
    #    a.plot(c0.statistics['energy(t)'],
    #            label = 'NSVE')
    #    a.plot(c1.statistics['energy(t)'],
    #            label = 'NSE')
    #    a.legend(loc = 'best')
    #    a = fig.add_subplot(212)
    #    a.plot(c0.statistics['kshell'],
    #           c0.statistics['energy(k)'],
    #           label = 'NSVE')
    #    a.plot(c1.statistics['kshell'],
    #           c1.statistics['energy(k)'],
    #           label = 'NSE')
    #    a.set_xscale('log')
    #    a.set_yscale('log')
    #    a.set_ylim(1e-7, 10)
    #    fig.tight_layout()
    #    fig.savefig('comparison_energy_long.pdf')
    #    plt.close(fig)
    #    ##########
    #    fig = plt.figure()
    #    a = fig.add_subplot(211)
    #    a.plot(c0.statistics['vel_max(t)'],
    #            label = 'NSVE')
    #    a.plot(c1.statistics['vel_max(t)'],
    #            label = 'NSE')
    #    a.legend(loc = 'best')
    #    a = fig.add_subplot(212)
    #    err = get_err(
    #            c0.statistics['vel_max(t)'],
    #            c1.statistics['vel_max(t)'])
    #    a.plot(c0.statistics['t'] / c0.statistics['tauK'],
    #           err,
    #           label = 'velocity')
    #    vort0_max = c0.get_data_file()['statistics/moments/vorticity'][:, 9, 3]
    #    vort1_max = c1.get_data_file()['statistics/moments/vorticity'][:, 9, 3]
    #    a.plot(c0.statistics['t'] / c0.statistics['tauK'],
    #           get_err(vort0_max, vort1_max),
    #           label = 'vorticity')
    #    a.legend(loc = 'best')
    #    a.set_yscale('log')
    #    a.set_ylim(bottom = 1e-6)
    #    fig.tight_layout()
    #    fig.savefig('comparison_velmax_long.pdf')
    #    plt.close(fig)
    return None

if __name__ == '__main__':
    parser = argparse.ArgumentParser('')
    parser.add_argument('--NSE', dest = 'NSE', action = 'store_true')
    opt = parser.parse_args()
    if opt.NSE:
        main_NSE()
    else:
        pass

