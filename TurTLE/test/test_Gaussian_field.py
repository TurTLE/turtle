#! /usr/bin/env python

import numpy as np
from scipy import trapz
from scipy.stats import norm
from scipy.integrate import quad
from scipy.optimize import root
import h5py
import sys, os
import time

import TurTLE
from TurTLE import TEST

try:
    import matplotlib.pyplot as plt
except:
    plt = None

def main():
    # size of grid
    n = 256
    # from a 1024^3 simulation
    dissipation = 0.37127850066981344
    Lint = 0.9029294339535114 # probably the wrong (inconsistent with Pope) definition of Lint
    etaK = 0.003730966576882254

    c_L, c_eta = calculate_constants(epsilon=dissipation, Lint=Lint, etaK=etaK)

    bin_no = 100
    rseed = int(time.time())
    simname = 'Gaussianity_test'

    if not os.path.exists(simname+'.h5'):
        c = TEST()
        opt = c.launch(
            ['Gauss_field_test',
             '--nx', str(n),
             '--ny', str(n),
             '--nz', str(n),
             '--simname', simname,
             '--np', '4',
             '--environment', 'short',
             '--minutes', '60',
             '--ntpp', '1',
             '--wd', './',
             '--histogram_bins', str(bin_no),
             '--max_velocity_estimate', '8.',
             '--spectrum_dissipation', str(dissipation),
             '--spectrum_Lint', str(Lint),
             '--spectrum_etaK', str(etaK),
             '--spectrum_large_scale_const', str(c_L),
             '--spectrum_small_scale_const', str(c_eta),
             '--field_random_seed', str(rseed)] +
             sys.argv[1:])
    plot_stuff(simname)
    return None

def main_premultiplied_spectra_test():
    n_arr = [64, 128, 256, 512]
    #n_arr = [64]
    run_no = 3
    dk = 1.
    diss = 0.007036634455033392
    energy = 0.0417604575121591
    etaK = 0.2157937040813811
    Lint = energy**(3./2.)/diss

    c_L, c_eta = calculate_constants(epsilon=diss, Lint=Lint, etaK=etaK)

    bin_no = 100

    i = 1
    if not os.path.exists('conv_test_n64_run0.h5'):
        for n in n_arr:
            for run in range(run_no):
                simname = 'conv_test_n{0}_run{1}'.format(n, run)

                rseed = i
                i += 1
                c = TEST()
                opt = c.launch(
                    ['Gauss_field_test',
                     '--nx', str(n),
                     '--ny', str(n),
                     '--nz', str(n),
                     '--dkx', str(dk),
                     '--dky', str(dk),
                     '--dkz', str(dk),
                     '--simname', simname,
                     '--np', '4',
                     '--environment', 'short',
                     '--minutes', '60',
                     '--ntpp', '1',
                     '--wd', './',
                     '--histogram_bins', str(bin_no),
                     '--max_velocity_estimate', '8.',
                     '--spectrum_dissipation', str(diss),
                     '--spectrum_Lint', str(Lint),
                     '--spectrum_etaK', str(etaK),
                     '--spectrum_large_scale_const', str(c_L),
                     '--spectrum_small_scale_const', str(c_eta),
                     '--field_random_seed', str(rseed)] +
                     sys.argv[1:])
    else:
        print('Energy = {}'.format(energy))

        Q = quad(lambda k : k**2*E(k, epsilon=diss, etaK=etaK, Lint=Lint, c_L=c_L, c_eta=c_eta),
                0., np.inf)[0]/15.
        print('Q = {}'.format(Q))        
        P = quad(lambda k : k**4*E(k, epsilon=diss, etaK=etaK, Lint=Lint, c_L=c_L, c_eta=c_eta),
                0., np.inf)[0]/105.
        print('P = {}'.format(P))        

        for n in n_arr:
            for run in range(run_no):
                simname = 'conv_test_n{0}_run{1}'.format(n, run)
                df = h5py.File(simname + '.h5', 'r')

                kk = df['kspace/kshell'][...]

                phi_ij = df['statistics/spectra/velocity_velocity'][0]
                energy_spec = (phi_ij[..., 0, 0] + phi_ij[..., 1, 1] + phi_ij[..., 2, 2])/2
                k2spec_trace = (
                        df['statistics/spectra/k*velocity_k*velocity'][..., 0, 0]
                        + df['statistics/spectra/k*velocity_k*velocity'][..., 1, 1]
                        + df['statistics/spectra/k*velocity_k*velocity'][..., 2, 2])/2
                k4spec_trace = (
                        df['statistics/spectra/k2*velocity_k2*velocity'][..., 0, 0]
                        + df['statistics/spectra/k2*velocity_k2*velocity'][..., 1, 1]
                        + df['statistics/spectra/k2*velocity_k2*velocity'][..., 2, 2])/2
                print('----------------------')
                print('n = {}, run = {}'.format(n, run))
                print('Energy = {}'.format(np.sum(energy_spec)))
                print('Q = {}'.format(np.sum(k2spec_trace)/15.))
                print('P = {}'.format(np.sum(k4spec_trace)/105.))

            

def main_deltak_test():
    n_arr = [64, 128, 256, 512]
    dk_arr = [1., 0.5, 0.25, 0.125]
    diss = 0.007036634455033392
    energy = 0.0417604575121591
    etaK = 0.2157937040813811
    Lint = energy**(3./2.)/diss

    c_L, c_eta = calculate_constants(epsilon=diss, Lint=Lint, etaK=etaK)

    bin_no = 100

    if not os.path.exists('conv_test_n64_run0.h5'):
        for n, dk in zip(n_arr, dk_arr):
            for run in range(3):
                simname = 'conv_test_n{0}_run{1}'.format(n, run)

                rseed = int(time.time())
                c = TEST()
                opt = c.launch(
                    ['Gauss_field_test',
                     '--nx', str(n),
                     '--ny', str(n),
                     '--nz', str(n),
                     '--dkx', str(dk),
                     '--dky', str(dk),
                     '--dkz', str(dk),
                     '--simname', simname,
                     '--np', '4',
                     '--environment', 'short',
                     '--minutes', '60',
                     '--ntpp', '1',
                     '--wd', './',
                     '--histogram_bins', str(bin_no),
                     '--max_velocity_estimate', '8.',
                     '--spectrum_dissipation', str(diss),
                     '--spectrum_Lint', str(Lint),
                     '--spectrum_etaK', str(etaK),
                     '--spectrum_large_scale_const', str(c_L),
                     '--spectrum_small_scale_const', str(c_eta),
                     '--field_random_seed', str(rseed)] +
                     sys.argv[1:])
    else:
        plt.figure()
        k = np.geomspace(1e-1, 4e1, 1000)
        plt.plot(k, E(k, epsilon=diss, Lint=Lint, etaK=etaK, c_L=c_L, c_eta=c_eta))
        for n in n_arr:
            run = 0
            simname = 'conv_test_n{0}_run{1}'.format(n, run)
            df = h5py.File(simname + '.h5', 'r')

            kk = df['kspace/kshell'][...]
            phi_ij = df['statistics/spectra/velocity_velocity'][0] / df['kspace/dk'][()]
            energy_spec = (phi_ij[..., 0, 0] + phi_ij[..., 1, 1] + phi_ij[..., 2, 2])/2
            plt.scatter(kk[1:-2], energy_spec[1:-2], label='n={0}, r={1}'.format(n, run))
        plt.xscale('log')
        plt.yscale('log')
        plt.legend()
        plt.savefig('spectra_convergence.pdf')
                 

def calculate_constants(*, epsilon, Lint, etaK):
    sol = root(optimize_func,
            x0 = (6.78, 0.40),
            args = (epsilon, Lint, etaK),
            method='hybr',
            options={'eps':0.001, 'factor':0.1})
    assert sol.success
    return sol.x

def optimize_func(constants, diss, Lint, etaK):
    energy = (Lint*diss)**(2./3.)
    energy_model = quad(
        lambda k : E(k, epsilon=diss, Lint=Lint, etaK=etaK, c_L=constants[0], c_eta=constants[1]),
        0, np.inf)[0]
    nu = (etaK**4*diss)**(1./3.)
    diss_model = quad(
        lambda k : 2*nu*k**2*E(k, epsilon=diss, Lint=Lint, etaK=etaK, c_L=constants[0], c_eta=constants[1]),
        0, np.inf)[0]
    return (energy_model - energy, diss_model - diss)

def E(k, *, epsilon, C=1.5, Lint, etaK, c_L=6.78, c_eta=0.4, beta=5.2):
    return C*epsilon**(2./3.)*k**(-5./3.)*f_L(k*Lint, c_L=c_L)*f_eta(k*etaK, beta=beta, c_eta=c_eta)

def f_L(x, *, c_L):
    return (x/(x**2 + c_L)**(1./2.))**(11./3.)

def f_eta(x, *, beta, c_eta):
    return np.exp(-beta*((x**4 + c_eta**4)**(1./4.) - c_eta))

def plot_stuff(simname):
    df = h5py.File(simname + '.h5', 'r')
    for kk in ['spectrum_dissipation',
               'spectrum_Lint',
               'spectrum_etaK',
               'spectrum_large_scale_const',
               'spectrum_small_scale_const',
               'field_random_seed',
               'histogram_bins']:
        print(kk, df['parameters/' + kk][...])
    dissipation = df['parameters/spectrum_dissipation'][()]
    Lint = df['parameters/spectrum_Lint'][()]
    etaK = df['parameters/spectrum_etaK'][()]
    c_L = df['parameters/spectrum_large_scale_const'][()]
    c_eta = df['parameters/spectrum_small_scale_const'][()]
    bin_no = df['parameters/histogram_bins'][()]

    f = plt.figure()
    # test spectrum
    a = f.add_subplot(121)
    kk = df['kspace/kshell'][...]
    print('dk: {}'.format(df['kspace/dk'][()]))
    phi_ij = df['statistics/spectra/velocity_velocity'][0] / df['kspace/dk'][()]
    energy = (phi_ij[..., 0, 0] + phi_ij[..., 1, 1] + phi_ij[..., 2, 2])/2

    a.scatter(kk[1:-2], energy[1:-2])
    a.plot(kk[1:-2],
            E(kk[1:-2],
                epsilon=dissipation, Lint=Lint, etaK=etaK, c_eta=c_eta, c_L=c_L),
            ls='--', c='C0')
    a.set_xscale('log')
    a.set_yscale('log')
    # test isotropy
    a = f.add_subplot(122)

    max_vel_estimate = df['parameters/max_velocity_estimate'][()]
    velbinsize = 2*max_vel_estimate/bin_no
    vel = np.linspace(-max_vel_estimate+velbinsize/2, max_vel_estimate-velbinsize/2, bin_no)
    hist_vel = df['statistics/histograms/velocity'][0, :, :3]
    f_vel = hist_vel / np.sum(hist_vel, axis=0, keepdims=True).astype(float) / velbinsize

    print(np.sum(energy*np.arange(len(energy))**2))
    print('Energy sum: {}'.format(np.sum(energy*df['kspace/dk'][()])))
    print('Moment sum: {}'.format(df['statistics/moments/velocity'][0,2,3]/2))
    print('Velocity variances: {}'.format(trapz(vel[:,None]**2*f_vel, vel[:,None], axis=0)))

    vel_variance = df['statistics/moments/velocity'][0,2,3]/3.
    a.plot(vel[:,None]/np.sqrt(vel_variance), f_vel*np.sqrt(vel_variance))
    a.plot(vel/np.sqrt(vel_variance), norm.pdf(vel/np.sqrt(vel_variance)), ls='--')
    a.set_yscale('log')
    a.set_xlim(-5,5)
    a.set_ylim(1e-5,1)
    f.tight_layout()
    f.savefig('spectrum_isotropy_test.pdf')
    plt.close(f)

    ### check divergence
    print('Divergence second moment is: {0}'.format(
            df['statistics/moments/velocity_divergence'][0, 2]))
    print('Gradient second moment is: {0}'.format(
            df['statistics/moments/velocity_gradient'][0, 2].mean()))

    print('----------- k2-premultiplied spectrum -----------')
    #k2func = lambda k, k_c=k_cutoff, s=slope : k**(2+s)*np.exp(-k/k_c)
    #k2sum_analytic = quad(k2func, 0, k_cutoff*20)[0]
    #print('Analytically: {}'.format(k2sum_analytic))
    k2spec_trace = (
            df['statistics/spectra/k*velocity_k*velocity'][..., 0, 0]
            + df['statistics/spectra/k*velocity_k*velocity'][..., 1, 1]
            + df['statistics/spectra/k*velocity_k*velocity'][..., 2, 2])
    #print('Energy sum: {}'.format(np.sum(k2spec_trace*df['kspace/dk'][()])/2./coeff))

    df.close()
    return None

if __name__ == '__main__':
    main_premultiplied_spectra_test()

