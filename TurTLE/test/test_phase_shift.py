#! /usr/bin/env python

import os
import sys
import numpy as np
import h5py

import TurTLE
from TurTLE import TEST

try:
    import matplotlib.pyplot as plt
    import cmocean
except:
    plt = None

def main():
    simname = 'test'
    if not os.path.exists(simname + '.h5'):
        c = TEST()
        opt = c.launch(
            ['phase_shift_test',
             '--simname', str(simname),
             '--random_phase_seed', str(1)] +
             sys.argv[1:])
    if type(plt) != type(None):
        df = h5py.File(simname + '_phase_field.h5', 'r')
        phi0 = df['random_phase_field/complex/0'][()]
        phi1 = df['random_phase_field/complex/1'][()]
        div = df['divergence/real/1'][()]
        df.close()
        print('max divergence is', np.max(np.abs(div)))
        f = plt.figure(figsize = (6, 6))
        a = f.add_subplot(111)
        XX = 0
        #a.imshow(, interpolation = 'none')
        diff = np.abs(phi0 - phi1)
        ii = np.unravel_index(np.argmax(diff), diff.shape)
        print(phi0[15, 12, 16])
        print(phi1[15, 12, 16])
        print(np.max(diff), ii, diff[ii], phi0[ii], phi1[ii])
        a.imshow(np.abs(phi0[:, :, XX]),
                interpolation = 'none',
                cmap = cmocean.cm.balance,
                vmin = -1,
                vmax = 1)
        f.tight_layout()
        f.savefig(simname + '_symmetry_error.pdf')
    return None

if __name__ == '__main__':
    main()

