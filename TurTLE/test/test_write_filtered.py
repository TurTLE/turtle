#! /usr/bin/env python

import numpy as np
import h5py
import sys

import TurTLE
from TurTLE import TEST

try:
    import matplotlib.pyplot as plt
except:
    plt = None

def main():
    c = TEST()
    c.launch(
            ['write_filtered_test',
             '--simname', 'write_filtered_test',
             '--np', '4',
             '--ntpp', '1',
             '--wd', './'] +
             sys.argv[1:])
    df = h5py.File('data_for_write_filtered.h5', 'r')
    bla0 = df['scal_field_full/complex/0'][:, 0]
    bla1 = df['scal_field_z0slice/complex/0'][...]
    max_dist = np.max(np.abs(bla1 - bla0))
    print('maximum distance for scalar is ', max_dist)
    assert(max_dist < 1e-5)
    bla0 = df['vec_field_full/complex/0'][:, 0]
    bla1 = df['vec_field_z0slice/complex/0'][...]
    max_dist = np.max(np.abs(bla1 - bla0))
    print('maximum distance for vector is ', max_dist)
    assert(max_dist < 1e-5)
    bla0 = df['tens_field_full/complex/0'][:, 0]
    bla1 = df['tens_field_z0slice/complex/0'][...]
    max_dist = np.max(np.abs(bla1 - bla0))
    print('maximum distance for tensor is ', max_dist)
    assert(max_dist < 1e-5)
    print('SUCCESS!!! z=0 slice agrees between different datasets')
    return None

if __name__ == '__main__':
    main()

