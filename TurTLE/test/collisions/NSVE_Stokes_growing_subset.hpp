/**********************************************************************
*                                                                     *
*  Copyright 2022 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#ifndef NSVE_STOKES_GROWING_SUBSET_HPP
#define NSVE_STOKES_GROWING_SUBSET_HPP



#include "base.hpp"
#include "vorticity_equation.hpp"
#include "full_code/NSVE.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_output_hdf5.hpp"
#include "particles/particles_sampling.hpp"
#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/particle_solver.hpp"
//#include "stokes_collision_counter_rhs.hpp"

#include <cstdlib>

/** \brief Navier-Stokes solver that includes simple Lagrangian tracers.
 *
 *  Child of Navier Stokes vorticity equation solver, this class calls all the
 *  methods from `NSVE`, and in addition integrates simple Lagrangian tracers
 *  in the resulting velocity field.
 */

template <typename rnumber>
class NSVE_Stokes_growing_subset : public NSVE<rnumber>
{
    public:

        /* parameters that are read in read_parameters */
        int niter_part;
        int niter_part_fine_period;
        int niter_part_fine_duration;
        int nparticles;
        int tracers0_integration_steps;
        int tracers0_neighbours;
        int tracers0_smoothness;
        double tracers0_cutoff;

	/* specific parameters */
        int sample_pressure;
        int sample_pressure_gradient;
        int sample_pressure_Hessian;
        int sample_velocity_gradient;
        long long int nb_growing_particles;


        /* other stuff */
        field_tinterpolator<rnumber, FFTW, THREE, NONE> *fti;
        stokes_collisions_with_background_rhs<rnumber, FFTW, NONE> *srhs;
        particle_set<7, 3, 2> *pset;
        particle_solver *psolver;


        particles_output_hdf5<long long int, double, 7> *particles_output_writer_mpi;
        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;

        NSVE_Stokes_growing_subset(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        ~NSVE_Stokes_growing_subset(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//NSVE_STOKES_GROWING_SUBSET_HPP

