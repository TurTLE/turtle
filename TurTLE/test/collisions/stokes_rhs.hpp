/******************************************************************************
*                                                                             *
*  Copyright 2022 Max Planck Institute for Dynamics and Self-Organization     *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef STOKES_RHS_HPP
#define STOKES_RHS_HPP

#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/abstract_particle_rhs.hpp"



template <typename rnumber,
          field_backend be,
          temporal_interpolation_type tt>
class stokes_rhs: public abstract_particle_rhs
{
    protected:
        field_tinterpolator<rnumber, be, THREE, tt> *velocity;
        particle_rnumber drag_coefficient;
        int state_size;
    public:
        stokes_rhs(){}
        ~stokes_rhs(){}
        int getStateSize() const
        {
             // 3 numbers for position
             // // 9 numbers for components of deformation tensor                                                                                                                                                        // 3 numbers to store logarithmic factors
             return state_size;
        }

        int setVelocity(field_tinterpolator<rnumber, be, THREE, tt> *in_velocity)
        {
            this->velocity = in_velocity;
            return EXIT_SUCCESS;
        }

        int operator()(
                double t,
                abstract_particle_set &pset,
                particle_rnumber *result,
                std::vector<std::unique_ptr<abstract_particle_set::particle_rnumber[]>> &additional_data)
        {
            TIMEZONE("stokes_rhs::operator()");
            auto *sampled_velocity = new particle_rnumber[pset.getLocalNumberOfParticles()*3];
            // interpolation adds on top of existing values, so result must be cleared.
            std::fill_n(sampled_velocity, pset.getLocalNumberOfParticles()*3, 0);
            (*(this->velocity))(t, pset, sampled_velocity);
            for (long long int idx_part = 0; idx_part < pset.getLocalNumberOfParticles(); idx_part++)
            {
                if(pset.getStateSize()==6)
                    {
                            result[idx_part*pset.getStateSize() + IDXC_X] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X];
                            result[idx_part*pset.getStateSize() + IDXC_Y] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y];
                            result[idx_part*pset.getStateSize() + IDXC_Z] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z];
                            result[idx_part*pset.getStateSize() + 3 + IDXC_X] = - this->drag_coefficient
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_X]);
                            result[idx_part*pset.getStateSize() + 3 + IDXC_Y] = - this->drag_coefficient
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_Y]);
                            result[idx_part*pset.getStateSize() + 3 + IDXC_Z] = - this->drag_coefficient
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_Z]);
                    }


                if(pset.getStateSize()==7)
                    {
			    result[idx_part*pset.getStateSize() + IDXC_X] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X];
                            result[idx_part*pset.getStateSize() + IDXC_Y] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y];
                            result[idx_part*pset.getStateSize() + IDXC_Z] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z];
                            result[idx_part*pset.getStateSize() + 3 + IDXC_X] = - pset.getParticleState()[idx_part*pset.getStateSize() +6]
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_X]);
                            result[idx_part*pset.getStateSize() + 3 + IDXC_Y] = - pset.getParticleState()[idx_part*pset.getStateSize() +6]
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_Y]);
                            result[idx_part*pset.getStateSize() + 3 + IDXC_Z] = - pset.getParticleState()[idx_part*pset.getStateSize() +6]
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_Z]);
                            result[idx_part*pset.getStateSize() + 6] = 0; 
		    }

            }
            return EXIT_SUCCESS;
        }

        int imposeModelConstraints(abstract_particle_set &pset) const
        {
            return EXIT_SUCCESS;
        }

        void setDragCoefficient(particle_rnumber in_drag_coefficient)
        {
            this->drag_coefficient = in_drag_coefficient;    
        }

        void setStateSize(int in_state_size)
        {
            this->state_size = in_state_size;
        }

        particle_rnumber getDragCoefficient()
        {
            return this->drag_coefficient;
        }
};

#endif//STOKES_RHS_HPP

