/******************************************************************************
*                                                                             *
*  Copyright 2022 Max Planck Institute for Dynamics and Self-Organization     *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/
#ifndef P2P_STOKES_COLLISIONS_GROWING_SUBSET_HPP
#define P2P_STOKES_COLLISIONS_GROWING_SUBSET_HPP

#include "particles/p2p/p2p_ghost_collisions.hpp"

#include <cstring>
#include <cmath>       /* for sqrt, abs */

#include <set>
#include <utility>
#include <vector>

// This class computes ghost collisions for Stokes particles where the 7th degree of freedom is the drag coeffcient
template <class real_number, class partsize_t>
class p2p_stokes_collisions_growing_subset: public p2p_ghost_collisions<real_number, partsize_t>
{
    private:
        // Following doubles are needed for the collision computation
        double dt;
        double dt_inverse;
        double nu;
        double density_ratio;
        int order;
        const double pi2 = atan(double(1))*double(8);
        const double pi = atan(double(1))*double(4);
        long long int nb_growing_particles;
public:
    p2p_stokes_collisions_growing_subset() : dt(0.01), nu(0.1), density_ratio(0.001), order(1)
    {
        TIMEZONE("p2p_stokes_collisions_growing_subset::p2p_stokes_collisions_growing_subset");
        dt_inverse = 1/dt;
        this->set_sphere();
        DEBUG_MSG(" nu: %f\n", nu);
    }

    // Copy constructor use a counter set to zero
    p2p_stokes_collisions_growing_subset(const p2p_stokes_collisions_growing_subset& p2p_sc)
    {
        TIMEZONE("p2p_stokes_collisions_growing_subset::p2p_stokes_collisions copy constructor");
        DEBUG_MSG("copy constructor is called\n");
        dt = p2p_sc.dt;
        dt_inverse = p2p_sc.dt_inverse;
        nu = p2p_sc.nu;
        density_ratio = p2p_sc.density_ratio;
        order = p2p_sc.order;
    }

    template <int size_particle_positions, int size_particle_rhs>
    void compute_interaction(const partsize_t idx_part1,
                             real_number pos_part1[],
                             real_number rhs_part1[],
                             const partsize_t idx_part2,
                             real_number pos_part2[],
                             real_number rhs_part2[],
                             const real_number rr,
                             const real_number /*cutoff*/,
                             const real_number x,
                             const real_number y,
                             const real_number z){
        static_assert(size_particle_positions == 7, "This kernel works only with 7 values for one particle's position+orientation");
        static_assert(size_particle_rhs == 7, "This kernel works only with 7 values per particle's rhs");
        if( pos_part1[6]<2 or pos_part2[6]<2){
            DEBUG_MSG(" drag1: %f\n", pos_part1[6]);
            DEBUG_MSG(" drag2: %f\n", pos_part2[6]);
        }
        double t_min, r_min2;

        const double v_x = pos_part2[3] - pos_part1[3];
        const double v_y = pos_part2[4] - pos_part1[4];
        const double v_z = pos_part2[5] - pos_part1[5];

        const double rv = x*v_x + y*v_y + z*v_z;
        const double vv = v_x*v_x + v_y*v_y + v_z*v_z;
        const double a1 = sqrt((double(9)/double(2))*density_ratio*nu/pos_part1[6]);
        const double a2 = sqrt((double(9)/double(2))*density_ratio*nu/pos_part2[6]);
        const double r_col = a1 + a2;
        if(rr>r_col*r_col)
        {
            if(vv!=0)
            {
                t_min = rv/vv;
            }
            else
            {
                t_min = 0;
            }
            assert(pos_part1[6] > 0);
            if(t_min <= 0)
            {
                r_min2 = rr;
		DEBUG_MSG("r_min2 test1 %f", r_min2);
            }
            else if(t_min > dt)
            {
                const double xtmp2 = x - v_x*dt;
                const double ytmp2 = y - v_y*dt;
                const double ztmp2 = z - v_z*dt;
                r_min2 = xtmp2*xtmp2 + ytmp2*ytmp2 + ztmp2*ztmp2;
		DEBUG_MSG("r_min2 test2 %f", r_min2);

            }
            else
            {
                r_min2 = rr - rv*rv/vv;
		DEBUG_MSG("r_min2 test3 %f", r_min2);
            }
            if(r_min2 < r_col*r_col)
            {
                DEBUG_MSG("nu: %f collision radius: %f drag1: %f\n",nu,r_col,pos_part1[6]);
                DEBUG_MSG("Collision happens, %d %d\n", idx_part1, idx_part2);
                this->add_colliding_pair(idx_part1, idx_part2);
                DEBUG_MSG("local_number of collision pairs: %d\n", this->collision_pairs_local.size());
                assert(idx_part1!=idx_part2);
                const bool condition1 = (idx_part1 <  this->nb_growing_particles and idx_part2 >= this->nb_growing_particles);
                const bool condition2 = (idx_part1 >= this->nb_growing_particles and idx_part2 <  this->nb_growing_particles);
                if (condition1 or condition2)
                {   
                    const double tmp_m1 = pow(pos_part1[6], -double(3)/double(2)); 
                    const double tmp_m2 =  pow(pos_part2[6], -double(3)/double(2)); 
                    const double tmp = tmp_m1 + tmp_m2;
                    const double new_size = 1./std::cbrt(tmp*tmp);
                    if (condition1)
                    {
                        DEBUG_MSG("rhs started to be adapted\n");
                        rhs_part1[6] =  dt_inverse*(new_size - pos_part1[6]);
                        DEBUG_MSG("drag_coeff rhs: %f\n", rhs_part1[6]);
                        rhs_part1[3] = dt_inverse*((tmp_m1*pos_part1[3] + tmp_m2*pos_part2[3])/tmp - pos_part1[3]);
                        DEBUG_MSG(" rhs 3: %f\n", rhs_part1[3]);

                        rhs_part1[4] = dt_inverse*((tmp_m1*pos_part1[4] + tmp_m2*pos_part2[4])/tmp - pos_part1[4]);
                        rhs_part1[5] = dt_inverse*((tmp_m1*pos_part1[5] + tmp_m2*pos_part2[5])/tmp - pos_part1[5]);
                        DEBUG_MSG("rhs should be adapdted\n");
                    }
                    else
                    {
                        DEBUG_MSG("rhs started to be adapted\n");
                        rhs_part2[6] = dt_inverse*(new_size - pos_part2[6]);
                        DEBUG_MSG("drag_coeff rhs: %f\n", rhs_part2[6]);

                        rhs_part2[3] = dt_inverse*((tmp_m1*pos_part1[3] + tmp_m2*pos_part2[3])/tmp - pos_part2[3]);
                        rhs_part2[4] = dt_inverse*((tmp_m1*pos_part1[4] + tmp_m2*pos_part2[4])/tmp - pos_part2[4]);
                        rhs_part2[5] = dt_inverse*((tmp_m1*pos_part1[5] + tmp_m2*pos_part2[5])/tmp - pos_part2[5]);
                        DEBUG_MSG("rhs should be adapdted\n");
                    }
                    DEBUG_MSG("growth should have happend\n");
                }
            }
        }

    }

    void set_density_ratio(const double DENSITY_RATIO)
    {
        this->density_ratio = DENSITY_RATIO;
    }

    double get_density_ratio()
    {
        return this->density_ratio;
    }

    void set_nu(const double NU)
    {
        this->nu = NU;
    }

    void set_order(const int ORDER)
    {
        this->order = ORDER;
    }

    double get_nu()
    {
        return this->nu;
    }

    void set_dt(const double DT)
    {
        this->dt = DT;
        this->dt_inverse = 1/DT;
    }

    void set_nb_growing_particles(const long long int NB_GROWING_PARTICLES)
    {
        this->nb_growing_particles = NB_GROWING_PARTICLES;
    }

    double get_dt()
    {
    return this->dt;
    }

    int get_local_collision_nb()
    {
        return this->collision_pairs_local.size();
    }

    std::set<std::pair <partsize_t, partsize_t>> getCollisionPairsLocal() const
    {
        return this->collision_pairs_local;
    }
};


#endif // P2P_STOKES_COLLISIONS_GROWING_SUBSET_HPP

