/******************************************************************************
*                                                                             *
*  Copyright 2022 Max Planck Institute for Dynamics and Self-Organization     *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef STOKES_COLLISIONS_WITH_BACKGROUND_RHS_HPP
#define STOKES_COLLISIONS_WITH_BACKGROUND_RHS_HPP

#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/abstract_particle_rhs.hpp"
//#include "stokes_rhs.hpp"
//#include "p2p_stokes_collisions.hpp"
//#include "stokes_rhs.hpp"

template <typename rnumber,
          field_backend be,
          temporal_interpolation_type tt>
class stokes_collisions_with_background_rhs: public stokes_rhs<rnumber, be, tt>
{
    protected:
        p2p_stokes_collisions_growing_subset<abstract_particle_set::particle_rnumber, abstract_particle_set::partsize_t> p2p_sc;
    public:
        stokes_collisions_with_background_rhs(){}
        ~stokes_collisions_with_background_rhs(){}

        int operator()(
                double t,
                abstract_particle_set &pset,
                abstract_particle_set::particle_rnumber *result,
		std::vector<std::unique_ptr<abstract_particle_set::particle_rnumber[]>> &additional_data)
        {
            auto *sampled_velocity = new abstract_particle_set::particle_rnumber[pset.getLocalNumberOfParticles()*3];
            // interpolation adds on top of existing values, so result must be cleared.
            std::fill_n(sampled_velocity, pset.getLocalNumberOfParticles()*3, 0);
            (*(this->velocity))(t, pset, sampled_velocity);
            additional_data.push_back(std::unique_ptr<abstract_particle_set::particle_rnumber[]>(
                    new abstract_particle_set::particle_rnumber[pset.getLocalNumberOfParticles()*pset.getStateSize()]));


            switch (pset.getStateSize())
            {
                case 6:
                    for (long long int idx_part = 0; idx_part < pset.getLocalNumberOfParticles(); idx_part++)
                    {
                        result[idx_part*pset.getStateSize() + IDXC_X] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X];
                        result[idx_part*pset.getStateSize() + IDXC_Y] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y];
                        result[idx_part*pset.getStateSize() + IDXC_Z] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z];
                        result[idx_part*pset.getStateSize() + 3 + IDXC_X] = - this->drag_coefficient
                                                                                *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X]
                                                                                - sampled_velocity[idx_part*3 + IDXC_X]);
                        result[idx_part*pset.getStateSize() + 3 + IDXC_Y] = - this->drag_coefficient
                                                                                *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y]
                                                                                - sampled_velocity[idx_part*3 + IDXC_Y]);
                        result[idx_part*pset.getStateSize() + 3 + IDXC_Z] = - this->drag_coefficient
                                                                                *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z]
                                                                                - sampled_velocity[idx_part*3 + IDXC_Z]);
                    }
                    break;
                case 7:
                    for (long long int idx_part = 0; idx_part < pset.getLocalNumberOfParticles(); idx_part++)
                    {
                            result[idx_part*pset.getStateSize() + IDXC_X] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X];
                            result[idx_part*pset.getStateSize() + IDXC_Y] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y];
                            result[idx_part*pset.getStateSize() + IDXC_Z] = pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z];
                            result[idx_part*pset.getStateSize() + 3 + IDXC_X] = - pset.getParticleState()[idx_part*pset.getStateSize() +6]
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_X]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_X]);
                            result[idx_part*pset.getStateSize() + 3 + IDXC_Y] = - pset.getParticleState()[idx_part*pset.getStateSize() +6]
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Y]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_Y]);
                            result[idx_part*pset.getStateSize() + 3 + IDXC_Z] = - pset.getParticleState()[idx_part*pset.getStateSize() +6]
                                                                                    *(pset.getParticleState()[idx_part*pset.getStateSize() + 3 + IDXC_Z]
                                                                                    - sampled_velocity[idx_part*3 + IDXC_Z]);
                            result[idx_part*pset.getStateSize() + 6] = 0;
                    }
                    break;
                default:
                    break;
            }

	        // applying P2P kernel typically implies a reordering of the particle data
            // we must apply the reordering to the previously computed rhs values
            // create array where to store rhs values
            // copy rhs values to temporary array
            pset.copy_state_tofrom(
                    additional_data[additional_data.size()-1].get(),
                    result);

            // clear list of colliding particles
            this->p2p_sc.reset_collision_pairs();
            // update list of colliding particles
            pset.template applyP2PKernel<7, p2p_stokes_collisions_growing_subset<abstract_particle_set::particle_rnumber, abstract_particle_set::partsize_t>>(
                    this->p2p_sc,
                    additional_data);
            // copy shuffled rhs values
            pset.copy_state_tofrom(
                    result,
                    additional_data[additional_data.size()-1].get());
	    // clear temporary array
            additional_data.pop_back();
            delete[] sampled_velocity; 
	    return EXIT_SUCCESS;
        }

        p2p_stokes_collisions_growing_subset<abstract_particle_set::particle_rnumber, abstract_particle_set::partsize_t> &getCollisionCounter()
        {
            return this->p2p_sc;
        }

};

#endif//STOKES_COLLISIONS_WITH_BACKGROUND_RHS_HPP

