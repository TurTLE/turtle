import os
import numpy as np
import h5py

import TurTLE
from TurTLE import TEST

cpp_location = os.path.join(TurTLE.data_dir, 'particle_set')

class aTEST(TEST):
    def write_src(
            self):
        self.version_message = (
                '/***********************************************************************\n' +
                '* this code automatically generated by TurTLE\n' +
                '* version {0}\n'.format(TurTLE.__version__) +
                '***********************************************************************/\n\n\n')
        self.include_list = [
                '"full_code/main_code.hpp"',
                '"full_code/NSVEparticles.hpp"']
        self.main = """
            int main(int argc, char *argv[])
            {{
                bool fpe = (
                    (getenv("BFPS_FPE_OFF") == nullptr) ||
                    (getenv("BFPS_FPE_OFF") != std::string("TRUE")));
                return main_code< {0} >(argc, argv, fpe);
            }}
            """.format(self.dns_type + '<{0}>'.format(self.C_field_dtype))
        self.includes = '\n'.join(
                ['#include ' + hh
                 for hh in self.include_list])
        self.definitions = open(
            os.path.join(
                cpp_location, self.dns_type + '.hpp'), 'r').read()
        self.definitions += open(
            os.path.join(
                cpp_location, self.dns_type + '.cpp'), 'r').read()
        with open(self.name + '.cpp', 'w') as outfile:
            outfile.write(self.version_message + '\n\n')
            outfile.write(self.includes + '\n\n')
            outfile.write(self.definitions + '\n\n')
            outfile.write(self.main + '\n')
        self.check_current_vorticity_exists = False
        self.check_current_velocity_exists = False
        return None
    def launch(
            self,
            args = [],
            **kwargs):
        self.simname = 'test_pset_init_functionality'
        self.dns_type = 'test_particle_set_init'
        self.C_field_dtype = 'double'
        self.fluid_precision = 'double'
        self.name = 'test_particle_set_init_' + self.fluid_precision
        if not os.path.exists(self.get_data_file_name()):
            self.write_par()
        self.prepare_particle_file()
        self.run(1, 1)
        return None
    def prepare_particle_file(self):
        with h5py.File(self.simname + '_particles.h5', 'w') as ofile:
            for species in ['tracers0', 'tracers1']:
                ofile.create_group(species)
                ofile[species].create_group('label')
                ofile[species].create_group('state')
        return None


def main():
    c = aTEST()
    c.launch()
    df = h5py.File(c.simname + '_particles.h5', 'r')
    ll0 = df['tracers0/label/0'][()]
    ll1 = df['tracers1/label/0'][()]
    assert((ll0 == ll1).all())
    xx0 = df['tracers0/state/0'][()]
    xx1 = df['tracers1/state/0'][()]
    assert((xx0 == xx1).all())
    df.close()
    print('SUCCESS, particle data was copied correctly')
    return None

if __name__ == '__main__':
    main()

