import os
import sys
import re

import numpy as np
import h5py
import matplotlib.pyplot as plt

plt.rc('text', usetex=True)
plt.rc('text.latex', preamble=('\\usepackage{amsmath}\n'
                             + '\\usepackage{amsfonts}\n'))

import TurTLE
from TurTLE.tools import get_fornberg_coeffs
import TurTLE.DNS

cpp_location = os.path.join(
        TurTLE.data_dir, 'particle_set')

def fix_fontsizes(a):
    a.tick_params(axis = 'both',
            which = 'both',
            labelsize = 6)
    return None

def scalar_error(vv1, vv2):
    """
        relative:
            difference / max(abs(variable1), abs(variable2))
        absolute:
            abs(difference) / sqrt(L2norm(variable1)*L2norm(variable2))
    """
    assert(vv1.shape == vv2.shape)
    diff = np.abs(vv1 - vv2).flatten()
    unit = (np.mean(vv1.flatten()**2) * np.mean(vv2.flatten()**2))**0.25
    err_absolute = diff / unit
    denominator = np.maximum(np.abs(vv1), np.abs(vv2)).flatten()
    err_relative = (diff / denominator)
    assert (not np.any(np.isnan(err_absolute)))
    assert (not np.any(np.isnan(err_relative)))
    return {'scalar_relative' : err_relative,
            'scalar_absolute' : err_absolute}

def vector_error(vv1, vv2):
    """
        three values:
           * abs(abs(v1) - abs(v2)) / sqrt(abs(v1)*abs(v2))
           * abs(v1 - v2) / sqrt(abs(v1)*abs(v2))
           * acos((v1.v2) / abs(v1)*abs(v2))
    """
    assert(vv1.shape[1] == 3 and vv2.shape[1] == 3)
    abs1 = np.sqrt(np.sum(vv1**2, axis = 1))
    abs2 = np.sqrt(np.sum(vv2**2, axis = 1))
    err1 = np.abs(abs1 - abs2) / np.sqrt(abs1*abs2)
    err2 = np.sqrt(np.sum((vv1 - vv2)**2, axis = 1)) / np.sqrt(abs1*abs2)
    cosine = np.sum(vv1*vv2, axis = 1) / (abs1*abs2)
    assert((cosine.max() - 1) < 1e-14)
    cosine[np.where(cosine > 1)] = 1.
    err3 = np.arccos(cosine)
    return {'vector_abs'   : err1,
            'vector_diff'  : err2,
            'vector_angle' : err3}

def plot_np_histogram(ax, ff, bins = 30, **kwargs):
    hh, bins = np.histogram(ff, bins = bins, density = True)
    ax.plot(0.5*(bins[1:] + bins[:-1]), hh, **kwargs)
    return None

def plot_error_PDFs(
        axes_dict,
        vv1,
        vv2,
        bins = 30,
        **kwargs):
    err_dict = scalar_error(vv1, vv2)
    err_dict.update(vector_error(vv1, vv2))
    for suffix in ['scalar_relative',
                   'scalar_absolute',
                   'vector_abs',
                   'vector_diff',
                   'vector_angle']:
        plot_np_histogram(
            axes_dict[suffix],
            err_dict[suffix],
            bins = bins,
            **kwargs)
    return None

def read_diff_data(
        info,
        species_index = -1,
        neighbours = 3,
        particle_format = 'default',
        iteration = None,
        total_iterations = None):
    if type(total_iterations) == type(None):
        sim_file = h5py.File(
                os.path.join(info['work_dir'], info['simname'] + '.h5'),
                'r')
        total_iterations = sim_file['iteration'][()]
        sim_file.close()
    if type(iteration) == type(None):
        iteration = total_iterations//2
    sname = info['particles/species_list'][species_index]
    particle_file = h5py.File(
            os.path.join(info['work_dir'], info['simname'] + '_particles.h5'),
            'r')
    if 'niter_part' not in info['parameters'].keys():
        info['parameters']['niter_part'] = 1
    if particle_format == 'default':
        x = np.array([particle_file[sname + '/position/{0}'.format(ii)][()]
                      for ii in range(iteration-neighbours*info['parameters']['niter_part'],
                                      iteration+neighbours*info['parameters']['niter_part']+1,
                                      info['parameters']['niter_part'])])
        u = np.array([particle_file[sname + '/velocity/{0}'.format(ii)][()]
                      for ii in range(iteration-neighbours*info['parameters']['niter_part'],
                                      iteration+neighbours*info['parameters']['niter_part']+1,
                                      info['parameters']['niter_part'])])
        uu = particle_file[sname + '/velocity/{0}'.format(iteration)][()]
        aa = particle_file[sname + '/acceleration/{0}'.format(iteration)][()]
    else:
        if 'position' in particle_file[sname].keys():
            x = particle_file[sname + '/position'][
                iteration-neighbours:iteration+neighbours+1]
        else:
            x = particle_file[sname + '/state'][
                iteration-neighbours:iteration+neighbours+1]
        u = particle_file[sname + '/velocity'][
                iteration-neighbours:iteration+neighbours+1]
        uu = particle_file[sname + '/velocity'][iteration]
        aa = particle_file[sname + '/acceleration'][iteration]
        if len(x.shape) > 3:
            assert(x.shape[-1] == 3)
            x = x.reshape(x.shape[0], -1, 3)
            u = u.reshape(u.shape[0], -1, 3)
            uu = uu.reshape(-1, 3)
            aa = aa.reshape(-1, 3)
    particle_file.close()

    fornberg_coefficients = get_fornberg_coeffs(
            0, [ii*info['parameters']['dt']*info['parameters']['niter_part']
                for ii in range(-neighbours, neighbours+1)])
    d1p = 0.0
    d1u = 0.0
    d2p = 0.0
    for ii in range(-neighbours, neighbours+1):
        d1p += x[neighbours+ii]*fornberg_coefficients[1][ii+neighbours]
        d2p += x[neighbours+ii]*fornberg_coefficients[2][ii+neighbours]
        d1u += u[neighbours+ii]*fornberg_coefficients[1][ii+neighbours]
    result = {'a' : aa,
              'u' : uu,
              'd1p' : d1p,
              'd2p' : d2p,
              'd1u' : d1u,
              'species_index' : species_index,
              'species_name'  : sname,
              'diff_neighbours' : neighbours}
    interp_information = re.search('_n([0-9]+)_m(\d)_', sname)
    if type(interp_information) != type(None):
        result['neighbours'] = int(interp_information.group(1))
        result['smoothness'] = int(interp_information.group(2))
    return result

def relative_vector_difference(vv1, vv2, reference = None):
    assert(vv1.shape[1] == 3 and vv2.shape[1] == 3)
    diff = np.sum((vv1 - vv2)**2, axis = 1)
    if type(reference) == type(None):
        reference = np.maximum(
                np.sum(vv1**2, axis = 1),
                np.sum(vv2**2, axis = 1))
    return np.sqrt( diff / reference)

def relative_vector_dot(vv1, vv2, reference = None):
    assert(vv1.shape[1] == 3 and vv2.shape[1] == 3)
    dot = np.sum(vv1*vv2, axis = 1)
    if type(reference) == type(None):
        reference = (np.sum(vv1**2, axis = 1)
                   * np.sum(vv2**2, axis = 1))**0.5
    return (dot / reference)

def plot_diff_stat_check(
        info,
        fname = None,
        species_index = -1,
        neighbours = 3,
        particle_format = 'default',
        iteration = None,
        total_iterations = None):
    if type(total_iterations) == type(None):
        sim_file = h5py.File(
                os.path.join(info['work_dir'], info['simname'] + '.h5'),
                'r')
        total_iterations = sim_file['iteration'][()]
        sim_file.close()
    if type(iteration) == type(None):
        iteration = total_iterations//2
    sname = info['particles/species_list'][species_index]
    particle_file = h5py.File(
            os.path.join(info['work_dir'], info['simname'] + '_particles.h5'),
            'r')
    if 'niter_part' not in info['parameters'].keys():
        info['parameters']['niter_part'] = 1
    if particle_format == 'default':
        x = np.array([particle_file[sname + '/position/{0}'.format(ii)][()]
                      for ii in range(iteration-neighbours*info['parameters']['niter_part'],
                                      iteration+neighbours*info['parameters']['niter_part']+1,
                                      info['parameters']['niter_part'])])
        u = np.array([particle_file[sname + '/velocity/{0}'.format(ii)][()]
                      for ii in range(iteration-neighbours*info['parameters']['niter_part'],
                                      iteration+neighbours*info['parameters']['niter_part']+1,
                                      info['parameters']['niter_part'])])
        uu = particle_file[sname + '/velocity/{0}'.format(iteration)][()]
        aa = particle_file[sname + '/acceleration/{0}'.format(iteration)][()]
    else:
        if 'position' in particle_file[sname].keys():
            x = particle_file[sname + '/position'][
                iteration-neighbours:iteration+neighbours+1]
        else:
            x = particle_file[sname + '/state'][
                iteration-neighbours:iteration+neighbours+1]
        u = particle_file[sname + '/velocity'][
                iteration-neighbours:iteration+neighbours+1]
        uu = particle_file[sname + '/velocity'][iteration]
        aa = particle_file[sname + '/acceleration'][iteration]
        if len(x.shape) > 3:
            assert(x.shape[-1] == 3)
            x = x.reshape(x.shape[0], -1, 3)
            u = u.reshape(u.shape[0], -1, 3)
            uu = uu.reshape(-1, 3)
            aa = aa.reshape(-1, 3)
    particle_file.close()

    fornberg_coefficients = get_fornberg_coeffs(
            0, [ii*info['parameters']['dt']*info['parameters']['niter_part']
                for ii in range(-neighbours, neighbours+1)])
    d1p = 0.0
    d1u = 0.0
    d2p = 0.0
    for ii in range(-neighbours, neighbours+1):
        d1p += x[neighbours+ii]*fornberg_coefficients[1][ii+neighbours]
        d2p += x[neighbours+ii]*fornberg_coefficients[2][ii+neighbours]
        d1u += u[neighbours+ii]*fornberg_coefficients[1][ii+neighbours]
    f = plt.figure(figsize = (6, 6))
    a = f.add_subplot(311)
    plot_np_histogram(
            a,
            relative_vector_difference(d1p, uu),
            label = 'd1p vs velocity')
    plot_np_histogram(
            a,
            relative_vector_difference(d2p, d1u),
            label = 'd2p vs d1u')
    plot_np_histogram(
            a,
            relative_vector_difference(d2p, aa),
            label = 'd2p vs acceleration')
    plot_np_histogram(
            a,
            relative_vector_difference(d1u, aa),
            label = 'd1u vs acceleration',
            dashes = (2, 2))
    a.set_title(
            'histograms of relative difference {0} TurTLE v{1}'.format(
                sname,
                info['code_version']),
            fontsize = 6)
    a.legend(loc = 'best', fontsize = 6)
    a.set_yscale('log')
    a.set_xscale('log')
    fix_fontsizes(a)
    a = f.add_subplot(312)
    plot_np_histogram(
            a,
            relative_vector_dot(d1p, uu),
            label = 'd1p vs velocity')
    plot_np_histogram(
            a,
            relative_vector_dot(d2p, d1u),
            label = 'd2p vs d1u')
    plot_np_histogram(
            a,
            relative_vector_dot(d2p, aa),
            label = 'd2p vs acceleration')
    plot_np_histogram(
            a,
            relative_vector_dot(d1u, aa),
            label = 'd1u vs acceleration',
            dashes = (2, 2))
    a.set_title(
            'histograms of relative dot {0} TurTLE v{1}'.format(
                sname,
                info['code_version']),
            fontsize = 6)
    a.legend(loc = 'best', fontsize = 6)
    a.set_yscale('log')
    a.set_xscale('log')
    fix_fontsizes(a)
    a = f.add_subplot(313)
    plot_np_histogram(
            a,
            d2p.flatten(),
            bins = 127,
            label = 'd2p')
    plot_np_histogram(
            a,
            d1u.flatten(),
            bins = 127,
            label = 'd1u',
            dashes = (2, 2))
    plot_np_histogram(
            a,
            aa.flatten(),
            bins = 127,
            label = 'a',
            dashes = (2, 3))
    a.legend(loc = 'best', fontsize = 6)
    a.set_yscale('log')
    fix_fontsizes(a)
    f.tight_layout(pad = 0)
    if type(fname) == type(None):
        fname = 'diff_stats_' + info['simname']
    f.savefig(fname + '.pdf')
    plt.close(f)
    return None

class ADNS(TurTLE.DNS):
    def __init__(
            self,
            **kwargs):
        TurTLE.DNS.__init__(self, **kwargs)
        self.list_of_particle_codes.append('NSVE_tracers_test')
        self.extra_parameters['NSVE_tracers_test'] = {}
        return None
    def write_src(
            self):
        self.version_message = (
                '/***********************************************************************\n' +
                '* this code automatically generated by TurTLE\n' +
                '* version {0}\n'.format(TurTLE.__version__) +
                '***********************************************************************/\n\n\n')
        self.include_list = [
                '"base.hpp"',
                '"scope_timer.hpp"',
                '"fftw_interface.hpp"',
                '"full_code/main_code.hpp"',
                '"full_code/NSVEparticles.hpp"',
                '<cmath>',
                '<iostream>',
                '<hdf5.h>',
                '<string>',
                '<cstring>',
                '<fftw3-mpi.h>',
                '<omp.h>',
                '<cfenv>',
                '<cstdlib>']
        self.main = """
            int main(int argc, char *argv[])
            {{
                bool fpe = (
                    (getenv("BFPS_FPE_OFF") == nullptr) ||
                    (getenv("BFPS_FPE_OFF") != std::string("TRUE")));
                return main_code< {0} >(argc, argv, fpe);
            }}
            """.format(self.dns_type + '<{0}>'.format(self.C_field_dtype))
        self.includes = '\n'.join(
                ['#include ' + hh
                 for hh in self.include_list])
        self.name = 'NSVE_tracers_test'
        self.dns_type = 'NSVE_tracers_test'
        self.definitions = open(
            os.path.join(
                cpp_location, 'NSVE_tracers_test.hpp'), 'r').read()
        self.definitions += open(
            os.path.join(
                cpp_location, 'NSVE_tracers_test.cpp'), 'r').read()
        with open(self.name + '.cpp', 'w') as outfile:
            outfile.write(self.version_message + '\n\n')
            outfile.write(self.includes + '\n\n')
            outfile.write(self.definitions + '\n\n')
            outfile.write(self.main + '\n')
        self.check_current_vorticity_exists = True
        return None

    def write_par(
            self,
            iter0 = 0):
        TurTLE.DNS.write_par(self, iter0 = iter0)
        with h5py.File(self.get_data_file_name(), 'r+') as ofile:
            #TODO move collision stats to particle stats
            for i in range(self.parameters['niter_out']):
                ofile.create_dataset('statistics/collisions/'+str(i),
                                     shape=(1,),
                                     dtype = np.int64)
        return None

    def launch(
            self,
            args = [],
            **kwargs):
        opt = self.prepare_launch(args = args)
        if not os.path.exists(self.get_data_file_name()):
            self.write_par()
            self.generate_initial_condition(opt = opt)
            with h5py.File(self.get_particle_file_name(), 'w') as particle_file:
                for (n, m) in [
                        (0, 0),
                        (1, 0), (2, 0), (3, 0),
                        (1, 1), (2, 1), (3, 1),
                        (1, 2), (2, 2), (3, 2),
                        (1, 3), (2, 3)]:
                    particle_file.create_group('tracers_n{0}_m{1}/position'.format(n, m))
                    particle_file.create_group('tracers_n{0}_m{1}/velocity'.format(n, m))
                    particle_file.create_group('tracers_n{0}_m{1}/acceleration'.format(n, m))
            self.generate_tracer_state(integration_steps = 0,
                                       rseed = opt.particle_rand_seed,
                                       ncomponents = 3)
        self.run(
                nb_processes = opt.nb_processes,
                nb_threads_per_process = opt.nb_threads_per_process,
                njobs = opt.njobs,
                hours = opt.minutes // 60,
                minutes = opt.minutes % 60,
                no_submit = opt.no_submit,
                no_debug = not opt.debug)
        return None

def main():
    bla = ADNS()
    if not os.path.exists('test.h5'):
        bla.launch([
            'NSVE_tracers_test',
            '--np', '1',
            '--niter_todo', '20',
            '--niter_stat', '10',
            '--niter_out', '20',
            '--nparticles', '1000',
            '--kMeta', '3.0',
            ] + sys.argv[1:])
    else:
        bla.read_parameters()
    df = bla.get_particle_file()
    f = plt.figure(figsize=(6,6))
    ax1 = f.add_subplot(421)
    ax2 = f.add_subplot(423)
    ax3 = f.add_subplot(425)
    ax4 = f.add_subplot(427)
    ax5 = f.add_subplot(422)
    ax6 = f.add_subplot(424)
    ax7 = f.add_subplot(426)
    ax8 = f.add_subplot(428)
    for species in df.keys():
        iterations = range(bla.parameters['niter_out']//2, bla.parameters['niter_out'])
        dashes = (None, None)
        if ('m1' in species):
            dashes = (1, 1)
        elif ('m2' in species):
            dashes = (2, 2)
        elif ('m3' in species):
            dashes = (3, 3)
        pos = np.array(
                [df[species + '/position/{0}'.format(ii)][()]
                 for ii in iterations])
        vel = np.array(
                [df[species + '/velocity/{0}'.format(ii)][()]
                 for ii in iterations])
        acc = np.array(
                [df[species + '/acceleration/{0}'.format(ii)][()]
                 for ii in iterations])
        d1x = (pos[2:] - pos[:-2]) / (2*bla.parameters['dt'])
        d2x = (pos[2:] - 2*pos[1:-1] + pos[:-2]) / (bla.parameters['dt']**2)
        d1v = (vel[2:] - vel[:-2]) / (2*bla.parameters['dt'])
        vv = vel[1:-1]
        aa = acc[1:-1]
        errv = np.abs(np.sum((vv - d1x)**2, axis = 2) / np.sum(vv**2, axis = 2))**0.5
        erra1 = np.abs(np.sum((aa - d1v)**2, axis = 2) / np.sum(aa**2, axis = 2))**0.5
        erra2 = np.abs(np.sum((aa - d2x)**2, axis = 2) / np.sum(aa**2, axis = 2))**0.5
        erra3 = np.abs(np.sum((d1v - d2x)**2, axis = 2) / np.sum(aa**2, axis = 2))**0.5
        plot_np_histogram(ax1,
                errv.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
        plot_np_histogram(ax2,
                erra1.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
        plot_np_histogram(ax3,
                erra2.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
        plot_np_histogram(ax4,
                erra3.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
        errv = relative_vector_difference(vv.reshape(-1, 3), d1x.reshape(-1, 3))
        erra1 = relative_vector_difference(aa.reshape(-1, 3), d1v.reshape(-1, 3))
        erra2 = relative_vector_difference(aa.reshape(-1, 3), d2x.reshape(-1, 3))
        erra3 = relative_vector_difference(d1v.reshape(-1, 3), d2x.reshape(-1, 3))
        plot_np_histogram(ax5,
                errv.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
        plot_np_histogram(ax6,
                erra1.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
        plot_np_histogram(ax7,
                erra2.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
        plot_np_histogram(ax8,
                erra3.flatten(),
                bins = 101,
                label = species,
                dashes = dashes)
    for aa in [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8]:
        aa.set_xscale('log')
        aa.set_yscale('log')
        fix_fontsizes(aa)
        aa.set_xlim(1e-5, 2)
    ax1.legend(loc = 'best', fontsize = 5, ncols = 2)
    ax1.set_ylabel('vel vs d1x', fontsize = 6)
    ax2.set_ylabel('acc vs d1v', fontsize = 6)
    ax3.set_ylabel('acc vs d2x', fontsize = 6)
    ax4.set_ylabel('d1v vs d2x', fontsize = 6)
    ax5.set_ylabel('vel vs d1x', fontsize = 6)
    ax6.set_ylabel('acc vs d1v', fontsize = 6)
    ax7.set_ylabel('acc vs d2x', fontsize = 6)
    ax8.set_ylabel('d1v vs d2x', fontsize = 6)
    f.tight_layout(pad = 0)
    f.savefig('tmp.pdf')
    plt.close(f)
    return None

def main2():
    info = {'work_dir' : './',
            'simname'  : 'test'}
    info['particles/species_list'] = sorted(h5py.File('test_particles.h5', 'r').keys())
    info['parameters'] = {}
    ff = h5py.File('test.h5', 'r')
    for key in ff['parameters'].keys():
        info['parameters'][key] = ff['parameters/' + key][()]
    info['code_version'] = TurTLE.__version__

    dd = read_diff_data(
        info,
        species_index = -1,
        neighbours = 3)

    f = plt.figure(figsize = (6, 6))
    a_scal_rel = f.add_subplot(511)
    a_scal_abs = f.add_subplot(512)
    a_vec_abs = f.add_subplot(513)
    a_vec_diff = f.add_subplot(514)
    a_vec_ang = f.add_subplot(515)
    axes_dictionary = {
        'scalar_relative' : a_scal_rel,
        'scalar_absolute' : a_scal_abs,
        'vector_abs'      : a_vec_abs,
        'vector_diff'     : a_vec_diff,
        'vector_angle'    : a_vec_ang}
    plot_error_PDFs(
            axes_dictionary,
            dd['u'], dd['d1p'],
            label = 'vel vs d1x')
    plot_error_PDFs(
            axes_dictionary,
            dd['a'], dd['d1u'],
            label = 'acc vs d1u')
    plot_error_PDFs(
            axes_dictionary,
            dd['a'], dd['d2p'],
            label = 'acc vs d2x')
    plot_error_PDFs(
            axes_dictionary,
            dd['d1u'], dd['d2p'],
            label = 'd1u vs d2x')
    for aa in [a_scal_rel, a_scal_abs, a_vec_abs, a_vec_diff, a_vec_ang]:
        fix_fontsizes(aa)
        aa.set_yscale('log')
    for aa in [a_scal_rel, a_scal_abs, a_vec_abs, a_vec_diff]:
        aa.set_xscale('log')
    a_scal_rel.legend(loc = 'best')
    a_scal_rel.set_title('component $|q_1 - q_2| / max(|q_1|, |q_2|)$',
                         fontsize = 6)
    a_scal_abs.set_title('component $|q_1 - q_2| / \sqrt{rms_1 \cdot rms_2}$',
                         fontsize = 6)
    a_vec_abs.set_title('vector abs error',
                         fontsize = 6)
    a_vec_diff.set_title('vector diff error',
                         fontsize = 6)
    a_vec_ang.set_title('vector angle error',
                         fontsize = 6)
    f.tight_layout(pad = 0)
    fname = 'diff2_stats_' + info['simname']
    f.savefig(fname + '.pdf')
    plt.close(f)
    return None

if __name__ == '__main__':
    main()
    main2()

