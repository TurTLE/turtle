import numpy as np
import h5py
import matplotlib.pyplot as plt

from TurTLE import TEST

def main():
    c = TEST()
    c.launch([
        'test_interpolation_methods'])

    data_file = h5py.File(c.simname + '_particles.h5', 'r')


    f = plt.figure(figsize = (9, 9))

    xx = data_file['particle/position/0'][()]

    counter = 1
    for n, m in [(0, 0),
                 (1, 1),
                 (2, 1),
                 (2, 2),
                 (3, 1),
                 (3, 2),
                 (4, 1),
                 (4, 2),
                 (5, 2)]:
        phi = data_file['particle/phi_{0}{1}/0'.format(n, m)][()]
        a = f.add_subplot(330 + counter)
        a.contour(xx[..., 0], xx[..., 2], phi[..., 0], levels = 50)
        a.set_title('{0}{1}'.format(n, m))
        counter += 1

    f.tight_layout()
    f.savefig('test_interpolation_methods.pdf')
    plt.close(f)

    f = plt.figure(figsize = (6, 6))
    a = f.add_subplot(111)
    err_mean_1 = []
    err_max_1 = []
    n = 1
    phi0 = data_file['particle/phi_{0}{1}/0'.format(n  , 1)][()]
    phi1 = data_file['particle/phi_{0}{1}/0'.format(n+1, 1)][()]
    err = np.abs(phi0 - phi1)
    err_mean_1.append(err.mean())
    err_max_1.append(err.max())
    err_mean_2 = []
    err_max_2 = []
    for n in [2, 3, 4]:
        phi0 = data_file['particle/phi_{0}{1}/0'.format(n  , 1)][()]
        phi1 = data_file['particle/phi_{0}{1}/0'.format(n+1, 1)][()]
        err = np.abs(phi0 - phi1)
        err_mean_1.append(err.mean())
        err_max_1.append(err.max())
        phi0 = data_file['particle/phi_{0}{1}/0'.format(n  , 2)][()]
        phi1 = data_file['particle/phi_{0}{1}/0'.format(n+1, 2)][()]
        err = np.abs(phi0 - phi1)
        err_mean_2.append(err.mean())
        err_max_2.append(err.max())
    neighbour_list = np.array([1, 2, 3, 4]).astype(float)
    a.plot(neighbour_list, err_mean_1, marker = '.', label = 'm=1, mean')
    a.plot(neighbour_list, err_max_1,  marker = '.', label = 'm=1, max')
    a.plot(neighbour_list, 1e-4*neighbour_list**(-4),  color = 'black', dashes = (2, 2))
    neighbour_list = np.array([2, 3, 4]).astype(float)
    a.plot(neighbour_list, err_mean_2, marker = '.', label = 'm=2, mean')
    a.plot(neighbour_list, err_max_2,  marker = '.', label = 'm=2, max')

    a.set_xscale('log')
    a.set_yscale('log')
    a.legend(loc = 'best')

    f.tight_layout()
    f.savefig('test_interpolation_methods_err.pdf')
    plt.close(f)

    data_file.close()
    return None

if __name__ == '__main__':
    main()

