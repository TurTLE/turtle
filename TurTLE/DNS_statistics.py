################################################################################
#                                                                              #
#  Copyright 2024 TurTLE team                                                  #
#                                                                              #
#  This file is part of TurTLE.                                                #
#                                                                              #
#  TurTLE is free software: you can redistribute it and/or modify              #
#  it under the terms of the GNU General Public License as published           #
#  by the Free Software Foundation, either version 3 of the License,           #
#  or (at your option) any later version.                                      #
#                                                                              #
#  TurTLE is distributed in the hope that it will be useful,                   #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>              #
#                                                                              #
# Contact: Cristian.Lalescu@mpcdf.mpg.de                                       #
#                                                                              #
################################################################################



import os
import h5py
import numpy as np

def compute_statistics(
        self,
        iter0 = 0,
        iter1 = None,
        strict_Parseval_check = True):
    """Run basic postprocessing on raw data.
    The energy spectrum :math:`E(t, k)` and the enstrophy spectrum
    :math:`\\frac{1}{2}\omega^2(t, k)` are computed from the

    .. math::

        \sum_{k \\leq \\|\\mathbf{k}\\| \\leq k+dk}\\hat{u_i} \\hat{u_j}^*, \\hskip .5cm
        \sum_{k \\leq \\|\\mathbf{k}\\| \\leq k+dk}\\hat{\omega_i} \\hat{\\omega_j}^*

    tensors, and the enstrophy spectrum is also used to
    compute the dissipation :math:`\\varepsilon(t)`.
    These basic quantities are stored in a newly created HDF5 file,
    ``simname_cache.h5``.
    """
    if len(list(self.statistics.keys())) > 0:
        return None
    if not os.path.exists(self.get_data_file_name()):
        if os.path.exists(self.get_cache_file_name()):
            self.read_parameters(fname = self.get_cache_file_name())
            pp_file = self.get_cache_file()
            for k in ['t',
                      'energy(t)',
                      'energy(k)',
                      'enstrophy(t)',
                      'enstrophy(k)',
                      'R_ij(t)',
                      'vel_max(t)',
                      'renergy(t)',
                      'renstrophy(t)']:
                if k in pp_file.keys():
                    self.statistics[k] = pp_file[k][...]
            self.statistics['kM'] = pp_file['kspace/kM'][...]
            self.statistics['dk'] = pp_file['kspace/dk'][...]
            self.statistics['kshell'] = pp_file['kspace/kshell'][...]
            self.statistics['nshell'] = pp_file['kspace/nshell'][...]
        else:
            return None
    else:
        self.read_parameters()
        with self.get_data_file() as data_file:
            if 'moments' not in data_file['statistics'].keys():
                return None
            iter0 = min(((data_file['statistics/moments/velocity'].shape[0]-1) *
                         self.parameters['niter_stat']),
                        iter0)
            if type(iter1) == type(None):
                iter1 = data_file['iteration'][...]
            else:
                iter1 = min(data_file['iteration'][...], iter1)
            ii0 = iter0 // self.parameters['niter_stat']
            ii1 = iter1 // self.parameters['niter_stat']
            self.statistics['kshell'] = data_file['kspace/kshell'][...]
            self.statistics['nshell'] = data_file['kspace/nshell'][...]
            for kk in [-1, -2]:
                if (self.statistics['kshell'][kk] == 0):
                    self.statistics['kshell'][kk] = np.nan
            self.statistics['kM'] = data_file['kspace/kM'][...]
            self.statistics['dk'] = data_file['kspace/dk'][...]
            computation_needed = True
            pp_file = h5py.File(self.get_postprocess_file_name(), 'a')
            if not ('parameters' in pp_file.keys()):
                data_file.copy('parameters', pp_file)
                data_file.copy('kspace', pp_file)
            if 'ii0' in pp_file.keys():
                computation_needed =  not (ii0 == pp_file['ii0'][...] and
                                           ii1 == pp_file['ii1'][...])
                if computation_needed:
                    for k in ['t', 'vel_max(t)',
                              'renergy(t)',
                              'renstrophy(t)',
                              'energy(t)', 'enstrophy(t)',
                              'energy(k)', 'enstrophy(k)',
                              'energy(t, k)',
                              'enstrophy(t, k)',
                              'R_ij(t)',
                              'ii0', 'ii1', 'iter0', 'iter1']:
                        if k in pp_file.keys():
                            del pp_file[k]
            if computation_needed:
                #TODO figure out whether normalization is sane or not
                pp_file['iter0'] = iter0
                pp_file['iter1'] = iter1
                pp_file['ii0'] = ii0
                pp_file['ii1'] = ii1
                pp_file['t'] = (self.parameters['dt']*
                                self.parameters['niter_stat']*
                                (np.arange(ii0, ii1+1).astype(np.float64)))
                # we have an extra division by shell_width because of the Dirac delta restricting integration to the shell
                phi_ij = data_file['statistics/spectra/velocity_velocity'][ii0:ii1+1] / self.statistics['dk']
                pp_file['R_ij(t)'] = np.sum(phi_ij*self.statistics['dk'], axis = 1)
                energy_tk = (
                    phi_ij[:, :, 0, 0] +
                    phi_ij[:, :, 1, 1] +
                    phi_ij[:, :, 2, 2])/2
                pp_file['energy(t)'] = np.sum(energy_tk*self.statistics['dk'], axis = 1)
                # normalization factor is (4 pi * shell_width * kshell^2) / (nmodes in shell * dkx*dky*dkz)
                norm_factor = (4*np.pi*self.statistics['dk']*self.statistics['kshell']**2) / (self.parameters['dkx']*self.parameters['dky']*self.parameters['dkz']*self.statistics['nshell'])
                pp_file['energy(k)'] = np.mean(energy_tk, axis = 0)*norm_factor
                phi_vorticity_ij = data_file['statistics/spectra/vorticity_vorticity'][ii0:ii1+1] / self.statistics['dk']
                enstrophy_tk = (
                    phi_vorticity_ij[:, :, 0, 0] +
                    phi_vorticity_ij[:, :, 1, 1] +
                    phi_vorticity_ij[:, :, 2, 2])/2
                pp_file['enstrophy(t)'] = np.sum(enstrophy_tk*self.statistics['dk'], axis = 1)
                pp_file['enstrophy(k)'] = np.mean(enstrophy_tk, axis = 0)*norm_factor
                pp_file['vel_max(t)'] = data_file['statistics/moments/velocity'][ii0:ii1+1, 9, 3]
                pp_file['renergy(t)'] = data_file['statistics/moments/velocity'][ii0:ii1+1, 2, 3]/2
                pp_file['renstrophy(t)'] = data_file['statistics/moments/vorticity'][ii0:ii1+1, 2, 3]/2
    for k in ['t',
              'energy(t)',
              'energy(k)',
              'enstrophy(t)',
              'enstrophy(k)',
              'R_ij(t)',
              'vel_max(t)',
              'renergy(t)',
              'renstrophy(t)',
              'iter0',
              'iter1']:
        if k in pp_file.keys():
            self.statistics[k] = pp_file[k][...]
    # sanity check --- Parseval theorem check
    energy_error = np.max(np.abs(
            self.statistics['renergy(t)'] -
            self.statistics['energy(t)']) / self.statistics['energy(t)'])
    test_energy = (energy_error < 1e-5)
    enstrophy_error = np.max(np.abs(
            self.statistics['renstrophy(t)'] -
            self.statistics['enstrophy(t)']) / self.statistics['enstrophy(t)'])
    test_enstrophy = (enstrophy_error < 1e-5)
    if strict_Parseval_check:
        assert(test_energy)
        assert(test_enstrophy)
    else:
        if not test_energy:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('warning: Parseval theorem failed for velocity')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        if not test_energy:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('warning: Parseval theorem failed for vorticity')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    self.compute_time_averages()
    return None

def compute_Reynolds_stress_invariants(
        self):
    """
    see Choi and Lumley, JFM v436 p59 (2001)
    """
    Rij = self.statistics['R_ij(t)']
    Rij /= (2*self.statistics['energy(t)'][:, None, None])
    Rij[:, 0, 0] -= 1./3
    Rij[:, 1, 1] -= 1./3
    Rij[:, 2, 2] -= 1./3
    self.statistics['I2(t)'] = np.sqrt(np.einsum('...ij,...ij', Rij, Rij, optimize = True) / 6)
    self.statistics['I3(t)'] = np.cbrt(np.einsum('...ij,...jk,...ki', Rij, Rij, Rij, optimize = True) / 6)
    return None

def compute_time_averages(self):
    """Compute easy stats.

    Further computation of statistics based on the contents of
    ``simname_cache.h5``.
    Standard quantities are as follows
    (consistent with [ishihara2007jfm]_):

    .. math::

        U_{\\textrm{int}}(t) = \\sqrt{\\frac{2E(t)}{3}}, \\hskip .5cm
        L_{\\textrm{int}} = \\frac{\pi}{2U_{int}^2} \\int \\frac{dk}{k} E(k), \\hskip .5cm
        T_{\\textrm{int}} =
        \\frac{L_{\\textrm{int}}}{U_{\\textrm{int}}}

        \\eta_K = \\left(\\frac{\\nu^3}{\\varepsilon}\\right)^{1/4}, \\hskip .5cm
        \\tau_K = \\left(\\frac{\\nu}{\\varepsilon}\\right)^{1/2}, \\hskip .5cm
        \\lambda = \\sqrt{\\frac{15 \\nu U_{\\textrm{int}}^2}{\\varepsilon}}

        Re = \\frac{U_{\\textrm{int}} L_{\\textrm{int}}}{\\nu}, \\hskip
        .5cm
        R_{\\lambda} = \\frac{U_{\\textrm{int}} \\lambda}{\\nu}

        D = \\frac{\\varepsilon L_{\\textrm{int}}}{U_{\\textrm{int}}^3}


    Note: the "time-averaged" integral-scale velocity is obtained from the
    time-averaged energy. This is nominally different from time-averaging
    the time-dependent integral-scale velocity, but in practice the
    differences are not very large for data we've looked at so far.
    For future reference, it seems advisable to compute energy and
    dissipation as "primary" quantities (nice because they are L2 norms),
    to which temporal averaging can be applied; other "time-averaged"
    derived quantities (Rlambda, Taylor micro-scale itself, etc) should be
    computed from the time-averaged energy and dissipation.
    This is consistent with the integral length-scale, which is computed
    from the time-averaged energy spectrum.

    Note: if using the smooth dealiasing option, the value of "kMeta" reported
    by the code has an extra factor of 0.8.
    This is because with the smooth dealiasing option the "effective" maximum
    wavenumber is roughly 0.8 * kM, and it seems like the more relevant
    number for the physics of the DNS.
    """
    for key in ['energy',
                'enstrophy',
                'mean_trS2']:
        if key + '(t)' in self.statistics.keys():
            self.statistics[key] = np.average(self.statistics[key + '(t)'], axis = 0)
    self.statistics['vel_max'] = np.max(self.statistics['vel_max(t)'])
    for suffix in ['', '(t)']:
        self.statistics['Uint'    + suffix] = np.sqrt(2*self.statistics['energy' + suffix] / 3)
        self.statistics['diss'    + suffix] = (self.parameters['nu'] *
                                               self.statistics['enstrophy' + suffix]*2)
        self.statistics['etaK'    + suffix] = (self.parameters['nu']**3 /
                                               self.statistics['diss' + suffix])**.25
        self.statistics['tauK'    + suffix] =  (self.parameters['nu'] /
                                                self.statistics['diss' + suffix])**.5
        self.statistics['lambda' + suffix] = (15 * self.parameters['nu'] *
                                              self.statistics['Uint' + suffix]**2 /
                                              self.statistics['diss' + suffix])**.5
        self.statistics['Rlambda' + suffix] = (self.statistics['Uint' + suffix] *
                                               self.statistics['lambda' + suffix] /
                                               self.parameters['nu'])
        self.statistics['kMeta' + suffix] = (self.statistics['kM'] *
                                             self.statistics['etaK' + suffix])
        if self.parameters['dealias_type'] == 1:
            self.statistics['kMeta' + suffix] *= 0.8
    self.statistics['Lint'] = ((np.pi /
                                (2*self.statistics['Uint']**2)) *
                               np.sum(self.statistics['energy(k)'][1:-2] /
                                      self.statistics['kshell'][1:-2] *
                                      self.statistics['dk']))
    self.statistics['Re'] = (self.statistics['Uint'] *
                             self.statistics['Lint'] /
                             self.parameters['nu'])
    self.statistics['Tint'] = self.statistics['Lint'] / self.statistics['Uint']
    self.statistics['D'] = self.statistics['Lint'] * self.statistics['diss'] / self.statistics['Uint']**3
    self.statistics['Taylor_microscale'] = self.statistics['lambda']
    return None

def read_moments(
        self,
        quantity = 'velocity',
        base_group = 'statistics',
        pp_file = None,
        iteration_range = None,
        return_full_history = False):
    assert(len(self.statistics.keys()) > 0)
    if type(pp_file) == type(None):
        pp_file = self.get_data_file()
    if type(iteration_range) == type(None):
        iteration_range = [self.statistics['iter0'], self.statistics['iter1']]
    mm = pp_file[base_group + '/moments/' + quantity][
            iteration_range[0]//self.parameters['niter_stat']:
            iteration_range[1]//self.parameters['niter_stat'] + 1]
    if return_full_history:
        return mm
    mean_mm = np.zeros(mm.shape[1:], dtype=mm.dtype)
    mean_mm[0] = np.amin(mm[:, 0], axis = 0)
    mean_mm[1:-1] = np.mean(mm[:, 1:-1], axis = 0)
    mean_mm[-1] = np.amax(mm[:, -1], axis = 0)
    return mean_mm

def read_histogram(
        self,
        quantity = 'velocity',
        base_group = 'statistics',
        pp_file = None,
        iteration_range = None,
        min_estimate = None,
        max_estimate = None,
        niter_stat = None,
        nbins = None,
        return_full_history = False,
        undersample_warning_on = False):
    assert(len(self.statistics.keys()) > 0)
    if type(pp_file) == type(None):
        pp_file = self.get_data_file()
    if type(iteration_range) == type(None):
        iteration_range = [self.statistics['iter0'], self.statistics['iter1']]
    if type(max_estimate) == type(None):
        if quantity[:5] == 'log2_':
            max_estimate = np.log2(self.parameters['max_' + quantity[5:] + '_estimate'])
        else:
            max_estimate = self.parameters['max_' + quantity + '_estimate']
    if type(niter_stat) == type(None):
        niter_stat = self.parameters['niter_stat']
    if type(nbins) == type(None):
        nbins = self.parameters['histogram_bins']
    hh = pp_file[base_group + '/histograms/' + quantity][
            iteration_range[0]//niter_stat:
            iteration_range[1]//niter_stat + 1]
    if (hh.shape[-1] == 4):
        hh = hh[..., :3]
        max_estimate /= 3**0.5
    if type(min_estimate) == type(None):
        if quantity[:5] == 'log2_':
            if 'min_' + quantity[5:] + '_estimate' in self.parameters:
                min_estimate = np.log2(self.parameters['min_' + quantity[5:] + '_estimate'])
            else:
                min_estimate = -max_estimate # for compatibility with old data sets
        else:
            if 'min_' + quantity + '_estimate' in self.parameters:
                min_estimate = self.parameters['min_' + quantity + '_estimate']
            else:
                min_estimate = -max_estimate # for compatibility with old data sets
    ngrid = self.parameters['nx']*self.parameters['ny']*self.parameters['nz']
    if return_full_history:
        npoints_expected = ngrid
        npoints = np.sum(hh, axis = 1, keepdims=True)
    else:
        npoints_expected = ngrid*hh.shape[0]
        hh = np.sum(hh, axis = 0)
        npoints = np.sum(hh, axis = 0, keepdims=True)
    bb0 = np.linspace(min_estimate, max_estimate, nbins+1)
    bb = (bb0[1:] + bb0[:-1])*0.5
    if (undersample_warning_on):
        if not (npoints == npoints_expected).all():
            print('Warning: maximum estimates were wrong, histogram does not capture all values of ' + quantity + '.')
            print('Total number of points is {0}, histogram captures on average {1} points.'.format(
                    npoints_expected, np.mean(npoints)))
    pdf = hh / (npoints * (bb[1] - bb[0]))
    return bb, hh, pdf

def compute_statistics_error(
        self,
        **kwargs):
    mm = self.read_moments(**kwargs)
    bb, hh, pdf = self.read_histogram(**kwargs)
    alt_mm2 = np.sum(pdf*(bb[:, None]**2)*(bb[1] - bb[0]), axis = 0)
    relative_error = np.abs(mm[2, :3] - alt_mm2) / (mm[2, :3])
    assert(relative_error.max() < 0.01)
    return relative_error.max()

def set_plt_style(
        self,
        style = {'dashes' : (None, None)}):
    if 'style' in self.__dict__.keys():
        self.style.update(style)
    else:
        self.style = style
    return None

def plot_basic_stats(
        self,
        fig_format = 'pdf',
        fig_dir = None):
    """Plots basic diagnostics of DNS."""
    if type(fig_dir) == type(None):
        fig_dir = os.path.join(self.work_dir, 'figs')
    if not os.path.exists(fig_dir):
        os.makedirs(fig_dir)
    fig_suffix = '_{0}'.format(self.simname)
    try:
        import matplotlib.pyplot as plt
    except ImportError:
        print('Cannot plot basic stats, matplotlib not available')
        return None
    fig = plt.figure(figsize=(8, 8))
    speclog = fig.add_subplot(321)
    speclin = fig.add_subplot(322)
    ii0 = self.get_cache_file()['ii0'][()]
    ii1 = self.get_cache_file()['ii1'][()]
    with self.get_data_file() as data_file:
        # read spectrum at first and last iterations
        energy = [(data_file['statistics/spectra/velocity_velocity'][ii0, :, 0, 0] +
                   data_file['statistics/spectra/velocity_velocity'][ii0, :, 1, 1] +
                   data_file['statistics/spectra/velocity_velocity'][ii0, :, 2, 2])/2,
                  (data_file['statistics/spectra/velocity_velocity'][ii1, :, 0, 0] +
                   data_file['statistics/spectra/velocity_velocity'][ii1, :, 1, 1] +
                   data_file['statistics/spectra/velocity_velocity'][ii1, :, 2, 2])/2] / self.statistics['dk']
    # normalization factor is (4 pi * shell_width * kshell^2) / (nmodes in shell * dkx*dky*dkz)
    norm_factor = (4*np.pi*self.statistics['dk']*self.statistics['kshell']**2) / (self.parameters['dkx']*self.parameters['dky']*self.parameters['dkz']*self.statistics['nshell'])
    energy *= norm_factor
    for ii, energyii in zip([ii0, ii1], [energy[0], energy[1]]):
        norm_factor = (self.parameters['nu']**5 * self.statistics['diss(t)'][ii-ii0])**(-.25)
        k = self.statistics['kshell']*self.statistics['etaK(t)'][ii-ii0]
        speclog.plot(
                k[1:],
                energyii[1:]*norm_factor,
                label = 'iteration {0}'.format(ii*self.parameters['niter_stat']))
        speclin.plot(
                k[1:],
                energyii[1:]*norm_factor*k[1:]**(5./3))
    speclin.plot(
            k[1:],
            2*np.ones(k[1:].shape),
            label = '2',
            color = (0, 0, 0),
            dashes = (1, 1),
            linewidth = 1)
    speclog.plot(
            k[1:],
            2*k[1:]**(-5./3),
            label = '$2(k \\eta_K)^{-5/3}$',
            color = (0, 0, 0),
            dashes = (1, 1),
            linewidth = 1)
    speclog.set_xlabel('$k \\eta_K$')
    speclin.set_xlabel('$k \\eta_K$')
    speclin.set_ylabel('$(\\nu^5 \\varepsilon)^{-1/4} E(k) (k \\eta_K)^{5/3}$')
    speclog.set_ylabel('$(\\nu^5 \\varepsilon)^{-1/4} E(k)$')
    speclog.set_xscale('log')
    speclog.set_yscale('log')
    speclog.set_ylim(bottom = 1e-7)
    speclin.set_xscale('log')
    speclin.set_ylim(top = 3)
    for spec_ax in [speclog, speclin]:
        if self.parameters['fk0'] >= self.statistics['dk']/2:
            spec_ax.axvline(
                self.parameters['fk0']*self.statistics['etaK(t)'][-1],
                color = 'black',
                linewidth = 2,
                dashes = (2, 2))
        if self.parameters['fk1'] >= self.statistics['dk']/2:
            spec_ax.axvline(
                self.parameters['fk1']*self.statistics['etaK(t)'][-1],
                color = 'black',
                linewidth = 2,
                dashes = (2, 2),
                label='forcing band')
    speclog.legend(loc = 'lower left')
    a = fig.add_subplot(312)
    sresolution = self.statistics['kMeta(t)']**(-1)
    if (   (self.parameters['nx'] != self.parameters['ny'])
        or (self.parameters['nx'] != self.parameters['nz'])):
        print('Warning: basic sanity check using possibly inappropriate CFL estimate')
    dx = ((2*np.pi/self.parameters['dkx'])/self.parameters['nx']) # Lx / nx
    dy = ((2*np.pi/self.parameters['dky'])/self.parameters['ny'])
    dz = ((2*np.pi/self.parameters['dkz'])/self.parameters['nz'])
    tresolution = (self.parameters['dt']*np.sqrt(3)*self.statistics['vel_max(t)'] /
                   dx)
    if ((np.round(dx, 5) != np.round(dy, 5))
        or (np.round(dx, 5) != np.round(dz, 5))):
        print('WARNING: CFL computation does not make sense for non-isotropic grids')
    a.plot(self.statistics['t'] / self.statistics['Tint'],
           sresolution,
           label = '$(k_M \eta_K)^{-1}$ (spatial resolution)')
    a.plot(self.statistics['t'] / self.statistics['Tint'],
           tresolution,
           label = '$\\frac{\\Delta t}{\\Delta x} \sqrt{3}\\| u \\|_\infty$ (CFL upper bound)')
    a.set_ylim(top = 1.05*max(sresolution.max(),
                              tresolution.max()),
                              bottom = 0)
    with self.get_data_file() as data_file:
        if 'CFL_velocity' in data_file['statistics'].keys():
            self.statistics['CFL_velocity(t)'] = data_file['statistics/CFL_velocity'][ii0:ii1+1]
            tresolution2 = (self.parameters['dt'] * self.statistics['CFL_velocity(t)']
                            / dx)
            a.plot(self.statistics['t'] / self.statistics['Tint'],
                    tresolution2,
                    label = '$\\frac{\\Delta t}{\\Delta x} \\| u \\|_{CFL}$')
            a.set_ylim(
                    top = 1.05*max(sresolution.max(), tresolution.max(), tresolution2.max()),
                    bottom = 0)
    a.legend(loc = 'best')
    a.set_xlim(
            self.statistics['t'][0] / self.statistics['Tint'],
            self.statistics['t'][-1] / self.statistics['Tint'])
    a.set_xlabel('$t / T$')
    a.set_ylim(top = 1., bottom = 0)
    c = fig.add_subplot(313)
    c.axhline(1.0, linewidth=0.5, color = 'gray', dashes = (1, 1))
    # compare dissipation to injection rates
    diss_rate = self.statistics['diss(t)'][1:-1]
    energy_rate  = ((
            self.statistics['energy(t)'][2:]
          - self.statistics['energy(t)'][:-2]) / (
            2 * (self.statistics['t'][1] - self.statistics['t'][0])))
    inj_rate = energy_rate + diss_rate
    c.plot(self.statistics['t'][1:-1] / self.statistics['tauK'],
           inj_rate / diss_rate, label = '$\\varepsilon_i(t) / \\varepsilon_\\nu(t)$')
    if self.parameters['forcing_type'] == 'fixed_energy_injection_rate':
        c.plot(self.statistics['t'][1:-1] / self.statistics['tauK'],
               inj_rate / self.parameters['injection_rate'],
               label = '$\\varepsilon_i(t) / $(prescribed rate)',
               color = 'C5')
    # quasi-time-dependent drag coefficient
    # (Lint is obtained from time-averaged spectrum,
    #  and it's expensive to compute Lint(t))
    drag_coeff = self.statistics['diss(t)']*self.statistics['Lint'] / self.statistics['Uint(t)']**3
    drag_plot = c.plot(self.statistics['t'] / self.statistics['tauK'],
            drag_coeff,
            label = '$\\varepsilon_\\nu(t) L / (U(t)^3)$')
    # drag coefficient computed from time-averaged quantities
    c.axhline(
            self.statistics['diss']*self.statistics['Lint'] / self.statistics['Uint']**3,
            color = drag_plot[0]._color,
            dashes = (1, 1))
    c.set_xlim(
            self.statistics['t'][0] / self.statistics['tauK'],
            self.statistics['t'][-1] / self.statistics['tauK'])
    if ((inj_rate / diss_rate).max() > 2):
        c.set_ylim(top = 2)
    c.set_ylim(bottom = 0)
    c.set_xlabel('$t / \\tau_K$')
    b = c.twinx()
    b.plot(self.statistics['t'] / self.statistics['tauK'],
           self.statistics['Rlambda(t)'],
           color = (1, 0, 0))
    b.set_ylabel('$R_\\lambda$', color = (1, 0, 0))
    for tt in b.get_yticklabels():
        tt.set_color((1, 0, 0))
    c.legend(loc = 'best')
    c.set_zorder(b.get_zorder()+1)
    c.set_frame_on(False)
    fig.suptitle(self.simname)
    fig.tight_layout()
    fig.savefig(os.path.join(fig_dir, 'stats' + fig_suffix + '.' + fig_format))
    plt.close(fig)
    return None

