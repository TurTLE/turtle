################################################################################
#                                                                              #
#  Copyright 2015-2019 Max Planck Institute for Dynamics and Self-Organization #
#                                                                              #
#  This file is part of TurTLE.                                                #
#                                                                              #
#  TurTLE is free software: you can redistribute it and/or modify              #
#  it under the terms of the GNU General Public License as published           #
#  by the Free Software Foundation, either version 3 of the License,           #
#  or (at your option) any later version.                                      #
#                                                                              #
#  TurTLE is distributed in the hope that it will be useful,                   #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>              #
#                                                                              #
# Contact: Cristian.Lalescu@ds.mpg.de                                          #
#                                                                              #
################################################################################



import os
import shutil
import subprocess
import argparse
import h5py
import warnings

import numpy as np

import TurTLE
import TurTLE.tools
from TurTLE.host_info import host_info
from ._base import _base

class _code(_base):
    """This class is meant to stitch together the C++ code into a final source file,
    compile it, and handle all job launching.
    """
    def __init__(
            self,
            work_dir = './',
            simname = 'test'):
        _base.__init__(self, work_dir = work_dir, simname = simname)
        self.host_info = host_info
        self.check_current_vorticity_exists = True
        self.check_current_velocity_exists = False
        return None
    def set_precision(
            self,
            fluid_dtype):
        if fluid_dtype in [np.float32, np.float64]:
            self.fluid_dtype = fluid_dtype
        elif fluid_dtype in ['single', 'double']:
            if fluid_dtype == 'single':
                self.fluid_dtype = np.dtype(np.float32)
            elif fluid_dtype == 'double':
                self.fluid_dtype = np.dtype(np.float64)
        self.rtype = self.fluid_dtype
        if self.rtype == np.float32:
            self.ctype = np.dtype(np.complex64)
            self.C_field_dtype = 'float'
            self.fluid_precision = 'single'
        elif self.rtype == np.float64:
            self.ctype = np.dtype(np.complex128)
            self.C_field_dtype = 'double'
            self.fluid_precision = 'double'
        return None
    def compile_code(
            self,
            no_debug = True):
        if os.path.exists(os.path.join(self.work_dir, self.name)):
            return 0
        # compile code
        build_dir = 'TurTLE_build_' + self.name
        os.makedirs(build_dir, exist_ok = True)
        os.chdir(build_dir)
        self.write_src()
        with open('CMakeLists.txt', 'w') as outfile:
            outfile.write('cmake_minimum_required(VERSION 3.23)\n')
            outfile.write('set(CMAKE_CXX_STANDARD 17)\n')
            outfile.write('set(CMAKE_CXX_STANDARD_REQUIRED ON)\n')
            outfile.write('if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.25")\n')
            outfile.write('  cmake_policy(SET CMP0140 NEW)\n')
            outfile.write('endif()\n')
            outfile.write('if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.27")\n')
            outfile.write('  cmake_policy(SET CMP0144 NEW)\n')
            outfile.write('endif()\n')
            outfile.write('project(project_{0} LANGUAGES C CXX)\n'.format(self.name))
            outfile.write('add_executable({0} {0}.cpp)\n'.format(self.name))
            outfile.write('find_package(OpenMP REQUIRED)\n')
            outfile.write('find_package(MPI REQUIRED COMPONENTS C)\n')
            outfile.write('set(HDF5_STATIC ON)\n')
            outfile.write('set(HDF5_PREFER_PARALLEL TRUE)\n')
            #outfile.write('set(HDF5_NO_FIND_PACKAGE_CONFIG_FILE TRUE)\n')
            outfile.write('find_package(HDF5 REQUIRED COMPONENTS C)\n')
            if not no_debug:
                outfile.write('message(VERBOSE "found HDF5 include dir at ${HDF5_C_INCLUDE_DIR}")\n')
            #outfile.write('target_include_directories({0} PRIVATE ${{HDF5_C_INCLUDE_DIR}})\n'.format(self.name))
            #ideally we should use something like the following 2 lines
            #outfile.write('set(CMAKE_CXX_COMPILER ${TURTLE_CXX_COMPILER})\n')
            #outfile.write('set(CMAKE_C_COMPILER ${TURTLE_C_COMPILER})\n')
            outfile.write('set(CMAKE_MODULE_PATH $ENV{CMAKE_MODULE_PATH} ${CMAKE_MODULE_PATH})\n')
            if not no_debug:
                outfile.write('message(VERBOSE "CMAKE_MODULE_PATH is ${CMAKE_MODULE_PATH}")\n')
            outfile.write('find_package(GSL)\n')
            outfile.write('find_package(TurTLE REQUIRED)\n')
            outfile.write('include(set_up_FFTW)\n')
            outfile.write('set(CMAKE_CXX_COMPILE_FLAGS "${CMAKE_CXX_COMPILE_FLAGS} $ENV{TURTLE_COMPILATION_FLAGS}")\n')
            outfile.write('set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_CXX_COMPILE_FLAGS}")\n')
            outfile.write('set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} $ENV{TURTLE_EXE_LINKER_FLAGS}")\n')
            outfile.write('if(NDEBUG)\n')
            outfile.write('    add_definitions(-DNDEBUG)\n')
            outfile.write('endif()\n')
            outfile.write('target_link_libraries({0}\n'.format(self.name)
                        + 'PRIVATE\n'
                        + 'TurTLE::TurTLE\n'
                        + 'HDF5::HDF5\n'
                        + 'MPI::MPI_C\n'
                        + 'OpenMP::OpenMP_CXX\n'
                        + ')\n')
            outfile.write('target_link_FFTW(' + self.name + ')\n')
            fname = self.dns_type + '_extra_cmake.txt'
            if os.path.exists(fname):
                with open(fname, 'r') as ifile:
                    for text_line in ifile:
                        outfile.write(text_line.replace(self.dns_type, self.name))
        current_environment = os.environ
        if no_debug:
            subprocess.check_call(
                    ['cmake', '.'],
                    env = current_environment,
                    stdout = open('TurTLE_cmake_output.log', 'w'))
            make_result = subprocess.check_call(
                    ['make'],
                    env = current_environment,
                    stdout = open('TurTLE_make_output.log', 'w'))
        else:
            current_environment['VERBOSE'] = '1'
            subprocess.check_call(
                    ['cmake', '.'],
                    env = current_environment)
            make_result = subprocess.check_call(
                    ['make'],
                    env = current_environment)
        os.chdir('..')
        shutil.copy2(os.path.join(build_dir, self.name), os.path.join(self.work_dir, self.name))
        return make_result
    def set_host_info(
            self,
            host_info = {}):
        self.host_info.update(host_info)
        return None
    def run(self,
            nb_processes,
            nb_threads_per_process,
            out_file = 'out_file',
            err_file = 'err_file',
            hours = 0,
            minutes = 10,
            njobs = 1,
            no_submit = False,
            no_debug = True):
        self.read_parameters()
        with h5py.File(os.path.join(self.work_dir, self.simname + '.h5'), 'r') as ifile:
            # if we are restarting, check that we're using the same precision
            # I consider it an error to run a different precision code at different points,
            # although technically some might just call it "bad practice".
            # control the behavior here if you want to
            if 'code_info' in ifile.keys():
                if 'exec_name' in ifile['code_info'].keys():
                    if not (str(self.fluid_precision) in str(ifile['code_info/exec_name'][()])):
                        raise AssertionError('Desired precision does not match existing DNS precision.')
        # check if stop_<simname> exists, remove if it does
        if os.path.exists(os.path.join(self.work_dir, 'stop_' + self.simname)):
            warnings.warn("Found stop_<simname> file, I will remove it.")
            os.remove(os.path.join(self.work_dir, 'stop_' + self.simname))
        with h5py.File(os.path.join(self.work_dir, self.simname + '.h5'), 'r') as data_file:
            iter0 = data_file['iteration'][...]
            checkpoint0 = data_file['checkpoint'][()]
            cp_fname = os.path.join(
                    self.work_dir,
                    self.simname + '_checkpoint_{0}.h5'.format(checkpoint0))
            if (self.check_current_vorticity_exists
                    or self.check_current_velocity_exists):
                if self.check_current_vorticity_exists:
                    checkpoint_field = 'vorticity'
                elif self.check_current_velocity_exists:
                    checkpoint_field = 'velocity'
                else:
                    checkpoint_field = 'none'
                assert os.path.exists(cp_fname)
                with h5py.File(cp_fname, 'r') as checkpoint_file:
                    assert '{0}'.format(iter0) in checkpoint_file[
                            checkpoint_field + '/complex'].keys()
        if not os.path.isdir(self.work_dir):
            os.makedirs(self.work_dir)
        assert (self.compile_code(no_debug = no_debug) == 0)
        if 'niter_todo' not in self.parameters.keys():
            self.parameters['niter_todo'] = 1
        current_dir = os.getcwd()
        os.chdir(self.work_dir)
        os.chdir(current_dir)
        if not 'MPI' in self.host_info.keys():
            self.host_info['MPI'] = 'mpich'
        if self.host_info['MPI'] == 'openmpi':
            mpirun_environment_set = 'x'
        else:
            mpirun_environment_set = 'env'
        command_atoms = ['mpirun',
                         '-np',
                         '{0}'.format(nb_processes),
                         './' + self.name,
                         self.simname]
        if self.host_info['type'] == 'cluster':
            job_name_list = []
            for j in range(njobs):
                suffix = self.simname + '_{0}'.format(iter0 + j*self.parameters['niter_todo'])
                qsub_script_name = 'run_' + suffix + '.sh'
                self.write_sge_file(
                    file_name     = os.path.join(self.work_dir, qsub_script_name),
                    nprocesses    = nb_processes,
                    name_of_run   = suffix,
                    command_atoms = command_atoms[3:],
                    hours         = hours,
                    minutes       = minutes,
                    out_file      = out_file + '_' + suffix + '.%j',
                    err_file      = err_file + '_' + suffix + '.%j',
                    nb_threads_per_process = nb_threads_per_process)
                os.chdir(self.work_dir)
                qsub_atoms = ['qsub']
                if not no_submit:
                    if len(job_name_list) >= 1:
                        qsub_atoms += ['-hold_jid', job_name_list[-1]]
                    subprocess.check_call(qsub_atoms + [qsub_script_name])
                    os.chdir(current_dir)
                    job_name_list.append(suffix)
        if self.host_info['type'] == 'SLURM':
            job_id_list = []
            for j in range(njobs):
                suffix = self.simname + '_{0}'.format(iter0 + j*self.parameters['niter_todo'])
                qsub_script_name = 'run_' + suffix + '.sh'
                self.write_slurm_file(
                    file_name     = os.path.join(self.work_dir, qsub_script_name),
                    name_of_run   = suffix,
                    command_atoms = command_atoms[3:],
                    hours         = hours,
                    minutes       = minutes,
                    out_file      = out_file + '_' + suffix,
                    err_file      = err_file + '_' + suffix,
                    nb_mpi_processes = nb_processes,
                    nb_threads_per_process = nb_threads_per_process)
                os.chdir(self.work_dir)
                qsub_atoms = ['sbatch']

                if not no_submit:
                    if len(job_id_list) >= 1:
                        qsub_atoms += ['--dependency=afterok:{0}'.format(job_id_list[-1])]
                    p = subprocess.Popen(
                        qsub_atoms + [qsub_script_name],
                        stdout = subprocess.PIPE)
                    out, err = p.communicate()
                    p.terminate()
                    job_id_list.append(int(out.split()[-1]))
                os.chdir(current_dir)
        elif self.host_info['type'] == 'IBMLoadLeveler':
            suffix = self.simname + '_{0}'.format(iter0)
            job_script_name = 'run_' + suffix + '.sh'
            energy_policy_tag = (
                    'TurTLE'
                    + '_np{0}_ntpp{1}'.format(
                        nb_processes, nb_threads_per_process)
                    + '_Nx{0}_Ny{1}_Nz{2}'.format(
                        self.parameters['nx'], self.parameters['ny'], self.parameters['nz']))
            if 'nparticles' in self.parameters.keys():
                energy_policy_tag += '_nparticles{0}'.format(self.parameters['nparticles'])
            if (njobs == 1):
                self.write_IBMLoadLeveler_file_single_job(
                    file_name     = os.path.join(self.work_dir, job_script_name),
                    name_of_run   = suffix,
                    command_atoms = command_atoms[3:],
                    hours         = hours,
                    minutes       = minutes,
                    out_file      = out_file + '_' + suffix,
                    err_file      = err_file + '_' + suffix,
                    nb_mpi_processes = nb_processes,
                    nb_threads_per_process = nb_threads_per_process,
                    energy_policy_tag = energy_policy_tag)
            else:
                self.write_IBMLoadLeveler_file_many_job(
                    file_name     = os.path.join(self.work_dir, job_script_name),
                    name_of_run   = suffix,
                    command_atoms = command_atoms[3:],
                    hours         = hours,
                    minutes       = minutes,
                    out_file      = out_file + '_' + suffix,
                    err_file      = err_file + '_' + suffix,
                    njobs = njobs,
                    nb_mpi_processes = nb_processes,
                    nb_threads_per_process = nb_threads_per_process,
                    energy_policy_tag = energy_policy_tag)
            submit_atoms = ['llsubmit']

            if not no_submit:
                subprocess.check_call(submit_atoms + [os.path.join(self.work_dir, job_script_name)])

        elif self.host_info['type'] == 'pc':
            if 'executable_launcher' in self.host_info.keys():
                command_atoms = [self.host_info['executable_launcher']]
                if self.host_info['executable_launcher'] == 'srun':
                    command_atoms += ['-p', 'interactive']
                if 'executable_parameters' in self.host_info.keys():
                    command_atoms += self.host_info['executable_parameters']
                command_atoms += ['-n',
                                  '{0}'.format(nb_processes),
                                  './' + self.name,
                                  self.simname]
            if not no_submit:
                os.chdir(self.work_dir)
                custom_env = os.environ
                custom_env['OMP_NUM_THREADS'] = str(nb_threads_per_process)
                for j in range(njobs):
                    suffix = self.simname + '_{0}'.format(iter0 + j*self.parameters['niter_todo'])
                    print('running code with command\n' + ' '.join(command_atoms))
                    try:
                        subprocess.check_call(
                                        command_atoms,
                                        stdout = open(os.path.join(self.work_dir, out_file + '_' + suffix), 'w'),
                                        stderr = open(os.path.join(self.work_dir, err_file + '_' + suffix), 'w'),
                                        env = custom_env)
                    except subprocess.CalledProcessError:
                        print('!!!! TurTLE-generated executable failed to run, output follows:')
                        print(open(out_file + '_' + suffix, 'r').read())
                        print('!!!! TurTLE-generated executable failed to run, error log follows:')
                        print(open(err_file + '_' + suffix, 'r').read())
                        raise SystemExit('TurTLE-generated executable failed to run, see above for details.')
                os.chdir(current_dir)
        return None
    def write_IBMLoadLeveler_file_single_job(
            self,
            file_name = None,
            nprocesses = None,
            name_of_run = None,
            command_atoms = [],
            hours = None,
            minutes = None,
            out_file = None,
            err_file = None,
            nb_mpi_processes = None,
            nb_threads_per_process = None,
            energy_policy_tag = 'TurTLE'):

        script_file = open(file_name, 'w')
        script_file.write('# @ shell=/bin/bash\n')
        # error file
        if type(err_file) == type(None):
            err_file = 'err.job.$(jobid)'
        script_file.write('# @ error = ' + os.path.join(self.work_dir, err_file) + '\n')
        # output file
        if type(out_file) == type(None):
            out_file = 'out.job.$(jobid)'
        script_file.write('# @ output = ' + os.path.join(self.work_dir, out_file) + '\n')

        # If Ibm is used should be : script_file.write('# @ job_type = parallel\n')
        script_file.write('# @ job_type = MPICH\n')
        assert(type(self.host_info['environment']) != type(None))
        script_file.write('# @ class = {0}\n'.format(self.host_info['environment']))

        script_file.write('# @ node_usage = not_shared\n')
        script_file.write('# @ notification = complete\n')
        script_file.write('# @ notify_user = {0}\n'.format(self.host_info['mail_address']))

        nb_cpus_per_node = self.host_info['deltanprocs']
        assert isinstance(nb_cpus_per_node, int) and \
               nb_cpus_per_node >= 1, \
               'nb_cpus_per_node is {}'.format(nb_cpus_per_node)

        # No more threads than the number of cores
        assert nb_threads_per_process <= nb_cpus_per_node, \
               "Cannot use more threads ({} asked) than the number of cores ({})".format(
                   nb_threads_per_process, nb_cpus_per_node)
        # Warn if some core will not be ued
        if nb_cpus_per_node%nb_threads_per_process != 0:
            warnings.warn("The number of threads is smaller than the number of cores (machine will be underused)",
                    UserWarning)

        nb_cpus = nb_mpi_processes*nb_threads_per_process
        if (nb_cpus < nb_cpus_per_node):
            # in case we use only a few process on a single node
            nb_nodes = 1
            nb_processes_per_node = nb_mpi_processes
            first_node_tasks = nb_mpi_processes
        else:
            nb_nodes = int((nb_cpus+nb_cpus_per_node-1) // nb_cpus_per_node)
            # if more than one node we requiere to have a multiple of deltanprocs
            nb_processes_per_node = int(nb_cpus_per_node // nb_threads_per_process)
            first_node_tasks = int(nb_mpi_processes - (nb_nodes-1)*nb_processes_per_node)

        script_file.write('# @ energy_policy_tag = {0}\n'.format(energy_policy_tag))
        script_file.write('# @ minimize_time_to_solution = yes\n')
        script_file.write('# @ resources = ConsumableCpus({})\n'.format(nb_threads_per_process))
        script_file.write('# @ network.MPI = sn_all,not_shared,us\n')
        script_file.write('# @ wall_clock_limit = {0}:{1:0>2d}:00\n'.format(hours, minutes))
        script_file.write('# @ node = {0}\n'.format(nb_nodes))
        script_file.write('# @ tasks_per_node = {0}\n'.format(nb_processes_per_node))
        if (first_node_tasks > 0):
            script_file.write('# @ first_node_tasks = {0}\n'.format(first_node_tasks))
        script_file.write('# @ queue\n')


        script_file.write('source ~/.config/TurTLE/bashrc\n')
        script_file.write('module li\n')
        script_file.write('export OMP_NUM_THREADS={}\n'.format(nb_threads_per_process))

        script_file.write('echo "Start time is `date`"\n')
        script_file.write('export HTMLOUTPUT={}.html\n'.format(command_atoms[-1]))
        script_file.write('cd ' + self.work_dir + '\n')

        script_file.write('export KMP_AFFINITY=compact,verbose\n')
        script_file.write('export I_MPI_PIN_DOMAIN=omp\n')
        script_file.write('mpiexec.hydra '
            + ' -np {} '.format(nb_mpi_processes)
            + ' -ppn {} '.format(nb_processes_per_node)
            #+ ' -ordered-output -prepend-rank '
            + os.path.join(
                self.work_dir,
                command_atoms[0]) +
            ' ' +
            ' '.join(command_atoms[1:]) +
            '\n')

        script_file.write('echo "End time is `date`"\n')
        script_file.write('exit 0\n')
        script_file.close()
        return None
    def write_IBMLoadLeveler_file_many_job(
            self,
            file_name = None,
            nprocesses = None,
            name_of_run = None,
            command_atoms = [],
            hours = None,
            minutes = None,
            out_file = None,
            err_file = None,
            njobs = 2,
            nb_mpi_processes = None,
            nb_threads_per_process = None,
            energy_policy_tag = 'TurTLE'):
        assert(type(self.host_info['environment']) != type(None))
        script_file = open(file_name, 'w')
        script_file.write('# @ shell=/bin/bash\n')
        # error file
        if type(err_file) == type(None):
            err_file = 'err.job.$(jobid).$(stepid)'
        script_file.write('# @ error = ' + os.path.join(self.work_dir, err_file) + '\n')
        # output file
        if type(out_file) == type(None):
            out_file = 'out.job.$(jobid).$(stepid)'
        script_file.write('# @ output = ' + os.path.join(self.work_dir, out_file) + '\n')
        # If Ibm is used should be : script_file.write('# @ job_type = parallel\n')
        script_file.write('# @ job_type = MPICH\n')
        assert(type(self.host_info['environment']) != type(None))
        script_file.write('# @ class = {0}\n'.format(self.host_info['environment']))
        script_file.write('# @ node_usage = not_shared\n')

        script_file.write('# @ notification = error\n')
        script_file.write('# @ notify_user = {0}\n'.format(self.host_info['mail_address']))
        script_file.write('#\n')

        nb_cpus_per_node = self.host_info['deltanprocs']
        assert isinstance(nb_cpus_per_node, int) and \
               nb_cpus_per_node >= 1, \
               'nb_cpus_per_node is {}'.format(nb_cpus_per_node)

        # No more threads than the number of cores
        assert nb_threads_per_process <= nb_cpus_per_node, \
               "Cannot use more threads ({} asked) than the number of cores ({})".format(
                   nb_threads_per_process, nb_cpus_per_node)
        # Warn if some core will not be ued
        if nb_cpus_per_node%nb_threads_per_process != 0:
            warnings.warn("The number of threads is smaller than the number of cores (machine will be underused)",
                    UserWarning)

        nb_cpus = nb_mpi_processes*nb_threads_per_process
        if (nb_cpus < nb_cpus_per_node):
            # in case we use only a few process on a single node
            nb_nodes = 1
            nb_processes_per_node = nb_mpi_processes
            first_node_tasks = nb_mpi_processes
        else:
            nb_nodes = int((nb_cpus+nb_cpus_per_node-1) // nb_cpus_per_node)
            # if more than one node we requiere to have a multiple of deltanprocs
            nb_processes_per_node = int(nb_cpus_per_node // nb_threads_per_process)
            first_node_tasks = int(nb_mpi_processes - (nb_nodes-1)*nb_processes_per_node)

        for job in range(njobs):
            script_file.write('# @ step_name = {0}.{1}\n'.format(self.simname, job))
            if job > 0:
                script_file.write('# @ dependency = {0}.{1} == 0\n'.format(self.simname, job - 1))
            script_file.write('# @ resources = ConsumableCpus({})\n'.format(nb_threads_per_process))
            script_file.write('# @ network.MPI = sn_all,not_shared,us\n')
            script_file.write('# @ wall_clock_limit = {0}:{1:0>2d}:00\n'.format(hours, minutes))
            script_file.write('# @ energy_policy_tag = {0}\n'.format(energy_policy_tag))
            script_file.write('# @ minimize_time_to_solution = yes\n')
            assert type(self.host_info['environment']) != type(None)
            script_file.write('# @ node = {0}\n'.format(nb_nodes))
            script_file.write('# @ tasks_per_node = {0}\n'.format(nb_processes_per_node))
            if (first_node_tasks > 0):
                script_file.write('# @ first_node_tasks = {0}\n'.format(first_node_tasks))
            script_file.write('# @ queue\n')

        script_file.write('source ~/.config/TurTLE/bashrc\n')
        script_file.write('module li\n')
        script_file.write('export OMP_NUM_THREADS={}\n'.format(nb_threads_per_process))

        script_file.write('echo "Start time is `date`"\n')
        script_file.write('export HTMLOUTPUT={}.html\n'.format(command_atoms[-1]))
        script_file.write('cd ' + self.work_dir + '\n')

        script_file.write('export KMP_AFFINITY=compact,verbose\n')
        script_file.write('export I_MPI_PIN_DOMAIN=omp\n')

        script_file.write('mpiexec.hydra '
            + ' -np {} '.format(nb_mpi_processes)
            + ' -ppn {} '.format(nb_processes_per_node)
            + ' -ordered-output -prepend-rank '
            + os.path.join(
                self.work_dir,
                command_atoms[0]) +
            ' ' +
            ' '.join(command_atoms[1:]) +
            '\n')

        script_file.write('echo "End time is `date`"\n')
        script_file.write('exit 0\n')
        script_file.close()
        return None
    def write_sge_file(
            self,
            file_name = None,
            nprocesses = None,
            name_of_run = None,
            command_atoms = [],
            hours = None,
            minutes = None,
            out_file = None,
            err_file = None,
            nb_threads_per_process = 1):
        script_file = open(file_name, 'w')
        script_file.write('#!/bin/bash\n')
        # export all environment variables
        script_file.write('#$ -V\n')
        # job name
        script_file.write('#$ -N {0}\n'.format(name_of_run))
        # use current working directory
        script_file.write('#$ -cwd\n')
        # error file
        if not type(err_file) == type(None):
            script_file.write('#$ -e ' + os.path.join(self.work_dir, err_file) + '\n')
        # output file
        if not type(out_file) == type(None):
            script_file.write('#$ -o ' + os.path.join(self.work_dir, out_file) + '\n')
        if not type(self.host_info['environment']) == type(None):
            envprocs = nb_threads_per_process * nprocesses
            script_file.write('#$ -pe {0} {1}\n'.format(
                    self.host_info['environment'],
                    envprocs))
        script_file.write('echo "got $NSLOTS slots."\n')
        script_file.write('echo "Start time is `date`"\n')
        script_file.write('mpiexec \\\n' +
                          '\t-machinefile $TMPDIR/machines \\\n' +
                          '\t-genv OMP_NUM_THREADS={0} \\\n'.format(nb_threads_per_process) +
                          '\t-genv OMP_PLACES=cores \\\n' +
                          '\t-n {0} \\\n\t{1}\n'.format(nprocesses, ' '.join(command_atoms)))
        script_file.write('echo "End time is `date`"\n')
        script_file.write('exit 0\n')
        script_file.close()
        return None
    def write_slurm_file(
            self,
            file_name = None,
            name_of_run = None,
            command_atoms = [],
            hours = None,
            minutes = None,
            out_file = None,
            err_file = None,
            nb_mpi_processes = None,
            nb_threads_per_process = None):
        script_file = open(file_name, 'w')
        script_file.write('#!/bin/bash -l\n')
        # job name
        script_file.write('#SBATCH -J {0}\n'.format(name_of_run))
        # use current working directory
        script_file.write('#SBATCH -D ./\n')
        # error file
        if not type(err_file) == type(None):
            script_file.write('#SBATCH -e ' + os.path.join(self.work_dir, err_file) + '\n')
        # output file
        if not type(out_file) == type(None):
            script_file.write('#SBATCH -o ' + os.path.join(self.work_dir, out_file) + '\n')

        # set up environment
        if self.host_info['explicit_slurm_environment']:
            script_file.write('#SBATCH --partition={0}\n'.format(
                    self.host_info['environment']))
        if 'account' in self.host_info.keys():
            script_file.write('#SBATCH --account={0}\n'.format(
                    self.host_info['account']))

        nb_cpus_per_node = self.host_info['deltanprocs']
        assert isinstance(nb_cpus_per_node, int) \
               and nb_cpus_per_node >= 1, \
               'nb_cpus_per_node is {}'.format(nb_cpus_per_node)

        # No more threads than the number of cores
        assert nb_threads_per_process <= nb_cpus_per_node, \
               "Cannot use more threads ({} asked) than the number of cores ({})".format(
                   nb_threads_per_process, nb_cpus_per_node)
        # Warn if some core will not be ued
        if nb_cpus_per_node%nb_threads_per_process != 0:
            warnings.warn(
                    "The number of threads is smaller than the number of cores (machine will be underused)",
                    UserWarning)

        nb_cpus = nb_mpi_processes*nb_threads_per_process
        if (nb_cpus < nb_cpus_per_node):
            # in case we use only a few process on a single node
            nb_nodes = 1
            nb_processes_per_node = nb_mpi_processes
        else:
            nb_nodes = int((nb_cpus+nb_cpus_per_node-1) // nb_cpus_per_node)
            # if more than one node we requiere to have a multiple of deltanprocs
            nb_processes_per_node = int(nb_cpus_per_node // nb_threads_per_process)


        script_file.write('#SBATCH --nodes={0}\n'.format(nb_nodes))
        script_file.write('#SBATCH --ntasks-per-node={0}\n'.format(nb_processes_per_node))
        script_file.write('#SBATCH --cpus-per-task={0}\n'.format(nb_threads_per_process))

        script_file.write('#SBATCH --mail-type=none\n')
        script_file.write('#SBATCH --time={0}:{1:0>2d}:00\n'.format(hours, minutes))
        if 'extra_slurm_lines' in self.host_info.keys():
            for line in self.host_info['extra_slurm_lines']:
                script_file.write(line + '\n')
        ## following cleans up environment for job.
        ## put these in the 'extra_slurm_lines' list if you need them
        ## make sure that "~/.config/TurTLE/bashrc" exists and builds desired job environment
        #script_file.write('#SBATCH --export=NONE\n')
        #script_file.write('#SBATCH --get-user-env\n')
        #script_file.write('export OMP_PLACES=cores\n') # or threads, as appropriate
        # also look up OMP_PROC_BIND and SLURM_HINT
        #script_file.write('source ~/.config/TurTLE/bashrc\n')
        if nb_threads_per_process > 1:
            script_file.write('export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}\n')

        # explicit binding options that can be used further on if needed
        if 'use_TurTLE_core_distribution' not in host_info.keys():
            host_info['use_TurTLE_core_distribution'] = False
        if host_info['use_TurTLE_core_distribution']:
            core_masks = TurTLE.tools.distribute_cores_evenly(
                    nprocesses = nb_processes_per_node,
                    nthreads_per_process = nb_threads_per_process,
                    total_number_of_cores = nb_cpus_per_node)
            script_file.write('export SLURM_CPU_BIND_OPTION="--cpu-bind=verbose,mask_cpu:' +
                              ','.join(['0x{0:x}'.format(mm) for mm in core_masks]) + '"\n')

        script_file.write('echo "Start time is `date`"\n')
        script_file.write('cd ' + self.work_dir + '\n')
        script_file.write('export HTMLOUTPUT={}.html\n'.format(command_atoms[-1]))
        if not 'use_vtune' in host_info.keys():
            host_info['use_vtune'] = False
        if not 'use_aps' in host_info.keys():
            host_info['use_aps'] = False
        if host_info['use_vtune']:
            script_file.write('module load vtune\n')
        if host_info['use_aps']:
            if 'aps_setup' not in host_info.keys():
                host_info['aps_setup'] = 'module load vtune'
            script_file.write(host_info['aps_setup'] + '\n')
        if 'executable_launcher' in self.host_info.keys():
            executable_launcher = self.host_info['executable_launcher']
        else:
            executable_launcher = 'srun'
            if host_info['use_TurTLE_core_distribution']:
                executable_launcher += ' ${SLURM_CPU_BIND_OPTION}'
            if host_info['use_vtune']:
                if 'vtune_executable' not in host_info.keys():
                    host_info['vtune_executable'] = 'vtune'
                executable_launcher += ' ' + host_info['vtune_executable'] + ' -collect hpc-performance -trace-mpi -quiet -result-dir=vtune_${SLURM_JOB_NAME}'
            if host_info['use_aps']:
                if 'aps_executable' not in host_info.keys():
                    host_info['aps_executable'] = 'aps'

                executable_launcher += ' ' + host_info['aps_executable'] + ' --result-dir=aps_${SLURM_JOB_NAME} --collection-mode=all'
        script_file.write(executable_launcher + ' {0}\n'.format(' '.join(command_atoms)))
        script_file.write('echo "End time is `date`"\n')
        script_file.write('exit 0\n')
        script_file.close()
        return None
    def prepare_launch(
            self,
            args = [],
            **kwargs):
        parser = argparse.ArgumentParser('turtle ' + type(self).__name__)
        self.add_parser_arguments(parser)
        opt = parser.parse_args(args)

        if opt.ncpu != -1:
            warnings.warn(
                    'ncpu should be replaced by np/ntpp',
                    DeprecationWarning)
            opt.nb_processes = opt.ncpu
            opt.nb_threads_per_process = 1

        self.set_host_info(TurTLE.host_info)
        if type(opt.environment) != type(None):
            self.host_info['environment'] = opt.environment
        # we cannot use both vtune and aps for profiling at the same time
        assert(not (opt.use_vtune and opt.use_aps))
        self.host_info['use_vtune'] = opt.use_vtune
        self.host_info['use_aps'] = opt.use_aps
        return opt

