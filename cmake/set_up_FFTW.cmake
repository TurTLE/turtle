if(MKL_AS_FFTW)
  function(target_link_FFTW target)
    target_link_libraries(
      ${target}
      PRIVATE
      ${mkl_fftw_wrapper}
      MKL::MKL)

    target_include_directories(
      ${target}
      PRIVATE
      ${MKL_DIR}/../../../include/fftw)
  endfunction()
else()
  find_package(PkgConfig REQUIRED)

  pkg_check_modules(FFTW3 REQUIRED IMPORTED_TARGET fftw3)
  pkg_check_modules(FFTW3F REQUIRED IMPORTED_TARGET fftw3f)

  foreach(fftw
          fftw3
          fftw3f)
    foreach(lib_suffix
            omp
            mpi
            threads)
      find_library(
        ${fftw}_${lib_suffix}
        NAMES ${fftw}_${lib_suffix}
        HINTS "${FFTW3_LIBRARY_DIRS}"
        REQUIRED)
    endforeach()
  endforeach()

  function(target_link_FFTW target)
    target_link_libraries(
      ${target}
      PRIVATE
      ${fftw3_mpi}
      ${fftw3f_mpi}
      ${fftw3_omp}
      ${fftw3f_omp}
      ${fftw3_threads}
      ${fftw3f_threads})

    target_link_libraries(
      ${target}
      PRIVATE
      PkgConfig::FFTW3
      PkgConfig::FFTW3F)
  endfunction()

  get_directory_property(hasParent PARENT_DIRECTORY)
  if (hasParent)
    return(PROPAGATE FFTW3_INCLUDE_DIRS)
  endif()
endif()
