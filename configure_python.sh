#! /usr/bin/env bash

set -e

cp host_info.py TurTLE/
sed "s/@TURTLE_VERSION_LONG@/3.15/" setup.py.in > setup.py
sed "s/@TURTLE_VERSION_LONG@/3.15/" TurTLE/__init__.py.in > TurTLE/__init__.py.tmp
sed "s/@CMAKE_INSTALL_DIR@/UNAVAILABLE_FOR_PURE_PYTHON_INSTALLS/" TurTLE/__init__.py.tmp > TurTLE/__init__.py
rm TurTLE/__init__.py.tmp

