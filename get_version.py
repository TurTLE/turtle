################################################################################
#                                                                              #
#  Copyright 2019 Max Planck Institute for Dynamics and Self-Organization      #
#                                                                              #
#  This file is part of TurTLE.                                                #
#                                                                              #
#  TurTLE is free software: you can redistribute it and/or modify              #
#  it under the terms of the GNU General Public License as published           #
#  by the Free Software Foundation, either version 3 of the License,           #
#  or (at your option) any later version.                                      #
#                                                                              #
#  TurTLE is distributed in the hope that it will be useful,                   #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>              #
#                                                                              #
# Contact: Cristian.Lalescu@ds.mpg.de                                          #
#                                                                              #
################################################################################



import datetime
import subprocess
import argparse

def main():
    parser = argparse.ArgumentParser('Get git version.')
    parser.add_argument(
            '-l',
            '--long',
            dest = 'long',
            help = 'Request exact versioning information (including git hash).',
            action = 'store_true')
    # get current time
    now = datetime.datetime.now()
    # obtain version
    try:
        git_branch = subprocess.check_output(['git',
                                              'rev-parse',
                                              '--abbrev-ref',
                                              'HEAD']).strip().split()[-1].decode()
        git_revision = subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()
        git_date = datetime.datetime.fromtimestamp(int(subprocess.check_output(['git', 'log', '-1', '--format=%ct']).strip()))
    except:
        git_revision = ''
        git_branch = ''
        git_date = now
    if git_branch == '':
        # there's no git available or something
        VERSION = '{0:0>4}{1:0>2}{2:0>2}.{3:0>2}{4:0>2}{5:0>2}'.format(
                    git_date.year, git_date.month, git_date.day,
                    git_date.hour, git_date.minute, git_date.second)
        VERSION_long = VERSION
    else:
        VERSION = subprocess.check_output(['git', 'describe', '--tags']).strip().decode().split('-')[0]
        VERSION_long = subprocess.check_output(
                ['git', 'describe', '--tags', '--dirty']).strip().decode().replace('-g', '+g').replace('-dirty', '.dirty').replace('-', '.post')
    opt = parser.parse_args()
    if opt.long:
        print(VERSION_long)
    else:
        print(VERSION)
    return VERSION_long

if __name__ == '__main__':
    main()

