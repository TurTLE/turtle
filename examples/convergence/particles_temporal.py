import numpy as np
import h5py
import matplotlib.pyplot as plt

import TurTLE
from TurTLE import DNS

base_niterations = 256

nparticles = 1000
particle_random_seed = 15
base_particle_dt = 0.5
test_niterations_particles = 128

neighbours_smoothness_list = [
        (1, 1),
        (3, 2)]

def main():
    generate_initial_conditions()

    run_simulations_particles()

    plot_error_particles()
    plot_traj_particles()
    return None

def generate_initial_conditions():
    # change these two values as needed.
    # number of MPI processes to use
    nprocesses = 8
    # number of OpenMP threads per MPI process to use
    nthreads_per_process = 1

# 1. Generate quasistationary state to use for initial conditions.
    # create a dns object
    c0 = DNS()
    # launch the simulation
    c0.launch([
            'NSVE',
            '-n', '32',
            '--np', '{0}'.format(nprocesses),
            '--ntpp', '{0}'.format(nthreads_per_process),
            '--precision', 'double',
            '--src-simname', 'B32p1e4',
            '--src-wd', TurTLE.data_dir,
            '--src-iteration', '0',
            '--simname', 'base',
            '--niter_todo', '{0}'.format(base_niterations),
            '--niter_out', '{0}'.format(base_niterations),
            '--overwrite-src-parameters',
            '--kMeta',  '1.',
            '--niter_stat', '16',
            '--checkpoints_per_file', '{0}'.format(16)])
    return None

def run_simulations_particles():
    # change these two values as needed.
    # number of MPI processes to use
    nprocesses = 8
    # number of OpenMP threads per MPI process to use
    nthreads_per_process = 1

# 1. Run NSVEparticles for a few iterations, to build up rhs values, consistent
#    with the relevant velocity field.
    factor = 1
    for neighbours, smoothness in neighbours_smoothness_list:
        interp_name = 'I{0}O{1}'.format(2*neighbours+2, 2*smoothness+1)
        # create dns object
        cc = DNS()
        # launch simulation
        cc.launch([
            'NSVEparticles',
            '-n', '{0}'.format(factor*32),
            '--np', '{0}'.format(nprocesses),
            '--ntpp', '{0}'.format(nthreads_per_process),
            '--src-simname', 'base',
            '--src-iteration', '{0}'.format(base_niterations),
            '--simname', 'nsvep_base_' + interp_name,
            '--precision', 'double',
            '--dtfactor', '{0}'.format(base_particle_dt/4),
            '--kMeta', '{0}'.format(factor*1.0),
            '--niter_todo', '{0}'.format(20),
            '--niter_out', '{0}'.format(1),
            '--niter_stat', '{0}'.format(1),
            '--nparticles', '{0}'.format(nparticles),
            '--niter_part', '{0}'.format(1),
            '--tracers0_neighbours', '{0}'.format(neighbours),
            '--tracers0_smoothness', '{0}'.format(smoothness),
            '--tracers0_integration_steps', '4',
            '--cpp_random_particles', '{0}'.format(particle_random_seed)])

# 2. Prepare initial conditions
    for neighbours, smoothness in neighbours_smoothness_list:
        interp_name = 'I{0}O{1}'.format(2*neighbours+2, 2*smoothness+1)
        for factor in [1, 2, 4]:
            df = h5py.File('nsvep_{0}x_{1}_checkpoint_0.h5'.format(factor, interp_name), 'w')
            # field
            field_iteration = 16
            df['vorticity/complex/{0}'.format(8*factor)] = h5py.ExternalLink(
                'nsvep_base_{0}_checkpoint_0.h5'.format(interp_name),
                'vorticity/complex/{0}'.format(field_iteration))
            # particles
            df['tracers0/state/{0}'.format(8*factor)] = h5py.ExternalLink(
                    'nsvep_base_{0}_checkpoint_0.h5'.format(interp_name),
                    'tracers0/state/16')
            # copy rhs
            source_file = h5py.File(
                    'nsvep_base_{0}_checkpoint_0.h5'.format(interp_name), 'r')
            rhs = source_file['tracers0/rhs/16'][()]
            for tt in range(1, rhs.shape[0]):
                ii = 16 - tt*4//factor + 1
                #print(factor, tt, ii)
                rhs[tt] = source_file['tracers0/rhs/{0}'.format(16 - tt*4//factor + 1)][1]
            df['tracers0/rhs/{0}'.format(8*factor)] = rhs
            df.close()

# 3. Run NSVEparticles
    for factor in [1, 2, 4]:
        # Test different interpolations.
        # The loop iterates over number of neighbours and smoothness.
        # The simulation names are defined based on "kernel size" and "order
        # of polynomial".
        # We do this to emphasize the one-to-one correspondence between
        # neighbours and kernel size, and smoothness and order of polynomial.
        for neighbours, smoothness in neighbours_smoothness_list:
            interp_name = 'I{0}O{1}'.format(2*neighbours+2, 2*smoothness+1)
            source_file = h5py.File('nsvep_base_{0}.h5'.format(interp_name), 'r')
            # create dns object
            cc = DNS()
            # launch simulation
            cc.launch([
                    'NSVEparticles',
                    '-n', '{0}'.format(source_file['parameters/nx'][()]),
                    '--np', '{0}'.format(nprocesses),
                    '--ntpp', '{0}'.format(nthreads_per_process),
                    '--simname', 'nsvep_{0}x_{1}'.format(factor, interp_name),
                    '--precision', 'double',
                    '--dtfactor', '{0}'.format(base_particle_dt/factor),
                    '--niter_todo', '{0}'.format(test_niterations_particles*factor+8*factor),
                    '--niter_out', '{0}'.format(test_niterations_particles*factor+8*factor),
                    '--niter_stat', '{0}'.format(1),
                    ## ensure fluid physics is the same
                    '--dealias_type', '{0}'.format(source_file['parameters/dealias_type'][()]),
                    '--energy', '{0}'.format(source_file['parameters/energy'][()]),
                    '--famplitude', '{0}'.format(source_file['parameters/famplitude'][()]),
                    '--fk0', '{0}'.format(source_file['parameters/fk0'][()]),
                    '--fk1', '{0}'.format(source_file['parameters/fk1'][()]),
                    '--fmode', '{0}'.format(source_file['parameters/fmode'][()]),
                    '--friction_coefficient', '{0}'.format(source_file['parameters/friction_coefficient'][()]),
                    '--injection_rate', '{0}'.format(source_file['parameters/injection_rate'][()]),
                    '--nu', '{0}'.format(source_file['parameters/nu'][()]),
                    '--forcing_type', '{0}'.format(str(source_file['parameters/forcing_type'][()], 'ascii')),
                    ## particle params
                    '--nparticles', '{0}'.format(nparticles),
                    '--niter_part', '{0}'.format(1),
                    '--tracers0_neighbours', '{0}'.format(neighbours),
                    '--tracers0_smoothness', '{0}'.format(smoothness),
                    '--tracers0_integration_steps', '4',
                    '--cpp_random_particles', '0'], # turn off C++ particle initialization
                    iter0 = 8*factor)
            source_file.close()
    return None

def plot_error_particles():
    f = plt.figure()
    a = f.add_subplot(111)
    factor_list = [1, 2]
    factor_list = np.array(factor_list).astype(float)
    for neighbours, smoothness in neighbours_smoothness_list:
        interp_name = 'I{0}O{1}'.format(2*neighbours+2, 2*smoothness+1)
        err_max_list = []
        err_mean_list = []
        for factor in factor_list:
            factor = int(factor)
            c1 = DNS(simname = 'nsvep_{0}x_'.format(int(factor)) + interp_name)
            c2 = DNS(simname = 'nsvep_{0}x_'.format(int(factor)*2) + interp_name)
            for cc in [c1, c2]:
                cc.parameters['niter_part'] = cc.get_data_file()['parameters/niter_part'][()]
            c1.compute_statistics(iter0 = 8*factor)
            c2.compute_statistics(iter0 = 8*factor*2)
            final_iter_x1 = c1.get_data_file()['iteration'][()]
            final_iter_x2 = c2.get_data_file()['iteration'][()]
            f1 = h5py.File(c1.simname + '_checkpoint_0.h5', 'r')
            f2 = h5py.File(c2.simname + '_checkpoint_0.h5', 'r')
            x1 = f1['tracers0/state/{0}'.format(final_iter_x1)][()]
            x2 = f2['tracers0/state/{0}'.format(final_iter_x2)][()]
            diff = np.sqrt(np.sum((x2-x1)**2, axis = -1))
            err_max_list.append(np.max(diff))
            err_mean_list.append(np.mean(diff))
            f1.close()
            f2.close()
        err_max_list = np.array(err_max_list)
        err_mean_list = np.array(err_mean_list)
        a.plot(factor_list, err_max_list, marker = '.', label = 'max error ' + interp_name)
        a.plot(factor_list, err_mean_list, marker = '.', label = 'mean error ' + interp_name)
    for ee in [2, 3, 4]:
        a.plot(factor_list,
               (1e-4)*factor_list**(-ee),
               color = 'black',
               dashes = (ee, ee),
               label = '$\propto f^{{-{0}}}$'.format(ee))
    a.set_xscale('log')
    a.set_yscale('log')
    a.set_xlabel('resolution factor f')
    a.set_ylabel('absolute trajectory error [DNS units]')
    a.legend(loc = 'best', fontsize = 7)
    f.tight_layout()
    f.savefig('err_vs_dt_particle.pdf')
    f.savefig('err_vs_dt_particle.svg')
    plt.close(f)
    return None

def plot_traj_particles():
    ###########################################################################
    f = plt.figure()
    a = f.add_subplot(111)
    for neighbours, smoothness in neighbours_smoothness_list:
        interp_name = 'I{0}O{1}'.format(2*neighbours+2, 2*smoothness+1)
        for factor in [1, 2, 4]:
            cc = DNS(simname = 'nsvep_{0}x_'.format(factor) + interp_name)
            cc.compute_statistics(iter0 = 8*factor)
            tt = 3
            xx = read_trajectory(cc, tt, iter0 = 8*factor)
            a.plot(xx[:, 0],
                   xx[:, 1],
                   label = interp_name + '$f = {0}$'.format(factor),
                   marker = 'x',
                   dashes = (4/factor, 3, 4/factor),
                   alpha = 0.2)
            #a.plot(xx[:, 0], xx[:, 1], dashes = (4/factor, 3, 4/factor), alpha = 0.2)
    a.legend(loc = 'best', fontsize = 7)
    a.set_xlabel('$x$ [code units]')
    a.set_ylabel('$y$ [code units]')
    f.tight_layout()
    f.savefig('trajectories.pdf')
    f.savefig('trajectories.svg')
    plt.close(f)
    ###########################################################################
    ###########################################################################
    traj_index = 3
    for neighbours, smoothness in neighbours_smoothness_list:
        interp_name = 'I{0}O{1}'.format(2*neighbours+2, 2*smoothness+1)
        cc = DNS(simname = 'nsvep_base_' + interp_name)
        cc.compute_statistics()
        xx_base = read_trajectory(cc, traj_index, iter0 = 0, dset_name = 'velocity')
        iter0 = 16
        tt_base = np.array(range(iter0, iter0 + xx_base.shape[0]))*cc.parameters['dt']
        f = plt.figure()
        ax_counter = 1
        for factor in [1, 2, 4]:
            a = f.add_subplot(310 + ax_counter)
            ax_counter += 1
            a.plot(tt_base,
                   xx_base[:, 2],
                   label = 'base_' + interp_name,
                   marker = '+',
                   linewidth = 0.5,
                   alpha = 0.2)
            cc = DNS(simname = 'nsvep_{0}x_'.format(factor) + interp_name)
            cc.compute_statistics(iter0 = 8*factor)
            xx = read_trajectory(cc, traj_index, iter0 = 8*factor, dset_name = 'velocity')
            tt = np.array(range(8*factor, xx.shape[0]+8*factor))*cc.parameters['dt']
            a.plot(tt,
                   xx[:, 2],
                   label = interp_name + '$f = {0}$'.format(factor),
                   marker = 'x',
                   dashes = (4/factor, 3, 4/factor),
                   alpha = 0.2)
            df = h5py.File(cc.simname + '_checkpoint_0.h5', 'r')
            xx = df['tracers0/rhs/{0}'.format(8*factor)][()]
            for rhs_index in [1, 2, 3]:
                a.scatter((8*factor - rhs_index)*cc.parameters['dt'],
                          xx[rhs_index, traj_index, 2],
                          marker = '*',
                          zorder = -10,
                          alpha = 0.5,
                          color = 'black')
            df.close()
            a.legend(loc = 'best', fontsize = 7)
            a.set_xlabel('$t$ [code units]')
            a.set_ylabel('$v_y$ [code units]')
        f.tight_layout()
        f.savefig('velocities_{0}.pdf'.format(interp_name))
        plt.close(f)
    ###########################################################################
    return None

def read_trajectory(
        cc,
        traj_index,
        iter0,
        dset_name = 'position'):
    cc.parameters['niter_part'] = cc.get_data_file()['parameters/niter_part'][()]
    df = cc.get_particle_file()
    xx = []
    for ii in range(iter0, cc.get_data_file()['iteration'][()]+1, cc.parameters['niter_part']):
        xx.append(df['tracers0/' + dset_name + '/{0}'.format(ii)][traj_index])
    df.close()
    return np.array(xx)

if __name__ == '__main__':
    main()

