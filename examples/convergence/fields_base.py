import numpy as np
import h5py
import matplotlib.pyplot as plt

def main():
    return None

def get_velocity(
        cc,
        iteration = 0):
    data_file = h5py.File(
            cc.get_checkpoint_fname(iteration = iteration),
            'r')
    if 'velocity' in data_file.keys():
        if 'real' in data_file['velocity'].keys():
            vel = data_file['velocity/real/{0}'.format(iteration)][()]
        else:
            cvel = data_file['velocity/complex/{0}'.format(iteration)][()]
            vel = ift(cvel)
    else:
        assert('complex' in data_file['vorticity'].keys())
        cvort = data_file['vorticity/complex/{0}'.format(iteration)][()]
        cvel = compute_curl(cc, cvort, inverse_curl = True)
        vel = ift(cvel)
    data_file.close()
    return vel

def ift(cfield):
    ff = np.fft.irfftn(cfield, axes = (0, 1, 2))
    field = np.transpose(ff, (1, 0, 2, 3)).copy()
    return field*(field.shape[0]*field.shape[1]*field.shape[2])

def compute_curl(
        cc,
        cfield,
        inverse_curl = False):
    kx = cc.get_data_file()['kspace/kx'][()]
    ky = cc.get_data_file()['kspace/ky'][()]
    kz = cc.get_data_file()['kspace/kz'][()]
    ff = cfield.copy()
    ff[..., 0] = 1j*(ky[:, None, None]*cfield[..., 2] - kz[None, :, None]*cfield[..., 1])
    ff[..., 1] = 1j*(kz[None, :, None]*cfield[..., 0] - kx[None, None, :]*cfield[..., 2])
    ff[..., 2] = 1j*(kx[None, None, :]*cfield[..., 1] - ky[:, None, None]*cfield[..., 0])
    if inverse_curl:
        k2 = (kx[None, None,    :]**2 +
              ky[   :, None, None]**2 +
              kz[None,    :, None]**2)
        k2[0, 0, 0] = 1.
        ff /= k2[:, :, :, None]
    return ff

def compute_vector_field_distance(
        f1, f2,
        figname = None):
    diff = f1 - f2
    rms1 = np.mean(np.sum(f1**2, axis = 3))**0.5
    rms2 = np.mean(np.sum(f2**2, axis = 3))**0.5
    rms = np.sqrt(rms1 * rms2)
    dd = np.sum(diff**2, axis = 3)
    L2 = np.sqrt(np.mean(dd))
    max_val = np.max(np.sqrt(dd))
    if type(figname) != type(None):
        fig = plt.figure(figsize = (8, 4))
        a = fig.add_subplot(121)
        a.imshow(f1[:, 5, :, 2], vmin = -1, vmax = 1, interpolation = 'none')
        a = fig.add_subplot(122)
        a.imshow(f2[:, 5, :, 2], vmin = -1, vmax = 1, interpolation = 'none')
        fig.tight_layout()
        fig.savefig('field_slice_' + figname + '.pdf')
        plt.close(fig)
    return {'L2' : L2,
            'L2_rel' : L2 / rms,
            'max' : max_val,
            'max_rel' : max_val / rms}

if __name__ == '__main__':
    main()

