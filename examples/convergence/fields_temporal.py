import numpy as np
import h5py
import matplotlib.pyplot as plt

import TurTLE
from TurTLE import DNS, PP

from fields_base import *

base_niterations = 512
test_niterations = 32
grid_size = 128

def main():
    generate_initial_conditions()

    run_simulations_field(
            err_vs_t = False)
    plot_error_field(
            err_vs_t = False)
    return None

def generate_initial_conditions():
    # change these two values as needed.
    # number of MPI processes to use
    nprocesses = 8
    # number of OpenMP threads per MPI process to use
    nthreads_per_process = 1

# 1. Generate quasistationary state to use for initial conditions.
    # create a dns object
    c0 = DNS()
    # launch the simulation
    c0.launch([
            'NSVE',
            '-n', '{0}'.format(grid_size),
            '--np', '{0}'.format(nprocesses),
            '--ntpp', '{0}'.format(nthreads_per_process),
            '--precision', 'double',
            '--src-simname', 'B32p1e4',
            '--src-wd', TurTLE.data_dir,
            '--src-iteration', '0',
            '--simname', 'base',
            '--niter_todo', '{0}'.format(base_niterations),
            '--niter_out', '{0}'.format(base_niterations),
            '--overwrite-src-parameters',
            '--kMeta',  '1.5',
            '--fk0', '2',
            '--fk1', '3',
            '--niter_stat', '16',
            '--checkpoints_per_file', '{0}'.format(64)])

# 3. Generate initial conditions for NSE runs.
    # create postprocess object
    cc = PP()
    # launches code to compute velocity field at last iteration
    cc.launch([
            'get_velocity',
            '--np', '{0}'.format(nprocesses),
            '--ntpp', '{0}'.format(nthreads_per_process),
            '--simname', 'base',
            '--precision', 'double',
            '--iter0', '{0}'.format(base_niterations),
            '--iter1', '{0}'.format(base_niterations)])
    return None

def run_simulations_field(err_vs_t = False):
    # change these two values as needed.
    # number of MPI processes to use
    nprocesses = 8
    # number of OpenMP threads per MPI process to use
    nthreads_per_process = 1

    if err_vs_t:
        niter_out_factor = 1
    else:
        niter_out_factor = test_niterations

# 1. Run NSVE for the three resolutions.
    for factor in [1, 2, 4]:
        # create dns object
        cc = DNS()
        # launch simulation
        cc.launch([
                'NSVE',
                '-n', '{0}'.format(grid_size),
                '--np', '{0}'.format(nprocesses),
                '--ntpp', '{0}'.format(nthreads_per_process),
                '--src-simname', 'base',
                '--src-iteration', '{0}'.format(base_niterations),
                '--simname', 'nsve_{0}x'.format(factor),
                '--precision', 'double',
                '--dtfactor', '{0}'.format(0.5 / factor),
                '--niter_todo', '{0}'.format(test_niterations*factor),
                '--niter_out', '{0}'.format(niter_out_factor*factor),
                '--niter_stat', '{0}'.format(factor)])

# 2. Run NSE for the three resolutions.
    for factor in [1, 2, 4]:
        # create dns object
        cc = DNS()
        # launch simulation
        cc.launch([
                'NSE',
                '-n', '{0}'.format(grid_size),
                '--np', '{0}'.format(nprocesses),
                '--ntpp', '{0}'.format(nthreads_per_process),
                '--src-simname', 'base',
                '--src-iteration', '{0}'.format(base_niterations),
                '--simname', 'nse_{0}x'.format(factor),
                '--precision', 'double',
                '--dtfactor', '{0}'.format(0.5 / factor),
                '--niter_todo', '{0}'.format(test_niterations*factor),
                '--niter_out', '{0}'.format(niter_out_factor*factor),
                '--niter_out', '{0}'.format(test_niterations*factor),
                '--niter_stat', '{0}'.format(factor)])
    return None

def plot_error_field(err_vs_t = False):
    factor_list = [1, 2, 4]
    c0_list = [DNS(simname = 'nsve_{0}x'.format(factor))
               for factor in factor_list]
    c1_list = [DNS(simname = 'nse_{0}x'.format(factor))
               for factor in factor_list]
    cl = [c0_list, c1_list]
    # sanity checks
    for cc in c0_list + c1_list:
        cc.compute_statistics()

    # errors for individual solvers
    error_list = [[], [], []]
    for ii in range(len(factor_list)-1):
        factor = factor_list[ii]
        for jj in [0, 1]:
            vel1 = get_velocity(cl[jj][ii  ], iteration =   test_niterations*factor)
            vel2 = get_velocity(cl[jj][ii+1], iteration = 2*test_niterations*factor)
            dd = compute_vector_field_distance(vel1, vel2, figname = cl[jj][ii].simname + '_vs_' + cl[jj][ii+1].simname)
            error_list[jj].append(dd['L2_rel'])

    # comparisons of two solutions
    for ii in range(len(factor_list)):
        factor = factor_list[ii]
        vel1 = get_velocity(cl[0][ii], iteration = test_niterations*factor)
        vel2 = get_velocity(cl[1][ii], iteration = test_niterations*factor)
        dd = compute_vector_field_distance(vel1, vel2)
        error_list[2].append(dd['L2_rel'])

    f = plt.figure(figsize = (4, 4))
    a = f.add_subplot(111)
    a.plot(factor_list[:len(error_list[0])],
           error_list[0],
           marker = '.',
           dashes = (2, 3),
           label = 'NSVE')
    a.plot(factor_list[:len(error_list[1])],
           error_list[1],
           marker = '.',
           dashes = (3, 5),
           label = 'NSE')
    a.plot(factor_list,
           error_list[2],
           marker = '.',
           label = 'NSVE vs NSE')
    fl = np.array(factor_list).astype(float)
    for ee in [2, 3]:
        a.plot(fl[:2], error_list[0][0] * fl[:2]**(-ee),
               dashes = (ee, ee),
               color = 'black',
               label = '$\propto f^{{-{0}}}$'.format(ee),
               zorder = -ee)
    a.set_ylabel('relative error')
    a.set_xlabel('resolution factor $f$')
    a.set_xscale('log')
    a.set_yscale('log')
    a.legend(loc = 'best', fontsize = 6)
    f.tight_layout()
    f.savefig('err_vs_dt_field.pdf')
    f.savefig('err_vs_dt_field.svg')
    plt.close(f)

    if err_vs_t:
        t = range(test_niterations+1)
        err_vs_t = [[], [], []]
        # comparisons of two solvers
        for ii in range(len(factor_list)):
            factor = factor_list[ii]
            for tt in t:
                vel1 = get_velocity(cl[0][ii], iteration = tt*factor)
                vel2 = get_velocity(cl[1][ii], iteration = tt*factor)
                dd = compute_vector_field_distance(vel1, vel2)
                err_vs_t[ii].append(dd['L2_rel'])
        # distance between NSVE and NSE as a function of time
        f = plt.figure(figsize = (4, 4))
        a = f.add_subplot(111)
        a.plot(t, err_vs_t[0], label = 'f = 1')
        a.plot(t, err_vs_t[1], label = 'f = 2')
        a.plot(t, err_vs_t[2], label = 'f = 4')
        a.set_yscale('log')
        a.legend(loc = 'best', fontsize = 6)
        f.tight_layout()
        f.savefig('err_vs_t_field.pdf')
        f.savefig('err_vs_t_field.svg')
        plt.close(f)
    return None

if __name__ == '__main__':
    main()

