Recommendations for TurTLE projects
-----------------------------------

This document makes a few recommendations on using TurTLE for specific
projects.
The recommendations may be periodically updated, so please revisit it
regularly.

0. **Prerequisites**

   Before using TurTLE for production work, you should have passed an
   "Introduction to programming" class, and you should be familiar with
   basic concepts of numerical analysis (e.g. "numerical integration"
   and "FFT").
   If this is not the case, please discuss this point with your
   supervisor as soon as possible.

1. **Identify the relevant mathematical entities**.

   TurTLE is a fairly generic solver, mostly implementing basic
   functionality that is meant to be used for specific purposes (but the
   specific functionality must be implemented for individual projects).
   For example, numerical representations of scalar fields are
   available, as well as the capability of computing arbitrary
   expressions involving these scalar fields; however there is no
   "apply Laplacean operator" functionality available by default.

   To this end, it helps to clearly and succinctly state the equations to
   be solved, as well as the quantities that need to be measured from
   the solutions.

   If applicable, also state any and all analytic expressions that must be
   satisfied by the results.

1.5 **Communicate with developers**.

   The "mathematical summary" should be discussed with an experienced
   TurTLE user/developer.
   While we strive for the documentation to be informative, it is
   possible that the optimum way to use TurTLE for specific tasks is not
   obvious without a more thorough understanding of a majority of
   technical implementation details.

2. **Identify what code does what task**.

   It is very likely that a C++ code will not directly output the
   desired result, but rather some postprocessing (in a Python script)
   will be necessary.

   A document should be maintained that parallels the initial
   brief mathematical description.
   In this document, it should be clearly stated which mathematical
   quantity is computed by which component (i.e. which C++ class/file,
   or which Python script etc).
   Ideally this information is partially embedded in the source code
   itself (as documentation).

   Note the use of the word "maintained" in the above paragraph.
   It is recommended to first write a combination of
   C++/Python code that works correctly (and is documented as above).
   To the extent that performance requirements lead to subsequent
   modifications, the documentation should also be kept up to date.

3. **Test first**.

   Ideally there should be independent sanity checks performed on the
   data at all stages of the study.

   Most of the time, it will be possible to quickly run simulations in a
   "minimal parameter" regime.
   Minimal here refers to parameters that require minimal computing
   resources, while still resulting in somewhat meaningful results (i.e.
   the flow should be at least chaotic --- not laminar --- if ultimately
   one wants to study turbulence).
   It is recommended that the full "data pipeline" is tested for such
   parameter regimes, i.e. everything from running the C++ executable to
   generating figures to be ultimately used in publications.

4. **Prepare archiving solution**.

   Once the minimal parameter regime run is performed, it is recommended
   to extrapolate data storage requirements to the production parameter
   regime, and prepare beforehand for archiving and/or other data
   transfers.
   Notably, many HPC systems have limited scratch partition space
   available, and implement automatic removal of files older than a few
   weeks.

   In other words: (a) ensure that a location with enough free storage is
   available, (b) organize periodic data transfers (if the production jobs
   are expected to to overrun the available scratch partition space),
   (c) confirm with supervisor that your solution respects data archiving
   regulations.

5. **Track contributions**

   Ideally your project will end in a publication.
   During the related authorship/acknowledgement discussion it will help
   if you keep notes, during the project, of who contributed and how to
   the project.
   This is particularly relevant in the case when functionality is coded
   into outside repositories (i.e. not shown by `git log` executed in
   your project repository).
