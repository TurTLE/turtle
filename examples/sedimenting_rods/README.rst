Collision counter for sedimenting rods
--------------------------------------

Please see the PNAS publication
José-Agustı́n Arguedas-Leiva, Jonasz Słomka, Cristian C. Lalescu, Roman Stocker, and Michael Wilczek
*Elongation enhances encounter rates betweenin turbulence*

In this folder we include the code used to generate the results
presented in the paper above.
