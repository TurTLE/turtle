/******************************************************************************
*                                                                             *
*  Copyright 2019 Max Planck Institute for Dynamics and Self-Organization     *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLES_INNER_COMPUTER_SEDIMENTING_RODS_HPP
#define PARTICLES_INNER_COMPUTER_SEDIMENTING_RODS_HPP

#include <cstring>
#include <cassert>
#include <iostream>

template <class real_number, class partsize_t>
class particles_inner_computer_sedimenting_rods{
    bool isActive;
    const real_number D_rho, u_sed;
    const real_number beta0, beta1;
    const real_number lambda;
    const real_number lambda1;
    const real_number lambda2;
    const real_number lambda3;

public:
    explicit particles_inner_computer_sedimenting_rods(const real_number inLambda, const real_number inD_rho, const real_number inu_sed, const real_number in_beta0, const real_number in_beta1):
        isActive(true),
        D_rho(inD_rho),
        u_sed(inu_sed),
        beta0(in_beta0),
        beta1(in_beta1),
        lambda(inLambda),
        lambda1(0),
        lambda2(0),
        lambda3(0)
    {}

    template <int size_particle_positions, int size_particle_rhs>
    void compute_interaction(
            const partsize_t nb_particles,
            const real_number pos_part[],
            real_number rhs_part[]) const;
    // for given orientation and right-hand-side, recompute right-hand-side such
    // that it is perpendicular to the current orientation.
    // this is the job of the Lagrange multiplier terms, hence the
    // "add_Lagrange_multipliers" name of the method.
    template <int size_particle_positions, int size_particle_rhs>
    void add_Lagrange_multipliers(
            const partsize_t nb_particles,
            const real_number pos_part[],
            real_number rhs_part[]) const;
    template <int size_particle_positions, int size_particle_rhs, int size_particle_rhs_extra>
    void compute_interaction_with_extra(
            const partsize_t nb_particles,
            const real_number pos_part[],
            real_number rhs_part[],
            const real_number rhs_part_extra[]) const;
    // meant to be called AFTER executing the time-stepping operation.
    // once the particles have been moved, ensure that the orientation is a unit vector.
    template <int size_particle_positions>
    void enforce_unit_orientation(
            const partsize_t nb_particles,
            real_number pos_part[]) const;

    bool isEnable() const {
        return isActive;
    }

    void setEnable(const bool inIsActive) {
        isActive = inIsActive;
    }

    double get_sedimenting_velocity () {
        return u_sed;
    }

    void set_sedimenting_velocity (const double bla) {
        this->u_sed = bla;
    }

    void set_D_rho (const double DRHO) {
        this->D_rho = DRHO;
    }

    void set_beta0 (const double bla) {
        this->beta0 = bla;
    }

    void set_beta1 (const double bla) {
        this->beta1 = bla;
    }

    double get_D_rho () {
        return D_rho;
    }

    double get_beta0 () {
        return beta0;
    }
    double get_beta1 () {
        return beta1;
    }
};

#endif
