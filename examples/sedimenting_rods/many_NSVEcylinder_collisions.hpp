/**********************************************************************
*                                                                     *
*  Copyright 2020 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/

#ifndef many_NSVEcylinder_collisions_HPP
#define many_NSVEcylinder_collisions_HPP

#include <cstdlib>
#include "base.hpp"
#include "vorticity_equation.hpp"
#include "full_code/NSVE.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_output_hdf5.hpp"
#include "particles/particles_sampling.hpp"
#include "particles/p2p/p2p_computer.hpp"
#include "particles/p2p/p2p_ghost_collisions.hpp"

/** \brief Navier-Stokes solver that includes cylinders sedimenting in gravity with a ghost collision counter.
 *
 *  Child of Navier Stokes vorticity equation solver, this class calls all the
 *  methods from `NSVE`, and in addition integrates `sedimenting rods
 *  in the resulting velocity field.
 *  Sedimenting rods are very small particles with a density offset to the
 *  fluid flow. They have an orientation and are idealized as very thin ellongated
 *  ellipsoids, whose orientation evolves accordint to Jeffery's equations.
 */

template <typename rnumber>
class many_NSVEcylinder_collisions: public NSVE<rnumber>
{
    public:

        /* parameters that are read in read_parameters */
        std::vector<int> many_integration_steps;
        int niter_part;
        int niter_part_fine_period;//NEW
        int niter_part_fine_duration;//NEW
        std::vector<int> nparticles;
        int tracers0_integration_steps;
        int tracers0_neighbours;
        int tracers0_smoothness;
        std::vector<double> tracers0_cutoff;
        std::vector<double> tracers0_lambda;
        std::vector<double> tracers0_D_rho;
        std::vector<double> tracers0_u_sed;
        std::vector<double> tracers0_beta0;
        std::vector<double> tracers0_beta1;

        bool enable_p2p;
        bool enable_inner;
        bool enable_vorticity_omega;

        /* other stuff */
        std::vector<std::unique_ptr<abstract_particles_system_with_p2p<long long int, double, p2p_ghost_collisions<double, long long int>>>> many_ps;

        std::vector<particles_output_hdf5<long long int, double,6>*> particles_output_writer_mpi;
        std::vector<particles_output_sampling_hdf5<long long int, double, double, 3>*> particles_sample_writer_mpi;
        // field for sampling velocity gradient
        field<rnumber, FFTW, THREExTHREE> *nabla_u;

        hid_t cols_file;
        hid_t cols_file_PP;
        hid_t subset_file;

        many_NSVEcylinder_collisions(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE<rnumber>(
                    COMMUNICATOR,
                    simulation_name),
                    tracers0_cutoff(0.314),
                    tracers0_lambda(100.0),
                    tracers0_D_rho(1.0),
                    tracers0_u_sed(1.0),
                    enable_p2p(true),//interaction has to be turned off by hand in p2p_ghost_collision_base
                    enable_inner(true),
                    enable_vorticity_omega(true){}//PUT TRUE HERE AS IN NSVE
        ~many_NSVEcylinder_collisions(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        virtual int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
        
        std::vector <std::vector <long long int>> collision_pairs_old;
        std::vector <std::vector <long long int>> collision_pairs_new;
        std::vector <std::vector <long long int>> collision_pairs_same;
        std::vector <std::vector <long long int>> collision_pairs_time_0;
        std::vector <std::vector <long long int>> collision_pairs_time_1;
  
void reset_collision_lists(int index){
        this->collision_pairs_old[index].resize(0);
        this->collision_pairs_new[index].resize(0);
        this->collision_pairs_same[index].resize(0);
        }
        
};

#endif//many_NSVEcylinder_collisions_HPP
