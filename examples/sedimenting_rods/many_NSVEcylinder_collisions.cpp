/**********************************************************************
*                                                                     *
*  Copyright 2020 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/

#include <string>
#include <cmath>
#include "scope_timer.hpp"
#include "particles/particles_sampling.hpp"
#include "particles/p2p/p2p_computer.hpp"
#include "particles/inner/particles_inner_computer.hpp"
#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/p2p/p2p_ghost_collisions.hpp"

template <typename rnumber>
int many_NSVEcylinder_collisions<rnumber>::initialize(void)
{
    TIMEZONE("many_NSVEcylinder_collisions::initialize");
    this->NSVE<rnumber>::initialize();
    this->many_ps.resize(this->many_integration_steps.size());
    this->particles_output_writer_mpi.resize(this->many_integration_steps.size());
    this->particles_sample_writer_mpi.resize(this->many_integration_steps.size());

    // initialize particles
    //force inner term to true.
    //not sure why, since it's set to true in the constructor prototype
    this->enable_inner = true;

    for (unsigned int index = 0; index < this->many_ps.size(); index++)
    {
        DEBUG_MSG("in many_ps loop %d\n", index);
        p2p_ghost_collisions<double, long long int> p2p_cc;
        particles_inner_computer_sedimenting_rods<double, long long int> current_particles_inner_computer(this->tracers0_lambda[index], this->tracers0_D_rho[index], this->tracers0_u_sed[index], this->tracers0_beta0[index], this->tracers0_beta1[index]);
        current_particles_inner_computer.setEnable(enable_inner);

        DEBUG_MSG("Settling velocity is %g\n", current_particles_inner_computer.get_sedimenting_velocity() );
        DEBUG_MSG("D_rho is %g\n", current_particles_inner_computer.get_D_rho() );
        DEBUG_MSG("beta0 is %g\n", current_particles_inner_computer.get_beta0() );
        DEBUG_MSG("beta1 is %g\n", current_particles_inner_computer.get_beta1() );
        DEBUG_MSG("Is enable is %d\n", current_particles_inner_computer.isEnable() );

        this->many_ps[index] = Template_double_for_if::evaluate<std::unique_ptr<abstract_particles_system_with_p2p<long long int, double, p2p_ghost_collisions<double, long long int>>>,
                        int, 1, 11, 1, // interpolation_size
                        int, 0, 3, 1, // spline_mode
                        particles_system_build_container<long long int, rnumber,FFTW,THREE,double,
                                                            p2p_ghost_collisions<double, long long int>,
                                                            particles_inner_computer_sedimenting_rods<double,long long int>,
                                                            6,6>>(
                            this->tracers0_neighbours, // template iterator 1
                            this->tracers0_smoothness, // template iterator 2
                            this->fs->cvelocity,
                            this->fs->kk,
                            tracers0_integration_steps,
                            (long long int)nparticles[index],
                            this->get_current_fname(),
                            std::string("/tracers" + std::to_string(index) + "/state/") + std::to_string(this->iteration),
                            std::string("/tracers" + std::to_string(index) + "/rhs/") + std::to_string(this->iteration),
                            this->comm,
                            this->iteration+1,
                            std::move(p2p_cc),
                            std::move(current_particles_inner_computer),
                            this->tracers0_cutoff[index]);

        //DEBUG_MSG("Settling velocity is %g\n", this->ps->getInnerComputer().get_sedimenting_velocity() );
        //DEBUG_MSG("Is enable is %d\n", this->ps->getInnerComputer().isEnable() );

        if (this->tracers0_lambda[index]>1.0){
        this->many_ps[index]->getP2PComputer().setEnable(true);
        this->many_ps[index]->getP2PComputer().set_cylinder();
        this->many_ps[index]->getP2PComputer().set_cylinder_length(this->tracers0_cutoff[index]*(1.0-1.0/this->tracers0_lambda[index]));
        this->many_ps[index]->getP2PComputer().set_cylinder_width(this->tracers0_cutoff[index] / this->tracers0_lambda[index]);
        } else if (this->tracers0_lambda[index]==1.0){
        this->many_ps[index]->getP2PComputer().set_sphere();
        }

        // initialize output objects
        this->particles_output_writer_mpi[index] = new particles_output_hdf5<
            long long int, double, 6>(
                    MPI_COMM_WORLD,
                    "tracers" + std::to_string(index),
                    nparticles[index],
                    this->many_integration_steps[index]);
        this->particles_output_writer_mpi[index]->setParticleFileLayout(this->many_ps[index]->getParticleFileLayout());

    }

    /// allocate grad vel field
    this->nabla_u = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->fs->cvorticity->fftw_plan_rigor);
	/// open collisions file
    if (this->myrank == 0){
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting cols_file cache I got %d\n", cache_err);
        this->cols_file = H5Fopen(
                (this->simname + "_collisions.h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
        this->cols_file_PP = H5Fopen(
                (this->simname + "_collisions_PP.h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
        this->collision_pairs_old.resize(this->many_ps.size());
        this->collision_pairs_new.resize(this->many_ps.size());
        this->collision_pairs_same.resize(this->many_ps.size());
        this->collision_pairs_time_0.resize(this->many_ps.size());
        this->collision_pairs_time_1.resize(this->many_ps.size());
        for (unsigned int index = 0; index < this->many_ps.size(); index++)
            {
            DEBUG_MSG("index %d\n", index);
            reset_collision_lists(index);
            }
    }

    return EXIT_SUCCESS;
}

template <typename rnumber>
int many_NSVEcylinder_collisions<rnumber>::step(void)
{
    TIMEZONE("many_NSVEcylinder_collisions::step");
    this->fs->compute_velocity(this->fs->cvorticity);

    compute_gradient(
        this->fs->kk,
        this->fs->cvelocity,
        this->nabla_u);
    this->nabla_u->ift();
    this->fs->cvelocity->ift(); // needed before completeloop
    //std::unique_ptr<double[]> sampled_vorticity(new double[9*this->ps->getLocalNbParticles()]);
    //std::fill_n(sampled_vorticity.get(), 9*this->ps->getLocalNbParticles(), 0);
    //this->ps->sample_compute_field(*this->nabla_u, sampled_vorticity.get());
    //*this->tmp_vec_field = this->fs->cvorticity->get_cdata();
    //this->tmp_vec_field->ift();

    for (unsigned int index = 0; index < this->many_ps.size(); index++)
    {
        DEBUG_MSG(("step for tracers"+std::to_string(index)+"\n").c_str());
        this->many_ps[index]->getP2PComputer().reset_collision_pairs();
        DEBUG_MSG(("step for tracers"+std::to_string(index)+", after reset_collision_pairs\n").c_str());
        this->many_ps[index]->completeLoopWithExtraField(this->dt, *this->nabla_u);
        DEBUG_MSG(("step for tracers"+std::to_string(index)+", after completeLoopWithExtraField\n").c_str());
    }

    this->NSVE<rnumber>::step();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int many_NSVEcylinder_collisions<rnumber>::write_checkpoint(void)
{
    TIMEZONE("many_NSVEcylinder_collisions::write_checkpoint");
    this->NSVE<rnumber>::write_checkpoint();
    for (unsigned int index = 0; index < this->many_ps.size(); index++)
    {
        this->particles_output_writer_mpi[index]->open_file(this->fs->get_current_fname());
        // TODO P2P write particle data too
        this->particles_output_writer_mpi[index]->template save<6>(
                this->many_ps[index]->getParticlesState(),
                this->many_ps[index]->getParticlesRhs(),
                this->many_ps[index]->getParticlesIndexes(),
                this->many_ps[index]->getLocalNbParticles(),
                this->fs->iteration);
        this->particles_output_writer_mpi[index]->close_file();
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int many_NSVEcylinder_collisions<rnumber>::finalize(void)
{
    TIMEZONE("many_NSVEcylinder_collisions::finalize");
    if (this->myrank == 0){
         H5Fclose(this->cols_file);
         H5Fclose(this->cols_file_PP);
         }
    delete this->nabla_u;
    for (unsigned int index = 0; index < this->many_ps.size(); index++)
    {
        delete this->particles_output_writer_mpi[index];
    }
    this->NSVE<rnumber>::finalize();

    return EXIT_SUCCESS;
}

/** \brief Compute fluid stats and sample particle data.
 */

template <typename rnumber>
int many_NSVEcylinder_collisions<rnumber>::do_stats()
{
    bool save_pair;
    long long int indx_0_0, indx_0_1, indx_1_0, indx_1_1;
    TIMEZONE("many_NSVEcylinder_collisions::do_stats");
    /// perform fluid stats
    this->NSVE<rnumber>::do_stats();

    // compute velocity, copy to tmp field
    //this->fs->compute_velocity(this->fs->cvorticity);
    //*(this->tmp_vec_field) = *this->fs->cvelocity;

     /// sample velocity gradient
     /// fs->cvelocity should contain the velocity in Fourier space
     this->fs->compute_velocity(this->fs->cvorticity);
     compute_gradient(
             this->fs->kk,
             this->fs->cvelocity,
             this->nabla_u);
     this->nabla_u->ift();
     // Dont need acceleration in the moment //
     ///// compute acceleration and sample it
     //this->fs->compute_Lagrangian_acceleration(this->tmp_vec_field);
    // move velocity to real space
    this->fs->cvelocity->ift();

    // Carry out collision measurement
    if (this->enable_p2p) {
        for (unsigned int index = 0; index < this->many_ps.size(); index++)
        {
            /* DEBUG_MSG("Saving collisions at index %d\n",this->many_ps[index]->get_step_idx()-1); */
            /* Here the collisions are saved both at the end and at the beginning of a subsequent simulation. This produces an error message. */
            DEBUG_MSG(("merge for tracers"+std::to_string(index)+"\n").c_str());
            this->many_ps[index]->getP2PComputer().MPI_merge(
                    this->comm,
                    this->myrank,
                    this->nprocs);

            if (this->myrank == 0){
                /* Reset the collision lists old, same, and new */
                this->reset_collision_lists(index);
                /* Copy the collision list from the current time step onto collision_pairs_time_1 */
                this->collision_pairs_time_1[index] = this->many_ps[index]->getP2PComputer().get_collision_pairs(
                                this->comm,
                                this->myrank,
                                this->nprocs);
                /* Get the collisions that dissapeared in this time step and those that remain the same */
                for (unsigned int pair_index_0 = 0; pair_index_0 < this->collision_pairs_time_0[index].size()/2; pair_index_0++)
                    {
                    indx_0_0 = this->collision_pairs_time_0[index][pair_index_0*2];
                    indx_0_1 = this->collision_pairs_time_0[index][pair_index_0*2+1];
                    save_pair = true;
                    for (unsigned int pair_index_1 = 0; pair_index_1 < this->collision_pairs_time_1[index].size()/2; pair_index_1++)
                        {
                        indx_1_0 = this->collision_pairs_time_1[index][pair_index_1*2];
                        indx_1_1 = this->collision_pairs_time_1[index][pair_index_1*2+1];
                        if (indx_0_0==indx_1_0 and indx_0_1==indx_1_1)
                            {
                            save_pair = false;
                            this->collision_pairs_same[index].push_back(indx_0_0);
                            this->collision_pairs_same[index].push_back(indx_0_1);
                            break;
                            }
                        }
                    if (save_pair){
                    this->collision_pairs_old[index].push_back(indx_0_0);
                    this->collision_pairs_old[index].push_back(indx_0_1);
                    }
                }
                /* Get the new collisions from this time step */
                for (unsigned int pair_index_0 = 0; pair_index_0 < this->collision_pairs_time_1[index].size()/2; pair_index_0++)
                    {
                    indx_0_0 = this->collision_pairs_time_1[index][pair_index_0*2];
                    indx_0_1 = this->collision_pairs_time_1[index][pair_index_0*2+1];
                    save_pair = true;
                    for (unsigned int pair_index_1 = 0; pair_index_1 < this->collision_pairs_time_0[index].size()/2; pair_index_1++)
                        {
                        indx_1_0 = this->collision_pairs_time_0[index][pair_index_1*2];
                        indx_1_1 = this->collision_pairs_time_0[index][pair_index_1*2+1];
                        if (indx_0_0==indx_1_0 and indx_0_1==indx_1_1)
                            {
                            save_pair = false;
                            break;
                            }
                        }
                    if (save_pair){
                    this->collision_pairs_new[index].push_back(indx_0_0);
                    this->collision_pairs_new[index].push_back(indx_0_1);
                    }
                }
                DEBUG_MSG(("save cols for tracers"+std::to_string(index)+"\n").c_str());
                // this->cols_file is only defined for rank 0,
                // so only rank 0 can write the pairs.
                // this is also a reason for calling MPI_merge beforehand.
                std::string dset_name = (
                        "/collisions"
                         + std::to_string(index)
                         + "/pair_list_"
                         + std::to_string(this->many_ps[index]->get_step_idx()-1));
                // if the dataset already exists, it's because we're doing the statistics for
                // first iteration in current run. this is fine for spectra etc (what happens for example
                // in NSVE::do_stats), where the same element of some big array gets rewritten.
                // However, it's not fine for "write_particle_ID_pairs", because this method creates
                // a new dataset. Which raises an error when the dataset already exists.
                // Hence I chose to write the current data with "extra", for redundancy checks.
                // Although I assume the "extra" dataset will be empty since timestep has not run
                // and no collisions have been counted.
                if (H5Lexists(this->cols_file, dset_name.c_str(), H5P_DEFAULT))
                    dset_name += "_extra";
                hdf5_tools::write_particle_ID_pairs_with_single_rank<long long>(
                        this->collision_pairs_time_1[index],
                        this->cols_file,
                        dset_name.c_str());
                /* Save the new, old, and same collisions */
                DEBUG_MSG(("save new cols for tracers"+std::to_string(index)+"\n").c_str());
                dset_name = (
                        "/new_collisions"
                         + std::to_string(index)
                         + "/pair_list_"
                         + std::to_string(this->many_ps[index]->get_step_idx()-1));
                if (H5Lexists(this->cols_file, dset_name.c_str(), H5P_DEFAULT))
                    dset_name += "_extra";
                hdf5_tools::write_particle_ID_pairs_with_single_rank<long long>(
                        this->collision_pairs_new[index],
                        this->cols_file,
                        dset_name.c_str());
                hdf5_tools::write_particle_ID_pairs_with_single_rank<long long>(
                        this->collision_pairs_new[index],
                        this->cols_file_PP,
                        dset_name.c_str());
                DEBUG_MSG(("save old cols for tracers"+std::to_string(index)+"\n").c_str());
                dset_name = (
                        "/old_collisions"
                         + std::to_string(index)
                         + "/pair_list_"
                         + std::to_string(this->many_ps[index]->get_step_idx()-1));
                if (H5Lexists(this->cols_file, dset_name.c_str(), H5P_DEFAULT))
                    dset_name += "_extra";
                hdf5_tools::write_particle_ID_pairs_with_single_rank<long long>(
                        this->collision_pairs_old[index],
                        this->cols_file,
                        dset_name.c_str());
                DEBUG_MSG(("save same cols for tracers"+std::to_string(index)+"\n").c_str());
                dset_name = (
                        "/same_collisions"
                         + std::to_string(index)
                         + "/pair_list_"
                         + std::to_string(this->many_ps[index]->get_step_idx()-1));
                if (H5Lexists(this->cols_file, dset_name.c_str(), H5P_DEFAULT))
                    dset_name += "_extra";
                hdf5_tools::write_particle_ID_pairs_with_single_rank<long long>(
                        this->collision_pairs_same[index],
                        this->cols_file,
                        dset_name.c_str());
                DEBUG_MSG(("save stats for tracers"+std::to_string(index)+"\n").c_str());
                /* Copy the collision list from the current time step onto collision_pairs_time_0 */
                this->collision_pairs_time_0[index]=this->collision_pairs_time_1[index];
                DEBUG_MSG(("done saving stats for tracers"+std::to_string(index)+"\n").c_str());
            }

            {
                // this block for subsampling
                // declare temporary particle set for subset sampling
                DEBUG_MSG_WAIT(this->comm, "00 hello from subsample block\n");
                particle_set<6, 3, 2> psubset(
                        this->fs->cvelocity->rlayout,
                        this->dkx,
                        this->dky,
                        this->dkz,
                        this->tracers0_cutoff[index]);
                DEBUG_MSG_WAIT(this->comm, "01 hello from subsample block\n");

                // first extract unique IDs of colliding particles
                int nparticles_to_sample;
                std::vector<long long int> indices_to_sample;
                if (this->myrank == 0)
                {
                    indices_to_sample.resize(this->collision_pairs_new[index].size());
                    // direct copy
                    for (unsigned ii = 0; ii < indices_to_sample.size(); ii++)
                        indices_to_sample[ii] = this->collision_pairs_new[index][ii];
                    // sort in place
                    std::sort(indices_to_sample.begin(), indices_to_sample.end());
                    // remove adjacent duplicates
                    auto last = std::unique(indices_to_sample.begin(), indices_to_sample.end());
                    // clean up undefined values
                    indices_to_sample.erase(last, indices_to_sample.end());
                    // now indices_to_sample contains unique particle labels.
                    nparticles_to_sample = indices_to_sample.size();
                }
                DEBUG_MSG_WAIT(this->comm, "02 hello from subsample block\n");

                // tell all processes how many particles there are
                AssertMpi(MPI_Bcast(
                            &nparticles_to_sample,
                            1,
                            MPI_INT,
                            0,
                            this->comm));
                DEBUG_MSG_WAIT(this->comm, "03 hello from subsample block\n");
                if (nparticles_to_sample > 0)
                {
                    DEBUG_MSG_WAIT(this->comm, "hello, there are %d particles to sample\n", nparticles_to_sample);
                    // tell all processes which particles are in the subset
                    if (this->myrank != 0)
                        indices_to_sample.resize(nparticles_to_sample);
                    AssertMpi(MPI_Bcast(
                                &indices_to_sample.front(),
                                nparticles_to_sample,
                                MPI_LONG,
                                0,
                                this->comm));
                    // now initialize particle set
                    psubset.init_as_subset_of(
                            *this->many_ps[index],
                            indices_to_sample);
                    DEBUG_MSG_WAIT(this->comm, "hello, psubset has %ld particles out of %ld\n",
                            psubset.getLocalNumberOfParticles(),
                            psubset.getTotalNumberOfParticles());
                    // write labels of colliding particles
                    // open subset file only with rank 0
                    // START WARNING: subset file opens "colliding_particle_subset"
                    std::string file_name = this->simname + "_colliding_particle_subset.h5";
                    if (this->myrank == 0)
                        this->subset_file = H5Fopen(
                                file_name.c_str(),
                                H5F_ACC_RDWR,
                                H5P_DEFAULT);
                    else
                        this->subset_file = -1;
                    DEBUG_MSG_WAIT(this->comm, "psubset check 00\n");
                    hdf5_tools::gather_and_write_with_single_rank(
                            this->myrank,
                            0,
                            this->comm,
                            psubset.getParticleLabels(),
                            psubset.getLocalNumberOfParticles(),
                            this->subset_file,
                            "subset_tracers"+ std::to_string(index) + "/labels/" + std::to_string(this->iteration));
                    // write positions of colliding particles
                    DEBUG_MSG_WAIT(this->comm, "psubset check 01\n");
                    std::unique_ptr<double[]> position = psubset.extractFromParticleState(0, 3);
                    hdf5_tools::gather_and_write_with_single_rank(
                            this->myrank,
                            0,
                            this->comm,
                            position.get(),
                            psubset.getLocalNumberOfParticles()*3,
                            this->subset_file,
                            "subset_tracers"+ std::to_string(index) + "/position/" + std::to_string(this->iteration));
                    delete[] position.release();
                    // write orientations of colliding particles
                    DEBUG_MSG_WAIT(this->comm, "psubset check 02\n");
                    std::unique_ptr<double[]> orientation = psubset.extractFromParticleState(3, 6);
                    hdf5_tools::gather_and_write_with_single_rank(
                            this->myrank,
                            0,
                            this->comm,
                            orientation.get(),
                            psubset.getLocalNumberOfParticles()*3,
                            this->subset_file,
                            "subset_tracers"+ std::to_string(index) + "/orientation/" + std::to_string(this->iteration));
                    if (this->myrank == 0)
                        H5Fclose(this->subset_file);
                    DEBUG_MSG_WAIT(this->comm, "psubset check 03\n");
                    MPI_Barrier(this->comm);
                    // END WARNING: subset file "colliding_particle_subset"
                    delete[] orientation.release();
                    // START WARNING these calls use "colliding_particle_subset"
                    // now write velocity sample
                    psubset.writeSample(
                        this->fs->cvelocity,
                        this->simname + "_colliding_particle_subset.h5",
                        "subset_tracers"+ std::to_string(index),
                        "velocity",
                        this->iteration);
                    // now write gradient sample
                    psubset.writeSample(
                        this->nabla_u,
                        this->simname + "_colliding_particle_subset.h5",
                        "subset_tracers"+ std::to_string(index),
                        "velocity_gradient",
                        this->iteration);
                    // END WARNING
                }
                DEBUG_MSG_WAIT(this->comm, "04 hello from subsample block\n");
            }
        if (this->myrank == 0)
        {
            /* Reset the collision lists old, same, and new */
            this->reset_collision_lists(index);
        }
        }
    }

    /// check if particle stats should be performed now;
    /// if not, exit method.
    if (!(this->iteration % this->niter_part == 0))
    {
        return EXIT_SUCCESS;
    }


    DEBUG_MSG_WAIT(this->comm, "before sample write\n");
    for (unsigned int index = 0; index < this->many_ps.size(); index++)
    {
        this->particles_sample_writer_mpi[index] = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
            MPI_COMM_WORLD,
            this->many_ps[index]->getGlobalNbParticles(),
            (this->simname + "_particles.h5"),
            "tracers" + std::to_string(index),
            "position/0");

        this->particles_sample_writer_mpi[index]->setParticleFileLayout(this->many_ps[index]->getParticleFileLayout());
        DEBUG_MSG(("sampling tracers"+std::to_string(index)+"\n").c_str());
        /// allocate temporary data array
        /// initialize pdata0 with the positions, and pdata1 with the orientations
        std::unique_ptr<double[]> pdata0 = this->many_ps[index]->extractParticlesState(0, 3);
        std::unique_ptr<double[]> pdata1 = this->many_ps[index]->extractParticlesState(3, 6);
        std::unique_ptr<double[]> pdata2(new double[9*this->many_ps[index]->getLocalNbParticles()]);
        DEBUG_MSG(("sampling position for tracers"+std::to_string(index)+"\n").c_str());
        /// sample position
        this->particles_sample_writer_mpi[index]->template save_dataset<3>(
                "tracers" + std::to_string(index),
                "position",
                pdata0.get(), // we need to use pdata0.get() here, because it's 3D, whereas getParticlesState may give something else
                &pdata0,
                this->many_ps[index]->getParticlesIndexes(),
                this->many_ps[index]->getLocalNbParticles(),
                this->many_ps[index]->get_step_idx()-1);
        DEBUG_MSG(("sampling orientation for tracers"+std::to_string(index)+"\n").c_str());
        /// sample orientation
        this->particles_sample_writer_mpi[index]->template save_dataset<3>(
                "tracers" + std::to_string(index),
                "orientation",
                pdata0.get(),
                &pdata1,
                this->many_ps[index]->getParticlesIndexes(),
                this->many_ps[index]->getLocalNbParticles(),
                this->many_ps[index]->get_step_idx()-1);
        DEBUG_MSG(("sampling velocity for tracers"+std::to_string(index)+"\n").c_str());
        /// sample velocity
        /// from now on, we need to clean up data arrays before interpolation
        std::fill_n(pdata1.get(), 3*this->many_ps[index]->getLocalNbParticles(), 0);
        DEBUG_MSG(("filled pdata1 for tracers"+std::to_string(index)+"\n").c_str());
        this->many_ps[index]->sample_compute_field(*this->fs->cvelocity, pdata1.get());
        DEBUG_MSG(("sampled for tracers"+std::to_string(index)+"\n").c_str());
        this->particles_sample_writer_mpi[index]->template save_dataset<3>(
                "tracers" + std::to_string(index),
                "velocity",
                pdata0.get(),
                &pdata1,
                this->many_ps[index]->getParticlesIndexes(),
                this->many_ps[index]->getLocalNbParticles(),
                this->many_ps[index]->get_step_idx()-1);
        DEBUG_MSG(("sampling velocity gradient for tracers"+std::to_string(index)+"\n").c_str());
        std::fill_n(pdata2.get(), 9*this->many_ps[index]->getLocalNbParticles(), 0);
        this->many_ps[index]->sample_compute_field(*this->nabla_u, pdata2.get());
        this->particles_sample_writer_mpi[index]->template save_dataset<9>(
                "tracers" + std::to_string(index),
                "velocity_gradient",
                pdata0.get(),
                &pdata2,
                this->many_ps[index]->getParticlesIndexes(),
                this->many_ps[index]->getLocalNbParticles(),
                this->many_ps[index]->get_step_idx()-1);

    //Dont need accelaration at the moment.
//          std::fill_n(pdata1.get(), 3*this->many_ps[index]->getLocalNbParticles(), 0);
//         this->many_ps[index]->sample_compute_field(*this->tmp_vec_field, pdata1.get());
//         this->particles_sample_writer_mpi[index]->template save_dataset<3>(
//                 "tracers" + std::to_string(index),
//                 "acceleration",
//                 pdata0.get(),
//                 &pdata1,
//                 this->many_ps[index]->getParticlesIndexes(),
//                 this->many_ps[index]->getLocalNbParticles(),
//                 this->many_ps[index]->get_step_idx()-1);
        delete this->particles_sample_writer_mpi[index];
        // deallocate temporary data array
        delete[] pdata0.release();
        delete[] pdata1.release();
        delete[] pdata2.release();
    }
    DEBUG_MSG_WAIT(this->comm, "after sample write\n");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int many_NSVEcylinder_collisions<rnumber>::read_parameters(void)
{
    TIMEZONE("many_NSVEcylinder_collisions::read_parameters");
    this->NSVE<rnumber>::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->niter_part = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part");
    DEBUG_MSG("Loading nparticles");
    this->nparticles = hdf5_tools::read_vector<int>(
            parameter_file,
            "parameters/nparticles");
    //this->nparticles = hdf5_tools::read_value<int>(parameter_file,"parameters/nparticles");
    this->tracers0_integration_steps = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_integration_steps");
    this->tracers0_neighbours = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_neighbours");
    this->tracers0_smoothness = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_smoothness");
    this->enable_p2p = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_enable_p2p");
    this->enable_inner = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_enable_inner");
    int tval = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_enable_vorticity_omega");
    this->enable_vorticity_omega = tval;
    DEBUG_MSG("tracers0_enable_vorticity_omega = %d, this->enable_vorticity_omega = %d\n",
              tval, this->enable_vorticity_omega);
    DEBUG_MSG("Loading tracers0_cutoff \n");
    this->tracers0_cutoff = hdf5_tools::read_vector<double>(
            parameter_file,
            "parameters/tracers0_cutoff");
    DEBUG_MSG("Loading tracers0_lambda \n");
    this->tracers0_lambda = hdf5_tools::read_vector<double>(
            parameter_file,
            "parameters/tracers0_lambda");
    DEBUG_MSG("Loading tracers0_D_rho \n");
    this->tracers0_D_rho = hdf5_tools::read_vector<double>(
            parameter_file,
            "parameters/tracers0_D_rho");
    DEBUG_MSG("Loading many_integration_steps \n");
    this->many_integration_steps = hdf5_tools::read_vector<int>(
            parameter_file,
            "parameters/many_integration_steps");
    DEBUG_MSG("Loading tracers0_u_sed \n");
    this->tracers0_u_sed = hdf5_tools::read_vector<double>(
            parameter_file,
            "parameters/tracers0_u_sed");
    DEBUG_MSG("Loading tracers0_beta0 \n");
    this->tracers0_beta0 = hdf5_tools::read_vector<double>(
            parameter_file,
            "parameters/tracers0_beta0");
    DEBUG_MSG("Loading tracers0_beta1 \n");
    this->tracers0_beta1 = hdf5_tools::read_vector<double>(
            parameter_file,
            "parameters/tracers0_beta1");
    this->niter_part_fine_period = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_period");
    this->niter_part_fine_duration = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_duration");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    assert(this->many_integration_steps.size() == this->tracers0_D_rho.size());
    return EXIT_SUCCESS;
}


template class many_NSVEcylinder_collisions<float>;
template class many_NSVEcylinder_collisions<double>;
