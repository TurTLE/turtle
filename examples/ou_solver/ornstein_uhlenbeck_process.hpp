#ifndef ORNSTEIN_UHLENBECK_PROCESS_HPP
#define ORNSTEIN_UHLENBECK_PROCESS_HPP

#include "field.hpp"

#include <sys/stat.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <random>

extern int myrank, nprocs;

template <typename rnumber, field_backend be>
class ornstein_uhlenbeck_process{
    public:
        char name[256];

        int iteration;
        double ou_kmin_squ;
        double ou_kmax_squ;
        double ou_energy_amplitude;
        double ou_gamma_factor;
        double kolmogorov_constant = 2;
        double epsilon;

        field<rnumber,be,THREE> *ou_field;
        field<rnumber,be,THREE> *ou_field_vort;
        field<rnumber,be,THREExTHREE> *B;
        kspace<be,SMOOTH> *kk;

        std::vector<std::mt19937_64> gen;
        std::normal_distribution<double> d{0,1};

        ornstein_uhlenbeck_process(
            const char *NAME,
            int nx,
            int ny,
            int nz,
            double ou_kmin,
            double ou_kmax,
            double ou_energy_amplitude,
            double ou_gamma_factor,
            double DKX = 1.0,
            double DKY = 1.0,
            double DKZ = 1.0,
            unsigned FFTW_PLAN_RIGOR = FFTW_MEASURE);
        ~ornstein_uhlenbeck_process(void);

        void step(double dt);
        inline double energy(double kabs)
        {
            return this->ou_energy_amplitude * pow(kabs,-5./3);
        }

        inline double gamma(double kabs)
        {
            return this->ou_gamma_factor*pow(kabs,2./3.)*sqrt(this->ou_energy_amplitude);
        }

        void initialize_B(void);

        void setup_field(int iteration,field<rnumber, be, THREE> *src);

        void add_to_field_replace(
          field<rnumber,be,THREE> *src, std::string uv);

        void strip_from_field(
          field<rnumber, be, THREE> *src);

        void set_from_field(
          field<rnumber, be, THREE> *src);

        void calc_ou_vorticity(void);
        void calc_ou_velocity(void);


};


#endif
