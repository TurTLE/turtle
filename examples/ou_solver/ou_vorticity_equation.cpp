//#include "ou_vorticity_equation.hpp"
#include "scope_timer.hpp"
#include <cmath>
#include <cstring>
#include <cassert>


template <class rnumber, field_backend be>
ou_vorticity_equation<rnumber,be>::~ou_vorticity_equation()
{
  delete this->ou;
}

template <class rnumber,
          field_backend be>
void ou_vorticity_equation<rnumber, be>::step(double dt)
{
    DEBUG_MSG("vorticity_equation::step\n");
    TIMEZONE("vorticity_equation::step");
    this->ou->add_to_field_replace(this->cvorticity, "vorticity");
    *this->v[1] = 0.0;
    this->omega_nonlin(0);
    this->kk->CLOOP(
                [&](ptrdiff_t cindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex,
                    double k2){
        if (k2 <= this->kk->kM2)
        {
            double factor0;
            factor0 = exp(-this->nu * k2 * dt);
            for (int cc=0; cc<3; cc++) for (int i=0; i<2; i++)
                this->v[1]->cval(cindex,cc,i) = (
                        this->v[0]->cval(cindex,cc,i) +
                        dt*this->u->cval(cindex,cc,i))*factor0;
        }
    }
    );

    this->omega_nonlin(1);
    this->kk->CLOOP(
                [&](ptrdiff_t cindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex,
                    double k2){
        if (k2 <= this->kk->kM2)
        {
            double factor0, factor1;
            factor0 = exp(-this->nu * k2 * dt/2);
            factor1 = exp( this->nu * k2 * dt/2);
            for (int cc=0; cc<3; cc++) for (int i=0; i<2; i++)
                this->v[2]->cval(cindex, cc, i) = (
                        3*this->v[0]->cval(cindex,cc,i)*factor0 +
                        ( this->v[1]->cval(cindex,cc,i) +
                         dt*this->u->cval(cindex,cc,i))*factor1)*0.25;
        }
    }
    );

    this->omega_nonlin(2);
    // store old vorticity
    *this->v[1] = *this->v[0];
    this->kk->CLOOP(
                [&](ptrdiff_t cindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex,
                    double k2){
        if (k2 <= this->kk->kM2)
        {
            double factor0;
            factor0 = exp(-this->nu * k2 * dt * 0.5);
            for (int cc=0; cc<3; cc++) for (int i=0; i<2; i++)
                this->v[3]->cval(cindex,cc,i) = (
                        this->v[0]->cval(cindex,cc,i)*factor0 +
                        2*(this->v[2]->cval(cindex,cc,i) +
                           dt*this->u->cval(cindex,cc,i)))*factor0/3;
        }
    }
    );

    this->kk->template force_divfree<rnumber>(this->cvorticity->get_cdata());
    this->cvorticity->symmetrize();
    this->iteration++;
}


template <class rnumber, field_backend be>
void ou_vorticity_equation<rnumber, be>::omega_nonlin(int src)
{
  DEBUG_MSG("ou_vorticity_equation::omega_nonlin(%d)\n", src);
  assert(src >= 0 && src < 3);
  this->compute_velocity(this->v[src]);

  this->add_ou_forcing_velocity();
  // TIME STEP TODO

  /* get fields from Fourier space to real space */
  this->u->ift();
  this->rvorticity->real_space_representation = false;
  *this->rvorticity = this->v[src]->get_cdata();
  this->rvorticity->ift();
  /* compute cross product $u \times \omega$, and normalize */
  this->u->RLOOP(
              [&](ptrdiff_t rindex,
                  ptrdiff_t xindex,
                  ptrdiff_t yindex,
                  ptrdiff_t zindex){
      rnumber tmp[3];
      for (int cc=0; cc<3; cc++)
          tmp[cc] = (this->u->rval(rindex,(cc+1)%3)*this->rvorticity->rval(rindex,(cc+2)%3) -
                     this->u->rval(rindex,(cc+2)%3)*this->rvorticity->rval(rindex,(cc+1)%3));
      for (int cc=0; cc<3; cc++)
          this->u->rval(rindex,cc) = tmp[cc] / this->u->npoints;
  }
  );
  /* go back to Fourier space */
  //this->clean_up_real_space(this->ru, 3);
  this->u->dft();
  this->kk->template dealias<rnumber, THREE>(this->u->get_cdata());
  /* $\imath k \times Fourier(u \times \omega)$ */
  this->kk->CLOOP(
              [&](ptrdiff_t cindex,
                  ptrdiff_t xindex,
                  ptrdiff_t yindex,
                  ptrdiff_t zindex){
      rnumber tmp[3][2];
      {
          tmp[0][0] = -(this->kk->ky[yindex]*this->u->cval(cindex,2,1) - this->kk->kz[zindex]*this->u->cval(cindex,1,1));
          tmp[1][0] = -(this->kk->kz[zindex]*this->u->cval(cindex,0,1) - this->kk->kx[xindex]*this->u->cval(cindex,2,1));
          tmp[2][0] = -(this->kk->kx[xindex]*this->u->cval(cindex,1,1) - this->kk->ky[yindex]*this->u->cval(cindex,0,1));
          tmp[0][1] =  (this->kk->ky[yindex]*this->u->cval(cindex,2,0) - this->kk->kz[zindex]*this->u->cval(cindex,1,0));
          tmp[1][1] =  (this->kk->kz[zindex]*this->u->cval(cindex,0,0) - this->kk->kx[xindex]*this->u->cval(cindex,2,0));
          tmp[2][1] =  (this->kk->kx[xindex]*this->u->cval(cindex,1,0) - this->kk->ky[yindex]*this->u->cval(cindex,0,0));
      }
      for (int cc=0; cc<3; cc++) for (int i=0; i<2; i++)
          this->u->cval(cindex, cc, i) = tmp[cc][i];
  }
  );
  this->kk->template force_divfree<rnumber>(this->u->get_cdata());
  this->u->symmetrize();
}

template <class rnumber, field_backend be>
void ou_vorticity_equation<rnumber, be>::add_ou_forcing_velocity()
{
  if (this->ou_forcing_type == "replace"){
    this->ou->add_to_field_replace(this->u,"velocity");
  }
}

template <class rnumber, field_backend be>
void ou_vorticity_equation<rnumber, be>::add_ou_forcing_vorticity()
{
  if (this->ou_forcing_type == "replace"){
    this->ou->add_to_field_replace(this->cvorticity,"vorticity");
  }
}

template class ou_vorticity_equation<float, FFTW>;
template class ou_vorticity_equation<double, FFTW>;
