################################################################################
#                                                                              #
#  Copyright 2015-2019 Max Planck Institute for Dynamics and Self-Organization #
#                                                                              #
#  This file is part of TurTLE.                                                #
#                                                                              #
#  TurTLE is free software: you can redistribute it and/or modify              #
#  it under the terms of the GNU General Public License as published           #
#  by the Free Software Foundation, either version 3 of the License,           #
#  or (at your option) any later version.                                      #
#                                                                              #
#  TurTLE is distributed in the hope that it will be useful,                   #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>              #
#                                                                              #
# Contact: Cristian.Lalescu@ds.mpg.de                                          #
#                                                                              #
################################################################################



import os
import argparse

import pathlib
cpp_location = pathlib.Path(__file__).parent.resolve()

import TurTLE
from TurTLE import TEST

class aTEST(TEST):
    def write_src(
            self):
        self.version_message = (
                '/***********************************************************************\n' +
                '* this code automatically generated by TurTLE\n' +
                '* version {0}\n'.format(TurTLE.__version__) +
                '***********************************************************************/\n\n\n')
        self.include_list = [
                '"full_code/main_code.hpp"',
                '<cmath>']
        self.main = """
            int main(int argc, char *argv[])
            {{
                bool fpe = (
                    (getenv("BFPS_FPE_OFF") == nullptr) ||
                    (getenv("BFPS_FPE_OFF") != std::string("TRUE")));
                return main_code< {0} >(argc, argv, fpe);
            }}
            """.format(self.dns_type + '<{0}>'.format(self.C_field_dtype))
        self.includes = '\n'.join(
                ['#include ' + hh
                 for hh in self.include_list])
        self.name = 'ornstein_uhlenbeck_process'
        self.dns_type = 'ornstein_uhlenbeck_process'
        self.definitions = ''
        self.definitions += open(
            os.path.join(
                cpp_location, 'ornstein_uhlenbeck_process.hpp'), 'r').read()
        self.definitions += open(
            os.path.join(
                cpp_location, 'ornstein_uhlenbeck_process.cpp'), 'r').read()
        self.definitions += open(
            os.path.join(
                cpp_location, 'ou_vorticity_equation.hpp'), 'r').read()
        self.definitions += open(
            os.path.join(
                cpp_location, 'ou_vorticity_equation.cpp'), 'r').read()
        with open(self.name + '.cpp', 'w') as outfile:
            outfile.write(self.version_message + '\n\n')
            outfile.write(self.includes + '\n\n')
            outfile.write(self.definitions + '\n\n')
            outfile.write(self.main + '\n')
        return None
    def add_parser_arguments(
            self,
            parser):
        subparsers = parser.add_subparsers(
                dest = 'TEST_class',
                help = 'type of simulation to run')
        subparsers.required = True
        parser_ornstein_uhlenbeck_test = subparsers.add_parser(
                'ornstein_uhlenbeck_process',
                help = 'test ornstein uhlenbeck process')

        for parser in ['ornstein_uhlenbeck_test']:
            eval('self.simulation_parser_arguments(parser_' + parser + ')')
            eval('self.job_parser_arguments(parser_' + parser + ')')
            eval('self.parameters_to_parser_arguments(parser_' + parser + ')')
            eval('self.parameters_to_parser_arguments(parser_' + parser + ', self.generate_extra_parameters(dns_type = \'' + parser + '\'))')
        return None

def main():
    aa = aTEST()
    aa.launch(
            ['ornstein_uhlenbeck_process'])
    return None

if __name__ == '__main__':
    main()
