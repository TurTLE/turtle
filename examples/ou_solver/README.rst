Fluid solver with forcing coming from Ornstein-Uhlenbeck process.

Note: it's very likely that this algorithm does not do what was
originally intended, since the expression of the forcing is different
in the velocity and vorticity formulations of the NS equations.
The code is nevertheless kept here for future reference.

TODO:

* fix compilation

* check that code runs
