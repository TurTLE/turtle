Sanity check for Lagrangian acceleration
========================================

This is a system test: the Lagrangian acceleration computed by NSVE is
compared with the one computed by NSE.
