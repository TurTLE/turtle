/******************************************************************************
*                                                                             *
*  Copyright 2024 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef ACCELERATION_CHECK_HPP
#define ACCELERATION_CHECK_HPP

/** \brief Error analysis for fluid solver.
 */

#include "full_code/NSVE_field_stats.hpp"

template <typename rnumber>
class acceleration_check: public NSVE_field_stats<rnumber>
{
    public:
        acceleration_check(
                const MPI_Comm COMMUNICATOR,
                const std::string &simname):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simname)
        {}
        ~acceleration_check(){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};

#endif//ACCELERATION_CHECK_HPP

