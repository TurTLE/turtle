/******************************************************************************
*                                                                             *
*  Copyright 2024 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/


//#include "acceleration_check.hpp"
#include "scope_timer.hpp"
#include "shared_array.hpp"
#include "fftw_tools.hpp"

#include <string>
#include <cmath>
#include <filesystem>

template <typename rnumber>
int acceleration_check<rnumber>::initialize(void)
{
    TIMEZONE("acceleration_check::intialize");
    this->read_parameters();
    this->NSVE_field_stats<rnumber>::initialize();
    const int read_iteration_result = this->read_iteration_list(
            this->simname + std::string("_post.h5"),
            "acceleration_check");
    assert(read_iteration_result == EXIT_SUCCESS);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int acceleration_check<rnumber>::finalize(void)
{
    TIMEZONE("acceleration_check::finalize");
    this->NSVE_field_stats<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_components fc>
int get_error(
        field<rnumber, FFTW, fc>* a,
        field<rnumber, FFTW, fc>* b,
        double& max_diff,
        double& L2norm)
{
    max_diff = 0;
    L2norm = 0;
    shared_array<double> L2norm_threaded(
            1,
            [&](double *val_tmp){
                *val_tmp = double(0.0);
            });
    shared_array<double> max_diff_threaded(
            1,
            [&](double *val_tmp){
                *val_tmp = double(0.0);
            });
    switch (fc) {
        case THREE:
            {
            a->RLOOP(
                [&](ptrdiff_t rindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex){
                for (auto cc = 0; cc < 3; cc++) {
                    const double difference = std::abs(
                            a->rval(rindex, cc) - b->rval(rindex, cc));
                    double *max_diff = max_diff_threaded.getMine();
                    double *L2norm   = L2norm_threaded.getMine();
                    *max_diff = std::max(*max_diff, difference);
                    *L2norm += std::sqrt(a->rval(rindex, cc)
                                       * a->rval(rindex, cc));
            }});
            }
            break;
        case ONE:
            {
            a->RLOOP(
                [&](ptrdiff_t rindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex){
                const double difference = std::abs(
                        a->rval(rindex)
                      - b->rval(rindex));
                double *max_diff = max_diff_threaded.getMine();
                double *L2norm   = L2norm_threaded.getMine();
                *max_diff = std::max(*max_diff, difference);
                *L2norm += (a->rval(rindex) * a->rval(rindex));
                });
            }
            break;
        }

    L2norm_threaded.mergeParallel();
    max_diff_threaded.mergeParallel(
            [&](const int idx, const double& v1, const double& v2) -> double {
                return std::max(v1, v2);
            });

    MPI_Allreduce(
            L2norm_threaded.getMasterData(),
            &L2norm,
            1,
            MPI_DOUBLE,
            MPI_SUM,
            a->comm);
    MPI_Allreduce(
            max_diff_threaded.getMasterData(),
            &max_diff,
            1,
            MPI_DOUBLE,
            MPI_MAX,
            a->comm);
    L2norm = std::sqrt(L2norm / (ncomp(fc)*a->npoints));
    return EXIT_SUCCESS;
}

template <typename rnumber>
int acceleration_check<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("acceleration_check::work_on_current_iteration");
    DEBUG_MSG("work on current iteration, iteration is %d\n", this->iteration);

    // temporary fields
    field<rnumber, FFTW, ONE>* pressure_NSE = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    field<rnumber, FFTW, THREE>* acc_NSE = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    field<rnumber, FFTW, ONE>* pressure_NSVE = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    field<rnumber, FFTW, THREE>* acc_NSVE = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->read_current_cvorticity();

    this->vorticity->symmetrize();
    NSE<rnumber>* nse = new NSE<rnumber>(this->comm, this->simname);
    this->copy_parameters_into(nse);
    nse->allocate();
    // we're reading vorticity other than through vorticity_equation,
    // so we have to apply divfree.
    // otherwise the energy in the forcing band is computed incorrectly
    nse->kk->template force_divfree<rnumber>(this->vorticity->get_cdata());
    invert_curl(
        nse->kk,
        this->vorticity,
        nse->velocity);

    NSVE<rnumber>* nsve = new NSVE<rnumber>(this->comm, this->simname);
    this->copy_parameters_into(nsve);
    nsve->allocate();
    *(nsve->fs->cvorticity) = this->vorticity->get_cdata();

    double acceleration_max_diff, pressure_max_diff;
    double acceleration_L2norm, pressure_L2norm;

    /***************************/
    // pressure
    // NSE
    nse->get_cpressure(pressure_NSE);
    // NSVE
    nsve->fs->compute_velocity(nsve->fs->cvorticity);
    nsve->fs->cvelocity->ift();
    nsve->fs->compute_pressure(pressure_NSVE);
    pressure_NSE->ift();
    pressure_NSVE->ift();
    //pressure_NSE->io("field_test.h5",
    //                  "nse",
    //                  this->iteration,
    //                  false);
    //pressure_NSVE->io("field_test.h5",
    //                  "nsve",
    //                  this->iteration,
    //                  false);
    get_error(
        pressure_NSE,
        pressure_NSVE,
        pressure_max_diff,
        pressure_L2norm);
    const bool pressure_ok = (pressure_max_diff / pressure_L2norm) < 1e-3;
    //pressure_NSE->dft();
    //pressure_NSE->io("field_test.h5",
    //                  "nse",
    //                  this->iteration,
    //                  false);
    //pressure_NSVE->dft();
    //pressure_NSVE->io("field_test.h5",
    //                  "nsve",
    //                  this->iteration,
    //                  false);
    /***************************/

    /***************************/
    // fixed energy injection rate
    // NSE
    nse->forcing_type = "fixed_energy_injection_rate";
    DEBUG_MSG("calling acceleration for NSE\n");
    nse->get_cacceleration(acc_NSE);
    // NSVE
    nsve->fs->forcing_type = "fixed_energy_injection_rate";
    nsve->fs->compute_Lagrangian_acceleration(acc_NSVE);
    acc_NSE->ift();
    acc_NSVE->ift();
    get_error(
        acc_NSE,
        acc_NSVE,
        acceleration_max_diff,
        acceleration_L2norm);

    const double err_max_feir = (acceleration_max_diff / acceleration_L2norm);
    const bool acceleration_ok_feir = err_max_feir < 1e-3;
    /***************************/
    // linear
    // NSE
    nse->forcing_type = "linear";
    nse->get_cacceleration(acc_NSE);
    // NSVE
    nsve->fs->forcing_type = "linear";
    nsve->fs->compute_Lagrangian_acceleration(acc_NSVE);
    acc_NSE->ift();
    acc_NSVE->ift();
    get_error(
        acc_NSE,
        acc_NSVE,
        acceleration_max_diff,
        acceleration_L2norm);

    const double err_max_linear = (acceleration_max_diff / acceleration_L2norm);
    const bool acceleration_ok_linear = err_max_linear < 1e-2;

    delete nsve;
    delete nse;
    delete acc_NSE;
    delete acc_NSVE;
    delete pressure_NSE;
    delete pressure_NSVE;

    if (acceleration_ok_feir && acceleration_ok_linear && pressure_ok)
        return EXIT_SUCCESS;
    else {
        DEBUG_MSG("pressure_ok = %d, max_error = %g\n",
                pressure_ok, pressure_max_diff);
        DEBUG_MSG("acceleration_ok_feir = %d, max error = %g\n",
                acceleration_ok_feir, err_max_feir);
        DEBUG_MSG("acceleration_ok_linear = %d, max error = %g\n",
                acceleration_ok_linear, err_max_linear);
        return EXIT_FAILURE;
    }
}

template class acceleration_check<float>;
template class acceleration_check<double>;

