/******************************************************************************
*                                                                             *
*  Copyright 2024 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/


//#include "fluid_solver_check.hpp"
#include "scope_timer.hpp"
#include "shared_array.hpp"
#include "fftw_tools.hpp"
#include "full_code/NSVE.hpp"
#include "full_code/NSE.hpp"

#include <string>
#include <cmath>
#include <filesystem>

template <typename rnumber>
int fluid_solver_check<rnumber>::initialize(void)
{
    TIMEZONE("fluid_solver_check::intialize");
    this->nx = 32;
    this->ny = 32;
    this->nz = 32;
    this->dkx = 1.0;
    this->dky = 1.0;
    this->dkz = 1.0;
    this->dealias_type = SMOOTH;

    this->spectrum_dissipation = 0.4;
    this->spectrum_Lint = 1.3;
    this->spectrum_etaK = 0.3;
    this->spectrum_large_scale_const = 6.78;
    this->spectrum_small_scale_const = 0.40;
    this->random_seed = 2;

    this->dt                   = 0.1;
    this->famplitude           = 0.5;
    this->friction_coefficient = 0.1;
    this->fk0                  = 1.4;
    this->fk1                  = 2.3;
    this->energy               = 0.5;
    this->injection_rate       = 0.4;
    this->variation_strength   = 0.25;
    this->variation_time_scale = 1.0;
    this->fmode                = 2;
    this->forcing_type         = "linear";
    this->nu                   = 0.2;

    return EXIT_SUCCESS;
}

template <typename rnumber>
int fluid_solver_check<rnumber>::finalize(void)
{
    TIMEZONE("fluid_solver_check::finalize");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int check_solvers(
        NSE<rnumber>*  nse,
        NSVE<rnumber>* nsve)
{
    double energy_NSE,
           energy_NSVE;
    assert(nse->get_nx() == nsve->get_nx());
    assert(nse->get_ny() == nsve->get_ny());
    assert(nse->get_nz() == nsve->get_nz());

    nse->print_debug_info();
    nsve->print_debug_info();

    const ptrdiff_t cindex = 2;
    // fix initial conditions
    *(nse->velocity) = 0.0;
    nse->velocity->real_space_representation = false;
    if (nse->myrank == 0) {
        // impose yamplitude for mode k_x=2
        // yindex = 0
        // zindex = 0
        // xindex = 2
        // cindex = (yindex * nz + zindex)*nx + xindex
        nse->velocity->cval(cindex, 1, 0) = 1.0;
    }
    compute_curl(
        nse->kk,
        nse->velocity,
        nsve->fs->cvorticity);
    nsve->fs->compute_velocity(nsve->fs->cvorticity);

    energy_NSE = 0.5*(
            nse->velocity->cval(cindex, 0, 0)*nse->velocity->cval(cindex, 0, 0)
          + nse->velocity->cval(cindex, 1, 0)*nse->velocity->cval(cindex, 1, 0)
          + nse->velocity->cval(cindex, 2, 0)*nse->velocity->cval(cindex, 2, 0)
          + nse->velocity->cval(cindex, 0, 1)*nse->velocity->cval(cindex, 0, 1)
          + nse->velocity->cval(cindex, 1, 1)*nse->velocity->cval(cindex, 1, 1)
          + nse->velocity->cval(cindex, 2, 1)*nse->velocity->cval(cindex, 2, 1));
    energy_NSVE = 0.5*(
            nsve->fs->cvelocity->cval(cindex, 0, 0)*nsve->fs->cvelocity->cval(cindex, 0, 0)
          + nsve->fs->cvelocity->cval(cindex, 1, 0)*nsve->fs->cvelocity->cval(cindex, 1, 0)
          + nsve->fs->cvelocity->cval(cindex, 2, 0)*nsve->fs->cvelocity->cval(cindex, 2, 0)
          + nsve->fs->cvelocity->cval(cindex, 0, 1)*nsve->fs->cvelocity->cval(cindex, 0, 1)
          + nsve->fs->cvelocity->cval(cindex, 1, 1)*nsve->fs->cvelocity->cval(cindex, 1, 1)
          + nsve->fs->cvelocity->cval(cindex, 2, 1)*nsve->fs->cvelocity->cval(cindex, 2, 1));
    DEBUG_MSG("initial energy_NSE = %g, energy_NSVE = %g\n",
            energy_NSE, energy_NSVE);

    // step
    //nse->Euler_step();
    //nsve->fs->Euler_step(nsve->fs->dt);
    nse->step();
    nsve->fs->step(nsve->fs->dt);

    // compare results
    double diff_L2, diff_max;
    double nse_uL2, nsve_uL2;
    nsve->fs->compute_velocity(nsve->fs->cvorticity);

    energy_NSE = 0.5*(
            nse->velocity->cval(cindex, 0, 0)*nse->velocity->cval(cindex, 0, 0)
          + nse->velocity->cval(cindex, 1, 0)*nse->velocity->cval(cindex, 1, 0)
          + nse->velocity->cval(cindex, 2, 0)*nse->velocity->cval(cindex, 2, 0)
          + nse->velocity->cval(cindex, 0, 1)*nse->velocity->cval(cindex, 0, 1)
          + nse->velocity->cval(cindex, 1, 1)*nse->velocity->cval(cindex, 1, 1)
          + nse->velocity->cval(cindex, 2, 1)*nse->velocity->cval(cindex, 2, 1));
    energy_NSVE = 0.5*(
            nsve->fs->cvelocity->cval(cindex, 0, 0)*nsve->fs->cvelocity->cval(cindex, 0, 0)
          + nsve->fs->cvelocity->cval(cindex, 1, 0)*nsve->fs->cvelocity->cval(cindex, 1, 0)
          + nsve->fs->cvelocity->cval(cindex, 2, 0)*nsve->fs->cvelocity->cval(cindex, 2, 0)
          + nsve->fs->cvelocity->cval(cindex, 0, 1)*nsve->fs->cvelocity->cval(cindex, 0, 1)
          + nsve->fs->cvelocity->cval(cindex, 1, 1)*nsve->fs->cvelocity->cval(cindex, 1, 1)
          + nsve->fs->cvelocity->cval(cindex, 2, 1)*nsve->fs->cvelocity->cval(cindex, 2, 1));
    double analytic_energy;
    if (nse->forcing_type == "linear") {
        analytic_energy = 0.5*std::exp(2*(nse->famplitude - 4*nse->nu)*nse->dt);
    } else if (nse->forcing_type == "fixed_energy_injection_rate") {
        const double v2 = nse->injection_rate / (2*nse->nu*4);
        DEBUG_MSG("v2 = %g\n", v2);
        analytic_energy = 0.5*(
                v2 + (1 - v2)*std::exp(-2*nse->nu*4*nse->dt));

    }
    DEBUG_MSG("final %s: energy_NSE = %g, energy_NSVE = %g, analytic = %g\n",
            nse->forcing_type.c_str(),
            energy_NSE,
            energy_NSVE,
            analytic_energy);
    const bool difference_small_NSE =
        (energy_NSE - analytic_energy) / std::sqrt(energy_NSE*analytic_energy) < 1e-5;
    const bool difference_small_NSVE =
        (energy_NSVE - analytic_energy) / std::sqrt(energy_NSVE*analytic_energy) < 1e-5;
    if (difference_small_NSE && difference_small_NSVE)
        return EXIT_SUCCESS;
    else
        return EXIT_FAILURE;
}

template <typename rnumber>
int fluid_solver_check<rnumber>::do_work(void)
{
    TIMEZONE("fluid_solver_check::do_work");

    NSE<rnumber>* nse = new NSE<rnumber>(this->get_communicator(), this->simname);
    clone_parameters(nse);
    nse->allocate();

    NSVE<rnumber>* nsve = new NSVE<rnumber>(this->get_communicator(), this->simname);
    clone_parameters(nsve);
    nsve->allocate();

    // temporary field

    nse->forcing_type = "fixed_energy_injection_rate";
    nsve->fs->forcing_type = "fixed_energy_injection_rate";
    const int return_result_feir = check_solvers(nse, nsve);
    nse->forcing_type = "linear";
    nsve->fs->forcing_type = "linear";
    const int return_result_linear = check_solvers(nse, nsve);

    delete nse;
    delete nsve;
    return EXIT_SUCCESS;

    if ((return_result_linear == EXIT_SUCCESS)
     && (return_result_feir   == EXIT_SUCCESS))
        return EXIT_SUCCESS;
    else
        return EXIT_FAILURE;
}

template class fluid_solver_check<float>;
template class fluid_solver_check<double>;

