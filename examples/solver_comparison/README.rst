========================================
Sanity check: direct NSVE-NSE comparison
========================================

This is a system test: the NSVE solution is compared with the NSE
solution.


Exact solution
==============

We start with a single mode:

.. math::
    \textbf{u}(\textbf{x},0) = \textbf{U} (e^{i\textbf{k}\cdot \textbf{x}} + e^{-i\textbf{k}\cdot \textbf{x}})

with :math:`\textbf{U} \perp \textbf{k}`

**linear forcing**

solution is

.. math::
    \textbf{u}(\textbf{x},t) = \exp((-\nu\textbf{k}^2 + \alpha)t) \textbf{u}(\textbf{x},0)

where :math:`\alpha` is the forcing coefficient.

Energy is

.. math::
   E(t) = \exp(2(\alpha -\nu\textbf{k}^2)t) E(0)


**fixed energy injection rate**

Solution amplitude respects

.. math::
    U^2(t) = V^2 + (U^2(0) - V^2) \exp(-2\nu k^2 t)

with
:math:`V^2 = \epsilon / (2\nu k^2)`


