/******************************************************************************
*                                                                             *
*  Copyright 2024 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef FLUID_SOLVER_CHECK_HPP
#define FLUID_SOLVER_CHECK_HPP

/** \brief Error analysis for fluid solver.
 */

#include "full_code/test.hpp"

template <typename rnumber>
class fluid_solver_check: public test
{
    private:
        double max_velocity_estimate;
        double spectrum_dissipation;
        double spectrum_Lint;
        double spectrum_etaK;
        double spectrum_large_scale_const;
        double spectrum_small_scale_const;
        int output_incompressible_field;
        int random_seed;

        double dt;
        double famplitude;
        double friction_coefficient;
        double fk0;
        double fk1;
        double energy;
        double injection_rate;
        double variation_strength;
        double variation_time_scale;
        int fmode;
        std::string forcing_type;
        double nu;

    public:
        fluid_solver_check(
                const MPI_Comm COMMUNICATOR,
                const std::string &simname):
            test(
                    COMMUNICATOR,
                    simname)
        {}
        ~fluid_solver_check(){}

        int initialize(void);
        int do_work(void);
        int finalize(void);

        template <class dtype>
        inline int clone_parameters(dtype *dst) {
            dst->nx                   = this->nx;
            dst->ny                   = this->ny;
            dst->nz                   = this->nz;
            dst->dkx                  = this->dkx;
            dst->dky                  = this->dky;
            dst->dkz                  = this->dkz;
            dst->dealias_type         = this->dealias_type;

            dst->dt                   = this->dt;
            dst->famplitude           = this->famplitude;
            dst->friction_coefficient = this->friction_coefficient;
            dst->fk0                  = this->fk0;
            dst->fk1                  = this->fk1;
            dst->energy               = this->energy;
            dst->injection_rate       = this->injection_rate;
            dst->fmode                = this->fmode;
            dst->forcing_type         = this->forcing_type;
            dst->nu                   = this->nu;
            return EXIT_SUCCESS;
        }

};

#endif//FLUID_SOLVER_CHECK_HPP

