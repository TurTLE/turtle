/******************************************************************************
*                                                                             *
*  Copyright 2023 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/


//#include "solver_comparison.hpp"
#include "scope_timer.hpp"

#include <string>
#include <cmath>
#include <filesystem>

template <typename rnumber>
int solver_comparison<rnumber>::initialize(void)
{
    TIMEZONE("solver_comparison::intialize");
    this->read_parameters();

    for (int tt=0; tt < time_step_denominator.size(); tt++)
    {
        this->fsA[tt] = create_fluid_solver<rnumber>(
            tfs_NSVE,
            this->get_communicator(),
            this->simname + "NSVE_" + time_step_name[tt]);
        this->fsA[tt]->initialize();
        this->fsB[tt] = create_fluid_solver<rnumber>(
            tfs_NSE,
            this->get_communicator(),
            this->simname + "NSE_" + time_step_name[tt]);
        this->fsB[tt]->initialize();
        this->fsA[tt]->print_debug_info();
        this->fsB[tt]->print_debug_info();
    }

    this->fsA[1]->copy_state_from(*(this->fsA[0]));
    this->fsA[2]->copy_state_from(*(this->fsA[0]));
    this->fsB[0]->copy_state_from(*(this->fsA[0]));
    this->fsB[1]->copy_state_from(*(this->fsA[0]));
    this->fsB[2]->copy_state_from(*(this->fsA[0]));

    // open stat file
    if (this->myrank == 0) {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    this->grow_file_datasets();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int solver_comparison<rnumber>::step(void)
{
    TIMEZONE("solver_comparison::step");
    for (int tt = 0; tt < time_step_denominator.size(); tt++)
        for (int ii = 0; ii < time_step_denominator[tt]; ii++) {
            this->fsA[tt]->step();
            this->fsB[tt]->step();
        }

    this->iteration++;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int solver_comparison<rnumber>::write_checkpoint(void)
{
    TIMEZONE("solver_comparison::write_checkpoint");
    for (int tt = 0; tt < time_step_denominator.size(); tt++) {
        this->fsA[tt]->write_checkpoint();
        this->fsB[tt]->write_checkpoint();
    }
    this->write_iteration();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int solver_comparison<rnumber>::finalize(void)
{
    TIMEZONE("solver_comparison::finalize");
    for (int tt = 0; tt < time_step_denominator.size(); tt++) {
        this->fsA[tt]->finalize();
        delete this->fsA[tt];
        this->fsB[tt]->finalize();
        delete this->fsB[tt];
    }
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    return EXIT_SUCCESS;
}

template <typename rnumber>
inline rnumber relative_difference(
        const rnumber a,
        const rnumber b)
{
    const rnumber difference = a - b;
    const rnumber denominator2 = a*a + b*b;
    return ((denominator2 > std::numeric_limits<rnumber>::epsilon())
            ? (difference / std::sqrt(denominator2))
            : 0);
}

template <typename rnumber>
int compare_real_fields(
        kspace<FFTW, SMOOTH>* kk,
        const field<rnumber, FFTW, THREE>* a,
        const field<rnumber, FFTW, THREE>* b,
        const std::string prefix,
        const std::string suffix,
        const hid_t stat_group,
        const int ii,
        const double max_err_estimate,
        const double max_err_relative_estimate)
{
    TIMEZONE("compare_real_fields");
    field<rnumber, FFTW, THREE> * err;
    field<rnumber, FFTW, THREE> * err_relative;
    err = new field<rnumber, FFTW, THREE>(
            a->get_nx(), a->get_ny(), a->get_nz(),
            a->comm,
            a->fftw_plan_rigor);
    err_relative = new field<rnumber, FFTW, THREE>(
            a->get_nx(), a->get_ny(), a->get_nz(),
            a->comm,
            a->fftw_plan_rigor);
    err->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
                for (auto ii = 0; ii < 3; ii++) {
                    err->rval(rindex, ii) = (
                            a->rval(rindex, ii)
                          - b->rval(rindex, ii));
                    err_relative->rval(rindex, ii) = relative_difference(
                            a->rval(rindex, ii),
                            b->rval(rindex, ii));
                }
                });
    err->compute_stats(
            kk,
            stat_group,
            prefix + "_err" + suffix,
            ii,
            max_err_estimate);
    err_relative->compute_stats(
            kk,
            stat_group,
            prefix + "_err" + suffix + "_relative",
            ii,
            max_err_relative_estimate);
    delete err;
    delete err_relative;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int solver_comparison<rnumber>::do_stats()
{
    TIMEZONE("solver_comparison::do_stats");
    for (int tt = 0; tt < time_step_denominator.size(); tt++) {
        this->fsA[tt]->do_stats();
        this->fsB[tt]->do_stats();
    }
    // compute errors and error stats
    if (!(this->iteration % this->niter_stat == 0))
        return EXIT_SUCCESS;
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "statistics",
                H5P_DEFAULT);
    else
        stat_group = 0;

    // temporary fields
    field<rnumber, FFTW, THREE> * tmp_vec0;
    field<rnumber, FFTW, THREE> * tmp_vec1;
    tmp_vec0 = new field<rnumber, FFTW, THREE>(
            this->fsA[0]->get_nx(),
            this->fsA[0]->get_ny(),
            this->fsA[0]->get_nz(),
            this->fsA[0]->get_communicator(),
            this->fsA[0]->get_fftw_plan_rigor());
    tmp_vec1 = new field<rnumber, FFTW, THREE>(
            this->fsA[1]->get_nx(),
            this->fsA[1]->get_ny(),
            this->fsA[1]->get_nz(),
            this->fsA[1]->get_communicator(),
            this->fsA[1]->get_fftw_plan_rigor());

    for (int tt = 0; tt < time_step_denominator.size()-1; tt++) {
        this->fsA[tt]->get_rvelocity(tmp_vec0);
        this->fsA[tt+1]->get_rvelocity(tmp_vec1);
        compare_real_fields(
                this->fsA[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "velocity_NSVE_vs_NSVE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);
        this->fsA[tt]->get_rvorticity(tmp_vec0);
        this->fsA[tt+1]->get_rvorticity(tmp_vec1);
        compare_real_fields(
                this->fsA[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "vorticity_NSVE_vs_NSVE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);

        this->fsB[tt]->get_rvelocity(tmp_vec0);
        this->fsB[tt+1]->get_rvelocity(tmp_vec1);
        compare_real_fields(
                this->fsB[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "velocity_NSE_vs_NSE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);
        this->fsB[tt]->get_rvorticity(tmp_vec0);
        this->fsB[tt+1]->get_rvorticity(tmp_vec1);
        compare_real_fields(
                this->fsB[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "vorticity_NSE_vs_NSE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);

        this->fsA[tt]->get_rvelocity(tmp_vec0);
        this->fsB[tt+1]->get_rvelocity(tmp_vec1);
        compare_real_fields(
                this->fsB[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "velocity_NSVE_vs_NSE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);
        this->fsA[tt]->get_rvorticity(tmp_vec0);
        this->fsB[tt+1]->get_rvorticity(tmp_vec1);
        compare_real_fields(
                this->fsB[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "vorticity_NSVE_vs_NSE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);

        this->fsB[tt]->get_rvelocity(tmp_vec0);
        this->fsA[tt+1]->get_rvelocity(tmp_vec1);
        compare_real_fields(
                this->fsB[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "velocity_NSE_vs_NSVE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);
        this->fsB[tt]->get_rvorticity(tmp_vec0);
        this->fsA[tt+1]->get_rvorticity(tmp_vec1);
        compare_real_fields(
                this->fsB[0]->get_kspace(),
                tmp_vec1,
                tmp_vec0,
                "vorticity_NSE_vs_NSVE",
                time_step_name[tt],
                stat_group,
                this->iteration / niter_stat,
                this->max_err_estimate,
                this->max_err_relative_estimate);
    }

    delete tmp_vec1;
    delete tmp_vec0;

    if (this->myrank == 0)
        H5Gclose(stat_group);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int solver_comparison<rnumber>::read_parameters(void)
{
    TIMEZONE("solver_comparison::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + ".h5").c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->max_err_estimate = hdf5_tools::read_value<double>(
            parameter_file, "parameters/max_err_estimate");
    this->max_err_relative_estimate = hdf5_tools::read_value<double>(
            parameter_file, "parameters/max_err_relative_estimate");
    H5Fclose(parameter_file);
    MPI_Barrier(this->get_communicator()); // "lock" on parameter file
    return EXIT_SUCCESS;
}

template class solver_comparison<float>;
template class solver_comparison<double>;

