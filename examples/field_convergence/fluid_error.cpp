/******************************************************************************
*                                                                             *
*  Copyright 2023 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/


//#include "fluid_error.hpp"
#include "scope_timer.hpp"

#include <string>
#include <cmath>
#include <filesystem>

template <typename rnumber>
int fluid_error<rnumber>::initialize(void)
{
    TIMEZONE("fluid_error::intialize");
    this->read_parameters();

    this->fs1 = create_fluid_solver<rnumber>(
            this->tfs,
            this->get_communicator(),
            this->simname + "1");

    this->fs2 = create_fluid_solver<rnumber>(
            this->tfs,
            this->get_communicator(),
            this->simname + "2");

    this->fs4 = create_fluid_solver<rnumber>(
            this->tfs,
            this->get_communicator(),
            this->simname + "4");

    this->fs1->initialize();
    this->fs2->initialize();
    this->fs4->initialize();

    this->fs2->copy_state_from(*(this->fs1));
    this->fs4->copy_state_from(*(this->fs1));

    // open stat file
    if (this->myrank == 0) {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    this->grow_file_datasets();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int fluid_error<rnumber>::step(void)
{
    TIMEZONE("fluid_error::step");
    this->fs1->step();

    this->fs2->step();
    this->fs2->step();

    this->fs4->step();
    this->fs4->step();
    this->fs4->step();
    this->fs4->step();

    this->iteration++;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int fluid_error<rnumber>::write_checkpoint(void)
{
    TIMEZONE("fluid_error::write_checkpoint");
    this->fs1->write_checkpoint();
    this->fs2->write_checkpoint();
    this->fs4->write_checkpoint();
    this->write_iteration();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int fluid_error<rnumber>::finalize(void)
{
    TIMEZONE("fluid_error::finalize");
    this->fs1->finalize();
    this->fs2->finalize();
    this->fs4->finalize();
    delete this->fs1;
    delete this->fs2;
    delete this->fs4;
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    return EXIT_SUCCESS;
}

template <typename rnumber>
inline rnumber relative_difference(
        const rnumber a,
        const rnumber b)
{
    const rnumber difference = a - b;
    const rnumber denominator2 = a*a + b*b;
    return ((denominator2 > std::numeric_limits<rnumber>::epsilon())
            ? (difference / std::sqrt(denominator2))
            : 0);
}

template <typename rnumber>
int compare_real_fields(
        kspace<FFTW, SMOOTH>* kk,
        const field<rnumber, FFTW, THREE>* a,
        const field<rnumber, FFTW, THREE>* b,
        const std::string prefix,
        const std::string suffix,
        const hid_t stat_group,
        const int ii,
        const double max_err_estimate,
        const double max_err_relative_estimate)
{
    TIMEZONE("compare_real_fields");
    field<rnumber, FFTW, THREE> * err;
    field<rnumber, FFTW, THREE> * err_relative;
    err = new field<rnumber, FFTW, THREE>(
            a->get_nx(), a->get_ny(), a->get_nz(),
            a->comm,
            a->fftw_plan_rigor);
    err_relative = new field<rnumber, FFTW, THREE>(
            a->get_nx(), a->get_ny(), a->get_nz(),
            a->comm,
            a->fftw_plan_rigor);
    err->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
                for (auto ii = 0; ii < 3; ii++) {
                    err->rval(rindex, ii) = (
                            a->rval(rindex, ii)
                          - b->rval(rindex, ii));
                    err_relative->rval(rindex, ii) = relative_difference(
                            a->rval(rindex, ii),
                            b->rval(rindex, ii));
                }
                });
    err->compute_stats(
            kk,
            stat_group,
            prefix + "_err" + suffix,
            ii,
            max_err_estimate);
    err_relative->compute_stats(
            kk,
            stat_group,
            prefix + "_err" + suffix + "_relative",
            ii,
            max_err_relative_estimate);
    delete err;
    delete err_relative;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int fluid_error<rnumber>::do_stats()
{
    TIMEZONE("fluid_error::do_stats");
    this->fs1->do_stats();
    this->fs2->do_stats();
    this->fs4->do_stats();
    // compute errors and error stats
    if (!(this->iteration % this->niter_stat == 0))
        return EXIT_SUCCESS;
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "statistics",
                H5P_DEFAULT);
    else
        stat_group = 0;
    field<rnumber, FFTW, THREE> * tmp_vec0;
    field<rnumber, FFTW, THREE> * tmp_vec1;
    tmp_vec0 = new field<rnumber, FFTW, THREE>(
            this->fs1->get_nx(),
            this->fs1->get_ny(),
            this->fs1->get_nz(),
            this->fs1->get_communicator(),
            this->fs1->get_fftw_plan_rigor());
    tmp_vec1 = new field<rnumber, FFTW, THREE>(
            this->fs2->get_nx(),
            this->fs2->get_ny(),
            this->fs2->get_nz(),
            this->fs2->get_communicator(),
            this->fs2->get_fftw_plan_rigor());

    this->fs1->get_rvelocity(tmp_vec0);
    this->fs2->get_rvelocity(tmp_vec1);
    compare_real_fields(
            this->fs1->get_kspace(),
            tmp_vec1,
            tmp_vec0,
            "velocity",
            "1",
            stat_group,
            this->iteration / niter_stat,
            this->max_err_estimate,
            this->max_err_relative_estimate);
    this->fs1->get_rvorticity(tmp_vec0);
    this->fs2->get_rvorticity(tmp_vec1);
    compare_real_fields(
            this->fs1->get_kspace(),
            tmp_vec1,
            tmp_vec0,
            "vorticity",
            "1",
            stat_group,
            this->iteration / niter_stat,
            this->max_err_estimate,
            this->max_err_relative_estimate);

    // clumsy delete/new because in the future we may want different
    // problem sizes for the different individual DNS.
    delete tmp_vec0;
    tmp_vec0 = new field<rnumber, FFTW, THREE>(
            this->fs4->get_nx(),
            this->fs4->get_ny(),
            this->fs4->get_nz(),
            this->fs4->get_communicator(),
            this->fs4->get_fftw_plan_rigor());

    this->fs4->get_rvorticity(tmp_vec0);
    compare_real_fields(
            this->fs2->get_kspace(),
            tmp_vec0,
            tmp_vec1,
            "vorticity",
            "2",
            stat_group,
            this->iteration / niter_stat,
            this->max_err_estimate,
            this->max_err_relative_estimate);
    this->fs2->get_rvelocity(tmp_vec1);
    this->fs4->get_rvelocity(tmp_vec0);
    compare_real_fields(
            this->fs2->get_kspace(),
            tmp_vec0,
            tmp_vec1,
            "velocity",
            "2",
            stat_group,
            this->iteration / niter_stat,
            this->max_err_estimate,
            this->max_err_relative_estimate);

    delete tmp_vec1;
    delete tmp_vec0;

    if (this->myrank == 0)
        H5Gclose(stat_group);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int fluid_error<rnumber>::read_parameters(void)
{
    TIMEZONE("fluid_error::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + ".h5").c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->tfs = type_of_fluid_solver(hdf5_tools::read_value<int>(
            parameter_file, "parameters/tfs"));
    this->max_err_estimate = hdf5_tools::read_value<double>(
            parameter_file, "parameters/max_err_estimate");
    this->max_err_relative_estimate = hdf5_tools::read_value<double>(
            parameter_file, "parameters/max_err_relative_estimate");
    H5Fclose(parameter_file);
    MPI_Barrier(this->get_communicator()); // "lock" on parameter file
    return EXIT_SUCCESS;
}

template class fluid_error<float>;
template class fluid_error<double>;

