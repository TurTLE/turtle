/******************************************************************************
*                                                                             *
*  Copyright 2023 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef FLUID_ERROR_HPP
#define FLUID_ERROR_HPP

//#include "fluid_solver.hpp"

/** \brief Error analysis for fluid solver.
 */

template <typename rnumber>
class fluid_error: public direct_numerical_simulation
{
    public:

        /* fundamental ensemble entities */
        type_of_fluid_solver tfs;
        abstract_fluid_solver<rnumber> * fs1;
        abstract_fluid_solver<rnumber> * fs2;
        abstract_fluid_solver<rnumber> * fs4;

        double max_err_estimate;
        double max_err_relative_estimate;

        fluid_error(
                const MPI_Comm COMMUNICATOR,
                const std::string &simname):
            direct_numerical_simulation(
                    COMMUNICATOR,
                    simname)
        {}
        ~fluid_error(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//FLUID_ERROR_HPP

