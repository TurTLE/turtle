/******************************************************************************
*                                                                             *
*  Copyright 2024 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef SOLVER_COMPARISON_HPP
#define SOLVER_COMPARISON_HPP

#include <array>

//#include "fluid_solver.hpp"

/** \brief Error analysis for fluid solver.
 */

template <typename rnumber>
class solver_comparison: public direct_numerical_simulation
{
    public:

        /* fundamental ensemble entities */
        std::array<abstract_fluid_solver<rnumber>*, 3> fsA;
        std::array<abstract_fluid_solver<rnumber>*, 3> fsB;
        std::array<int, 3>
            time_step_denominator = {1, 2, 4};
        std::array<std::string, 3>
            time_step_name = {"1", "2", "4"};

        double max_err_estimate;
        double max_err_relative_estimate;

        solver_comparison(
                const MPI_Comm COMMUNICATOR,
                const std::string &simname):
            direct_numerical_simulation(
                    COMMUNICATOR,
                    simname)
        {}
        ~solver_comparison(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//SOLVER_COMPARISON_HPP

