/******************************************************************************
*                                                                             *
*  Copyright 2023 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef FLUID_SOLVER_HPP
#define FLUID_SOLVER_HPP

//#include "abstract_fluid_solver.hpp"
#include "full_code/NSVE.hpp"
#include "full_code/NSE.hpp"


template <typename rnumber>
class fluid_solver_NSVE:
    public abstract_fluid_solver<rnumber>,
    public NSVE<rnumber>
{
    public:
        fluid_solver_NSVE(const MPI_Comm COMMUNICATOR,
                     const std::string &simulation_name)
        : abstract_fluid_solver<rnumber>(COMMUNICATOR, simulation_name)
        , NSVE<rnumber>(COMMUNICATOR, simulation_name){}

        ~fluid_solver_NSVE() noexcept(false) {}

        int get_cvelocity(field<rnumber, FFTW, THREE> *destination)
        {
            this->fs->compute_velocity(this->fs->cvorticity);
            *destination = *(this->fs->cvelocity);
            return EXIT_SUCCESS;
        }
        int get_cvorticity(field<rnumber, FFTW, THREE> *destination)
        {
            *destination = *(this->fs->cvorticity);
            return EXIT_SUCCESS;
        }
        int copy_state_from(abstract_fluid_solver<rnumber> &src)
        {
            src.get_cvorticity(this->fs->cvorticity);
            return EXIT_SUCCESS;
        }
        kspace<FFTW, SMOOTH>* get_kspace()
        {
            return this->fs->kk;
        }
        unsigned get_fftw_plan_rigor() const
        {
            return this->fs->cvorticity->fftw_plan_rigor;
        }

        inline MPI_Comm get_communicator() const
        {
            return this->NSVE<rnumber>::get_communicator();
        }
        inline int get_nx() const {return this->NSVE<rnumber>::get_nx();}
        inline int get_ny() const {return this->NSVE<rnumber>::get_ny();}
        inline int get_nz() const {return this->NSVE<rnumber>::get_nz();}

        inline int write_checkpoint(void)
        {return this->NSVE<rnumber>::write_checkpoint();}
        inline int initialize(void)
        {return this->NSVE<rnumber>::initialize();}
        inline int step(void)
        {return this->NSVE<rnumber>::step();}
        inline int do_stats(void)
        {return this->NSVE<rnumber>::do_stats();}
        inline int finalize(void)
        {return this->NSVE<rnumber>::finalize();}
        inline int print_debug_info(void)
        {return this->NSVE<rnumber>::print_debug_info();}
};


template <typename rnumber>
class fluid_solver_NSE:
    public abstract_fluid_solver<rnumber>,
    public NSE<rnumber>
{
    public:
        fluid_solver_NSE(const MPI_Comm COMMUNICATOR,
                     const std::string &simulation_name)
        : abstract_fluid_solver<rnumber>(COMMUNICATOR, simulation_name)
        , NSE<rnumber>(COMMUNICATOR, simulation_name){}
        ~fluid_solver_NSE() noexcept(false) {}

        int get_cvelocity(field<rnumber, FFTW, THREE> *destination)
        {
            *destination = *(this->velocity);
            return EXIT_SUCCESS;
        }
        int get_cvorticity(field<rnumber, FFTW, THREE> *destination)
        {
            compute_curl(this->kk,
                         this->velocity,
                         destination);
            return EXIT_SUCCESS;
        }
        int copy_state_from(abstract_fluid_solver<rnumber> &src)
        {
            src.get_cvelocity(this->velocity);
            return EXIT_SUCCESS;
        }
        kspace<FFTW, SMOOTH>* get_kspace()
        {
            return this->kk;
        }
        unsigned get_fftw_plan_rigor() const
        {
            return this->velocity->fftw_plan_rigor;
        }

        inline MPI_Comm get_communicator() const
        {
            return this->NSE<rnumber>::get_communicator();
        }
        inline int get_nx() const {return this->NSE<rnumber>::get_nx();}
        inline int get_ny() const {return this->NSE<rnumber>::get_ny();}
        inline int get_nz() const {return this->NSE<rnumber>::get_nz();}

        inline int write_checkpoint(void)
        {return this->NSE<rnumber>::write_checkpoint();}
        inline int initialize(void)
        {return this->NSE<rnumber>::initialize();}
        inline int step(void)
        {return this->NSE<rnumber>::step();}
        inline int do_stats(void)
        {return this->NSE<rnumber>::do_stats();}
        inline int finalize(void)
        {return this->NSE<rnumber>::finalize();}
        inline int print_debug_info(void)
        {return this->NSE<rnumber>::print_debug_info();}
};

enum type_of_fluid_solver {
    tfs_NSVE = 0,
    tfs_NSE  = 1
};

template <typename rnumber>
abstract_fluid_solver<rnumber> * create_fluid_solver(
        const type_of_fluid_solver tt,
        const MPI_Comm COMMUNICATOR,
        const std::string &simulation_name)
{
    switch (tt)
    {
        case tfs_NSVE:
            return new fluid_solver_NSVE<rnumber>(
                    COMMUNICATOR,
                    simulation_name);
            break;
        case tfs_NSE:
            return new fluid_solver_NSE<rnumber>(
                    COMMUNICATOR,
                    simulation_name);
            break;
        default:
            DEBUG_MSG("wrong type of fluid solver passed to create_fluid_solver.\n");
            assert(false);
            break;
    }
}

#endif//FLUID_SOLVER_HPP

