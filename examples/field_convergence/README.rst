Error analysis for Navier-Stokes solvers
========================================

This is a code to analyze errors of the fluid solver in TurTLE.

There are two main goals to achieve:

* perform error analysis for Navier-Stokes solvers.

* provide an example of TurTLE facilitating "exotic" studies, i.e. use TurTLE as a framework.

This folder contains a basic C++ class to compute the errors, as well as
a Python wrapper that will launch jobs and read the results.
