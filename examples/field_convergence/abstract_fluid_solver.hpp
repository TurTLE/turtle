/******************************************************************************
*                                                                             *
*  Copyright 2023 TurTLE team                                                 *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef ABSTRACT_FLUID_SOLVER_HPP
#define ABSTRACT_FLUID_SOLVER_HPP

#include "field.hpp"
#include "kspace.hpp"

/** \brief Abstract fluid solver class
 */

template <typename rnumber>
class abstract_fluid_solver
{
    public:
        abstract_fluid_solver(
                const MPI_Comm COMM,
                const std::string &simname)
        {}
        virtual ~abstract_fluid_solver() noexcept(false) {}

        virtual MPI_Comm get_communicator() const = 0;
        virtual int get_nx() const = 0;
        virtual int get_ny() const = 0;
        virtual int get_nz() const = 0;

        virtual int write_checkpoint(void) = 0;
        virtual int initialize(void) = 0;
        virtual int step(void) = 0;
        virtual int do_stats(void) = 0;
        virtual int finalize(void) = 0;

        virtual int get_cvelocity(field<rnumber, FFTW, THREE> *) = 0;
        virtual int get_cvorticity(field<rnumber, FFTW, THREE> *) = 0;

        virtual int copy_state_from(abstract_fluid_solver<rnumber> &) = 0;

        virtual kspace<FFTW, SMOOTH>* get_kspace() = 0;

        virtual unsigned get_fftw_plan_rigor() const = 0;

        int get_rvelocity(field<rnumber, FFTW, THREE> *destination)
        {
            this->get_cvelocity(destination);
            destination->ift();
            return EXIT_SUCCESS;
        }
        int get_rvorticity(field<rnumber, FFTW, THREE> *destination)
        {
            this->get_cvorticity(destination);
            destination->ift();
            return EXIT_SUCCESS;
        }

        virtual int print_debug_info() = 0;
};

#endif//ABSTRACT_FLUID_SOLVER_HPP

