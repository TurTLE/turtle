#! /bin/bash

# Example TurTLE resize script

# run "small" simulation
turtle DNS NSVE \
    -n 32 --simname test32

# double the size of a specific snapshot (iteration 8 in this case)
# it's probably a good idea to always give "new_simname" as "old_simnamex2",
# but you are free to choose as appropriate.
# TODO: the python script should create an appropriate parameter file for new_simname.
turtle PP resize \
    --simname test32 \
    --iter0 8 --iter1 8 \
    --new_nx 64 --new_ny 64 --new_nz 64 --new_simname test32x2

# create a checkpoint symlink to the resized field
ln -s test32x2_fields.h5 test32x2_checkpoint_0.h5

# run "large" simulation using the resized field
turtle DNS NSVE \
    -n 64 --simname test64 \
    --src-simname test32x2 --src-iteration 8

