import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import math
import numpy as np

def main1():
    slab = 2
    nproc = 6
    f = plt.figure(figsize = (4., 4.))
    a = f.add_subplot(111)
    for p in range(nproc):
        for y in range((nproc)*slab):
            a.plot([y, y],
                   range(p*slab, (p+1)*slab),
                   marker = '.',
                   linestyle = 'none',
                   color = 'brown')
    a.set_ylim(bottom = -1, top = 12)
    a.set_xlim(left = -1)
    a.set_xlabel('$z$')
    a.set_ylabel('$x,y$')
    a.set_aspect('equal')
    f.tight_layout(pad = 0.5)
    f.savefig('interp_problem_0.pdf')
    for p in range(nproc):
        color = plt.get_cmap('plasma')(p*1./nproc)
        a.add_patch(
                mpatches.Rectangle(
                        [p*slab, 0],
                        1, slab*(nproc)-1,
                        color = color,
                        alpha = .2))
        a.text(p*slab+.5, -.6, '$p_{0}$'.format(p),
               verticalalignment = 'center')
    f.savefig('interp_problem_1.pdf')
    for X, Y in [(8.9, 8.3),
                 (3.3, 2.7)]:
        a.plot([X], [Y],
               color = 'black',
               marker = 'x',
               markersize = 10,
               markeredgewidth=3)
    f.savefig('interp_problem_2.pdf')
    for X, Y in [(8.9, 8.3),
                 (3.3, 2.7)]:
        for n in [1, 2]:
            a.add_patch(
                    mpatches.Rectangle(
                            [math.floor(X-n), math.floor(Y-n)],
                            2*n+1, 2*n+1,
                            color = 'green',
                            alpha = .2))
            a.text(math.floor(X)+.5, math.floor(Y - n)+.3,
                   '$I = {0}$'.format(2*n+2),
                   horizontalalignment = 'center')
    f.savefig('interp_problem.pdf')
    f.savefig('interp_problem_3.pdf')
    return None

def main0():
    slab = 2
    nproc = 6
    f = plt.figure(figsize = (4., 4.))
    a = f.add_subplot(111)
    for p in range(nproc):
        for y in range((nproc)*slab):
            a.plot([y, y],
                   range(p*slab, (p+1)*slab),
                   marker = '.',
                   linestyle = 'none',
                   color = 'brown')
    a.set_ylim(bottom = -1, top = 12)
    a.set_xlim(left = -1)
    a.set_ylabel('$z$')
    a.set_xlabel('$x,y$')
    a.set_aspect('equal')
    f.tight_layout(pad = 0.5)
    f.savefig('interp_problem_0.pdf')
    for p in range(nproc):
        color = plt.get_cmap('plasma')(p*1./nproc)
        a.add_patch(
                mpatches.Rectangle(
                        [0, p*slab],
                        slab*(nproc)-1, 1,
                        color = color,
                        alpha = .2))
        a.text(-.6, p*slab+.5, '$p_{0}$'.format(p),
               verticalalignment = 'center')
    f.savefig('interp_problem_1.pdf')
    for X, Y in [(8.9, 8.3),
                 (2.3, 3.7)]:
        a.plot([X], [Y],
               color = 'black',
               marker = 'x',
               markersize = 10,
               markeredgewidth=3)
    f.savefig('interp_problem_2.pdf')
    for X, Y in [(8.9, 8.3),
                 (2.3, 3.7)]:
        for n in [1, 2]:
            a.add_patch(
                    mpatches.Rectangle(
                            [math.floor(X-n), math.floor(Y-n)],
                            2*n+1, 2*n+1,
                            color = 'green',
                            alpha = .2))
            a.text(math.floor(X)+.5, math.floor(Y - n)-.6,
                   '$n = {0}$'.format(n),
                   horizontalalignment = 'center')
    f.savefig('interp_problem.pdf')
    f.savefig('interp_problem_3.pdf')
    return None

def main_method():
    N = 5
    # field is on grid
    f = plt.figure(figsize = (4., 4.))
    a = f.add_subplot(111)
    x = np.array(range(N))
    xx = np.zeros((x.shape[0], x.shape[0], 2), dtype = np.float64)
    xx[:, :, 0] = x[None, :]
    xx[:, :, 1] = x[:, None]
    a.plot(xx[..., 0], xx[..., 1],
           marker = '.',
           linestyle = 'none',
           color = 'brown')
    a.set_ylim(bottom = -0.5, top = N-0.5)
    a.set_xlim(left = -0.5, right = N-0.5)
    a.set_xlabel('$z$')
    a.set_ylabel('$x,y$')
    a.set_aspect('equal')
    f.tight_layout(pad = 0.5)
    f.savefig('interp_problem_0.pdf')
    # add point
    X = np.array([1.2, 2.6])
    cell = np.floor([1.2, 2.6])
    xbar = X - cell
    a.plot([X[0]], [X[1]],
            color = 'black',
            marker = 'x',
            markersize = 10,
            markeredgewidth=3)
    f.savefig('interp_problem_1.pdf')
    # add cell, position within cell
    a.add_patch(
        mpatches.Rectangle(
                [cell[0], cell[1]],
                1, 1,
                color = 'red',
                alpha = .2))
    a.plot([cell[0], X[0]],
           [0.75, 0.75],
           color = 'black',
           linewidth = 2,
           marker = '|'
           )
    a.text((cell[0] + X[0])/2, 0.5, '$\\bar{z}$',
           verticalalignment = 'center',
           horizontalalignment = 'center')
    a.plot([3.25, 3.25],
           [cell[1], X[1]],
           color = 'black',
           linewidth = 2,
           marker = '_'
           )
    a.text(3.7, (cell[1] + X[1])/2, '$\\bar{x}, \\bar{y}$',
           verticalalignment = 'center',
           horizontalalignment = 'center')
    f.savefig('interp_problem_2.pdf')
    a.add_patch(
        mpatches.Rectangle(
                [cell[0]-1, cell[1]-1],
                3, 3,
                color = 'green',
                alpha = .2))
    f.savefig('interp_problem_3.pdf')
    # add interpolation kernel
    #for p in range(nproc):
    #    color = plt.get_cmap('plasma')(p*1./nproc)
    #    a.add_patch(
    #            mpatches.Rectangle(
    #                    [p*slab, 0],
    #                    1, slab*(nproc)-1,
    #                    color = color,
    #                    alpha = .2))
    #    a.text(p*slab+.5, -.6, '$p_{0}$'.format(p),
    #           verticalalignment = 'center')
    #f.savefig('interp_problem_1.pdf')
    #for X, Y in [(8.9, 8.3),
    #             (3.3, 2.7)]:
    #    a.plot([X], [Y],
    #           color = 'black',
    #           marker = 'x',
    #           markersize = 10,
    #           markeredgewidth=3)
    #f.savefig('interp_problem_2.pdf')
    #for X, Y in [(8.9, 8.3),
    #             (3.3, 2.7)]:
    #    for n in [1, 2]:
    #        a.add_patch(
    #                mpatches.Rectangle(
    #                        [math.floor(X-n), math.floor(Y-n)],
    #                        2*n+1, 2*n+1,
    #                        color = 'green',
    #                        alpha = .2))
    #        a.text(math.floor(X)+.5, math.floor(Y - n)+.3,
    #               '$I = {0}$'.format(2*n+2),
    #               horizontalalignment = 'center')
    #f.savefig('interp_problem.pdf')
    #f.savefig('interp_problem_3.pdf')
    return None

if __name__ == '__main__':
    main_method()

