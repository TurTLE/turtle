---------------------------------
Tutorial: post-processing example
---------------------------------

Example problem:
----------------

Statistics of band-passed fields.
Please see [drivas2017prf]_.

The relevant files are :code:`cpp/full_code/bandpass_stats.?pp`,
:code:`TurTLE/PP.py` and :code:`CMakeLists.txt` (the list of headers should be
updated).

Instructions in brief
---------------------

These are minimal instructions on how to create a generic post-processing tool,
and they are roughly the procedure followed when writing the
:code:`cpp/full_code/bandpass_stats.?pp` files.

1. Get code, install (please see README).
2. Have a look at the Doxygen generated documentation for the
   ``postprocess`` class.
3. Write down the explicit numerical task.
   Identify algorithms, parameters, check whether required functionality
   is available.
4. Write down simple outline of new code to implement within
   ``initialize``, ``work_on_current_iteration`` and ``finalize``.
5. Create a new :code:`git` branch to work on, :code:`feature/<mypptool>`.
6. One option is to start from preexisting tools such as :code:`get_rfields`
   and modify them accordingly.
   In particular simple standard modifications must be made to the :code:`PP.py`
   and :code:`CmakeLists.txt` files.
7. Design a test of the new functionality (possibly just a modification
   of an existing test as needed).
   Test should begin as a parameter I/O sanity check, and then grow as
   the required functionality is implemented.

