===========
Development
===========


---------------------
Versioning guidelines
---------------------

Version tracking for :mod:`TurTLE` is done with ``git`` (see https://git-scm.com/
for description).
The branching model described at
http://nvie.com/posts/a-successful-git-branching-model/ is used for
inspiration.
However, experience shows that our team does not need a ``develop``
branch, hence we branch from and merg to ``master``.
If a situation comes up where we will need to maintain different
versions, we will branch from the respective tags as needed.

The usable ``VERSION`` number will be constructed according to semantic
versioning rules (see http://semver.org/), ``MAJOR.MINOR.PATCH``.
In principle, if you use :mod:`TurTLE` and you've created children of the
:class:`DNS <TurTLE.DNS>` class, you should not need to rewrite your code unless
the ``MAJOR`` version changes.
The package version is maintained through ``git`` tags.

``CMake`` will essentially call ``git`` to read in the ``VERSION``: it will get the
latest available tag.
The full output of ``git describe --tags`` is placed in
``TURTLE_VERSION_LONG`` and the stripped down ``MAJOR.MINOR.PATCH`` is
placed in ``TURTLE_VERSION``, and made available to CMake and the C++
code itself.

At the moment the following rules seem adequate.
Once I get feedback from someone who actually knows how to handle bigger
projects, these may change.

1. New features are worked on in branches forked from ``master``, with
   the branch name of the form ``feature/new``.
   Feature branches are merged back into ``master`` only after all tests
   pass.
   Typically the ``MINOR`` value will be increased at these merges.
2. Whenever a bug is discovered, a new branch called ``bugfix/something``
   is created (preferably forked from the earliest commit where the bug is
   apparent).
   When merged, the ``PATCH`` value is increased.

Please also read "code review" section below.

------------
Code testing
------------

We use CTest for testing `TurTLE`, and the ``.gitlab-ci.yml`` script
calls CTest.

-----------
Code review
-----------

For each merge request a review should be conducted.
Ideally, a person other than the initiator of the merge request should
review the code.

Code reviews can be more or less thorough, depending on how many
separate "review steps" are gone through.

Possible steps, not an exhaustive list:

0. No regressions.
   I.e. merge requests should not introduce mistakes into the code.
   To the extent that the CI coverage is adequate, "the pipeline passes"
   is a reasonable proxy for "no regressions".

1. New functionality is tested.
   Ideally a new test is added aimed at newly implemented functionality.
   In the case of bugfixes, a test can simply consist of triggering the
   bug.
   In this sense, the reviewer is encouraged to define a clear pass/fail
   test that the updated code should pass.
   Depending on concrete details, it may be better if the reviewer
   writes the test that needs to pass.
