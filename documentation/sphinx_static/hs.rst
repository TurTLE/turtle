Tutorial: hydrodynamic similarity
---------------------------------

run turtle twice with different values of parameters.


first run with default parameters

.. code:: bash
    
    turtle NSE --simname test_1 \
        --dkx 1.0 \
        --dky 1.0 \
        --dkz 1.0 \
        --nu 0.1 \
        --injection_rate 0.4 \


second, scale parameters by 2

.. code:: bash

    turtle NSE --simname test_2 \
        --dkx 0.5 \
        --dky 0.5 \
        --dkz 0.5 \
        --nu 0.1*factor \ #FIXME
        --injection_rate 0.4*forcing_factor #FIXME


afterwards, execute the following python code:

.. code:: python

    c1 = NSReader(simname = 'test_1')
    c1 = NSReader(simname = 'test_2')
    u1 = c1.read_cvelocity(iter1)
    u2 = c2.read_cvelocity(iter1)

    diff = np.abs(u1 - u2*vel_factor) / np.abs(u1) # take care of 0s

    assert(diff.max() < 1e-5)

