Bibliography
============

.. [fornberg1988mc] B. Fornberg,
    *Generation of Finite Difference Formulas on Arbitrarily Spaced Grids*.
    Mathematics of Computation,
    **51:184**, 699-706, 1988

.. [ishihara2007jfm] T. Ishihara et al,
    *Small-scale statistics in high-resolution direct numerical
    simulation of turbulence: Reynolds number dependence of
    one-point velocity gradient statistics*.
    J. Fluid Mech.,
    **592**, 335-366, 2007

.. [lalescu2010jcp] C. C. Lalescu, B. Teaca and D. Carati
    *Implementation of high order spline interpolations for tracking test
    particles in discretized fields*.
    J. Comput. Phys.
    **229**, 5862-5869, 2010

.. [lalescu2021arXiv] C. C. Lalescu, B. Bramas, M. Rampp and M. Wilczek
    *An Efficient Particle Tracking Algorithm for Large-Scale Parallel
    Pseudo-Spectral Simulations of Turbulence*.
    `arXiv 2107.01104 (2021) <https://arxiv.org/abs/2107.01104>`_

.. [lalescu2018jfm] C. C. Lalescu and M. Wilczek
    *Acceleration statistics of tracer particles in filtered turbulent fields*.
    `J. Fluid Mech. 847, R2 (2018)
    <https://doi.org/10.1017/jfm.2018.381>`_

.. [drivas2017prf] Drivas, T. D. and Johnson, P. L. and Lalescu, C. C. and Wilczek, M.
    *Large-scale sweeping of small-scale eddies in turbulence: A filtering approach*.
    `Phys Rev Fluids 2 104603 (2017) <https://dx.doi.org/10.1103/PhysRevFluids.2.104603>`_

.. [shu1988jcp] C.-W. Shu and S. Osher,
    *Efficient implementation of essentially non-oscillatory shock-capturing schemes*.
    `J. Comput. Phys. 77, 439-471 (1988) <http://dx.doi.org/10.1016/0021-9991(88)90177-5>`_

