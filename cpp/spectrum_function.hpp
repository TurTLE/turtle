#ifndef SPECTRUM_FUNCTION_CPP
#define SPECTRUM_FUNCTION_CPP

#include "kspace.hpp"

#include<cmath>


template <field_backend be,
          kspace_dealias_type dt>
class spectrum_function
{
    private:
        const kspace<be, dt> *kk;
        const std::vector<double> values;

    public:
        spectrum_function(
                const kspace<be, dt> *KK,
                const std::vector<double> &source_values):
            kk(KK),
            values(source_values)
        {
            assert(this->values.size() == this->kk->nshells);
        }
        ~spectrum_function() noexcept(false){}

        double operator()(double kvalue)
        {
            assert(kvalue >= double(0));
            int index = std::floor(kvalue / this->kk->dk);
            assert(index < this->values.size());
            return this->values[index];
        }
};

#endif//SPECTRUM_FUNCTION_CPP

