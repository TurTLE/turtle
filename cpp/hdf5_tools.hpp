/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef HDF5_TOOLS_HPP
#define HDF5_TOOLS_HPP

#include "base.hpp"

#include <vector>
#include <hdf5.h>
#include <string>

namespace hdf5_tools
{
    // see https://support.hdfgroup.org/HDF5/doc/H5.user/Datatypes.html
    template <typename data_type> hid_t hdf5_type_id();

    int grow_single_dataset(
            hid_t dset,
            int tincrement);

    bool field_exists(
            const std::string file_name,
            const std::string field_name,
            const std::string representation,
            const int iteration);

    herr_t grow_dataset_visitor(
        hid_t o_id,
        const char *name,
        const H5O_info_t *info,
        void *op_data);

    int grow_file_datasets(
            const hid_t stat_file,
            const std::string group_name,
            int tincrement);

    int require_size_single_dataset(
            hid_t dset,
            int tincrement);

    herr_t require_size_dataset_visitor(
        hid_t o_id,
        const char *name,
        const H5O_info_t *info,
        void *op_data);

    herr_t list_dataset_visitor(
        hid_t o_id,
        const char *name,
        const H5O_info_t *info,
        void *op_data);

    int require_size_file_datasets(
            const hid_t stat_file,
            const std::string group_name,
            int tincrement);

    int list_file_datasets(
            const hid_t stat_file,
            const std::string group_name);

    template <typename number>
    std::vector<number> read_vector(
            const hid_t group,
            const std::string dset_name);

    template <typename number>
    std::vector<number> read_vector_with_single_rank(
            const int myrank,
            const int rank_to_use,
            const MPI_Comm COMM,
            const hid_t group,
            const std::string dset_name);

    std::string read_string(
            const hid_t group,
            const std::string dset_name);

    template <typename number>
    number read_value(
            const hid_t group,
            const std::string dset_name);

    template <typename number>
    int write_value_with_single_rank(
            const hid_t group,
            const std::string dset_name,
            const number value);

    template <typename number>
    int update_time_series_with_single_rank(
            const hid_t group,
            const std::string dset_name,
            const hsize_t toffset,
            const hsize_t nvalues,
            const number *data);

    template <class partsize_t>
    int write_particle_ID_pairs_with_single_rank(
            const std::vector<partsize_t> v,
            const hid_t group,
            const std::string dset_name);

    template <class real_number>
    int write_vector_with_single_rank(
            const std::vector<real_number> v,
            const hid_t group,
            const std::string dset_name);

    template <typename number>
    int gather_and_write_with_single_rank(
            const int myrank,
            const int rank_to_use,
            const MPI_Comm COMM,
            const number *data,
            const int number_of_items,
            const hid_t group,
            const std::string dset_name);
}

#endif//HDF5_TOOLS_HPP

