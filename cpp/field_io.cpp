/**********************************************************************
*                                                                     *
*  Copyright 2024 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/


#include <sys/stat.h>
#include <algorithm>
#include <random>

template <typename rnumber,
          field_backend be,
          field_components fc>
int field<rnumber, be, fc>::print_plan(const std::string preamble)
{
    char *c2r_plan_information = fftw_interface<rnumber>::sprint(this->c2r_plan);
    char *r2c_plan_information = fftw_interface<rnumber>::sprint(this->r2c_plan);
    if (this->myrank == 0)
    {
        std::cout << preamble <<
                     std::endl <<
                     "----c2r plan is:\n" <<
                     c2r_plan_information <<
                     std::endl <<
                     "----r2c plan is:\n" <<
                     r2c_plan_information <<
                     std::endl;
    }
#ifdef TURTLE_DEBUG_MESSAGES_ON
    std::string err_message = (
            std::string("MPI rank ") +
            preamble +
            std::to_string(this->myrank) +
            std::string("\n----c2r plan is:\n") +
            std::string(c2r_plan_information) +
            std::string("\n----r2c plan is:\n") +
            std::string(r2c_plan_information) +
            std::string("\n"));
    std::cerr << err_message;
#endif//TURTLE_DEBUG_MESSAGES_ON

    free(c2r_plan_information);
    free(r2c_plan_information);
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
int field<rnumber, be, fc>::io_binary(
        const std::string fname,
        const int iteration,
        const bool read)
{
    const std::string full_fname = (
            fname +
            "_i" +
            std::to_string(iteration) +
            ".bin");
    if (this->real_space_representation)
    {
        field_binary_IO<rnumber, REAL, fc> *bin_IO = new field_binary_IO <rnumber, REAL, fc>(
                this->rlayout->sizes,
                this->rlayout->subsizes,
                this->rlayout->starts,
                this->rlayout->comm);
        if(read)
            bin_IO->read(
                    full_fname,
                    this->get_rdata());
        else
            bin_IO->write(
                    full_fname,
                    this->get_rdata());
        delete bin_IO;
    }
    else
    {
        field_binary_IO<rnumber, COMPLEX, fc> *bin_IO = new field_binary_IO <rnumber, COMPLEX, fc>(
                this->clayout->sizes,
                this->clayout->subsizes,
                this->clayout->starts,
                this->clayout->comm);
        if(read)
            bin_IO->read(
                    full_fname,
                    this->get_cdata());
        else
            bin_IO->write(
                    full_fname,
                    this->get_cdata());
        delete bin_IO;
    }
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
int field<rnumber, be, fc>::io(
        const std::string fname,
        const std::string field_name,
        const int iteration,
        const bool read)
{
    /* file dataset has same dimensions as field */
    TIMEZONE("field::io");
    hid_t file_id, dset_id, plist_id;
    file_id = H5I_BADID;
    dset_id = H5I_BADID;
    plist_id = H5I_BADID;
    std::string representation = std::string(
            this->real_space_representation ?
                "real" : "complex");
    std::string dset_name = (
            "/" + field_name +
            "/" + representation +
            "/" + std::to_string(iteration));

    /* open/create file */
    start_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, this->comm, MPI_INFO_NULL);
    bool file_exists = false;
    {
        struct stat file_buffer;
        file_exists = (stat(fname.c_str(), &file_buffer) == 0);
    }
    if (read)
    {
        DEBUG_MSG("field::io trying to read field %s from file %s\n", dset_name.c_str(), fname.c_str());
        assert(file_exists);
        file_id = H5Fopen(fname.c_str(), H5F_ACC_RDONLY, plist_id);
    }
    else
    {
        if (file_exists)
            file_id = H5Fopen(fname.c_str(), H5F_ACC_RDWR, plist_id);
        else
            file_id = H5Fcreate(fname.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, plist_id);
    }
    assert(file_id >= 0);
    if (file_id < 0) {
        DEBUG_MSG("couldn't open file");
        throw std::runtime_error(
                std::string("Couldn't open file for field I/O.\n")
                + std::string("file name = ") + fname + "\n");
    }
    H5Pclose(plist_id);

    /* check what kind of representation is being used */
    if (read)
    {
        dset_id = H5Dopen(
                file_id,
                dset_name.c_str(),
                H5P_DEFAULT);
        assert(dset_id >= 0);
        if (dset_id < 0) {
            DEBUG_MSG("couldn't open dataset");
            throw std::runtime_error(
                    std::string("Couldn't open dataset for field I/O.\n")
                    + std::string("file name = ") + fname + "\n"
                    + std::string("dataset name = ") + dset_name + "\n");
        }
        hid_t dset_type = H5Dget_type(dset_id);
        assert(dset_type >= 0);
        bool io_for_real = (
                H5Tequal(dset_type, H5T_IEEE_F32BE) ||
                H5Tequal(dset_type, H5T_IEEE_F32LE) ||
                H5Tequal(dset_type, H5T_INTEL_F32) ||
                H5Tequal(dset_type, H5T_NATIVE_FLOAT) ||
                H5Tequal(dset_type, H5T_IEEE_F64BE) ||
                H5Tequal(dset_type, H5T_IEEE_F64LE) ||
                H5Tequal(dset_type, H5T_INTEL_F64) ||
                H5Tequal(dset_type, H5T_NATIVE_DOUBLE));
        variable_used_only_in_assert(io_for_real);
        H5Tclose(dset_type);
        assert(this->real_space_representation == io_for_real);
    }
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);

    /* generic space initialization */
    hid_t fspace, mspace;
    hsize_t count[ndim(fc)], offset[ndim(fc)], dims[ndim(fc)];
    hsize_t memoffset[ndim(fc)], memshape[ndim(fc)];

    if (this->real_space_representation)
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            count[i] = this->rlayout->subsizes[i];
            offset[i] = this->rlayout->starts[i];
            dims[i] = this->rlayout->sizes[i];
            memshape[i] = this->rmemlayout->subsizes[i];
            memoffset[i] = 0;
        }
    }
    else
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            count [i] = this->clayout->subsizes[i];
            offset[i] = this->clayout->starts[i];
            dims  [i] = this->clayout->sizes[i];
            memshape [i] = count[i];
            memoffset[i] = 0;
        }
    }
    start_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);
    mspace = H5Screate_simple(ndim(fc), memshape, NULL);
    H5Sselect_hyperslab(mspace, H5S_SELECT_SET, memoffset, NULL, count, NULL);

    /* open/create data set */
    if (read)
        fspace = H5Dget_space(dset_id);
    else
    {
        if (!H5Lexists(file_id, field_name.c_str(), H5P_DEFAULT))
        {
            hid_t gid_tmp = H5Gcreate(
                    file_id, field_name.c_str(),
                    H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            H5Gclose(gid_tmp);
        }

        if (!H5Lexists(file_id, (field_name + "/" + representation).c_str(), H5P_DEFAULT))
        {
            hid_t gid_tmp = H5Gcreate(
                    file_id, ("/" + field_name + "/" + representation).c_str(),
                    H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            H5Gclose(gid_tmp);
        }
        if (H5Lexists(file_id, dset_name.c_str(), H5P_DEFAULT))
        {
            dset_id = H5Dopen(file_id, dset_name.c_str(), H5P_DEFAULT);
            fspace = H5Dget_space(dset_id);
        }
        else
        {
            fspace = H5Screate_simple(
                    ndim(fc),
                    dims,
                    NULL);
            /* chunking needs to go in here */
            dset_id = H5Dcreate(
                    file_id,
                    dset_name.c_str(),
                    (this->real_space_representation ? this->rnumber_H5T : this->cnumber_H5T),
                    fspace,
                    H5P_DEFAULT,
                    H5P_DEFAULT,
                    H5P_DEFAULT);
            assert(dset_id > 0);
        }
    }
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);
    /* both dset_id and fspace should now have sane values */

    /* if reading, first set local data to 0 */
    if (read)
        *this = rnumber(0.0);

    start_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);
    /* both dset_id and fspace should now have sane values */
    /* check file space */
    int ndims_fspace = H5Sget_simple_extent_dims(fspace, dims, NULL);
    variable_used_only_in_assert(ndims_fspace);
    assert(((unsigned int)(ndims_fspace)) == ndim(fc));
    if (this->real_space_representation)
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            offset[i] = this->rlayout->starts[i];
            assert(dims[i] == this->rlayout->sizes[i]);
        }
        H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        if (read)
        {
            H5Dread(dset_id, this->rnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
        }
        else
        {
            assert(this->real_space_representation);
            H5Dwrite(dset_id, this->rnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
        }
        H5Sclose(mspace);
    }
    else
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            offset[i] = this->clayout->starts[i];
            assert(dims[i] == this->clayout->sizes[i]);
        }
        H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        if (read)
        {
            H5Dread(dset_id, this->cnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
            this->symmetrize();
        }
        else
        {
            assert(!this->real_space_representation);
            H5Dwrite(dset_id, this->cnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
        }
        H5Sclose(mspace);
    }

    H5Sclose(fspace);
    /* close data set */
    H5Dclose(dset_id);
    /* close file */
    H5Fclose(file_id);
    /* ensure all processes are finished writing before exiting the method */
    MPI_Barrier(this->comm);
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
int field<rnumber, be, fc>::io_database(
        const std::string fname,
        const std::string field_name,
        const int toffset,
        const bool read)
{
    /* file dataset is has a time dimension as well */
    TIMEZONE("field::io_database");
    hid_t file_id, dset_id, plist_id;
    dset_id = H5I_BADID;
    std::string representation = std::string(
            this->real_space_representation ?
                "real" : "complex");
    std::string dset_name = (
            "/" + field_name +
            "/" + representation);

    /* open/create file */
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, this->comm, MPI_INFO_NULL);
    bool file_exists = false;
    {
        struct stat file_buffer;
        file_exists = (stat(fname.c_str(), &file_buffer) == 0);
    }
    if (read)
    {
        DEBUG_MSG("field::io trying to read field from file %s\n", fname.c_str());
        assert(file_exists);
        file_id = H5Fopen(fname.c_str(), H5F_ACC_RDONLY, plist_id);
    }
    else
    {
        if (file_exists)
            file_id = H5Fopen(fname.c_str(), H5F_ACC_RDWR, plist_id);
        else
            file_id = H5Fcreate(fname.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, plist_id);
    }
    H5Pclose(plist_id);

    /* check what kind of representation is being used */
    if (read)
    {
        dset_id = H5Dopen(
                file_id,
                dset_name.c_str(),
                H5P_DEFAULT);
        hid_t dset_type = H5Dget_type(dset_id);
        bool io_for_real = (
                H5Tequal(dset_type, H5T_IEEE_F32BE) ||
                H5Tequal(dset_type, H5T_IEEE_F32LE) ||
                H5Tequal(dset_type, H5T_INTEL_F32) ||
                H5Tequal(dset_type, H5T_NATIVE_FLOAT) ||
                H5Tequal(dset_type, H5T_IEEE_F64BE) ||
                H5Tequal(dset_type, H5T_IEEE_F64LE) ||
                H5Tequal(dset_type, H5T_INTEL_F64) ||
                H5Tequal(dset_type, H5T_NATIVE_DOUBLE));
        variable_used_only_in_assert(io_for_real);
        H5Tclose(dset_type);
        assert(this->real_space_representation == io_for_real);
    }

    /* generic space initialization */
    hid_t fspace, mspace;
    hsize_t count[ndim(fc)+1], offset[ndim(fc)+1], dims[ndim(fc)+1];
    hsize_t memoffset[ndim(fc)+1], memshape[ndim(fc)+1];

    int dim_counter_offset = 1;
    dim_counter_offset = 1;
    count[0] = 1;
    memshape[0] = 1;
    memoffset[0] = 0;
    if (this->real_space_representation)
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            count[i+dim_counter_offset] = this->rlayout->subsizes[i];
            offset[i+dim_counter_offset] = this->rlayout->starts[i];
            dims[i+dim_counter_offset] = this->rlayout->sizes[i];
            memshape[i+dim_counter_offset] = this->rmemlayout->subsizes[i];
            memoffset[i+dim_counter_offset] = 0;
        }
        mspace = H5Screate_simple(dim_counter_offset + ndim(fc), memshape, NULL);
        H5Sselect_hyperslab(mspace, H5S_SELECT_SET, memoffset, NULL, count, NULL);
    }
    else
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            count[i+dim_counter_offset] = this->clayout->subsizes[i];
            offset[i+dim_counter_offset] = this->clayout->starts[i];
            dims[i+dim_counter_offset] = this->clayout->sizes[i];
            memshape[i+dim_counter_offset] = count[i+dim_counter_offset];
            memoffset[i+dim_counter_offset] = 0;
        }
        mspace = H5Screate_simple(dim_counter_offset + ndim(fc), memshape, NULL);
        H5Sselect_hyperslab(mspace, H5S_SELECT_SET, memoffset, NULL, count, NULL);
    }

    /* open/create data set */
    if (read)
        fspace = H5Dget_space(dset_id);
    else
    {
        if (!H5Lexists(file_id, field_name.c_str(), H5P_DEFAULT))
            H5Gcreate(
                    file_id, field_name.c_str(),
                    H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        if (H5Lexists(file_id, dset_name.c_str(), H5P_DEFAULT))
        {
            dset_id = H5Dopen(file_id, dset_name.c_str(), H5P_DEFAULT);
            fspace = H5Dget_space(dset_id);
        }
        else
        {
            fspace = H5Screate_simple(
                    ndim(fc),
                    dims,
                    NULL);
            /* chunking needs to go in here */
            dset_id = H5Dcreate(
                    file_id,
                    dset_name.c_str(),
                    (this->real_space_representation ? this->rnumber_H5T : this->cnumber_H5T),
                    fspace,
                    H5P_DEFAULT,
                    H5P_DEFAULT,
                    H5P_DEFAULT);
            assert(dset_id > 0);
        }
    }
    /* both dset_id and fspace should now have sane values */

    if (read)
        *this = rnumber(0.0);

    /* check file space */
    int ndims_fspace = H5Sget_simple_extent_dims(fspace, dims, NULL);
    variable_used_only_in_assert(ndims_fspace);
    assert(ndims_fspace == int(ndim(fc) + 1));
    offset[0] = toffset;
    if (this->real_space_representation)
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            offset[i+dim_counter_offset] = this->rlayout->starts[i];
            assert(dims[i+dim_counter_offset] == this->rlayout->sizes[i]);
        }
        H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        if (read)
        {
            H5Dread(dset_id, this->rnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
            this->real_space_representation = true;
        }
        else
        {
            assert(this->real_space_representation);
            H5Dwrite(dset_id, this->rnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
        }
        H5Sclose(mspace);
    }
    else
    {
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            offset[i+dim_counter_offset] = this->clayout->starts[i];
            assert(dims[i+dim_counter_offset] == this->clayout->sizes[i]);
        }
        H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        if (read)
        {
            H5Dread(dset_id, this->cnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
            this->real_space_representation = false;
            this->symmetrize();
        }
        else
        {
            assert(!this->real_space_representation);
            H5Dwrite(dset_id, this->cnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
        }
        H5Sclose(mspace);
    }

    H5Sclose(fspace);
    /* close data set */
    H5Dclose(dset_id);
    /* close file */
    H5Fclose(file_id);
    return EXIT_SUCCESS;
}


template <typename rnumber,
          field_backend be,
          field_components fc>
int field<rnumber, be, fc>::write_0slice(
        const hid_t group,
        const std::string field_name,
        const int iteration)
{
    start_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);
    // this should in principle work for any fc
    TIMEZONE("field::write_0slice");
    assert(this->real_space_representation);
    if (this->myrank == 0)
    {
        hid_t dset, wspace, mspace;
        int ndims;
        hsize_t count[5], offset[5], dims[5];
        offset[0] = iteration;
        offset[1] = 0;
        offset[2] = 0;
        offset[3] = 0;
        offset[4] = 0;
        dset = H5Dopen(
                group,
                ("0slices/" + field_name + "/real").c_str(),
                H5P_DEFAULT);
        wspace = H5Dget_space(dset);
        ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
        // array in memory has 2 extra x points, because FFTW
        count[0] = 1;
        count[1] = this->rmemlayout->sizes[1];
        count[2] = this->rmemlayout->sizes[2];
        count[3] = 3;
        count[4] = 3;
        mspace = H5Screate_simple(ndims, count, NULL);
        // array in file should not have the extra 2 points
        count[1] = this->rlayout->sizes[1];
        count[2] = this->rlayout->sizes[2];
        // select right slice in file
        H5Sselect_hyperslab(
            wspace,
            H5S_SELECT_SET,
            offset,
            NULL,
            count,
            NULL);
        offset[0] = 0;
        // select proper regions of memory
        H5Sselect_hyperslab(
            mspace,
            H5S_SELECT_SET,
            offset,
            NULL,
            count,
            NULL);
        H5Dwrite(
            dset,
            this->rnumber_H5T,
            mspace,
            wspace,
            H5P_DEFAULT,
            this->data);
        H5Dclose(dset);
        H5Sclose(mspace);
        H5Sclose(wspace);
    }
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::HDF5);
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
int field<rnumber, be, fc>::write_filtered(
        const std::string fname,
        const std::string field_name,
        const int iteration,
        int nx,
        int ny,
        int nz)
{
    /* file dataset has same dimensions as field */
    TIMEZONE("field::write_filtered");
    // only works in Fourier representation
    assert(!this->real_space_representation);
    assert(hsize_t(nx) <= this->rlayout->sizes[2]);
    assert(hsize_t(ny) <= this->rlayout->sizes[1]);
    assert(hsize_t(nz) <= this->rlayout->sizes[0]);
    // current algorithm only works for more than one process
    assert(this->nprocs >= 2);
    hid_t file_id, dset_id, plist_id;
    dset_id = H5I_BADID;
    std::string dset_name = (
            "/" + field_name +
            "/complex" +
            "/" + std::to_string(iteration));

    /* open/create file */
    plist_id = H5Pcreate(H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(plist_id, this->comm, MPI_INFO_NULL);
    bool file_exists = false;
    struct stat file_buffer;
    file_exists = (stat(fname.c_str(), &file_buffer) == 0);
    if (file_exists)
        file_id = H5Fopen(fname.c_str(), H5F_ACC_RDWR, plist_id);
    else
        file_id = H5Fcreate(fname.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, plist_id);
    assert(file_id >= 0);
    H5Pclose(plist_id);

    /* generic space initialization */
    hid_t fspace, mspace;
    hsize_t count[ndim(fc)], offset[ndim(fc)], dims[ndim(fc)], fdims[ndim(fc)];
    hsize_t memoffset[ndim(fc)], memshape[ndim(fc)];

    // set up dimensions
    for (unsigned int i=3; i<ndim(fc); i++)
    {
        count [i] = this->clayout->subsizes[i];
        offset[i] = this->clayout->starts[i];
        dims  [i] = this->clayout->sizes[i];
        memshape [i] = count[i];
        memoffset[i] = 0;
    }
    // these are dimensions of dataset, needed
    // to create dataset
    dims[0] = ny;
    dims[1] = nz;
    dims[2] = nx/2+1;

    /* open/create data set */
    if (!H5Lexists(file_id, field_name.c_str(), H5P_DEFAULT))
    {
        hid_t gid_tmp = H5Gcreate(
                file_id, field_name.c_str(),
                H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        H5Gclose(gid_tmp);
    }
    if (!H5Lexists(file_id, (field_name + "/complex").c_str(), H5P_DEFAULT))
    {
        hid_t gid_tmp = H5Gcreate(
                file_id, ("/" + field_name + "/complex").c_str(),
                H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        H5Gclose(gid_tmp);
    }
    if (H5Lexists(file_id, dset_name.c_str(), H5P_DEFAULT))
    {
        dset_id = H5Dopen(file_id, dset_name.c_str(), H5P_DEFAULT);
        fspace = H5Dget_space(dset_id);
    }
    else
    {
        if (nz == 1)
        {
            hsize_t temp_dims[ndim(fc)-1];
            temp_dims[0] = dims[0];
            for (unsigned int i=1; i<ndim(fc)-1; i++)
                temp_dims[i] = dims[i+1];
            fspace = H5Screate_simple(
                    ndim(fc)-1,
                    temp_dims,
                    NULL);
        }
        else
            fspace = H5Screate_simple(
                    ndim(fc),
                    dims,
                    NULL);
        /* chunking needs to go in here */
        dset_id = H5Dcreate(
                file_id,
                dset_name.c_str(),
                this->cnumber_H5T,
                fspace,
                H5P_DEFAULT,
                H5P_DEFAULT,
                H5P_DEFAULT);
        assert(dset_id > 0);
    }
    /* check file space */
    int ndims_fspace = H5Sget_simple_extent_dims(fspace, fdims, NULL);
    variable_used_only_in_assert(ndims_fspace);
    if (nz == 1)
    {
        assert(((unsigned int)(ndims_fspace)) == ndim(fc)-1);
        assert(dims[0] == fdims[0]);
        for (unsigned int i=1; i<ndim(fc)-1; i++)
            assert(dims[i+1] == fdims[i]);
    }
    else
    {
        assert(((unsigned int)(ndims_fspace)) == ndim(fc));
        for (unsigned int i=0; i<ndim(fc); i++)
        {
            assert(dims[i] == fdims[i]);
        }
    }
    /* both dset_id and fspace now have sane values */

    /// set up counts and offsets
    /// x is easy, since only positive modes are present
    count [2] = nx/2+1;
    offset[2] = 0;
    memshape [2] = this->clayout->subsizes[2];
    memoffset[2] = 0;

    /// three options for y:
    /// this->starts[0] <= ny/2
    /// ny / 2 < this->starts[0] +this->clayout->subsizes[0] < this->sizes[0] - ny/2
    /// this->starts[0] >= this->sizes[0] - ny/2
    /// we don't care about saving the ny/2 mode, because of symmetry
    hsize_t y0 = this->clayout->starts[0];
    hsize_t y1 = this->clayout->starts[0] + this->clayout->subsizes[0];
    memshape[0] = this->clayout->subsizes[0];
    if (y1 <= hsize_t(ny/2))
    {
        count[0] = this->clayout->subsizes[0];
        offset[0] = y0;
        memoffset[0] = 0;
    }
    else
    {
        if (y0 < hsize_t(ny)/2)
        {
            count[0] = ny/2 - y0;
            offset[0] = y0;
            memoffset[0] = 0;
        }
        else
        {
            if (y1 <= hsize_t(this->clayout->sizes[0] - ny/2 + 1))
            { // y0 < y1 therefore y0 <= this->clayout->sizes[0] - ny/2
                count[0] = 0;
                offset[0] = ny/2;
                memoffset[0] = 0;
            }
            else
            {
                if (y0 <= hsize_t(this->clayout->sizes[0] - ny/2))
                {
                    count[0] = y1 - (this->clayout->sizes[0] - ny/2);
                    offset[0] = ny - (this->clayout->sizes[0] - y0);
                    memoffset[0] = this->clayout->subsizes[0] - count[0];
                }
                else
                {
                    count[0] = this->clayout->subsizes[0];
                    offset[0] = ny - (this->clayout->sizes[0] - y0);
                    memoffset[0] = 0;
                }
            }
        }
    }
    if (nz>=2)
    {
        assert(nz%2==0);
        /// for z, we need to take into account that there are
        /// both positive and negative modes
        for (int cz = 0; cz < 2; cz++)
        {
            count [1] = nz/2;
            offset[1] = cz*nz/2;
            memshape [1] = this->clayout->sizes[1];
            memoffset[1] = cz*(this->clayout->sizes[1] - nz/2);

            //now write data
            mspace = H5Screate_simple(ndim(fc), memshape, NULL);
            H5Sselect_hyperslab(mspace, H5S_SELECT_SET, memoffset, NULL, count, NULL);
            H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset, NULL, count, NULL);
            H5Dwrite(dset_id, this->cnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
            H5Sclose(mspace);
        }
    }
    else
    {
        assert(nz == 1);
        count [1] = 1;
        offset[1] = 0;
        memshape [1] = this->clayout->sizes[1];
        memoffset[1] = 0;

        //now write data
        mspace = H5Screate_simple(ndim(fc), memshape, NULL);
        H5Sselect_hyperslab(mspace, H5S_SELECT_SET, memoffset, NULL, count, NULL);
        for (unsigned int i=1; i<ndim(fc)-1; i++)
            count[i] = count[i+1];
        H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset_id, this->cnumber_H5T, mspace, fspace, H5P_DEFAULT, this->data);
        H5Sclose(mspace);
    }


    /* close file data space */
    H5Sclose(fspace);
    /* close data set */
    H5Dclose(dset_id);
    /* close file */
    H5Fclose(file_id);
    return EXIT_SUCCESS;
}

