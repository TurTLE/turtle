/**********************************************************************
*                                                                     *
*  Copyright 2015 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef FFTW_TOOLS
#define FFTW_TOOLS


#include <fftw3-mpi.h>
#include <string>
#include <map>
#include <cmath>

extern int myrank, nprocs;

extern std::map<std::string, unsigned> fftw_planner_string_to_flag;


// helper function
inline void complex_number_phase_shifter(double &xx, double &yy, const double cos_val, const double sin_val)
{
    const double new_real = double(xx*cos_val - yy*sin_val);
    const double new_imag = double(yy*cos_val + xx*sin_val);
    xx = new_real;
    yy = new_imag;
}

inline void complex_number_phase_shifter(double &xx, double &yy, const double phase)
{
    const double cval = std::cos(phase);
    const double sval = std::sin(phase);
    complex_number_phase_shifter(xx, yy, cval, sval);
}

inline void complex_number_phase_shifter(float &xx, float &yy, const double cos_val, const double sin_val)
{
    const float new_real = float(xx*cos_val - yy*sin_val);
    const float new_imag = float(yy*cos_val + xx*sin_val);
    xx = new_real;
    yy = new_imag;
}

inline void complex_number_phase_shifter(float &xx, float &yy, const double phase)
{
    const double cval = std::cos(phase);
    const double sval = std::sin(phase);
    complex_number_phase_shifter(xx, yy, cval, sval);
}

#endif//FFTW_TOOLS

