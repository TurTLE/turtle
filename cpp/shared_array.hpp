/******************************************************************************
*                                                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef SHAREDARRAY_HPP
#define SHAREDARRAY_HPP

#include <omp.h>
#include <functional>

template <class ValueType, size_t dim>
void shared_array_zero_initializer(ValueType *data) {
    std::fill_n(data, dim, 0);
}

/** \class shared_array
 *  \brief Manages array data in for OpenMP parallelism.
 *
 *  This class allocates distinct "clones" of the desired array for each OpenMP
 *  thread, and exposes them with `getMine`.
 *  Separately, it provides a generic method to reduce the data (`merge`
 *  method), and afterwards the results can be accessed with `getMasterData`.
 *
 *  Normally it's preferable to use omp directives to specify shared/private
 *  data and reductions.
 *  In the case of TurTLE, we use the "LOOP" methods to hide the omp pragma
 *  statements, hence we need this class to handle shared data.
 *
 *  \warning Cannot be used by different parallel section at the same time.
 *
 *  \note 2024-02-22 the inner loop from "merge" is marked as "novector"
 *  because the intel compiler triggers floating point exceptions otherwise.
 */
template <class ValueType>
class shared_array{
    const int currentNbThreads;
    ValueType** __restrict__ values;
    const size_t dim;

    std::function<void(ValueType*)> initFunc;

    bool hasBeenMerged;

public:
    shared_array(const size_t inDim)
            : currentNbThreads(omp_get_max_threads())
            , values(nullptr)
            , dim(inDim)
            , hasBeenMerged(false){
        values = new ValueType*[currentNbThreads];
        values[0] = new ValueType[dim];
        for(int idxThread = 1 ; idxThread < currentNbThreads ; ++idxThread){
            values[idxThread] = nullptr;
        }
    }

    shared_array(const size_t inDim, std::function<void(ValueType*)> inInitFunc)
            : shared_array(inDim){
        setInitFunction(inInitFunc);
    }

    ~shared_array(){
        for(int idxThread = 0 ; idxThread < currentNbThreads ; ++idxThread){
            delete[] values[idxThread];
        }
        delete[] values;
        if(hasBeenMerged == false){
        }
    }

    ValueType* getMasterData(){
        return values[0];
    }

    const ValueType* getMasterData() const{
        return values[0];
    }

    void merge(){
        ValueType* __restrict__ dest = values[0];
        for(int idxThread = 1 ; idxThread < currentNbThreads ; ++idxThread){
            if(values[idxThread]){
                const ValueType* __restrict__ src = values[idxThread];
                for( size_t idxVal = 0 ; idxVal < dim ; ++idxVal){
                    dest[idxVal] += src[idxVal];
                }
            }
        }
        hasBeenMerged = true;
    }

    template <class Func>
    void merge(Func func){
        ValueType* __restrict__ dest = values[0];
        for(int idxThread = 1 ; idxThread < currentNbThreads ; ++idxThread){
            if(values[idxThread]){
                const ValueType* __restrict__ src = values[idxThread];
                #if ( defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER) )
                    #pragma novector
                #else
                    //#pragma GCC novector
                    #pragma omp simd if(0)
                #endif
                for( size_t idxVal = 0 ; idxVal < dim ; ++idxVal){
                    dest[idxVal] = func(idxVal, dest[idxVal], src[idxVal]);
                }
            }
        }
        hasBeenMerged = true;
    }

    void mergeParallel(){
        merge(); // not done yet
    }

    template <class Func>
    void mergeParallel(Func func){
        merge(func); // not done yet
    }

    void setInitFunction(std::function<void(ValueType*)> inInitFunc){
        initFunc = inInitFunc;
        initFunc(values[0]);
    }

    ValueType* getMine(){
        assert(omp_get_thread_num() < currentNbThreads);

        if(values[omp_get_thread_num()] == nullptr){
            ValueType* myValue = new ValueType[dim];
            if(initFunc){
                initFunc(myValue);
            }

            values[omp_get_thread_num()] = myValue;
	        return myValue;
        }

        return values[omp_get_thread_num()];
    }
};

#endif
