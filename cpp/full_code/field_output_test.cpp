/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "field_output_test.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"
#include "shared_array.hpp"

#include <cmath>
#include <random>

template <typename rnumber>
int field_output_test<rnumber>::initialize(void)
{
    TIMEZONE("field_output_test::initialize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int field_output_test<rnumber>::finalize(void)
{
    TIMEZONE("field_output_test::finalize");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int field_output_test<rnumber>::read_parameters()
{
    TIMEZONE("field_output_test::read_parameters");
    this->test::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->random_seed = hdf5_tools::read_value<int>(parameter_file, "/parameters/field_random_seed");
    this->number_operations = hdf5_tools::read_value<int>(parameter_file, "/parameters/number_operations");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int field_output_test<rnumber>::do_work(void)
{
    TIMEZONE("field_output_test::do_work");
    // allocate
    field<rnumber, FFTW, ONE> *scal_field = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);
    field<rnumber, FFTW, ONE> *backup_field = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);


    kspace<FFTW, SMOOTH> *kk = new kspace<FFTW, SMOOTH>(
            scal_field->clayout, this->dkx, this->dky, this->dkz);

    make_gaussian_random_field(
        kk,
        scal_field,
        random_seed,
        0.4,
        1.0,
        6.0 / this->nx);
    scal_field->ift();

    *backup_field = *scal_field;

    for (int i=0; i<this->number_operations; i++)
    {
        DEBUG_MSG("IO operation %d\n", i);
        // write data
        scal_field->io(
            this->simname + std::string("_fields.h5"),
            "scal_field_",
            i,
            false);
        scal_field->io_binary(
            this->simname + std::string("_field.bin"),
            i,
            false);

        // read HDF5
        scal_field->io(
            this->simname + std::string("_fields.h5"),
            "scal_field_",
            i,
            true);
        // RLOOP uses a "#pragma openmp parallel"
        // so we need a shared array
        shared_array<bool> comparison_0_ok(
                1,
                [&](bool* local_comparison){
                    std::fill_n(local_comparison, 1, true);
                });
        scal_field->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
                comparison_0_ok.getMine()[0] = (
                        comparison_0_ok.getMine()[0] &&
                        (scal_field->rval(rindex) ==
                         backup_field->rval(rindex)));
                });
        comparison_0_ok.mergeParallel(
                [&](const int idx,
                    const bool& v1,
                    const bool& v2) -> bool {
                return v1 && v2;});
        if (!comparison_0_ok.getMasterData()[0])
            DEBUG_MSG("error with HDF5 comparison\n");

        // read binary
        scal_field->io_binary(
            this->simname + std::string("_field"),
            i,
            true);
        // RLOOP uses a "#pragma openmp parallel"
        // so we need a shared array
        shared_array<bool> comparison_1_ok(
                1,
                [&](bool* local_comparison){
                    std::fill_n(local_comparison, 1, true);
                });
        scal_field->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
                comparison_1_ok.getMine()[0] = (
                        comparison_1_ok.getMine()[0] &&
                        (scal_field->rval(rindex) ==
                         backup_field->rval(rindex)));
                });
        comparison_1_ok.mergeParallel(
                [&](const int idx,
                    const bool& v1,
                    const bool& v2) -> bool {
                return v1 && v2;});
        if (!comparison_1_ok.getMasterData()[0])
            DEBUG_MSG("error with binary comparison\n");
    }

    // deallocate
    delete kk;
    delete backup_field;
    delete scal_field;
    return EXIT_SUCCESS;
}

template class field_output_test<float>;
template class field_output_test<double>;

