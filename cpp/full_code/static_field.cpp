/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "static_field.hpp"
#include "scope_timer.hpp"
#include "fftw_tools.hpp"


template <typename rnumber>
int static_field<rnumber>::initialize(void)
{
    TIMEZONE("static_file::initialize");
    this->read_iteration();
    this->read_parameters();
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    this->grow_file_datasets();

    this->vorticity = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    this->vorticity->real_space_representation = false;

    this->velocity = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    this->velocity->real_space_representation = false;

    this->kk = new kspace<FFTW, SMOOTH>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);

    //read static vorticity field from iteration 0
    std::string checkpoint_fname = (
            std::string(this->simname) +
            std::string("_checkpoint_") +
            std::to_string(0) +
            std::string(".h5"));
    if (this->iteration == 0) {
        if (!hdf5_tools::field_exists(
                    checkpoint_fname,
                    "vorticity",
                    "complex",
                    0)) {
            // generate initial condition
            make_gaussian_random_field(
                this->kk,
                this->vorticity,
                this->field_random_seed,
                0.4,                        // injection rate
                1.0,                        // Lint
                1.5 / this->kk->kM,     // etaK
                6.78,
                0.40,
                3./2.);
            this->vorticity->symmetrize();
            this->kk->template project_divfree<rnumber>(
                    this->vorticity->get_cdata());
        } else {
            this->vorticity->io(
	            checkpoint_fname,
	            "vorticity",
	            0,
	            true);
        }
    } else {
        this->vorticity->io(
	        checkpoint_fname,
	        "vorticity",
	        0,
	        true);
    }

    // compute the velocity field and store
    invert_curl(
        this->kk,
        this->vorticity,
        velocity);
    // transform velocity and vorticity fields to real space
    this->velocity->ift();
    this->vorticity->ift();

    // initialize particles
    this->ps = particles_system_builder(
                this->velocity,              // (field object)
                this->kk,                     // (kspace object, contains dkx, dky, dkz)
                tracers0_integration_steps, // to check coherency between parameters and hdf input file (nb rhs)
                (long long int)nparticles,  // to check coherency between parameters and hdf input file
                this->get_current_fname(),    // particles input filename
                std::string("/tracers0/state/") + std::to_string(this->iteration), // dataset name for initial input
                std::string("/tracers0/rhs/")  + std::to_string(this->iteration),  // dataset name for initial input
                tracers0_neighbours,        // parameter (interpolation no neighbours)
                tracers0_smoothness,        // parameter
                this->comm,
                this->iteration+1);
    // initialize output objects
    this->particles_output_writer_mpi = new particles_output_hdf5<
        long long int, double, 3>(
                MPI_COMM_WORLD,
                "tracers0",
                nparticles,
                tracers0_integration_steps);
    this->particles_output_writer_mpi->setParticleFileLayout(this->ps->getParticleFileLayout());
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                MPI_COMM_WORLD,
                this->ps->getGlobalNbParticles(),
                (this->simname + "_particles.h5"),
                "tracers0",
                "position/0");
    this->particles_sample_writer_mpi->setParticleFileLayout(this->ps->getParticleFileLayout());

    return EXIT_SUCCESS;
}

template <typename rnumber>
int static_field<rnumber>::step(void)
{
    TIMEZONE("static_file::step");
    this->ps->completeLoop(this->dt);
    this->iteration++;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int static_field<rnumber>::write_checkpoint(void)
{
    TIMEZONE("static_file::write_checkpoint");
    this->particles_output_writer_mpi->open_file(this->get_current_fname());
    this->particles_output_writer_mpi->template save<3>(
            this->ps->getParticlesState(),
            this->ps->getParticlesRhs(),
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->iteration);
    this->particles_output_writer_mpi->close_file();
    this->write_iteration();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int static_field<rnumber>::finalize(void)
{
    TIMEZONE("static_field::finalize");
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    // good practice rule number n+1: always delete in inverse order of allocation
    delete this->ps.release();
    delete this->particles_output_writer_mpi;
    delete this->particles_sample_writer_mpi;
    delete this->kk;
    delete this->velocity;
    delete this->vorticity;
    return EXIT_SUCCESS;
}

/** \brief Compute statistics.
 *
 */

template <typename rnumber>
int static_field<rnumber>::do_stats()
{
    TIMEZONE("static_field::do_stats");
    /// either one of two conditions suffices to compute statistics:
    /// 1) current iteration is a multiple of niter_part
    /// 2) we are within niter_part_fine_duration/2 of a multiple of niter_part_fine_period
    if (!(this->iteration % this->niter_part == 0 ||
          ((this->iteration + this->niter_part_fine_duration/2) % this->niter_part_fine_period <=
           this->niter_part_fine_duration)))
        return EXIT_SUCCESS;

    // allocate temporary data array
    std::unique_ptr<double[]> pdata(new double[3*this->ps->getLocalNbParticles()]);

    /// copy position data

    /// sample position
    std::copy(this->ps->getParticlesState(),
              this->ps->getParticlesState()+3*this->ps->getLocalNbParticles(),
              pdata.get());
    this->particles_sample_writer_mpi->template save_dataset<3>(
            "tracers0",
            "position",
            this->ps->getParticlesState(),
            &pdata,
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->ps->get_step_idx()-1);

    /// sample velocity
    std::fill_n(pdata.get(), 3*this->ps->getLocalNbParticles(), 0);
    this->ps->sample_compute_field(*this->velocity, pdata.get());
    this->particles_sample_writer_mpi->template save_dataset<3>(
            "tracers0",
            "velocity",
            this->ps->getParticlesState(),
            &pdata,
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->ps->get_step_idx()-1);

    // deallocate temporary data array
    delete[] pdata.release();

    return EXIT_SUCCESS;
}

template <typename rnumber>
int static_field<rnumber>::read_parameters(void)
{
    TIMEZONE("static_field::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->fftw_plan_rigor = hdf5_tools::read_string(parameter_file, "parameters/fftw_plan_rigor");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "parameters/dt");
    this->niter_part = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part");
    this->niter_part_fine_period = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_period");
    this->niter_part_fine_duration = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_duration");
    this->nparticles = hdf5_tools::read_value<long long int>(parameter_file, "parameters/nparticles");
    this->tracers0_integration_steps = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_integration_steps");
    this->tracers0_neighbours = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_neighbours");
    this->tracers0_smoothness = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_smoothness");
    this->field_random_seed = hdf5_tools::read_value<int>(parameter_file, "parameters/field_random_seed");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class static_field<float>;
template class static_field<double>;

