/**********************************************************************
*                                                                     *
*  Copyright 2019 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef WRITE_FILTERED_TEST_HPP
#define WRITE_FILTERED_TEST_HPP



#include "field.hpp"
#include "full_code/test.hpp"

/** \brief A class for testing basic field class functionality.
 */

template <typename rnumber>
class write_filtered_test: public test
{
    public:
        write_filtered_test(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            test(
                    COMMUNICATOR,
                    simulation_name){}
        ~write_filtered_test(){}

        int initialize(void);
        int do_work(void);
        int finalize(void);
        int read_parameters(void);
};

#endif//WRITE_FILTERED_TEST_HPP

