/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "field_single_to_double.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int field_single_to_double<rnumber>::initialize(void)
{
    TIMEZONE("field_single_to_double::intialize");
    this->NSVE_field_stats<rnumber>::initialize();
    DEBUG_MSG("after NSVE_field_stats::initialize\n");
    this->kk = new kspace<FFTW, SMOOTH>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);
    this->vec_field_double = new field<double, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    this->vec_field_double->real_space_representation = false;
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    if (H5Lexists(parameter_file, "/parameters/checkpoints_per_file", H5P_DEFAULT))
    {
        hid_t dset = H5Dopen(parameter_file, "/parameters/checkpoints_per_file", H5P_DEFAULT);
        H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &this->checkpoints_per_file);
        H5Dclose(dset);
    }
    else
        this->checkpoints_per_file = 1;
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    DEBUG_MSG("before read_vector\n");
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/field_single_to_double/parameters/iteration_list");
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int field_single_to_double<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("field_single_to_double::work_on_current_iteration");
    DEBUG_MSG("before read_vorticity at iteration %d\n", this->iteration);
    this->read_current_cvorticity();
    DEBUG_MSG("after read_vorticity at iteration %d\n", this->iteration);

    // using CLOOP as opposed to a global std::copy because CLOOP
    // is openmp parallelized.
    this->kk->CLOOP(
                [&](ptrdiff_t cindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex){
        {
            std::copy(
                    (rnumber*)(this->vorticity->get_cdata() + cindex*3),
                    (rnumber*)(this->vorticity->get_cdata() + cindex*3) + 6,
                    (double*)(this->vec_field_double->get_cdata() + cindex*3));
        }
    }
    );

    std::string fname = (
            this->simname +
            std::string("_checkpoint_double_") +
            std::to_string(this->iteration / (this->niter_out*this->checkpoints_per_file)) +
            std::string(".h5"));
    this->vec_field_double->io(
            fname,
            "vorticity",
            this->iteration,
            false);

    return EXIT_SUCCESS;
}

template <typename rnumber>
int field_single_to_double<rnumber>::finalize(void)
{
    TIMEZONE("field_single_to_double::finalize");
    delete this->vec_field_double;
    delete this->kk;
    return EXIT_SUCCESS;
}

template class field_single_to_double<float>;

