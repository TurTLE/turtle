/**********************************************************************
*                                                                     *
*  Copyright 2019 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#include "dealias_test.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>
#include <random>

template <typename rnumber>
int dealias_test<rnumber>::initialize(void)
{
    TIMEZONE("dealias_test::initialize");
    this->read_parameters();
    this->vector_field0 = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);
    this->vector_field1 = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);
    this->kk = new kspace<FFTW, SMOOTH>(
            this->vector_field0->clayout, this->dkx, this->dky, this->dkz);
    this->kk4 = new kspace<FFTW, ONE_HALF>(
            this->vector_field0->clayout, this->dkx, this->dky, this->dkz);
    this->kk3 = new kspace<FFTW, TWO_THIRDS>(
            this->vector_field0->clayout, this->dkx, this->dky, this->dkz);

    if (this->myrank == 0)
    {
        hid_t stat_file = H5Fopen(
                (this->simname + std::string(".h5")).c_str(),
                H5F_ACC_RDWR,
                H5P_DEFAULT);
        this->kk->store(stat_file);
        H5Fclose(stat_file);
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int dealias_test<rnumber>::finalize(void)
{
    TIMEZONE("dealias_test::finalize");
    delete this->vector_field1;
    delete this->vector_field0;
    delete this->kk3;
    delete this->kk4;
    delete this->kk;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int dealias_test<rnumber>::read_parameters()
{
    TIMEZONE("dealias_test::read_parameters");
    this->test::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->max_velocity_estimate = hdf5_tools::read_value<double>(parameter_file, "/parameters/max_velocity_estimate");
    this->spectrum_dissipation = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_dissipation");
    this->spectrum_Lint = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_Lint");
    this->spectrum_etaK = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_etaK");
    this->spectrum_large_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_large_scale_const");
    this->spectrum_small_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_small_scale_const");
    this->random_seed = hdf5_tools::read_value<int>(parameter_file, "/parameters/field_random_seed");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int dealias_test<rnumber>::do_work(void)
{
    TIMEZONE("dealias_test::do_work");

    /// initialize vector Gaussian random field
    make_gaussian_random_field(
        this->kk,
        this->vector_field0,
        this->random_seed,
        this->spectrum_dissipation,
        this->spectrum_Lint,
        this->spectrum_etaK,
        this->spectrum_large_scale_const,
        this->spectrum_small_scale_const,
        3./2. //incompressibility projection factor
        );

    /// impose divergence free condition while maintaining the energy of the field
    this->kk->template project_divfree<rnumber>(this->vector_field0->get_cdata());

    /// initialize statistics file.
    hid_t stat_group, stat_file;
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
        stat_group = H5Gopen(
                stat_file,
                "statistics",
                H5P_DEFAULT);
    }
    else
    {
        stat_file = 0;
        stat_group = 0;
    }

    /// copy field to temporary field
    *this->vector_field1 = *this->vector_field0;

    /// dealias temporary field with two thirds sharp cutoff
    this->kk3->template dealias<rnumber, THREE>(this->vector_field1->get_cdata());

    /// compute basic statistics of temporary field
    this->vector_field1->compute_stats(
            this->kk,
            stat_group,
            "field3",
            0,
            this->max_velocity_estimate);

    /// copy field to temporary field
    *this->vector_field1 = *this->vector_field0;

    /// dealias temporary field with one half sharp cutoff
    this->kk4->template dealias<rnumber, THREE>(this->vector_field1->get_cdata());

    /// compute basic statistics of temporary field
    this->vector_field1->compute_stats(
            this->kk,
            stat_group,
            "field4",
            0,
            this->max_velocity_estimate);

    /// close stat file
    if (this->myrank == 0)
    {
        H5Gclose(stat_group);
        H5Fclose(stat_file);
    }
    return EXIT_SUCCESS;
}

template class dealias_test<float>;
template class dealias_test<double>;

