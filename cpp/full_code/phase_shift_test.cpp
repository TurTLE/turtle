/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "phase_shift_test.hpp"
#include "scope_timer.hpp"
#include <cmath>
#include <random>


template <typename rnumber>
int phase_shift_test<rnumber>::initialize(void)
{
    TIMEZONE("phase_shift_test::initialize");
    this->read_parameters();
    this->random_phase_seed = 1;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int phase_shift_test<rnumber>::finalize(void)
{
    TIMEZONE("phase_shift_test::finalize");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int phase_shift_test<rnumber>::read_parameters()
{
    TIMEZONE("phase_shift_test::read_parameters");
    this->test::read_parameters();
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int phase_shift_test<rnumber>::do_work(void)
{
    TIMEZONE("phase_shift_test::do_work");
    // allocate
    this->phase_field = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);
    this->phase_field->real_space_representation = false;
    this->kk = new kspace<FFTW, SMOOTH>(
            this->phase_field->clayout, this->dkx, this->dky, this->dkz);
    // phase field must be in Fourier space
    // then the following initializes real parts with 1, and imaginary parts with 0
    *this->phase_field = 1.0;
    // now apply random phase shift
    generate_random_phase_field(
            this->kk,
            this->phase_field,
            this->random_phase_seed);

    this->phase_field->io(
            this->simname + "_phase_field.h5",
            "random_phase_field",
            0,
            false);

    this->phase_field->ift();
    this->phase_field->dft();
    this->phase_field->normalize();

    this->phase_field->io(
            this->simname + "_phase_field.h5",
            "random_phase_field",
            1,
            false);

    field<rnumber, FFTW, THREE> *vf = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);
    make_gaussian_random_field(
        this->kk,
        vf,
        1,
        0.4,
        1,
        0.01,
        1,
        1,
        3./2. //incompressibility projection factor
        );
    this->kk->template project_divfree<rnumber>(vf->get_cdata());

    apply_phase_field_shift(
            this->kk,
            vf,
            this->phase_field);

    compute_divergence(
            this->kk,
            vf,
            this->phase_field);

    this->phase_field->ift();

    this->phase_field->io(
            this->simname + "_phase_field.h5",
            "divergence",
            1,
            false);

    // deallocate
    delete vf;
    delete this->phase_field;
    delete this->kk;
    return EXIT_SUCCESS;
}

template class phase_shift_test<float>;
template class phase_shift_test<double>;

