/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "filter_test.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int filter_test<rnumber>::initialize(void)
{
    TIMEZONE("filter_test::initialize");
    this->read_parameters();
    this->scal_field = new field<rnumber, FFTW, ONE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);
    this->kk = new kspace<FFTW, SMOOTH>(
            this->scal_field->clayout, this->dkx, this->dky, this->dkz);

    if (this->myrank == 0)
    {
        hid_t stat_file = H5Fopen(
                (this->simname + std::string(".h5")).c_str(),
                H5F_ACC_RDWR,
                H5P_DEFAULT);
        this->kk->store(stat_file);
        H5Fclose(stat_file);
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int filter_test<rnumber>::finalize(void)
{
    TIMEZONE("filter_test::finalize");
    delete this->scal_field;
    delete this->kk;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int filter_test<rnumber>::read_parameters()
{
    TIMEZONE("filter_test::read_parameters");
    this->test::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->filter_length = hdf5_tools::read_value<double>(parameter_file, "/parameters/filter_length");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int filter_test<rnumber>::reset_field(
        int dimension)
{
    TIMEZONE("filter_test::reset_field");
    this->scal_field->real_space_representation = true;
    *this->scal_field = 0.0;
    if (this->scal_field->rlayout->starts[0] == 0)
    {
        switch(dimension)
        {
            case 0:
                this->scal_field->rval(0) = 1.0 / (
                        (4*acos(0) / (this->nx*this->dkx))*
                        (4*acos(0) / (this->ny*this->dky))*
                        (4*acos(0) / (this->nz*this->dkz)));
                break;
            case 1:
                for (ptrdiff_t xindex = 0; xindex < this->nx; xindex++)
                    this->scal_field->rval(this->scal_field->get_rindex(xindex, 0, 0)) = 1.0 / (
                        (4*acos(0) / (this->ny*this->dky))*
                        (4*acos(0) / (this->nz*this->dkz)));
                break;
            case 2:
                for (ptrdiff_t yindex = 0; yindex < this->ny; yindex++)
                for (ptrdiff_t xindex = 0; xindex < this->nx; xindex++)
                {
                    this->scal_field->rval(
                            this->scal_field->get_rindex(xindex, yindex, 0)) = 1.0 / (
                        (4*acos(0) / (this->nz*this->dkz)));
                }
                break;
            default:
                break;
        }
    }
    this->scal_field->dft();
    this->scal_field->symmetrize();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int filter_test<rnumber>::do_work(void)
{
    TIMEZONE("filter_test::do_work");
    std::string filename = this->simname + std::string("_fields.h5");
    for (int dimension = 0; dimension < 3; dimension++)
    {
        this->reset_field(dimension);
        this->kk->template filter<rnumber, ONE>(
                this->scal_field->get_cdata(),
                4*acos(0) / this->filter_length,
                "sharp_Fourier_sphere");
        this->scal_field->ift();
        this->scal_field->normalize();
        this->scal_field->io(
                filename,
                "sharp_Fourier_sphere",
                dimension,
                false);
        this->reset_field(dimension);
        this->kk->template filter<rnumber, ONE>(
                this->scal_field->get_cdata(),
                4*acos(0) / this->filter_length,
                "Gauss");
        this->scal_field->ift();
        this->scal_field->normalize();
        this->scal_field->io(
                filename,
                "Gauss",
                dimension,
                false);
        this->reset_field(dimension);
        this->kk->template filter<rnumber, ONE>(
                this->scal_field->get_cdata(),
                4*acos(0) / this->filter_length,
                "ball");
        this->scal_field->ift();
        this->scal_field->normalize();
        this->scal_field->io(
                filename,
                "ball",
                dimension,
                false);
    }
    return EXIT_SUCCESS;
}

template class filter_test<float>;
template class filter_test<double>;

