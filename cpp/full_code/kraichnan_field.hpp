/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef KRAICHNAN_FIELD_HPP
#define KRAICHNAN_FIELD_HPP



#include "full_code/direct_numerical_simulation.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_sampling.hpp"

template <typename rnumber>
class kraichnan_field: public direct_numerical_simulation
{
    public:

        /* parameters that are read in read_parameters */
        double dt;
        std::string fftw_plan_rigor;

        /* particle parameters */
        int niter_part;
        int niter_part_fine_period;
        int niter_part_fine_duration;
        long long int nparticles;
        int tracers0_integration_steps;
        int tracers0_neighbours;
        int tracers0_smoothness;
        int output_velocity;

        /* other stuff */
        kspace<FFTW, SMOOTH> *kk;
        field<rnumber, FFTW, THREE> *velocity;
        double spectrum_dissipation;
        double spectrum_Lint;
        double spectrum_etaK;
        double spectrum_large_scale_const;
        double spectrum_small_scale_const;
        double max_velocity_estimate;
        int field_random_seed;

        /* other stuff */
        std::unique_ptr<abstract_particles_system<long long int, double>> ps;

        particles_output_hdf5<long long int, double,3> *particles_output_writer_mpi;
        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;


        kraichnan_field(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            direct_numerical_simulation(
                    COMMUNICATOR,
                    simulation_name){}
        ~kraichnan_field(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        virtual int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);

        int generate_random_velocity(void);
        void update_checkpoint(void);
};

#endif//KRAICHNAN_FIELD_HPP

