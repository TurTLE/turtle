/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef NSVE_FIELD_STATS_HPP
#define NSVE_FIELD_STATS_HPP

#include "field_binary_IO.hpp"
#include "full_code/postprocess.hpp"

template <typename rnumber>
class NSVE_field_stats: public postprocess
{
    private:
        field_binary_IO<rnumber, COMPLEX, THREE> *bin_IO;
    public:
        int niter_out;

        std::string fftw_plan_rigor;

        field<rnumber, FFTW, THREE> *vorticity;

        NSVE_field_stats(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            postprocess(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~NSVE_field_stats() noexcept(false){}

        virtual int read_parameters(void);
        virtual int initialize(void);
        virtual int work_on_current_iteration(void);
        virtual int finalize(void);

        int read_current_cvorticity(void);
        int read_arbitrary_cvorticity(int iter);
};

#endif//NSVE_FIELD_STATS_HPP

