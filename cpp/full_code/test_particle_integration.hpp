/******************************************************************************
*                                                                             *
*  Copyright 2022 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef TEST_PARTICLE_INTEGRATION_HPP
#define TEST_PARTICLE_INTEGRATION_HPP



#include "full_code/test.hpp"
#include "particles/particle_solver.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/interpolation/field_tinterpolator.hpp"

/** \brief Test of interpolation methods.
 *
 */

template <typename rnumber>
class test_particle_integration: public test
{
    public:
        int nxparticles;
        int nzparticles;
        double dt;
        int niter_todo;
        int random_seed;


        field_tinterpolator<rnumber, FFTW, THREE, NONE> *velocity_front;

        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;

        field<rnumber, FFTW, THREE> *velocity_back;

        kspace<FFTW, SMOOTH> *kk;

        test_particle_integration(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            test(
                    COMMUNICATOR,
                    simulation_name),
            random_seed(0),
            velocity_front(nullptr),
            particles_sample_writer_mpi(nullptr),
            velocity_back(nullptr),
            kk(nullptr){}
        ~test_particle_integration(){}

        int initialize(void);
        int do_work(void);
        int finalize(void);

        int read_parameters(void);

        template <int neighbours, int smoothness>
            int integrate(
                    const int order,
                    const int factor,
                    const int total_iterations);
};

#endif//TEST_PARTICLE_INTEGRATION_HPP

