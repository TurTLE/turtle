/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "symmetrize_test.hpp"
#include "fftw_tools.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

template <typename rnumber>
int symmetrize_test<rnumber>::initialize(void)
{
    TIMEZONE("symmetrize_test::initialize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int symmetrize_test<rnumber>::finalize(void)
{
    TIMEZONE("symmetrize_test::finalize");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int symmetrize_test<rnumber>::read_parameters()
{
    TIMEZONE("symmetrize_test::read_parameters");
    this->test::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->random_seed = hdf5_tools::read_value<int>(
            parameter_file, "/parameters/random_seed");
    this->fftw_plan_rigor = hdf5_tools::read_string(parameter_file, "parameters/fftw_plan_rigor");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

/** \brief Sanity check for symmetrize
 *
 * Generate a random field, apply symmetrize to it, call the result f0.
 * Make a copy, and perform an ift+dft+normalize, call the result f1.
 * Compare f0 with f1, they should be fairly close. Since we know that
 * ift+dft+normalize only changes the field within numerical precision
 * if it's properly Hermitian, then any difference between f0 and f1 is
 * due to our own symmetrize being broken somehow.
 * */

template <typename rnumber>
int symmetrize_test<rnumber>::do_work(void)
{
    TIMEZONE("symmetrize_test::do_work");
    // allocate
    DEBUG_MSG("about to allocate field0\n");
    field<rnumber, FFTW, THREE> *test_field0 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    DEBUG_MSG("finished allocating field0\n");
    DEBUG_MSG("about to allocate field1\n");
    field<rnumber, FFTW, THREE> *test_field1 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    DEBUG_MSG("finished allocating field1\n");
    kspace<FFTW,SMOOTH> *kk = new kspace<FFTW, SMOOTH>(
            test_field0->clayout, this->dkx, this->dky, this->dkz);

    if (this->myrank == 0)
    {
        hid_t stat_file = H5Fopen(
                (this->simname + std::string(".h5")).c_str(),
                H5F_ACC_RDWR,
                H5P_DEFAULT);
        kk->store(stat_file);
        H5Fclose(stat_file);
    }

    // compute minimum available lengthscale
    int max_n = this->nx;
    if (this->ny > max_n)
        max_n = this->ny;
    if (this->nz > max_n)
        max_n = this->nz;
    double min_lengthscale = 8*acos(0) / max_n;
    /*************************************************
     * here we test that after applying symmetrize
     * field is actually Hermitian-symmetric
     *************************************************/
    // fill up test_field0
    *test_field0 = rnumber(0.0);
    make_gaussian_random_field<rnumber, FFTW, THREE, SMOOTH>(
            kk,
            test_field0,
            1,
            0.4,
            1.0,
            min_lengthscale);
    // make the field divergence free
    kk->template force_divfree<rnumber>(test_field0->get_cdata());
    // apply symmetrize to test_field0
    test_field0->symmetrize();


    // make copy in test_field1
    // this MUST be made after symmetrizing test_field0
    // (alternatively, we may symmetrize test_field1 as well before the ift-dft cycle
    *test_field1 = test_field0->get_cdata();

    // go back and forth with test_field1, to enforce symmetry
    test_field1->symmetrize_FFT();

    // temporary variables for field comparison
    double max_diff;
    ptrdiff_t ix, iy, iz;
    variable_used_only_in_assert(ix);
    variable_used_only_in_assert(iy);
    variable_used_only_in_assert(iz);
    double k_at_max_diff;
    variable_used_only_in_assert(k_at_max_diff);
    double a0, a1;
    variable_used_only_in_assert(a0);
    variable_used_only_in_assert(a1);

    // now compare the two fields
    max_diff = 0;
    k_at_max_diff = 0;
    kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            double diff_re0 = test_field0->cval(cindex, 0, 0) - test_field1->cval(cindex, 0, 0);
            double diff_re1 = test_field0->cval(cindex, 1, 0) - test_field1->cval(cindex, 1, 0);
            double diff_re2 = test_field0->cval(cindex, 2, 0) - test_field1->cval(cindex, 2, 0);
            double diff_im0 = test_field0->cval(cindex, 0, 1) - test_field1->cval(cindex, 0, 1);
            double diff_im1 = test_field0->cval(cindex, 1, 1) - test_field1->cval(cindex, 1, 1);
            double diff_im2 = test_field0->cval(cindex, 2, 1) - test_field1->cval(cindex, 2, 1);
            double diff = std::sqrt(
                    diff_re0*diff_re0 + diff_re1*diff_re1 + diff_re2*diff_re2 +
                    diff_im0*diff_im0 + diff_im1*diff_im1 + diff_im2*diff_im2);
            double amplitude0 = (test_field0->cval(cindex, 0, 0)*test_field0->cval(cindex, 0, 0) +
                                 test_field0->cval(cindex, 1, 0)*test_field0->cval(cindex, 1, 0) +
                                 test_field0->cval(cindex, 2, 0)*test_field0->cval(cindex, 2, 0) +
                                 test_field0->cval(cindex, 0, 1)*test_field0->cval(cindex, 0, 1) +
                                 test_field0->cval(cindex, 1, 1)*test_field0->cval(cindex, 1, 1) +
                                 test_field0->cval(cindex, 2, 1)*test_field0->cval(cindex, 2, 1));
            double amplitude1 = (test_field1->cval(cindex, 0, 0)*test_field1->cval(cindex, 0, 0) +
                                 test_field1->cval(cindex, 1, 0)*test_field1->cval(cindex, 1, 0) +
                                 test_field1->cval(cindex, 2, 0)*test_field1->cval(cindex, 2, 0) +
                                 test_field1->cval(cindex, 0, 1)*test_field1->cval(cindex, 0, 1) +
                                 test_field1->cval(cindex, 1, 1)*test_field1->cval(cindex, 1, 1) +
                                 test_field1->cval(cindex, 2, 1)*test_field1->cval(cindex, 2, 1));
            double amplitude = std::sqrt((amplitude0 + amplitude1)/2);
            if (amplitude > 0)
            if (diff/amplitude > max_diff)
            {
                max_diff = diff / amplitude;
                ix = xindex;
                iy = yindex + test_field0->clayout->starts[0];
                iz = zindex;
                k_at_max_diff = std::sqrt(k2);
                a0 = std::sqrt(amplitude0);
                a1 = std::sqrt(amplitude1);
            }
            });
    DEBUG_MSG("rseed1 found maximum relative difference %g at ix = %ld, iy = %ld, iz = %ld, wavenumber = %g, amplitudes %g %g\n",
            max_diff, ix, iy, iz, k_at_max_diff, a0, a1);

    test_field0->io(
            this->simname + "_fields.h5",
            "field0",
            0,
            false);
    test_field1->io(
            this->simname + "_fields.h5",
            "field1",
            0,
            false);
    test_field1->ift();
    test_field1->io(
            this->simname + "_fields.h5",
            "field1",
            0,
            false);

    /*************************************************
     * here we test that applying our symmetrize, or
     * just using fft, leads to the same result.
     * there's no guarantee that this will work though,
     * since FFT documentation doesn't say IFT+DFT+normalize leads to
     * arithmetic mean-based Hermitian symmetry.
     *************************************************/

    make_gaussian_random_field<rnumber, FFTW, THREE, SMOOTH>(
            kk,
            test_field0,
            2,
            0.4,
            1.0,
            min_lengthscale);

    // copy unmodified data
    *test_field1 = test_field0->get_cdata();

    // apply different symmetrize methods
    test_field0->symmetrize();
    test_field1->symmetrize_FFT();

    // now compare the two fields
    max_diff = 0;
    k_at_max_diff = 0;
    kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            double diff_re0 = test_field0->cval(cindex, 0, 0) - test_field1->cval(cindex, 0, 0);
            double diff_re1 = test_field0->cval(cindex, 1, 0) - test_field1->cval(cindex, 1, 0);
            double diff_re2 = test_field0->cval(cindex, 2, 0) - test_field1->cval(cindex, 2, 0);
            double diff_im0 = test_field0->cval(cindex, 0, 1) - test_field1->cval(cindex, 0, 1);
            double diff_im1 = test_field0->cval(cindex, 1, 1) - test_field1->cval(cindex, 1, 1);
            double diff_im2 = test_field0->cval(cindex, 2, 1) - test_field1->cval(cindex, 2, 1);
            double diff = std::sqrt(
                    diff_re0*diff_re0 + diff_re1*diff_re1 + diff_re2*diff_re2 +
                    diff_im0*diff_im0 + diff_im1*diff_im1 + diff_im2*diff_im2);
            double amplitude0 = (test_field0->cval(cindex, 0, 0)*test_field0->cval(cindex, 0, 0) +
                                 test_field0->cval(cindex, 1, 0)*test_field0->cval(cindex, 1, 0) +
                                 test_field0->cval(cindex, 2, 0)*test_field0->cval(cindex, 2, 0) +
                                 test_field0->cval(cindex, 0, 1)*test_field0->cval(cindex, 0, 1) +
                                 test_field0->cval(cindex, 1, 1)*test_field0->cval(cindex, 1, 1) +
                                 test_field0->cval(cindex, 2, 1)*test_field0->cval(cindex, 2, 1));
            double amplitude1 = (test_field1->cval(cindex, 0, 0)*test_field1->cval(cindex, 0, 0) +
                                 test_field1->cval(cindex, 1, 0)*test_field1->cval(cindex, 1, 0) +
                                 test_field1->cval(cindex, 2, 0)*test_field1->cval(cindex, 2, 0) +
                                 test_field1->cval(cindex, 0, 1)*test_field1->cval(cindex, 0, 1) +
                                 test_field1->cval(cindex, 1, 1)*test_field1->cval(cindex, 1, 1) +
                                 test_field1->cval(cindex, 2, 1)*test_field1->cval(cindex, 2, 1));
            double amplitude = std::sqrt((amplitude0 + amplitude1)/2);
            if (amplitude > 0)
            if (diff/amplitude > max_diff)
            {
                max_diff = diff / amplitude;
                ix = xindex;
                iy = yindex + test_field0->clayout->starts[0];
                iz = zindex;
                k_at_max_diff = std::sqrt(k2);
                a0 = std::sqrt(amplitude0);
                a1 = std::sqrt(amplitude1);
            }
            });
    DEBUG_MSG("rseed2 found maximum relative difference %g at ix = %ld, iy = %ld, iz = %ld, wavenumber = %g, amplitudes %g %g\n",
            max_diff, ix, iy, iz, k_at_max_diff, a0, a1);

    // output fields for possible subsequent comparison
    test_field0->io(
            this->simname + "_fields.h5",
            "rseed2_field0",
            0,
            false);
    test_field1->io(
            this->simname + "_fields.h5",
            "rseed2_field1",
            0,
            false);

    // deallocate
    delete kk;
    delete test_field1;
    delete test_field0;
    return EXIT_SUCCESS;
}

template class symmetrize_test<float>;
template class symmetrize_test<double>;

