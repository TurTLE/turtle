/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "test_tracer_set.hpp"
#include "particles/rhs/tracer_with_collision_counter_rhs.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/particle_solver.hpp"
#include "particles/particles_input_random.hpp"

template <typename rnumber>
int test_tracer_set<rnumber>::initialize(void)
{
    TIMEZONE("test_tracer_set::initialize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int test_tracer_set<rnumber>::finalize(void)
{
    TIMEZONE("test_tracer_set::finalize");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int test_tracer_set<rnumber>::read_parameters()
{
    TIMEZONE("test_tracer_set::read_parameters");
    this->test::read_parameters();
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int test_tracer_set<rnumber>::do_work(void)
{
    TIMEZONE("test_tracer_set::do_work");
    const int nparticles = 1001;
    // allocate
    field<rnumber, FFTW, THREE> *vec_field = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);

    kspace<FFTW, SMOOTH> *kk = new kspace<FFTW, SMOOTH>(
            vec_field->clayout,
            this->dkx, this->dky, this->dkz);

    // initialize field
    vec_field->real_space_representation = false;
    make_gaussian_random_field(
            kk,
            vec_field,
            1 // rseed
            );
    vec_field->ift();

    field_tinterpolator<rnumber, FFTW, THREE, NONE> *fti = new field_tinterpolator<rnumber, FFTW, THREE, NONE>();
    tracer_with_collision_counter_rhs<rnumber, FFTW, NONE> *trhs = new tracer_with_collision_counter_rhs<rnumber, FFTW, NONE>();

    particle_set<3, 2, 1> pset(
            vec_field->rlayout,
            this->dkx,
            this->dky,
            this->dkz,
            0.5);

    // initialize particles
    particles_input_random<long long int, double, 3> bla(
            this->comm,
            nparticles,
            1,
            pset.getSpatialLowLimitZ(),
            pset.getSpatialUpLimitZ());
    pset.init(bla);

    // initialize particle output object
    particles_output_hdf5<long long int, double, 3> *particle_output_writer = new particles_output_hdf5<
        long long int, double, 3>(
                MPI_COMM_WORLD,
                "tracers0",
                pset.getTotalNumberOfParticles(),
                0);
    particles_output_sampling_hdf5<long long int, double, double, 3> *particle_sample_writer = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                MPI_COMM_WORLD,
                pset.getTotalNumberOfParticles(),
                "test_particle_sample.h5",
                "tracers0",
                "position/0");

    fti->set_field(vec_field);
    trhs->setVelocity(fti);

    particle_solver psolver(pset, 0);

    particle_output_writer->open_file("test_particle_checkpoint.h5");
    psolver.template writeCheckpoint<3>(particle_output_writer);
    pset.writeSample(
            vec_field,
            particle_sample_writer,
            "tracers0",
            "velocity",
            psolver.getIteration());
    pset.writeStateTriplet(
            0,
            particle_sample_writer,
            "tracers0",
            "position",
            psolver.getIteration());
    psolver.Heun(0.001, *trhs);
    psolver.setIteration(1);
    psolver.template writeCheckpoint<3>(particle_output_writer);
    pset.writeSample(
            vec_field,
            particle_sample_writer,
            "tracers0",
            "velocity",
            psolver.getIteration());
    pset.writeStateTriplet(
            0,
            particle_sample_writer,
            "tracers0",
            "position",
            psolver.getIteration());
    particle_output_writer->close_file();

    // deallocate
    delete particle_sample_writer;
    delete particle_output_writer;
    delete trhs;
    delete fti;
    delete kk;
    delete vec_field;
    return EXIT_SUCCESS;
}

template class test_tracer_set<float>;
template class test_tracer_set<double>;

