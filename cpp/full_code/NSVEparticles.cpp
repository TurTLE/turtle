/**********************************************************************
*                                                                     *
*  Copyright 2019 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#include "NSVEparticles.hpp"
#include "particles/interpolation/particle_set.hpp"
#include "particles/particles_input_random.hpp"

#include <cmath>

template <typename rnumber>
int NSVEparticles<rnumber>::initialize(void)
{
    TIMEZONE("NSVEparticles::intialize");
    this->NSVE<rnumber>::initialize();

    // initialize particle output object
    // this may be needed before the particle system,
    // if cpp_random_particles is true
    this->particles_output_writer_mpi = new particles_output_hdf5<
        long long int, double, 3>(
                MPI_COMM_WORLD,
                "tracers0",
                nparticles,
                tracers0_integration_steps);

    // if this is the first iteration, and we are supposed to generate our own random particles,
    // we do that here, and then we write them to file for future reference.
    if ((this->cpp_random_particles > 0) &&
         this->fs->iteration == 0)
    {
        // temporary particle set
        // interpolation setup is irrelevant for output
        particle_set<3, 1, 1> pset(
                this->fs->cvelocity->rlayout,
                this->fs->kk->dkx,
                this->fs->kk->dky,
                this->fs->kk->dkz);
        particles_input_random<long long int, double, 3> pinput(
                this->comm,
                this->nparticles,
                this->cpp_random_particles, // seed
                pset.getSpatialLowLimitZ(),
                pset.getSpatialUpLimitZ());
        // initialize particle set
        pset.init(pinput);
        // allocate dummy rhs data
        // we must fill these arrays with 0
        // otherwise we get floating point exceptions because data is being written/read
        std::vector<std::unique_ptr<double[]>> rhs_data;
        rhs_data.resize(tracers0_integration_steps);
        for (int counter = 0; counter < tracers0_integration_steps; counter++)
        {
            rhs_data[counter].reset(new double[pset.getLocalNumberOfParticles()*3]); // memory allocated here will be freed outside of the "if" block
            std::fill_n(rhs_data[counter].get(), pset.getLocalNumberOfParticles()*3, 0);
        }
        // create dummy file_layout object
        std::vector<hsize_t> file_layout;
        file_layout.resize(1);
        file_layout[0] = this->nparticles;
        // write data
        this->particles_output_writer_mpi->open_file(this->fs->get_current_fname());
        this->particles_output_writer_mpi->update_particle_species_name("tracers0");
        this->particles_output_writer_mpi->setParticleFileLayout(file_layout);
#ifdef USE_TIMING_OUTPUT
        // barrier so that timings within save are more accurate.
        MPI_Barrier(this->comm);
#endif
        this->particles_output_writer_mpi->template save<3>(
                pset.getParticleState(),
                rhs_data.data(),
                pset.getParticleIndices(),
                pset.getLocalNumberOfParticles(),
                0);
        this->particles_output_writer_mpi->close_file();
    }

    DEBUG_MSG("fs->iteration = %d\n", this->fs->iteration);
    DEBUG_MSG_WAIT(MPI_COMM_WORLD, "about to call particles_system_builder\n");
    this->ps = particles_system_builder(
                this->fs->cvelocity,              // (field object)
                this->fs->kk,                     // (kspace object, contains dkx, dky, dkz)
                tracers0_integration_steps, // to check coherency between parameters and hdf input file (nb rhs)
                (long long int)nparticles,  // to check coherency between parameters and hdf input file
                this->fs->get_current_fname(),    // particles input filename
                std::string("/tracers0/state/") + std::to_string(this->fs->iteration), // dataset name for initial input
                std::string("/tracers0/rhs/")  + std::to_string(this->fs->iteration),  // dataset name for initial input
                tracers0_neighbours,        // parameter (interpolation no neighbours)
                tracers0_smoothness,        // parameter
                this->comm,
                this->fs->iteration+1);
    DEBUG_MSG_WAIT(MPI_COMM_WORLD, "after call to particles_system_builder\n");

    // initialize sample object
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                MPI_COMM_WORLD,
                this->ps->getGlobalNbParticles(),
                (this->simname + "_particles.h5"),
                "tracers0",
                "position/0");

    // set particle file layout for output objects
    this->particles_output_writer_mpi->setParticleFileLayout(this->ps->getParticleFileLayout());
    this->particles_sample_writer_mpi->setParticleFileLayout(this->ps->getParticleFileLayout());
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticles<rnumber>::step(void)
{
    TIMEZONE("NSVEparticles::step");
    this->fs->compute_velocity(this->fs->cvorticity);
    this->fs->cvelocity->ift();
#ifdef USE_TIMING_OUTPUT
    // barrier so that timings for completeLoop are more accurate
    MPI_Barrier(this->comm);
#endif
    this->ps->completeLoop(this->dt);
#ifdef USE_TIMING_OUTPUT
    // barrier so that timings for step are more accurate
    MPI_Barrier(this->comm);
#endif
    this->NSVE<rnumber>::step();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticles<rnumber>::write_checkpoint(void)
{
    TIMEZONE("NSVEparticles::write_checkpoint");
    this->NSVE<rnumber>::write_checkpoint();
    this->particles_output_writer_mpi->open_file(this->fs->get_current_fname());
    this->particles_output_writer_mpi->template save<3>(
            this->ps->getParticlesState(),
            this->ps->getParticlesRhs(),
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->fs->iteration);
    this->particles_output_writer_mpi->close_file();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticles<rnumber>::finalize(void)
{
    TIMEZONE("NSVEparticles::finalize");
    delete this->ps.release();
    delete this->particles_output_writer_mpi;
    delete this->particles_sample_writer_mpi;
    this->NSVE<rnumber>::finalize();
    return EXIT_SUCCESS;
}

/** \brief Compute fluid stats and sample fields at particle locations.
 */

template <typename rnumber>
int NSVEparticles<rnumber>::do_stats()
{
    TIMEZONE("NSVEparticles::do_stats");
    /// fluid stats go here
    this->NSVE<rnumber>::do_stats();


    start_mpi_profiling_zone(turtle_mpi_pcontrol::PARTICLES);
    /// either one of two conditions suffices to compute statistics:
    /// 1) current iteration is a multiple of niter_part
    /// 2) we are within niter_part_fine_duration/2 of a multiple of niter_part_fine_period
    if (!(this->iteration % this->niter_part == 0 ||
          ((this->iteration + this->niter_part_fine_duration/2) % this->niter_part_fine_period <=
           this->niter_part_fine_duration)))
        return EXIT_SUCCESS;

    // allocate temporary data array
    std::unique_ptr<double[]> pdata(new double[3*this->ps->getLocalNbParticles()]);

    /// copy position data

    /// sample position
    std::copy(this->ps->getParticlesState(),
              this->ps->getParticlesState()+3*this->ps->getLocalNbParticles(),
              pdata.get());
    this->particles_sample_writer_mpi->template save_dataset<3>(
            "tracers0",
            "position",
            this->ps->getParticlesState(),
            &pdata,
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->ps->get_step_idx()-1);

    /// sample velocity
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::PARTICLES);
    if (!(this->iteration % this->niter_stat == 0))
    {
        // we need to compute velocity field manually, because it didn't happen in NSVE::do_stats()
        this->fs->compute_velocity(this->fs->cvorticity);
        *this->tmp_vec_field = this->fs->cvelocity->get_cdata();
        this->tmp_vec_field->ift();
    }
    start_mpi_profiling_zone(turtle_mpi_pcontrol::PARTICLES);
    std::fill_n(pdata.get(), 3*this->ps->getLocalNbParticles(), 0);
    this->ps->sample_compute_field(*this->tmp_vec_field, pdata.get());
    this->particles_sample_writer_mpi->template save_dataset<3>(
            "tracers0",
            "velocity",
            this->ps->getParticlesState(),
            &pdata,
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->ps->get_step_idx()-1);
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::PARTICLES);

    /// compute acceleration and sample it
    if (this->sample_acceleration == 1)
    {
        this->fs->compute_Lagrangian_acceleration(this->tmp_vec_field);
        this->tmp_vec_field->ift();
        start_mpi_profiling_zone(turtle_mpi_pcontrol::PARTICLES);
        std::fill_n(pdata.get(), 3*this->ps->getLocalNbParticles(), 0);
        this->ps->sample_compute_field(*this->tmp_vec_field, pdata.get());
        this->particles_sample_writer_mpi->template save_dataset<3>(
                "tracers0",
                "acceleration",
                this->ps->getParticlesState(),
                &pdata,
                this->ps->getParticlesIndexes(),
                this->ps->getLocalNbParticles(),
                this->ps->get_step_idx()-1);
    }

    // deallocate temporary data array
    delete[] pdata.release();
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::PARTICLES);

    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEparticles<rnumber>::read_parameters(void)
{
    TIMEZONE("NSVEparticles::read_parameters");
    this->NSVE<rnumber>::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->niter_part = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part");
    this->niter_part_fine_period = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_period");
    this->niter_part_fine_duration = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_duration");
    this->nparticles = hdf5_tools::read_value<long long int>(parameter_file, "parameters/nparticles");
    this->cpp_random_particles = hdf5_tools::read_value<int>(parameter_file, "parameters/cpp_random_particles");
    this->tracers0_integration_steps = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_integration_steps");
    this->tracers0_neighbours = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_neighbours");
    this->tracers0_smoothness = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_smoothness");
    this->sample_acceleration = hdf5_tools::read_value<int>(parameter_file, "parameters/sample_acceleration");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class NSVEparticles<float>;
template class NSVEparticles<double>;

