/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef NSVE_HPP
#define NSVE_HPP


#include "vorticity_equation.hpp"
#include "full_code/direct_numerical_simulation.hpp"

/** \brief Navier-Stokes solver.
 *
 * Solves the vorticity equation for a Navier-Stokes fluid.
 *
 * Allocates 1 vorticity equation and 1 vector field.
 */


template <typename rnumber>
class NSVE: public direct_numerical_simulation
{
    public:

        /* parameters that are read in read_parameters */
        double dt;
        double famplitude;
        double friction_coefficient;
        double variation_strength;
        double variation_time_scale;
        double fk0;
        double fk1;
        double energy;
        double injection_rate;
        int fmode;
        int field_random_seed;
        std::string forcing_type;
        int histogram_bins;
        double max_velocity_estimate;
        double max_vorticity_estimate;
        double nu;
        std::string fftw_plan_rigor;

        /* other stuff */
        vorticity_equation<rnumber, FFTW> *fs;
        field<rnumber, FFTW, THREE> *tmp_vec_field;
        field<rnumber, FFTW, ONE> *tmp_scal_field;


        NSVE(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            direct_numerical_simulation(
                    COMMUNICATOR,
                    simulation_name){}
        ~NSVE() noexcept(false){}

        int initialize(void);
        int allocate(void);
        int deallocate(void);
        int step(void);
        int finalize(void);

        virtual int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);

        int print_debug_info();
};

#endif//NSVE_HPP

