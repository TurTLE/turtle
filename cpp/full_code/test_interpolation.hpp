/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef TEST_INTERPOLATION_HPP
#define TEST_INTERPOLATION_HPP



#include "full_code/test.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_sampling.hpp"

/** \brief Interpolation tester.
 *
 */

template <typename rnumber>
class test_interpolation: public test
{
    public:
        long long int nparticles;
        int tracers0_integration_steps;
        int tracers0_neighbours;
        int tracers0_smoothness;

        std::unique_ptr<abstract_particles_system<long long int, double>> ps;

        particles_output_hdf5<long long int, double,3> *particles_output_writer_mpi;
        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;

        field<rnumber, FFTW, THREE> *velocity, *vorticity;
        field<rnumber, FFTW, THREExTHREE> *nabla_u;

        kspace<FFTW, SMOOTH> *kk;

        test_interpolation(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            test(
                    COMMUNICATOR,
                    simulation_name),
            particles_output_writer_mpi(nullptr),
            particles_sample_writer_mpi(nullptr),
            velocity(nullptr),
            vorticity(nullptr),
            nabla_u(nullptr),
            kk(nullptr) {}
        ~test_interpolation(){}

        int initialize(void);
        int do_work(void);
        int finalize(void);

        int read_parameters(void);
};

#endif//TEST_INTERPOLATION_HPP

