/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include <cmath>
#include "native_binary_to_hdf5.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"


template <typename rnumber>
int native_binary_to_hdf5<rnumber>::initialize(void)
{
    TIMEZONE("native_binary_to_hdf5::initialize");
    this->read_parameters();
    this->vec_field = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);
    this->vec_field->real_space_representation = false;
    this->bin_IO = new field_binary_IO<rnumber, COMPLEX, THREE>(
            this->vec_field->clayout->sizes,
            this->vec_field->clayout->subsizes,
            this->vec_field->clayout->starts,
            this->vec_field->clayout->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int native_binary_to_hdf5<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("native_binary_to_hdf5::work_on_current_iteration");
    char itername[16];
    sprintf(itername, "i%.5x", this->iteration);
    std::string native_binary_fname = (
            this->simname +
            std::string("_cvorticity_") +
            std::string(itername));
    this->bin_IO->read(
            native_binary_fname,
            this->vec_field->get_cdata());
    this->vec_field->io(
            (native_binary_fname +
             std::string(".h5")),
            "vorticity",
            this->iteration,
            false);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int native_binary_to_hdf5<rnumber>::finalize(void)
{
    TIMEZONE("native_binary_to_hdf5::finalize");
    delete this->bin_IO;
    delete this->vec_field;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int native_binary_to_hdf5<rnumber>::read_parameters(void)
{
    TIMEZONE("native_binary_to_hdf5::read_parameters");
    this->postprocess::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/native_binary_to_hdf5/iteration_list");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class native_binary_to_hdf5<float>;
template class native_binary_to_hdf5<double>;

