/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef WRITE_RPRESSURE
#define WRITE_RPRESSURE

#include "full_code/NSVE_field_stats.hpp"

/** generate and write out pressure field in real space **/

template <typename rnumber>
class write_rpressure: public NSVE_field_stats<rnumber>
{
    public:
        vorticity_equation<rnumber, FFTW> *ve;
        field<rnumber, FFTW, ONE> *pressure;
        field_binary_IO <rnumber, REAL, ONE> *bin_IO_scalar;
        int clip_zero_padding(field <rnumber, FFTW, ONE>* );

        write_rpressure(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~write_rpressure() noexcept(false){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};


#endif//WRITE_PRESSURE

