/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef NSVEP_EXTRA_SAMPLING_HPP
#define NSVEP_EXTRA_SAMPLING_HPP



#include "full_code/NSVEparticles.hpp"

/** \brief Navier-Stokes solver with tracers that sample velocity gradient
 *  and pressure Hessian.
 *
 */

template <typename rnumber>
class NSVEp_extra_sampling: public NSVEparticles<rnumber>
{
    public:
        /* specific parameters */
        int sample_pressure;
        int sample_pressure_gradient;
        int sample_pressure_Hessian;
        int sample_velocity_gradient;

        /* other stuff */
        field<rnumber, FFTW, ONE> *pressure;
        field<rnumber, FFTW, THREE> *nabla_p;
        field<rnumber, FFTW, THREExTHREE> *nabla_u;
        field<rnumber, FFTW, THREExTHREE> *Hessian_p;

        NSVEp_extra_sampling(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVEparticles<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        ~NSVEp_extra_sampling(){}

        int initialize(void);
        int finalize(void);

        int read_parameters(void);
        int do_stats(void);
};

#endif//NSVEP_EXTRA_SAMPLING_HPP


