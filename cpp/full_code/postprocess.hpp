/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef POSTPROCESS_HPP
#define POSTPROCESS_HPP

#include "full_code/code_base.hpp"
#include "vorticity_equation.hpp"
#include "full_code/NSE.hpp"
#include "full_code/NSVE.hpp"

class postprocess: public code_base
{
    public:
        std::vector<int> iteration_list;
        hid_t stat_file;

        /* parameters that are read in read_parameters.
         * This includes physical parameters, such that children of `postprocess`
         * do not need to read them (but they do need to be copied with
         * the `copy_parameters_into` method or similar.
         * */
        double dt;
        double famplitude;
        double friction_coefficient;
        double fk0;
        double fk1;
        double energy;
        double injection_rate;
        double variation_strength;
        double variation_time_scale;
        int fmode;
        std::string forcing_type;
        double nu;

        postprocess(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            code_base(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~postprocess() noexcept(false){}

        virtual int initialize(void) = 0;
        virtual int work_on_current_iteration(void) = 0;
        virtual int finalize(void) = 0;

        int main_loop(void);
        virtual int read_parameters(void);
        int read_iteration_list(
                const std::string fname,
                const std::string group_name);

        /* Method to copy physical simulation parameters into arbitrary
         * `vorticity_equation` object (defined by children classes).
         * */
        template <typename rnumber,
                  field_backend be>
        int copy_parameters_into(
                vorticity_equation<rnumber, be> *dst);

        /* Method to copy physical simulation parameters into arbitrary
         * `NSE` object (defined by children classes).
         * */
        template <typename rnumber>
        int copy_parameters_into(
                NSE<rnumber> *dst);

        /* Method to copy physical simulation parameters into arbitrary
         * `NSVE` object (defined by children classes).
         * */
        template <typename rnumber>
        int copy_parameters_into(
                NSVE<rnumber> *dst);
};

#endif//POSTPROCESS_HPP

