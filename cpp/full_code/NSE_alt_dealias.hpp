/**********************************************************************
*                                                                     *
*  Copyright 2017 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of bfps.                                         *
*                                                                     *
*  bfps is free software: you can redistribute it and/or modify       *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  bfps is distributed in the hope that it will be useful,            *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with bfps.  If not, see <http://www.gnu.org/licenses/>       *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef NSE_ALT_DEALIAS_HPP
#define NSE_ALT_DEALIAS_HPP



#include <cstdlib>
#include "base.hpp"
#include "vorticity_equation.hpp"
#include "full_code/direct_numerical_simulation.hpp"

/** \brief Navier-Stokes solver.
 *
 * Solves the Navier Stokes equation.
 *
 * Allocates fields and a kspace object.
 */


template <typename rnumber>
class NSE_alt_dealias: public direct_numerical_simulation
{
    public:
        std::string name;

        /* parameters that are read in read_parameters */
        double dt;
        double famplitude;
        double friction_coefficient;
        double fk0;
        double fk1;
        double energy;
        double injection_rate;
        int fmode;
        std::string forcing_type;
        int histogram_bins;
        double max_velocity_estimate;
        double max_vorticity_estimate;
        double nu;
        std::string fftw_plan_rigor;

        /* other stuff */
        kspace<FFTW, SMOOTH> *kk;
        field<rnumber, FFTW, THREE> *velocity;
        field<rnumber, FFTW, THREE> *tmp_velocity1; // for timestep
        field<rnumber, FFTW, THREE> *tmp_velocity2; // for timestep
        field<rnumber, FFTW, THREE> *tmp_vec_field0; // can be changed during methods
        field<rnumber, FFTW, THREE> *tmp_vec_field1; // can be changed during methods

        NSE_alt_dealias(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            direct_numerical_simulation(
                    COMMUNICATOR,
                    simulation_name){}
        ~NSE_alt_dealias(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int Euler_step(void);
        int ShuOsher(void);

        int add_field_band(
                field<rnumber, FFTW, THREE> *dst,
                field<rnumber, FFTW, THREE> *src,
                const double k0,
                const double k1,
                const double prefactor);
        virtual int add_nonlinear_term(
                const field<rnumber, FFTW, THREE> *vel,
                field<rnumber, FFTW, THREE> *dst);
        int add_forcing_term(
                field<rnumber, FFTW, THREE> *vel,
                field<rnumber, FFTW, THREE> *dst);
        int impose_forcing(
                field<rnumber, FFTW, THREE> *vel);

        virtual int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//NSE_ALT_DEALIAS_HPP

