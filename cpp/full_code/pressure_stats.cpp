/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "pressure_stats.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int pressure_stats<rnumber>::initialize(void)
{
    TIMEZONE("pressure_stats::initialize");
    this->NSVE_field_stats<rnumber>::initialize();

    /// allocate kspace objects
    this->kk4 = new kspace<FFTW, ONE_HALF>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);
    this->kk3 = new kspace<FFTW, TWO_THIRDS>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);

    this->ve = new vorticity_equation<rnumber, FFTW>(
            this->simname.c_str(),
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            this->dkx,
            this->dky,
            this->dkz,
            this->vorticity->fftw_plan_rigor);
    this->template copy_parameters_into<rnumber, FFTW>(this->ve);

    /// allocate extra fields
    this->nabla_u = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->ve->cvorticity->fftw_plan_rigor);
    this->pressure = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->ve->cvorticity->fftw_plan_rigor);
    this->nabla_p = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->ve->cvorticity->fftw_plan_rigor);
    this->Hessian_p = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->ve->cvorticity->fftw_plan_rigor);
    this->scalar = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->ve->cvorticity->fftw_plan_rigor);

    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->checkpoints_per_file = hdf5_tools::read_value<int>(parameter_file, "/parameters/checkpoints_per_file");
    if (this->checkpoints_per_file == INT_MAX) // value returned if dataset does not exist
        this->checkpoints_per_file = 1;
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/pressure_stats/parameters/iteration_list");
    this->max_pressure_estimate = hdf5_tools::read_value<double>(
            parameter_file,
            "/pressure_stats/parameters/max_pressure_estimate");
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + "_post.h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    else
    {
        this->stat_file = 0;
    }
    int data_file_problem;
    if (this->myrank == 0)
        data_file_problem = hdf5_tools::require_size_file_datasets(
                this->stat_file,
                "pressure_stats",
                (this->iteration_list.back() / this->niter_out) + 1);
    MPI_Bcast(&data_file_problem, 1, MPI_INT, 0, this->comm);
    if (data_file_problem > 0)
    {
        std::cerr <<
            data_file_problem <<
            " problems setting sizes of file datasets.\ntrying to exit now." <<
            std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int pressure_stats<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("pressure_stats::work_on_current_iteration");
    /// read current vorticity, place it in this->ve->cvorticity
    this->read_current_cvorticity();
    *this->ve->cvorticity = this->vorticity->get_cdata();
    /// after the previous instruction, we are free to use this->vorticity
    /// for any other purpose

    /// initialize `stat_group`.
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "pressure_stats",
                H5P_DEFAULT);
    else
        stat_group = 0;

    /// define local "compute scalar stats" lambda function
    /// it can't be a simple function, C++ doesn't allow that.
    /// however, we want a simple function here because this whole block
    /// consists of code that must be identical when applied to different
    /// versions of the velocity field (i.e. vel field differently filtered).
    /// luckily, C++11 allows lambda functions.
    /// we must explicitly pass the "this" and "stat_group" names to the lambda function.
    auto compute_stats_for_cvelocity = [this, stat_group](std::string stat_prefix)
    {
        /// set scalar to 0
        this->scalar->real_space_representation = true;
        *this->scalar = 0.0;

        /// compute velocity
        this->ve->compute_velocity(this->ve->cvorticity);
        /// compute velocity gradient
        compute_gradient(
                this->ve->kk,
                this->ve->cvelocity,
                this->nabla_u);
        /// put velocity gradient in real space
        this->nabla_u->ift();
        /// add Aij Aji to scalar
        this->scalar->RLOOP(
                [&](ptrdiff_t rindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex){
                for (int c0 = 0; c0 < 3; c0++)
                    for (int c1 = 0; c1 < 3; c1++)
                    {
                        scalar->rval(rindex) += this->nabla_u->rval(rindex, c0, c1)*this->nabla_u->rval(rindex, c1, c0);
                    }
                });

        /// compute pressure. velocity should already be in Fourier space.
        this->ve->compute_pressure(this->pressure);
        /// compute pressure gradient
        compute_gradient(
                this->ve->kk,
                this->pressure,
                this->nabla_p);
        /// compute pressure Hessian
        compute_gradient(
                this->ve->kk,
                this->nabla_p,
                this->Hessian_p);
        this->Hessian_p->ift();

        /// add Hii to scalar
        this->scalar->RLOOP(
                [&](ptrdiff_t rindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex){
                for (int c0 = 0; c0 < 3; c0++)
                {
                    scalar->rval(rindex) += this->Hessian_p->rval(rindex, c0, c0);
                }
                });

        /// compute statistics of scalar
        std::vector<double> max_scal_estimate;
        max_scal_estimate.resize(1, max_pressure_estimate);
        this->scalar->compute_rspace_stats(
                stat_group,
                stat_prefix + std::string("_scalar"),
                this->iteration / this->niter_out,
                max_scal_estimate);
    };

    /// 1. "unfiltered" velocity
    compute_stats_for_cvelocity("kk0");

    /// 2. "2/3" velocity
    this->kk3->template dealias<rnumber, THREE>(this->ve->cvorticity->get_cdata());
    compute_stats_for_cvelocity("kk3");

    /// 3. "1/2" velocity. the filtering is a simple band pass,
    /// so applying it to the 2/3 filtered data is not a problem.
    this->kk4->template dealias<rnumber, THREE>(this->ve->cvorticity->get_cdata());
    compute_stats_for_cvelocity("kk4");

    /// close stat group
    if (this->myrank == 0)
        H5Gclose(stat_group);

    return EXIT_SUCCESS;
}

template <typename rnumber>
int pressure_stats<rnumber>::finalize(void)
{
    DEBUG_MSG("entered pressure_stats::finalize\n");
    delete this->scalar;
    delete this->nabla_u;
    delete this->pressure;
    delete this->nabla_p;
    delete this->Hessian_p;
    delete this->ve;
    delete this->kk3;
    delete this->kk4;
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    this->NSVE_field_stats<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template class pressure_stats<float>;
template class pressure_stats<double>;

