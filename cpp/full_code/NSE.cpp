/******************************************************************************
*                                                                             *
*  Copyright 2021 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "NSE.hpp"
#include "scope_timer.hpp"
#include "fftw_tools.hpp"
#include "shared_array.hpp"

template <typename rnumber>
int NSE<rnumber>::initialize(void)
{
    TIMEZONE("NSE::initialize");
    this->read_iteration();
    this->read_parameters();
    if (this->myrank == 0) {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    this->grow_file_datasets();

    // allocate fields and kspace
    this->allocate();

    if (this->myrank == 0 && this->iteration == 0) {
        this->kk->store(stat_file);
    }

    /* initialize field */
    this->velocity->real_space_representation = false;
    if ((this->iteration == 0)
     && (this->field_random_seed != 0)) {
        TIMEZONE("NSE::initialize::generate_initial_condition");
        // generate initial condition
        make_gaussian_random_field(
            this->kk,
            this->velocity,
            this->field_random_seed,
            this->injection_rate,
            1.0,                    // Lint
            1.5 / this->kk->kM,     // etaK
            6.78,
            0.40,
            3./2.);
        this->kk->template low_pass<rnumber, THREE>(
                this->velocity->get_cdata(),
                this->kk->kM);
        this->kk->template project_divfree<rnumber>(
                this->velocity->get_cdata());
        this->velocity->symmetrize();
        this->velocity->io(
            this->get_current_fname(),
            "velocity",
            this->iteration,
            false);
    } else {
        TIMEZONE("NSE::initialize::read_initial_condition");
        this->velocity->io(
            this->get_current_fname(),
            "velocity",
            this->iteration,
            true);
    }
    return EXIT_SUCCESS;
}

/** \brief Allocates fields and kspace.
 */
template <typename rnumber>
int NSE<rnumber>::allocate(void)
{
    TIMEZONE("NSE::allocate");
    this->velocity = new field<rnumber, FFTW, THREE>(
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_velocity1 = new field<rnumber, FFTW, THREE>(
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_velocity2 = new field<rnumber, FFTW, THREE>(
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_vec_field0 = new field<rnumber, FFTW, THREE>(
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_vec_field1 = new field<rnumber, FFTW, THREE>(
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->kk = new kspace<FFTW, SMOOTH>(
            this->velocity->clayout, this->dkx, this->dky, this->dkz);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::step(void)
{
    TIMEZONE("NSE::step");
    this->iteration++;
    //return this->Euler_step();
    //return this->Heun_step();
    return this->ShuOsher();
}

template <typename rnumber>
int NSE<rnumber>::add_field_band(
        field<rnumber, FFTW, THREE> *dst,
        field<rnumber, FFTW, THREE> *src,
        const double k0,
        const double k1,
        const double prefactor)
{
    TIMEZONE("NSE::add_field_band");
    this->kk->CLOOP(
                [&](const ptrdiff_t cindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
        const double knorm = std::sqrt(
                this->kk->kx[xindex]*this->kk->kx[xindex] +
                this->kk->ky[yindex]*this->kk->ky[yindex] +
                this->kk->kz[zindex]*this->kk->kz[zindex]);
        if ((k0 <= knorm) &&
            (k1 >= knorm))
            for (int c=0; c<3; c++)
                for (int i=0; i<2; i++)
                    dst->cval(cindex,c,i) += prefactor*src->cval(cindex,c,i);
    }
    );
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::add_forcing_term(
        field<rnumber, FFTW, THREE> *vel,
        field<rnumber, FFTW, THREE> *dst)
{
    TIMEZONE("NSE::add_forcing_term");
    if (this->forcing_type == std::string("linear"))
    {
        return this->add_field_band(
                dst, vel,
                this->fk0, this->fk1,
                this->famplitude);
    }
    if ((this->forcing_type == std::string("fixed_energy_injection_rate")) ||
        (this->forcing_type == std::string("fixed_energy_injection_rate_and_drag")))
    {
        // first, compute energy in shell
        shared_array<double> local_energy_in_shell(
                1,
                shared_array_zero_initializer<double, 1>);
        double energy_in_shell = 0;
        this->kk->CLOOP(
                    [&](const ptrdiff_t cindex,
                        const ptrdiff_t xindex,
                        const ptrdiff_t yindex,
                        const ptrdiff_t zindex,
                        const double k2,
                        const int nxmodes){
            const double knorm = std::sqrt(k2);
            if ((k2 > 0) &&
                (this->fk0 <= knorm) &&
                (this->fk1 >= knorm))
                    *local_energy_in_shell.getMine() += nxmodes*(
                            vel->cval(cindex, 0, 0)*vel->cval(cindex, 0, 0) + vel->cval(cindex, 0, 1)*vel->cval(cindex, 0, 1) +
                            vel->cval(cindex, 1, 0)*vel->cval(cindex, 1, 0) + vel->cval(cindex, 1, 1)*vel->cval(cindex, 1, 1) +
                            vel->cval(cindex, 2, 0)*vel->cval(cindex, 2, 0) + vel->cval(cindex, 2, 1)*vel->cval(cindex, 2, 1)
                            );
        }
        );
        local_energy_in_shell.mergeParallel();
        MPI_Allreduce(
                local_energy_in_shell.getMasterData(),
                &energy_in_shell,
                1,
                MPI_DOUBLE,
                MPI_SUM,
                vel->comm);
        // we should divide by 2, if we wanted energy;
        // but then we would need to multiply the amplitude by 2 anyway,
        // because what we really care about is force dotted into velocity,
        // without the division by 2.

        // now, modify amplitudes
        if (energy_in_shell < 10*std::numeric_limits<rnumber>::epsilon())
            energy_in_shell = 1;
        double temp_famplitude = this->injection_rate / energy_in_shell;
        this->add_field_band(
                dst, vel,
                this->fk0, this->fk1,
                temp_famplitude);
        // and add drag if desired
        if (this->forcing_type == std::string("fixed_energy_injection_rate_and_drag"))
            this->add_field_band(
                    dst, vel,
                    this->fmode, this->fmode + (this->fk1 - this->fk0),
                    -this->friction_coefficient);
        return EXIT_SUCCESS;
    }
    if (this->forcing_type == std::string("fixed_energy"))
        return EXIT_SUCCESS;
    else
    {
        DEBUG_MSG("unknown forcing type printed on next line\n%s", this->forcing_type.c_str());
        throw std::invalid_argument("unknown forcing type");
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::impose_forcing(
        field<rnumber, FFTW, THREE> *dst)
{
    TIMEZONE("NSE::impose_forcing");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::add_nonlinear_term(
        const field<rnumber, FFTW, THREE> *vel,
        field<rnumber, FFTW, THREE> *dst)
{
    TIMEZONE("NSE::add_nonlinear_term");
    dst->real_space_representation = false;

    /* This computes the nonlinear term coming from vel, for the Navier Stokes
     * equations. Using vel=\f$u\f$ and dst=\f$a^{nl}\f$, we compute
     * \f[
     *  a^{nl}_i = - u_j \partial_j u_i
     * \f]
     * The result is given in Fourier representation.
     * */
    const std::array<int, 2> sign_array = {1, -1};

    /* copy velocity */
    this->tmp_vec_field0->real_space_representation = false;
    *this->tmp_vec_field0 = *vel;

    // put velocity in real space
    this->tmp_vec_field0->ift();

    /* compute uu */
    /* store 00 11 22 components */
    this->tmp_vec_field1->real_space_representation = true;
    this->tmp_vec_field1->RLOOP(
            [&](const ptrdiff_t rindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
        for (int cc=0; cc<3; cc++)
            this->tmp_vec_field1->rval(rindex,cc) = (
                this->tmp_vec_field0->rval(rindex,cc) *
                this->tmp_vec_field0->rval(rindex,cc) ) / this->tmp_vec_field1->npoints;
        });
    /* take 00 11 22 components to Fourier representation */
    this->tmp_vec_field1->dft();
    this->kk->template dealias<rnumber, THREE>(this->tmp_vec_field1->get_cdata());

    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int ccc = 0; ccc < 2; ccc++)
            {
                dst->cval(cindex, 0, ccc) += (
                        sign_array[ccc]*this->kk->kx[xindex] * this->tmp_vec_field1->cval(cindex, 0, (ccc+1)%2));
                dst->cval(cindex, 1, ccc) += (
                        sign_array[ccc]*this->kk->ky[yindex] * this->tmp_vec_field1->cval(cindex, 1, (ccc+1)%2));
                dst->cval(cindex, 2, ccc) += (
                        sign_array[ccc]*this->kk->kz[zindex] * this->tmp_vec_field1->cval(cindex, 2, (ccc+1)%2));
            }
            });

    /* store 01 12 20 components */
    this->tmp_vec_field1->real_space_representation = true;
    this->tmp_vec_field1->RLOOP(
            [&](const ptrdiff_t rindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
        for (int cc=0; cc<3; cc++)
            this->tmp_vec_field1->rval(rindex,cc) = (
                this->tmp_vec_field0->rval(rindex,cc) *
                this->tmp_vec_field0->rval(rindex,(cc+1)%3) ) / this->tmp_vec_field1->npoints;
        });
    /* take 01 12 20 components to Fourier representation */
    this->tmp_vec_field1->dft();
    this->kk->template dealias<rnumber, THREE>(this->tmp_vec_field1->get_cdata());

    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int ccc = 0; ccc < 2; ccc++)
            {
                dst->cval(cindex, 0, ccc) += (
                        sign_array[ccc]*this->kk->ky[yindex] * this->tmp_vec_field1->cval(cindex, 0, (ccc+1)%2) +
                        sign_array[ccc]*this->kk->kz[zindex] * this->tmp_vec_field1->cval(cindex, 2, (ccc+1)%2));
                dst->cval(cindex, 1, ccc) += (
                        sign_array[ccc]*this->kk->kz[zindex] * this->tmp_vec_field1->cval(cindex, 1, (ccc+1)%2) +
                        sign_array[ccc]*this->kk->kx[xindex] * this->tmp_vec_field1->cval(cindex, 0, (ccc+1)%2));
                dst->cval(cindex, 2, ccc) += (
                        sign_array[ccc]*this->kk->kx[xindex] * this->tmp_vec_field1->cval(cindex, 2, (ccc+1)%2) +
                        sign_array[ccc]*this->kk->ky[yindex] * this->tmp_vec_field1->cval(cindex, 1, (ccc+1)%2));
            }
            });

    this->kk->template force_divfree<rnumber>(dst->get_cdata());
    dst->symmetrize();
    /* done */

    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::Euler_step(void)
{
    // technically this is not a pure Euler method,
    // because I use an exponential factor for the diffusion term
    TIMEZONE("NSE::Euler_step");

    // temporary field
    field<rnumber, FFTW, THREE> *acc = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    *acc = rnumber(0);

    // add nonlinear term
    this->add_nonlinear_term(
            this->velocity,
            acc);

    // add forcing
    this->add_forcing_term(this->velocity, acc);

    // perform Euler step
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) += this->dt*acc->cval(cindex, cc, ccc);
            }
            });

    // enforce incompressibility
    this->kk->template force_divfree<rnumber>(this->velocity->get_cdata());

    // apply diffusion exponential
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor = std::exp(-this->nu*k2 * this->dt);
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) *= factor;
            }
            });

    // enforce Hermitian symmetry
    this->velocity->symmetrize();

    // delete temporary field
    delete acc;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::Heun_step(void)
{
    // technically this is not the Heun method, but rather a
    // composition between the exact diffusion solution and the Heun method
    // for the nonlinear term
    TIMEZONE("NSE::Heun_step");

    // temporary field
    field<rnumber, FFTW, THREE> *acc1 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    field<rnumber, FFTW, THREE> *acc2 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor = std::exp(-0.5*this->nu*k2 * this->dt);
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) *= factor;
            }
            });
    *acc1 = rnumber(0);
    this->add_nonlinear_term(
            this->velocity,
            acc1);
    this->add_forcing_term(this->velocity, acc1);
    *(this->tmp_velocity1) = *(this->velocity);
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->tmp_velocity1->cval(cindex, cc, ccc) +=
                    this->dt*acc1->cval(cindex, cc, ccc);
            }
            });
    this->kk->template force_divfree<rnumber>(this->tmp_velocity1->get_cdata());
    this->velocity->symmetrize();
    this->tmp_velocity1->symmetrize();

    *acc2 = rnumber(0);
    this->add_nonlinear_term(
            this->tmp_velocity1,
            acc2);
    this->add_forcing_term(this->tmp_velocity1, acc2);
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) +=
                    0.5*this->dt*(acc1->cval(cindex, cc, ccc)
                                + acc2->cval(cindex, cc, ccc));
            }
            });
    this->kk->template force_divfree<rnumber>(this->velocity->get_cdata());
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor = std::exp(-0.5*this->nu*k2 * this->dt);
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) *= factor;
            }
            });
    this->velocity->symmetrize();

    // delete temporary field
    delete acc1;
    delete acc2;
    return EXIT_SUCCESS;
}


/** \brief Time step with Shu Osher method
 *
 * The Navier Stokes equations are integrated with a
 * third-order Runge-Kutta method [shu1988jcp]_, which  is an explicit
 * Runge-Kutta method with the Butcher tableau
 *
 *  |   |   |   |   |
 *  |:--|:--|:--|:--|
 *  | 0 | | | |
 *  | 1 | 1 | | |
 *  | 1/2 | 1/4 | 1/4 | |
 *  | | 1/6 | 1/6 | 2/3 |
 *
 * In addition to the stability properties described in [shu1988jcp]_, this method has the advantage that it is memory-efficient, requiring only two additional field allocations, as can be seen from
 *     \f{eqnarray*}{
 *         \hat{\boldsymbol w}_1(\boldsymbol k) &= {\hat{\boldsymbol u}}(\boldsymbol k, t)e^{-\nu k^2 h} + h \textrm{NL}[\hat{\boldsymbol u}(\boldsymbol k, t)]e^{-\nu k^2 h}, \\
 *         \hat{\boldsymbol w}_2(\boldsymbol k) &= \frac{3}{4}\hat{\boldsymbol u}( \boldsymbol k, t)e^{-\nu k^2 h/2}
 *         +
 *         \frac{1}{4}(\hat{\boldsymbol w}_1(\boldsymbol k) + h \textrm{NL}[\hat{\boldsymbol w}_1(\boldsymbol k)])e^{\nu k^2 h/2}, \\
 *         \hat{\boldsymbol u}(\boldsymbol k, t+h) &= \frac{1}{3}\hat{\boldsymbol u}(\boldsymbol k, t)e^{-\nu k^2 h} +
 *         \frac{2}{3}(\hat{\boldsymbol w}_2(\boldsymbol k) + h \textrm{NL}[\hat{\boldsymbol w}_2(\boldsymbol k)])e^{-\nu k^2 h/2},
 *     \f}
 */
template <typename rnumber>
int NSE<rnumber>::ShuOsher(void)
{
    TIMEZONE("NSE::ShuOsher");

    // temporary field
    field<rnumber, FFTW, THREE> *acc = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    *acc = rnumber(0);
    this->add_nonlinear_term(
            this->velocity,
            acc);
    this->add_forcing_term(this->velocity, acc);
    *(this->tmp_velocity1) = rnumber(0.0);
    this->tmp_velocity1->real_space_representation = false;
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor1 = std::exp(-this->nu * k2 * this->dt);
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->tmp_velocity1->cval(cindex, cc, ccc) =
                    (this->velocity->cval(cindex, cc, ccc)
                     + this->dt*acc->cval(cindex, cc, ccc))*factor1;
            }
            });
    this->tmp_velocity1->symmetrize();

    *acc = rnumber(0);
    this->add_nonlinear_term(
            this->tmp_velocity1,
            acc);
    this->add_forcing_term(this->tmp_velocity1, acc);
    *(this->tmp_velocity2) = rnumber(0.0);
    this->tmp_velocity2->real_space_representation = false;
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor0 = double(3)*std::exp(-0.5*this->nu * k2 * this->dt)/4;
            const double factor1 = std::exp(0.5*this->nu * k2 * this->dt)/4;
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->tmp_velocity2->cval(cindex, cc, ccc) = (
                    this->velocity->cval(cindex, cc, ccc)*factor0
                     + (this->tmp_velocity1->cval(cindex, cc, ccc)
                         + this->dt*acc->cval(cindex, cc, ccc))*factor1);
            }
            });
    this->tmp_velocity2->symmetrize();

    *acc = rnumber(0);
    this->add_nonlinear_term(
            this->tmp_velocity2,
            acc);
    this->add_forcing_term(this->tmp_velocity2, acc);
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor0 = std::exp(-this->nu * k2 * this->dt)/3;
            const double factor1 = double(2)*std::exp(-0.5*this->nu * k2 * this->dt)/3;
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) = (
                    this->velocity->cval(cindex, cc, ccc)*factor0
                     + (this->tmp_velocity2->cval(cindex, cc, ccc)
                         + this->dt*acc->cval(cindex, cc, ccc))*factor1);
            }
            });
    this->velocity->symmetrize();

    // delete temporary field
    delete acc;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::write_checkpoint(void)
{
    TIMEZONE("NSE::write_checkpoint");
    this->update_checkpoint();

    assert(!this->velocity->real_space_representation);
    std::string fname = this->get_current_fname();
    this->velocity->io(
            fname,
            "velocity",
            this->iteration,
            false);

    this->write_iteration();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::deallocate(void)
{
    TIMEZONE("NSE::deallocate");
    delete this->kk;
    delete this->tmp_velocity1;
    delete this->tmp_velocity2;
    delete this->tmp_vec_field1;
    delete this->tmp_vec_field0;
    delete this->velocity;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::finalize(void)
{
    TIMEZONE("NSE::finalize");
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    const int deallocate_result = this->deallocate();
    variable_used_only_in_assert(deallocate_result);
    assert(deallocate_result == EXIT_SUCCESS);
    return EXIT_SUCCESS;
}

/** \brief Compute standard statistics for velocity and vorticity fields.
 *
 *  IMPORTANT: at the end of this subroutine, `this->fs->cvelocity` contains
 *  the Fourier space representation of the velocity field, and
 *  `this->tmp_vec_field` contains the real space representation of the
 *  velocity field.
 *  This behavior is relied upon in the `NSEparticles` class, so please
 *  don't break it.
 */

template <typename rnumber>
int NSE<rnumber>::do_stats()
{
    TIMEZONE("NSE::do_stats");
    if (!(this->iteration % this->niter_stat == 0))
        return EXIT_SUCCESS;
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "statistics",
                H5P_DEFAULT);
    else
        stat_group = 0;

    this->tmp_vec_field0->real_space_representation = false;
    *this->tmp_vec_field0 = *this->velocity;

    this->tmp_vec_field0->compute_stats(
            this->kk,
            stat_group,
            "velocity",
            this->iteration / niter_stat,
            this->max_velocity_estimate/std::sqrt(3));

    double CFL_velocity = tmp_vec_field0->compute_CFL_velocity();
    if (this->myrank == 0)
        hdf5_tools::update_time_series_with_single_rank<double>(
                stat_group,
                "CFL_velocity",
                this->iteration / niter_stat,
                1,
                &CFL_velocity);

    compute_curl(this->kk,
                 this->velocity,
                 this->tmp_vec_field0);

    this->tmp_vec_field0->compute_stats(
            this->kk,
            stat_group,
            "vorticity",
            this->iteration / niter_stat,
            this->max_vorticity_estimate/std::sqrt(3));

    if (this->myrank == 0)
        H5Gclose(stat_group);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::read_parameters(void)
{
    TIMEZONE("NSE::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->nu = hdf5_tools::read_value<double>(parameter_file, "parameters/nu");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "parameters/dt");
    this->fmode = hdf5_tools::read_value<int>(parameter_file, "parameters/fmode");
    this->field_random_seed = hdf5_tools::read_value<int>(parameter_file, "parameters/field_random_seed");
    this->famplitude = hdf5_tools::read_value<double>(parameter_file, "parameters/famplitude");
    this->friction_coefficient = hdf5_tools::read_value<double>(parameter_file, "parameters/friction_coefficient");
    this->injection_rate = hdf5_tools::read_value<double>(parameter_file, "parameters/injection_rate");
    this->fk0 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk0");
    this->fk1 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk1");
    this->energy = hdf5_tools::read_value<double>(parameter_file, "parameters/energy");
    this->histogram_bins = hdf5_tools::read_value<int>(parameter_file, "parameters/histogram_bins");
    this->max_velocity_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_velocity_estimate");
    this->max_vorticity_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_vorticity_estimate");
    this->forcing_type = hdf5_tools::read_string(parameter_file, "parameters/forcing_type");
    this->fftw_plan_rigor = hdf5_tools::read_string(parameter_file, "parameters/fftw_plan_rigor");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::print_debug_info(void)
{
    DEBUG_MSG("start NSE::print_debug_info\n");
    DEBUG_MSG("nu = %g\n", this->nu);
    DEBUG_MSG("dt = %g\n", this->dt);
    DEBUG_MSG("fmode = %d\n", this->fmode);
    DEBUG_MSG("famplitude = %g\n", this->famplitude);
    DEBUG_MSG("friction_coefficient = %g\n", this->friction_coefficient);
    DEBUG_MSG("injection_rate = %g\n", this->injection_rate);
    DEBUG_MSG("fk0 = %g\n", this->fk0);
    DEBUG_MSG("fk1 = %g\n", this->fk1);
    DEBUG_MSG("energy = %g\n", this->energy);
    DEBUG_MSG("forcing_type = \"%s\"\n", this->forcing_type.c_str());
    DEBUG_MSG("end NSE::print_debug_info\n");
    return EXIT_SUCCESS;
}


template <typename rnumber>
int NSE<rnumber>::get_cpressure(field<rnumber, FFTW, ONE> *destination)
{
    TIMEZONE("NSE::get_cpressure");
    *destination = 0.0;
    destination->real_space_representation = false;
    // get velocity to real space
    field<rnumber, FFTW, THREE>* rvelocity = new field<rnumber, FFTW, THREE>(
        this->nx, this->ny, this->nz,
        this->comm,
        this->get_fftw_plan_rigor());
    *rvelocity = *(this->velocity);
    rvelocity->ift();
    // allocate temporary field
    field<rnumber, FFTW, ONE>* temp_scalar = new field<rnumber, FFTW, ONE>(
        this->nx, this->ny, this->nz,
        this->comm,
        this->get_fftw_plan_rigor());
    // 00
    this->store_nonlinear_product(temp_scalar, rvelocity, 0, 0);
    temp_scalar->dft();
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if ((k2 < this->kk->kM2) && (k2 > 0))
            for (int ccc = 0; ccc < 2; ccc++)
                destination->cval(cindex, ccc) -= (
                    this->kk->kx[xindex]*this->kk->kx[xindex]
                  * temp_scalar->cval(cindex, ccc)
                  / k2);
            });
    // 01
    this->store_nonlinear_product(temp_scalar, rvelocity, 0, 1);
    temp_scalar->dft();
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if ((k2 < this->kk->kM2) && (k2 > 0))
            for (int ccc = 0; ccc < 2; ccc++)
                destination->cval(cindex, ccc) -= 2*(
                    this->kk->kx[xindex]*this->kk->ky[yindex]
                  * temp_scalar->cval(cindex, ccc)
                  / k2);
            });
    // 02
    this->store_nonlinear_product(temp_scalar, rvelocity, 0, 2);
    temp_scalar->dft();
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if ((k2 < this->kk->kM2) && (k2 > 0))
            for (int ccc = 0; ccc < 2; ccc++)
                destination->cval(cindex, ccc) -= 2*(
                    this->kk->kx[xindex]*this->kk->kz[zindex]
                  * temp_scalar->cval(cindex, ccc)
                  / k2);
            });
    // 11
    this->store_nonlinear_product(temp_scalar, rvelocity, 1, 1);
    temp_scalar->dft();
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if ((k2 < this->kk->kM2) && (k2 > 0))
            for (int ccc = 0; ccc < 2; ccc++)
                destination->cval(cindex, ccc) -= (
                    this->kk->ky[yindex]*this->kk->ky[yindex]
                  * temp_scalar->cval(cindex, ccc)
                  / k2);
            });
    // 12
    this->store_nonlinear_product(temp_scalar, rvelocity, 1, 2);
    temp_scalar->dft();
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if ((k2 < this->kk->kM2) && (k2 > 0))
            for (int ccc = 0; ccc < 2; ccc++)
                destination->cval(cindex, ccc) -= 2*(
                    this->kk->ky[yindex]*this->kk->kz[zindex]
                  * temp_scalar->cval(cindex, ccc)
                  / k2);
            });
    // 22
    this->store_nonlinear_product(temp_scalar, rvelocity, 2, 2);
    temp_scalar->dft();
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if ((k2 < this->kk->kM2) && (k2 > 0))
            for (int ccc = 0; ccc < 2; ccc++)
                destination->cval(cindex, ccc) -= (
                    this->kk->kz[zindex]*this->kk->kz[zindex]
                  * temp_scalar->cval(cindex, ccc)
                  / k2);
            });
    destination->symmetrize();
    this->kk->template dealias<rnumber, ONE>(destination->get_cdata());
    delete temp_scalar;
    delete rvelocity;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE<rnumber>::get_cacceleration(field<rnumber, FFTW, THREE> *destination)
{
    TIMEZONE("NSE::get_cacceleration");
    DEBUG_MSG("NSE::get_cacceleration\n");
    *destination = 0.0;
    destination->real_space_representation = false;
    // put forcing in
    DEBUG_MSG("NSE::get_cacceleration before add_forcing_term\n");
    this->add_forcing_term(
            this->velocity,
            destination);
    field<rnumber, FFTW, ONE>* pressure = new field<rnumber, FFTW, ONE>(
        this->nx, this->ny, this->nz,
        this->comm,
        this->velocity->fftw_plan_rigor);
    this->get_cpressure(pressure);
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            // pressure gradient
            destination->cval(cindex, 0, 0) += (
                this->kk->kx[xindex]*pressure->cval(cindex, 1));
            destination->cval(cindex, 0, 1) -= (
                this->kk->kx[xindex]*pressure->cval(cindex, 0));
            destination->cval(cindex, 1, 0) += (
                this->kk->ky[yindex]*pressure->cval(cindex, 1));
            destination->cval(cindex, 1, 1) -= (
                this->kk->ky[yindex]*pressure->cval(cindex, 0));
            destination->cval(cindex, 2, 0) += (
                this->kk->kz[zindex]*pressure->cval(cindex, 1));
            destination->cval(cindex, 2, 1) -= (
                this->kk->kz[zindex]*pressure->cval(cindex, 0));
            // dissipation
            const double factor = this->nu*k2;
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
                destination->cval(cindex, cc, ccc) -= (
                    factor*this->velocity->cval(cindex, cc, ccc));
            });
    destination->symmetrize();
    return EXIT_SUCCESS;
}

template class NSE<float>;
template class NSE<double>;

