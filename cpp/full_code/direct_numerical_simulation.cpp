/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "direct_numerical_simulation.hpp"
#include "scope_timer.hpp"

int direct_numerical_simulation::grow_file_datasets()
{
    TIMEZONE("direct_numerical_simulation::grow_file_datasets");
    int data_file_problem = EXIT_FAILURE;
    if (this->myrank == 0) {
        assert(this->stat_file != H5E_BADFILE);
        data_file_problem = hdf5_tools::grow_file_datasets(
            this->stat_file,
            "statistics",
            this->niter_todo / this->niter_stat);
    }
    MPI_Bcast(&data_file_problem, 1, MPI_INT, 0, this->comm);
    if (data_file_problem != EXIT_SUCCESS) {
        std::cerr
            << " datafile_problem = "
            << data_file_problem
            << ". Something went wrong growing file datasets.\ntrying to exit now."
            << std::endl;
        throw std::runtime_error("Computed nshells not equal to data file nshells. Something is probably wrong with the dealiasing option.\n");
        return EXIT_FAILURE;
    }
    return data_file_problem;
}

int direct_numerical_simulation::read_iteration(void)
{
    TIMEZONE("direct_numerical_simulation::read_iteration");
    /* read iteration */
    hid_t dset;
    hid_t iteration_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    dset = H5Dopen(
            iteration_file,
            "iteration",
            H5P_DEFAULT);
    H5Dread(
            dset,
            H5T_NATIVE_INT,
            H5S_ALL,
            H5S_ALL,
            H5P_DEFAULT,
            &this->iteration);
    H5Dclose(dset);
    dset = H5Dopen(
            iteration_file,
            "checkpoint",
            H5P_DEFAULT);
    H5Dread(
            dset,
            H5T_NATIVE_INT,
            H5S_ALL,
            H5S_ALL,
            H5P_DEFAULT,
            &this->checkpoint);
    H5Dclose(dset);
    H5Fclose(iteration_file);
    MPI_Barrier(this->comm);
    DEBUG_MSG("simname is %s, iteration is %d and checkpoint is %d\n",
            this->simname.c_str(),
            this->iteration,
            this->checkpoint);
    return EXIT_SUCCESS;
}

int direct_numerical_simulation::write_iteration(void)
{
    TIMEZONE("direct_numerical_simulation::write_iteration");
    if (this->myrank == 0)
    {
        assert(this->stat_file != H5E_BADFILE);
        hid_t dset = H5Dopen(
                this->stat_file,
                "iteration",
                H5P_DEFAULT);
        H5Dwrite(
                dset,
                H5T_NATIVE_INT,
                H5S_ALL,
                H5S_ALL,
                H5P_DEFAULT,
                &this->iteration);
        H5Dclose(dset);
        dset = H5Dopen(
                this->stat_file,
                "checkpoint",
                H5P_DEFAULT);
        H5Dwrite(
                dset,
                H5T_NATIVE_INT,
                H5S_ALL,
                H5S_ALL,
                H5P_DEFAULT,
                &this->checkpoint);
        H5Dclose(dset);
    }
    return EXIT_SUCCESS;
}

int direct_numerical_simulation::update_checkpoint(void)
{
    assert(this->checkpoints_per_file > 0);
    this->checkpoint = this->iteration / (this->niter_out*this->checkpoints_per_file);
    return EXIT_SUCCESS;
}

int direct_numerical_simulation::main_loop(void)
{
    TIMEZONE("direct_numerical_simulation::main_loop");
    this->start_simple_timer();
    int max_iter = (this->iteration + this->niter_todo -
                    (this->iteration % this->niter_todo));
    for (; this->iteration < max_iter;)
    {
    #ifdef USE_TIMING_OUTPUT
        const std::string loopLabel = ("code::main_start::loop-" +
                                       std::to_string(this->iteration));
        TIMEZONE(loopLabel.c_str());
    #endif
        this->do_stats();

        this->step();
        if (this->iteration % this->niter_out == 0)
            this->write_checkpoint();
        this->print_simple_timer(
                "iteration " + std::to_string(this->iteration));
        this->check_stopping_condition();
        if (this->stop_code_now)
            break;
    }
    this->do_stats();
    this->print_simple_timer(
            "final call to do_stats ");
    if (this->iteration % this->niter_out != 0)
        this->write_checkpoint();
    return EXIT_SUCCESS;
}

int direct_numerical_simulation::read_parameters(void)
{
    TIMEZONE("direct_numerical_simulation::read_parameters");
    this->code_base::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->checkpoints_per_file = hdf5_tools::read_value<int>(parameter_file, "parameters/checkpoints_per_file");
    this->niter_out = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_out");
    this->niter_stat = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_stat");
    this->niter_todo = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_todo");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}
