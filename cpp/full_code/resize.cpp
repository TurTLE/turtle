/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "resize.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int resize<rnumber>::initialize(void)
{
    TIMEZONE("resize::initialize");
    this->NSVE_field_stats<rnumber>::initialize();
    DEBUG_MSG("after NSVE_field_stats::initialize\n");
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);

    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    DEBUG_MSG("before read_vector\n");
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/resize/parameters/iteration_list");

    this->new_nx = hdf5_tools::read_value<int>(
            parameter_file, "/resize/parameters/new_nx");
    this->new_ny = hdf5_tools::read_value<int>(
            parameter_file, "/resize/parameters/new_ny");
    this->new_nz = hdf5_tools::read_value<int>(
            parameter_file, "/resize/parameters/new_nz");
    this->new_simname = hdf5_tools::read_string(
            parameter_file, "/resize/parameters/new_simname");
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);

    this->new_field = new field<rnumber, FFTW, THREE>(
            this->new_nx, this->new_ny, this->new_nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int resize<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("resize::work_on_current_iteration");
    this->read_current_cvorticity();

    std::string fname = (
            this->new_simname +
            std::string("_fields.h5"));
    *this->new_field = *this->vorticity;
    this->new_field->io(
            fname,
            "vorticity",
            this->iteration,
            false);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int resize<rnumber>::finalize(void)
{
    TIMEZONE("resize::finalize");
    delete this->new_field;
    this->NSVE_field_stats<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template class resize<float>;
template class resize<double>;

