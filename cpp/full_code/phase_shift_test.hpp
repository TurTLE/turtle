/**********************************************************************
*                                                                     *
*  Copyright 2021 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#ifndef PHASE_SHIFT_TEST_HPP
#define PHASE_SHIFT_TEST_HPP



#include "field.hpp"
#include "full_code/test.hpp"

/** \brief A class for testing phase shift functionality.
 */

template <typename rnumber>
class phase_shift_test: public test
{
    private:
        kspace<FFTW, SMOOTH> *kk;
        field<rnumber, FFTW, ONE> *phase_field;
        int random_phase_seed;
    public:
        phase_shift_test(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            test(
                    COMMUNICATOR,
                    simulation_name){}
        ~phase_shift_test() noexcept(false){}

        int initialize(void);
        int do_work(void);
        int finalize(void);
        int read_parameters(void);
};

#endif// PHASE_SHIFT_TEST_HPP

