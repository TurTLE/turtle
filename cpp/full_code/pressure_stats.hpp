/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef PRESSURE_STATS_HPP
#define PRESSURE_STATS_HPP

#include "full_code/NSVE_field_stats.hpp"

template <typename rnumber>
class pressure_stats: public NSVE_field_stats<rnumber>
{
    public:
        hid_t stat_file;
        kspace<FFTW, SMOOTH> *kk;
        vorticity_equation<rnumber, FFTW> *ve;

        kspace<FFTW, ONE_HALF> *kk4;
        kspace<FFTW, TWO_THIRDS> *kk3;

        field<rnumber, FFTW, ONE> *pressure;
        field<rnumber, FFTW, THREE> *nabla_p;
        field<rnumber, FFTW, THREExTHREE> *nabla_u;
        field<rnumber, FFTW, THREExTHREE> *Hessian_p;
        field<rnumber, FFTW, ONE> *scalar;

        int checkpoints_per_file;
        double max_pressure_estimate;

        pressure_stats(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~pressure_stats() noexcept(false){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};

#endif//PRESSURE_STATS_HPP

