/**********************************************************************
*                                                                     *
*  Copyright 2021 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef NSE_HPP
#define NSE_HPP



#include "full_code/direct_numerical_simulation.hpp"
#include "field.hpp"

/** \brief Navier-Stokes solver.
 *
 * Solves the Navier Stokes equation.
 *
 * Allocates fields and a kspace object.
 */


template <typename rnumber>
class NSE: public direct_numerical_simulation
{
    public:
        std::string name;

        /* parameters that are read in read_parameters */
        double dt;
        double famplitude;
        double friction_coefficient;
        double fk0;
        double fk1;
        double energy;
        double injection_rate;
        int fmode;
        int field_random_seed;
        std::string forcing_type;
        int histogram_bins;
        double max_velocity_estimate;
        double max_vorticity_estimate;
        double nu;
        std::string fftw_plan_rigor;

        /* other stuff */
        kspace<FFTW, SMOOTH> *kk;
        field<rnumber, FFTW, THREE> *velocity;
        field<rnumber, FFTW, THREE> *tmp_velocity1; // for timestep
        field<rnumber, FFTW, THREE> *tmp_velocity2; // for timestep
        field<rnumber, FFTW, THREE> *tmp_vec_field0; // can be changed during methods
        field<rnumber, FFTW, THREE> *tmp_vec_field1; // can be changed during methods

        NSE(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            direct_numerical_simulation(
                    COMMUNICATOR,
                    simulation_name){}
        ~NSE(){}

        int initialize(void);
        int allocate(void);
        int deallocate(void);
        int step(void);
        int finalize(void);

        int Euler_step(void);
        int Heun_step(void);
        int ShuOsher(void);

        int add_field_band(
                field<rnumber, FFTW, THREE> *dst,
                field<rnumber, FFTW, THREE> *src,
                const double k0,
                const double k1,
                const double prefactor);
        virtual int add_nonlinear_term(
                const field<rnumber, FFTW, THREE> *vel,
                field<rnumber, FFTW, THREE> *dst);
        int add_forcing_term(
                field<rnumber, FFTW, THREE> *vel,
                field<rnumber, FFTW, THREE> *dst);
        int impose_forcing(
                field<rnumber, FFTW, THREE> *vel);


        inline unsigned get_fftw_plan_rigor() const
        {
            return this->velocity->fftw_plan_rigor;
        }
        inline int store_nonlinear_product(
                field<rnumber, FFTW, ONE>* destination,
                const field<rnumber, FFTW, THREE>* rvelocity,
                const int component1,
                const int component2)
        {
            destination->real_space_representation = true;
            destination->RLOOP(
                    [&](const ptrdiff_t rindex,
                        const ptrdiff_t xindex,
                        const ptrdiff_t yindex,
                        const ptrdiff_t zindex) {
                destination->rval(rindex) = (
                    rvelocity->rval(rindex,component1)
                  * rvelocity->rval(rindex,component2) ) / destination->npoints;
            });
            return EXIT_SUCCESS;
        }
        /** \brief compute pressure
         *
         * real space:
         * \f[
         * \partial_i \partial_i p = - (\partial_i u_j) (\partial_j u_i)
         * \f]
         * Fourier space:
         * \f[
         * \mathcal{F}[p] = - \left[
         * \frac{k_i k_j}{k^2} \mathcal{F}[u_j u_i] \right]
         * \f]
         * The result is given in Fourier representation.
         */
        int get_cpressure(field<rnumber, FFTW, ONE> *destination);
        /** \brief compute (Lagrangian) acceleration
         */
        int get_cacceleration(field<rnumber, FFTW, THREE> *destination);

        virtual int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);

        int print_debug_info();
};

#endif//NSE_HPP

