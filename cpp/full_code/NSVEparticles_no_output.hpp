/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef NSVEPARTICLES_NO_OUTPUT_HPP
#define NSVEPARTICLES_NO_OUTPUT_HPP

#include "full_code/NSVEparticles.hpp"

template <typename rnumber>
class NSVEparticles_no_output: public NSVEparticles<rnumber>
{
    public:
    NSVEparticles_no_output(
            const MPI_Comm COMMUNICATOR,
            const std::string &simulation_name):
        NSVEparticles<rnumber>(
                COMMUNICATOR,
                simulation_name){}
    ~NSVEparticles_no_output(){}
    int write_checkpoint(void)
    {
        TIMEZONE("NSVEparticles_no_output::write_checkpoint");
        return EXIT_SUCCESS;
    }
};

#endif//NSVEPARTICLES_NO_OUTPUT_HPP

