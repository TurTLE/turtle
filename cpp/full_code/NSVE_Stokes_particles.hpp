/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef NSVE_STOKES_PARTICLES_HPP
#define NSVE_STOKES_PARTICLES_HPP



#include "full_code/NSVE.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_sampling.hpp"

/** \brief Navier-Stokes solver that includes simple Lagrangian tracers.
 *
 *  Child of Navier Stokes vorticity equation solver, this class calls all the
 *  methods from `NSVE`, and in addition integrates simple Lagrangian tracers
 *  in the resulting velocity field.
 */

template <typename rnumber>
class NSVE_Stokes_particles: public NSVE<rnumber>
{
    public:

        /* parameters that are read in read_parameters */
        int niter_part;
        int niter_part_fine_period;
        int niter_part_fine_duration;
        long long int nparticles;
        int tracers0_integration_steps;
        int tracers0_neighbours;
        int tracers0_smoothness;
        double tracers0_cutoff;
        double drag_coefficient;

        /* other stuff */
        std::unique_ptr<abstract_particles_system<long long int, double>> ps;
        field<rnumber, FFTW, ONE> *pressure;

        particles_output_hdf5<long long int, double,6> *particles_output_writer_mpi;
        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;


        NSVE_Stokes_particles(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        ~NSVE_Stokes_particles(){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//NSVE_STOKES_PARTICLES_HPP

