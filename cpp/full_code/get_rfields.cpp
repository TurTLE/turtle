/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "get_rfields.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int get_rfields<rnumber>::initialize(void)
{
    TIMEZONE("get_rfields::initialize");
    this->NSVE_field_stats<rnumber>::initialize();

    // allocate kspace
    this->kk = new kspace<FFTW, SMOOTH>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);
    // allocate field for TrS2
    this->traceS2 = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    this->traceS2->real_space_representation = true;
    // allocate field for velocity
    this->vel = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    this->vel->real_space_representation = false;
    // allocate field for velocity gradient
    this->grad_vel = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->checkpoints_per_file = hdf5_tools::read_value<int>(parameter_file, "/parameters/checkpoints_per_file");
    if (this->checkpoints_per_file == INT_MAX) // value returned if dataset does not exist
        this->checkpoints_per_file = 1;
    H5Fclose(parameter_file);
    // ensure the file is free for rank 0 to open in read/write mode:
    MPI_Barrier(this->comm);
    int read_iteration_result = this->read_iteration_list(
            this->simname + std::string("_post.h5"),
            "get_rfields");
    variable_used_only_in_assert(read_iteration_result);
    assert(read_iteration_result == EXIT_SUCCESS);
    parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->TrS2_on = hdf5_tools::read_value<int>(
            parameter_file,
            "/get_rfields/parameters/TrS2_on");
    this->velocity_on = hdf5_tools::read_value<int>(
            parameter_file,
            "/get_rfields/parameters/velocity_on");
    this->vorticity_on = hdf5_tools::read_value<int>(
            parameter_file,
            "/get_rfields/parameters/vorticity_on");
    this->velocity_gradient_on = hdf5_tools::read_value<int>(
            parameter_file,
            "/get_rfields/parameters/velocity_gradient_on");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int get_rfields<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("get_rfields::work_on_current_iteration");
    this->read_current_cvorticity();

    invert_curl(
        this->kk,
        this->vorticity,
        this->vel);

    /// compute velocity gradient
    compute_gradient<rnumber, FFTW, THREE, THREExTHREE>(
            this->kk,
            this->vel,
            this->grad_vel);

    // compute TrS2
    this->grad_vel->ift();
    this->traceS2->RLOOP(
            [&](ptrdiff_t rindex,
                ptrdiff_t xindex,
                ptrdiff_t yindex,
                ptrdiff_t zindex){
            rnumber AxxAxx = this->grad_vel->rval(rindex, 0, 0)*this->grad_vel->rval(rindex, 0, 0);
            rnumber AyyAyy = this->grad_vel->rval(rindex, 1, 1)*this->grad_vel->rval(rindex, 1, 1);
            rnumber AzzAzz = this->grad_vel->rval(rindex, 2, 2)*this->grad_vel->rval(rindex, 2, 2);
            rnumber Sxy = this->grad_vel->rval(rindex, 0, 1) + this->grad_vel->rval(rindex, 1, 0);
            rnumber Syz = this->grad_vel->rval(rindex, 1, 2) + this->grad_vel->rval(rindex, 2, 1);
            rnumber Szx = this->grad_vel->rval(rindex, 2, 0) + this->grad_vel->rval(rindex, 0, 2);
            this->traceS2->rval(rindex) = (
                    AxxAxx + AyyAyy + AzzAzz +
                    (Sxy*Sxy + Syz*Syz + Szx*Szx)/2);
            });

    std::string fname = (
            this->simname +
            std::string("_checkpoint_") +
            std::to_string(this->iteration / (this->niter_out*this->checkpoints_per_file)) +
            std::string(".h5"));

    /// output velocity field
    if (this->velocity_on) {
        this->vel->ift();
        this->vel->io(
                fname,
                "velocity",
                this->iteration,
                false);
    }

    /// output vorticity field
    if (this->vorticity_on) {
        this->vorticity->ift();
        this->vorticity->io(
                fname,
                "vorticity",
                this->iteration,
                false);
    }

    if (this->TrS2_on)
    {
        this->traceS2->io(
            fname,
            "TrS2",
            this->iteration,
            false);
    }

    if (this->velocity_gradient_on)
    {
        this->grad_vel->io(
            fname,
            "velocity_gradient",
            this->iteration,
            false);
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int get_rfields<rnumber>::finalize(void)
{
    TIMEZONE("get_rfields::finalize");
    delete this->kk;
    delete this->traceS2;
    delete this->vel;
    delete this->grad_vel;
    this->NSVE_field_stats<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template class get_rfields<float>;
template class get_rfields<double>;

