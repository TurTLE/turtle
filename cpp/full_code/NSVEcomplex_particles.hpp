/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef NSVECOMPLEX_PARTICLES_HPP
#define NSVECOMPLEX_PARTICLES_HPP



#include "full_code/NSVE.hpp"
#include "particles/particles_system_builder.hpp"
#include "particles/particles_sampling.hpp"

/** \brief Navier-Stokes solver that includes complex particles.
 *
 *  Child of Navier Stokes vorticity equation solver, this class calls all the
 *  methods from `NSVE`, and in addition integrates `complex particles`
 *  in the resulting velocity field.
 *  By `complex particles` we mean neutrally buoyant, very small particles,
 *  which have an orientation and actively swim in that direction, and they may
 *  also interact with each other, trying to reorient to a common orientation.
 */

template <typename rnumber>
class NSVEcomplex_particles: public NSVE<rnumber>
{
    public:

        /* parameters that are read in read_parameters */
        int niter_part;
        long long int nparticles;
        int tracers0_integration_steps;
        int tracers0_neighbours;
        int tracers0_smoothness;

        double cutoff;
        double inner_v0;
        double lambda;
        bool enable_p2p;
        bool enable_inner;
        bool enable_vorticity_omega;

        /* other stuff */
        std::unique_ptr<abstract_particles_system<long long int, double>> ps;
        // TODO P2P use a reader with particle data
        particles_output_hdf5<long long int, double,6> *particles_output_writer_mpi;
        particles_output_sampling_hdf5<long long int, double, double, 3> *particles_sample_writer_mpi;
        // field for sampling velocity gradient
        field<rnumber, FFTW, THREExTHREE> *nabla_u;


        NSVEcomplex_particles(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE<rnumber>(
                    COMMUNICATOR,
                    simulation_name),
            cutoff(10), inner_v0(1), lambda(1.0), enable_p2p(true), enable_inner(true), enable_vorticity_omega(true){}
        ~NSVEcomplex_particles() noexcept(false){}

        int initialize(void);
        int step(void);
        int finalize(void);

        int read_parameters(void);
        int write_checkpoint(void);
        int do_stats(void);
};

#endif//NSVECOMPLEX_PARTICLES_HPP

