/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "bandpass_stats.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int bandpass_stats<rnumber>::initialize(void)
{
    DEBUG_MSG("entered bandpass_stats::initialize\n");
    TIMEZONE("bandpass_stats::initialize");
    this->NSVE_field_stats<rnumber>::initialize();
    DEBUG_MSG("after NSVE_field_stats::initialize\n");
    this->kk = new kspace<FFTW, SMOOTH>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->checkpoints_per_file = hdf5_tools::read_value<int>(parameter_file, "/parameters/checkpoints_per_file");
    if (this->checkpoints_per_file == INT_MAX) // value returned if dataset does not exist
        this->checkpoints_per_file = 1;
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/bandpass_stats/parameters/iteration_list");
    this->max_velocity_estimate = hdf5_tools::read_value<double>(
            parameter_file,
            "/bandpass_stats/parameters/max_velocity_estimate");
    this->k0list = hdf5_tools::read_vector<double>(
            parameter_file,
            "/bandpass_stats/parameters/k0list");
    this->k1list = hdf5_tools::read_vector<double>(
            parameter_file,
            "/bandpass_stats/parameters/k1list");
    this->filter_type = hdf5_tools::read_string(
            parameter_file,
            "/bandpass_stats/parameters/filter_type");
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    assert(this->k0list.size() == this->k1list.size());
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + "_post.h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    else
    {
        this->stat_file = 0;
    }
    int data_file_problem;
    if (this->myrank == 0)
        data_file_problem = hdf5_tools::require_size_file_datasets(
                this->stat_file,
                "bandpass_stats",
                (this->iteration_list.back() / this->niter_out) + 1);
    MPI_Bcast(&data_file_problem, 1, MPI_INT, 0, this->comm);
    if (data_file_problem > 0)
    {
        std::cerr <<
            data_file_problem <<
            " problems setting sizes of file datasets.\ntrying to exit now." <<
            std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int bandpass_stats<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("bandpass_stats::work_on_current_iteration");
    this->read_current_cvorticity();
    field<rnumber, FFTW, THREE> *vel = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);


    /// compute the velocity, store in vel
    invert_curl(
        this->kk,
        this->vorticity,
        vel);

    /// allocate temporary filtered field container
    field<rnumber, FFTW, THREE> *filtered_vel0 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    field<rnumber, FFTW, THREE> *filtered_vel1 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);

    /// make max vel estimate std::vector, required by compute_rspace_stats
    std::vector<double> max_vel_estimate;
    max_vel_estimate.resize(4, max_velocity_estimate / std::sqrt(3));
    max_vel_estimate[3] = max_velocity_estimate;

    /// initialize `stat_group`.
    /// i.e. open HDF5 file for writing stats
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "bandpass_stats",
                H5P_DEFAULT);
    else
        stat_group = 0;

    /// loop over all bands
    for (int band_counter = 0; band_counter < int(this->k0list.size()); band_counter++)
    {
        *filtered_vel0 = *vel;
        *filtered_vel1 = *vel;
        this->kk->template filter_calibrated_ell<rnumber, THREE>(
                filtered_vel0->get_cdata(),
                4*acos(0) / this->k0list[band_counter],
                this->filter_type);
        this->kk->template filter_calibrated_ell<rnumber, THREE>(
                filtered_vel1->get_cdata(),
                4*acos(0) / this->k1list[band_counter],
                this->filter_type);
        this->kk->CLOOP(
                [&](
                    ptrdiff_t cindex,
                    ptrdiff_t xindex,
                    ptrdiff_t yindex,
                    ptrdiff_t zindex){
                for (unsigned int component=0; component < 3; component++)
                    for (unsigned int cc=0; cc < 2; cc++)
                        filtered_vel1->cval(cindex, component, cc) -= filtered_vel0->cval(cindex, component, cc);
            });
        filtered_vel1->ift();
        filtered_vel1->compute_rspace_stats(
                stat_group,
                "velocity_band" + std::to_string(band_counter),
                this->iteration / this->niter_out,
                max_vel_estimate);
    }

    if (this->myrank == 0)
        H5Gclose(stat_group);

    delete filtered_vel1;
    delete filtered_vel0;
    delete vel;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int bandpass_stats<rnumber>::finalize(void)
{
    TIMEZONE("bandpass_stats::finalize");
    delete this->kk;
    this->NSVE_field_stats<rnumber>::finalize();
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    return EXIT_SUCCESS;
}

template class bandpass_stats<float>;
template class bandpass_stats<double>;

