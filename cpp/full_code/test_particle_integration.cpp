/******************************************************************************
*                                                                             *
*  Copyright 2022 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#include "test_particle_integration.hpp"
#include "particles/particles_input_grid.hpp"
#include "particles/rhs/tracer_rhs.hpp"

template <typename rnumber>
int test_particle_integration<rnumber>::initialize()
{
    TIMEZONE("test_particle_integration::initialize");
    this->read_parameters();

    this->velocity_back = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);

    this->kk = new kspace<FFTW, SMOOTH>(
            this->velocity_back->clayout,
            this->dkx, this->dky, this->dkz);

    this->velocity_front = new field_tinterpolator<rnumber, FFTW, THREE, NONE>();
    this->velocity_front->set_field(this->velocity_back);

    if (this->myrank == 0)
    {
        hid_t stat_file = H5Fopen(
                (this->simname + std::string(".h5")).c_str(),
                H5F_ACC_RDWR,
                H5P_DEFAULT);
        this->kk->store(stat_file);
        H5Fclose(stat_file);
    }

    return EXIT_SUCCESS;
}


template <typename rnumber>
template <int neighbours, int smoothness>
int test_particle_integration<rnumber>::integrate(
        const int order,
        const int factor,
        const int total_iterations)
{
    TIMEZONE("test_particle_integration::integrate");
    DEBUG_MSG("entered integrate<%d, %d>(order=%d, factor=%d, iterations=%d)\n",
            neighbours,
            smoothness,
            order,
            factor,
            total_iterations);
    // create a particle object
    particle_set<3, neighbours, smoothness> pset(
            this->velocity_back->rlayout,
            this->kk->dkx,
            this->kk->dky,
            this->kk->dkz);

    // initialize particles
    particles_input_grid<long long int, double, 3> pinput(
            this->comm,
            this->nxparticles,
            this->nzparticles,
            pset.getSpatialLowLimitZ(),
            pset.getSpatialUpLimitZ());

    pset.init(pinput);

    tracer_rhs<rnumber, FFTW, NONE> rhs;
    rhs.setVelocity(this->velocity_front);

    particle_solver psolver(pset, 0);

    const std::string species_name = (
            "particle_o" + std::to_string(order) +
            "_n" + std::to_string(neighbours) +
            "_m" + std::to_string(smoothness) +
            "_f" + std::to_string(factor));

    for (int tt = 0; tt < total_iterations*factor; tt++)
    {
        // output
        if (tt % 4 == 0)
        {
            pset.writeStateTriplet(
                0,
                this->particles_sample_writer_mpi,
                species_name,
                "position",
                tt);
            pset.writeSample(
                this->velocity_back,
                this->particles_sample_writer_mpi,
                species_name,
                "velocity",
                tt);
        }
        // time step
        switch(order)
        {
            case 1:
                psolver.Euler(this->dt / factor, rhs);
                break;
            case 2:
                psolver.Heun(this->dt / factor, rhs);
                break;
            case 4:
                psolver.cRK4(this->dt / factor, rhs);
                break;
            default:
                assert(false);
                break;
        }
    }
    // output final state
    {
        int tt = total_iterations*factor;
        pset.writeStateTriplet(
            0,
            this->particles_sample_writer_mpi,
            species_name,
            "position",
            tt);
        pset.writeSample(
            this->velocity_back,
            this->particles_sample_writer_mpi,
            species_name,
            "velocity",
            tt);
    }

    return EXIT_SUCCESS;
}


template <typename rnumber>
int test_particle_integration<rnumber>::do_work()
{
    TIMEZONE("test_particle_integration::do_work");

    // create field
    make_gaussian_random_field(
            this->kk,
            this->velocity_back,
            this->random_seed,
            0.4,  // dissipation
            1.,   // Lint
            4*acos(0) / this->nx, // etaK
            6.78, // default
            0.40, // default
            3*3./2); // 3/2 to account for divfree call below
                     // 3 because I want a larger amplitude
    this->kk->template force_divfree<rnumber>(this->velocity_back->get_cdata());

    // take field to real space
    this->velocity_back->ift();

    // create a particle object
    particle_set<3, 1, 1> pset(
            this->velocity_back->rlayout,
            this->kk->dkx,
            this->kk->dky,
            this->kk->dkz);
    particles_input_grid<long long int, double, 3> pinput(
            this->comm,
            this->nxparticles,
            this->nzparticles,
            pset.getSpatialLowLimitZ(),
            pset.getSpatialUpLimitZ());

    pset.init(pinput);

    // initialize writer
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                this->comm,
                pset.getTotalNumberOfParticles(),
                (this->simname + "_particles.h5"),
                "particle",
                "position/0");
    this->particles_sample_writer_mpi->setParticleFileLayout(pset.getParticleFileLayout());

    // test integration
    for (int oo = 1; oo < 5; oo *= 2)
    for (int ff = 1; ff < 9; ff *= 2)
    {
        this->template integrate<0, 0>(oo, ff, this->niter_todo);
        this->template integrate<1, 1>(oo, ff, this->niter_todo);
        this->template integrate<2, 1>(oo, ff, this->niter_todo);
        this->template integrate<3, 1>(oo, ff, this->niter_todo);
        this->template integrate<4, 1>(oo, ff, this->niter_todo);
        this->template integrate<5, 1>(oo, ff, this->niter_todo);
        this->template integrate<2, 2>(oo, ff, this->niter_todo);
        this->template integrate<3, 2>(oo, ff, this->niter_todo);
        this->template integrate<4, 2>(oo, ff, this->niter_todo);
        this->template integrate<5, 2>(oo, ff, this->niter_todo);
    }

    delete this->particles_sample_writer_mpi;

    return EXIT_SUCCESS;
}


template <typename rnumber>
int test_particle_integration<rnumber>::finalize()
{
    TIMEZONE("test_particle_integration::finalize");
    delete this->velocity_front;
    delete this->velocity_back;
    delete this->kk;
    return EXIT_SUCCESS;
}


template <typename rnumber>
int test_particle_integration<rnumber>::read_parameters()
{
    TIMEZONE("test_particle_integration::read_parameters");
    this->test::read_parameters();

    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->nxparticles = hdf5_tools::read_value<int>(parameter_file, "/parameters/nxparticles");
    this->nzparticles = hdf5_tools::read_value<int>(parameter_file, "/parameters/nzparticles");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "/parameters/dt");
    this->niter_todo = hdf5_tools::read_value<int>(parameter_file, "/parameters/niter_todo");
    this->random_seed = hdf5_tools::read_value<int>(parameter_file, "/parameters/field_random_seed");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}


template class test_particle_integration<float>;
template class test_particle_integration<double>;

