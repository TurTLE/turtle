/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef MAIN_CODE_HPP
#define MAIN_CODE_HPP


#include "field.hpp"
#include "scope_timer.hpp"


#include <cfenv>

int myrank, nprocs;

#ifdef PINCHECK_FOUND
#include <pincheck.hpp>

void print_pinning_info(void)
{
    // obtain string with pinning information on rank 0,
    // ranks >0 get an empty string
    const std::string pinning_info = pincheck::pincheck();
    if (myrank == 0)
    {
        std::cerr << "### pinning info begin" << std::endl;
        std::cerr << pinning_info;
        std::cerr << "### pinning info end" << std::endl;
    }
}

#else

#define print_pinning_info(...)

#endif

template <class DNS>
int main_code(
        int argc,
        char *argv[],
        const bool floating_point_exceptions)
{
    /**********************/
    /* floating point exception switch */
    if (floating_point_exceptions)
        feenableexcept(FE_INVALID | FE_OVERFLOW);
    else
        // using std::cerr because DEBUG_MSG requires myrank to be defined
        std::cerr << "FPE have been turned OFF" << std::endl;

    if (argc != 2)
    {
        std::cerr <<
            "Wrong number of command line arguments. Stopping." <<
            std::endl;
        MPI_Init(&argc, &argv);
        MPI_Finalize();
        return EXIT_SUCCESS;
    }
    std::string simname = std::string(argv[1]);
    /**********************/


    /**********************/
    /* initialize MPI environment */
#ifndef USE_FFTW_OMP
    MPI_Init(&argc, &argv);
#else
    int mpiprovided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpiprovided);
#endif

    /* turn off MPI profiling for initialization */
    MPI_Pcontrol(0);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

#ifndef USE_FFTW_OMP
    fftw_mpi_init();
    fftwf_mpi_init();
    DEBUG_MSG("There are %d processes\n", nprocs);
#else
    assert(mpiprovided >= MPI_THREAD_FUNNELED);

    const int nThreads = omp_get_max_threads();
    if (nThreads > 1){
        int result_init_threads = fftw_init_threads();
        variable_used_only_in_assert(result_init_threads);
        DEBUG_MSG("fftw_init_threads returned %d\n", result_init_threads);
        int result_init_threadsf = fftwf_init_threads();
        variable_used_only_in_assert(result_init_threadsf);
        DEBUG_MSG("fftwf_init_threads returned %d\n", result_init_threadsf);
    }
    fftw_mpi_init();
    fftwf_mpi_init();
    DEBUG_MSG("There are %d processes and %d threads\n",
              nprocs,
              nThreads);
    if (nThreads > 1){
        fftw_plan_with_nthreads(nThreads);
        fftwf_plan_with_nthreads(nThreads);
    }
#endif

    print_pinning_info();
    DEBUG_MSG("TurTLE version %s, simname = %s\n",
              TURTLE_VERSION,
              simname.c_str());

    /* read maximum MPI tag value */
    read_maximum_MPI_tag_value();
    /**********************/


    /**********************/
    /* import fftw wisdom */
    if (myrank == 0)
    {
        fftwf_import_wisdom_from_filename(
                (simname + std::string("_fftwf_wisdom.txt")).c_str());
        fftw_import_wisdom_from_filename(
                (simname + std::string("_fftw_wisdom.txt")).c_str());
    }
    fftwf_mpi_broadcast_wisdom(MPI_COMM_WORLD);
    fftw_mpi_broadcast_wisdom(MPI_COMM_WORLD);

    fftwf_set_timelimit(300);
    fftw_set_timelimit(300);
    /**********************/



    /**********************/
    /* actually run DNS */
    /* usage of assert:
     * we could use assert here, but I assume that any problems we can still
     * recover from should not be important enough to not clean up fftw and MPI
     * things.
     */
    DNS *dns = new DNS(
            MPI_COMM_WORLD,
            simname);
    int return_value_initialize = EXIT_FAILURE;
    int return_value_main_loop = EXIT_FAILURE;
    int return_value_finalize = EXIT_FAILURE;
    int global_return_value = EXIT_SUCCESS;
    return_value_initialize = dns->initialize();
    if (return_value_initialize == EXIT_SUCCESS) {
        // turn on MPI profiling for main loop
        MPI_Pcontrol(1);
        return_value_main_loop = dns->main_loop();
        // turn off MPI profiling
        MPI_Pcontrol(0);
        // we call finalize independently of main loop success
        return_value_finalize = dns->finalize();
    } else {
        DEBUG_MSG("problem calling dns->initialize(), return value is %d\n",
                  return_value_initialize);
        global_return_value = EXIT_FAILURE;
    }
    if (return_value_main_loop == EXIT_FAILURE) {
        DEBUG_MSG("problem calling dns->main_loop(), return value is %d\n",
                  return_value_main_loop);
        global_return_value = EXIT_FAILURE;
    }
    if (return_value_finalize == EXIT_FAILURE) {
        DEBUG_MSG("problem calling dns->finalize(), return value is %d\n",
                  return_value_finalize);
        global_return_value = EXIT_FAILURE;
    }

    delete dns;
    /**********************/


    /**********************/
    /* export fftw wisdom */
    fftwf_mpi_gather_wisdom(MPI_COMM_WORLD);
    fftw_mpi_gather_wisdom(MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    if (myrank == 0)
    {
        fftwf_export_wisdom_to_filename(
                (simname + std::string("_fftwf_wisdom.txt")).c_str());
        fftw_export_wisdom_to_filename(
                (simname + std::string("_fftw_wisdom.txt")).c_str());
    }
    /**********************/



    /**********************/
    /* clean up */
    fftwf_mpi_cleanup();
    fftw_mpi_cleanup();
#ifdef USE_FFTW_OMP
    if (nThreads > 1){
        fftw_cleanup_threads();
        fftwf_cleanup_threads();
    }
#endif
#ifdef USE_TIMING_OUTPUT
    global_timer_manager.show(MPI_COMM_WORLD);
    global_timer_manager.showMpi(MPI_COMM_WORLD);
    global_timer_manager.showHtml(MPI_COMM_WORLD);
#endif

    MPI_Finalize();
    /**********************/
    return global_return_value;
}


#endif//MAIN_CODE_HPP

