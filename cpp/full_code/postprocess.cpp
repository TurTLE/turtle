/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#include "scope_timer.hpp"
#include "hdf5_tools.hpp"
#include "full_code/postprocess.hpp"

int postprocess::main_loop(void)
{
    TIMEZONE("postprocess::main_loop");
    int return_value = EXIT_SUCCESS;
    this->start_simple_timer();
    for (unsigned int iteration_counter = 0;
         iteration_counter < iteration_list.size();
         iteration_counter++)
    {
        this->iteration = iteration_list[iteration_counter];
    #ifdef USE_TIMING_OUTPUT
        const std::string loopLabel = ("postprocess::main_loop-" +
                                       std::to_string(this->iteration));
        TIMEZONE(loopLabel.c_str());
    #endif
        const int work_result = this->work_on_current_iteration();
        if (work_result != EXIT_SUCCESS) {
            DEBUG_MSG("postprocess::work_on_current_iteration returned %d\n",
                    work_result);
            return_value = EXIT_FAILURE;
            break;
        }
        this->print_simple_timer(
                "iteration " + std::to_string(this->iteration));

        this->check_stopping_condition();
        if (this->stop_code_now)
            break;
    }
    return return_value;
}


int postprocess::read_parameters()
{
    TIMEZONE("postprocess::read_parameters");
    this->code_base::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->nu = hdf5_tools::read_value<double>(parameter_file, "parameters/nu");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "parameters/dt");
    this->fmode = hdf5_tools::read_value<int>(parameter_file, "parameters/fmode");
    this->famplitude = hdf5_tools::read_value<double>(parameter_file, "parameters/famplitude");
    this->friction_coefficient = hdf5_tools::read_value<double>(parameter_file, "parameters/friction_coefficient");
    this->fk0 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk0");
    this->fk1 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk1");
    this->variation_strength = hdf5_tools::read_value<double>(parameter_file, "parameters/variation_strength");
    this->variation_time_scale = hdf5_tools::read_value<double>(parameter_file, "parameters/variation_time_scale");
    this->energy = hdf5_tools::read_value<double>(parameter_file, "parameters/energy");
    this->injection_rate = hdf5_tools::read_value<double>(parameter_file, "parameters/injection_rate");
    this->forcing_type = hdf5_tools::read_string(parameter_file, "parameters/forcing_type");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

int postprocess::read_iteration_list(
        const std::string fname,
        const std::string group_name)
{
    TIMEZONE("postprocess::read_iteration_list");
    hid_t parameter_file = H5Fopen(
            fname.c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            (std::string("/")
            + group_name
            + std::string("/parameters/iteration_list")).c_str());
    H5Fclose(parameter_file);
    // the following MPI_Barrier ensures the file is free for arbitrary access
    // outside of this function call
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be>
int postprocess::copy_parameters_into(
        vorticity_equation<rnumber, be> *dst)
{
    TIMEZONE("postprocess::copy_parameters_into");
    dst->nu = this->nu;
    dst->fmode = this->fmode;
    dst->famplitude = this->famplitude;
    dst->fk0 = this->fk0;
    dst->fk1 = this->fk1;
    dst->injection_rate = this->injection_rate;
    dst->energy = this->energy;
    dst->friction_coefficient = this->friction_coefficient;
    dst->variation_strength = this->variation_strength;
    dst->variation_time_scale = this->variation_time_scale;
    dst->forcing_type = this->forcing_type;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int postprocess::copy_parameters_into(
        NSVE<rnumber> *dst)
{
    TIMEZONE("postprocess::copy_parameters_into NSVE");
    // code_base
    dst->dkx = this->dkx;
    dst->dky = this->dky;
    dst->dkz = this->dkz;
    dst->nx = this->nx;
    dst->ny = this->ny;
    dst->nz = this->nz;
    dst->dealias_type = this->dealias_type;
    // postprocess
    dst->nu = this->nu;
    dst->fmode = this->fmode;
    dst->famplitude = this->famplitude;
    dst->fk0 = this->fk0;
    dst->fk1 = this->fk1;
    dst->injection_rate = this->injection_rate;
    dst->energy = this->energy;
    dst->friction_coefficient = this->friction_coefficient;
    dst->forcing_type = this->forcing_type;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int postprocess::copy_parameters_into(
        NSE<rnumber> *dst)
{
    TIMEZONE("postprocess::copy_parameters_into NSE");
    // code_base
    dst->dkx = this->dkx;
    dst->dky = this->dky;
    dst->dkz = this->dkz;
    dst->nx = this->nx;
    dst->ny = this->ny;
    dst->nz = this->nz;
    dst->dealias_type = this->dealias_type;
    // postprocess
    dst->nu = this->nu;
    dst->fmode = this->fmode;
    dst->famplitude = this->famplitude;
    dst->fk0 = this->fk0;
    dst->fk1 = this->fk1;
    dst->injection_rate = this->injection_rate;
    dst->energy = this->energy;
    dst->friction_coefficient = this->friction_coefficient;
    dst->forcing_type = this->forcing_type;
    return EXIT_SUCCESS;
}

template int postprocess::copy_parameters_into<float, FFTW>(
        vorticity_equation<float, FFTW> *dst);

template int postprocess::copy_parameters_into<double, FFTW>(
        vorticity_equation<double, FFTW> *dst);

template int postprocess::copy_parameters_into<float>(
        NSE<float> *dst);

template int postprocess::copy_parameters_into<double>(
        NSE<double> *dst);

template int postprocess::copy_parameters_into<float>(
        NSVE<float> *dst);

template int postprocess::copy_parameters_into<double>(
        NSVE<double> *dst);

