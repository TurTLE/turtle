/**********************************************************************
*                                                                     *
*  Copyright 2024 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#ifndef CHECK_DIVERGENCE_HPP
#define CHECK_DIVERGENCE_HPP

#include "full_code/NSVE_field_stats.hpp"

template <typename rnumber>
class check_divergence: public NSVE_field_stats<rnumber>
{
    public:
        int checkpoints_per_file;
        kspace<FFTW, SMOOTH>* kk;
        field<rnumber, FFTW, ONE>* div_field;

        check_divergence(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~check_divergence(){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};

#endif//CHECK_DIVERGENCE_HPP

