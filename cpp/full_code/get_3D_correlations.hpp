/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef GET_3D_CORRELATIONS_HPP
#define GET_3D_CORRELATIONS_HPP

#include "full_code/NSVE_field_stats.hpp"

template <typename rnumber>
class get_3D_correlations: public NSVE_field_stats<rnumber>
{
    public:
        hid_t stat_file;
        int checkpoints_per_file;
        bool full_snapshot_output;
        kspace<FFTW, SMOOTH> *kk;
        field<rnumber, FFTW, THREE> *vel;
        field<rnumber, FFTW, THREExTHREE> *Rij;

        get_3D_correlations(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~get_3D_correlations(){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};

#endif//GET_3D_CORRELATIONS_HPP

