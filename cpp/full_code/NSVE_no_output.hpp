/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef NSVE_NO_OUTPUT_HPP
#define NSVE_NO_OUTPUT_HPP

#include "full_code/NSVE.hpp"

template <typename rnumber>
class NSVE_no_output: public NSVE<rnumber>
{
    public:
    NSVE_no_output(
            const MPI_Comm COMMUNICATOR,
            const std::string &simulation_name):
        NSVE<rnumber>(
                COMMUNICATOR,
                simulation_name){}
    ~NSVE_no_output() noexcept(false){}
    int write_checkpoint(void)
    {
        TIMEZONE("NSVE_no_output::write_checkpoint");
        return EXIT_SUCCESS;
    }
};

#endif//NSVE_NO_OUTPUT_HPP

