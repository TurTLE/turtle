/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "full_code/NSVEp_extra_sampling.hpp"

template <typename rnumber>
int NSVEp_extra_sampling<rnumber>::initialize(void)
{
    TIMEZONE("NSVEp_extra_sampling::initialize");
    this->NSVEparticles<rnumber>::initialize();

    /// allocate grad vel field
    this->nabla_u = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->fs->cvorticity->fftw_plan_rigor);
    this->pressure = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->fs->cvorticity->fftw_plan_rigor);
    this->nabla_p = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->fs->cvorticity->fftw_plan_rigor);
    this->Hessian_p = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->fs->cvorticity->fftw_plan_rigor);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEp_extra_sampling<rnumber>::finalize(void)
{
    TIMEZONE("NSVEp_extra_sampling::finalize");
    delete this->nabla_u;
    delete this->pressure;
    delete this->nabla_p;
    delete this->Hessian_p;
    this->NSVEparticles<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEp_extra_sampling<rnumber>::do_stats()
{
    TIMEZONE("NSVEp_extra_sampling::do_stats");
    this->NSVEparticles<rnumber>::do_stats();
    if (!(this->iteration % this->niter_part == 0))
        return EXIT_SUCCESS;

    // allocate temporary array
    std::unique_ptr<double[]> pdata(new double[9*this->ps->getLocalNbParticles()]);

    if (this->sample_velocity_gradient == 1)
    {
        /// fs->cvelocity should contain the velocity in Fourier space
        this->fs->compute_velocity(this->fs->cvorticity);
        compute_gradient(
                this->fs->kk,
                this->fs->cvelocity,
                this->nabla_u);
        this->nabla_u->ift();

        // clean up and sample
        std::fill_n(pdata.get(), 9*this->ps->getLocalNbParticles(), 0);
        this->ps->sample_compute_field(*this->nabla_u, pdata.get());

        // write to file
        this->particles_sample_writer_mpi->template save_dataset<9>(
                "tracers0",
                "velocity_gradient",
                this->ps->getParticlesState(),
                &pdata,
                this->ps->getParticlesIndexes(),
                this->ps->getLocalNbParticles(),
                this->ps->get_step_idx()-1);
    }

    if ((this->sample_pressure == 1) ||
        (this->sample_pressure_gradient == 1) ||
        (this->sample_pressure_Hessian == 1))
    {
        this->fs->compute_pressure(this->pressure);

        if ((this->sample_pressure_gradient == 1) ||
            (this->sample_pressure_Hessian == 1))
        {
            compute_gradient(
                    this->fs->kk,
                    this->pressure,
                    this->nabla_p);

            if (this->sample_pressure_Hessian == 1)
            {
                compute_gradient(
                        this->fs->kk,
                        this->nabla_p,
                        this->Hessian_p);
            }
        }

        if (this->sample_pressure == 1)
        {
        // sample pressure
            this->pressure->ift();
            std::fill_n(pdata.get(), this->ps->getLocalNbParticles(), 0);
            this->ps->sample_compute_field(*this->pressure, pdata.get());
            this->particles_sample_writer_mpi->template save_dataset<1>(
                    "tracers0",
                    "pressure",
                    this->ps->getParticlesState(),
                    &pdata,
                    this->ps->getParticlesIndexes(),
                    this->ps->getLocalNbParticles(),
                    this->ps->get_step_idx()-1);
        }

        if (this->sample_pressure_gradient == 1)
        {
        // sample pressure gradient
            this->nabla_p->ift();
            std::fill_n(pdata.get(), 3*this->ps->getLocalNbParticles(), 0);
            this->ps->sample_compute_field(*this->nabla_p, pdata.get());
            this->particles_sample_writer_mpi->template save_dataset<3>(
                    "tracers0",
                    "pressure_gradient",
                    this->ps->getParticlesState(),
                    &pdata,
                    this->ps->getParticlesIndexes(),
                    this->ps->getLocalNbParticles(),
                    this->ps->get_step_idx()-1);
        }

        if (this->sample_pressure_Hessian == 1)
        {
        // sample pressure Hessian
            this->Hessian_p->ift();
            std::fill_n(pdata.get(), 9*this->ps->getLocalNbParticles(), 0);
            this->ps->sample_compute_field(*this->Hessian_p, pdata.get());
            this->particles_sample_writer_mpi->template save_dataset<9>(
                    "tracers0",
                    "pressure_Hessian",
                    this->ps->getParticlesState(),
                    &pdata,
                    this->ps->getParticlesIndexes(),
                    this->ps->getLocalNbParticles(),
                    this->ps->get_step_idx()-1);
        }
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVEp_extra_sampling<rnumber>::read_parameters(void)
{
    TIMEZONE("NSVEp_extra_sampling::read_parameters");
    this->NSVEparticles<rnumber>::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->sample_pressure = hdf5_tools::read_value<int>(parameter_file, "parameters/sample_pressure");
    this->sample_pressure_gradient = hdf5_tools::read_value<int>(parameter_file, "parameters/sample_pressure_gradient");
    this->sample_pressure_Hessian = hdf5_tools::read_value<int>(parameter_file, "parameters/sample_pressure_Hessian");
    this->sample_velocity_gradient = hdf5_tools::read_value<int>(parameter_file, "parameters/sample_velocity_gradient");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class NSVEp_extra_sampling<float>;
template class NSVEp_extra_sampling<double>;

