/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef FIELD_SINGLE_TO_DOUBLE_HPP
#define FIELD_SINGLE_TO_DOUBLE_HPP

#include "full_code/NSVE_field_stats.hpp"

template <typename rnumber>
class field_single_to_double: public NSVE_field_stats<rnumber>
{
    public:
        int checkpoints_per_file;
        kspace<FFTW, SMOOTH> *kk;

        field<double, FFTW, THREE> *vec_field_double;

        field_single_to_double(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~field_single_to_double() noexcept(false){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};

#endif//FIELD_SINGLE_TO_DOUBLE_HPP

