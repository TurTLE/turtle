/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef DIRECT_NUMERICAL_SIMULATION_HPP
#define DIRECT_NUMERICAL_SIMULATION_HPP

#include "hdf5_tools.hpp"
#include "full_code/code_base.hpp"

class direct_numerical_simulation: public code_base
{
    public:
        std::string name;
        int checkpoint;
        int checkpoints_per_file;
        int niter_out;
        int niter_stat;
        int niter_todo;
        hid_t stat_file;

        direct_numerical_simulation(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            code_base(
                    COMMUNICATOR,
                    simulation_name){
                this->stat_file = H5E_BADFILE;
            }
        virtual ~direct_numerical_simulation() noexcept(false){}

        virtual int read_parameters(void);
        virtual int write_checkpoint(void) = 0;

        /** \brief Initialize DNS
         *
         * Must assign :code:`this->stat_file`.
         */
        virtual int initialize(void) = 0;
        virtual int step(void) = 0;
        virtual int do_stats(void) = 0;
        virtual int finalize(void) = 0;

        int main_loop(void);
        int read_iteration(void);
        int write_iteration(void);
        int grow_file_datasets(void);

        /** \brief Basic computation of current checkpoint
         *
         * This will fail if the values of `niter_out`, `niter_todo`, or
         * `checkpoints_per_file` are changed.
         */
        int update_checkpoint(void);

        const inline std::string get_current_fname()
        {
            return (
                    std::string(this->simname) +
                    std::string("_checkpoint_") +
                    std::to_string(this->checkpoint) +
                    std::string(".h5"));
        }
};

#endif//DIRECT_NUMERICAL_SIMULATION_HPP

