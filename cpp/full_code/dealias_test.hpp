/**********************************************************************
*                                                                     *
*  Copyright 2019 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef DEALIAS_TEST_HPP
#define DEALIAS_TEST_HPP



#include "field.hpp"
#include "full_code/test.hpp"

/** \brief Basic sanity check for dealiasing.
 *
 */

template <typename rnumber>
class dealias_test: public test
{
    public:

        /* parameters that are read in read_parameters */
        double max_velocity_estimate;
        double spectrum_dissipation;
        double spectrum_Lint;
        double spectrum_etaK;
        double spectrum_large_scale_const;
        double spectrum_small_scale_const;
        int random_seed;

        /* other stuff */
        kspace<FFTW, SMOOTH> *kk;
        kspace<FFTW, ONE_HALF> *kk4;
        kspace<FFTW, TWO_THIRDS> *kk3;
        field<rnumber, FFTW, THREE> *vector_field0, *vector_field1;

        dealias_test(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            test(
                    COMMUNICATOR,
                    simulation_name){}
        ~dealias_test() noexcept(false){}

        int initialize(void);
        int do_work(void);
        int finalize(void);
        int read_parameters(void);
};

#endif//DEALIAS_TEST_HPP

