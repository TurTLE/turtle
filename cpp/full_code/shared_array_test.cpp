/******************************************************************************
*                                                                             *
*  Copyright 2023 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/


#include "shared_array_test.hpp"
#include "scope_timer.hpp"

#include <cmath>
#include <random>

const int array_size = 50;
const int number_of_loops = 100;


template <typename rnumber>
int shared_array_test<rnumber>::initialize(void)
{
    TIMEZONE("shared_array_test::initialize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int shared_array_test<rnumber>::finalize(void)
{
    TIMEZONE("shared_array_test::finalize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int shared_array_test<rnumber>::read_parameters()
{
    TIMEZONE("shared_array_test::read_parameters");
    this->test::read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int shared_array_test<rnumber>::do_work(void)
{
    TIMEZONE("shared_array_test::do_work");
    double value = 0.0;
    for (auto ii = 0; ii < number_of_loops; ii++)
    {
        std::cerr << "shared_array test loop " << ii << std::endl;
        shared_array<double> local_value(
                array_size,
                shared_array_zero_initializer<double, array_size>);
        #pragma omp parallel for
        for (auto iii = 0; iii < array_size; iii++)
            local_value.getMine()[iii] += value - 0.0; // trying to get around compiler optimizations
        local_value.mergeParallel();
        for (auto iii = 0; iii < array_size; iii++)
            value += local_value.getMasterData()[iii];
    }
    if (value == 0)
        return EXIT_SUCCESS;
    else
        return EXIT_FAILURE;
}

template class shared_array_test<float>;
template class shared_array_test<double>;

