/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "get_3D_correlations.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int get_3D_correlations<rnumber>::initialize(void)
{
    TIMEZONE("get_3D_correlations::initialize");
    this->NSVE_field_stats<rnumber>::initialize();

    // allocate kspace
    this->kk = new kspace<FFTW, SMOOTH>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);
    // allocate field for velocity
    this->vel = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    this->vel->real_space_representation = false;
    // allocate field for velocity correlations
    this->Rij = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    this->Rij->real_space_representation = false;

    // open parameter file
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->checkpoints_per_file = hdf5_tools::read_value<int>(parameter_file, "/parameters/checkpoints_per_file");
    if (this->checkpoints_per_file == INT_MAX) // value returned if dataset does not exist
        this->checkpoints_per_file = 1;
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/get_3D_correlations/parameters/iteration_list");
    this->full_snapshot_output = bool(
            hdf5_tools::read_value<int>(
                parameter_file,
                "/get_3D_correlations/parameters/full_snapshot_output"));
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    if (this->myrank == 0) {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + "_post.h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    } else {
        this->stat_file = 0;
        }
    int data_file_problem;
    if (this->myrank == 0)
        data_file_problem = hdf5_tools::require_size_file_datasets(
                this->stat_file,
                "get_3D_correlations",
                (this->iteration_list.back() / this->niter_out) + 1);
    MPI_Bcast(&data_file_problem, 1, MPI_INT, 0, this->comm);
    if (data_file_problem > 0)
    {
        std::cerr <<
            data_file_problem <<
            " problems setting sizes of file datasets.\ntrying to exit now." <<
            std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int compute_correlation(
        kspace<FFTW, SMOOTH> *kk,
        field<rnumber, FFTW, THREE> *vel,
        field<rnumber, FFTW, THREExTHREE> *Rij,
        hid_t group,
        const int toffset,
        const std::string label = "velocity",
        const std::string field_fname = "",
        const int iteration = 0)
{
    *(Rij) = 0.0;
    Rij->real_space_representation = false;

    const double dkvolume = kk->dkx*kk->dky*kk->dkz;
    // compute Rij
    // the computation is taken from Pope
    // this corresponds to $Rij(\mathbf{r}) = \langle u_i(\textbf{x}) u_j(\textbf{x} + \textbf{r})\rangle$
    // (note that "r" appears in "u_j")
    kk->CLOOP(
            [&](ptrdiff_t cindex,
                ptrdiff_t xindex,
                ptrdiff_t yindex,
                ptrdiff_t zindex,
                const double k2){
            // compute v_i[-k] v_j[k]
            for (unsigned int i=0; i<3; i++)
            for (unsigned int j=0; j<3; j++)
            {
                Rij->cval(cindex, i, j, 0) = (
                    vel->cval(cindex, i, 0)*vel->cval(cindex, j, 0) +
                    vel->cval(cindex, i, 1)*vel->cval(cindex, j, 1)) / dkvolume;
                Rij->cval(cindex, i, j, 1) = (
                    vel->cval(cindex, i, 0)*vel->cval(cindex, j, 1) -
                    vel->cval(cindex, i, 1)*vel->cval(cindex, j, 0)) / dkvolume;
            }
            });

    // go to real space
    Rij->ift();

    /// output Rij field
    if (field_fname.size() > 0) {
        std::cerr << "fname size is " << field_fname.size()
            << ", and fname is " << field_fname << std::endl;
        Rij->io(
                field_fname,
                "Rij",
                iteration,
                false);
    }

    // extract edges of domain (used for longitudinal and transversal components).
    double Rijx[9*Rij->get_nx()];
    double Rijy[9*Rij->get_ny()];
    double Rijz[9*Rij->get_nz()];
    double Rijz_local[9*Rij->get_nz()];
    std::fill_n(Rijz_local, 9*Rij->get_nz(), 0);
    std::fill_n(Rijz, 9*Rij->get_nz(), 0);
    for (int i=0; i<3; i++)
    {
        for (int j=0; j<3; j++)
        {
            if (Rij->rlayout->myrank == Rij->rlayout->rank[0][0])
            {
                for (int ix=0; ix<Rij->get_nx(); ix++)
                    Rijx[9*ix + (i*3+j)] = Rij->rval(
                            ix, i, j);
                for (int iy=0; iy<Rij->get_ny(); iy++)
                    Rijy[9*iy + (i*3+j)] = Rij->rval(
                            iy*(Rij->get_nx()+2), i, j);
            }
            for (std::size_t iiz=0; iiz<Rij->rlayout->subsizes[0]; iiz++)
                Rijz_local[(i*3+j) + 9*(iiz + Rij->rlayout->starts[0])] =
                    Rij->rval(iiz*(Rij->get_nx()+2)*Rij->get_ny(), i, j);
        }
    }
    MPI_Allreduce(
            Rijz_local,
            Rijz,
            9*Rij->get_nz(),
            MPI_DOUBLE,
            MPI_SUM,
            Rij->comm);

    // output edges of domain
    if (Rij->rlayout->myrank == 0)
    {
        hid_t dset, mspace, wspace;
        hsize_t count[4], offset[4];
        offset[0] = toffset;
        offset[1] = 0;
        offset[2] = 0;
        offset[3] = 0;
        count[0] = 1;
        count[1] = Rij->get_nx();
        count[2] = 3;
        count[3] = 3;
        dset = H5Dopen(
                group,
                (std::string("correlations/R^") + label + std::string("_ij(0, 0, x)")).c_str(),
                H5P_DEFAULT);
        mspace = H5Screate_simple(4, count, NULL);
        wspace = H5Dget_space(dset);
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_DOUBLE, mspace, wspace, H5P_DEFAULT, Rijx);
        H5Dclose(dset);
        H5Sclose(wspace);
        H5Sclose(mspace);
        dset = H5Dopen(
                group,
                (std::string("correlations/R^") + label + std::string("_ij(0, y, 0)")).c_str(),
                H5P_DEFAULT);
        count[1] = Rij->get_ny();
        mspace = H5Screate_simple(4, count, NULL);
        wspace = H5Dget_space(dset);
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_DOUBLE, mspace, wspace, H5P_DEFAULT, Rijy);
        H5Dclose(dset);
        H5Sclose(wspace);
        H5Sclose(mspace);
        dset = H5Dopen(
                group,
                (std::string("correlations/R^") + label + std::string("_ij(z, 0, 0)")).c_str(),
                H5P_DEFAULT);
        count[1] = Rij->get_nz();
        mspace = H5Screate_simple(4, count, NULL);
        wspace = H5Dget_space(dset);
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_DOUBLE, mspace, wspace, H5P_DEFAULT, Rijz);
        H5Dclose(dset);
        H5Sclose(wspace);
        H5Sclose(mspace);
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int get_3D_correlations<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("get_3D_correlations::work_on_current_iteration");
    this->read_current_cvorticity();

    invert_curl(
        this->kk,
        this->vorticity,
        this->vel);

    std::string fname = "";
    if (this->full_snapshot_output) {
        fname = (
             this->simname +
             std::string("_checkpoint_Rij_") +
             std::to_string(this->iteration / (this->niter_out*this->checkpoints_per_file)) +
             std::string(".h5"));
    }
    /// initialize `stat_group`.
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "get_3D_correlations",
                H5P_DEFAULT);
    else
        stat_group = 0;

    compute_correlation(
            this->kk,
            this->vel,
            this->Rij,
            stat_group,
            this->iteration / this->niter_out,
            "velocity",
            fname,
            this->iteration);

    /// close stat group
    if (this->myrank == 0)
        H5Gclose(stat_group);

    return EXIT_SUCCESS;
}

template <typename rnumber>
int get_3D_correlations<rnumber>::finalize(void)
{
    TIMEZONE("get_3D_correlations::finalize");
    delete this->Rij;
    delete this->vel;
    delete this->kk;
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    this->NSVE_field_stats<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template class get_3D_correlations<float>;
template class get_3D_correlations<double>;

