/******************************************************************************
*                                                                             *
*  Copyright 2019 Max Planck Institute for Dynamics and Self-Organization     *
*                                                                             *
*  This file is part of bfps.                                                 *
*                                                                             *
*  bfps is free software: you can redistribute it and/or modify               *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  bfps is distributed in the hope that it will be useful,                    *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with bfps.  If not, see <http://www.gnu.org/licenses/>               *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include <string>
#include <cmath>
#include "NSE_alt_dealias.hpp"
#include "scope_timer.hpp"
#include "fftw_tools.hpp"
#include "hdf5_tools.hpp"
#include "shared_array.hpp"


template <typename rnumber>
int NSE_alt_dealias<rnumber>::initialize(void)
{
    TIMEZONE("NSE_alt_dealias::initialize");
    this->read_iteration();
    this->read_parameters();
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    int data_file_problem;
    if (this->myrank == 0)
        data_file_problem = this->grow_file_datasets();
    MPI_Bcast(&data_file_problem, 1, MPI_INT, 0, this->comm);
    if (data_file_problem > 0)
    {
        std::cerr <<
            data_file_problem <<
            " problems growing file datasets.\ntrying to exit now." <<
            std::endl;
        return EXIT_FAILURE;
    }

    /* allocate vector fields */
    this->velocity = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_velocity1 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_velocity2 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_vec_field0 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    this->tmp_vec_field1 = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    /* initialize kspace */
    this->kk = new kspace<FFTW, SMOOTH>(
            this->velocity->clayout, this->dkx, this->dky, this->dkz);

    {
        this->velocity->real_space_representation = false;
        this->velocity->io(
                this->get_current_fname(),
                "velocity",
                this->iteration,
                true);
        TIMEZONE("NSE_alt_dealias::initialize::read_initial_condition");
        this->kk->template low_pass<rnumber, THREE>(
                this->velocity->get_cdata(),
                this->kk->kM);
        this->kk->template force_divfree<rnumber>(
                this->velocity->get_cdata());
        this->velocity->symmetrize();
        MPI_Barrier(this->comm);
    }

    if (this->myrank == 0 && this->iteration == 0)
        this->kk->store(stat_file);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::step(void)
{
    TIMEZONE("NSE_alt_dealias::step");
    this->iteration++;
    //return this->Euler_step();
    return this->ShuOsher();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::add_field_band(
        field<rnumber, FFTW, THREE> *dst,
        field<rnumber, FFTW, THREE> *src,
        const double k0,
        const double k1,
        const double prefactor)
{
    TIMEZONE("NSE_alt_dealias::add_field_band");
    this->kk->CLOOP(
                [&](const ptrdiff_t cindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
        const double knorm = sqrt(this->kk->kx[xindex]*this->kk->kx[xindex] +
                                  this->kk->ky[yindex]*this->kk->ky[yindex] +
                                  this->kk->kz[zindex]*this->kk->kz[zindex]);
        if ((k0 <= knorm) &&
            (k1 >= knorm))
            for (int c=0; c<3; c++)
                for (int i=0; i<2; i++)
                    dst->cval(cindex,c,i) += prefactor*src->cval(cindex,c,i);
    }
    );
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::add_forcing_term(
        field<rnumber, FFTW, THREE> *vel,
        field<rnumber, FFTW, THREE> *dst)
{
    TIMEZONE("NSE_alt_dealias::add_forcing_term");
    if (this->forcing_type == std::string("linear"))
    {
        return this->add_field_band(
                dst, this->velocity,
                this->fk0, this->fk1,
                this->famplitude);
    }
    if ((this->forcing_type == std::string("fixed_energy_injection_rate")) ||
        (this->forcing_type == std::string("fixed_energy_injection_rate_and_drag")))
    {
        // first, compute energy in shell
        shared_array<double> local_energy_in_shell(1);
        double energy_in_shell = 0;
        this->kk->CLOOP(
                    [&](const ptrdiff_t cindex,
                        const ptrdiff_t xindex,
                        const ptrdiff_t yindex,
                        const ptrdiff_t zindex,
                        const double k2,
                        const int nxmodes){
            const double knorm = sqrt(k2);
            if ((k2 > 0) &&
                (this->fk0 <= knorm) &&
                (this->fk1 >= knorm))
                    *local_energy_in_shell.getMine() += nxmodes*(
                            vel->cval(cindex, 0, 0)*vel->cval(cindex, 0, 0) + vel->cval(cindex, 0, 1)*vel->cval(cindex, 0, 1) +
                            vel->cval(cindex, 1, 0)*vel->cval(cindex, 1, 0) + vel->cval(cindex, 1, 1)*vel->cval(cindex, 1, 1) +
                            vel->cval(cindex, 2, 0)*vel->cval(cindex, 2, 0) + vel->cval(cindex, 2, 1)*vel->cval(cindex, 2, 1)
                            );
        }
        );
        local_energy_in_shell.mergeParallel();
        MPI_Allreduce(
                local_energy_in_shell.getMasterData(),
                &energy_in_shell,
                1,
                MPI_DOUBLE,
                MPI_SUM,
                vel->comm);
        // we should divide by 2, if we wanted energy;
        // but then we would need to multiply the amplitude by 2 anyway,
        // because what we really care about is force dotted into velocity,
        // without the division by 2.

        // now, modify amplitudes
        if (energy_in_shell < 10*std::numeric_limits<rnumber>::epsilon())
            energy_in_shell = 1;
        double temp_famplitude = this->injection_rate / energy_in_shell;
        this->add_field_band(
                dst, vel,
                this->fk0, this->fk1,
                temp_famplitude);
        // and add drag if desired
        if (this->forcing_type == std::string("fixed_energy_injection_rate_and_drag"))
            this->add_field_band(
                    dst, vel,
                    this->fmode, this->fmode + (this->fk1 - this->fk0),
                    -this->friction_coefficient);
        return EXIT_SUCCESS;
    }
    if (this->forcing_type == std::string("fixed_energy"))
        return EXIT_SUCCESS;
    else
    {
        DEBUG_MSG("unknown forcing type printed on next line\n%s", this->forcing_type.c_str());
        throw std::invalid_argument("unknown forcing type");
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::impose_forcing(
        field<rnumber, FFTW, THREE> *dst)
{
    TIMEZONE("NSE_alt_dealias::impose_forcing");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::add_nonlinear_term(
        const field<rnumber, FFTW, THREE> *vel,
        field<rnumber, FFTW, THREE> *dst)
{
    TIMEZONE("NSE_alt_dealias::add_nonlinear_term");
    dst->real_space_representation = false;

    /* This computes the nonlinear term coming from vel, for the Navier Stokes
     * equations. Using vel=\f$u\f$ and dst=\f$a^{nl}\f$, we compute
     * \f[
     *  a^{nl}_i = - u_j \partial_j u_i
     * \f]
     * The result is given in Fourier representation.
     * */
    const int sign_array[2] = {1, -1};

    /* copy velocity */
    this->tmp_vec_field0->real_space_representation = false;
    *this->tmp_vec_field0 = *vel;

    //dealias
    this->kk->template dealias<rnumber, THREE>(this->tmp_vec_field0->get_cdata());
    // put velocity in real space
    this->tmp_vec_field0->ift();

    /* compute uu */
    /* store 11 22 33 components */
    this->tmp_vec_field1->real_space_representation = true;
    this->tmp_vec_field1->RLOOP(
            [&](const ptrdiff_t rindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
        for (int cc=0; cc<3; cc++)
            this->tmp_vec_field1->rval(rindex,cc) = (
                this->tmp_vec_field0->rval(rindex,cc) *
                this->tmp_vec_field0->rval(rindex,cc) ) / this->tmp_vec_field1->npoints;
        });
    /* take 11 22 33 components to Fourier representation */
    this->tmp_vec_field1->dft();

    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int ccc = 0; ccc < 2; ccc++)
            {
                dst->cval(cindex, 0, ccc) += (
                        sign_array[ccc]*this->kk->kx[xindex] * this->tmp_vec_field1->cval(cindex, 0, (ccc+1)%2));
                dst->cval(cindex, 1, ccc) += (
                        sign_array[ccc]*this->kk->ky[yindex] * this->tmp_vec_field1->cval(cindex, 1, (ccc+1)%2));
                dst->cval(cindex, 2, ccc) += (
                        sign_array[ccc]*this->kk->kz[zindex] * this->tmp_vec_field1->cval(cindex, 2, (ccc+1)%2));
            }
            });

    /* store 12 23 31 components */
    this->tmp_vec_field1->real_space_representation = true;
    this->tmp_vec_field1->RLOOP(
            [&](const ptrdiff_t rindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
        for (int cc=0; cc<3; cc++)
            this->tmp_vec_field1->rval(rindex,cc) = (
                this->tmp_vec_field0->rval(rindex,cc) *
                this->tmp_vec_field0->rval(rindex,(cc+1)%3) ) / this->tmp_vec_field1->npoints;
        });
    /* take 12 23 31 components to Fourier representation */
    this->tmp_vec_field1->dft();

    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int ccc = 0; ccc < 2; ccc++)
            {
                dst->cval(cindex, 0, ccc) += (
                        sign_array[ccc]*this->kk->ky[yindex] * this->tmp_vec_field1->cval(cindex, 0, (ccc+1)%2) +
                        sign_array[ccc]*this->kk->kz[zindex] * this->tmp_vec_field1->cval(cindex, 2, (ccc+1)%2));
                dst->cval(cindex, 1, ccc) += (
                        sign_array[ccc]*this->kk->kz[zindex] * this->tmp_vec_field1->cval(cindex, 1, (ccc+1)%2) +
                        sign_array[ccc]*this->kk->kx[xindex] * this->tmp_vec_field1->cval(cindex, 0, (ccc+1)%2));
                dst->cval(cindex, 2, ccc) += (
                        sign_array[ccc]*this->kk->kx[xindex] * this->tmp_vec_field1->cval(cindex, 2, (ccc+1)%2) +
                        sign_array[ccc]*this->kk->ky[yindex] * this->tmp_vec_field1->cval(cindex, 1, (ccc+1)%2));
            }
            });

    dst->symmetrize();
    /* done */

    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::Euler_step(void)
{
    // technically this is not a pure Euler method,
    // because I use an exponential factor for the diffusion term
    TIMEZONE("NSE_alt_dealias::Euler_step");

    // temporary field
    field<rnumber, FFTW, THREE> *acc = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);

    *acc = rnumber(0);

    // add nonlinear term
    this->add_nonlinear_term(
            this->velocity,
            acc);

    // add forcing
    this->add_forcing_term(this->velocity, acc);

    // perform Euler step
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex){
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) += this->dt*acc->cval(cindex, cc, ccc);
            }
            });

    // enforce incompressibility
    this->kk->template force_divfree<rnumber>(this->velocity->get_cdata());

    // apply diffusion exponential
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor = exp(-this->nu*k2 * this->dt);
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) *= factor;
            }
            });

    // enforce Hermitian symmetry
    this->velocity->symmetrize();

    // delete temporary field
    delete acc;
    return EXIT_SUCCESS;
}

/** \brief Time step with Shu Osher method
 *
 * The Navier Stokes equations are integrated with a
 * third-order Runge-Kutta method [shu1988jcp]_, which  is an explicit
 * Runge-Kutta method with the Butcher tableau
 *
 *  |   |   |   |   |
 *  |:--|:--|:--|:--|
 *  | 0 | | | |
 *  | 1 | 1 | | |
 *  | 1/2 | 1/4 | 1/4 | |
 *  | | 1/6 | 1/6 | 2/3 |
 *
 * In addition to the stability properties described in [shu1988jcp]_, this method has the advantage that it is memory-efficient, requiring only two additional field allocations, as can be seen from
 *     \f{eqnarray*}{
 *         \hat{\boldsymbol w}_1(\boldsymbol k) &= {\hat{\boldsymbol u}}(\boldsymbol k, t)e^{-\nu k^2 h} + h \textrm{NL}[\hat{\boldsymbol u}(\boldsymbol k, t)]e^{-\nu k^2 h}, \\
 *         \hat{\boldsymbol w}_2(\boldsymbol k) &= \frac{3}{4}\hat{\boldsymbol u}( \boldsymbol k, t)e^{-\nu k^2 h/2}
 *         +
 *         \frac{1}{4}(\hat{\boldsymbol w}_1(\boldsymbol k) + h \textrm{NL}[\hat{\boldsymbol w}_1(\boldsymbol k)])e^{\nu k^2 h/2}, \\
 *         \hat{\boldsymbol u}(\boldsymbol k, t+h) &= \frac{1}{3}\hat{\boldsymbol u}(\boldsymbol k, t)e^{-\nu k^2 h} +
 *         \frac{2}{3}(\hat{\boldsymbol w}_2(\boldsymbol k) + h \textrm{NL}[\hat{\boldsymbol w}_2(\boldsymbol k)])e^{-\nu k^2 h/2},
 *     \f}
 */
template <typename rnumber>
int NSE_alt_dealias<rnumber>::ShuOsher(void)
{
    *this->tmp_velocity1 = rnumber(0);
    this->add_nonlinear_term(this->velocity, this->tmp_velocity1);
    this->add_forcing_term(this->velocity, this->tmp_velocity1);
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor = exp(-this->nu*k2*this->dt);
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->tmp_velocity1->cval(cindex, cc, ccc) = factor*(
                        this->velocity->cval(cindex, cc, ccc) +
                        this->dt*this->tmp_velocity1->cval(cindex, cc, ccc));
            }});
    this->impose_forcing(this->tmp_velocity1);
    this->kk->template force_divfree<rnumber>(this->tmp_velocity1->get_cdata());

    *this->tmp_velocity2 = rnumber(0);
    this->add_nonlinear_term(this->tmp_velocity1, this->tmp_velocity2);
    this->add_forcing_term(this->tmp_velocity1, this->tmp_velocity2);
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor0 = exp(-0.5*this->nu*k2*this->dt)*0.75;
            const double factor1 = exp( 0.5*this->nu*k2*this->dt)*0.25;
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->tmp_velocity2->cval(cindex, cc, ccc) = (
                        this->velocity->cval(cindex, cc, ccc)*factor0 +
                        (this->tmp_velocity1->cval(cindex, cc, ccc) +
                         this->dt*this->tmp_velocity2->cval(cindex, cc, ccc))*factor1);
            }});
    this->impose_forcing(this->tmp_velocity2);
    this->kk->template force_divfree<rnumber>(this->tmp_velocity2->get_cdata());

    *this->tmp_velocity1 = rnumber(0);
    this->add_nonlinear_term(this->tmp_velocity2, this->tmp_velocity1);
    this->add_forcing_term(this->tmp_velocity2, this->tmp_velocity1);
    this->kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            const double factor0 =   exp(-    this->nu*k2*this->dt) / 3;
            const double factor1 = 2*exp(-0.5*this->nu*k2*this->dt) / 3;
            for (int cc = 0; cc < 3; cc++)
            for (int ccc = 0; ccc < 2; ccc++)
            {
                this->velocity->cval(cindex, cc, ccc) = (
                        this->velocity->cval(cindex, cc, ccc)*factor0 +
                        (this->tmp_velocity2->cval(cindex, cc, ccc) +
                         this->dt*this->tmp_velocity1->cval(cindex, cc, ccc))*factor1);
            }});
    this->impose_forcing(this->velocity);
    this->kk->template force_divfree<rnumber>(this->velocity->get_cdata());

    this->velocity->symmetrize();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::write_checkpoint(void)
{
    TIMEZONE("NSE_alt_dealias::write_checkpoint");
    this->update_checkpoint();

    assert(!this->velocity->real_space_representation);
    std::string fname = this->get_current_fname();
    this->velocity->io(
            fname,
            "velocity",
            this->iteration,
            false);

    this->write_iteration();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::finalize(void)
{
    TIMEZONE("NSE_alt_dealias::finalize");
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    delete this->kk;
    delete this->tmp_velocity1;
    delete this->tmp_velocity2;
    delete this->tmp_vec_field1;
    delete this->tmp_vec_field0;
    delete this->velocity;
    return EXIT_SUCCESS;
}

/** \brief Compute standard statistics for velocity and vorticity fields.
 *
 *  IMPORTANT: at the end of this subroutine, `this->fs->cvelocity` contains
 *  the Fourier space representation of the velocity field, and
 *  `this->tmp_vec_field` contains the real space representation of the
 *  velocity field.
 *  This behavior is relied upon in the `NSEparticles` class, so please
 *  don't break it.
 */

template <typename rnumber>
int NSE_alt_dealias<rnumber>::do_stats()
{
    TIMEZONE("NSE_alt_dealias::do_stats");
    if (!(this->iteration % this->niter_stat == 0))
        return EXIT_SUCCESS;
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "statistics",
                H5P_DEFAULT);
    else
        stat_group = 0;

    this->tmp_vec_field0->real_space_representation = false;
    *this->tmp_vec_field0 = *this->velocity;

    this->tmp_vec_field0->compute_stats(
            this->kk,
            stat_group,
            "velocity",
            this->iteration / niter_stat,
            this->max_velocity_estimate/sqrt(3));

    compute_curl(this->kk,
                 this->velocity,
                 this->tmp_vec_field0);

    this->tmp_vec_field0->compute_stats(
            this->kk,
            stat_group,
            "vorticity",
            this->iteration / niter_stat,
            this->max_vorticity_estimate/sqrt(3));

    if (this->myrank == 0)
        H5Gclose(stat_group);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSE_alt_dealias<rnumber>::read_parameters(void)
{
    TIMEZONE("NSE_alt_dealias::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->nu = hdf5_tools::read_value<double>(parameter_file, "parameters/nu");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "parameters/dt");
    this->fmode = hdf5_tools::read_value<int>(parameter_file, "parameters/fmode");
    this->famplitude = hdf5_tools::read_value<double>(parameter_file, "parameters/famplitude");
    this->friction_coefficient = hdf5_tools::read_value<double>(parameter_file, "parameters/friction_coefficient");
    this->injection_rate = hdf5_tools::read_value<double>(parameter_file, "parameters/injection_rate");
    this->fk0 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk0");
    this->fk1 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk1");
    this->energy = hdf5_tools::read_value<double>(parameter_file, "parameters/energy");
    this->histogram_bins = hdf5_tools::read_value<int>(parameter_file, "parameters/histogram_bins");
    this->max_velocity_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_velocity_estimate");
    this->max_vorticity_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_vorticity_estimate");
    this->forcing_type = hdf5_tools::read_string(parameter_file, "parameters/forcing_type");
    this->fftw_plan_rigor = hdf5_tools::read_string(parameter_file, "parameters/fftw_plan_rigor");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template class NSE_alt_dealias<float>;
template class NSE_alt_dealias<double>;

