/**********************************************************************
*                                                                     *
*  Copyright 2021 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#ifndef GET_VELOCITY_HPP
#define GET_VELOCITY_HPP

#include "full_code/NSVE_field_stats.hpp"

template <typename rnumber>
class get_velocity: public NSVE_field_stats<rnumber>
{
    public:
        int checkpoints_per_file;
        kspace<FFTW, SMOOTH> *kk;
        field<rnumber, FFTW, THREE> *vel;

        get_velocity(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~get_velocity(){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};

#endif//GET_VELOCITY_HPP

