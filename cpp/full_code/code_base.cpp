/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#include "code_base.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

code_base::code_base(
        const MPI_Comm COMMUNICATOR,
        const std::string &simulation_name):
    comm(COMMUNICATOR),
    simname(simulation_name)
{
    TIMEZONE("code_base::code_base");
    MPI_Comm_rank(this->comm, &this->myrank);
    MPI_Comm_size(this->comm, &this->nprocs);
    this->stop_code_now = false;
    this->iteration = 0;
}

int code_base::check_stopping_condition(void)
{
    TIMEZONE("code_base::check_stopping_condition");
    if (myrank == 0)
    {
        std::string fname = (
                std::string("stop_") +
                std::string(this->simname));
        {
            struct stat file_buffer;
            this->stop_code_now = (
                    stat(fname.c_str(), &file_buffer) == 0);
        }
    }
    MPI_Bcast(
            &this->stop_code_now,
            1,
            MPI_C_BOOL,
            0,
            this->comm);
    return EXIT_SUCCESS;
}

int code_base::read_parameters(void)
{
    TIMEZONE("code_base::read_parameters");
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->dkx = hdf5_tools::read_value<double>(parameter_file, "parameters/dkx");
    this->dky = hdf5_tools::read_value<double>(parameter_file, "parameters/dky");
    this->dkz = hdf5_tools::read_value<double>(parameter_file, "parameters/dkz");
    this->nx = hdf5_tools::read_value<int>(parameter_file, "parameters/nx");
    this->ny = hdf5_tools::read_value<int>(parameter_file, "parameters/ny");
    this->nz = hdf5_tools::read_value<int>(parameter_file, "parameters/nz");
    this->dealias_type = hdf5_tools::read_value<int>(parameter_file, "parameters/dealias_type");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

