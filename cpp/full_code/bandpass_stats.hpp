/**********************************************************************
*                                                                     *
*  Copyright 2017 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef BANDPASS_STATS_HPP
#define BANDPASS_STATS_HPP

#include "full_code/NSVE_field_stats.hpp"

/** \brief A class for running bandpass statistics.
 *
 *  This class computes statistics for a particular type of filter.
 *  Given a list of pairs of length-scales, the velocity field is filtered
 *  with a band-pass filter for each pair of length scales, and standard
 *  statistics of the band-passed field are computed.
 *  Relevant publication:
 *  Drivas, T. D. and Johnson, P. L. and Lalescu, C. C. and Wilczek, M. Phys Rev Fluids 2 104603 (2017)
 *  https://dx.doi.org/10.1103/PhysRevFluids.2.104603
 *  Not all statistics discussed in the paper are performed here, but the
 *  existing code can be expanded if needed.
 */

template <typename rnumber>
class bandpass_stats: public NSVE_field_stats<rnumber>
{
    public:
        int checkpoints_per_file;
        double max_velocity_estimate;
        kspace<FFTW, SMOOTH> *kk;
        /* parameters that are read in read_parameters */
        std::vector<double> k0list, k1list;
        std::string filter_type;

        bandpass_stats(
                const MPI_Comm COMMUNICATOR,
                const std::string &simulation_name):
            NSVE_field_stats<rnumber>(
                    COMMUNICATOR,
                    simulation_name){}
        virtual ~bandpass_stats(){}

        int initialize(void);
        int work_on_current_iteration(void);
        int finalize(void);
};

#endif//BANDPASS_STATS_HPP

