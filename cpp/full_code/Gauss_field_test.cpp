/**********************************************************************
*                                                                     *
*  Copyright 2019 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#include "Gauss_field_test.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>
#include <random>

template <typename rnumber>
int Gauss_field_test<rnumber>::initialize(void)
{
    TIMEZONE("Gauss_field_test::initialize");
    this->read_parameters();
    this->scalar_field = new field<rnumber, FFTW, ONE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);
    this->vector_field = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);
    this->kk = new kspace<FFTW, SMOOTH>(
            this->scalar_field->clayout, this->dkx, this->dky, this->dkz);

    if (this->myrank == 0)
    {
        hid_t stat_file = H5Fopen(
                (this->simname + std::string(".h5")).c_str(),
                H5F_ACC_RDWR,
                H5P_DEFAULT);
        this->kk->store(stat_file);
        H5Fclose(stat_file);
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int Gauss_field_test<rnumber>::finalize(void)
{
    TIMEZONE("Gauss_field_test::finalize");
    delete this->vector_field;
    delete this->scalar_field;
    delete this->kk;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int Gauss_field_test<rnumber>::read_parameters()
{
    TIMEZONE("Gauss_field_test::read_parameters");
    this->test::read_parameters();
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->max_velocity_estimate = hdf5_tools::read_value<double>(parameter_file, "/parameters/max_velocity_estimate");
    this->spectrum_dissipation = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_dissipation");
    this->spectrum_Lint = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_Lint");
    this->spectrum_etaK = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_etaK");
    this->spectrum_large_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_large_scale_const");
    this->spectrum_small_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_small_scale_const");
    this->random_seed = hdf5_tools::read_value<int>(parameter_file, "/parameters/field_random_seed");
    this->output_incompressible_field = hdf5_tools::read_value<int>(parameter_file, "/parameters/output_incompressible_field");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int Gauss_field_test<rnumber>::do_work(void)
{
    TIMEZONE("Gauss_field_test::do_work");

    /// initialize vector Gaussian random field
    make_gaussian_random_field(
        this->kk,
        this->vector_field,
        this->random_seed,
        this->spectrum_dissipation,
        this->spectrum_Lint,
        this->spectrum_etaK,
        this->spectrum_large_scale_const,
        this->spectrum_small_scale_const,
        3./2. //incompressibility projection factor
        );

    /// impose divergence free condition while maintaining the energy of the field
    this->kk->template project_divfree<rnumber>(this->vector_field->get_cdata());

    if (this->output_incompressible_field == 1)
    {
        this->vector_field->io(
                (std::string(this->simname) +
                 std::string("_checkpoint_") +
                 std::to_string(0) +
                 std::string(".h5")),
                "vorticity",
                0,
                false);
    }

    /// initialize statistics file.
    hid_t stat_group, stat_file;
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
        stat_group = H5Gopen(
                stat_file,
                "statistics",
                H5P_DEFAULT);
    }
    else
    {
        stat_file = 0;
        stat_group = 0;
    }

    /// compute gradient and divergence of field
    field<rnumber, FFTW, THREExTHREE> *tensor_field = new field<rnumber, FFTW, THREExTHREE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);
    compute_gradient(
            this->kk,
            this->vector_field,
            tensor_field);
    compute_divergence(
            this->kk,
            this->vector_field,
            this->scalar_field);


    /// compute integral of premultiplied energy spectrum
    this->kk->template cospectrum<rnumber, THREE>(
        this->vector_field->get_cdata(),
        stat_group,
        "k*velocity_k*velocity",
        0,
        2.);
    this->kk->template cospectrum<rnumber, THREE>(
        this->vector_field->get_cdata(),
        stat_group,
        "k2*velocity_k2*velocity",
        0,
        4.);

    /// compute basic statistics of Gaussian field
    this->vector_field->compute_stats(
            this->kk,
            stat_group,
            "velocity",
            0,
            this->max_velocity_estimate);

    /// verify divergence free method
    tensor_field->ift();
    /// for the stats max value doesn't really matter, I just want the moments
    tensor_field->compute_rspace_stats(
            stat_group,
            "velocity_gradient",
            0,
            std::vector<double>({1, 1, 1, 1, 1, 1, 1, 1, 1}));
    this->scalar_field->ift();
    this->scalar_field->compute_rspace_stats(
            stat_group,
            "velocity_divergence",
            0,
            std::vector<double>({1}));
    delete tensor_field;

    /// close stat file
    if (this->myrank == 0)
    {
        H5Gclose(stat_group);
        H5Fclose(stat_file);
    }
    return EXIT_SUCCESS;
}

template class Gauss_field_test<float>;
template class Gauss_field_test<double>;

