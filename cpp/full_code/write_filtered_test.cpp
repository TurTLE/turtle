/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/


#include "write_filtered_test.hpp"
#include "scope_timer.hpp"

#include <cmath>
#include <random>

template <typename rnumber>
int write_filtered_test<rnumber>::initialize(void)
{
    TIMEZONE("write_filtered_test::initialize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int write_filtered_test<rnumber>::finalize(void)
{
    TIMEZONE("write_filtered_test::finalize");
    this->read_parameters();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int write_filtered_test<rnumber>::read_parameters()
{
    TIMEZONE("write_filtered_test::read_parameters");
    this->test::read_parameters();
    // in case any parameters are needed, this is where they should be read
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int write_filtered_test<rnumber>::do_work(void)
{
    TIMEZONE("write_filtered_test::do_work");
    // allocate
    field<rnumber, FFTW, ONE> *scal_field = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);
    std::default_random_engine rgen;
    std::normal_distribution<rnumber> rdist;
    rgen.seed(2);

    // fill up scal_field
    scal_field->real_space_representation = true;
    scal_field->RLOOP(
            [&](ptrdiff_t rindex,
                ptrdiff_t xindex,
                ptrdiff_t yindex,
                ptrdiff_t zindex){
            scal_field->rval(rindex) = rdist(rgen);
            });
    scal_field->dft();

    scal_field->write_filtered(
            "data_for_write_filtered.h5",
            "scal_field_z0slice",
            this->iteration,
            this->nx,
            this->ny,
            1);

    scal_field->write_filtered(
            "data_for_write_filtered.h5",
            "scal_field_full",
            this->iteration,
            this->nx,
            this->ny,
            this->nz);

    scal_field->write_filtered(
            "data_for_write_filtered.h5",
            "scal_field_half",
            this->iteration,
            this->nx/2,
            this->ny/2,
            this->nz/2);

    // deallocate
    delete scal_field;

    // allocate
    field<rnumber, FFTW, THREE> *vec_field = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);

    // fill up vec_field
    vec_field->real_space_representation = true;
    vec_field->RLOOP(
            [&](ptrdiff_t rindex,
                ptrdiff_t xindex,
                ptrdiff_t yindex,
                ptrdiff_t zindex){
            vec_field->rval(rindex, 0) = rdist(rgen);
            vec_field->rval(rindex, 1) = rdist(rgen);
            vec_field->rval(rindex, 2) = rdist(rgen);
            });
    vec_field->dft();

    vec_field->write_filtered(
            "data_for_write_filtered.h5",
            "vec_field_z0slice",
            this->iteration,
            this->nx,
            this->ny,
            1);

    vec_field->write_filtered(
            "data_for_write_filtered.h5",
            "vec_field_full",
            this->iteration,
            this->nx,
            this->ny,
            this->nz);

    vec_field->write_filtered(
            "data_for_write_filtered.h5",
            "vec_field_half",
            this->iteration,
            this->nx/2,
            this->ny/2,
            this->nz/2);

    // deallocate
    delete vec_field;

    // allocate
    field<rnumber, FFTW, THREExTHREE> *tens_field = new field<rnumber, FFTW, THREExTHREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            FFTW_ESTIMATE);

    // fill up tens_field
    tens_field->real_space_representation = true;
    tens_field->RLOOP(
            [&](ptrdiff_t rindex,
                ptrdiff_t xindex,
                ptrdiff_t yindex,
                ptrdiff_t zindex){
            for (int cc = 0; cc<3; cc++)
            for (int ccc = 0; ccc<3; ccc++)
                tens_field->rval(rindex, cc, ccc) = rdist(rgen);
            });
    tens_field->dft();

    tens_field->write_filtered(
            "data_for_write_filtered.h5",
            "tens_field_z0slice",
            this->iteration,
            this->nx,
            this->ny,
            1);

    tens_field->write_filtered(
            "data_for_write_filtered.h5",
            "tens_field_full",
            this->iteration,
            this->nx,
            this->ny,
            this->nz);

    tens_field->write_filtered(
            "data_for_write_filtered.h5",
            "tens_field_half",
            this->iteration,
            this->nx/2,
            this->ny/2,
            this->nz/2);

    // deallocate
    delete tens_field;
    return EXIT_SUCCESS;
}

template class write_filtered_test<float>;
template class write_filtered_test<double>;

