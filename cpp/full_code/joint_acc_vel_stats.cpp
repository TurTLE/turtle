/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "joint_acc_vel_stats.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int joint_acc_vel_stats<rnumber>::initialize(void)
{
    TIMEZONE("joint_acc_vel_stats::initialize");
    this->NSVE_field_stats<rnumber>::initialize();
    this->kk = new kspace<FFTW, SMOOTH>(
            this->vorticity->clayout, this->dkx, this->dky, this->dkz);
    this->ve = new vorticity_equation<rnumber, FFTW>(
            this->simname.c_str(),
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            this->dkx,
            this->dky,
            this->dkz,
            this->vorticity->fftw_plan_rigor);
    this->template copy_parameters_into<rnumber, FFTW>(this->ve);
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    if (H5Lexists(parameter_file, "/parameters/checkpoints_per_file", H5P_DEFAULT))
    {
        this->checkpoints_per_file = hdf5_tools::read_value<int>(parameter_file, "/parameters/checkpoints_per_file");
    }
    else
        this->checkpoints_per_file = 1;
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/joint_acc_vel_stats/parameters/iteration_list");
    this->max_acceleration_estimate = hdf5_tools::read_value<int>(parameter_file, "joint_acc_vel_stats/parameters/max_acceleration_estimate");
    this->max_velocity_estimate = hdf5_tools::read_value<int>(parameter_file, "joint_acc_vel_stats/parameters/max_velocity_estimate");
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + "_post.h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    else
    {
        this->stat_file = 0;
    }
    int data_file_problem;
    if (this->myrank == 0)
        data_file_problem = hdf5_tools::require_size_file_datasets(
                this->stat_file,
                "joint_acc_vel_stats",
                (this->iteration_list.back() / this->niter_out) + 1);
    MPI_Bcast(&data_file_problem, 1, MPI_INT, 0, this->comm);
    if (data_file_problem > 0)
    {
        std::cerr <<
            data_file_problem <<
            " problems setting sizes of file datasets.\ntrying to exit now." <<
            std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int joint_acc_vel_stats<rnumber>::work_on_current_iteration(void)
{
    TIMEZONE("joint_acc_vel_stats::work_on_current_iteration");
    /// read current vorticity, place it in this->ve->cvorticity
    this->read_current_cvorticity();
    *this->ve->cvorticity = this->vorticity->get_cdata();
    /// after the previous instruction, we are free to use this->vorticity
    /// for any other purpose

    /// initialize `stat_group`.
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "joint_acc_vel_stats",
                H5P_DEFAULT);
    else
        stat_group = 0;

    field<rnumber, FFTW, THREE> *vel;
    field<rnumber, FFTW, THREE> *acc;

    /// compute velocity
    vel = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);
    invert_curl(kk, this->ve->cvorticity, vel);
    vel->ift();

    /// compute Lagrangian acceleration
    /// use this->vorticity as temporary field
    acc = this->vorticity;
    this->ve->compute_Lagrangian_acceleration(acc);
    acc->ift();

    /// compute single field real space statistics
    std::vector<double> max_acc_estimate;
    std::vector<double> max_vel_estimate;

    max_acc_estimate.resize(4, max_acceleration_estimate / std::sqrt(3));
    max_vel_estimate.resize(4, max_velocity_estimate / std::sqrt(3));
    max_acc_estimate[3] = max_acceleration_estimate;
    max_vel_estimate[3] = max_velocity_estimate;

    /// compute joint PDF
    joint_rspace_PDF(
            acc, vel,
            stat_group,
            "acceleration_and_velocity",
            this->iteration / this->niter_out,
            max_acc_estimate,
            max_vel_estimate);

    /// compute real space stats and spectra
    acc->compute_stats(
            kk,
            stat_group,
            "acceleration",
            this->iteration / this->niter_out,
            max_acceleration_estimate / std::sqrt(3));
    vel->compute_stats(
            kk,
            stat_group,
            "velocity",
            this->iteration / this->niter_out,
            max_velocity_estimate / std::sqrt(3));

    /// close stat group
    if (this->myrank == 0)
        H5Gclose(stat_group);

    delete vel;

    return EXIT_SUCCESS;
}

template <typename rnumber>
int joint_acc_vel_stats<rnumber>::finalize(void)
{
    TIMEZONE("joint_acc_vel_stats::finalize");
    delete this->ve;
    delete this->kk;
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    this->NSVE_field_stats<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template class joint_acc_vel_stats<float>;
template class joint_acc_vel_stats<double>;

