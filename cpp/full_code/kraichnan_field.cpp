/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "kraichnan_field.hpp"
#include "scope_timer.hpp"
#include "fftw_tools.hpp"


template <typename rnumber>
int kraichnan_field<rnumber>::initialize(void)
{
    TIMEZONE("kraichnan_file::initialize");
    this->read_iteration();
    this->read_parameters();
    if (this->myrank == 0)
    {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    this->grow_file_datasets();

    this->velocity = new field<rnumber, FFTW, THREE>(
            nx, ny, nz,
            this->comm,
	        fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    this->velocity->real_space_representation = true;

    // the velocity layout should be the same as the vorticity one
    this->kk = new kspace<FFTW, SMOOTH>(
            this->velocity->clayout, this->dkx, this->dky, this->dkz);

    // initialize particles
    this->ps = particles_system_builder(
                this->velocity,              // (field object)
                this->kk,                     // (kspace object, contains dkx, dky, dkz)
                tracers0_integration_steps, // to check coherency between parameters and hdf input file (nb rhs)
                (long long int)nparticles,  // to check coherency between parameters and hdf input file
	            this->get_current_fname(),    // particles input filename
                std::string("/tracers0/state/") + std::to_string(this->iteration), // dataset name for initial input
                std::string("/tracers0/rhs/")  + std::to_string(this->iteration),  // dataset name for initial input
                tracers0_neighbours,        // parameter (interpolation no neighbours)
                tracers0_smoothness,        // parameter
                this->comm,
                this->iteration+1);
    // initialize output objects
    this->particles_output_writer_mpi = new particles_output_hdf5<
        long long int, double, 3>(
                MPI_COMM_WORLD,
                "tracers0",
                nparticles,
                tracers0_integration_steps);
    this->particles_output_writer_mpi->setParticleFileLayout(this->ps->getParticleFileLayout());
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                MPI_COMM_WORLD,
                this->ps->getGlobalNbParticles(),
                (this->simname + "_particles.h5"),
                "tracers0",
                "position/0");
    this->particles_sample_writer_mpi->setParticleFileLayout(this->ps->getParticleFileLayout());

    this->generate_random_velocity();
    // is this the first iteration?
    if ((this->iteration == 0) and (this->output_velocity == 1))
    {
        // if yes, generate random field and save it
        this->velocity->io(
                this->get_current_fname(),
                "velocity",
                this->iteration,
                false);
    }

    return EXIT_SUCCESS;
}

template <typename rnumber>
int kraichnan_field<rnumber>::generate_random_velocity(void)
{
    TIMEZONE("kraichnan_file::make_random_field");
    // generate Gaussian random field
    make_gaussian_random_field(
        this->kk,
        this->velocity,
        this->field_random_seed + 10*this->iteration,
        this->spectrum_dissipation,
        this->spectrum_Lint,
        this->spectrum_etaK,
        this->spectrum_large_scale_const,
        this->spectrum_small_scale_const,
        3./2.); // incompressibility projection factor
    // not an ideal choice because resulting field sequence depends on MPI/OpenMP configuration. See note below
    // this->velocity is now in Fourier space
    // project divfree, requires field in Fourier space
    // Note on the choice of random seed:
    // If in the future the simulation will continue with a smaller number of total threads (number of processes times number of threads per process),
    // then during that run some of the threads will be seeded with a seed that has already been used for a previous iteration.
    // So some sequences of Fourier modes will be identical to sequences of Fourier modes that occured in the past.
    // Also see implementation of "make_gaussian_random_field".
    // One work-around would be to multiply "this->iteration" with 10 or so ---
    // it is unlikely the simulation will be continued with less than 0.1 of the initial total number of threads.
    DEBUG_MSG("L2Norm before: %g\n",
            this->velocity->L2norm(this->kk));
    this->kk->template project_divfree<rnumber>(this->velocity->get_cdata());
    DEBUG_MSG("L2Norm after: %g\n",
            this->velocity->L2norm(this->kk));
    // transform velocity to real space representation
    this->velocity->ift();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int kraichnan_field<rnumber>::step(void)
{
    TIMEZONE("kraichnan_file::step");
    this->ps->completeLoop(std::sqrt(this->dt));
    this->generate_random_velocity();
    this->iteration++;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int kraichnan_field<rnumber>::write_checkpoint(void)
{
    TIMEZONE("kraichnan_file::write_checkpoint");
    this->update_checkpoint();
    // output field
    if (this->output_velocity == 1)
    {
        assert(this->velocity->real_space_representation);
        std::string fname = this->get_current_fname();
        this->velocity->io(
                fname,
                "velocity",
                this->iteration,
                false);
    }
    // output particles
    this->particles_output_writer_mpi->open_file(this->get_current_fname());
    this->particles_output_writer_mpi->template save<3>(
            this->ps->getParticlesState(),
            this->ps->getParticlesRhs(),
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->iteration);
    this->particles_output_writer_mpi->close_file();
    this->write_iteration();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int kraichnan_field<rnumber>::finalize(void)
{
    TIMEZONE("kraichnan_field::finalize");
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    // good practice rule number n+1: always delete in inverse order of allocation
    delete this->ps.release();
    delete this->particles_output_writer_mpi;
    delete this->particles_sample_writer_mpi;
    delete this->kk;
    delete this->velocity;
    return EXIT_SUCCESS;
}

/** \brief Compute statistics.
 *
 */

template <typename rnumber>
int kraichnan_field<rnumber>::do_stats()
{
    TIMEZONE("kraichnan_field::do_stats");
    /// compute field statistics
    if (this->iteration % this->niter_stat == 0)
    {
        hid_t stat_group;
        if (this->myrank == 0)
            stat_group = H5Gopen(
                    this->stat_file,
                    "statistics",
                    H5P_DEFAULT);
        else
            stat_group = 0;
        field<rnumber, FFTW, THREE> *tvelocity = new field<rnumber, FFTW, THREE>(
                this->nx, this->ny, this->nz,
                this->comm,
	            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
        *tvelocity = *this->velocity;
        tvelocity->compute_stats(
            this->kk,
            stat_group,
            "velocity",
            this->iteration / this->niter_stat,
            this->max_velocity_estimate/std::sqrt(3));
        if (this->myrank == 0)
            H5Gclose(stat_group);
        delete tvelocity;
    }
    /// either one of two conditions suffices to compute statistics:
    /// 1) current iteration is a multiple of niter_part
    /// 2) we are within niter_part_fine_duration/2 of a multiple of niter_part_fine_period
    if (!(this->iteration % this->niter_part == 0 ||
          ((this->iteration + this->niter_part_fine_duration/2) % this->niter_part_fine_period <=
           this->niter_part_fine_duration)))
        return EXIT_SUCCESS;

    // allocate temporary data array
    std::unique_ptr<double[]> pdata(new double[3*this->ps->getLocalNbParticles()]);

    /// copy position data

    /// sample position
    std::copy(this->ps->getParticlesState(),
              this->ps->getParticlesState()+3*this->ps->getLocalNbParticles(),
              pdata.get());
    this->particles_sample_writer_mpi->template save_dataset<3>(
            "tracers0",
            "position",
            this->ps->getParticlesState(),
            &pdata,
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->ps->get_step_idx()-1);

    /// sample velocity
    std::fill_n(pdata.get(), 3*this->ps->getLocalNbParticles(), 0);
    this->ps->sample_compute_field(*this->velocity, pdata.get());
    this->particles_sample_writer_mpi->template save_dataset<3>(
            "tracers0",
            "velocity",
            this->ps->getParticlesState(),
            &pdata,
            this->ps->getParticlesIndexes(),
            this->ps->getLocalNbParticles(),
            this->ps->get_step_idx()-1);

    // deallocate temporary data array
    delete[] pdata.release();

    return EXIT_SUCCESS;
}

template <typename rnumber>
int kraichnan_field<rnumber>::read_parameters(void)
{
    TIMEZONE("kraichnan_field::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->fftw_plan_rigor = hdf5_tools::read_string(parameter_file, "parameters/fftw_plan_rigor");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "parameters/dt");
    this->niter_part = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part");
    this->niter_part_fine_period = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_period");
    this->niter_part_fine_duration = hdf5_tools::read_value<int>(parameter_file, "parameters/niter_part_fine_duration");
    this->nparticles = hdf5_tools::read_value<long long int>(parameter_file, "parameters/nparticles");
    this->tracers0_integration_steps = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_integration_steps");
    this->tracers0_neighbours = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_neighbours");
    this->tracers0_smoothness = hdf5_tools::read_value<int>(parameter_file, "parameters/tracers0_smoothness");
    this->spectrum_dissipation = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_dissipation");
    this->spectrum_Lint = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_Lint");
    this->spectrum_etaK = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_etaK");
    this->spectrum_large_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_large_scale_const");
    this->spectrum_small_scale_const = hdf5_tools::read_value<double>(parameter_file, "parameters/spectrum_small_scale_const");
    this->field_random_seed = hdf5_tools::read_value<int>(parameter_file, "parameters/field_random_seed");
    this->output_velocity = hdf5_tools::read_value<int>(parameter_file, "parameters/output_velocity");
    this->max_velocity_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_velocity_estimate");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <class rnumber>
void kraichnan_field<rnumber>::update_checkpoint()
{
    TIMEZONE("kraichnan_field::update_checkpoint");
    DEBUG_MSG("entered kraichan_field::update_checkpoint\n");
    std::string fname = this->get_current_fname();
    if (this->kk->layout->myrank == 0)
    {
        bool file_exists = false;
        {
            struct stat file_buffer;
            file_exists = (stat(fname.c_str(), &file_buffer) == 0);
        }
        if (file_exists)
        {
            // check how many fields there are in the checkpoint file
            // increment checkpoint if needed
            hsize_t fields_stored;
            hid_t fid, group_id;
            fid = H5Fopen(fname.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
            group_id = H5Gopen(fid, "tracers0/state", H5P_DEFAULT);
            bool dset_exists;
            if (group_id > 0)
            {
                H5Gget_num_objs(
                    group_id,
                    &fields_stored);
                dset_exists = H5Lexists(
                    group_id,
                    std::to_string(this->iteration).c_str(),
                    H5P_DEFAULT);
            }
            else
            {
                fields_stored = 0;
                dset_exists = false;
            }
            H5Gclose(group_id);
            H5Fclose(fid);
            if ((int(fields_stored) >= this->checkpoints_per_file) &&
                !dset_exists)
                this->checkpoint++;
        }
        else
        {
            // create file, create fields_stored dset
            hid_t fid = H5Fcreate(
                    fname.c_str(),
                    H5F_ACC_EXCL,
                    H5P_DEFAULT,
                    H5P_DEFAULT);
            hid_t gg = H5Gcreate(
                    fid,
                    "tracers0",
                    H5P_DEFAULT,
                    H5P_DEFAULT,
                    H5P_DEFAULT);
            hid_t ggg = H5Gcreate(
                    gg,
                    "state",
                    H5P_DEFAULT,
                    H5P_DEFAULT,
                    H5P_DEFAULT);
            H5Gclose(ggg);
            H5Gclose(gg);
            H5Fclose(fid);
        }
    }
    MPI_Bcast(&this->checkpoint, 1, MPI_INT, 0, this->kk->layout->comm);
    DEBUG_MSG("exiting kraichan_field::update_checkpoint\n");
}

template class kraichnan_field<float>;
template class kraichnan_field<double>;

