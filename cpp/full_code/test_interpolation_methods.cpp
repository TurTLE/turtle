/******************************************************************************
*                                                                             *
*  Copyright 2022 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#include "test_interpolation_methods.hpp"
#include "particles/particles_input_grid.hpp"

template <typename rnumber>
int test_interpolation_methods<rnumber>::initialize()
{
    TIMEZONE("test_interpolation_methods::initialize");
    this->read_parameters();

    this->phi = new field<rnumber, FFTW, ONE>(
            nx, ny, nz,
            this->comm,
            FFTW_ESTIMATE);

    this->kk = new kspace<FFTW, SMOOTH>(
            this->phi->clayout,
            this->dkx, this->dky, this->dkz);

    if (this->myrank == 0)
    {
        hid_t stat_file = H5Fopen(
                (this->simname + std::string(".h5")).c_str(),
                H5F_ACC_RDWR,
                H5P_DEFAULT);
        this->kk->store(stat_file);
        H5Fclose(stat_file);
    }

    return EXIT_SUCCESS;
}


template <typename rnumber>
template <int neighbours, int smoothness>
int test_interpolation_methods<rnumber>::interpolate()
{
    // create a particle object
    particle_set<3, neighbours, smoothness> pset(
            this->phi->rlayout,
            this->kk->dkx,
            this->kk->dky,
            this->kk->dkz);

    // initialize particles
    particles_input_grid<long long int, double, 3> pinput(
            this->comm,
            this->nxparticles,
            this->nzparticles,
            pset.getSpatialLowLimitZ(),
            pset.getSpatialUpLimitZ());

    pset.init(pinput);

    assert(this->phi->real_space_representation);

    pset.writeSample(
            this->phi,
            this->particles_sample_writer_mpi,
            "particle",
            "phi_" + std::to_string(neighbours) + std::to_string(smoothness),
            0);

    return EXIT_SUCCESS;
}


template <typename rnumber>
int test_interpolation_methods<rnumber>::do_work()
{
    TIMEZONE("test_interpolation_methods::do_work");

    // create field
    make_gaussian_random_field(
            this->kk,
            this->phi,
            this->random_seed,
            0.4,  // dissipation
            1.,   // Lint
            2*4*acos(0) / this->nx); // etaK

    // take field to real space
    this->phi->ift();

    // create a particle object
    particle_set<3, 1, 1> pset(
            this->phi->rlayout,
            this->kk->dkx,
            this->kk->dky,
            this->kk->dkz);
    particles_input_grid<long long int, double, 3> pinput(
            this->comm,
            this->nxparticles,
            this->nzparticles,
            pset.getSpatialLowLimitZ(),
            pset.getSpatialUpLimitZ());

    pset.init(pinput);

    // initialize writer
    this->particles_sample_writer_mpi = new particles_output_sampling_hdf5<
        long long int, double, double, 3>(
                this->comm,
                pset.getTotalNumberOfParticles(),
                (this->simname + "_particles.h5"),
                "particle",
                "position/0");
    this->particles_sample_writer_mpi->setParticleFileLayout(pset.getParticleFileLayout());

    // sample position
    pset.writeStateTriplet(
            0,
            this->particles_sample_writer_mpi,
            "particle",
            "position",
            0);

    // sample field
    this->template interpolate<0, 0>();
    this->template interpolate<1, 1>();
    this->template interpolate<2, 1>();
    this->template interpolate<2, 2>();
    this->template interpolate<3, 1>();
    this->template interpolate<3, 2>();
    this->template interpolate<4, 1>();
    this->template interpolate<4, 2>();
    this->template interpolate<5, 1>();
    this->template interpolate<5, 2>();

    delete this->particles_sample_writer_mpi;

    return EXIT_SUCCESS;
}


template <typename rnumber>
int test_interpolation_methods<rnumber>::finalize()
{
    TIMEZONE("test_interpolation_methods::finalize");
    delete this->kk;
    delete this->phi;
    return EXIT_SUCCESS;
}


template <typename rnumber>
int test_interpolation_methods<rnumber>::read_parameters()
{
    TIMEZONE("test_interpolation_methods::read_parameters");
    this->test::read_parameters();

    hid_t parameter_file = H5Fopen(
            (this->simname + std::string(".h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->nxparticles = hdf5_tools::read_value<int>(parameter_file, "/parameters/nxparticles");
    this->nzparticles = hdf5_tools::read_value<int>(parameter_file, "/parameters/nzparticles");
    this->random_seed = hdf5_tools::read_value<int>(parameter_file, "/parameters/field_random_seed");
    H5Fclose(parameter_file);
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}


template class test_interpolation_methods<float>;
template class test_interpolation_methods<double>;

