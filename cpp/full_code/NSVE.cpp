/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "NSVE.hpp"
#include "scope_timer.hpp"
#include "fftw_tools.hpp"
#include "hdf5_tools.hpp"

/** \brief Allocates fields and kspace.
 */
template <typename rnumber>
int NSVE<rnumber>::allocate(void)
{
    this->fs = new vorticity_equation<rnumber, FFTW>(
            simname.c_str(),
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            this->dkx,
            this->dky,
            this->dkz,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);
    this->tmp_vec_field = new field<rnumber, FFTW, THREE>(
            this->nx, this->ny, this->nz,
            this->comm,
            fftw_planner_string_to_flag[this->fftw_plan_rigor]);


    this->fs->checkpoints_per_file = checkpoints_per_file;
    this->fs->nu = nu;
    this->fs->fmode = fmode;
    this->fs->famplitude = famplitude;
    this->fs->friction_coefficient = friction_coefficient;
    this->fs->variation_strength = variation_strength;
    this->fs->variation_time_scale = variation_time_scale;
    this->fs->energy = energy;
    this->fs->injection_rate = injection_rate;
    this->fs->fk0 = fk0;
    this->fs->fk1 = fk1;
    this->fs->dt = dt;
    this->fs->forcing_type = forcing_type;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE<rnumber>::initialize(void)
{
    TIMEZONE("NSVE::initialize");
    this->read_iteration();
    this->read_parameters();
    if (this->myrank == 0) {
        // set caching parameters
        hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
        herr_t cache_err = H5Pset_cache(fapl, 0, 521, 134217728, 1.0);
        variable_used_only_in_assert(cache_err);
        DEBUG_MSG("when setting stat_file cache I got %d\n", cache_err);
        this->stat_file = H5Fopen(
                (this->simname + ".h5").c_str(),
                H5F_ACC_RDWR,
                fapl);
    }
    this->grow_file_datasets();

    this->allocate();

    this->fs->iteration = this->iteration;
    this->fs->checkpoint = this->checkpoint;

    this->fs->cvorticity->real_space_representation = false;
    if (this->iteration == 0) {
        this->fs->kk->store(stat_file);
    }
    if (this->iteration == 0) {
        if (!hdf5_tools::field_exists(
                    this->fs->get_current_fname(),
                    "vorticity",
                    "complex",
                    0)) {
            // generate initial condition
            make_gaussian_random_field(
                this->fs->kk,
                this->fs->cvelocity,
                this->field_random_seed,
                this->fs->injection_rate,
                1.0,                        // Lint
                1.5 / this->fs->kk->kM,     // etaK
                6.78,
                0.40,
                3./2.);
            this->fs->kk->template project_divfree<rnumber>(
                    this->fs->cvelocity->get_cdata());
            compute_curl(this->fs->kk, this->fs->cvelocity, this->fs->cvorticity);
            this->fs->kk->template low_pass<rnumber, THREE>(
                    this->fs->cvorticity->get_cdata(), this->fs->kk->kM);
            this->fs->cvorticity->symmetrize();
            this->fs->io_checkpoint(false);
        } else {
            this->fs->io_checkpoint();
        }
    } else {
        this->fs->io_checkpoint();
    }
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE<rnumber>::step(void)
{
    TIMEZONE("NSVE::step");
    this->fs->step(this->dt);
    //this->fs->Euler_step(this->dt);
    this->iteration = this->fs->iteration;
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE<rnumber>::write_checkpoint(void)
{
    TIMEZONE("NSVE::write_checkpoint");
    this->fs->io_checkpoint(false);
    this->checkpoint = this->fs->checkpoint;
    this->write_iteration();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE<rnumber>::finalize(void)
{
    TIMEZONE("NSVE::finalize");
    if (this->myrank == 0)
        H5Fclose(this->stat_file);
    this->deallocate();
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE<rnumber>::deallocate(void)
{
    delete this->fs;
    delete this->tmp_vec_field;
    return EXIT_SUCCESS;
}

/** \brief Compute standard statistics for velocity and vorticity fields.
 *
 *  IMPORTANT: at the end of this subroutine, `this->fs->cvelocity` contains
 *  the Fourier space representation of the velocity field, and
 *  `this->tmp_vec_field` contains the real space representation of the
 *  velocity field.
 *  This behavior is relied upon in the `NSVEparticles` class, so please
 *  don't break it.
 */

template <typename rnumber>
int NSVE<rnumber>::do_stats()
{
    TIMEZONE("NSVE::do_stats");
    if (!(this->iteration % this->niter_stat == 0))
        return EXIT_SUCCESS;
    hid_t stat_group;
    if (this->myrank == 0)
        stat_group = H5Gopen(
                this->stat_file,
                "statistics",
                H5P_DEFAULT);
    else
        stat_group = 0;

    *tmp_vec_field = fs->cvorticity->get_cdata();
    tmp_vec_field->compute_stats(
            fs->kk,
            stat_group,
            "vorticity",
            fs->iteration / niter_stat,
            max_vorticity_estimate/std::sqrt(3));

    fs->compute_velocity(fs->cvorticity);
    *tmp_vec_field = fs->cvelocity->get_cdata();
    tmp_vec_field->compute_stats(
            fs->kk,
            stat_group,
            "velocity",
            fs->iteration / niter_stat,
            max_velocity_estimate/std::sqrt(3));

    double CFL_velocity = tmp_vec_field->compute_CFL_velocity();
    if (this->myrank == 0)
        hdf5_tools::update_time_series_with_single_rank<double>(
                stat_group,
                "CFL_velocity",
                fs->iteration / niter_stat,
                1,
                &CFL_velocity);


    if (this->myrank == 0)
        H5Gclose(stat_group);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE<rnumber>::read_parameters(void)
{
    TIMEZONE("NSVE::read_parameters");
    this->direct_numerical_simulation::read_parameters();
    hid_t parameter_file = H5Fopen((this->simname + ".h5").c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    this->nu = hdf5_tools::read_value<double>(parameter_file, "parameters/nu");
    this->dt = hdf5_tools::read_value<double>(parameter_file, "parameters/dt");
    this->fmode = hdf5_tools::read_value<int>(parameter_file, "parameters/fmode");
    this->field_random_seed = hdf5_tools::read_value<int>(parameter_file, "parameters/field_random_seed");
    this->famplitude = hdf5_tools::read_value<double>(parameter_file, "parameters/famplitude");
    this->friction_coefficient = hdf5_tools::read_value<double>(parameter_file, "parameters/friction_coefficient");
    this->variation_strength = hdf5_tools::read_value<double>(parameter_file, "parameters/variation_strength");
    this->variation_time_scale = hdf5_tools::read_value<double>(parameter_file, "parameters/variation_time_scale");
    this->injection_rate = hdf5_tools::read_value<double>(parameter_file, "parameters/injection_rate");
    this->fk0 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk0");
    this->fk1 = hdf5_tools::read_value<double>(parameter_file, "parameters/fk1");
    this->energy = hdf5_tools::read_value<double>(parameter_file, "parameters/energy");
    this->histogram_bins = hdf5_tools::read_value<int>(parameter_file, "parameters/histogram_bins");
    this->max_velocity_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_velocity_estimate");
    this->max_vorticity_estimate = hdf5_tools::read_value<double>(parameter_file, "parameters/max_vorticity_estimate");
    this->forcing_type = hdf5_tools::read_string(parameter_file, "parameters/forcing_type");
    this->fftw_plan_rigor = hdf5_tools::read_string(parameter_file, "parameters/fftw_plan_rigor");
    H5Fclose(parameter_file);
    // the following ensures the file is free after exiting this method
    MPI_Barrier(this->comm);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int NSVE<rnumber>::print_debug_info(void)
{
    DEBUG_MSG("start NSVE::print_debug_info\n");
    DEBUG_MSG("nu = %g\n", this->fs->nu);
    DEBUG_MSG("dt = %g\n", this->fs->dt);
    DEBUG_MSG("fmode = %d\n", this->fs->fmode);
    DEBUG_MSG("famplitude = %g\n", this->fs->famplitude);
    DEBUG_MSG("friction_coefficient = %g\n", this->fs->friction_coefficient);
    DEBUG_MSG("injection_rate = %g\n", this->fs->injection_rate);
    DEBUG_MSG("fk0 = %g\n", this->fs->fk0);
    DEBUG_MSG("fk1 = %g\n", this->fs->fk1);
    DEBUG_MSG("energy = %g\n", this->fs->energy);
    DEBUG_MSG("forcing_type = \"%s\"\n", this->fs->forcing_type.c_str());
    DEBUG_MSG("variation_strength = %g\n", this->fs->variation_strength);
    DEBUG_MSG("variation_time_scale = %g\n", this->fs->variation_time_scale);
    DEBUG_MSG("end NSVE::print_debug_info\n");
    return EXIT_SUCCESS;
}

template class NSVE<float>;
template class NSVE<double>;

