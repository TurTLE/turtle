#include "write_rpressure.hpp"
#include "scope_timer.hpp"
#include "hdf5_tools.hpp"

#include <cmath>

template <typename rnumber>
int write_rpressure<rnumber>::initialize(void)
{

    // initialize
    this->NSVE_field_stats<rnumber>::initialize();

    //iteration list needed by postprocess.cpp for the main loop
    hid_t parameter_file = H5Fopen(
            (this->simname + std::string("_post.h5")).c_str(),
            H5F_ACC_RDONLY,
            H5P_DEFAULT);
    this->iteration_list = hdf5_tools::read_vector<int>(
            parameter_file,
            "/write_rpressure/parameters/iteration_list");
    H5Fclose(parameter_file);
    // the following ensures the file is free for rank 0 to open in read/write mode
    MPI_Barrier(this->comm);
    if (this->myrank==0) DEBUG_MSG("Iteration list[0] = %d \n", this->iteration_list[0]);

    //get the pressure from here
    this->ve = new vorticity_equation<rnumber, FFTW>(
            this->simname.c_str(),
            this->nx,
            this->ny,
            this->nz,
            this->comm,
            this->dkx,
            this->dky,
            this->dkz,
            this->vorticity->fftw_plan_rigor);

    // output field
    this->pressure = new field<rnumber, FFTW, ONE>(
            this->nx, this->ny, this->nz,
            this->comm,
            this->vorticity->fftw_plan_rigor);

    // write to binary
    this->bin_IO_scalar = new field_binary_IO<rnumber, REAL, ONE>(
                this->pressure->rlayout->sizes,
                this->pressure->rlayout->subsizes,
                this->pressure->rlayout->starts,
                this->pressure->rlayout->comm);

    if (this->myrank==0) DEBUG_MSG("Initialize end\n");
    return EXIT_SUCCESS;
}

template <typename rnumber>
int write_rpressure<rnumber>::clip_zero_padding(
    field <rnumber, FFTW, ONE>* f)
{
    rnumber *a = f->get_rdata();
    rnumber *b = f->get_rdata();
    ptrdiff_t copy_size = f->rlayout->sizes[2] * ncomp(ONE);
    ptrdiff_t skip_size = copy_size + 2*ncomp(ONE);
    for (int i0 = 0; i0 < int(f->rlayout->subsizes[0]); i0++)
        for (int i1 = 0; i1 < int(f->rlayout->sizes[1]); i1++)
        {
            std::copy(a, a + copy_size, b);
            a += skip_size;
            b += copy_size;
        }
    return EXIT_SUCCESS;
}


template <typename rnumber>
int write_rpressure<rnumber>::work_on_current_iteration(void)
{
    if (this->myrank==0) DEBUG_MSG("\nComputing the pressure\n");
    //compute_pressure
    this->read_current_cvorticity();
    *this->ve->cvorticity = this->vorticity->get_cdata();

    this->ve->compute_velocity(this->ve->cvorticity);
    this->ve->cvelocity->ift();
    this->ve->compute_pressure(this->pressure);


    if (this->myrank==0) DEBUG_MSG("Computed the pressure\n");

    if (this->myrank==0) DEBUG_MSG("vorticity in ve fourier = [%lG, %lG; ...; %lG, %lG;  %lG, %lG;.....; %lG, %lG;] \n", this->vorticity->get_cdata()[0][0], this->vorticity->get_cdata()[0][1], this->vorticity->get_cdata()[1][0], this->vorticity->get_cdata()[1][1], this->vorticity->get_cdata()[2][0], this->vorticity->get_cdata()[2][1],this->vorticity->get_cdata()[65][0], this->vorticity->get_cdata()[65][1]);

    if (this->myrank==0) DEBUG_MSG("vorticity in ve fourier = [%lG, %lG; ...; %lG, %lG;  %lG, %lG;.....; %lG, %lG;] \n", this->ve->cvorticity->get_cdata()[0][0], this->ve->cvorticity->get_cdata()[0][1], this->ve->cvorticity->get_cdata()[1][0], this->ve->cvorticity->get_cdata()[1][1], this->ve->cvorticity->get_cdata()[2][0], this->ve->cvorticity->get_cdata()[2][1],this->ve->cvorticity->get_cdata()[65][0], this->ve->cvorticity->get_cdata()[65][1]);
    this->ve->compute_velocity(this->vorticity);

    if (this->myrank==0) DEBUG_MSG("velocity in ve fourier = [%lG, %lG; ...; %lG, %lG;  %lG, %lG;.....; %lG, %lG;] \n", this->ve->u->get_cdata()[0][0], this->ve->u->get_cdata()[0][1], this->ve->u->get_cdata()[1][0], this->ve->u->get_cdata()[1][1], this->ve->u->get_cdata()[2][0], this->ve->u->get_cdata()[2][1],this->ve->u->get_cdata()[65][0], this->ve->u->get_cdata()[65][1]);

    if (this->myrank==0) DEBUG_MSG("pressure fourier = [%lG, %lG; ...; %lG, %lG;  %lG, %lG;.....; %lG, %lG;] \n", this->pressure->get_cdata()[0][0], this->pressure->get_cdata()[0][1], this->pressure->get_cdata()[1][0], this->pressure->get_cdata()[1][1], this->pressure->get_cdata()[2][0], this->pressure->get_cdata()[2][1],this->pressure->get_cdata()[65][0], this->pressure->get_cdata()[65][1]);

    //transform to real space
    if (this->pressure->real_space_representation == false)
        this->pressure->ift();

    if (this->myrank==0) DEBUG_MSG("Real space\n");

    if (this->myrank==0) DEBUG_MSG("pressure before clipping = [%lG; ...; %lG;.....; %lG;] \n", this->pressure->get_rdata()[0], this->pressure->get_rdata()[2], this->pressure->get_rdata()[13549]);

    //write out to file
    this->clip_zero_padding(pressure);
    if (this->myrank==0) DEBUG_MSG("pressure after clipping = [%lG; ...; %lG;.....; %lG;] \n", this->pressure->get_rdata()[0], this->pressure->get_rdata()[2], this->pressure->get_rdata()[13549]);

    //write to file -- THIS IS OK
    char itername[16];
    sprintf(itername, "i%.5x", this->iteration);
    std::string native_binary_fname;

    native_binary_fname = (
            this->simname +
            std::string("_rpressure_") +
            std::string(itername));
    if (this->myrank ==0) DEBUG_MSG("%s\n", native_binary_fname.c_str());

    this->bin_IO_scalar->write(
            native_binary_fname,
            this->pressure->get_rdata());
    return EXIT_SUCCESS;
}

template <typename rnumber>
int write_rpressure<rnumber>::finalize(void)
{

    delete this->bin_IO_scalar;
    delete this->pressure;
    delete this->ve;
    this->NSVE_field_stats<rnumber>::finalize();
    return EXIT_SUCCESS;
}

template class write_rpressure<float>;
template class write_rpressure<double>;

