/**********************************************************************
*                                                                     *
*  Copyright 2015 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/

#include "base.hpp"
#include "fftw_tools.hpp"
#include "fftw_interface.hpp"

std::map<std::string, unsigned> fftw_planner_string_to_flag = {
    {"FFTW_ESTIMATE", FFTW_ESTIMATE},
    {"FFTW_MEASURE", FFTW_MEASURE},
    {"FFTW_PATIENT", FFTW_PATIENT},
    {"parameter does not exist", DEFAULT_FFTW_FLAG},
};

