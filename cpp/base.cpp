#include "base.hpp"

#include <iostream>

int maximum_MPI_tag_value;

int read_maximum_MPI_tag_value()
{
//  see https://www.intel.com/content/www/us/en/developer/articles/technical/large-mpi-tags-with-the-intel-mpi.html
    void *max_tag;
    int flag;
    MPI_Comm_get_attr(
            MPI_COMM_WORLD,
            MPI_TAG_UB,
            &max_tag,
            &flag);
    if (flag){
        maximum_MPI_tag_value = *((int*)(max_tag));
        if (myrank == 0)
            DEBUG_MSG("Maximum MPI tag value queried from rank 0 is %d\n",
                      maximum_MPI_tag_value);
    }
    else{
        std::cerr << "TurTLE error: couldn't get maximum MPI tag value." << std::endl;
        assert(false);
    }
    return EXIT_SUCCESS;
}

/////////////////////////////////////////////////////////////

