/**********************************************************************
*                                                                     *
*  Copyright 2015 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#include "fftw_tools.hpp"
#include "scope_timer.hpp"
#include "shared_array.hpp"
#include "field_binary_IO.hpp"

#include "field_io.cpp"

template <typename rnumber,
          field_backend be,
          field_components fc>
field<rnumber, be, fc>::field(
                const int nx,
                const int ny,
                const int nz,
                const MPI_Comm COMM_TO_USE,
                const unsigned FFTW_PLAN_RIGOR)
{
    TIMEZONE("field::field");
    this->comm = COMM_TO_USE;
    MPI_Comm_rank(this->comm, &this->myrank);
    MPI_Comm_size(this->comm, &this->nprocs);

    this->fftw_plan_rigor = FFTW_PLAN_RIGOR;
    this->real_space_representation = true;

    /* generate HDF5 data types */
    if (typeid(rnumber) == typeid(float))
        this->rnumber_H5T = H5Tcopy(H5T_NATIVE_FLOAT);
    else if (typeid(rnumber) == typeid(double))
        this->rnumber_H5T = H5Tcopy(H5T_NATIVE_DOUBLE);
    typedef struct {
        rnumber re;   /*real part*/
        rnumber im;   /*imaginary part*/
    } tmp_complex_type;
    this->cnumber_H5T = H5Tcreate(H5T_COMPOUND, sizeof(tmp_complex_type));
    H5Tinsert(this->cnumber_H5T, "r", HOFFSET(tmp_complex_type, re), this->rnumber_H5T);
    H5Tinsert(this->cnumber_H5T, "i", HOFFSET(tmp_complex_type, im), this->rnumber_H5T);

    /* switch on backend */
    switch(be)
    {
        case FFTW:
            ptrdiff_t nfftw[3];
            nfftw[0] = nz;
            nfftw[1] = ny;
            nfftw[2] = nx;
            hsize_t tmp_local_size;
            ptrdiff_t local_n0, local_0_start;
            ptrdiff_t local_n1, local_1_start;
            variable_used_only_in_assert(tmp_local_size);
            tmp_local_size = fftw_interface<rnumber>::mpi_local_size_many_transposed(
                    3, nfftw, ncomp(fc),
                    FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK, this->comm,
                    &local_n0, &local_0_start,
                    &local_n1, &local_1_start);
            hsize_t sizes[3], subsizes[3], starts[3];
            sizes[0] = nz; sizes[1] = ny; sizes[2] = nx;
            subsizes[0] = local_n0; subsizes[1] = ny; subsizes[2] = nx;
            starts[0] = local_0_start; starts[1] = 0; starts[2] = 0;
            this->rlayout = new field_layout<fc>(
                    sizes, subsizes, starts, this->comm);
            assert(tmp_local_size == this->rlayout->local_size);
            this->npoints = this->rlayout->full_size / ncomp(fc);
            sizes[0] = nz; sizes[1] = ny; sizes[2] = nx+2;
            subsizes[0] = local_n0; subsizes[1] = ny; subsizes[2] = nx+2;
            starts[0] = local_0_start; starts[1] = 0; starts[2] = 0;
            this->rmemlayout = new field_layout<fc>(
                    sizes, subsizes, starts, this->comm);
            sizes[0] = ny; sizes[1] = nz; sizes[2] = nx/2+1;
            subsizes[0] = local_n1; subsizes[1] = nz; subsizes[2] = nx/2+1;
            starts[0] = local_1_start; starts[1] = 0; starts[2] = 0;
            this->clayout = new field_layout<fc>(
                    sizes, subsizes, starts, this->comm);
            this->data = fftw_interface<rnumber>::alloc_real(
                    this->rmemlayout->local_size);
            //char *plan_information = NULL;
            this->c2r_plan = fftw_interface<rnumber>::mpi_plan_many_dft_c2r(
                    3, nfftw, ncomp(fc),
                    FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
                    (typename fftw_interface<rnumber>::complex*)this->data,
                    this->data,
                    this->comm,
                    this->fftw_plan_rigor | FFTW_MPI_TRANSPOSED_IN);
            //plan_information = fftw_interface<rnumber>::sprint(this->c2r_plan);
            //DEBUG_MSG("field::field c2r plan representation is\n\%s\n", plan_information);
            //free(plan_information);
            this->r2c_plan = fftw_interface<rnumber>::mpi_plan_many_dft_r2c(
                    3, nfftw, ncomp(fc),
                    FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
                    this->data,
                    (typename fftw_interface<rnumber>::complex*)this->data,
                    this->comm,
                    this->fftw_plan_rigor | FFTW_MPI_TRANSPOSED_OUT);
            //plan_information = fftw_interface<rnumber>::sprint(this->r2c_plan);
            //DEBUG_MSG("field::field r2c plan representation is\n\%s\n", plan_information);
            //free(plan_information);
            break;
    }
    // use Fourier representation for setting field to 0,
    // since this actually touches all data
    this->real_space_representation = false;
    this->operator=(rnumber(0));
    this->real_space_representation = true;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
field<rnumber, be, fc>::~field() noexcept(false)
{
    /* close data types */
    H5Tclose(this->rnumber_H5T);
    H5Tclose(this->cnumber_H5T);
    switch(be)
    {
        case FFTW:
            delete this->rlayout;
            delete this->rmemlayout;
            delete this->clayout;
            fftw_interface<rnumber>::free(this->data);
            fftw_interface<rnumber>::destroy_plan(this->c2r_plan);
            fftw_interface<rnumber>::destroy_plan(this->r2c_plan);
            break;
    }
}

template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::ift()
{
    TIMEZONE("field::ift");
    start_mpi_profiling_zone(turtle_mpi_pcontrol::FFTW);
    fftw_interface<rnumber>::execute(this->c2r_plan);
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::FFTW);
    this->real_space_representation = true;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::dft()
{
    TIMEZONE("field::dft");
    start_mpi_profiling_zone(turtle_mpi_pcontrol::FFTW);
    fftw_interface<rnumber>::execute(this->r2c_plan);
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::FFTW);
    this->real_space_representation = false;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::compute_rspace_xincrement_stats(
                const int xcells,
                const hid_t group,
                const std::string dset_name,
                const hsize_t toffset,
                const std::vector<double> max_estimate,
                field<rnumber, be, fc> *tmp_field)
{
    TIMEZONE("field::compute_rspace_xincrement_stats");
    assert(this->real_space_representation);
    assert(fc == ONE || fc == THREE);
    bool own_field = false;
    own_field = (tmp_field == NULL);
    if (own_field)
        tmp_field = new field<rnumber, be, fc>(
                this->rlayout->sizes[2],
                this->rlayout->sizes[1],
                this->rlayout->sizes[0],
                this->rlayout->comm);
    tmp_field->real_space_representation = true;
    this->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
            const hsize_t rrindex = (xindex + xcells)%this->rlayout->sizes[2] + (
                zindex * this->rlayout->subsizes[1] + yindex)*(
                    this->rmemlayout->subsizes[2]);
            for (unsigned int component=0; component < ncomp(fc); component++)
                tmp_field->data[rindex*ncomp(fc) + component] =
                    this->data[rrindex*ncomp(fc) + component] -
                    this->data[rindex*ncomp(fc) + component];
                    });
    tmp_field->compute_rspace_stats(
            group,
            dset_name,
            toffset,
            max_estimate);
    if (own_field)
        delete tmp_field;
}



/** \brief Compute CFL velocity
 *
 * For the CFL condition we need a specific "maximum" norm of the velocity
 * field.
 * \f[
 *  \| u \|_{CFL} \equiv \max_{\mathbf{x}}\left(\sum_{i=1,2,3}\big(|u_i(\mathbf{x})|\big)\right)
 * \f]
 */
template <typename rnumber,
          field_backend be,
          field_components fc>
double field<rnumber, be, fc>::compute_CFL_velocity()
{
    TIMEZONE("field::compute_CFL_velocity");
    assert(this->real_space_representation);
    assert(fc == THREE);
    double CFL_velocity = 0;
    shared_array<double> local_CFL_velocity(
            1,
            shared_array_zero_initializer<double, 1>);
    this->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
                double temp_CFL_velocity = (
                        std::abs(this->rval(rindex, 0))
                      + std::abs(this->rval(rindex, 1))
                      + std::abs(this->rval(rindex, 2)));
                if (*local_CFL_velocity.getMine() < temp_CFL_velocity)
                    *local_CFL_velocity.getMine() = temp_CFL_velocity;
                    });
    local_CFL_velocity.mergeParallel(
            [&](const int idx,
                const double& v1,
                const double& v2) -> double {
            assert(idx == 0);
            return std::max(v1, v2);
        });
    MPI_Allreduce(
            local_CFL_velocity.getMasterData(),
            &CFL_velocity,
            1,
            MPI_DOUBLE,
            MPI_MAX,
            this->comm);
    return CFL_velocity;
}



/** \brief Compute statistics (real-space representation)
 *
 * \note Moments from 1 through 8 are computed.
 * Moment array will have 10 elements:
 * - index 0 contains minimum value
 * - indices 1 through 8 contain moments 1 through 8
 * - index 9 contains maximum value
 */
template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::compute_rspace_stats(
                const hid_t group,
                const std::string dset_name,
                const hsize_t toffset,
                const std::vector<double> min_estimate,
                const std::vector<double> max_estimate,
                const unsigned int maximum_moment)
{
    TIMEZONE("field::compute_rspace_stats");
    assert(this->real_space_representation);
    const unsigned int nmoments = 10;
    assert(maximum_moment < nmoments);
    int nvals, nbins;
    if (this->myrank == 0)
    {
        hid_t dset, wspace;
        hsize_t dims[ndim(fc)-1];
        int ndims;
        dset = H5Dopen(group, ("moments/" + dset_name).c_str(), H5P_DEFAULT);
        if(dset < 0)
        {
            DEBUG_MSG("couldn't open dataset 'moments/%s'\n", dset_name.c_str());
        }
        wspace = H5Dget_space(dset);
        ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
        // the following -1 comes from -3+2
        // field data has 3 space array indices that are averaged over,
        // and possible component indices that are NOT averaged over
        // HDF5 dataset has 2 extra indices for time and order of moment,
        // and possible component indices
        //DEBUG_MSG("ndims = %d, ndim(fc) = %d, dsetname = %s\n",
        //        ndims, ndim(fc),
        //        dset_name.c_str());
        assert(ndims == int(ndim(fc))-1);
        assert(dims[1] == nmoments);
        // gcc complains about the following code, when ndim(fc)-1 is too small
        // since I already assert what the value of ndims is, I know the code
        // is safe, so I just tell GCC to ignore the apparent problem with this
        // pragma. See link below for more info:
        // https://gcc.gnu.org/onlinedocs/gcc/Diagnostic-Pragmas.html
#pragma GCC diagnostic ignored "-Warray-bounds"
#pragma GCC diagnostic push
        switch(ndims)
        {
            case 2:
                nvals = 1;
                break;
            case 3:
                nvals = dims[2];
                break;
            case 4:
                nvals = dims[2]*dims[3];
                break;
        }
#pragma GCC diagnostic pop
        H5Sclose(wspace);
        H5Dclose(dset);
        dset = H5Dopen(group, ("histograms/" + dset_name).c_str(), H5P_DEFAULT);
        wspace = H5Dget_space(dset);
        ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
        assert(ndims == int(ndim(fc))-1);
        nbins = dims[1];
        if (ndims == 3)
            assert(nvals == int(dims[2]));
        else if (ndims == 4)
            assert(nvals == int(dims[2]*dims[3]));
        H5Sclose(wspace);
        H5Dclose(dset);
    }
    {
        MPI_Bcast(&nvals, 1, MPI_INT, 0, this->comm);
        MPI_Bcast(&nbins, 1, MPI_INT, 0, this->comm);
    }
    assert(nvals == int(min_estimate.size()));
    assert(nvals == int(max_estimate.size()));
    for(int i=0; i<nvals; i++)
        assert(max_estimate[i] > min_estimate[i]);

    start_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
    shared_array<double> local_moments_threaded(
            nmoments*nvals,
            [&](double* local_moments){
                std::fill_n(local_moments, nmoments*nvals, 0);
                for (int i=0; i<nvals; i++)
                {
                    // see https://en.cppreference.com/w/cpp/types/numeric_limits
                    local_moments[0*nvals+i] = std::numeric_limits<double>::max();
                    local_moments[(nmoments-1)*nvals+i] = std::numeric_limits<double>::lowest();
                }
                if (nvals == 4) {
                    local_moments[0*nvals+3] = std::numeric_limits<double>::max();
                    local_moments[(nmoments-1)*nvals+3] = std::numeric_limits<double>::min();
                }
            });

    shared_array<double> val_tmp_threaded(
            nvals,
            [&](double *val_tmp){
                std::fill_n(val_tmp, nvals, 0);
            });

    shared_array<ptrdiff_t> local_hist_threaded(
            nbins*nvals,
            [&](ptrdiff_t* local_hist){
                std::fill_n(local_hist, nbins*nvals, 0);
            });

    shared_array<double> local_pow_tmp(nvals);

    double *binsize = new double[nvals];
    for (int i=0; i<nvals; i++)
        binsize[i] = (max_estimate[i] - min_estimate[i]) / nbins;

    {
        {
        TIMEZONE("field::RLOOP");
        this->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
            double* pow_tmp = local_pow_tmp.getMine();
            std::fill_n(pow_tmp, nvals, 1);

            double *local_moments = local_moments_threaded.getMine();
            double *val_tmp = val_tmp_threaded.getMine();
            ptrdiff_t *local_hist = local_hist_threaded.getMine();

            if (nvals == int(4)) val_tmp[3] = 0.0;
            for (unsigned int i=0; i<ncomp(fc); i++)
            {
                val_tmp[i] = this->data[rindex*ncomp(fc)+i];
                if (nvals == int(4)) val_tmp[3] += val_tmp[i]*val_tmp[i];
            }
            if (nvals == int(4))
            {
                val_tmp[3] = std::sqrt(val_tmp[3]);
            }
            for (int i=0; i<nvals; i++)
            {
                if (val_tmp[i] < local_moments[0*nvals+i])
                    local_moments[0*nvals+i] = val_tmp[i];
                if (val_tmp[i] > local_moments[(nmoments-1)*nvals+i])
                    local_moments[(nmoments-1)*nvals+i] = val_tmp[i];
                const int bin = int(std::floor((val_tmp[i] - min_estimate[i]) / binsize[i]));
                if (bin >= 0 && bin < nbins)
                    local_hist[bin*nvals+i]++;
            }
            for (unsigned int n=1; n < maximum_moment; n++){
                for (int i=0; i<nvals; i++){
                    local_moments[n*nvals + i] += (pow_tmp[i] = val_tmp[i]*pow_tmp[i]);
                }
            }
                });
#ifdef USE_TIMING_OUTPUT
        MPI_Barrier(this->comm);
#endif
        }

        {
        TIMEZONE("FIELD_RLOOP::Merge");
        local_moments_threaded.mergeParallel([&](const int idx, const double& v1, const double& v2) -> double {
            if(idx < int(nvals)){
                return std::min(v1, v2);
            }
            if(int(nmoments-1)*nvals <= idx){
                return std::max(v1, v2);
            }
            return v1 + v2;
        });

        local_hist_threaded.mergeParallel();
#ifdef USE_TIMING_OUTPUT
        MPI_Barrier(this->comm);
#endif
        }
    }
    ptrdiff_t *hist = new ptrdiff_t[nbins*nvals];
    double *moments = new double[nmoments*nvals];
    {
        TIMEZONE("MPI_Allreduce");
        MPI_Allreduce(
                (void*)local_moments_threaded.getMasterData(),
                (void*)moments,
                nvals,
                MPI_DOUBLE, MPI_MIN, this->comm);
        MPI_Allreduce(
                (void*)(local_moments_threaded.getMasterData() + nvals),
                (void*)(moments+nvals),
                (nmoments-2)*nvals,
                MPI_DOUBLE, MPI_SUM, this->comm);
        MPI_Allreduce(
                (void*)(local_moments_threaded.getMasterData() + (nmoments-1)*nvals),
                (void*)(moments+(nmoments-1)*nvals),
                nvals,
                MPI_DOUBLE, MPI_MAX, this->comm);
        MPI_Allreduce(
                (void*)local_hist_threaded.getMasterData(),
                (void*)hist,
                nbins*nvals,
                MPI_INT64_T, MPI_SUM, this->comm);
    }
    for (int n=1; n < int(nmoments)-1; n++)
        for (int i=0; i<nvals; i++)
            moments[n*nvals + i] /= this->npoints;

    delete[] binsize;
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
    if (this->myrank == 0)
    {
        TIMEZONE("root-work");
        hid_t dset, wspace, mspace;
        hsize_t count[ndim(fc)-1], offset[ndim(fc)-1], dims[ndim(fc)-1];
        dset = H5Dopen(group, ("moments/" + dset_name).c_str(), H5P_DEFAULT);
        assert(dset>0);
        wspace = H5Dget_space(dset);
        H5Sget_simple_extent_dims(wspace, dims, NULL);
        offset[0] = toffset;
        offset[1] = 0;
        count[0] = 1;
        count[1] = nmoments;
        if (fc == THREE)
        {
            offset[2] = 0;
            count[2] = nvals;
        }
        if (fc == THREExTHREE)
        {
            offset[2] = 0;
            count[2] = 3;
            offset[3] = 0;
            count[3] = 3;
        }
        mspace = H5Screate_simple(ndim(fc)-1, count, NULL);
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_DOUBLE, mspace, wspace, H5P_DEFAULT, moments);
        H5Sclose(wspace);
        H5Sclose(mspace);
        H5Dclose(dset);
        dset = H5Dopen(group, ("histograms/" + dset_name).c_str(), H5P_DEFAULT);
        assert(dset > 0);
        wspace = H5Dget_space(dset);
        count[1] = nbins;
        mspace = H5Screate_simple(ndim(fc)-1, count, NULL);
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_INT64, mspace, wspace, H5P_DEFAULT, hist);
        H5Sclose(wspace);
        H5Sclose(mspace);
        H5Dclose(dset);
    }
    delete[] moments;
    delete[] hist;
}



template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::compute_rspace_zaverage(
                const hid_t group,
                const std::string dset_name,
                const hsize_t toffset)
{
    TIMEZONE("field::compute_rspace_zaverage");
    assert(this->real_space_representation);
    const hsize_t slice_size = this->rlayout->local_size / this->rlayout->subsizes[0];

    // initial arrays MUST be 0, because I'm just adding to them afterwards.
    shared_array<double> local_zaverage_threaded(
            slice_size,
            [&](double* local_zaverage){
                std::fill_n(local_zaverage, slice_size, 0);
            });

    // sum along z direction
    {
        TIMEZONE("field::RLOOP");
        this->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){

            double *local_zaverage = local_zaverage_threaded.getMine();
            ptrdiff_t zaverage_index = (yindex*this->rlayout->subsizes[2]+xindex)*ncomp(fc);

            switch(fc)
            {
                case ONE:
                    local_zaverage[zaverage_index] += this->rval(rindex);
                    break;
                case THREE:
                    local_zaverage[zaverage_index+0] += this->rval(rindex, 0);
                    local_zaverage[zaverage_index+1] += this->rval(rindex, 1);
                    local_zaverage[zaverage_index+2] += this->rval(rindex, 2);
                    break;
                case THREExTHREE:
                    local_zaverage[zaverage_index+0 + 0] += this->rval(rindex, 0, 0);
                    local_zaverage[zaverage_index+0 + 1] += this->rval(rindex, 0, 1);
                    local_zaverage[zaverage_index+0 + 2] += this->rval(rindex, 0, 2);
                    local_zaverage[zaverage_index+3 + 0] += this->rval(rindex, 1, 0);
                    local_zaverage[zaverage_index+3 + 1] += this->rval(rindex, 1, 1);
                    local_zaverage[zaverage_index+3 + 2] += this->rval(rindex, 1, 2);
                    local_zaverage[zaverage_index+6 + 0] += this->rval(rindex, 2, 0);
                    local_zaverage[zaverage_index+6 + 1] += this->rval(rindex, 2, 1);
                    local_zaverage[zaverage_index+6 + 2] += this->rval(rindex, 2, 2);
                    break;
            }
                });

        TIMEZONE("FIELD_RLOOP::Merge");
        local_zaverage_threaded.mergeParallel();
    }
    // sum along MPI processes
    double *zaverage = new double[slice_size];
    {
        TIMEZONE("MPI_Allreduce");
        MPI_Allreduce(
                (void*)local_zaverage_threaded.getMasterData(),
                (void*)zaverage,
                slice_size,
                MPI_DOUBLE, MPI_SUM, this->comm);
    }
    // divide by total number of slices
    for (ptrdiff_t n=0; n < ptrdiff_t(slice_size); n++)
            zaverage[n] /= this->rlayout->sizes[0];

    if (this->myrank == 0)
    {
        TIMEZONE("root-work");
        hid_t dset, wspace, mspace;
        int ndims;
        hsize_t count[5], offset[5], dims[5];
        offset[0] = toffset;
        offset[1] = 0;
        offset[2] = 0;
        offset[3] = 0;
        offset[4] = 0;
        dset = H5Dopen(
                group,
                ("zaverage/" + dset_name).c_str(),
                H5P_DEFAULT);
        wspace = H5Dget_space(dset);
        ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
        count[0] = 1;
        count[1] = this->rlayout->sizes[1];
        count[2] = this->rlayout->sizes[2];
        count[3] = 3;
        count[4] = 3;
        // select right slice in file
        H5Sselect_hyperslab(
            wspace,
            H5S_SELECT_SET,
            offset,
            NULL,
            count,
            NULL);
        offset[0] = 0;
        // select proper regions of memory
        mspace = H5Screate_simple(ndims-1, count+1, NULL);
        H5Sselect_hyperslab(
            mspace,
            H5S_SELECT_SET,
            offset+1,
            NULL,
            count+1,
            NULL);
        H5Dwrite(
            dset,
            H5T_NATIVE_DOUBLE,
            mspace,
            wspace,
            H5P_DEFAULT,
            zaverage);
        H5Dclose(dset);
        H5Sclose(mspace);
        H5Sclose(wspace);
    }
    delete[] zaverage;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::Hermitian_reflect()
{
    TIMEZONE("field::Hermitian_reflect");
    start_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
    assert(!this->real_space_representation);
    typename fftw_interface<rnumber>::complex *cdata = this->get_cdata();
    // reflect kx = 0 plane, line by line, for ky != 0
    MPI_Status *mpistatus = new MPI_Status;
    // bufferp will hold data from "plus", i.e. iy
    // bufferm will hold data from "minus", i.e. ny - iy
    typename fftw_interface<rnumber>::complex *bufferp = new typename fftw_interface<rnumber>::complex[ncomp(fc)*this->clayout->sizes[1]];
    typename fftw_interface<rnumber>::complex *bufferm = new typename fftw_interface<rnumber>::complex[ncomp(fc)*this->clayout->sizes[1]];
    int rankp, rankm;
    // for each ky slice except ky=0
    for (ptrdiff_t iy = 1; iy < ptrdiff_t(this->clayout->sizes[0]/2); iy++)
    {
        // read rank plus and rank minus
        rankp = this->clayout->rank[0][iy];
        rankm = this->clayout->rank[0][this->clayout->sizes[0] - iy];
        // if my rank is rank plus, I should fill out the plus buffer
        if (this->clayout->myrank == rankp)
        {
            ptrdiff_t iyy = iy - this->clayout->starts[0];
            for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->sizes[1]); iz++)
            {
                ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                for (int cc = 0; cc < int(ncomp(fc)); cc++)
                    for (int imag_comp=0; imag_comp<2; imag_comp++)
                        (*(bufferp + ncomp(fc)*iz+cc))[imag_comp] =
                            (*(cdata + ncomp(fc)*cindex + cc))[imag_comp];
            }
        }
        // if my rank is rank minus, I should fill out the minus buffer
        if (this->clayout->myrank == rankm)
        {
            ptrdiff_t iyy = (this->clayout->sizes[0] - iy) - this->clayout->starts[0];
            for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->sizes[1]); iz++)
            {
                ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                for (int cc = 0; cc < int(ncomp(fc)); cc++)
                    for (int imag_comp=0; imag_comp<2; imag_comp++)
                        (*(bufferm + ncomp(fc)*iz+cc))[imag_comp] =
                            (*(cdata + ncomp(fc)*cindex + cc))[imag_comp];
            }
        }
        // if ranks are different, send and receive
        if (rankp != rankm)
        {
            // if my rank is rank plus, send buffer plus, receive buffer minus
            if (this->clayout->myrank == rankp)
            {
                MPI_Send((void*)bufferp,
                         ncomp(fc)*this->clayout->sizes[1],
                         mpi_real_type<rnumber>::complex(),
                         rankm, 2*iy+0,
                         this->clayout->comm);
                MPI_Recv((void*)bufferm,
                         ncomp(fc)*this->clayout->sizes[1],
                         mpi_real_type<rnumber>::complex(),
                         rankm, 2*iy+1,
                         this->clayout->comm,
                         mpistatus);
            }
            // if my rank is rank minus, receive buffer plus, send buffer minus
            if (this->clayout->myrank == rankm)
            {
                MPI_Recv((void*)bufferp,
                         ncomp(fc)*this->clayout->sizes[1],
                         mpi_real_type<rnumber>::complex(),
                         rankp, 2*iy+0,
                         this->clayout->comm,
                         mpistatus);
                MPI_Send((void*)bufferm,
                         ncomp(fc)*this->clayout->sizes[1],
                         mpi_real_type<rnumber>::complex(),
                         rankp, 2*iy+1,
                         this->clayout->comm);
            }
        }
        // if I my rank is either plus or minus, I should update my slice
        if (this->clayout->myrank == rankp)
        {
            ptrdiff_t iyy = iy - this->clayout->starts[0];
            for (ptrdiff_t iz = 1; iz < ptrdiff_t(this->clayout->sizes[1]); iz++)
            {
                ptrdiff_t izz = (this->clayout->sizes[1] - iz);
                ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                for (int cc = 0; cc < int(ncomp(fc)); cc++)
                {
                    (*(cdata + ncomp(fc)*cindex + cc))[0] =   (*(bufferm + ncomp(fc)*izz+cc))[0];
                    (*(cdata + ncomp(fc)*cindex + cc))[1] =  -(*(bufferm + ncomp(fc)*izz+cc))[1];
                }
            }
            ptrdiff_t cindex = this->get_cindex(0, iyy, 0);
            for (int cc = 0; cc < int(ncomp(fc)); cc++)
            {
                (*(cdata + cc + ncomp(fc)*cindex))[0] =   (*(bufferm + cc))[0];
                (*(cdata + cc + ncomp(fc)*cindex))[1] =  -(*(bufferm + cc))[1];
            }
        }
        // if I my rank is either plus or minus, I should update my slice
        if (this->clayout->myrank == rankm)
        {
            ptrdiff_t iyy = (this->clayout->sizes[0] - iy) - this->clayout->starts[0];
            for (ptrdiff_t iz = 1; iz < ptrdiff_t(this->clayout->sizes[1]); iz++)
            {
                ptrdiff_t izz = (this->clayout->sizes[1] - iz);
                ptrdiff_t cindex = this->get_cindex(0, iyy, izz);
                for (int cc = 0; cc < int(ncomp(fc)); cc++)
                {
                    (*(cdata + ncomp(fc)*cindex + cc))[0] =  (*(bufferp + ncomp(fc)*iz+cc))[0];
                    (*(cdata + ncomp(fc)*cindex + cc))[1] = -(*(bufferp + ncomp(fc)*iz+cc))[1];
                }
            }
            ptrdiff_t cindex = this->get_cindex(0, iyy, 0);
            for (int cc = 0; cc < int(ncomp(fc)); cc++)
            {
                (*(cdata + cc + ncomp(fc)*cindex))[0] =  (*(bufferp + cc))[0];
                (*(cdata + cc + ncomp(fc)*cindex))[1] = -(*(bufferp + cc))[1];
            }
        }
    }
    //fftw_interface<rnumber>::free(buffer);
    delete[] bufferp;
    delete[] bufferm;
    delete mpistatus;
    // reflect kx = 0, ky = 0 line
    if (this->clayout->myrank == this->clayout->rank[0][0])
    {
        for (ptrdiff_t iz = 1; iz < ptrdiff_t(this->clayout->sizes[1]/2); iz++)
        {
            ptrdiff_t cindex0 = this->get_cindex(0, 0, iz);
            ptrdiff_t cindex1 = this->get_cindex(0, 0, this->clayout->sizes[1] - iz);
            for (int cc = 0; cc < int(ncomp(fc)); cc++)
            {
                double rep = (*(cdata + cc + ncomp(fc)*cindex0))[0];
                double imp = (*(cdata + cc + ncomp(fc)*cindex0))[1];
                double rem = (*(cdata + cc + ncomp(fc)*cindex1))[0];
                double imm = (*(cdata + cc + ncomp(fc)*cindex1))[1];
                (*(cdata + cc + ncomp(fc)*cindex0))[0] =  rem;
                (*(cdata + cc + ncomp(fc)*cindex0))[1] = -imm;
                (*(cdata + cc + ncomp(fc)*cindex1))[0] =  rep;
                (*(cdata + cc + ncomp(fc)*cindex1))[1] = -imp;
            }
        }
    }
    // make 0 mode real
    if (this->myrank == this->clayout->rank[0][0])
    {
        for (ptrdiff_t cc = 0; cc < ncomp(fc); cc++)
            cdata[cc][1] = 0.0;
    }
    // put kx = nx/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
    {
        ptrdiff_t cindex = this->get_cindex(this->clayout->sizes[2]-1, iy, iz);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }
    // put ky = ny/2 modes to 0
    if (this->clayout->myrank == this->clayout->rank[0][this->clayout->sizes[0]/2])
    {
        for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
        for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
        {
            ptrdiff_t cindex = this->get_cindex(ix, this->clayout->sizes[0]/2-this->clayout->starts[0], iz);
            for (int cc = 0; cc < int(ncomp(fc)); cc++) {
                (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
                (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
            }
        }
    }
    // put kz = nz/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
    {
        ptrdiff_t cindex = this->get_cindex(ix, iy, this->clayout->sizes[1]/2);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
}


/** \brief Enforce Hermitian symmetry (slow, reference)
 *
 * TurTLE uses real-to-complex and complex-to-real FFTW transforms, because the
 * equations are PDEs of real-valued fields.
 * Hermitian symmetry means that Fourier-transformed real valued fields must
 * respect the equation \f$\hat f(-\mathbf{k}) = {\hat f}^*(\mathbf{k})\f$.
 *
 * FFTW enforces this property mainly by only storing the positive half of the
 * Fourier grid for the fastest array component. In TurTLE's case, this means
 * \f$ k_x \f$.
 * For the \f$ k_x = 0 \f$ plane, the symmetry must be enforced.
 *
 * This method uses a pair of backwards-forwards FFTs, which leads to FFTW
 * effectively imposing Hermitian symmetry. It should be used as a reference
 * implementation to calibrate against in the general case.
 *
 * */
template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::symmetrize_FFT()
{
    TIMEZONE("field::symmetrize_FFT");
    assert(!this->real_space_representation);
    typename fftw_interface<rnumber>::complex *cdata = this->get_cdata();

    // make 0 mode real
    if (this->myrank == this->clayout->rank[0][0])
    {
        for (ptrdiff_t cc = 0; cc < ncomp(fc); cc++)
            cdata[cc][1] = 0.0;
    }
    // put kx = nx/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
    {
        ptrdiff_t cindex = this->get_cindex(this->clayout->sizes[2]-1, iy, iz);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }
    // put ky = ny/2 modes to 0
    if (this->clayout->myrank == this->clayout->rank[0][this->clayout->sizes[0]/2])
    {
        for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
        for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
        {
            ptrdiff_t cindex = this->get_cindex(ix, this->clayout->sizes[0]/2-this->clayout->starts[0], iz);
            for (int cc = 0; cc < int(ncomp(fc)); cc++) {
                (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
                (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
            }
        }
    }
    // put kz = nz/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
    {
        ptrdiff_t cindex = this->get_cindex(ix, iy, this->clayout->sizes[1]/2);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }

    this->ift();
    this->dft();
    this->normalize();
    return;
}

/** \brief Enforce Hermitian symmetry (unoptimized, amplitude-aware)
 *
 * TurTLE uses real-to-complex and complex-to-real FFTW transforms, because the
 * equations are PDEs of real-valued fields.
 * Hermitian symmetry means that Fourier-transformed real valued fields must
 * respect the equation \f$\hat f(-\mathbf{k}) = {\hat f}^*(\mathbf{k})\f$.
 *
 * FFTW enforces this property mainly by only storing the positive half of the
 * Fourier grid for the fastest array component. In TurTLE's case, this means
 * \f$ k_x \f$.
 * For the \f$ k_x = 0 \f$ plane, the symmetry must be enforced.
 *
 * This method is an alternative to the default arithmetic mean method, meant
 * to be used in special circumstances where it is important to retain the
 * exact amplitude of modes.
 * Rather than an arithmetic mean, here we compute the amplitudes and phases
 * for the \f$(0, k_y, k_z)\f$ and \f$(0, -k_y, -k_z)\f$ modes. We then compute
 * a mean amplitude as the square root of the product of the two amplitudes,
 * and a mean phase as the arithmetic mean of the two phases.
 * When this method is applied to a field with fixed amplitudes, but random
 * phases, it should preserve the spectrum of the initial field exactly.
 *
 * */
template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::symmetrize_alternate()
{
    TIMEZONE("field::symmetrize");
    MPI_Barrier(this->clayout->comm); // TODO: figure out if this can be taken out by careful MPI tag generation
    start_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
    assert(!this->real_space_representation);
    typename fftw_interface<rnumber>::complex *cdata = this->get_cdata();

    // make 0 mode real
    if (this->myrank == this->clayout->rank[0][0])
    {
        for (ptrdiff_t cc = 0; cc < ncomp(fc); cc++)
            cdata[cc][1] = 0.0;
    }
    // put kx = nx/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
    {
        ptrdiff_t cindex = this->get_cindex(this->clayout->sizes[2]-1, iy, iz);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }
    // put ky = ny/2 modes to 0
    if (this->clayout->myrank == this->clayout->rank[0][this->clayout->sizes[0]/2])
    {
        for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
        for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
        {
            ptrdiff_t cindex = this->get_cindex(ix, this->clayout->sizes[0]/2-this->clayout->starts[0], iz);
            for (int cc = 0; cc < int(ncomp(fc)); cc++) {
                (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
                (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
            }
        }
    }
    // put kz = nz/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
    {
        ptrdiff_t cindex = this->get_cindex(ix, iy, this->clayout->sizes[1]/2);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }

    // symmetrize kx = 0 plane, line by line, for ky != 0
    MPI_Status *mpistatus = new MPI_Status;
    // bufferp will hold data from "plus", i.e. iy
    // bufferm will hold data from "minus", i.e. ny - iy
    typename fftw_interface<rnumber>::complex *bufferp = new typename fftw_interface<rnumber>::complex[ncomp(fc)*this->clayout->sizes[1]];
    typename fftw_interface<rnumber>::complex *bufferm = new typename fftw_interface<rnumber>::complex[ncomp(fc)*this->clayout->sizes[1]];
    int rankp, rankm;
    // for each ky slice except ky=0
    for (ptrdiff_t iy = 1; iy < ptrdiff_t(this->clayout->sizes[0]/2); iy++)
    {
        // read rank plus and rank minus
        rankp = this->clayout->rank[0][iy];
        rankm = this->clayout->rank[0][this->clayout->sizes[0] - iy];
        // if my rank is plus or minus, then I should do actual work
        if (this->clayout->myrank == rankp ||
            this->clayout->myrank == rankm)
        {
            #pragma omp parallel
        {
            const ptrdiff_t zstart = ptrdiff_t(OmpUtils::ForIntervalStart(this->clayout->sizes[1]));
            const ptrdiff_t zend   = ptrdiff_t(OmpUtils::ForIntervalEnd(this->clayout->sizes[1]));
            // if my rank is rank plus, I should fill out the plus buffer
            if (this->clayout->myrank == rankp)
            {
                ptrdiff_t iyy = iy - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                        for (int imag_comp=0; imag_comp<2; imag_comp++)
                            (*(bufferp + ncomp(fc)*iz+cc))[imag_comp] =
                                (*(cdata + ncomp(fc)*cindex + cc))[imag_comp];
                }
            }
            // if my rank is rank minus, I should fill out the minus buffer
            if (this->clayout->myrank == rankm)
            {
                ptrdiff_t iyy = (this->clayout->sizes[0] - iy) - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                        for (int imag_comp=0; imag_comp<2; imag_comp++)
                            (*(bufferm + ncomp(fc)*iz+cc))[imag_comp] =
                                (*(cdata + ncomp(fc)*cindex + cc))[imag_comp];
                }
            }
            // after filling out buffers, synchronize threads
            #pragma omp barrier

            // if ranks are different, send and receive
            if (rankp != rankm)
            {
                // if my rank is rank plus, send buffer plus, receive buffer minus
                if (this->clayout->myrank == rankp && omp_get_thread_num() == 0)
                {
                    MPI_Send((void*)bufferp,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankm, 2*iy+0,
                             this->clayout->comm);
                    MPI_Recv((void*)bufferm,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankm, 2*iy+1,
                             this->clayout->comm,
                             mpistatus);
                }
                // if my rank is rank minus, receive buffer plus, send buffer minus
                if (this->clayout->myrank == rankm && omp_get_thread_num() == 0)
                {
                    MPI_Recv((void*)bufferp,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankp, 2*iy+0,
                             this->clayout->comm,
                             mpistatus);
                    MPI_Send((void*)bufferm,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankp, 2*iy+1,
                             this->clayout->comm);
                }
            }
            // ensure buffers are updated by MPI transfer before threads
            // start working again
            #pragma omp barrier

            // if I my rank is either plus or minus, I should update my slice
            if (this->clayout->myrank == rankp)
            {
                ptrdiff_t iyy = iy - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    // modulo operation makes sense only for slab decomposition
                    ptrdiff_t izz = (this->clayout->sizes[1] - iz) % this->clayout->sizes[1];
                    ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                    {
                        const double ampp = std::sqrt(
                                (*(bufferp + ncomp(fc)*iz+cc))[0]*(*(bufferp + ncomp(fc)*iz+cc))[0] +
                                (*(bufferp + ncomp(fc)*iz+cc))[1]*(*(bufferp + ncomp(fc)*iz+cc))[1]);
                        const double phip = atan2(
                                (*(bufferp + ncomp(fc)*iz+cc))[1],
                                (*(bufferp + ncomp(fc)*iz+cc))[0]);
                        const double ampm = std::sqrt(
                                (*(bufferm + ncomp(fc)*izz+cc))[0]*(*(bufferm + ncomp(fc)*izz+cc))[0] +
                                (*(bufferm + ncomp(fc)*izz+cc))[1]*(*(bufferm + ncomp(fc)*izz+cc))[1]);
                        const double phim = atan2(
                                (*(bufferm + ncomp(fc)*izz+cc))[1],
                                (*(bufferm + ncomp(fc)*izz+cc))[0]);
                        const double amp = std::sqrt(ampp*ampm);
                        const double phi = (phip - phim)/2;
                        (*(cdata + ncomp(fc)*cindex + cc))[0] =  amp*std::cos(phi);
                        (*(cdata + ncomp(fc)*cindex + cc))[1] =  amp*std::sin(phi);
                    }
                }
            }
            // if I my rank is either plus or minus, I should update my slice
            if (this->clayout->myrank == rankm)
            {
                ptrdiff_t iyy = (this->clayout->sizes[0] - iy) - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    // modulo operation makes sense only for slab decomposition
                    ptrdiff_t izz = (this->clayout->sizes[1] - iz) % this->clayout->sizes[1];
                    ptrdiff_t cindex = this->get_cindex(0, iyy, izz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                    {
                        const double ampp = std::sqrt(
                                (*(bufferp + ncomp(fc)*iz+cc))[0]*(*(bufferp + ncomp(fc)*iz+cc))[0] +
                                (*(bufferp + ncomp(fc)*iz+cc))[1]*(*(bufferp + ncomp(fc)*iz+cc))[1]);
                        const double phip = atan2(
                                (*(bufferp + ncomp(fc)*iz+cc))[1],
                                (*(bufferp + ncomp(fc)*iz+cc))[0]);
                        const double ampm = std::sqrt(
                                (*(bufferm + ncomp(fc)*izz+cc))[0]*(*(bufferm + ncomp(fc)*izz+cc))[0] +
                                (*(bufferm + ncomp(fc)*izz+cc))[1]*(*(bufferm + ncomp(fc)*izz+cc))[1]);
                        const double phim = atan2(
                                (*(bufferm + ncomp(fc)*izz+cc))[1],
                                (*(bufferm + ncomp(fc)*izz+cc))[0]);
                        const double amp = std::sqrt(ampp*ampm);
                        const double phi = (phip - phim)/2;
                        (*(cdata + ncomp(fc)*cindex + cc))[0] =  amp*std::cos(phi);
                        (*(cdata + ncomp(fc)*cindex + cc))[1] = -amp*std::sin(phi);
                    }
                }
            }
        }// end omp parallel region
        }// end if
    }
    //fftw_interface<rnumber>::free(buffer);
    delete[] bufferp;
    delete[] bufferm;
    delete mpistatus;
    // symmetrize kx = 0, ky = 0 line
    if (this->clayout->myrank == this->clayout->rank[0][0])
    {
        for (ptrdiff_t iz = 1; iz < ptrdiff_t(this->clayout->sizes[1]/2); iz++)
        {
            ptrdiff_t cindex0 = this->get_cindex(0, 0, iz);
            ptrdiff_t cindex1 = this->get_cindex(0, 0, this->clayout->sizes[1] - iz);
            for (int cc = 0; cc < int(ncomp(fc)); cc++)
            {
                const double ampp = std::sqrt(
                        (*(cdata + cc + ncomp(fc)*cindex0))[0]*(*(cdata + cc + ncomp(fc)*cindex0))[0] +
                        (*(cdata + cc + ncomp(fc)*cindex0))[1]*(*(cdata + cc + ncomp(fc)*cindex0))[1]);
                const double phip = atan2(
                        (*(cdata + cc + ncomp(fc)*cindex0))[1],
                        (*(cdata + cc + ncomp(fc)*cindex0))[0]);
                const double ampm = std::sqrt(
                        (*(cdata + cc + ncomp(fc)*cindex1))[0]*(*(cdata + cc + ncomp(fc)*cindex1))[0] +
                        (*(cdata + cc + ncomp(fc)*cindex1))[1]*(*(cdata + cc + ncomp(fc)*cindex1))[1]);
                const double phim = atan2(
                        (*(cdata + cc + ncomp(fc)*cindex1))[1],
                        (*(cdata + cc + ncomp(fc)*cindex1))[0]);
                const double amp = std::sqrt(ampp*ampm);
                const double phi = (phip - phim)/2;
                (*(cdata + cc + ncomp(fc)*cindex0))[0] =  amp*std::cos(phi);
                (*(cdata + cc + ncomp(fc)*cindex0))[1] =  amp*std::sin(phi);
                (*(cdata + cc + ncomp(fc)*cindex1))[0] =  amp*std::cos(phi);
                (*(cdata + cc + ncomp(fc)*cindex1))[1] = -amp*std::sin(phi);
            }
        }
    }
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
    MPI_Barrier(this->clayout->comm); // TODO: figure out if this can be taken out by careful MPI tag generation
}

/** \brief Enforce Hermitian symmetry (fast and reasonable)
 *
 * TurTLE uses real-to-complex and complex-to-real FFTW transforms, because the
 * equations are PDEs of real-valued fields.
 * Hermitian symmetry means that Fourier-transformed real valued fields must
 * respect the equation \f$\hat f(-\mathbf{k}) = {\hat f}^*(\mathbf{k})\f$.
 *
 * FFTW enforces this property mainly by only storing the positive half of the
 * Fourier grid for the fastest array component. In TurTLE's case, this means
 * \f$ k_x \f$.
 * For the \f$ k_x = 0 \f$ plane, the symmetry must be enforced.
 *
 * This method uses an arithmetic mean of the \f$ (0, k_y, k_z)\f$ mode and
 * the conjugate of the \f$(0, -k_y, -k_z) \f$ mode to generate the desired
 * values. The method is fast (other than the required MPI communications).
 *
 * Note: the method is adequate either in cases where deviations from
 * Hermitian symmetry are small, or in cases where deviations from correct
 * physics is irrelevant.
 * In practice: initial condition fields may be strongly perturbed by the
 * application of this method, but they are unphysical anyway; during
 * the quasistationary regime of some simulation, the method is applied
 * regularly to all relevant fields, and deviations are expected to be small,
 * i.e. effect on PDE approximations is negligible.
 *
 * */

template <typename rnumber,
          field_backend be,
          field_components fc>
void field<rnumber, be, fc>::symmetrize()
{
    TIMEZONE("field::symmetrize");
    start_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
    assert(!this->real_space_representation);
    typename fftw_interface<rnumber>::complex *cdata = this->get_cdata();

    // make 0 mode real
    if (this->myrank == this->clayout->rank[0][0])
    {
        for (ptrdiff_t cc = 0; cc < ncomp(fc); cc++)
            cdata[cc][1] = 0.0;
    }
    // put kx = nx/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
    {
        ptrdiff_t cindex = this->get_cindex(this->clayout->sizes[2]-1, iy, iz);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }
    // put ky = ny/2 modes to 0
    if (this->clayout->myrank == this->clayout->rank[0][this->clayout->sizes[0]/2])
    {
        for (ptrdiff_t iz = 0; iz < ptrdiff_t(this->clayout->subsizes[1]); iz++)
        for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
        {
            ptrdiff_t cindex = this->get_cindex(ix, this->clayout->sizes[0]/2-this->clayout->starts[0], iz);
            for (int cc = 0; cc < int(ncomp(fc)); cc++) {
                (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
                (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
            }
        }
    }
    // put kz = nz/2 modes to 0
    for (ptrdiff_t iy = 0; iy < ptrdiff_t(this->clayout->subsizes[0]); iy++)
    for (ptrdiff_t ix = 0; ix < ptrdiff_t(this->clayout->subsizes[2]); ix++)
    {
        ptrdiff_t cindex = this->get_cindex(ix, iy, this->clayout->sizes[1]/2);
        for (int cc = 0; cc < int(ncomp(fc)); cc++) {
            (*(cdata + cc + ncomp(fc)*cindex))[0] = 0.0;
            (*(cdata + cc + ncomp(fc)*cindex))[1] = 0.0;
        }
    }

    // symmetrize kx = 0 plane, line by line, for ky != 0
    MPI_Status *mpistatus = new MPI_Status;
    // bufferp will hold data from "plus", i.e. iy
    // bufferm will hold data from "minus", i.e. ny - iy
    typename fftw_interface<rnumber>::complex *bufferp = new typename fftw_interface<rnumber>::complex[ncomp(fc)*this->clayout->sizes[1]];
    typename fftw_interface<rnumber>::complex *bufferm = new typename fftw_interface<rnumber>::complex[ncomp(fc)*this->clayout->sizes[1]];
    int rankp, rankm;
    // for each ky slice except ky=0
    for (ptrdiff_t iy = 1; iy < ptrdiff_t(this->clayout->sizes[0]/2); iy++)
    {
        // read rank plus and rank minus
        rankp = this->clayout->rank[0][iy];
        rankm = this->clayout->rank[0][this->clayout->sizes[0] - iy];
        // if my rank is plus or minus, then I should do actual work
        if (this->clayout->myrank == rankp ||
            this->clayout->myrank == rankm)
        {
            #pragma omp parallel
        {
            const ptrdiff_t zstart = ptrdiff_t(OmpUtils::ForIntervalStart(this->clayout->sizes[1]));
            const ptrdiff_t zend   = ptrdiff_t(OmpUtils::ForIntervalEnd(this->clayout->sizes[1]));
            // if my rank is rank plus, I should fill out the plus buffer
            if (this->clayout->myrank == rankp)
            {
                ptrdiff_t iyy = iy - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                        for (int imag_comp=0; imag_comp<2; imag_comp++)
                            (*(bufferp + ncomp(fc)*iz+cc))[imag_comp] =
                                (*(cdata + ncomp(fc)*cindex + cc))[imag_comp];
                }
            }
            // if my rank is rank minus, I should fill out the minus buffer
            if (this->clayout->myrank == rankm)
            {
                ptrdiff_t iyy = (this->clayout->sizes[0] - iy) - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                        for (int imag_comp=0; imag_comp<2; imag_comp++)
                            (*(bufferm + ncomp(fc)*iz+cc))[imag_comp] =
                                (*(cdata + ncomp(fc)*cindex + cc))[imag_comp];
                }
            }
            // after filling out buffers, synchronize threads
            #pragma omp barrier

            // if ranks are different, send and receive
            if (rankp != rankm)
            {
                // if my rank is rank plus, send buffer plus, receive buffer minus
                if (this->clayout->myrank == rankp && omp_get_thread_num() == 0)
                {
                    MPI_Send((void*)bufferp,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankm, 2*iy+0,
                             this->clayout->comm);
                    MPI_Recv((void*)bufferm,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankm, 2*iy+1,
                             this->clayout->comm,
                             mpistatus);
                }
                // if my rank is rank minus, receive buffer plus, send buffer minus
                if (this->clayout->myrank == rankm && omp_get_thread_num() == 0)
                {
                    MPI_Recv((void*)bufferp,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankp, 2*iy+0,
                             this->clayout->comm,
                             mpistatus);
                    MPI_Send((void*)bufferm,
                             ncomp(fc)*this->clayout->sizes[1],
                             mpi_real_type<rnumber>::complex(),
                             rankp, 2*iy+1,
                             this->clayout->comm);
                }
            }
            // ensure buffers are updated by MPI transfer before threads
            // start working again
            #pragma omp barrier

            // if I my rank is either plus or minus, I should update my slice
            if (this->clayout->myrank == rankp)
            {
                ptrdiff_t iyy = iy - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    // modulo operation makes sense only for slab decomposition
                    ptrdiff_t izz = (this->clayout->sizes[1] - iz) % this->clayout->sizes[1];
                    ptrdiff_t cindex = this->get_cindex(0, iyy, iz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                    {
                        (*(cdata + ncomp(fc)*cindex + cc))[0] =  ((*(bufferp + ncomp(fc)*iz+cc))[0] + (*(bufferm + ncomp(fc)*izz+cc))[0])/2;
                        (*(cdata + ncomp(fc)*cindex + cc))[1] =  ((*(bufferp + ncomp(fc)*iz+cc))[1] - (*(bufferm + ncomp(fc)*izz+cc))[1])/2;
                    }
                }
            }
            // if I my rank is either plus or minus, I should update my slice
            if (this->clayout->myrank == rankm)
            {
                ptrdiff_t iyy = (this->clayout->sizes[0] - iy) - this->clayout->starts[0];
                for (ptrdiff_t iz = zstart; iz < zend; iz++)
                {
                    // modulo operation makes sense only for slab decomposition
                    ptrdiff_t izz = (this->clayout->sizes[1] - iz) % this->clayout->sizes[1];
                    ptrdiff_t cindex = this->get_cindex(0, iyy, izz);
                    for (int cc = 0; cc < int(ncomp(fc)); cc++)
                    {
                        (*(cdata + ncomp(fc)*cindex + cc))[0] =  ((*(bufferp + ncomp(fc)*iz+cc))[0] + (*(bufferm + ncomp(fc)*izz+cc))[0])/2;
                        (*(cdata + ncomp(fc)*cindex + cc))[1] = -((*(bufferp + ncomp(fc)*iz+cc))[1] - (*(bufferm + ncomp(fc)*izz+cc))[1])/2;
                    }
                }
            }
        }// end omp parallel region
        }// end if
    }
    //fftw_interface<rnumber>::free(buffer);
    delete[] bufferp;
    delete[] bufferm;
    delete mpistatus;
    // symmetrize kx = 0, ky = 0 line
    if (this->clayout->myrank == this->clayout->rank[0][0])
    {
        for (ptrdiff_t iz = 1; iz < ptrdiff_t(this->clayout->sizes[1]/2); iz++)
        {
            ptrdiff_t cindex0 = this->get_cindex(0, 0, iz);
            ptrdiff_t cindex1 = this->get_cindex(0, 0, this->clayout->sizes[1] - iz);
            for (int cc = 0; cc < int(ncomp(fc)); cc++)
            {
                double re = ((*(cdata + cc + ncomp(fc)*cindex0))[0] + (*(cdata + cc + ncomp(fc)*cindex1))[0])/2;
                double im = ((*(cdata + cc + ncomp(fc)*cindex0))[1] - (*(cdata + cc + ncomp(fc)*cindex1))[1])/2;
                (*(cdata + cc + ncomp(fc)*cindex0))[0] =  re;
                (*(cdata + cc + ncomp(fc)*cindex0))[1] =  im;
                (*(cdata + cc + ncomp(fc)*cindex1))[0] =  re;
                (*(cdata + cc + ncomp(fc)*cindex1))[1] = -im;
            }
        }
    }
    finish_mpi_profiling_zone(turtle_mpi_pcontrol::FIELD);
}

/** \brief Compute field statistics
 *
 * \warning This method calls `compute_rspace_stats`, as well as `cospectrum`
 * This means that there will be a Fourier transform performed at some point.
 *
 * \warning The "max_estimate" value is manipulated before the call to `compute_rspace_stats`:
 * if the field is a vector field, the maximum estimate for the field magnitude is multiplied
 * by sqrt(3). This is a naive attempt to account for the fact that the magnitude is larger
 * than any 1 of the 3 components.
 */
template <typename rnumber,
          field_backend be,
          field_components fc>
template <kspace_dealias_type dt>
void field<rnumber, be, fc>::compute_stats(
        kspace<be, dt> *kk,
        const hid_t group,
        const std::string dset_name,
        const hsize_t toffset,
        const double max_estimate)
{
    TIMEZONE("field::compute_stats");
    std::vector<double> max_estimate_vector;
    bool did_rspace = false;
    switch(fc)
    {
        case ONE:
            max_estimate_vector.resize(1, max_estimate);
            break;
        case THREE:
            max_estimate_vector.resize(4, max_estimate);
            max_estimate_vector[3] *= std::sqrt(3);
            break;
        case THREExTHREE:
            max_estimate_vector.resize(9, max_estimate);
            break;
    }
    if (this->real_space_representation)
    {
        TIMEZONE("field::compute_stats::compute_rspace_stats");
        this->compute_rspace_stats(
                group,
                dset_name,
                toffset,
                max_estimate_vector);
        did_rspace = true;
        this->dft();
        this->normalize();
    }
    // what follows gave me a headache until I found this link:
    // http://stackoverflow.com/questions/8256636/expected-primary-expression-error-on-template-method-using
    kk->template cospectrum<rnumber, fc>(
            (typename fftw_interface<rnumber>::complex*)(this->data),
            group,
            dset_name + "_" + dset_name,
            toffset);
    if (!did_rspace)
    {
        this->ift();
        // normalization not required
        this->compute_rspace_stats(
                group,
                dset_name,
                toffset,
                max_estimate_vector);
    }
}

template <typename rnumber,
          field_backend be,
          field_components fc>
template <kspace_dealias_type dt>
double field<rnumber, be, fc>::L2norm(
        kspace<be, dt> *kk)
{
    TIMEZONE("field::L2norm");
    if (!this->real_space_representation)
        return kk->template L2norm<rnumber, fc>(this->get_cdata());
    else
    {
        shared_array<double> local_m2_threaded(
                1,
                [&](double* local_moment){
                    std::fill_n(local_moment, 1, 0);
            });

        this->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
                double *local_m2 = local_m2_threaded.getMine();
                for (unsigned int i=0; i<ncomp(fc); i++)
                    local_m2[0] += this->data[rindex*ncomp(fc)+i]*this->data[rindex*ncomp(fc)+i];
            });

        local_m2_threaded.mergeParallel();
        double m2;
        MPI_Allreduce(
                (void*)local_m2_threaded.getMasterData(),
                &m2,
                1,
                MPI_DOUBLE, MPI_SUM, this->comm);
        return std::sqrt(m2 / this->npoints);
    }
}

/** \brief Compute field gradient
 *
 *  Scalar fields are turned into vector fields
 *
 *  Vector fields are turned into tensor fields, with
 *   - fastest dimension corresponding to vector component
 *   - second fastest dimension corresponding to derivative direction
 *  i.e. values should be accessed with something like
 *   val(index, nabla_component, vector_component)
 *  (either cval or rval).
 * */

template <typename rnumber,
          field_backend be,
          field_components fc1,
          field_components fc2,
          kspace_dealias_type dt>
int compute_gradient(
        kspace<be, dt> *kk,
        field<rnumber, be, fc1> *src,
        field<rnumber, be, fc2> *dst)
{
    TIMEZONE("compute_gradient");
    assert(!src->real_space_representation);
    assert((fc1 == ONE && fc2 == THREE) ||
           (fc1 == THREE && fc2 == THREExTHREE));
    *dst = 0.0;
    dst->real_space_representation = false;
    switch(fc1)
            {
                case ONE:
    kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if (k2 < kk->kM2)
            {
                    dst->cval(cindex, 0, 0) = -kk->kx[xindex]*src->cval(cindex, 1);
                    dst->cval(cindex, 0, 1) =  kk->kx[xindex]*src->cval(cindex, 0);
                    dst->cval(cindex, 1, 0) = -kk->ky[yindex]*src->cval(cindex, 1);
                    dst->cval(cindex, 1, 1) =  kk->ky[yindex]*src->cval(cindex, 0);
                    dst->cval(cindex, 2, 0) = -kk->kz[zindex]*src->cval(cindex, 1);
                    dst->cval(cindex, 2, 1) =  kk->kz[zindex]*src->cval(cindex, 0);
                    }});
                    break;
                case THREE:
    kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            if (k2 < kk->kM2)
            {
                    for (unsigned int field_component = 0;
                         field_component < ncomp(fc1);
                         field_component++)
                    {
                        dst->cval(cindex, 0, field_component, 0) = -kk->kx[xindex]*src->cval(cindex, field_component, 1);
                        dst->cval(cindex, 0, field_component, 1) =  kk->kx[xindex]*src->cval(cindex, field_component, 0);
                        dst->cval(cindex, 1, field_component, 0) = -kk->ky[yindex]*src->cval(cindex, field_component, 1);
                        dst->cval(cindex, 1, field_component, 1) =  kk->ky[yindex]*src->cval(cindex, field_component, 0);
                        dst->cval(cindex, 2, field_component, 0) = -kk->kz[zindex]*src->cval(cindex, field_component, 1);
                        dst->cval(cindex, 2, field_component, 1) =  kk->kz[zindex]*src->cval(cindex, field_component, 0);
                    }
                    }});
                    break;
            }
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          kspace_dealias_type dt>
int compute_divergence(
        kspace<be, dt> *kk,
        field<rnumber, be, THREE> *src,
        field<rnumber, be, ONE> *dst)
{
    TIMEZONE("compute_divergence");
    assert(!src->real_space_representation);
    *dst = 0.0;
    dst->real_space_representation = false;
    kk->CLOOP(
            [&](const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2){
            dst->cval(cindex, 0) = -(kk->kx[xindex]*src->cval(cindex, 0, 1) +
                                     kk->ky[yindex]*src->cval(cindex, 1, 1) +
                                     kk->kz[zindex]*src->cval(cindex, 2, 1));
            dst->cval(cindex, 1) =  (kk->kx[xindex]*src->cval(cindex, 0, 0) +
                                     kk->ky[yindex]*src->cval(cindex, 1, 0) +
                                     kk->kz[zindex]*src->cval(cindex, 2, 0));
            });
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          kspace_dealias_type dt>
int compute_curl(
        kspace<be, dt> *kk,
        field<rnumber, be, THREE> *src,
        field<rnumber, be, THREE> *dst)
{
    TIMEZONE("compute_curl");
    assert(!src->real_space_representation);
    *dst = 0.0;
    dst->real_space_representation = false;
    kk->CLOOP(
                [&](const ptrdiff_t cindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex,
                    const double k2){
        if (k2 <= kk->kM2)
        {
            dst->cval(cindex,0,0) = -(kk->ky[yindex]*src->cval(cindex,2,1) - kk->kz[zindex]*src->cval(cindex,1,1));
            dst->cval(cindex,0,1) =  (kk->ky[yindex]*src->cval(cindex,2,0) - kk->kz[zindex]*src->cval(cindex,1,0));
            dst->cval(cindex,1,0) = -(kk->kz[zindex]*src->cval(cindex,0,1) - kk->kx[xindex]*src->cval(cindex,2,1));
            dst->cval(cindex,1,1) =  (kk->kz[zindex]*src->cval(cindex,0,0) - kk->kx[xindex]*src->cval(cindex,2,0));
            dst->cval(cindex,2,0) = -(kk->kx[xindex]*src->cval(cindex,1,1) - kk->ky[yindex]*src->cval(cindex,0,1));
            dst->cval(cindex,2,1) =  (kk->kx[xindex]*src->cval(cindex,1,0) - kk->ky[yindex]*src->cval(cindex,0,0));
        }
        else
            std::fill_n((rnumber*)(dst->get_cdata()+3*cindex), 6, 0.0);
    }
    );
    if (kk->layout->myrank == 0)
        std::fill_n((rnumber*)(dst->get_cdata()), 6, 0.0);
    dst->symmetrize();
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          kspace_dealias_type dt>
int invert_curl(
        kspace<be, dt> *kk,
        field<rnumber, be, THREE> *src,
        field<rnumber, be, THREE> *dst)
{
    TIMEZONE("invert_curl");
    assert(!src->real_space_representation);
    *dst = 0.0;
    dst->real_space_representation = false;
    kk->CLOOP(
                [&](const ptrdiff_t cindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex,
                    const double k2){
        const double kk2 = (k2 > 0) ? k2 : 1.0;
        if (k2 <= kk->kM2)
        {
            dst->cval(cindex,0,0) = -(kk->ky[yindex]*src->cval(cindex,2,1) - kk->kz[zindex]*src->cval(cindex,1,1)) / kk2;
            dst->cval(cindex,0,1) =  (kk->ky[yindex]*src->cval(cindex,2,0) - kk->kz[zindex]*src->cval(cindex,1,0)) / kk2;
            dst->cval(cindex,1,0) = -(kk->kz[zindex]*src->cval(cindex,0,1) - kk->kx[xindex]*src->cval(cindex,2,1)) / kk2;
            dst->cval(cindex,1,1) =  (kk->kz[zindex]*src->cval(cindex,0,0) - kk->kx[xindex]*src->cval(cindex,2,0)) / kk2;
            dst->cval(cindex,2,0) = -(kk->kx[xindex]*src->cval(cindex,1,1) - kk->ky[yindex]*src->cval(cindex,0,1)) / kk2;
            dst->cval(cindex,2,1) =  (kk->kx[xindex]*src->cval(cindex,1,0) - kk->ky[yindex]*src->cval(cindex,0,0)) / kk2;
        }
        else
            std::fill_n((rnumber*)(dst->get_cdata()+3*cindex), 6, 0.0);
    });
    dst->symmetrize();
    return EXIT_SUCCESS;
}

inline int bin_id(
        const double value,
        const double max_estimate,
        const double binsize,
        const int total_number_of_bins)
{
    assert(max_estimate > 0);
    assert(binsize > 0);
    assert(total_number_of_bins > 0);
    return std::min(
            std::max(int(0), int(std::floor((value + max_estimate) / binsize))),
            total_number_of_bins-1);
}

template <typename rnumber,
          field_backend be,
          field_components fc>
int conditional_rspace_PDF(
        field<rnumber, be, fc>*   ff,
        field<rnumber, be, ONE>*  cc,
        const hid_t               group,
        const std::string         dset_name,
        const hsize_t             toffset,
        const std::vector<double> max_ff_estimate,
        const double max_cc_estimate)
{
    TIMEZONE("conditional_rspace_PDF");
    assert(ff->real_space_representation);
    assert(cc->real_space_representation);
    assert(max_ff_estimate.size() >= ncomp(fc));
    int nbins_ff, nbins_cc;

    if (cc->myrank == 0)
    {
        hid_t dset, wspace;
        hsize_t dims[5];
        int ndims;
        variable_used_only_in_assert(ndims);
        dset = H5Dopen(
                group,
                ("histograms/" + dset_name).c_str(),
                H5P_DEFAULT);
        if (dset <= 0) {
            throw std::runtime_error("Couldn't open " + dset_name + " for reading dimensions.\n");
        }
        wspace = H5Dget_space(dset);
        ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
        if (fc == ONE)
            assert(ndims == 3);
        else if (fc == THREE) {
            assert(ndims == 4);
            assert(dims[3] == 3);
        } else if (fc == THREExTHREE) {
            assert(ndims == 5);
            assert(dims[3] == 3);
            assert(dims[4] == 3);
        }
        H5Sclose(wspace);
        H5Dclose(dset);
        nbins_cc = dims[1];
        nbins_ff = dims[2];
    }
    {
        TIMEZONE("MPI_Bcast");
        MPI_Bcast(&nbins_cc, 1, MPI_INT, 0, cc->comm);
        MPI_Bcast(&nbins_ff, 1, MPI_INT, 0, cc->comm);
    }

    // allocate histogram
    shared_array<ptrdiff_t> local_hist_threaded(
            nbins_cc*nbins_ff*ncomp(fc),
            [&](ptrdiff_t* local_hist){
                std::fill_n(local_hist, nbins_cc*nbins_ff*ncomp(fc), 0);
                });

    /// set up bin sizes
    const double bin_cc_size = 2*max_cc_estimate / nbins_cc;
    std::vector<double> bin_ff_size;
    bin_ff_size.resize(ncomp(fc));
    for (unsigned int i=0; i<ncomp(fc); i++)
    {
        bin_ff_size[i] = 2*max_ff_estimate[i] / nbins_ff;
    }

    {
        TIMEZONE("field::RLOOP");
        cc->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
            ptrdiff_t *local_hist = local_hist_threaded.getMine();
            const int bin_cc = bin_id(
                    cc->rval(rindex),
                    max_cc_estimate,
                    bin_cc_size,
                    nbins_cc);
            for (unsigned int component = 0;
                 component < ncomp(fc);
                 component++) {
                    const int bin_ff = bin_id(
                            ff->rval(rindex, component),
                            max_ff_estimate[component],
                            bin_ff_size[component],
                            nbins_ff);
                    local_hist[
                        (nbins_ff * bin_cc + bin_ff)*ncomp(fc)+component]++;
                }
            });
    }
    // OpenMP merge
    local_hist_threaded.mergeParallel();
    ptrdiff_t *hist = new ptrdiff_t[nbins_cc*nbins_ff*ncomp(fc)];
    // MPI merge
    MPI_Allreduce(
            (void*)local_hist_threaded.getMasterData(),
            (void*)hist,
            nbins_cc*nbins_ff*ncomp(fc),
            MPI_INT64_T, MPI_SUM, cc->comm);

    // output
    if (cc->myrank == 0)
    {
        TIMEZONE("root-work");
        hid_t dset, wspace, mspace;
        hsize_t count[5], offset[5];
        offset[0] = toffset;
        offset[1] = 0;
        offset[2] = 0;
        offset[3] = 0;
        offset[4] = 0;
        count[0] = 1;
        count[1] = nbins_cc;
        count[2] = nbins_ff;
        count[3] = 3;
        count[4] = 3;
        dset = H5Dopen(
                group,
                ("histograms/" + dset_name).c_str(),
                H5P_DEFAULT);
        if (dset <= 0) {
            throw std::runtime_error(
                    "Couldn't open " + dset_name + " for writing.\n");
        }
        wspace = H5Dget_space(dset);
        if (fc == ONE) {
            mspace = H5Screate_simple(3, count, NULL);
        } else if (fc == THREE) {
            mspace = H5Screate_simple(4, count, NULL);
        } else if (fc == THREExTHREE) {
            mspace = H5Screate_simple(5, count, NULL);
        }
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_INT64, mspace, wspace, H5P_DEFAULT, hist);
        H5Sclose(wspace);
        H5Sclose(mspace);
        H5Dclose(dset);
    }

    delete[] hist;
    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
int joint_rspace_PDF(
        field<rnumber, be, fc> *f1,
        field<rnumber, be, fc> *f2,
        const hid_t group,
        const std::string dset_name,
        const hsize_t toffset,
        const std::vector<double> max_f1_estimate,
        const std::vector<double> max_f2_estimate)
{
    TIMEZONE("joint_rspace_PDF");
    assert(f1->real_space_representation);
    assert(f2->real_space_representation);
    if (fc == THREE)
    {
        assert(max_f1_estimate.size() == 4);
        assert(max_f2_estimate.size() == 4);
    }
    else if (fc == ONE)
    {
        assert(max_f1_estimate.size() == 1);
        assert(max_f2_estimate.size() == 1);
    }
    int nbins;
    std::string dsetc, dsetm;
    dsetc = "histograms/" + dset_name + "_components";
    if (fc == THREE)
        dsetm = "histograms/" + dset_name + "_magnitudes";
    else
        dsetm = "histograms/" + dset_name;
    if (f1->myrank == 0)
    {
        hid_t dset, wspace;
        hsize_t dims[5];
        int ndims;
        variable_used_only_in_assert(ndims);
        if (fc == THREE)
        {
            dset = H5Dopen(
                    group,
                    dsetc.c_str(),
                    H5P_DEFAULT);
            wspace = H5Dget_space(dset);
            ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
            assert(ndims == 5);
            assert(dims[3] == 3);
            assert(dims[4] == 3);
            H5Sclose(wspace);
            H5Dclose(dset);
        }
        dset = H5Dopen(
                group,
                dsetm.c_str(),
                H5P_DEFAULT);
        wspace = H5Dget_space(dset);
        ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
        assert(ndims == 3);
        H5Sclose(wspace);
        H5Dclose(dset);
        nbins = dims[1];
    }
    {
        TIMEZONE("MPI_Bcast");
        MPI_Bcast(&nbins, 1, MPI_INT, 0, f1->comm);
    }

        /// histogram components
        shared_array<ptrdiff_t> local_histc_threaded(
                nbins*nbins*9,
                [&](ptrdiff_t* local_hist){
                    std::fill_n(local_hist, nbins*nbins*9, 0);
                    });

    /// histogram magnitudes
    shared_array<ptrdiff_t> local_histm_threaded(
            nbins*nbins,
            [&](ptrdiff_t* local_hist){
                std::fill_n(local_hist, nbins*nbins, 0);
                });

    /// set up bin sizes
    std::vector<double> bin1size, bin2size;
    bin1size.resize(4);
    bin2size.resize(4);
    if (fc == THREE)
    {
        for (unsigned int i=0; i<3; i++)
        {
            bin1size[i] = 2*max_f1_estimate[i] / nbins;
            bin2size[i] = 2*max_f2_estimate[i] / nbins;
        }
        bin1size[3] = max_f1_estimate[3] / nbins;
        bin2size[3] = max_f2_estimate[3] / nbins;
    }
    else if (fc == ONE)
    {
        for (unsigned int i=0; i<4; i++)
        {
            bin1size[i] = 2*max_f1_estimate[0] / nbins;
            bin2size[i] = 2*max_f2_estimate[0] / nbins;
        }
    }


    {
        TIMEZONE("field::RLOOP");
        f1->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
            ptrdiff_t *local_histm = local_histm_threaded.getMine();
            int bin1 = 0;
            int bin2 = 0;
            if (fc == THREE)
            {
                ptrdiff_t *local_histc = local_histc_threaded.getMine();

                double mag1, mag2;
                mag1 = 0.0;
                mag2 = 0.0;
                for (unsigned int i=0; i<3; i++)
                {
                    double val1 = f1->rval(rindex, i);
                    mag1 += val1*val1;
                    int bin1 = int(std::floor((val1 + max_f1_estimate[i])/bin1size[i]));
                    mag2 = 0.0;
                    for (unsigned int j=0; j<3; j++)
                    {
                        double val2 = f2->rval(rindex, j);
                        mag2 += val2*val2;
                        int bin2 = int(std::floor((val2 + max_f2_estimate[j])/bin2size[j]));
                        if ((bin1 >= 0 && bin1 < nbins) &&
                            (bin2 >= 0 && bin2 < nbins))
                            local_histc[(bin1*nbins + bin2)*9 + i*3 + j]++;
                    }
                }
                bin1 = int(std::floor(std::sqrt(mag1)/bin1size[3]));
                bin2 = int(std::floor(std::sqrt(mag2)/bin2size[3]));
            }
            else if (fc == ONE)
            {
                bin1 = int(std::floor((f1->rval(rindex) + max_f1_estimate[0])/bin1size[3]));
                bin2 = int(std::floor((f2->rval(rindex) + max_f2_estimate[0])/bin2size[3]));
            }
            if ((bin1 >= 0 && bin1 < nbins) &&
                (bin2 >= 0 && bin2 < nbins))
                local_histm[bin1*nbins + bin2]++;
            });
    }
    local_histm_threaded.mergeParallel();
    ptrdiff_t *histm = new ptrdiff_t[nbins*nbins];
    ptrdiff_t *histc = NULL;
    if (fc == THREE)
    {
        local_histc_threaded.mergeParallel();
        histc = new ptrdiff_t[nbins*nbins*9];
    }
    {
        MPI_Allreduce(
                (void*)local_histm_threaded.getMasterData(),
                (void*)histm,
                nbins*nbins,
                MPI_INT64_T, MPI_SUM, f1->comm);
        if (fc == THREE)
            MPI_Allreduce(
                    (void*)local_histc_threaded.getMasterData(),
                    (void*)histc,
                    nbins*nbins*9,
                    MPI_INT64_T, MPI_SUM, f1->comm);
    }

    if (f1->myrank == 0)
    {
        TIMEZONE("root-work");
        hid_t dset, wspace, mspace;
        hsize_t count[5], offset[5];
        offset[0] = toffset;
        offset[1] = 0;
        offset[2] = 0;
        count[0] = 1;
        count[1] = nbins;
        count[2] = nbins;
        if (fc == THREE)
        {
            dset = H5Dopen(group, dsetc.c_str(), H5P_DEFAULT);
            assert(dset > 0);
            wspace = H5Dget_space(dset);
            offset[3] = 0;
            offset[4] = 0;
            count[3] = 3;
            count[4] = 3;
            mspace = H5Screate_simple(5, count, NULL);
            H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
            H5Dwrite(dset, H5T_NATIVE_INT64, mspace, wspace, H5P_DEFAULT, histc);
            H5Sclose(wspace);
            H5Sclose(mspace);
            H5Dclose(dset);
        }
        dset = H5Dopen(group, dsetm.c_str(), H5P_DEFAULT);
        assert(dset > 0);
        mspace = H5Screate_simple(3, count, NULL);
        wspace = H5Dget_space(dset);
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_INT64, mspace, wspace, H5P_DEFAULT, histm);
        H5Sclose(wspace);
        H5Sclose(mspace);
        H5Dclose(dset);
    }

    delete[] histm;
    if (fc == THREE)
        delete[] histc;

    return EXIT_SUCCESS;
}

// Debarghya edit for 3 scale PDFs //

template <typename rnumber,
          field_backend be>
int joint_rspace_3PDF(
        field<rnumber, be, ONE> *f1,
        field<rnumber, be, ONE> *f2,
        field<rnumber, be, ONE> *f3,
        const hid_t group,
        const std::string dset_name,
        const hsize_t toffset,
        const std::vector<double> max_f1_estimate,
        const std::vector<double> max_f2_estimate,
        const std::vector<double> max_f3_estimate)
{
    TIMEZONE("joint_rspace_3PDF");
    assert(f1->real_space_representation);
    assert(f2->real_space_representation);
    assert(f3->real_space_representation);

    assert(max_f1_estimate.size() == 1);
    assert(max_f2_estimate.size() == 1);
    assert(max_f3_estimate.size() == 1);

    int nbins;
    std::string dsetc, dsetm;
    dsetc = "histograms/" + dset_name + "_components";
    dsetm = "histograms/" + dset_name;
    if (f1->myrank == 0)
    {
        hid_t dset, wspace;
        hsize_t dims[5];
        int ndims;
        dset = H5Dopen(
                group,
                dsetm.c_str(),
                H5P_DEFAULT);
        wspace = H5Dget_space(dset);
        ndims = H5Sget_simple_extent_dims(wspace, dims, NULL);
        variable_used_only_in_assert(ndims);
        assert(ndims == 4);
        H5Sclose(wspace);
        H5Dclose(dset);
        nbins = dims[1];
    }
    {
        TIMEZONE("MPI_Bcast");
        MPI_Bcast(&nbins, 1, MPI_INT, 0, f1->comm);
    }


    /// histogram magnitudes
    shared_array<ptrdiff_t> local_histm_threaded(
            nbins*nbins*nbins,
            [&](ptrdiff_t* local_hist){
                std::fill_n(local_hist, nbins*nbins*nbins, 0);
                });

    /// set up bin sizes
    std::vector<double> bin1size, bin2size, bin3size;
    bin1size.resize(1);
    bin2size.resize(1);
    bin3size.resize(1);

    bin1size[0] = 2*max_f1_estimate[0] / nbins;
    bin2size[0] = 2*max_f2_estimate[0] / nbins;
    bin3size[0] = 2*max_f3_estimate[0] / nbins;


    {
        TIMEZONE("field::RLOOP");
        f1->RLOOP(
                [&](const ptrdiff_t rindex,
                    const ptrdiff_t xindex,
                    const ptrdiff_t yindex,
                    const ptrdiff_t zindex){
            ptrdiff_t *local_histm = local_histm_threaded.getMine();
            int bin1 = 0;
            int bin2 = 0;
            int bin3 = 0;

            bin1 = int(std::floor((f1->rval(rindex) + max_f1_estimate[0])/bin1size[0]));
            bin2 = int(std::floor((f2->rval(rindex) + max_f2_estimate[0])/bin2size[0]));
            bin3 = int(std::floor((f3->rval(rindex) + max_f3_estimate[0])/bin3size[0]));
            if ((bin1 >= 0 && bin1 < nbins) &&
                (bin2 >= 0 && bin2 < nbins) &&
                (bin3 >= 0 && bin3 < nbins))
                local_histm[bin1*nbins*nbins + bin2*nbins + bin3]++;
            });
    }
    local_histm_threaded.mergeParallel();
    ptrdiff_t *histm = new ptrdiff_t[nbins*nbins*nbins];
    {
        MPI_Allreduce(
                (void*)local_histm_threaded.getMasterData(),
                (void*)histm,
                nbins*nbins*nbins,
                MPI_INT64_T, MPI_SUM, f1->comm);
    }

    if (f1->myrank == 0)
    {
        TIMEZONE("root-work");
        hid_t dset, wspace, mspace;
        hsize_t count[5], offset[5];

        dset = H5Dopen(group, dsetm.c_str(), H5P_DEFAULT);
        assert(dset > 0);
        offset[0] = toffset;
        offset[1] = 0;
        offset[2] = 0;
        offset[3] = 0;
        count[0] = 1;
        count[1] = nbins;
        count[2] = nbins;
        count[3] = nbins;
        mspace = H5Screate_simple(4, count, NULL);
        wspace = H5Dget_space(dset);
        H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
        H5Dwrite(dset, H5T_NATIVE_INT64, mspace, wspace, H5P_DEFAULT, histm);
        H5Sclose(wspace);
        H5Sclose(mspace);
        H5Dclose(dset);
    }

    delete[] histm;

    return EXIT_SUCCESS;
}


template <typename rnumber,
          field_backend be,
          field_components fc>
field<rnumber, be, fc>& field<rnumber, be, fc>::operator=(const typename fftw_interface<rnumber>::complex *__restrict__ source)
{
    // use CLOOP pattern, because we want the array to be arranged in memory
    // for optimal access by FFTW
    #pragma omp parallel
    {
        const hsize_t start = OmpUtils::ForIntervalStart(this->clayout->subsizes[1]);
        const hsize_t end = OmpUtils::ForIntervalEnd(this->clayout->subsizes[1]);

        for (hsize_t yindex = 0; yindex < this->clayout->subsizes[0]; yindex++){
            for (hsize_t zindex = start; zindex < end; zindex++){
                const ptrdiff_t cindex = (
                        yindex*this->clayout->subsizes[1]*this->clayout->subsizes[2] +
                        zindex*this->clayout->subsizes[2]);
                std::copy((rnumber*)(source + cindex*ncomp(fc)),
                          (rnumber*)(source + (cindex+this->clayout->subsizes[2])*ncomp(fc)),
                          this->data+(cindex*ncomp(fc))*2);
            }
        }
    }
    this->real_space_representation = false;
    return *this;
}


template <typename rnumber,
          field_backend be,
          field_components fc>
field<rnumber, be, fc>& field<rnumber, be, fc>::operator=(const rnumber *__restrict__ source)
{
    TIMEZONE("field::operator=(const rnumber *source)");
    // use RLOOP, such that memory caching per thread stuff is not messed up
    this->RLOOP(
            [&](const ptrdiff_t rindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex)
            {
                std::copy(source + rindex*ncomp(fc),
                          source + (rindex+1)*ncomp(fc),
                          this->data + rindex*ncomp(fc));
            });
    this->real_space_representation = true;
    return *this;
}

template <typename rnumber,
          field_backend be,
          field_components fc>
field<rnumber, be, fc> &field<rnumber, be, fc>::operator=(
        const rnumber value)
{
    TIMEZONE("field::operator=(const rnumber value)");
    if (this->real_space_representation)
        // use RLOOP, such that memory caching per thread stuff is not messed up
        this->RLOOP(
            [&](const ptrdiff_t rindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex)
            {
                std::fill_n(this->data + rindex*ncomp(fc),
                            ncomp(fc),
                            value);
            });
    else
    {
        // use CLOOP, such that memory caching per thread stuff is not messed up
        #pragma omp parallel
        {
            const hsize_t start = OmpUtils::ForIntervalStart(this->clayout->subsizes[1]);
            const hsize_t end = OmpUtils::ForIntervalEnd(this->clayout->subsizes[1]);

            for (hsize_t yindex = 0; yindex < this->clayout->subsizes[0]; yindex++){
                for (hsize_t zindex = start; zindex < end; zindex++){
                    const ptrdiff_t cindex = (
                            yindex*this->clayout->subsizes[1]*this->clayout->subsizes[2] +
                            zindex*this->clayout->subsizes[2]);
                    for (hsize_t xindex = 0; xindex < this->clayout->subsizes[2]; xindex++)
                    {
                        for (unsigned int cc=0; cc < ncomp(fc); cc++)
                        {
                            *(this->data + 2*((cindex+xindex)*ncomp(fc) + cc)) = value;
                            *(this->data + 2*((cindex+xindex)*ncomp(fc) + cc)+1) = 0.0;
                        }
                    }
                }
            }
        }
    }
    return *this;
}



template <typename rnumber,
          field_backend be,
          field_components fc>
field<rnumber, be, fc> &field<rnumber, be, fc>::operator=(
        const field<rnumber, be, fc> &src)
{
    TIMEZONE("field::operator=(const field<rnumber, be, fc> &src)");
    if (src.real_space_representation)
    {
        assert(this->get_nx() == src.get_nx());
        assert(this->get_ny() == src.get_ny());
        assert(this->get_nz() == src.get_nz());
        this->operator=(src.data);
    }
    else
    {
        // simple copy
        if (this->get_nx() == src.get_nx() &&
            this->get_ny() == src.get_ny() &&
            this->get_nz() == src.get_nz())
        {
            this->operator=(src.get_cdata());
        }
        // complicated resize
        else
        {
            this->real_space_representation = false;
            int64_t slice_size = src.clayout->local_size / src.clayout->subsizes[0];
            // clean up
            *this = 0.0;
            typename fftw_interface<rnumber>::complex *buffer;
            buffer = fftw_interface<rnumber>::alloc_complex(slice_size*ncomp(fc));

            int min_fast_dim =
                    (src.clayout->sizes[2] > this->clayout->sizes[2]) ?
                        this->clayout->sizes[2] : src.clayout->sizes[2];

            int64_t ii0, ii1;
            int64_t oi0, oi1;
            int64_t delta1, delta0;
            int irank, orank;
            delta0 = (this->clayout->sizes[0] - src.clayout->sizes[0]);
            delta1 = (this->clayout->sizes[1] - src.clayout->sizes[1]);
            for (ii0=0; ii0 < int64_t(src.clayout->sizes[0]); ii0++)
            {
                if (ii0 <= int64_t(src.clayout->sizes[0]/2))
                {
                    oi0 = ii0;
                    if (oi0 > int64_t(this->clayout->sizes[0]/2))
                        continue;
                }
                else
                {
                    oi0 = ii0 + delta0;
                    if ((oi0 < 0) || ((int64_t(this->clayout->sizes[0]) - oi0) >= int64_t(this->clayout->sizes[0]/2)))
                        continue;
                }
                if (be == FFTW)
                {
                    irank = src.clayout->rank[0][ii0];
                    orank = this->clayout->rank[0][oi0];
                }
                else
                {// TODO: handle 2D layout here
                }
                if ((irank == orank) &&
                        (irank == src.clayout->myrank))
                {
                    std::copy(
                            (rnumber*)(src.get_cdata() + (ii0 - src.clayout->starts[0]    )*slice_size),
                            (rnumber*)(src.get_cdata() + (ii0 - src.clayout->starts[0] + 1)*slice_size),
                            (rnumber*)buffer);
                }
                else
                {
                    if (src.clayout->myrank == irank)
                    {
                        MPI_Send(
                                (void*)(src.get_cdata() + (ii0-src.clayout->starts[0])*slice_size),
                                slice_size,
                                mpi_real_type<rnumber>::complex(),
                                orank,
                                ii0,
                                src.clayout->comm);
                    }
                    if (src.clayout->myrank == orank)
                    {
                        MPI_Recv(
                                    (void*)(buffer),
                                    slice_size,
                                    mpi_real_type<rnumber>::complex(),
                                    irank,
                                    ii0,
                                    src.clayout->comm,
                                    MPI_STATUS_IGNORE);
                    }
                }
                if (src.clayout->myrank == orank)
                {
                    for (ii1 = 0; ii1 < int64_t(src.clayout->sizes[1]); ii1++)
                    {
                        if (ii1 <= int64_t(src.clayout->sizes[1]/2))
                        {
                            oi1 = ii1;
                            if (oi1 > int64_t(this->clayout->sizes[1]/2))
                                continue;
                        }
                        else
                        {
                            oi1 = ii1 + delta1;
                            if ((oi1 < 0) || ((int64_t(this->clayout->sizes[1]) - oi1) >= int64_t(this->clayout->sizes[1]/2)))
                                continue;
                        }
                        std::copy(
                                    (rnumber*)(buffer + (ii1*src.clayout->sizes[2]*ncomp(fc))),
                                (rnumber*)(buffer + (ii1*src.clayout->sizes[2] + min_fast_dim)*ncomp(fc)),
                                (rnumber*)(this->get_cdata() +
                                           ((oi0 - this->clayout->starts[0])*this->clayout->sizes[1] +
                                oi1)*this->clayout->sizes[2]*ncomp(fc)));
                    }
                }
            }
            fftw_interface<rnumber>::free(buffer);
            MPI_Barrier(src.clayout->comm);
        }
    }
    return *this;
}

template <typename rnumber,
          field_backend be,
          field_components fc,
          kspace_dealias_type dt>
int make_gaussian_random_field(
        kspace<be, dt> *kk,
        field<rnumber, be, fc> *output_field,
        const int rseed,
        const double dissipation,
        const double Lint,
        const double etaK,
        const double c_L,
        const double c_eta,
        const double coefficient)
{
    TIMEZONE("make_gaussian_random_field");
    // initialize a separate random number generator for each thread
    std::vector<std::mt19937_64> rgen;
    std::normal_distribution<rnumber> rdist;
    rgen.resize(omp_get_max_threads());
    // seed random number generators such that no seed is ever repeated if we change the value of rseed.
    // basically use a multi-dimensional array indexing technique to come up with actual seed.
    // Note: this method IS NOT MPI/OpenMP-invariant!
    for (int thread_id=0; thread_id < omp_get_max_threads(); thread_id++)
    {
        int current_seed = (
                rseed*omp_get_max_threads()*output_field->clayout->nprocs +
                output_field->clayout->myrank*omp_get_max_threads() +
                thread_id);
        //DEBUG_MSG("in make_gaussian_random_field, thread_id = %d, current_seed = %d\n", thread_id, current_seed);
        rgen[thread_id].seed(current_seed);
    }
    output_field->real_space_representation = false;
    *output_field = 0.0;
    //DEBUG_MSG("slope: %g\n", slope);
    // inside loop use only thread-local random number generator
    kk->CLOOP([&](
                const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2)
                    {
                    if (k2 > 0){
                        // Simple spectrum, diverges at zero
                        // double spectrum = coefficient * pow(k2, slope/2.) * exp(-sqrt(k2)/k_cutoff);

                        // Pope spectrum, Pope (2000), ''Turbulent flows'', p. 232
                        double C = 1.5;
                        double beta = 5.2;
                        double f_L = std::pow(std::sqrt(k2)*Lint/std::sqrt(k2*std::pow(Lint, 2) + c_L), 11./3.);
                        double f_eta = exp(-beta*(std::pow(std::pow(k2, 2)*std::pow(etaK, 4) + std::pow(c_eta, 4), 1./4.) - c_eta));
                        double spectrum = coefficient*C*std::pow(dissipation, 2./3.)*std::pow(k2, -5./6.)*f_L*f_eta;
                        // TODO: What about Delta k?
                        switch(fc)
                        {
                        case ONE:
                        {
                            output_field->cval(cindex,0) = rdist(rgen[omp_get_thread_num()]) * std::sqrt(spectrum * kk->dkx*kk->dky*kk->dkz / (4.*M_PI*k2));
                            output_field->cval(cindex,1) = rdist(rgen[omp_get_thread_num()]) * std::sqrt(spectrum * kk->dkx*kk->dky*kk->dkz / (4.*M_PI*k2));
                            break;
                        }
                        case THREE:
                        for (int cc = 0; cc<3; cc++)
                        {
                            // factor 3 compensates the trace between the spectral tensor and the energy spectrum
                            output_field->cval(cindex,cc,0) = rdist(rgen[omp_get_thread_num()]) * std::sqrt(spectrum * kk->dkx*kk->dky*kk->dkz / 3. / (4.*M_PI*k2));
                            output_field->cval(cindex,cc,1) = rdist(rgen[omp_get_thread_num()]) * std::sqrt(spectrum * kk->dkx*kk->dky*kk->dkz / 3. / (4.*M_PI*k2));
                        }
                            break;
                        case THREExTHREE:
                        for (int cc = 0; cc<3; cc++)
                        for (int ccc = 0; ccc<3; ccc++)
                        {
                            // factor 9 compensates the trace between the spectral tensor and the energy spectrum
                            output_field->cval(cindex,cc,ccc,0) = rdist(rgen[omp_get_thread_num()]) * std::sqrt(spectrum * kk->dkx*kk->dky*kk->dkz / 9. / (4.*M_PI*k2));
                            output_field->cval(cindex,cc,ccc,1) = rdist(rgen[omp_get_thread_num()]) * std::sqrt(spectrum * kk->dkx*kk->dky*kk->dkz / 9. / (4.*M_PI*k2));
                        }
                            break;
                        }
                    }
            });
    output_field->symmetrize();

    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          kspace_dealias_type dt>
int generate_random_phase_field(
        kspace<be, dt> *kk,
        field<rnumber, be, ONE> *output_field,
        const int rseed)
{
    TIMEZONE("generate_random_phase_field");
    assert(output_field->real_space_representation == false);
    const double twopi = 4*std::acos(0.0);

    // initialize a separate random number generator for each thread
    std::vector<std::mt19937_64> rgen;
    std::uniform_real_distribution<rnumber> rdist(0, twopi);
    rgen.resize(omp_get_max_threads());
    // seed random number generators such that no seed is ever repeated if we change the value of rseed.
    // basically use a multi-dimensional array indexing technique to come up with actual seed.
    // Note: this method IS NOT invariant to the MPI/OpenMP configuration.
    for (int thread_id=0; thread_id < omp_get_max_threads(); thread_id++)
    {
        int current_seed = (
                rseed*omp_get_max_threads()*output_field->clayout->nprocs +
                output_field->clayout->myrank*omp_get_max_threads() +
                thread_id);
        rgen[thread_id].seed(current_seed);
    }

    // inside loop use only thread-local random number generator
    kk->CLOOP([&](
                const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2)
                    {
                    if (k2 > 0 && k2 < kk->kM2)
                    {
                        // single phase shift for all components
                        const double phi = rdist(rgen[omp_get_thread_num()]);
                        output_field->cval(cindex, 0) = std::cos(phi);
                        output_field->cval(cindex, 1) = std::sin(phi);
                    }
                    else
                    {
                    output_field->cval(cindex, 0) = rnumber(0);
                    output_field->cval(cindex, 1) = rnumber(0);
                    }
            });
    output_field->symmetrize();
    // ensure absolute value is 1 for all modes
    {
    kk->CLOOP([&](
                const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2)
                    {
                    if (k2 > 0 && k2 < kk->kM2)
                    {
                        {
                            const double amplitude = std::sqrt(
                                output_field->cval(cindex, 0)*output_field->cval(cindex, 0)+
                                output_field->cval(cindex, 1)*output_field->cval(cindex, 1));
                            if (amplitude > 0)
                            {
                            output_field->cval(cindex, 0) /= amplitude;
                            output_field->cval(cindex, 1) /= amplitude;
                            }
                            else
                            {
                            output_field->cval(cindex, 0) = 1;
                            output_field->cval(cindex, 1) = 0;
                            }
                        }
                    }
                    if (xindex == ptrdiff_t(output_field->clayout->sizes[2])-ptrdiff_t(1))
                        {
                        output_field->cval(cindex, 0) = 0;
                        output_field->cval(cindex, 1) = 0;
                        }
            });
    }

    return EXIT_SUCCESS;
}

template <typename rnumber,
          field_backend be,
          field_components fc,
          kspace_dealias_type dt>
int apply_phase_field_shift(
        kspace<be, dt> *kk,
        field<rnumber, be, fc> *output_field,
        field<rnumber, be, ONE> *phase_field)
{
    TIMEZONE("apply_phase_field_shift");
    assert(output_field->real_space_representation == false);
    assert(phase_field->real_space_representation == false);

    // inside loop use only thread-local random number generator
    kk->CLOOP([&](
                const ptrdiff_t cindex,
                const ptrdiff_t xindex,
                const ptrdiff_t yindex,
                const ptrdiff_t zindex,
                const double k2)
                    {
                    if (k2 > 0)
                    {
                        // single phase shift for all components
                        switch(fc)
                        {
                        case ONE:
                        {
                            complex_number_phase_shifter(
                                    output_field->cval(cindex, 0),
                                    output_field->cval(cindex, 1),
                                    phase_field->cval(cindex, 0),
                                    phase_field->cval(cindex, 1));
                            break;
                        }
                        case THREE:
                            for (int cc = 0; cc<3; cc++)
                            {
                                complex_number_phase_shifter(
                                        output_field->cval(cindex, cc, 0),
                                        output_field->cval(cindex, cc, 1),
                                        phase_field->cval(cindex, 0),
                                        phase_field->cval(cindex, 1));
                            }
                            break;
                        case THREExTHREE:
                            for (int cc = 0; cc<3; cc++)
                            for (int ccc = 0; ccc<3; ccc++)
                            {
                                complex_number_phase_shifter(
                                        output_field->cval(cindex, cc, ccc, 0),
                                        output_field->cval(cindex, cc, ccc, 1),
                                        phase_field->cval(cindex, 0),
                                        phase_field->cval(cindex, 1));
                            }
                            break;
                        }
                    }
            });
    output_field->symmetrize();

    return EXIT_SUCCESS;
}

template class field<float, FFTW, ONE>;
template class field<float, FFTW, THREE>;
template class field<float, FFTW, THREExTHREE>;
template class field<double, FFTW, ONE>;
template class field<double, FFTW, THREE>;
template class field<double, FFTW, THREExTHREE>;

template void field<float, FFTW, ONE>::compute_stats<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<float, FFTW, THREE>::compute_stats<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<float, FFTW, THREExTHREE>::compute_stats<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *,
        const hid_t, const std::string, const hsize_t, const double);

template void field<double, FFTW, ONE>::compute_stats<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<double, FFTW, THREE>::compute_stats<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<double, FFTW, THREExTHREE>::compute_stats<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *,
        const hid_t, const std::string, const hsize_t, const double);

template void field<float, FFTW, ONE>::compute_stats<SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<float, FFTW, THREE>::compute_stats<SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<float, FFTW, THREExTHREE>::compute_stats<SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        const hid_t, const std::string, const hsize_t, const double);

template void field<double, FFTW, ONE>::compute_stats<SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<double, FFTW, THREE>::compute_stats<SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        const hid_t, const std::string, const hsize_t, const double);
template void field<double, FFTW, THREExTHREE>::compute_stats<SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        const hid_t, const std::string, const hsize_t, const double);

template double field<float, FFTW, ONE>::L2norm<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *);
template double field<float, FFTW, THREE>::L2norm<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *);
template double field<float, FFTW, THREExTHREE>::L2norm<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *);

template double field<double, FFTW, ONE>::L2norm<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *);
template double field<double, FFTW, THREE>::L2norm<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *);
template double field<double, FFTW, THREExTHREE>::L2norm<TWO_THIRDS>(
        kspace<FFTW, TWO_THIRDS> *);

template double field<float, FFTW, ONE>::L2norm<SMOOTH>(
        kspace<FFTW, SMOOTH> *);
template double field<float, FFTW, THREE>::L2norm<SMOOTH>(
        kspace<FFTW, SMOOTH> *);
template double field<float, FFTW, THREExTHREE>::L2norm<SMOOTH>(
        kspace<FFTW, SMOOTH> *);

template double field<double, FFTW, ONE>::L2norm<SMOOTH>(
        kspace<FFTW, SMOOTH> *);
template double field<double, FFTW, THREE>::L2norm<SMOOTH>(
        kspace<FFTW, SMOOTH> *);
template double field<double, FFTW, THREExTHREE>::L2norm<SMOOTH>(
        kspace<FFTW, SMOOTH> *);

template int compute_gradient<float, FFTW, THREE, THREExTHREE, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<float, FFTW, THREE> *,
        field<float, FFTW, THREExTHREE> *);
template int compute_gradient<double, FFTW, THREE, THREExTHREE, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<double, FFTW, THREE> *,
        field<double, FFTW, THREExTHREE> *);

template int compute_gradient<float, FFTW, ONE, THREE, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<float, FFTW, ONE> *,
        field<float, FFTW, THREE> *);
template int compute_gradient<double, FFTW, ONE, THREE, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<double, FFTW, ONE> *,
        field<double, FFTW, THREE> *);

template int compute_divergence(
        kspace<FFTW, SMOOTH> *,
        field<float, FFTW, THREE> *,
        field<float, FFTW, ONE> *);
template int compute_divergence(
        kspace<FFTW, SMOOTH> *,
        field<double, FFTW, THREE> *,
        field<double, FFTW, ONE> *);

template int compute_curl<float, FFTW, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<float, FFTW, THREE> *,
        field<float, FFTW, THREE> *);
template int compute_curl<double, FFTW, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<double, FFTW, THREE> *,
        field<double, FFTW, THREE> *);

template int invert_curl<float, FFTW, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<float, FFTW, THREE> *,
        field<float, FFTW, THREE> *);
template int invert_curl<double, FFTW, SMOOTH>(
        kspace<FFTW, SMOOTH> *,
        field<double, FFTW, THREE> *,
        field<double, FFTW, THREE> *);

template int joint_rspace_PDF<float, FFTW, THREE>(
        field<float, FFTW, THREE> *,
        field<float, FFTW, THREE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const std::vector<double>);
template int joint_rspace_PDF<double, FFTW, THREE>(
        field<double, FFTW, THREE> *,
        field<double, FFTW, THREE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const std::vector<double>);

template int joint_rspace_PDF<float, FFTW, ONE>(
        field<float, FFTW, ONE> *,
        field<float, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const std::vector<double>);
template int joint_rspace_PDF<double, FFTW, ONE>(
        field<double, FFTW, ONE> *,
        field<double, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const std::vector<double>);

template int conditional_rspace_PDF<float, FFTW, THREExTHREE>(
        field<float, FFTW, THREExTHREE> *,
        field<float, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const double);
template int conditional_rspace_PDF<double, FFTW, THREExTHREE>(
        field<double, FFTW, THREExTHREE> *,
        field<double, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const double);

template int conditional_rspace_PDF<float, FFTW, THREE>(
        field<float, FFTW, THREE> *,
        field<float, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const double);
template int conditional_rspace_PDF<double, FFTW, THREE>(
        field<double, FFTW, THREE> *,
        field<double, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const double);

template int conditional_rspace_PDF<float, FFTW, ONE>(
        field<float, FFTW, ONE> *,
        field<float, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const double);
template int conditional_rspace_PDF<double, FFTW, ONE>(
        field<double, FFTW, ONE> *,
        field<double, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const double);

template int joint_rspace_3PDF<float, FFTW>(
        field<float, FFTW, ONE> *,
        field<float, FFTW, ONE> *,
        field<float, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const std::vector<double>,
        const std::vector<double>);
template int joint_rspace_3PDF<double, FFTW>(
        field<double, FFTW, ONE> *,
        field<double, FFTW, ONE> *,
        field<double, FFTW, ONE> *,
        const hid_t,
        const std::string,
        const hsize_t,
        const std::vector<double>,
        const std::vector<double>,
        const std::vector<double>);


template int make_gaussian_random_field<
        float,
        FFTW,
        ONE,
        SMOOTH>(
        kspace<FFTW, SMOOTH> *kk,
        field<float, FFTW, ONE> *output_field,
        const int rseed,
        const double dissipation,
        const double Lint,
        const double etaK,
        const double c_L,
        const double c_eta,
        const double coefficient);
template int make_gaussian_random_field<
        double,
        FFTW,
        ONE,
        SMOOTH>(
        kspace<FFTW, SMOOTH> *kk,
        field<double, FFTW, ONE> *output_field,
        const int rseed,
        const double dissipation,
        const double Lint,
        const double etaK,
        const double c_L,
        const double c_eta,
        const double coefficient);
template int make_gaussian_random_field<
        float,
        FFTW,
        THREE,
        SMOOTH>(
        kspace<FFTW, SMOOTH> *kk,
        field<float, FFTW, THREE> *output_field,
        const int rseed,
        const double dissipation,
        const double Lint,
        const double etaK,
        const double c_L,
        const double c_eta,
        const double coefficient);
template int make_gaussian_random_field<
        double,
        FFTW,
        THREE,
        SMOOTH>(
        kspace<FFTW, SMOOTH> *kk,
        field<double, FFTW, THREE> *output_field,
        const int rseed,
        const double dissipation,
        const double Lint,
        const double etaK,
        const double c_L,
        const double c_eta,
        const double coefficient);

template int apply_phase_field_shift<float,
          FFTW,
          ONE,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<float, FFTW, ONE> *output_field,
        field<float, FFTW, ONE> *phase_field);

template int apply_phase_field_shift<float,
          FFTW,
          THREE,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<float, FFTW, THREE> *output_field,
        field<float, FFTW, ONE> *phase_field);

template int apply_phase_field_shift<float,
          FFTW,
          THREExTHREE,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<float, FFTW, THREExTHREE> *output_field,
        field<float, FFTW, ONE> *phase_field);

template int apply_phase_field_shift<double,
          FFTW,
          ONE,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<double, FFTW, ONE> *output_field,
        field<double, FFTW, ONE> *phase_field);

template int apply_phase_field_shift<double,
          FFTW,
          THREE,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<double, FFTW, THREE> *output_field,
        field<double, FFTW, ONE> *phase_field);

template int apply_phase_field_shift<double,
          FFTW,
          THREExTHREE,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<double, FFTW, THREExTHREE> *output_field,
        field<double, FFTW, ONE> *phase_field);


template int generate_random_phase_field<float,
          FFTW,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<float, FFTW, ONE> *output_field,
        const int rseed);

template int generate_random_phase_field<double,
          FFTW,
          SMOOTH>
(
        kspace<FFTW, SMOOTH> *kk,
        field<double, FFTW, ONE> *output_field,
        const int rseed);
