/**********************************************************************
*                                                                     *
*  Copyright 2015 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/


#ifndef BASE_HPP

#define BASE_HPP


#include "turtle_config.hpp"

#include <cassert>
#include <mpi.h>
#include <stdarg.h>
#include <typeinfo>
#include <cstdlib>

#ifndef AssertMpi
#define AssertMpi(X) if(MPI_SUCCESS != (X)) { printf("MPI Error at line %d\n",__LINE__); fflush(stdout) ; throw std::runtime_error("Stop from from mpi error"); }
#endif

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

static const int message_buffer_length = 32768;
extern int maximum_MPI_tag_value;
extern int myrank, nprocs;

inline int MOD(int a, int n)
{
    return (a > 0) ? (a%n) : (a%n + n);
}

template <typename T> int sgn(T val)
{
        return (T(0) < val) - (val < T(0));
}

int read_maximum_MPI_tag_value();

inline int normalizeMPITag(const int mpi_tag_input)
{
    // some environments need the tag to be small
    // see https://www.intel.com/content/www/us/en/developer/articles/technical/large-mpi-tags-with-the-intel-mpi.html
    // TODO: is there a better way of achieving this result?
    return MOD(mpi_tag_input, maximum_MPI_tag_value);
}

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

namespace turtle_mpi_pcontrol{
    const int FFTW = 5;
    const int FIELD = 6;
    const int PARTICLES = 7;
    const int HDF5 = 8;
}

#ifdef USE_TIMING_OUTPUT
inline int start_mpi_profiling_zone(const int zone_name)
{
    assert((zone_name == turtle_mpi_pcontrol::FFTW) ||
           (zone_name == turtle_mpi_pcontrol::FIELD) ||
           (zone_name == turtle_mpi_pcontrol::PARTICLES) ||
           (zone_name == turtle_mpi_pcontrol::HDF5));
    return MPI_Pcontrol(zone_name);
}
inline int finish_mpi_profiling_zone(const int zone_name)
{
    assert((zone_name == turtle_mpi_pcontrol::FFTW) ||
           (zone_name == turtle_mpi_pcontrol::FIELD) ||
           (zone_name == turtle_mpi_pcontrol::PARTICLES) ||
           (zone_name == turtle_mpi_pcontrol::HDF5));
    return MPI_Pcontrol(-zone_name);
}
#else
inline int start_mpi_profiling_zone(const int zone_name)
{
    return EXIT_SUCCESS;
}
inline int finish_mpi_profiling_zone(const int zone_name)
{
    return EXIT_SUCCESS;
}
#endif//USE_TIMING_OUTPUT

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

#ifdef OMPI_MPI_H

#define TURTLE_MPICXX_DOUBLE_COMPLEX MPI_DOUBLE_COMPLEX

#else

#define TURTLE_MPICXX_DOUBLE_COMPLEX MPI_C_DOUBLE_COMPLEX

#endif//OMPI_MPI_H

template <class realtype>
class mpi_real_type;

template <>
class mpi_real_type<float>
{
public:
    static constexpr MPI_Datatype real(){
        return MPI_FLOAT;
    }

    static constexpr MPI_Datatype complex(){
        return MPI_COMPLEX;
    }
};

template <>
class mpi_real_type<double>
{
public:
    static constexpr MPI_Datatype real(){
        return MPI_DOUBLE;
    }

    static constexpr MPI_Datatype complex(){
        return TURTLE_MPICXX_DOUBLE_COMPLEX;
    }
};

template <>
class mpi_real_type<int>
{
    public:
        static constexpr MPI_Datatype real(){
            return MPI_INT;
        }
};

template <>
class mpi_real_type<long int>
{
    public:
        static constexpr MPI_Datatype real(){
            return MPI_LONG;
        }
};

template <>
class mpi_real_type<long long int>
{
    public:
        static constexpr MPI_Datatype real(){
            return MPI_LONG_LONG;
        }
};

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

#ifdef TURTLE_DEBUG_MESSAGES_ON

#include <iostream>

inline void DEBUG_MSG(const char * format, ...)
{
    va_list argptr;
    va_start(argptr, format);
    static char debug_message_buffer[message_buffer_length];
    sprintf(
            debug_message_buffer,
            "MPIrank%.4d ",
            myrank);
    vsnprintf(
            debug_message_buffer + 12,
            message_buffer_length - 12,
            format,
            argptr);
    va_end(argptr);
    std::cerr << debug_message_buffer;
}

inline void DEBUG_MSG_WAIT(MPI_Comm communicator, const char * format, ...)
{
    va_list argptr;
    va_start(argptr, format);
    static char debug_message_buffer[message_buffer_length];
    sprintf(
            debug_message_buffer,
            "MPIrank%.4d ",
            myrank);
    vsnprintf(
            debug_message_buffer + 12,
            message_buffer_length - 12,
            format,
            argptr);
    va_end(argptr);
    std::cerr << debug_message_buffer;
    MPI_Barrier(communicator);
}

#else

#define DEBUG_MSG(...)
#define DEBUG_MSG_WAIT(...)

#endif//TURTLE_DEBUG_MESSAGES_ON

#define variable_used_only_in_assert(x) ((void)(x))

#endif//BASE_HPP

