#include "hdf5_tools.hpp"
#include "scope_timer.hpp"

#include <sys/stat.h>

template <> hid_t hdf5_tools::hdf5_type_id<char>()
{
    return H5T_NATIVE_CHAR;
}
template <> hid_t hdf5_tools::hdf5_type_id<signed char>()
{
    return H5T_NATIVE_SCHAR;
}
template <> hid_t hdf5_tools::hdf5_type_id<unsigned char>()
{
    return H5T_NATIVE_UCHAR;
}
template <> hid_t hdf5_tools::hdf5_type_id<short>()
{
    return H5T_NATIVE_SHORT;
}
template <> hid_t hdf5_tools::hdf5_type_id<unsigned short>()
{
    return H5T_NATIVE_USHORT;
}
template <> hid_t hdf5_tools::hdf5_type_id<int>()
{
    return H5T_NATIVE_INT;
}
template <> hid_t hdf5_tools::hdf5_type_id<unsigned>()
{
    return H5T_NATIVE_UINT;
}
template <> hid_t hdf5_tools::hdf5_type_id<long>()
{
    return H5T_NATIVE_LONG;
}
template <> hid_t hdf5_tools::hdf5_type_id<unsigned long>()
{
    return H5T_NATIVE_ULONG;
}
template <> hid_t hdf5_tools::hdf5_type_id<long long>()
{
    return H5T_NATIVE_LLONG;
}
template <> hid_t hdf5_tools::hdf5_type_id<unsigned long long>()
{
    return H5T_NATIVE_ULLONG;
}
template <> hid_t hdf5_tools::hdf5_type_id<float>()
{
    return H5T_NATIVE_FLOAT;
}
template <> hid_t hdf5_tools::hdf5_type_id<double>()
{
    return H5T_NATIVE_DOUBLE;
}
template <> hid_t hdf5_tools::hdf5_type_id<long double>()
{
    return H5T_NATIVE_LDOUBLE;
}

int hdf5_tools::require_size_single_dataset(hid_t dset, int tsize)
{
    TIMEZONE("hdf5_tools::require_size_single_dataset");
    int ndims;
    hid_t space;
    space = H5Dget_space(dset);
    ndims = H5Sget_simple_extent_ndims(space);
    hsize_t *dims = new hsize_t[ndims];
    hsize_t *maxdims = new hsize_t[ndims];
    H5Sget_simple_extent_dims(space, dims, maxdims);
    if (dims[0] < hsize_t(tsize) && maxdims[0] == H5S_UNLIMITED)
    {
        dims[0] = tsize;
        H5Dset_extent(dset, dims);
    }
    H5Sclose(space);
    delete[] maxdims;
    delete[] dims;
    return EXIT_SUCCESS;
}

int hdf5_tools::grow_single_dataset(hid_t dset, int tincrement)
{
    TIMEZONE("hdf5_tools::grow_single_dataset");
    int ndims;
    hid_t space;
    space = H5Dget_space(dset);
    ndims = H5Sget_simple_extent_ndims(space);
    hsize_t *dims = new hsize_t[ndims];
    hsize_t *maxdims = new hsize_t[ndims];
    H5Sget_simple_extent_dims(space, dims, maxdims);
    if (maxdims[0] == H5S_UNLIMITED)
    {
        dims[0] += tincrement;
        H5Dset_extent(dset, dims);
    }
    H5Sclose(space);
    delete[] maxdims;
    delete[] dims;
    return EXIT_SUCCESS;
}

herr_t hdf5_tools::require_size_dataset_visitor(
    hid_t o_id,
    const char *name,
    const H5O_info_t *info,
    void *op_data)
{
    TIMEZONE("hdf5_tools::require_size_dataset_visitor");
    if (info->type == H5O_TYPE_DATASET)
    {
        hid_t dset = H5Dopen(o_id, name, H5P_DEFAULT);
        require_size_single_dataset(dset, *((int*)(op_data)));
        H5Dclose(dset);
    }
    return EXIT_SUCCESS;
}

herr_t hdf5_tools::grow_dataset_visitor(
    hid_t o_id,
    const char *name,
    const H5O_info_t *info,
    void *op_data)
{
    TIMEZONE("hdf5_tools::grow_dataset_visitor");
    if (info->type == H5O_TYPE_DATASET)
    {
        hid_t dset = H5Dopen(o_id, name, H5P_DEFAULT);
        grow_single_dataset(dset, *((int*)(op_data)));
        H5Dclose(dset);
    }
    return EXIT_SUCCESS;
}


int hdf5_tools::grow_file_datasets(
        const hid_t stat_file,
        const std::string group_name,
        int tincrement)
{
    TIMEZONE("hdf5_tools::grow_file_datasets");
    int file_problems = 0;

    hid_t group;
    group = H5Gopen(stat_file, group_name.c_str(), H5P_DEFAULT);
    // in the following there's ugly code because of the HDF group
    // see https://portal.hdfgroup.org/display/HDF5/H5O_VISIT (valid link on 2021-12-12)
#if H5_VERSION_GE(1, 12, 0) // https://portal.hdfgroup.org/display/HDF5/H5_VERSION_GE
        H5Ovisit(
            group,
            H5_INDEX_NAME,
            H5_ITER_NATIVE,
            grow_dataset_visitor,
            &tincrement,
            H5O_INFO_ALL);
#else
        H5Ovisit(
            group,
            H5_INDEX_NAME,
            H5_ITER_NATIVE,
            grow_dataset_visitor,
            &tincrement);
#endif//H5_VERSION_GE
    H5Gclose(group);
    return file_problems;
}


int hdf5_tools::require_size_file_datasets(
        const hid_t stat_file,
        const std::string group_name,
        int tsize)
{
    TIMEZONE("hdf5_tools::require_size_file_datasets");
    int file_problems = 0;

    hid_t group;
    group = H5Gopen(stat_file, group_name.c_str(), H5P_DEFAULT);
#if H5_VERSION_GE(1, 12, 0)
    H5Ovisit(
            group,
            H5_INDEX_NAME,
            H5_ITER_NATIVE,
            require_size_dataset_visitor,
            &tsize,
            H5O_INFO_ALL);
#else
    H5Ovisit(
            group,
            H5_INDEX_NAME,
            H5_ITER_NATIVE,
            require_size_dataset_visitor,
            &tsize);
#endif//H5_VERSION_GE
    H5Gclose(group);
    return file_problems;
}


herr_t hdf5_tools::list_dataset_visitor(
    hid_t o_id,
    const char *name,
    const H5O_info_t *info,
    void *op_data)
{
    if (info->type == H5O_TYPE_DATASET)
    {
        std::cerr << name << std::endl;
    }
    return EXIT_SUCCESS;
}

int hdf5_tools::list_file_datasets(
        const hid_t stat_file,
        const std::string group_name)
{
    TIMEZONE("hdf5_tools::list_file_datasets");
    int file_problems = 0;

    hid_t group;
    group = H5Gopen(stat_file, group_name.c_str(), H5P_DEFAULT);
#if H5_VERSION_GE(1, 12, 0)
    H5Ovisit(
            group,
            H5_INDEX_NAME,
            H5_ITER_NATIVE,
            list_dataset_visitor,
            nullptr,
            H5O_INFO_ALL);
#else
    H5Ovisit(
            group,
            H5_INDEX_NAME,
            H5_ITER_NATIVE,
            list_dataset_visitor,
            nullptr);
#endif//H5_VERSION_GE
    H5Gclose(group);
    return file_problems;
}

template <typename number>
std::vector<number> hdf5_tools::read_vector(
        const hid_t group,
        const std::string dset_name)
{
    TIMEZONE("hdf5_tools::read_vector");
    DEBUG_MSG("hdf5_tools::read_vector dset_name = %s\n", dset_name.c_str());
    std::vector<number> result;
    hsize_t vector_length;
    // first, read size of array
    hid_t dset, dspace;
    hid_t mem_dtype = H5Tcopy(hdf5_tools::hdf5_type_id<number>());
    dset = H5Dopen(group, dset_name.c_str(), H5P_DEFAULT);
    if (dset < 0)
    {
        DEBUG_MSG("had an error trying to open %s.\n H5Dopen returned dset = %ld\n", dset_name.c_str(), dset);
    }
    dspace = H5Dget_space(dset);
    assert(H5Sget_simple_extent_ndims(dspace) == 1);
    H5Sget_simple_extent_dims(dspace, &vector_length, NULL);
    result.resize(vector_length);
    herr_t status = H5Dread(dset, mem_dtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, &result.front());
    if (status < 0)
    {
        DEBUG_MSG("had an error trying to read %s.\n H5Dread returned status %ld\n", dset_name.c_str(), status);
    }
    H5Sclose(dspace);
    H5Dclose(dset);
    H5Tclose(mem_dtype);
    return result;
}

template <typename number>
number hdf5_tools::read_value(
        const hid_t group,
        const std::string dset_name)
{
    TIMEZONE("hdf5_tools::read_value");
    number result;
    hid_t dset;
    hid_t mem_dtype = H5Tcopy(hdf5_tools::hdf5_type_id<number>());
    if (H5Lexists(group, dset_name.c_str(), H5P_DEFAULT))
    {
        dset = H5Dopen(group, dset_name.c_str(), H5P_DEFAULT);
        herr_t status = H5Dread(dset, mem_dtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, &result);
        if (status < 0)
        {
            DEBUG_MSG("had an error trying to read %s.\n H5Dread returned status %ld\n", dset_name.c_str(), status);
        }
        H5Dclose(dset);
    }
    else
    {
        DEBUG_MSG("attempted to read dataset %s which does not exist.\n",
                dset_name.c_str());
        result = std::numeric_limits<number>::max();
    }
    H5Tclose(mem_dtype);
    return result;
}

template <typename number>
int hdf5_tools::write_value_with_single_rank(
        const hid_t group,
        const std::string dset_name,
        const number value)
{
    assert(typeid(number) == typeid(int) ||
           typeid(number) == typeid(double));
    hid_t dset, mem_dtype;
    mem_dtype = H5Tcopy(hdf5_tools::hdf5_type_id<number>());
    if (H5Lexists(group, dset_name.c_str(), H5P_DEFAULT))
    {
        dset = H5Dopen(group, dset_name.c_str(), H5P_DEFAULT);
        assert(dset > 0);
    }
    else
    {
        hid_t fspace;
        hsize_t count[1];
        count[0] = 1;
        fspace = H5Screate_simple(1, count, NULL);
        assert(fspace > 0);
        dset = H5Dcreate(group, dset_name.c_str(), mem_dtype, fspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        assert(dset > 0);
        H5Sclose(fspace);
    }
    H5Dwrite(dset, mem_dtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, &value);
    H5Dclose(dset);
    H5Tclose(mem_dtype);
    return EXIT_SUCCESS;
}

template <typename rnumber>
int hdf5_tools::update_time_series_with_single_rank(
        const hid_t group,
        const std::string dset_name,
        const hsize_t toffset,
        const hsize_t nvalues,
        const rnumber *data)
{
    TIMEZONE("hdf5_tools::update_time_series_with_single_rank");
    hid_t dset = H5Dopen(group, dset_name.c_str(), H5P_DEFAULT);
    hid_t wspace = H5Dget_space(dset);
    int ndims = H5Sget_simple_extent_ndims(wspace);
    hsize_t dims[ndims];
    H5Sget_simple_extent_dims(wspace, dims, NULL);

    int file_nvalues = -1;
    variable_used_only_in_assert(file_nvalues);
    switch(ndims)
    {
        case 1:
            file_nvalues = 1;
            break;
        case 2:
            file_nvalues = dims[1];
            break;
        case 3:
            file_nvalues = dims[1]*dims[2];
            break;
    }
    assert(file_nvalues == int(nvalues));

    hsize_t count[ndims], offset[ndims];

    offset[0] = toffset;
    count[0] = 1;
    for (auto ii=1; ii < ndims; ii++)
    {
        offset[ii] = 0;
        count[ii] = dims[ii];
    }

    hid_t mspace = H5Screate_simple(ndims, count, NULL);

    H5Sselect_hyperslab(wspace, H5S_SELECT_SET, offset, NULL, count, NULL);
    H5Dwrite(
            dset,
            hdf5_tools::hdf5_type_id<rnumber>(),
            mspace,
            wspace,
            H5P_DEFAULT,
            data);

    H5Sclose(mspace);
    H5Sclose(wspace);
    H5Dclose(dset);
    return EXIT_SUCCESS;
}

template <typename dtype>
std::vector<dtype> hdf5_tools::read_vector_with_single_rank(
        const int myrank,
        const int rank_to_use,
        const MPI_Comm COMM,
        const hid_t file_id,
        const std::string dset_name)
{
    TIMEZONE("hdf5_tools::read_vector_with_single_rank");
    std::vector<dtype> data;
    int vector_size;
    if (myrank == rank_to_use)
    {
        data = hdf5_tools::read_vector<dtype>(
                file_id,
                dset_name);
        vector_size = data.size();
    }
    MPI_Bcast(
            &vector_size,
            1,
            MPI_INT,
            rank_to_use,
            COMM);

    if (myrank != rank_to_use)
        data.resize(vector_size);
    MPI_Bcast(
            &data.front(),
            vector_size,
            (typeid(dtype) == typeid(int)) ? MPI_INT : MPI_DOUBLE,
            rank_to_use,
            COMM);
    return data;
}

std::string hdf5_tools::read_string(
        const hid_t group,
        const std::string dset_name)
{
    TIMEZONE("hdf5_tools::read_string");
    if (H5Lexists(group, dset_name.c_str(), H5P_DEFAULT))
    {
        hid_t dset = H5Dopen(group, dset_name.c_str(), H5P_DEFAULT);
        hid_t space = H5Dget_space(dset);
        hid_t memtype = H5Dget_type(dset);
        // fsanitize complains unless I have a static array here
        // but that doesn't actually work (data is read incorrectly).
        // this is caught by TurTLE.test_NSVEparticles
        char *string_data = (char*)malloc(256);
        H5Dread(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, &string_data);
        std::string std_string_data = std::string(string_data);
        free(string_data);
        H5Sclose(space);
        H5Tclose(memtype);
        H5Dclose(dset);
        return std_string_data;
    }
    else
    {
        DEBUG_MSG("attempted to read dataset %s which does not exist.\n",
                dset_name.c_str());
        return std::string("parameter does not exist");
    }
}

template <class partsize_t>
int hdf5_tools::write_particle_ID_pairs_with_single_rank(
        const std::vector<partsize_t> v,
        const hid_t group,
        const std::string dset_name)
{
    TIMEZONE("hdf5_tools::write_particle_ID_pairs_with_single_rank");
    // the vector contains pair information, so its size must be a multiple of 2
    assert((v.size() % 2) == 0);
    // file space creation
    hid_t fspace;
    hsize_t dims[2];
    dims[0] = v.size()/2;
    dims[1] = 2;
    fspace = H5Screate_simple(2, dims, NULL);
    assert(fspace > 0);
    // create dataset
    hid_t dset_id = H5Dcreate(
            group,
            dset_name.c_str(),
            hdf5_tools::hdf5_type_id<partsize_t>(),
            fspace,
            H5P_DEFAULT,
            H5P_DEFAULT,
            H5P_DEFAULT);
    if (dset_id < 0)
    {
        DEBUG_MSG("could not create dataset %s in group %d\n",
                dset_name.c_str(),
                group);
    }
    assert(dset_id > 0);
    // write data
    H5Dwrite(
            dset_id,
            hdf5_tools::hdf5_type_id<partsize_t>(),
            H5S_ALL,
            H5S_ALL,
            H5P_DEFAULT,
            &v.front());
    // clean up
    H5Dclose(dset_id);
    H5Sclose(fspace);
    return EXIT_SUCCESS;
}

template <class real_number>
int hdf5_tools::write_vector_with_single_rank(
        const std::vector<real_number> v,
        const hid_t group,
        const std::string dset_name)
{
    TIMEZONE("hdf5_tools::write_vector_with_single_rank");
    // file space creation
    hid_t fspace;
    hsize_t dim[1];
    dim[0] = v.size();
    fspace = H5Screate_simple(1, dim, NULL);
    // create dataset
    hid_t dset_id = H5Dcreate(
            group,
            dset_name.c_str(),
            hdf5_tools::hdf5_type_id<real_number>(),
            fspace,
            H5P_DEFAULT,
            H5P_DEFAULT,
            H5P_DEFAULT);
    assert(dset_id > 0);
    // write data
    H5Dwrite(
            dset_id,
            hdf5_tools::hdf5_type_id<real_number>(),
            H5S_ALL,
            H5S_ALL,
            H5P_DEFAULT,
            &v.front());
    // clean up
    H5Dclose(dset_id);
    H5Sclose(fspace);
    return EXIT_SUCCESS;
}

template <typename number>
int hdf5_tools::gather_and_write_with_single_rank(
        const int myrank,
        const int rank_to_use,
        const MPI_Comm COMM,
        const number *data,
        const int number_of_items,
        const hid_t group,
        const std::string dset_name)
{
    TIMEZONE("hdf5_tools::gather_and_write_with_single_rank");
    int nprocs;
    MPI_Comm_size(COMM, &nprocs);
    // first get number of items for each rank:
    int *recvcounts = NULL;
    if (myrank == rank_to_use)
        recvcounts = new int[nprocs];
    MPI_Gather(
                &number_of_items,
                1,
                MPI_INT,
                recvcounts,
                1,
                MPI_INT,
                rank_to_use,
                COMM);
    // now prepare to gather data on rank_to_use
    int total_number_of_items = 0;
    int *displacement = NULL;
    if (myrank == rank_to_use)
    {
        displacement = new int[nprocs];
        displacement[0] = 0;
        for(int ii = 1; ii < nprocs; ii++)
        {
            displacement[ii] = displacement[ii-1] + recvcounts[ii-1];
        }
        total_number_of_items = displacement[nprocs-1] + recvcounts[nprocs-1];
    }
    std::vector<number> data_to_write;
    if (myrank == rank_to_use)
        data_to_write.resize(total_number_of_items);
    MPI_Gatherv(
                data,
                number_of_items,
                mpi_real_type<number>::real(),
                &data_to_write.front(),
                recvcounts,
                displacement,
                mpi_real_type<number>::real(),
                rank_to_use,
                COMM);
    if (myrank == rank_to_use)
    {
        write_vector_with_single_rank<number>(
            data_to_write,
            group,
            dset_name);
        delete[] recvcounts;
        delete[] displacement;
    }
    return EXIT_SUCCESS;
}

bool hdf5_tools::field_exists(
            const std::string file_name,
            const std::string field_name,
            const std::string representation,
            const int iteration)
{
    /* file dataset has same dimensions as field */
    TIMEZONE("hdf5_tools::field_exists");
    hid_t file_id = H5I_BADID;
    bool all_good = true;

    std::string dset_name = (
            "/" + field_name +
            "/" + representation +
            "/" + std::to_string(iteration));

    /* open/create file */
    bool file_exists = false;
    {
        struct stat file_buffer;
        file_exists = (stat(file_name.c_str(), &file_buffer) == 0);
    }
    all_good = all_good && file_exists;
    if (not file_exists)
        return false;
    file_id = H5Fopen(file_name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    all_good = all_good && (file_id >= 0);
    if (file_id >= 0) {
        all_good = all_good && (H5Lexists(file_id, dset_name.c_str(), H5P_DEFAULT));
    }

    if (file_id >= 0)
        H5Fclose(file_id);
    return all_good;
}

template
std::vector<int> hdf5_tools::read_vector<int>(
        const hid_t,
        const std::string);

template
std::vector<double> hdf5_tools::read_vector<double>(
        const hid_t,
        const std::string);

template
std::vector<int> hdf5_tools::read_vector_with_single_rank<int>(
        const int myrank,
        const int rank_to_use,
        const MPI_Comm COMM,
        const hid_t file_id,
        const std::string dset_name);

template
std::vector<double> hdf5_tools::read_vector_with_single_rank<double>(
        const int myrank,
        const int rank_to_use,
        const MPI_Comm COMM,
        const hid_t file_id,
        const std::string dset_name);

template
int hdf5_tools::read_value<int>(
        const hid_t,
        const std::string);

template
long long int hdf5_tools::read_value<long long int>(
        const hid_t,
        const std::string);

template
double hdf5_tools::read_value<double>(
        const hid_t,
        const std::string);

template
int hdf5_tools::write_value_with_single_rank<int>(
        const hid_t group,
        const std::string dset_name,
        const int value);

template
int hdf5_tools::write_value_with_single_rank<double>(
        const hid_t group,
        const std::string dset_name,
        const double value);

template
int hdf5_tools::update_time_series_with_single_rank<double>(
        const hid_t group,
        const std::string dset_name,
        const hsize_t toffset,
        const hsize_t nvalues,
        const double *data);

template
int hdf5_tools::write_particle_ID_pairs_with_single_rank(
        const std::vector<long long> v,
        const hid_t group,
        const std::string dset_name);

template
int hdf5_tools::write_vector_with_single_rank(
        const std::vector<double> v,
        const hid_t group,
        const std::string dset_name);

template
int hdf5_tools::write_vector_with_single_rank(
        const std::vector<long long int> v,
        const hid_t group,
        const std::string dset_name);

template
int hdf5_tools::gather_and_write_with_single_rank<double>(
        const int myrank,
        const int rank_to_use,
        const MPI_Comm COMM,
        const double *data,
        const int number_of_items,
        const hid_t group,
        const std::string dset_name);

template
int hdf5_tools::gather_and_write_with_single_rank<long long int>(
        const int myrank,
        const int rank_to_use,
        const MPI_Comm COMM,
        const long long int *data,
        const int number_of_items,
        const hid_t group,
        const std::string dset_name);

