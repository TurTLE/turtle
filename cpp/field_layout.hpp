/**********************************************************************
*                                                                     *
*  Copyright 2015 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@ds.mpg.de                                 *
*                                                                     *
**********************************************************************/



#ifndef FIELD_LAYOUT_HPP

#define FIELD_LAYOUT_HPP

#include "base.hpp"

#include <vector>
#include <hdf5.h>

enum field_components {ONE, THREE, THREExTHREE};

constexpr unsigned int ncomp(
        field_components fc)
    /* return actual number of field components for each enum value */
{
    return ((fc == THREE) ? 3 : (
            (fc == THREExTHREE) ? 9 : 1));
}

constexpr unsigned int ndim(
        field_components fc)
    /* return actual number of field dimensions for each enum value */
{
    return ((fc == THREE) ? 4 : (
            (fc == THREExTHREE) ? 5 : 3));
}

class abstract_field_layout
{
    public:
        virtual ~abstract_field_layout() noexcept(false){}

        virtual MPI_Comm getMPIComm() const = 0;
        virtual int getMPIRank() const = 0;

        virtual hsize_t getSize(int component) const = 0;
        virtual hsize_t getSubSize(int component) const = 0;
        virtual hsize_t getStart(int component) const = 0;
};

template <field_components fc>
class field_layout: public abstract_field_layout
{
    public:
        /* description */
        hsize_t sizes[ndim(fc)];
        hsize_t subsizes[ndim(fc)];
        hsize_t starts[ndim(fc)];
        hsize_t local_size, full_size;

        int myrank, nprocs;
        MPI_Comm comm;

        std::vector<std::vector<int>> rank;
        std::vector<std::vector<int>> all_start;
        std::vector<std::vector<int>> all_size;

        /* methods */
        field_layout(
                const hsize_t *SIZES,
                const hsize_t *SUBSIZES,
                const hsize_t *STARTS,
                const MPI_Comm COMM_TO_USE);
        ~field_layout() noexcept(false){}

        MPI_Comm getMPIComm() const
        {
            return this->comm;
        }
        int getMPIRank() const
        {
            return this->myrank;
        }
        hsize_t getSize(int component) const
        {
            assert(component >= 0 && component < int(ndim(fc)));
            return this->sizes[component];
        }
        hsize_t getSubSize(int component) const
        {
            assert(component >= 0 && component < int(ndim(fc)));
            return this->subsizes[component];
        }
        hsize_t getStart(int component) const
        {
            assert(component >= 0 && component < int(ndim(fc)));
            return this->starts[component];
        }
};

#endif//FIELD_LAYOUT_HPP

