/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/
#ifndef P2P_GHOST_COLLISIONS_HPP
#define P2P_GHOST_COLLISIONS_HPP

#include <cstring>
#include <utility>
#include <vector>
#include <cmath>


template <class partsize_t>
void print_pair_vec(std::vector<partsize_t> vec)
{
    assert(vec.size() % 2 == 0);
    DEBUG_MSG("start print_pair_vec\n");
    for (int i = 0; i < int(vec.size())/2; i++)
        DEBUG_MSG("pair %d is (%ld,%ld)\n", i, vec[2*i], vec[2*i+1]);
    DEBUG_MSG("end print_pair_vec\n");
}


template <class real_number, class partsize_t>
class p2p_ghost_collisions
{
    protected:
        const double pi  = atan(1.0)*4;
        const double pi2 = atan(1.0)*8;

        enum particle_shape {CYLINDER, SPHERE, DISK};

    private:
        bool isActive;

        bool synchronisation;

        // description for cylinders:
        double cylinder_width;
        double cylinder_length;

        // description for disks:
        double disk_width;

        // depending on the particle shape, we will be deciding whether particles intersect in different ways
        particle_shape current_particle_shape;

    protected:
        /** \brief Adds pair of colliding particle to list
         *
         * Given a pair of particle IDs, add them as an *ordered pair* to `collision_pairs_local`.
         *
         */
        void add_colliding_pair(partsize_t idx_part1, partsize_t idx_part2)
        {
            // store colliding particle ids in order, to be able to identify pairs more easily
            assert(idx_part1!=idx_part2);
            partsize_t idx_part_small = idx_part1;
            partsize_t idx_part_big = idx_part2;
            if (idx_part1 > idx_part2)
            {
                idx_part_small = idx_part2;
                idx_part_big = idx_part1;
            }
            this->collision_pairs_local.push_back(idx_part_small);
            this->collision_pairs_local.push_back(idx_part_big);
        }

        std::vector <partsize_t> collision_pairs_local;
        std::vector <partsize_t> collision_pairs_global;

public:
    p2p_ghost_collisions():
        isActive(true),
        synchronisation(false),
        cylinder_width(1.0),
        cylinder_length(1.0),
        disk_width(1.0),
        current_particle_shape(SPHERE) {}

    void copy_from_p2p_ghost_collisions(const p2p_ghost_collisions<real_number, partsize_t>& src)
    {
        this->current_particle_shape = src.current_particle_shape;
        this->cylinder_width = src.cylinder_width;
        this->cylinder_length = src.cylinder_length;
        this->disk_width = src.disk_width;
        this->isActive = src.isActive;
        this->synchronisation = src.synchronisation;
        this->collision_pairs_local.reserve(src.collision_pairs_local.capacity());
    }

    // Copy constructor use a counter set to zero
    p2p_ghost_collisions(const p2p_ghost_collisions<real_number, partsize_t>& src)
    {
        this->copy_from_p2p_ghost_collisions(src);
    }

    double rod_distance(double dist_pow2, double xp, double xq, double pq, double t, double s){
        double min_distance;
        min_distance = dist_pow2 + 0.25*this->get_cylinder_length()*this->get_cylinder_length() * ( t*t + s*s - 2.0*t*s*pq ) + this->get_cylinder_length() * ( t*xp - s*xq );
        min_distance = std::sqrt( min_distance );
        return min_distance;
    }

    template <int size_particle_rhs>
    void reduce_particles_rhs(real_number /*rhs_dst*/[], const real_number /*rhs_src*/[], const partsize_t /*nbParticles*/) const{
    }

    void merge(const p2p_ghost_collisions& other){
        collision_pairs_local.insert(
                collision_pairs_local.end(),
                other.collision_pairs_local.begin(),
                other.collision_pairs_local.end());
    }

    void MPI_merge(const MPI_Comm comm, const int myrank, const int nprocs) {
        ///collect collision pairs
        DEBUG_MSG("p2p_ghost_collisions::MPI_merge\n");
        int* recvcounts = NULL;
        int* displ = NULL;
        const int vec_len_local = collision_pairs_local.size();
        if (myrank == 0)
        {
            recvcounts = new int[nprocs];
        }
        MPI_Gather(&vec_len_local, 1, MPI_INT,
               recvcounts, 1, MPI_INT,
               0, comm);
        if (myrank == 0)
        {
            int nbElements;
            displ = new int[nprocs];
            displ[0]=0;
            for (int i=1; i<nprocs; i++)
            {
                displ[i]=displ[i-1]+recvcounts[i-1];
            }
            nbElements = displ[nprocs-1] + recvcounts[nprocs-1];
            this->collision_pairs_global.resize(nbElements);
            DEBUG_MSG(("nbElements: "+std::to_string(nbElements)+"\n").c_str());
        }
        MPI_Gatherv(&collision_pairs_local.front(),
                vec_len_local,
                MPI_LONG_LONG_INT,
                &this->collision_pairs_global.front(),
                recvcounts,
                displ,
                MPI_LONG_LONG_INT,
                0,
                comm);
        if(myrank == 0)
        {
            print_pair_vec(this->collision_pairs_global);
        }
        this->synchronisation = true;
        // free root rank memory
        if (myrank == 0)
        {
            delete[] recvcounts;
            delete[] displ;
        }
    }

    //get_collision_counter() will only give the correct result on rank 0
    long int get_collision_counter(MPI_Comm comm, int myrank, int nprocs) {
        if(synchronisation==false)
        {
            MPI_merge(comm, myrank, nprocs);
        }
        return this->collision_pairs_global.size();
    }

    //get_collision_pairs() will only give global pairs on rank 0
    std::vector <partsize_t> get_collision_pairs(MPI_Comm comm, int myrank, int nprocs) {
        if(synchronisation==false)
        {
            MPI_merge(comm, myrank, nprocs);
        }
        return this->collision_pairs_global;
    }


    void reset_collision_pairs(){
        this->collision_pairs_local.resize(0);
        this->collision_pairs_global.resize(0);
        this->synchronisation = false;
    }


    /**
     * NOTE: this is called only ONCE for each pair of interacting particles.
     */
    template <int size_particle_positions, int size_particle_rhs>
    void compute_interaction(const partsize_t idx_part1,
                             const real_number pos_part1[],
                             real_number /*rhs_part1*/[],
                             const partsize_t idx_part2,
                             const real_number pos_part2[],
                             real_number /*rhs_part2*/[],
                             const real_number dist_pow2,
                             const real_number /*cutoff*/,
                             const real_number xseparation, /* This separation is x1-x2 */
                             const real_number yseparation,
                             const real_number zseparation){
        switch(this->current_particle_shape)
        {
            case SPHERE:
                {
                    this->add_colliding_pair(idx_part1, idx_part2);
                }
                break;
            case CYLINDER:
                {
                    double pq, xp, xq, t, s, min_distance, min_distance_current;
                    double px,py, pz, qx, qy, qz;

                    const double x = xseparation;
                    const double y = yseparation;
                    const double z = zseparation;
                    /* const double r = std::sqrt(dist_pow2); This variable is not needed. */

                    /* p and q are the orientation vectors of the first and second particles. */
                    px = pos_part1[IDXC_X+3];
                    py = pos_part1[IDXC_Y+3];
                    pz = pos_part1[IDXC_Z+3];
                    qx = pos_part2[IDXC_X+3];
                    qy = pos_part2[IDXC_Y+3];
                    qz = pos_part2[IDXC_Z+3];
                    /* pq, xp, xq are scalar products of these vectors with x, relative position */
                    pq = px * qx + py * qy + pz * qz;
                    xp = x * px + y * py + z * pz;
                    xq = x * qx + y * qy + z * qz;
                    /* t and s parametrize the two rods. Find min distance: */
                    if( pq == 1.0 ){
                        min_distance = std::sqrt(dist_pow2-xp*xp);
                    }
                    else{
                    t = 2.0/(this->get_cylinder_length()*(pq*pq-1.0))*(xp-pq*xq);
                    s = 2.0/(this->get_cylinder_length()*(pq*pq-1.0))*(pq*xp-xq);
                    /* Test if -1<s<1 and -1<t<1 */
                    if( std::abs(t)<=1.0 and std::abs(s)<=1.0 )
                    {
                        /* Get minimal distance in case of both t and s in {-1,1}. Else: check edges */
                        min_distance = this->rod_distance(dist_pow2,xp,xq,pq,t,s);
                    }
                    else
                    {
                        /* t fixed at 1, find min along s */
                        t = 1.0;
                        s = t*pq+2.0/this->get_cylinder_length()*xq;
                        if( std::abs(s)>1.0 ) { s = s / std::abs(s) ;}
                            min_distance_current = this->rod_distance(dist_pow2,xp,xq,pq,t,s);
                            min_distance = min_distance_current;
                        /* t fixed at -1, find min along s */
                        t = -1.0;
                        s = t*pq+2.0/this->get_cylinder_length()*xq;
                        if( std::abs(s)>1.0 ) { s = s / std::abs(s) ;}
                            min_distance_current = this->rod_distance(dist_pow2,xp,xq,pq,t,s);
                            min_distance = fmin( min_distance_current, min_distance );
                        /* s fixed at 1, find min along t */
                        s = 1.0;
                        t = s*pq-2.0/this->get_cylinder_length()*xp;
                        if( std::abs(t)>1.0 ) { t = t / std::abs(t) ;}
                            min_distance_current = this->rod_distance(dist_pow2,xp,xq,pq,t,s);
                            min_distance = fmin( min_distance_current, min_distance );
                        /* s fixed at -1, find min along t */
                        s = -1.0;
                        t = s*pq-2.0/this->get_cylinder_length()*xp;
                        if( std::abs(t)>1.0 ) { t = t / std::abs(t) ;}
                            min_distance_current = this->rod_distance(dist_pow2,xp,xq,pq,t,s);
                            min_distance = fmin( min_distance_current, min_distance );
                    }
                }
                /* If cylinders overlap count it as a collision */
                if( min_distance<=this->get_cylinder_width() ){
                    this->add_colliding_pair(idx_part1, idx_part2);
                    }
                }
                break;
            case DISK:
                {
                    double p1p2, x1p1, x2p2, x0_p2_comp, p0_norm, x1_x0_2, x2_x0_2, x1p0, x2p0, det;
                    std::array<double, 3> x0, x1, x2, p0, p1, p2;
                    std::array<double, 2> t1, t2;
                    /* Particle 2 is in the origin. Use xseperation etc for particle 1. */
                    /* Particle 1 data. */
                    x1[IDXC_X] = xseparation;
                    x1[IDXC_Y] = yseparation;
                    x1[IDXC_Z] = zseparation;
                    p1[IDXC_X] = pos_part1[IDXC_X+3];
                    p1[IDXC_Y] = pos_part1[IDXC_Y+3];
                    p1[IDXC_Z] = pos_part1[IDXC_Z+3];
                    /* Particle 2 data. */
                    x2[IDXC_X] = 0.0;
                    x2[IDXC_Y] = 0.0;
                    x2[IDXC_Z] = 0.0;
                    p2[IDXC_X] = pos_part2[IDXC_X+3];
                    p2[IDXC_Y] = pos_part2[IDXC_Y+3];
                    p2[IDXC_Z] = pos_part2[IDXC_Z+3];
                    /* Exit if the disk planes parallel. */
                    p1p2 = p1[IDXC_X]*p2[IDXC_X] + p1[IDXC_Y]*p2[IDXC_Y] + p1[IDXC_Z]*p2[IDXC_Z];
                    if ( p1p2 == 1.0 ) {
                        return;
                    }
                    x1p1 = x1[IDXC_X]*p1[IDXC_X] + x1[IDXC_Y]*p1[IDXC_Y] + x1[IDXC_Z]*p1[IDXC_Z];
                    x2p2 = x2[IDXC_X]*p2[IDXC_X] + x2[IDXC_Y]*p2[IDXC_Y] + x2[IDXC_Z]*p2[IDXC_Z];
                    x0_p2_comp = (x2p2-p1p2*x1p1)/(1.0-p1p2*p1p2);
                    /* Get x0 and p0. These define the intersection of the two planes of the disks as x0+p0*t */
                    /* Get x0. */
                    x0[IDXC_X] = x1p1*p1[IDXC_X] + x0_p2_comp*( p2[IDXC_X]-p1p2*p1[IDXC_X] );
                    x0[IDXC_Y] = x1p1*p1[IDXC_Y] + x0_p2_comp*( p2[IDXC_Y]-p1p2*p1[IDXC_Y] );
                    x0[IDXC_Z] = x1p1*p1[IDXC_Z] + x0_p2_comp*( p2[IDXC_Z]-p1p2*p1[IDXC_Z] );
                    /* Get p0. */
                    p0[IDXC_X] = p1[IDXC_Y]*p2[IDXC_Z] - p1[IDXC_Z]*p2[IDXC_Y];
                    p0[IDXC_Y] = p1[IDXC_Z]*p2[IDXC_X] - p1[IDXC_X]*p2[IDXC_Z];
                    p0[IDXC_Z] = p1[IDXC_X]*p2[IDXC_Y] - p1[IDXC_Y]*p2[IDXC_X];
                    p0_norm = std::sqrt(p0[IDXC_X]*p0[IDXC_X]+p0[IDXC_Y]*p0[IDXC_Y]+p0[IDXC_Z]*p0[IDXC_Z]);
                    p0[IDXC_X] = p0[IDXC_X] / p0_norm;
                    p0[IDXC_Y] = p0[IDXC_Y] / p0_norm;
                    p0[IDXC_Z] = p0[IDXC_Z] / p0_norm;

                    /* Scalar products needed to solve for minimal distance in t. */
                    x1_x0_2 = (x1[IDXC_X]-x0[IDXC_X])*(x1[IDXC_X]-x0[IDXC_X]) + (x1[IDXC_Y]-x0[IDXC_Y])*(x1[IDXC_Y]-x0[IDXC_Y]) + (x1[IDXC_Z]-x0[IDXC_Z])*(x1[IDXC_Z]-x0[IDXC_Z]);
                    x2_x0_2 = (x2[IDXC_X]-x0[IDXC_X])*(x2[IDXC_X]-x0[IDXC_X]) + (x2[IDXC_Y]-x0[IDXC_Y])*(x2[IDXC_Y]-x0[IDXC_Y]) + (x2[IDXC_Z]-x0[IDXC_Z])*(x2[IDXC_Z]-x0[IDXC_Z]);
                    x1p0 = x1[IDXC_X]*p0[IDXC_X] + x1[IDXC_Y]*p0[IDXC_Y] + x1[IDXC_Z]*p0[IDXC_Z];
                    x2p0 = x2[IDXC_X]*p0[IDXC_X] + x2[IDXC_Y]*p0[IDXC_Y] + x2[IDXC_Z]*p0[IDXC_Z];

                    /*Check for collision. Check if segments of disk along common line between planes p1 and p2 intersect.  */
                    det = x1p0 * x1p0 + 0.25 * this->get_disk_width() * this->get_disk_width() - x1_x0_2;
                    if (det == 0.0){
                        t1[0] = x1p0;
                        t1[1] = x1p0;
                    } else if (det > 0.0) {
                        t1[0] =  x1p0 - std::sqrt(det);
                        t1[1] =  x1p0 + std::sqrt(det);
                    } else {return;}

                    det = x2p0 * x2p0 + 0.25 * this->get_disk_width() * this->get_disk_width() - x2_x0_2;
                    if (det == 0.0){
                        t2[0] = x2p0;
                        t2[1] = x2p0;
                    } else if (det > 0.0) {
                        t2[0] =  x2p0 - std::sqrt(det);
                        t2[1] =  x2p0 + std::sqrt(det);
                    } else {return;}

                    if (( t1[1]>=t2[0] ) and
                        ( t2[1]>=t1[0] ))
                    {
                        this->add_colliding_pair(idx_part1, idx_part2);
                        return;
                    }
                }
                break;
        }
    }

    void set_sphere()
    {
        this->current_particle_shape = SPHERE;
    }

    void set_cylinder()
    {
        this->current_particle_shape = CYLINDER;
    }

    void set_cylinder_width(const double WIDTH)
    {
        this->cylinder_width = WIDTH;
    }

    void set_cylinder_length(const double LENGTH)
    {
        this->cylinder_length = LENGTH;
    }

    double get_cylinder_width() const
    {
        return this->cylinder_width;
    }

    double get_cylinder_length() const
    {
        return this->cylinder_length;
    }

    void set_disk()
    {
        this->current_particle_shape = DISK;
    }

    particle_shape get_current_particle_shape() const
    {
        return this->current_particle_shape;
    }

    void set_disk_width(const double WIDTH)
    {
        this->disk_width = WIDTH;
    }

    double get_disk_width() const
    {
        return this->disk_width;
    }

    bool isEnable() const {
        return isActive;
    }

    void setEnable(const bool inIsActive)
    {
        isActive = inIsActive;
    }

    bool isSynchronized() const
    {
        return this->synchronisation;
    }
};


#endif // P2P_GHOST_COLLISIONS_HPP

