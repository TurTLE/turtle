/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef P2P_COMPUTER_EMPTY_HPP
#define P2P_COMPUTER_EMPTY_HPP

template <class real_number, class partsize_t>
class p2p_computer_empty{
public:

    template <int size_particle_rhs>
    void reduce_particles_rhs(real_number /*rhs_dst*/[], const real_number /*rhs_src*/[], const partsize_t /*nbParticles*/) const{
    }

    /**
     * NOTE: this is called only ONCE for each pair of interacting particles.
     */
    template <int size_particle_positions, int size_particle_rhs>
    void compute_interaction(const partsize_t /*idx_part1*/,
                             const real_number /*pos_part1*/[], real_number /*rhs_part1*/[],
                             const partsize_t /*idx_part2*/,
                             const real_number /*pos_part2*/[], real_number /*rhs_part2*/[],
                             const real_number /*dist_pow2*/,
                             const real_number /*cutoff*/,
                             const real_number /*xseparation*/,
                             const real_number /*yseparation*/,
                             const real_number /*zseparation*/) const{
    }

    void merge(const p2p_computer_empty&){}

    constexpr static bool isEnable() {
        return false;
    }
};

#endif
