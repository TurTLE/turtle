/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef P2P_TREE_HPP
#define P2P_TREE_HPP

#include <unordered_map>
#include <array>

template <class CellClass>
class p2p_tree{
    std::unordered_map<long int, CellClass> data;
    CellClass emptyCell;
    std::array<long int,3> nb_cell_levels;

    long int get_cell_coord_x_from_index(const long int index) const{
        return index % nb_cell_levels[IDXC_X];
    }

    long int get_cell_coord_y_from_index(const long int index) const{
        return (index % (nb_cell_levels[IDXC_X]*nb_cell_levels[IDXC_Y]))
                / nb_cell_levels[IDXC_X];
    }

    long int get_cell_coord_z_from_index(const long int index) const{
        return index / (nb_cell_levels[IDXC_X]*nb_cell_levels[IDXC_Y]);
    }

    long int get_cell_idx(const long int idx_x, const long int idx_y,
                          const long int idx_z) const {
        return (((idx_z*nb_cell_levels[IDXC_Y])+idx_y)*nb_cell_levels[IDXC_X])+idx_x;
    }

public:
    explicit p2p_tree(std::array<long int,3> in_nb_cell_levels)
        : nb_cell_levels(in_nb_cell_levels){
    }

    CellClass& getCell(const long int idx){
        return data[idx];
    }


    const CellClass& getCell(const long int idx) const {
        const auto& iter = data.find(idx);
        if(iter != data.end()){
            return iter->second;
        }
        return emptyCell;
    }

    template <class ShiftType>
    int getNeighbors(const long int idx, const CellClass* output[27], long int output_indexes[27],
                     std::array<ShiftType,3> shift[27], const bool include_target) const{
        int nbNeighbors = 0;

        std::fill_n(output, 27, nullptr);

        const long int idx_x = get_cell_coord_x_from_index(idx);
        const long int idx_y = get_cell_coord_y_from_index(idx);
        const long int idx_z = get_cell_coord_z_from_index(idx);

        for(long int neigh_x = -1 ; neigh_x <= 1 ; ++neigh_x){
            long int neigh_x_pbc = neigh_x+idx_x;
            ShiftType shift_x = 0;
            if(neigh_x_pbc < 0){
                neigh_x_pbc += nb_cell_levels[IDXC_X];
                shift_x = 1;
            }
            else if(nb_cell_levels[IDXC_X] <= neigh_x_pbc){
                neigh_x_pbc -= nb_cell_levels[IDXC_X];
                shift_x = -1;
            }

            for(long int neigh_y = -1 ; neigh_y <= 1 ; ++neigh_y){
                long int neigh_y_pbc = neigh_y+idx_y;
                ShiftType shift_y = 0;
                if(neigh_y_pbc < 0){
                    neigh_y_pbc += nb_cell_levels[IDXC_Y];
                    shift_y = 1;
                }
                else if(nb_cell_levels[IDXC_Y] <= neigh_y_pbc){
                    neigh_y_pbc -= nb_cell_levels[IDXC_Y];
                    shift_y = -1;
                }

                for(long int neigh_z = -1 ; neigh_z <= 1 ; ++neigh_z){
                    long int neigh_z_pbc = neigh_z+idx_z;
                    ShiftType shift_z = 0;
                    if(neigh_z_pbc < 0){
                        neigh_z_pbc += nb_cell_levels[IDXC_Z];
                        shift_z = 1;
                    }
                    else if(nb_cell_levels[IDXC_Z] <= neigh_z_pbc){
                        neigh_z_pbc -= nb_cell_levels[IDXC_Z];
                        shift_z = -1;
                    }

                    if(include_target || neigh_x_pbc != idx_x || neigh_y_pbc != idx_y || neigh_z_pbc != idx_z){
                        const long int idx_neigh = get_cell_idx(neigh_x_pbc,
                                                                  neigh_y_pbc,
                                                                  neigh_z_pbc);
                        const auto& iter = data.find(idx_neigh);
                        if(iter != data.end()){
                            output[nbNeighbors] = &(iter->second);
                            output_indexes[nbNeighbors] = idx_neigh;

                            shift[nbNeighbors][IDXC_X] = shift_x;
                            shift[nbNeighbors][IDXC_Y] = shift_y;
                            shift[nbNeighbors][IDXC_Z] = shift_z;

                            nbNeighbors += 1;
                        }
                    }
                }
            }
        }

        return nbNeighbors;
    }

    typename std::unordered_map<long int, CellClass>::iterator begin(){
        return data.begin();
    }

    typename std::unordered_map<long int, CellClass>::iterator end(){
        return data.end();
    }
};

#endif
