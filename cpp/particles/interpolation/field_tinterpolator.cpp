/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/

#include "particles/interpolation/field_tinterpolator.hpp"

template class field_tinterpolator<float,  FFTW, ONE,         NONE>;
template class field_tinterpolator<float,  FFTW, THREE,       NONE>;
template class field_tinterpolator<float,  FFTW, THREExTHREE, NONE>;

template class field_tinterpolator<double, FFTW, ONE,         NONE>;
template class field_tinterpolator<double, FFTW, THREE,       NONE>;
template class field_tinterpolator<double, FFTW, THREExTHREE, NONE>;

template class field_tinterpolator<float,  FFTW, ONE,         LINEAR>;
template class field_tinterpolator<float,  FFTW, THREE,       LINEAR>;
template class field_tinterpolator<float,  FFTW, THREExTHREE, LINEAR>;

template class field_tinterpolator<double, FFTW, ONE,         LINEAR>;
template class field_tinterpolator<double, FFTW, THREE,       LINEAR>;
template class field_tinterpolator<double, FFTW, THREExTHREE, LINEAR>;

