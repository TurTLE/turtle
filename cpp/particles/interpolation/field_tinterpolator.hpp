/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef FIELD_TINTERPOLATOR_HPP
#define FIELD_TINTERPOLATOR_HPP

#include "particles/particles_distr_mpi.hpp"
#include "particles/particles_output_hdf5.hpp"
#include "particles/interpolation/abstract_particle_set.hpp"


enum temporal_interpolation_type {NONE, LINEAR, CUBIC};

template <typename rnumber,
          field_backend be,
          field_components fc,
          temporal_interpolation_type tt>
class field_tinterpolator
{
    private:
        using particle_rnumber = double;
        using partsize_t = long long int;

        std::array<field<rnumber, be, fc>*, 4> field_list;  // list of fields for temporal interpolation

    public:
        field_tinterpolator(){
            this->field_list[0] = NULL;
            this->field_list[1] = NULL;
            this->field_list[2] = NULL;
            this->field_list[3] = NULL;
        }

        ~field_tinterpolator() noexcept(false){}

        int set_field(
                field<rnumber, be, fc> *field_src = NULL,
                const int tindex = 0)
        {
            switch(tt)
            {
                case NONE:
                    assert(tindex == 0);
                    this->field_list[0] = field_src;
                    break;
                case LINEAR:
                    assert(tindex == 0 || tindex == 1);
                    this->field_list[tindex] = field_src;
                    break;
                case CUBIC:
                    // TODO: add cubic spline interpolation in time here
                    assert(false);
                    break;
            }
            return EXIT_SUCCESS;
        }

        // t is a fraction between 0 and 1.
        int operator()(
                double t,
                abstract_particle_set &pset,
                particle_rnumber *result)
        {
            switch(tt)
            {
                case NONE:
                    {
                        pset.sample(*(this->field_list[0]), result);
                        break;
                    }
                case LINEAR:
                    {
                        particle_rnumber *result0, *result1;
                        result0 = new particle_rnumber[pset.getLocalNumberOfParticles()*ncomp(fc)];
                        result1 = new particle_rnumber[pset.getLocalNumberOfParticles()*ncomp(fc)];
                        // interpolation adds on top of existing values, so result must be cleared.
                        std::fill_n(result0, pset.getLocalNumberOfParticles()*ncomp(fc), 0);
                        pset.sample(*(this->field_list[0]), result0);
                        // interpolation adds on top of existing values, so result must be cleared.
                        std::fill_n(result1, pset.getLocalNumberOfParticles()*ncomp(fc), 0);
                        pset.sample(*(this->field_list[1]), result1);
                        // not using LOOP method below because we want the inner loop over ncomp,
                        // not over pset.getStateSize()
                        for (partsize_t idx_part = 0; idx_part < pset.getLocalNumberOfParticles(); idx_part++)
                        {
                            for (unsigned int cc = 0; cc < ncomp(fc); cc++)
                            {
                                result[idx_part*ncomp(fc) + cc] = (
                                        (1-t)*result0[idx_part*ncomp(fc) + cc] +
                                           t *result1[idx_part*ncomp(fc) + cc]);
                            }
                        }
                        delete[] result0;
                        delete[] result1;
                        break;
                    }
                case CUBIC:
                    // TODO: add cubic spline interpolation in time here
                    assert(false);
                    break;
            }
            return EXIT_SUCCESS;
        }
};

#endif//FIELD_TINTERPOLATOR_HPP

