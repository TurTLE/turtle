/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLES_GENERIC_INTERP_HPP
#define PARTICLES_GENERIC_INTERP_HPP

/** \brief Computes weights for interpolation formulas.
 *
 * */

template <class real_number, int interp_neighbours, int smoothness>
class particles_generic_interp;

#include "Lagrange_polys.hpp"
#include "spline.hpp"

/*****************************************************************************/
/* Linear interpolation */

template <>
class particles_generic_interp<double, 0,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n0_m0(in_derivative, in_part_val, poly_val);
    }
};
/*****************************************************************************/

/*****************************************************************************/
/* Lagrange interpolation */

template <>
class particles_generic_interp<double, 1,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 2,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 3,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 4,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 5,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n5(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 6,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n6(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 7,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n7(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 8,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n8(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 9,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n9(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 10,0>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_Lagrange_n10(in_derivative, in_part_val, poly_val);
    }
};

/*****************************************************************************/

/*****************************************************************************/
/* spline C1 */

template <>
class particles_generic_interp<double, 1,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n1_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 2,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n2_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 3,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n3_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 4,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n4_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 5,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n5_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 6,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n6_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 7,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n7_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 8,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n8_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 9,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n9_m1(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 10,1>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n10_m1(in_derivative, in_part_val, poly_val);
    }
};

/*****************************************************************************/

/*****************************************************************************/
/* spline C2 */

template <>
class particles_generic_interp<double, 1,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n1_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 2,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n2_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 3,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n3_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 4,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n4_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 5,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n5_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 6,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n6_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 7,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n7_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 8,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n8_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 9,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n9_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 10,2>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n10_m2(in_derivative, in_part_val, poly_val);
    }
};

/*****************************************************************************/

/*****************************************************************************/
/* spline C3 */

template <>
class particles_generic_interp<double, 1,3>{
    // no such thing as C3 interpolant with just a 4-point kernel, so we just use C2
    // this specialisation must exist, otherwise the template_double_for_if will fail
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n1_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 2,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n2_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 3,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n3_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 4,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n4_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 5,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n5_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 6,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n6_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 7,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n7_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 8,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n8_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 9,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n9_m3(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 10,3>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n10_m3(in_derivative, in_part_val, poly_val);
    }
};

/*****************************************************************************/

/*****************************************************************************/
/* spline C4 */

template <>
class particles_generic_interp<double, 1,4>{
    // no such thing as C4 interpolant with just a 4-point kernel, so we just use C2
    // this specialisation must exist, otherwise the template_double_for_if will fail
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n1_m2(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 2,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n2_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 3,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n3_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 4,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n4_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 5,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n5_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 6,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n6_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 7,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n7_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 8,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n8_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 9,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n9_m4(in_derivative, in_part_val, poly_val);
    }
};

template <>
class particles_generic_interp<double, 10,4>{
public:
    using real_number = double;

    void compute_beta(const int in_derivative, const double in_part_val, double poly_val[]) const {
        beta_n10_m4(in_derivative, in_part_val, poly_val);
    }
};

/*****************************************************************************/

#endif//PARTICLES_INTERP_SPLINE_HPP

