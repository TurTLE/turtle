/******************************************************************************
*                                                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef SPLINE_HPP
#define SPLINE_HPP

#include "particles/interpolation/spline_n0.hpp"
#include "particles/interpolation/spline_n1.hpp"
#include "particles/interpolation/spline_n2.hpp"
#include "particles/interpolation/spline_n3.hpp"
#include "particles/interpolation/spline_n4.hpp"
#include "particles/interpolation/spline_n5.hpp"
#include "particles/interpolation/spline_n6.hpp"
#include "particles/interpolation/spline_n7.hpp"
#include "particles/interpolation/spline_n8.hpp"
#include "particles/interpolation/spline_n9.hpp"
#include "particles/interpolation/spline_n10.hpp"

#endif
