/**********************************************************************
*                                                                     *
*  Copyright 2022 the TurTLE team                                     *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#include "spline_n0.hpp"

void beta_n0_m0(const int deriv, const double x, double *__restrict__ poly_val)
{
    switch(deriv)
    {
    case 0:
        poly_val[0] = -x + 1;
        poly_val[1] = x;
        break;
    case 1:
        poly_val[0] = -1;
        poly_val[1] = 1;
        break;
    case 2:
        poly_val[0] = 0;
        poly_val[1] = 0;
        break;
    }
}

