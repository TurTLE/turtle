/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLE_SET_HPP
#define PARTICLE_SET_HPP

#include "field.hpp"
#include "particles/particles_distr_mpi.hpp"
#include "particles/interpolation/particles_field_computer.hpp"
#include "particles/interpolation/particles_generic_interp.hpp"
#include "particles/interpolation/abstract_particle_set.hpp"


/** Practical implementation of particle sets.
 *
 * Child of `abstract_particle_set`. Brings together the functionality of
 * `particles_field_computer` and `particles_distr_mpi`.
 *
 */

template <int state_size,
          int neighbours,
          int smoothness>
class particle_set: public abstract_particle_set
{
    private:
        // custom names for types
        using interpolator_class = particles_field_computer<partsize_t,
                                                            particle_rnumber,
                                                            particles_generic_interp<particle_rnumber, neighbours, smoothness>,
                                                            neighbours>;
        using distributor_class = particles_distr_mpi<partsize_t, particle_rnumber>;

        // MPI
        MPI_Comm mpi_comm;

        // data
        const std::pair<int,int> current_partition_interval;
        const int partition_interval_size;

        std::unique_ptr<partsize_t[]> number_particles_per_partition;
        std::unique_ptr<partsize_t[]> offset_particles_for_partition;

        std::unique_ptr<particle_rnumber[]> local_state;
        std::unique_ptr<partsize_t[]>       local_indices;
        std::unique_ptr<partsize_t[]>       local_labels;
        partsize_t                          local_number_of_particles;
        partsize_t                          total_number_of_particles;

        // for I/O it helps to have custom array shape, we store it here
        std::vector<hsize_t> particle_file_layout;

        // raw objects for distributing data in MPI, and for interpolating fields
        distributor_class   pDistributor;
        int defaultDistributorCounterShiftTags;
        particles_generic_interp<particle_rnumber, neighbours, smoothness> interpolation_formula;
        interpolator_class  pInterpolator;
        p2p_distr_mpi<partsize_t, particle_rnumber> p2pComputer;

    public:
        particle_set(
                const abstract_field_layout *fl,
                const double dkx,
                const double dky,
                const double dkz,
                const double p2p_cutoff = 1.0):
            mpi_comm(fl->getMPIComm()),
            current_partition_interval(
                    {fl->getStart(IDXV_Z),
                     fl->getStart(IDXV_Z) + fl->getSubSize(IDXV_Z)}),
            partition_interval_size(current_partition_interval.second - current_partition_interval.first),
            pDistributor(
                    mpi_comm,
                    current_partition_interval,
                    std::array<size_t, 3>({  //field_grid_dim from particles_system_builder
                        fl->getSize(IDXV_X),
                        fl->getSize(IDXV_Y),
                        fl->getSize(IDXV_Z),
                        })),
            interpolation_formula(),
            pInterpolator(
                    std::array<size_t, 3>({ //field_grid_dim from particles_system_builder
                        fl->getSize(IDXV_X),
                        fl->getSize(IDXV_Y),
                        fl->getSize(IDXV_Z),
                        }),
                    current_partition_interval,
                    interpolation_formula,
                    std::array<particle_rnumber, 3>({ // spatial_box_width from particles_system_builder
                        4*acos(0) / dkx,
                        4*acos(0) / dky,
                        4*acos(0) / dkz,
                        }),
                    std::array<particle_rnumber, 3>({ // spatial_box_offset from particles_system_builder
                        0,
                        0,
                        0,
                        }),
                    std::array<particle_rnumber, 3>({ // spatial_box_width divided by field_grid_dim
                        (4*acos(0) / dkx) / fl->getSize(IDXV_X),
                        (4*acos(0) / dky) / fl->getSize(IDXV_Y),
                        (4*acos(0) / dkz) / fl->getSize(IDXV_Z)
                        })),
            p2pComputer(
                    mpi_comm,
                    current_partition_interval,
                    std::array<size_t, 3>({  //field_grid_dim from particles_system_builder
                        fl->getSize(IDXV_X),
                        fl->getSize(IDXV_Y),
                        fl->getSize(IDXV_Z),
                        }),
                    std::array<particle_rnumber, 3>({ // spatial_box_width from particles_system_builder
                        4*acos(0) / dkx,
                        4*acos(0) / dky,
                        4*acos(0) / dkz,
                        }),
                    std::array<particle_rnumber, 3>({ // spatial_box_offset from particles_system_builder
                        0,
                        0,
                        0,
                        }),
                    p2p_cutoff)

        {
            TIMEZONE("particle_set::particle_set");
            // if these assertions fail,
            // it means the constructor prototype is broken
            // because the arrays above are wrong.
            assert(IDXC_X == 0);
            assert(IDXC_Y == 1);
            assert(IDXC_Z == 2);

            //DEBUG_MSG("current partition interval %d %d\n",
            //        current_partition_interval.first,
            //        current_partition_interval.second);

            this->number_particles_per_partition.reset(new partsize_t[partition_interval_size]);
            this->offset_particles_for_partition.reset(new partsize_t[partition_interval_size+1]);

            this->defaultDistributorCounterShiftTags = 0;
        }

        int _print_debug_info()
        {
            int myrank;
            AssertMpi(MPI_Comm_rank(mpi_comm, &myrank));
            int nprocs;
            AssertMpi(MPI_Comm_size(mpi_comm, &nprocs));

            for (int rr = 0; rr < nprocs; rr++)
            {
                if (myrank == rr)
                {
                    DEBUG_MSG("##########################################\n");
                    DEBUG_MSG("# particle_set debug info follows\n");

                    DEBUG_MSG("current partition interval %d %d\n",
                            this->current_partition_interval.first,
                            this->current_partition_interval.second);
                    DEBUG_MSG("partition_interval_size %d\n",
                            this->partition_interval_size);

                    for (int i=0; i<this->partition_interval_size; i++)
                    {
                        DEBUG_MSG("number_particles_per_partition[%d] = %d\n",
                                i,
                                number_particles_per_partition[i]);
                    }

                    for (int i=0; i<this->partition_interval_size+1; i++)
                    {
                        DEBUG_MSG("offset_particles_for_partition[%d] = %d\n",
                                i,
                                offset_particles_for_partition[i]);
                    }

                    DEBUG_MSG("local_number_of_particles = %d\n",
                            this->local_number_of_particles);
                    DEBUG_MSG("total_number_of_particles = %d\n",
                            this->total_number_of_particles);

                    for (int i=0; i<int(this->particle_file_layout.size()); i++)
                        DEBUG_MSG("particle_file_layout[%d] = %d\n",
                                i,
                                this->particle_file_layout[i]);
                    if (this->total_number_of_particles < 100)
                    {
                        for (partsize_t ii=0; ii < this->local_number_of_particles; ii++)
                        {
                            DEBUG_MSG("local_counter = %d, index = %d, label = %d\n",
                                    ii,
                                    this->local_indices[ii],
                                    this->local_labels[ii]);
                        }
                    }

                    DEBUG_MSG("# particle_set debug info ends\n");
                    DEBUG_MSG("##########################################\n");
                }
                MPI_Barrier(this->mpi_comm);
            }
            return EXIT_SUCCESS;
        }

        ~particle_set() noexcept(false){}

        /** Method to handle technical MPI aspects
         *
         * Interpolation and particle redistribution require many messages,
         * and we do our best to avoid global MPI calls within these operations.
         * In turn this requires that MPI tags for the different messages are
         * carefully managed.
         * This method should be called as often as possible, but only if some
         * global MPI call accompanies it.
         * For example it should be called every time a full time step
         * computation is completed.
         * See `particles_distr_mpi.hpp` for more details.
         */
        inline int resetMPITagShiftCounter() {
            this->pDistributor.set_counter_shift_tags(this->defaultDistributorCounterShiftTags);
            return EXIT_SUCCESS;
        }

        inline int setMPITagShiftCounter(
                const int new_counter_shift_tags) {
            this->defaultDistributorCounterShiftTags = new_counter_shift_tags;
            return EXIT_SUCCESS;
        }

        inline int getMPITagShiftCounter() const {
            return this->defaultDistributorCounterShiftTags;
        }

        inline int getMPITagShiftOffset() const {
            return this->pDistributor.get_tag_shift_offset();
        }

        particle_rnumber* getParticleState() const
        {
            return this->local_state.get();
        }

        int setParticleState(particle_rnumber *src_local_state)
        {
            this->LOOP(
                [&](const partsize_t idx){
                this->local_state[idx] = src_local_state[idx];
                });
            return EXIT_SUCCESS;
        }

        int getParticleState(particle_rnumber *dst_local_state)
        {
            this->LOOP(
                [&](const partsize_t idx){
                dst_local_state[idx] = this->local_state[idx];
                });
            return EXIT_SUCCESS;
        }

        partsize_t* getParticleIndices() const
        {
            return this->local_indices.get();
        }

        partsize_t* getParticleLabels() const
        {
            return this->local_labels.get();
        }

        int setParticleLabels(partsize_t *src_local_labels)
        {
            for (partsize_t idx = 0; idx < this->getLocalNumberOfParticles(); idx++)
                this->local_labels[idx] = src_local_labels[idx];
            return EXIT_SUCCESS;
        }

        partsize_t getLocalNumberOfParticles() const
        {
            return this->local_number_of_particles;
        }

        partsize_t getTotalNumberOfParticles() const
        {
            return this->total_number_of_particles;
        }

        partsize_t* getParticlesPerPartition() const
        {
            return this->number_particles_per_partition.get();
        }

        int getStateSize() const
        {
            return state_size;
        }

        int setParticleFileLayout(std::vector<hsize_t> input_layout)
        {
            this->particle_file_layout.resize(input_layout.size());
            for (unsigned int i=0; i<this->particle_file_layout.size(); i++)
                this->particle_file_layout[i] = input_layout[i];
            return EXIT_SUCCESS;
        }

        std::vector<hsize_t> getParticleFileLayout(void)
        {
            return std::vector<hsize_t>(this->particle_file_layout);
        }

        std::unique_ptr<particle_rnumber[]> extractFromParticleState(
                const int firstState,
                const int lastState) const
        {
            TIMEZONE("particle_set::extractFromParticleState");
            const int numberOfStates = std::max(0,(std::min(lastState, state_size)-firstState));

            std::unique_ptr<particle_rnumber[]> stateExtract(new particle_rnumber[local_number_of_particles*numberOfStates]);

            for(partsize_t idx_part = 0 ; idx_part < this->local_number_of_particles ; ++idx_part){
                for(int idxState = 0 ; idxState < numberOfStates ; ++idxState){
                    stateExtract[idx_part*numberOfStates + idxState] = this->local_state[idx_part*state_size + idxState+firstState];
                }
            }

            return stateExtract;
        }

        template <typename field_rnumber,
                  field_backend be,
                  field_components fc>
        int template_sample(const field<field_rnumber, be, fc> &field_to_sample,
                   particle_rnumber *result)
        {
            TIMEZONE("particle_set::template_sample");
            // attention: compute_distr adds result on top of existing values.
            // please clean up result as appropriate before call.
            this->pDistributor.template compute_distr<interpolator_class,
                                                      field<field_rnumber, be, fc>,
                                                      state_size,
                                                      ncomp(fc)>(
                    this->pInterpolator,
                    field_to_sample,
                    this->number_particles_per_partition.get(),
                    this->local_state.get(),
                    result,
                    neighbours);
            return EXIT_SUCCESS;
        }

        int sample(const field<float, FFTW, ONE> &field_to_sample,
                   particle_rnumber *result)
        {
            return template_sample<float, FFTW, ONE>(field_to_sample, result);
        }
        int sample(const field<float, FFTW, THREE> &field_to_sample,
                   particle_rnumber *result)
        {
            return template_sample<float, FFTW, THREE>(field_to_sample, result);
        }
        int sample(const field<float, FFTW, THREExTHREE> &field_to_sample,
                   particle_rnumber *result)
        {
            return template_sample<float, FFTW, THREExTHREE>(field_to_sample, result);
        }
        int sample(const field<double, FFTW, ONE> &field_to_sample,
                   particle_rnumber *result)
        {
            return template_sample<double, FFTW, ONE>(field_to_sample, result);
        }
        int sample(const field<double, FFTW, THREE> &field_to_sample,
                   particle_rnumber *result)
        {
            return template_sample<double, FFTW, THREE>(field_to_sample, result);
        }
        int sample(const field<double, FFTW, THREExTHREE> &field_to_sample,
                   particle_rnumber *result)
        {
            return template_sample<double, FFTW, THREExTHREE>(field_to_sample, result);
        }

        int redistribute(std::vector<std::unique_ptr<particle_rnumber[]>> &additional_data)
        {
            TIMEZONE("particle_set::redistribute");
            this->pDistributor.template redistribute<interpolator_class,
                                                     state_size,
                                                     state_size,
                                                     1>(
                    this->pInterpolator,
                    this->number_particles_per_partition.get(),
                    &this->local_number_of_particles,
                    &this->local_state,
                    additional_data.data(),
                    int(additional_data.size()),
                    &this->local_indices,
                    &this->local_labels);
            return EXIT_SUCCESS;
        }

        int init(abstract_particles_input<partsize_t, particle_rnumber>& particles_input)
        {
            TIMEZONE("particle_set::init");

            this->local_state = particles_input.getMyParticles();
            this->local_indices = particles_input.getMyParticlesIndexes();
            this->local_labels = particles_input.getMyParticlesLabels();
            this->local_number_of_particles = particles_input.getLocalNbParticles();
            this->total_number_of_particles = particles_input.getTotalNbParticles();


            particles_utils::partition_extra_z<partsize_t, state_size>(
                    &this->local_state[0],
                    this->local_number_of_particles,
                    partition_interval_size,
                    this->number_particles_per_partition.get(),
                    this->offset_particles_for_partition.get(),
                    [&](const particle_rnumber& z_pos){
                        const int partition_level = this->pInterpolator.pbc_field_layer(z_pos, IDXC_Z);
                        assert(current_partition_interval.first <= partition_level && partition_level < current_partition_interval.second);
                        return partition_level - current_partition_interval.first;
                    },
                    [&](const partsize_t idx1, const partsize_t idx2){
                        std::swap(this->local_indices[idx1], this->local_indices[idx2]);
                        std::swap(this->local_labels[idx1], this->local_labels[idx2]);
                    });

            int file_layout_result = this->setParticleFileLayout(particles_input.getParticleFileLayout());
            variable_used_only_in_assert(file_layout_result);
            assert(file_layout_result == EXIT_SUCCESS);

            return EXIT_SUCCESS;
        }

        int init_as_subset_of(
                abstract_particles_system<partsize_t, particle_rnumber> &src,
                const std::vector<partsize_t> &indices_to_copy)
        {
            TIMEZONE("particle_set::init_as_subset_of");
            assert(indices_to_copy.size() > 0);
            particle_rnumber *tmp_local_state = new particle_rnumber[state_size * src.getLocalNbParticles()];
            partsize_t *tmp_local_indices = new partsize_t[src.getLocalNbParticles()];
            partsize_t *tmp_local_labels = new partsize_t[src.getLocalNbParticles()];
            this->local_number_of_particles = 0;
            this->total_number_of_particles = indices_to_copy.size();

            //DEBUG_MSG("particle_set::init_as_subset_of, desired subset_size = %ld, src_local_number = %ld, src_total_number = %ld\n",
            //        this->total_number_of_particles,
            //        src.getLocalNbParticles(),
            //        src.getGlobalNbParticles());

            // dumb selection of interesting particles
            for (partsize_t ii = 0; ii < partsize_t(src.getLocalNbParticles()); ii++)
            {
                partsize_t src_label = src.getParticlesIndexes()[ii];
                for (partsize_t iii=0; iii < partsize_t(indices_to_copy.size()); iii++)
                {
                    if (src_label == indices_to_copy[iii])
                    {
                        tmp_local_indices[this->local_number_of_particles] = iii;
                        tmp_local_labels[this->local_number_of_particles] = src_label;
                        std::copy(src.getParticlesState()+state_size*ii,
                                  src.getParticlesState()+state_size*(ii+1),
                                  tmp_local_state + state_size*this->local_number_of_particles);
                        this->local_number_of_particles++;
                        break;
                    }
                }
            }

            // now we actually put the data "here"
            if (this->local_number_of_particles > 0)
            {
                this->local_state.reset(new particle_rnumber[state_size*this->local_number_of_particles]);
                this->local_indices.reset(new partsize_t[this->local_number_of_particles]);
                this->local_labels.reset(new partsize_t[this->local_number_of_particles]);
                std::copy(tmp_local_state,
                          tmp_local_state + state_size*this->local_number_of_particles,
                          this->local_state.get());
                std::copy(tmp_local_indices,
                          tmp_local_indices + this->local_number_of_particles,
                          this->local_indices.get());
                std::copy(tmp_local_labels,
                          tmp_local_labels + this->local_number_of_particles,
                          this->local_labels.get());
            }

            particles_utils::partition_extra_z<partsize_t, state_size>(
                    &this->local_state[0],
                    this->local_number_of_particles,
                    partition_interval_size,
                    this->number_particles_per_partition.get(),
                    this->offset_particles_for_partition.get(),
                    [&](const particle_rnumber& z_pos){
                        const int partition_level = this->pInterpolator.pbc_field_layer(z_pos, IDXC_Z);
                        assert(current_partition_interval.first <= partition_level && partition_level < current_partition_interval.second);
                        return partition_level - current_partition_interval.first;
                    },
                    [&](const partsize_t idx1, const partsize_t idx2){
                        std::swap(this->local_indices[idx1], this->local_indices[idx2]);
                        std::swap(this->local_labels[idx1], this->local_labels[idx2]);
                    });

            delete[] tmp_local_state;
            delete[] tmp_local_indices;
            delete[] tmp_local_labels;

            std::vector<hsize_t> tmp_file_layout(1);
            tmp_file_layout[0] = hsize_t(this->total_number_of_particles);

            int file_layout_result = this->setParticleFileLayout(tmp_file_layout);
            variable_used_only_in_assert(file_layout_result);
            assert(file_layout_result == EXIT_SUCCESS);
            return EXIT_SUCCESS;
        }

        int operator=(
                abstract_particle_set *src)
        {
            TIMEZONE("particle_set::operator=");
            assert(src->getStateSize() == state_size);
            particle_rnumber *tmp_local_state = new particle_rnumber[state_size * src->getLocalNumberOfParticles()];
            partsize_t *tmp_local_indices = new partsize_t[src->getLocalNumberOfParticles()];
            partsize_t *tmp_local_labels = new partsize_t[src->getLocalNumberOfParticles()];
            this->local_number_of_particles = 0;

            this->total_number_of_particles = src->getTotalNumberOfParticles();

            for (partsize_t ii = 0; ii < partsize_t(src->getLocalNumberOfParticles()); ii++)
            {
                partsize_t src_label = src->getParticleLabels()[ii];
                partsize_t src_index = src->getParticleIndices()[ii];

                {
                    // set new index
                    tmp_local_indices[this->local_number_of_particles] = src_index;
                    tmp_local_labels[this->local_number_of_particles] = src_label;
                    std::copy(src->getParticleState()+state_size*ii,
                              src->getParticleState()+state_size*(ii+1),
                              tmp_local_state + state_size*this->local_number_of_particles);
                    this->local_number_of_particles++;
                }
            }

            // now we actually put the data "here"
            if (this->local_number_of_particles > 0)
            {
                this->local_state.reset(new particle_rnumber[state_size*this->local_number_of_particles]);
                this->local_indices.reset(new partsize_t[this->local_number_of_particles]);
                this->local_labels.reset(new partsize_t[this->local_number_of_particles]);
                std::copy(tmp_local_state,
                          tmp_local_state + state_size*this->local_number_of_particles,
                          this->local_state.get());
                std::copy(tmp_local_indices,
                          tmp_local_indices + this->local_number_of_particles,
                          this->local_indices.get());
                std::copy(tmp_local_labels,
                          tmp_local_labels + this->local_number_of_particles,
                          this->local_labels.get());
            }

            particles_utils::partition_extra_z<partsize_t, state_size>(
                    &this->local_state[0],
                    this->local_number_of_particles,
                    partition_interval_size,
                    this->number_particles_per_partition.get(),
                    this->offset_particles_for_partition.get(),
                    [&](const particle_rnumber& z_pos){
                        const int partition_level = this->pInterpolator.pbc_field_layer(z_pos, IDXC_Z);
                        assert(current_partition_interval.first <= partition_level && partition_level < current_partition_interval.second);
                        return partition_level - current_partition_interval.first;
                    },
                    [&](const partsize_t idx1, const partsize_t idx2){
                        std::swap(this->local_indices[idx1], this->local_indices[idx2]);
                        std::swap(this->local_labels[idx1], this->local_labels[idx2]);
                    });

            delete[] tmp_local_state;
            delete[] tmp_local_indices;
            delete[] tmp_local_labels;

            int file_layout_result = this->setParticleFileLayout(src->getParticleFileLayout());
            variable_used_only_in_assert(file_layout_result);
            assert(file_layout_result == EXIT_SUCCESS);
            return EXIT_SUCCESS;
        }

        int reset_labels()
        {
            std::copy(
                    this->local_indices.get(),
                    this->local_indices.get() + this->getLocalNumberOfParticles(),
                    this->local_labels.get());
            return EXIT_SUCCESS;
        }

        /** \brief Filter particle set versus small list of particle indices.
         *
         * Using this method, we can select particles from a `src` particle set into the current object.
         * The code will work reasonably as long as a small list of "indices of interest" is used.
         *
         * \warning The indices of interest must be existing indices, otherwise the object will be broken.
         * In principle we could have a general algorithm that could handle anything, but it would be a messy code,
         * which would need multiple passes of the data to figure out the new indices.
         * It's easier to just clean up the data before feeding it to this method.
         *
         * \warning `indices_of_interest` must be in increasing order.
         *
         */
        int init_as_subset_of(
                abstract_particle_set &src,
                const std::vector<partsize_t> indices_of_interest,
                bool use_set_complement)
        {
            TIMEZONE("particle_set::init_as_subset_of version 2");
            assert(indices_of_interest.size() > 0);
            assert(partsize_t(indices_of_interest.size()) < src.getTotalNumberOfParticles());
            // ensure that indices of interest are sorted
            particle_rnumber *tmp_local_state = new particle_rnumber[state_size * src.getLocalNumberOfParticles()];
            partsize_t *tmp_local_indices = new partsize_t[src.getLocalNumberOfParticles()];
            partsize_t *tmp_local_labels = new partsize_t[src.getLocalNumberOfParticles()];
            this->local_number_of_particles = 0;

            // set new number of particles
            if (use_set_complement)
                this->total_number_of_particles = src.getTotalNumberOfParticles() - indices_of_interest.size();
            else
                this->total_number_of_particles = indices_of_interest.size();

            // select interesting particles
            for (partsize_t ii = 0; ii < partsize_t(src.getLocalNumberOfParticles()); ii++)
            {
                partsize_t src_label = src.getParticleLabels()[ii];
                partsize_t src_index = src.getParticleIndices()[ii];

                bool index_is_interesting = false;
                // find out if index is interesting
                partsize_t iii = 0;
                for (; iii < partsize_t(indices_of_interest.size()); iii++)
                {
                    index_is_interesting = (indices_of_interest[iii] == src_index);
                    if (indices_of_interest[iii] >= src_index)
                        break;
                }
                bool copy_data = false;

                if (use_set_complement && (!index_is_interesting)) // we are effectively deleting particles present in "indices_of_interest"
                    copy_data = true;
                if ((!use_set_complement) && index_is_interesting) // we are only keeping particles present in "indices_of_interest"
                    copy_data = true;
                if (copy_data)
                {
                    // set new index
                    tmp_local_indices[this->local_number_of_particles] = src_index;
                    if (use_set_complement)
                        tmp_local_indices[this->local_number_of_particles] -= iii;
                    else
                        tmp_local_indices[this->local_number_of_particles] = iii;
                    // copy particle with label src_label
                    tmp_local_labels[this->local_number_of_particles] = src_label;
                    std::copy(src.getParticleState()+state_size*ii,
                              src.getParticleState()+state_size*(ii+1),
                              tmp_local_state + state_size*this->local_number_of_particles);
                    this->local_number_of_particles++;
                }
            }
            //DEBUG_MSG("particle_set::init_as_subset_of --- after particle selection local number of particles is %d\n",
            //        this->local_number_of_particles);
            //for (partsize_t ii = 0; ii < this->local_number_of_particles; ii++)
            //    DEBUG_MSG(" --- ii = %d, tmp_local_labels[ii] = %d, tmp_local_indices[ii] = %d\n",
            //            ii,
            //            tmp_local_labels[ii],
            //            tmp_local_indices[ii]);

            // now we actually put the data "here"
            if (this->local_number_of_particles > 0)
            {
                this->local_state.reset(new particle_rnumber[state_size*this->local_number_of_particles]);
                this->local_indices.reset(new partsize_t[this->local_number_of_particles]);
                this->local_labels.reset(new partsize_t[this->local_number_of_particles]);
                std::copy(tmp_local_state,
                          tmp_local_state + state_size*this->local_number_of_particles,
                          this->local_state.get());
                std::copy(tmp_local_indices,
                          tmp_local_indices + this->local_number_of_particles,
                          this->local_indices.get());
                std::copy(tmp_local_labels,
                          tmp_local_labels + this->local_number_of_particles,
                          this->local_labels.get());
            }

            particles_utils::partition_extra_z<partsize_t, state_size>(
                    &this->local_state[0],
                    this->local_number_of_particles,
                    partition_interval_size,
                    this->number_particles_per_partition.get(),
                    this->offset_particles_for_partition.get(),
                    [&](const particle_rnumber& z_pos){
                        const int partition_level = this->pInterpolator.pbc_field_layer(z_pos, IDXC_Z);
                        assert(current_partition_interval.first <= partition_level && partition_level < current_partition_interval.second);
                        return partition_level - current_partition_interval.first;
                    },
                    [&](const partsize_t idx1, const partsize_t idx2){
                        std::swap(this->local_indices[idx1], this->local_indices[idx2]);
                        std::swap(this->local_labels[idx1], this->local_labels[idx2]);
                    });

            delete[] tmp_local_state;
            delete[] tmp_local_indices;
            delete[] tmp_local_labels;

            // update particle file layout
            std::vector<hsize_t> tmp_file_layout(1);
            tmp_file_layout[0] = hsize_t(this->total_number_of_particles);

            int file_layout_result = this->setParticleFileLayout(tmp_file_layout);
            variable_used_only_in_assert(file_layout_result);
            assert(file_layout_result == EXIT_SUCCESS);
            return EXIT_SUCCESS;
        }

        particle_rnumber getSpatialLowLimitZ() const
        {
            return this->pInterpolator.getSpatialLowLimitZ();
        }

        particle_rnumber getSpatialUpLimitZ() const
        {
            return this->pInterpolator.getSpatialUpLimitZ();
        }

        p2p_distr_mpi<partsize_t, particle_rnumber> *getP2PComputer()
        {
            return &(this->p2pComputer);
        }

        std::vector<partsize_t> selectLocalParticleFromFewIndices(
                const std::vector<partsize_t> &inGlobalIndicesToDelete)
        {
            std::vector<partsize_t> local_indices;
            for (partsize_t i=0; i<this->getLocalNumberOfParticles(); i++)
                for (partsize_t ii=0; ii<partsize_t(inGlobalIndicesToDelete.size()); ii++)
                {
                    if (this->getParticleIndices()[i] == inGlobalIndicesToDelete[ii])
                        local_indices.push_back(inGlobalIndicesToDelete[ii]);
                }
            return local_indices;
        }
};

#endif//PARTICLE_SET_HPP

