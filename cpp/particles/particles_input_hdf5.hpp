/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLES_INPUT_HDF5_HPP
#define PARTICLES_INPUT_HDF5_HPP

#include "abstract_particles_input.hpp"
#include "alltoall_exchanger.hpp"
#include "hdf5_tools.hpp"

template <class partsize_t, class real_number, int size_particle_positions, int size_particle_rhs>
class particles_input_hdf5 : public abstract_particles_input<partsize_t, real_number> {
    const std::string filename;

    MPI_Comm mpi_comm;
    int my_rank;
    int nb_processes;

    hsize_t total_number_of_particles;
    hsize_t nb_rhs;
    partsize_t nb_particles_for_me;
    std::vector<hsize_t> particle_file_layout;   // to hold the shape of initial condition array

    std::unique_ptr<real_number[]> my_particles_positions;
    std::unique_ptr<partsize_t[]> my_particles_indexes;
    std::unique_ptr<partsize_t[]> my_particles_labels;
    std::vector<std::unique_ptr<real_number[]>> my_particles_rhs;

public:
    particles_input_hdf5(
            const MPI_Comm in_mpi_comm,
            const std::string& inFilename,
            const std::string& inDatanameState,
            const std::string& inDatanameRhs,
            const real_number my_spatial_low_limit,
            const real_number my_spatial_up_limit)
        : particles_input_hdf5(
                in_mpi_comm,
                inFilename,
                inDatanameState,
                inDatanameRhs,
                BuildLimitsAllProcesses<real_number>(
                    in_mpi_comm,
                    my_spatial_low_limit,
                    my_spatial_up_limit)){
    }

    particles_input_hdf5(
            const MPI_Comm in_mpi_comm,
            const std::string& inFilename,
            const std::string& inDatanameState,
            const std::string& inDatanameRhs,
            const std::vector<real_number>& in_spatial_limit_per_proc)
        : filename(inFilename),
          mpi_comm(in_mpi_comm),
          my_rank(-1),
          nb_processes(-1),
          total_number_of_particles(0),
          nb_particles_for_me(0){
        TIMEZONE("particles_input_hdf5");

        AssertMpi(MPI_Comm_rank(mpi_comm, &my_rank));
        AssertMpi(MPI_Comm_size(mpi_comm, &nb_processes));
        assert(int(in_spatial_limit_per_proc.size()) == nb_processes+1);

        hid_t plist_id_par = H5Pcreate(H5P_FILE_ACCESS);
        assert(plist_id_par >= 0);
        {
            int retTest = H5Pset_fapl_mpio(plist_id_par, mpi_comm, MPI_INFO_NULL);
            variable_used_only_in_assert(retTest);
            assert(retTest >= 0);
        }

        hid_t particle_file = H5Fopen(filename.c_str(), H5F_ACC_RDONLY, plist_id_par);
        assert(particle_file >= 0);

        {
            TIMEZONE("state");
            hid_t dset = H5Dopen(particle_file, inDatanameState.c_str(), H5P_DEFAULT);
            assert(dset >= 0);

            hid_t dspace = H5Dget_space(dset); // copy?
            assert(dspace >= 0);

            hid_t space_dim = H5Sget_simple_extent_ndims(dspace);
            assert(space_dim >= 2);

            std::vector<hsize_t> state_dim_array(space_dim);
            int hdfret = H5Sget_simple_extent_dims(dspace, &state_dim_array[0], NULL);
            variable_used_only_in_assert(hdfret);
            assert(hdfret >= 0);
            // Last value is the position dim of the particles
            assert(state_dim_array.back() == size_particle_positions);

            // compute total number of particles, store initial condition array shape
            total_number_of_particles = 1;
            particle_file_layout.resize(state_dim_array.size()-1);
            for (size_t idx_dim = 0; idx_dim < state_dim_array.size()-1; ++idx_dim){
                total_number_of_particles *= state_dim_array[idx_dim];
                particle_file_layout[idx_dim] = state_dim_array[idx_dim];
            }

            hdfret = H5Sclose(dspace);
            assert(hdfret >= 0);
            hdfret = H5Dclose(dset);
            assert(hdfret >= 0);
        }
        {
            TIMEZONE("rhs");
            hid_t dset = H5Dopen(particle_file, inDatanameRhs.c_str(), H5P_DEFAULT);
            assert(dset >= 0);
            hid_t dspace = H5Dget_space(dset); // copy?
            assert(dspace >= 0);

            hid_t rhs_dim = H5Sget_simple_extent_ndims(dspace);
            // Chichi comment: this assertion will fail in general, there's no reason for it.
                //assert(rhs_dim == 4);
            std::vector<hsize_t> rhs_dim_array(rhs_dim);

            // Chichi comment: wouldn't &rhs_dim_array.front() be safer?
            int hdfret = H5Sget_simple_extent_dims(dspace, &rhs_dim_array[0], NULL);
            variable_used_only_in_assert(hdfret);
            assert(hdfret >= 0);
            assert(rhs_dim_array.back() == size_particle_rhs);
            // Chichi comment: this assertion will fail in general
            //assert(rhs_dim_array.front() == 1);
            nb_rhs = rhs_dim_array[0];

            hdfret = H5Sclose(dspace);
            assert(hdfret >= 0);
            hdfret = H5Dclose(dset);
            assert(hdfret >= 0);
        }

        particles_utils::IntervalSplitter<hsize_t> load_splitter(total_number_of_particles, nb_processes, my_rank);

        static_assert(std::is_same<real_number, double>::value
                      || std::is_same<real_number, float>::value, "real_number must be double or float");
        const hid_t type_id = hdf5_tools::hdf5_type_id<real_number>();

        /// Load the data
        std::unique_ptr<real_number[]> split_particles_positions;
        if(load_splitter.getMySize()){
            split_particles_positions.reset(new real_number[load_splitter.getMySize()*size_particle_positions]);
        }

        {
            TIMEZONE("state-read");
            hid_t dset = H5Dopen(particle_file, inDatanameState.c_str(), H5P_DEFAULT);
            assert(dset >= 0);

            hsize_t file_space_dims[2] = {total_number_of_particles, size_particle_positions};
            hid_t rspace = H5Screate_simple(2, file_space_dims, NULL);
            assert(rspace >= 0);

            hsize_t offset[2] = {load_splitter.getMyOffset(), 0};
            hsize_t mem_dims[2] = {load_splitter.getMySize(), size_particle_positions};

            hid_t mspace = H5Screate_simple(2, &mem_dims[0], NULL);
            assert(mspace >= 0);

            int rethdf = H5Sselect_hyperslab(rspace, H5S_SELECT_SET, offset,
                                             NULL, mem_dims, NULL);
            variable_used_only_in_assert(rethdf);
            assert(rethdf >= 0);
            rethdf = H5Dread(dset, type_id, mspace, rspace, H5P_DEFAULT, split_particles_positions.get());
            assert(rethdf >= 0);

            rethdf = H5Sclose(rspace);
            assert(rethdf >= 0);
            rethdf = H5Dclose(dset);
            assert(rethdf >= 0);
        }
        std::vector<std::unique_ptr<real_number[]>> split_particles_rhs(nb_rhs);
        {
            TIMEZONE("rhs-read");
            hid_t dset = H5Dopen(particle_file, inDatanameRhs.c_str(), H5P_DEFAULT);
            assert(dset >= 0);
            hsize_t file_space_dims[3] = {nb_rhs, total_number_of_particles, size_particle_rhs};
            hid_t rspace = H5Screate_simple(3, file_space_dims, NULL);
            assert(rspace >= 0);

            for(hsize_t idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                if(load_splitter.getMySize()){
                    split_particles_rhs[idx_rhs].reset(new real_number[load_splitter.getMySize()*size_particle_rhs]);
                }

                hsize_t offset[3] = {idx_rhs, load_splitter.getMyOffset(), 0};
                hsize_t mem_dims[3] = {1, load_splitter.getMySize(), size_particle_rhs};

                hid_t mspace = H5Screate_simple( 3, &mem_dims[0], NULL);
                assert(mspace >= 0);

                int rethdf = H5Sselect_hyperslab( rspace, H5S_SELECT_SET, offset,
                                                 NULL, mem_dims, NULL);
                variable_used_only_in_assert(rethdf);
                assert(rethdf >= 0);
                rethdf = H5Dread(dset, type_id, mspace, rspace, H5P_DEFAULT, split_particles_rhs[idx_rhs].get());
                assert(rethdf >= 0);

                rethdf = H5Sclose(mspace);
                assert(rethdf >= 0);
            }

            int rethdf = H5Sclose(rspace);
            assert(rethdf >= 0);
            rethdf = H5Dclose(dset);
            variable_used_only_in_assert(rethdf);
            assert(rethdf >= 0);
        }

        std::unique_ptr<partsize_t[]> split_particles_indexes;
        if(load_splitter.getMySize()){
            split_particles_indexes.reset(new partsize_t[load_splitter.getMySize()]);
        }
        for(partsize_t idx_part = 0 ; idx_part < partsize_t(load_splitter.getMySize()) ; ++idx_part){
            split_particles_indexes[idx_part] = idx_part + partsize_t(load_splitter.getMyOffset());
        }

        // Permute
        std::vector<partsize_t> nb_particles_per_proc(nb_processes);
        {
            TIMEZONE("partition");

            const real_number spatial_box_offset = in_spatial_limit_per_proc[0];
            const real_number spatial_box_width = in_spatial_limit_per_proc[nb_processes] - in_spatial_limit_per_proc[0];

            partsize_t previousOffset = 0;
            for(int idx_proc = 0 ; idx_proc < nb_processes-1 ; ++idx_proc){
                const real_number limitPartitionShifted = in_spatial_limit_per_proc[idx_proc+1]-spatial_box_offset;
                const partsize_t localOffset = particles_utils::partition_extra<partsize_t, size_particle_positions>(
                                                &split_particles_positions[previousOffset*size_particle_positions],
                                                 partsize_t(load_splitter.getMySize())-previousOffset,
                                                 [&](const real_number val[]){
                    const real_number shiftPos = val[IDXC_Z]-spatial_box_offset;
                    const real_number nbRepeat = floor(shiftPos/spatial_box_width);
                    const real_number posInBox = shiftPos - (spatial_box_width*nbRepeat);
                    return posInBox < limitPartitionShifted;
                },
                [&](const partsize_t idx1, const partsize_t idx2){
                    std::swap(split_particles_indexes[idx1], split_particles_indexes[idx2]);
                    for(int idx_rhs = 0 ; idx_rhs < int(nb_rhs) ; ++idx_rhs){
                        for(int idx_val = 0 ; idx_val < size_particle_rhs ; ++idx_val){
                            std::swap(split_particles_rhs[idx_rhs][idx1*size_particle_rhs + idx_val],
                                      split_particles_rhs[idx_rhs][idx2*size_particle_rhs + idx_val]);
                        }
                    }
                }, previousOffset);

                nb_particles_per_proc[idx_proc] = localOffset;
                previousOffset += localOffset;
            }
            nb_particles_per_proc[nb_processes-1] = partsize_t(load_splitter.getMySize()) - previousOffset;
        }

        {
            TIMEZONE("exchanger");
            alltoall_exchanger exchanger(mpi_comm, std::move(nb_particles_per_proc));
            // nb_particles_per_processes cannot be used after due to move
            nb_particles_for_me = exchanger.getTotalToRecv();

            if(nb_particles_for_me){
                my_particles_positions.reset(new real_number[exchanger.getTotalToRecv()*size_particle_positions]);
            }
            exchanger.alltoallv<real_number>(split_particles_positions.get(), my_particles_positions.get(), size_particle_positions);
            delete[] split_particles_positions.release();

            if(nb_particles_for_me){
                my_particles_indexes.reset(new partsize_t[exchanger.getTotalToRecv()]);
            }
            exchanger.alltoallv<partsize_t>(split_particles_indexes.get(), my_particles_indexes.get());
            delete[] split_particles_indexes.release();

            my_particles_rhs.resize(nb_rhs);
            for(int idx_rhs = 0 ; idx_rhs < int(nb_rhs) ; ++idx_rhs){
                if(nb_particles_for_me){
                    my_particles_rhs[idx_rhs].reset(new real_number[exchanger.getTotalToRecv()*size_particle_rhs]);
                }
                exchanger.alltoallv<real_number>(split_particles_rhs[idx_rhs].get(), my_particles_rhs[idx_rhs].get(), size_particle_rhs);
            }
        }

        {
            TIMEZONE("close");
            int hdfret = H5Fclose(particle_file);
            variable_used_only_in_assert(hdfret);
            assert(hdfret >= 0);
            hdfret = H5Pclose(plist_id_par);
            assert(hdfret >= 0);
        }

        // clone indices in labels:
        my_particles_labels.reset(new partsize_t[this->getLocalNbParticles()]);
        std::copy(my_particles_indexes.get(),
                  my_particles_indexes.get()+this->getLocalNbParticles(),
                  my_particles_labels.get());
    }

    ~particles_input_hdf5() noexcept(false){
    }

    partsize_t getTotalNbParticles() final{
        return partsize_t(total_number_of_particles);
    }

    partsize_t getLocalNbParticles() final{
        return nb_particles_for_me;
    }

    int getNbRhs() final{
        return int(nb_rhs);
    }

    std::unique_ptr<real_number[]> getMyParticles() final {
        assert(my_particles_positions != nullptr || nb_particles_for_me == 0);
        return std::move(my_particles_positions);
    }

    std::vector<std::unique_ptr<real_number[]>> getMyRhs() final {
        assert(my_particles_rhs.size() == nb_rhs);
        return std::move(my_particles_rhs);
    }

    std::unique_ptr<partsize_t[]> getMyParticlesIndexes() final {
        assert(my_particles_indexes != nullptr || nb_particles_for_me == 0);
        return std::move(my_particles_indexes);
    }

    std::unique_ptr<partsize_t[]> getMyParticlesLabels() final {
        assert(my_particles_labels != nullptr || nb_particles_for_me == 0);
        return std::move(my_particles_labels);
    }

    std::vector<hsize_t> getParticleFileLayout(){
        return std::vector<hsize_t>(this->particle_file_layout);
    }
};

#endif
