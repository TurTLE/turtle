/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLES_INPUT_RANDOM_HPP
#define PARTICLES_INPUT_RANDOM_HPP

#include "particles/abstract_particles_input.hpp"

#include <mpi.h>
#include <cassert>
#include <random>

template <class partsize_t, class particle_rnumber, int state_size>
class particles_input_random: public abstract_particles_input<partsize_t, particle_rnumber>
{
    private:
        MPI_Comm comm;
        int myrank, nprocs;

        hsize_t total_number_of_particles;
        hsize_t number_rhs;
        partsize_t local_number_of_particles;

        std::unique_ptr<particle_rnumber[]> local_particle_state;
        std::unique_ptr<partsize_t[]> local_particle_index;
        std::unique_ptr<partsize_t[]> local_particle_label;
        std::vector<std::unique_ptr<particle_rnumber[]>> local_particle_rhs;
    public:
        particles_input_random(
                const MPI_Comm in_mpi_comm,
                const partsize_t NPARTICLES,
                const int rseed,
                const particle_rnumber my_spatial_low_limit,
                const particle_rnumber my_spatial_up_limit):
            comm(in_mpi_comm)
        {
            TIMEZONE("particles_input_random::particles_input_random");
            assert(state_size >= 3);
            static_assert((std::is_same<particle_rnumber, double>::value ||
                           std::is_same<particle_rnumber, float>::value),
                          "real_number must be double or float");
            AssertMpi(MPI_Comm_rank(comm, &myrank));
            AssertMpi(MPI_Comm_size(comm, &nprocs));
            std::vector<particle_rnumber> in_spatial_limit_per_prod = BuildLimitsAllProcesses<particle_rnumber>(
                    comm, my_spatial_low_limit, my_spatial_up_limit);
            assert(int(in_spatial_limit_per_prod.size()) == nprocs+1);

            // initialize a separate random number generator for each MPI process
            std::mt19937_64 rgen;
            const double twopi = 4*acos(0);
            // positions are uniformly distributed within 2pi cube
            // TODO: provide possibilities for different rectangle sizes
            std::uniform_real_distribution<particle_rnumber> udist(0.0, twopi);

            // other state components are normally distributed (presumably they're velocities)
            std::normal_distribution<particle_rnumber> gdist;

            // seed random number generators such that no seed is ever repeated if we change the value of rseed.
            // basically use a multi-dimensional array indexing technique to come up with actual seed.
            // Note: this method is not invariant to the number of MPI processes!
            int current_seed = (
                    rseed * (this->nprocs) +
                    this->myrank);
            rgen.seed(current_seed);

            this->total_number_of_particles = NPARTICLES;

            // we know the total number of particles, but we want to generate particle locations in parallel.
            // so we need a preliminary distributor of particles, location-agnostic:
            particles_utils::IntervalSplitter<hsize_t> load_splitter(
                    this->total_number_of_particles, this->nprocs, this->myrank);

            // allocate array for preliminary particle data
            std::unique_ptr<particle_rnumber[]> split_particle_state;
            if(load_splitter.getMySize())
            {
                split_particle_state.reset(new particle_rnumber[load_splitter.getMySize()*state_size]);
            }

            // allocate and populate array for preliminary particle indices
            std::unique_ptr<partsize_t[]> split_particle_index;
            if(load_splitter.getMySize())
            {
                split_particle_index.reset(new partsize_t[load_splitter.getMySize()]);
            }
            for(partsize_t idx = 0 ; idx < partsize_t(load_splitter.getMySize()) ; idx++){
                split_particle_index[idx] = idx + partsize_t(load_splitter.getMyOffset());
            }

            // generate random particle states
            {
                TIMEZONE("particles_input_random::generate random numbers");
                if (state_size == 15)
                    // deformation tensor
                {
                    for (partsize_t idx=0; idx < partsize_t(load_splitter.getMySize()); idx++)
                    {
                        // positions are uniformly distributed within cube
                        for (int cc=0; cc < 3; cc++)
                            split_particle_state[idx*state_size + cc] = udist(rgen);
                        // Q matrix is initially identity
                        split_particle_state[idx*state_size +  3] = particle_rnumber(1.0);
                        split_particle_state[idx*state_size +  4] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  5] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  6] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  7] = particle_rnumber(1.0);
                        split_particle_state[idx*state_size +  8] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  9] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 10] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 11] = particle_rnumber(1.0);
                        // log-stretching factors are initially 0
                        split_particle_state[idx*state_size + 12] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 13] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 14] = particle_rnumber(0.0);
                    }
                }
                else
                {
                    for (partsize_t idx=0; idx < partsize_t(load_splitter.getMySize()); idx++)
                    {
                        // positions are uniformly distributed within cube
                        for (int cc=0; cc < 3; cc++)
                            split_particle_state[idx*state_size + cc] = udist(rgen);
                        // velocities/orientations are normally distributed
                        // TODO: add option for normalization of orientation vectors etc
                        for (int cc = 3; cc < state_size; cc++)
                            split_particle_state[idx*state_size + cc] = gdist(rgen);
                    }
                }
            }


            /*************************************************/
            // meta: permutation and echange regions are copy/pasted from particles_input_hdf5
            // the two blocks should agree (other than type/variable renaming, and the removal of rhs stuff).
            // Permute
            std::vector<particle_rnumber> in_spatial_limit_per_proc = BuildLimitsAllProcesses<particle_rnumber>(
                    this->comm,
                    my_spatial_low_limit,
                    my_spatial_up_limit);
            std::vector<partsize_t> nb_particles_per_proc(this->nprocs);
            {
                TIMEZONE("particles_input_random::partition");

                const particle_rnumber spatial_box_offset = in_spatial_limit_per_proc[0];
                const particle_rnumber spatial_box_width =
                    in_spatial_limit_per_proc[this->nprocs] - in_spatial_limit_per_proc[0];

                partsize_t previousOffset = 0;
                for(int idx_proc = 0 ; idx_proc < this->nprocs-1 ; ++idx_proc){
                    const particle_rnumber limitPartitionShifted =
                            in_spatial_limit_per_proc[idx_proc+1] - spatial_box_offset;
                    const partsize_t localOffset = particles_utils::partition_extra<partsize_t, state_size>(
                            &split_particle_state[previousOffset*state_size],
                            partsize_t(load_splitter.getMySize())-previousOffset,
                            [&](const particle_rnumber val[]){
                                    const particle_rnumber shiftPos = val[IDXC_Z] - spatial_box_offset;
                                    const particle_rnumber nbRepeat = floor(shiftPos/spatial_box_width);
                                    const particle_rnumber posInBox = shiftPos - (spatial_box_width*nbRepeat);
                                    return posInBox < limitPartitionShifted;
                            },
                            [&](const partsize_t idx1, const partsize_t idx2){
                                    std::swap(split_particle_index[idx1],
                                              split_particle_index[idx2]);
                            },
                            previousOffset);

                    nb_particles_per_proc[idx_proc] = localOffset;
                    previousOffset += localOffset;
                }
                nb_particles_per_proc[this->nprocs-1] = partsize_t(load_splitter.getMySize()) - previousOffset;
            }

            {
                TIMEZONE("particles_input_random::exchanger");
                alltoall_exchanger exchanger(
                        this->comm,
                        std::move(nb_particles_per_proc));
                // nb_particles_per_processes cannot be used after due to move

                this->local_number_of_particles = exchanger.getTotalToRecv();

                if(this->local_number_of_particles){
                    this->local_particle_state.reset(new particle_rnumber[exchanger.getTotalToRecv()*state_size]);
                }
                exchanger.alltoallv<particle_rnumber>(
                        split_particle_state.get(),
                        this->local_particle_state.get(),
                        state_size);
                delete[] split_particle_state.release();

                if(this->local_number_of_particles){
                    this->local_particle_index.reset(new partsize_t[exchanger.getTotalToRecv()]);
                }
                exchanger.alltoallv<partsize_t>(
                        split_particle_index.get(),
                        this->local_particle_index.get());
                delete[] split_particle_index.release();
            }
            /*************************************************/


        // clone indices in labels:
        local_particle_label.reset(new partsize_t[this->getLocalNbParticles()]);
        std::copy(local_particle_index.get(),
                  local_particle_index.get()+this->getLocalNbParticles(),
                  local_particle_label.get());
        }

        partsize_t getTotalNbParticles()
        {
            return this->total_number_of_particles;
        }
        partsize_t getLocalNbParticles()
        {
            return this->local_number_of_particles;
        }
        int getNbRhs()
        {
            return 0;
        }

        std::unique_ptr<particle_rnumber[]> getMyParticles()
        {
            assert(this->local_particle_state != nullptr || this->local_number_of_particles == 0);
            return std::move(this->local_particle_state);
        }

        std::unique_ptr<partsize_t[]> getMyParticlesIndexes()
        {
            assert(this->local_particle_index != nullptr || this->local_number_of_particles == 0);
            return std::move(this->local_particle_index);
        }

        std::unique_ptr<partsize_t[]> getMyParticlesLabels()
        {
            assert(this->local_particle_label != nullptr || this->local_number_of_particles == 0);
            return std::move(this->local_particle_label);
        }

        std::vector<std::unique_ptr<particle_rnumber[]>> getMyRhs()
        {
            return std::move(std::vector<std::unique_ptr<particle_rnumber[]>>());
        }

        std::vector<hsize_t> getParticleFileLayout()
        {
            return std::vector<hsize_t>({
                    hsize_t(this->getTotalNbParticles())});
        }
};


#endif
