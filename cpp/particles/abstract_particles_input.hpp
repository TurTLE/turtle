/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef ABSTRACT_PARTICLES_INPUT_HPP
#define ABSTRACT_PARTICLES_INPUT_HPP

#include <tuple>

template <class partsize_t, class real_number>
class abstract_particles_input {
public:
    virtual ~abstract_particles_input() noexcept(false){}

    virtual partsize_t getTotalNbParticles()  = 0;
    virtual partsize_t getLocalNbParticles()  = 0;
    virtual int getNbRhs()  = 0;

    virtual std::unique_ptr<real_number[]> getMyParticles()  = 0;
    virtual std::unique_ptr<partsize_t[]> getMyParticlesIndexes()  = 0;
    virtual std::unique_ptr<partsize_t[]> getMyParticlesLabels()  = 0;
    virtual std::vector<std::unique_ptr<real_number[]>> getMyRhs()  = 0;
    virtual std::vector<hsize_t> getParticleFileLayout() = 0;
};


#endif
