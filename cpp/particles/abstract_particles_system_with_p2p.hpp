/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef ABSTRACT_PARTICLES_SYSTEM_WITH_P2P_HPP
#define ABSTRACT_PARTICLES_SYSTEM_WITH_P2P_HPP

#include "abstract_particles_system.hpp"

template <class partsize_t, class real_number, class p2p_computer_class>
class abstract_particles_system_with_p2p : public abstract_particles_system<partsize_t,real_number> {
public:
    virtual p2p_computer_class& getP2PComputer() = 0;
    virtual const p2p_computer_class& getP2PComputer() const = 0;
};

#endif // ABSTRACT_PARTICLES_SYSTEM_WITH_P2P_HPP
