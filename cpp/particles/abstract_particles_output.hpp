/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef ABSTRACT_PARTICLES_OUTPUT
#define ABSTRACT_PARTICLES_OUTPUT

#include "alltoall_exchanger.hpp"
#include "env_utils.hpp"

#include <algorithm>
#include <cstddef>
#include <hdf5.h>

/** \brief Class to handle distributed output of particle data
 *
 * \tparam partsize_t data type of particle index
 * \tparam position_type data type of particle positions
 * \tparam output_type data type of output (typically same as position_type, but it may be different)
 * \tparam size_particle_positions int, size of particle state
 *
 */
template <class partsize_t, class position_type, class output_type, int size_particle_positions>
class abstract_particles_output {
    MPI_Comm mpi_com;
    MPI_Comm mpi_com_writer;

    int my_rank;
    int nb_processes;

    const partsize_t total_nb_particles;
    const int nb_rhs;

    std::unique_ptr<std::pair<partsize_t,partsize_t>[]> buffer_indexes_send;
    std::unique_ptr<position_type[]> buffer_particles_positions_send;
    std::vector<std::unique_ptr<output_type[]>> buffer_particles_rhs_send;
    partsize_t size_buffers_send;
    int buffers_size_particle_rhs_send;

    std::unique_ptr<position_type[]> buffer_particles_positions_recv;
    std::vector<std::unique_ptr<output_type[]>> buffer_particles_rhs_recv;
    std::unique_ptr<partsize_t[]> buffer_indexes_recv;
    partsize_t size_buffers_recv;
    int buffers_size_particle_rhs_recv;

    int nb_processes_involved;
    bool current_is_involved;
    partsize_t particles_chunk_per_process;
    partsize_t particles_chunk_current_size;
    partsize_t particles_chunk_current_offset;

protected:
    MPI_Comm& getCom(){
        return mpi_com;
    }

    MPI_Comm& getComWriter(){
        return mpi_com_writer;
    }

    int getNbRhs() const {
        return nb_rhs;
    }

    int getMyRank(){
        return this->my_rank;
    }

    bool isInvolved() const{
        return current_is_involved;
    }

public:
    abstract_particles_output(MPI_Comm in_mpi_com, const partsize_t inTotalNbParticles, const int in_nb_rhs) noexcept(false)
            : mpi_com(in_mpi_com), my_rank(-1), nb_processes(-1),
                total_nb_particles(inTotalNbParticles), nb_rhs(in_nb_rhs),
                buffer_particles_rhs_send(in_nb_rhs), size_buffers_send(0),
                buffers_size_particle_rhs_send(0),
                buffer_particles_rhs_recv(in_nb_rhs), size_buffers_recv(0),
                buffers_size_particle_rhs_recv(0),
                nb_processes_involved(0), current_is_involved(true), particles_chunk_per_process(0),
                particles_chunk_current_size(0), particles_chunk_current_offset(0) {

        AssertMpi(MPI_Comm_rank(mpi_com, &my_rank));
        AssertMpi(MPI_Comm_size(mpi_com, &nb_processes));

        const size_t MinBytesPerProcess = env_utils::GetValue<size_t>("TURTLE_PO_MIN_BYTES", 32 * 1024 * 1024); // Default 32MB
        const size_t ChunkBytes = env_utils::GetValue<size_t>("TURTLE_PO_CHUNK_BYTES", 8 * 1024 * 1024); // Default 8MB
        const int MaxProcessesInvolved = std::min(nb_processes, env_utils::GetValue<int>("TURTLE_PO_MAX_PROCESSES", 128));

        // We split the processes using positions size only
        const size_t totalBytesForPositions = total_nb_particles*size_particle_positions*sizeof(position_type);


        if(MinBytesPerProcess*MaxProcessesInvolved < totalBytesForPositions){
            size_t extraChunkBytes = 1;
            while((MinBytesPerProcess+extraChunkBytes*ChunkBytes)*MaxProcessesInvolved < totalBytesForPositions){
                extraChunkBytes += 1;
            }
            const size_t bytesPerProcess = (MinBytesPerProcess+extraChunkBytes*ChunkBytes);
            particles_chunk_per_process = partsize_t((bytesPerProcess+sizeof(position_type)*size_particle_positions-1)/(sizeof(position_type)*size_particle_positions));
            nb_processes_involved = int((total_nb_particles+particles_chunk_per_process-1)/particles_chunk_per_process);
        }
        // else limit based on minBytesPerProcess
        else{
            nb_processes_involved = std::max(1,std::min(MaxProcessesInvolved,int((totalBytesForPositions+MinBytesPerProcess-1)/MinBytesPerProcess)));
            particles_chunk_per_process = partsize_t((MinBytesPerProcess+sizeof(position_type)*size_particle_positions-1)/(sizeof(position_type)*size_particle_positions));
        }

        // Print out
        if(my_rank == 0){
            DEBUG_MSG("[INFO] Limit of processes involved in the particles ouput = %d (TURTLE_PO_MAX_PROCESSES)\n", MaxProcessesInvolved);
            DEBUG_MSG("[INFO] Minimum bytes per process to write = %llu (TURTLE_PO_MIN_BYTES) for a complete output of = %llu for positions\n", MinBytesPerProcess, totalBytesForPositions);
            DEBUG_MSG("[INFO] Consequently, there are %d processes that actually write data (%d particles per process)\n", nb_processes_involved, particles_chunk_per_process);
        }

        if(my_rank < nb_processes_involved){
            current_is_involved = true;
            particles_chunk_current_offset = my_rank*particles_chunk_per_process;
            assert(particles_chunk_current_offset < total_nb_particles);
            particles_chunk_current_size = std::min(particles_chunk_per_process, total_nb_particles-particles_chunk_current_offset);
            assert(particles_chunk_current_offset + particles_chunk_current_size <= total_nb_particles);
            assert(my_rank != nb_processes_involved-1 || particles_chunk_current_offset + particles_chunk_current_size == total_nb_particles);
        }
        else{
            current_is_involved = false;
            particles_chunk_current_size = 0;
            particles_chunk_current_offset = total_nb_particles;
        }

        AssertMpi( MPI_Comm_split(mpi_com,
                       (current_is_involved ? 1 : MPI_UNDEFINED),
                       my_rank, &mpi_com_writer) );
    }

    virtual ~abstract_particles_output() noexcept(false){
        if(current_is_involved){
            AssertMpi( MPI_Comm_free(&mpi_com_writer) );
        }
    }   

    partsize_t getTotalNbParticles() const {
        return total_nb_particles;
    }

    void releaseMemory(){
        delete[] buffer_indexes_send.release();
        delete[] buffer_particles_positions_send.release();
        size_buffers_send = 0;
        delete[] buffer_indexes_recv.release();
        delete[] buffer_particles_positions_recv.release();
        size_buffers_recv = 0;
        for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
            delete[] buffer_particles_rhs_send[idx_rhs].release();
            delete[] buffer_particles_rhs_recv[idx_rhs].release();
        }
        buffers_size_particle_rhs_send = 0;
        buffers_size_particle_rhs_recv = 0;
    }

    template <int size_particle_rhs>
    void save(
            const position_type input_particles_positions[],
            const std::unique_ptr<output_type[]> input_particles_rhs[],
            const partsize_t index_particles[],
            const partsize_t nb_particles,
            const int idx_time_step){
        TIMEZONE("abstract_particles_output::save");
        assert(total_nb_particles != -1);

        {
            TIMEZONE("sort-to-distribute");

            if(size_buffers_send < nb_particles){
                size_buffers_send = nb_particles;
                buffer_indexes_send.reset(new std::pair<partsize_t,partsize_t>[size_buffers_send]);
                buffer_particles_positions_send.reset(new position_type[size_buffers_send*size_particle_positions]);

                if(buffers_size_particle_rhs_send < size_particle_rhs){
                    buffers_size_particle_rhs_send = size_particle_rhs;
                }
                for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                    buffer_particles_rhs_send[idx_rhs].reset(new output_type[size_buffers_send*buffers_size_particle_rhs_send]);
                }
            }
            else if(buffers_size_particle_rhs_send < size_particle_rhs){
                buffers_size_particle_rhs_send = size_particle_rhs;
                if(size_buffers_send > 0){
                    for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                        buffer_particles_rhs_send[idx_rhs].reset(new output_type[size_buffers_send*buffers_size_particle_rhs_send]);
                    }
                }
            }

            for(partsize_t idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
                buffer_indexes_send[idx_part].first = idx_part;
                buffer_indexes_send[idx_part].second = index_particles[idx_part];
            }

            std::sort(
                    &buffer_indexes_send[0],
                    &buffer_indexes_send[nb_particles],
                    [](const std::pair<partsize_t,partsize_t>& p1,
                       const std::pair<partsize_t,partsize_t>& p2)
                    {
                        return p1.second < p2.second;
                    });

            for(partsize_t idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
                const partsize_t src_idx = buffer_indexes_send[idx_part].first;
                const partsize_t dst_idx = idx_part;

                for(int idx_val = 0 ; idx_val < size_particle_positions ; ++idx_val){
                    buffer_particles_positions_send[dst_idx*size_particle_positions + idx_val]
                            = input_particles_positions[src_idx*size_particle_positions + idx_val];
                }
                for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                    for(int idx_val = 0 ; idx_val < int(size_particle_rhs) ; ++idx_val){
                        buffer_particles_rhs_send[idx_rhs][dst_idx*size_particle_rhs + idx_val]
                                = input_particles_rhs[idx_rhs][src_idx*size_particle_rhs + idx_val];
                    }
                }
            }
        }

        partsize_t* buffer_indexes_send_tmp = reinterpret_cast<partsize_t*>(buffer_indexes_send.get());// trick re-use buffer_indexes_send memory
        std::vector<partsize_t> nb_particles_to_send(nb_processes, 0);
        for(partsize_t idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
            const int dest_proc = int(buffer_indexes_send[idx_part].second/particles_chunk_per_process);
            assert(dest_proc < nb_processes_involved);
            nb_particles_to_send[dest_proc] += 1;
            buffer_indexes_send_tmp[idx_part] = buffer_indexes_send[idx_part].second;
        }

        alltoall_exchanger exchanger(mpi_com, std::move(nb_particles_to_send));
        // nb_particles_to_send is invalid after here

        const int nb_to_receive = exchanger.getTotalToRecv();
        //DEBUG_MSG("nb_to_receive = %d, particles_chunk_current_size = %d\n",
        //        nb_to_receive, particles_chunk_current_size);
        assert(nb_to_receive == particles_chunk_current_size);

        if(size_buffers_recv < nb_to_receive){
            size_buffers_recv = nb_to_receive;
            buffer_indexes_recv.reset(new partsize_t[size_buffers_recv]);
            buffer_particles_positions_recv.reset(new position_type[size_buffers_recv*size_particle_positions]);

            buffers_size_particle_rhs_recv = size_particle_rhs;
            for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                buffer_particles_rhs_recv[idx_rhs].reset(new output_type[size_buffers_recv*buffers_size_particle_rhs_recv]);
            }
        }
        else if(buffers_size_particle_rhs_recv < size_particle_rhs){
            buffers_size_particle_rhs_recv = size_particle_rhs;
            if(size_buffers_recv > 0){
                for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                    buffer_particles_rhs_recv[idx_rhs].reset(new output_type[size_buffers_recv*buffers_size_particle_rhs_recv]);
                }
            }
        }

        {
            TIMEZONE("exchange");
            // Could be done with multiple asynchronous coms
            exchanger.alltoallv<partsize_t>(buffer_indexes_send_tmp, buffer_indexes_recv.get());
            exchanger.alltoallv<position_type>(buffer_particles_positions_send.get(), buffer_particles_positions_recv.get(), size_particle_positions);
            for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                exchanger.alltoallv<output_type>(buffer_particles_rhs_send[idx_rhs].get(), buffer_particles_rhs_recv[idx_rhs].get(), size_particle_rhs);
            }
        }

        // Stop here if not involved
        if(current_is_involved == false){
            assert(nb_to_receive == 0);
            return;
        }

        if(size_buffers_send < nb_to_receive){
            size_buffers_send = nb_to_receive;
            buffer_indexes_send.reset(new std::pair<partsize_t,partsize_t>[size_buffers_send]);
            buffer_particles_positions_send.reset(new position_type[size_buffers_send*size_particle_positions]);
            buffers_size_particle_rhs_send = size_particle_rhs;
            for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                buffer_particles_rhs_send[idx_rhs].reset(new output_type[size_buffers_send*buffers_size_particle_rhs_send]);
            }
        }

        {
            // Local process has received particles in `buffer_particles_positions_recv`.
            // It will now place this data (which is shuffled), in order, in the `buffer_particles_positions_send` array.
            // However, to place the data it uses the particle indices as memory addresses.
            // This is fine in general, but if we use particle indices as labels, and we sometimes delete particles, the particle indices are no longer valid memory addresses.
            // So we need to fix this.
            TIMEZONE("copy-local-order");
            for(partsize_t idx_part = 0 ; idx_part < nb_to_receive ; ++idx_part){
                const partsize_t src_idx = idx_part;
                //DEBUG_MSG("idx_part = %d, buffer_indexes_recv[idx_part] = %d, particles_chunk_current_offset = %d\n",
                //        idx_part,
                //        buffer_indexes_recv[idx_part],
                //        particles_chunk_current_size);
                const partsize_t dst_idx = buffer_indexes_recv[idx_part]-particles_chunk_current_offset;
                //DEBUG_MSG("dst_idx is %d, particles_chunk_current_size is %d\n",
                //        dst_idx, particles_chunk_current_size);
                assert(0 <= dst_idx);
                assert(dst_idx < particles_chunk_current_size);

                for(int idx_val = 0 ; idx_val < size_particle_positions ; ++idx_val){
                    buffer_particles_positions_send[dst_idx*size_particle_positions + idx_val]
                            = buffer_particles_positions_recv[src_idx*size_particle_positions + idx_val];
                }
                for(int idx_rhs = 0 ; idx_rhs < nb_rhs ; ++idx_rhs){
                    for(int idx_val = 0 ; idx_val < int(size_particle_rhs) ; ++idx_val){
                        buffer_particles_rhs_send[idx_rhs][dst_idx*size_particle_rhs + idx_val]
                                = buffer_particles_rhs_recv[idx_rhs][src_idx*size_particle_rhs + idx_val];
                    }
                }
            }
        }

        write(idx_time_step, buffer_particles_positions_send.get(), buffer_particles_rhs_send.data(),
              nb_to_receive, particles_chunk_current_offset, size_particle_rhs);
    }

    virtual void write(const int idx_time_step, const position_type* positions, const std::unique_ptr<output_type[]>* rhs,
                       const partsize_t nb_particles, const partsize_t particles_idx_offset, const int size_particle_rhs) = 0;

    void require_groups(std::string filename, std::string particle_species_name, std::string group_name){
        if (this->current_is_involved) {
            if (this->my_rank == 0){
                bool file_exists = false;
                {
                    struct stat file_buffer;
                    file_exists = (stat(filename.c_str(), &file_buffer) == 0);
                }
                hid_t file_id;
                if (file_exists)
                    file_id = H5Fopen(
                        filename.c_str(),
                        H5F_ACC_RDWR | H5F_ACC_DEBUG,
                        H5P_DEFAULT);
                else
                    file_id = H5Fcreate(
                        filename.c_str(),
                        H5F_ACC_EXCL | H5F_ACC_DEBUG,
                        H5P_DEFAULT,
                        H5P_DEFAULT);
                assert(file_id >= 0);
                bool group_exists = H5Lexists(
                        file_id,
                        particle_species_name.c_str(),
                        H5P_DEFAULT);
                if (!group_exists)
                {
                    hid_t gg = H5Gcreate(
                        file_id,
                        particle_species_name.c_str(),
                        H5P_DEFAULT,
                        H5P_DEFAULT,
                        H5P_DEFAULT);
                    assert(gg >= 0);
                    H5Gclose(gg);
                }
                hid_t gg = H5Gopen(
                        file_id,
                        particle_species_name.c_str(),
                        H5P_DEFAULT);
                assert(gg >= 0);
                group_exists = H5Lexists(
                        gg,
                        group_name.c_str(),
                        H5P_DEFAULT);
                if (!group_exists)
                {
                    hid_t ggg = H5Gcreate(
                        gg,
                        group_name.c_str(),
                        H5P_DEFAULT,
                        H5P_DEFAULT,
                        H5P_DEFAULT);
                    assert(ggg >= 0);
                    H5Gclose(ggg);
                }
                H5Gclose(gg);
                H5Fclose(file_id);

            }
            MPI_Barrier(this->mpi_com_writer);
        }
    }

};

#endif
