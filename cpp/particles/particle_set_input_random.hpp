/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLE_SET_INPUT_RANDOM_HPP
#define PARTICLE_SET_INPUT_RANDOM_HPP

#include <random>
#include "particles/particle_set_input.hpp"

template <class partsize_t, class particle_rnumber, int state_size>
class particle_set_input_random: public particle_set_input<partsize_t, particle_rnumber, state_size>
{
    public:
        particle_set_input_random(
                const MPI_Comm in_mpi_comm,
                const partsize_t NPARTICLES,
                const int rseed,
                const particle_rnumber my_spatial_low_limit,
                const particle_rnumber my_spatial_up_limit):
            particle_set_input<partsize_t, particle_rnumber, state_size>(in_mpi_comm, my_spatial_low_limit, my_spatial_up_limit)
        {
            TIMEZONE("particles_set_input_random::particles_set_input_random");
            // initialize a separate random number generator for each MPI process
            std::mt19937_64 rgen;
            const double twopi = 4*acos(0);
            // positions are uniformly distributed within 2pi cube
            // TODO: provide possibilities for different rectangle sizes
            std::uniform_real_distribution<particle_rnumber> udist(0.0, twopi);

            // other state components are normally distributed (presumably they're velocities)
            std::normal_distribution<particle_rnumber> gdist;

            // seed random number generators such that no seed is ever repeated if we change the value of rseed.
            // basically use a multi-dimensional array indexing technique to come up with actual seed.
            // Note: this method is not invariant to the number of MPI processes!
            int current_seed = (
                    rseed * (this->nprocs) +
                    this->myrank);
            rgen.seed(current_seed);

            this->total_number_of_particles = NPARTICLES;

            // we know the total number of particles, but we want to generate particle locations in parallel.
            // so we need a preliminary distributor of particles, location-agnostic:
            particles_utils::IntervalSplitter<hsize_t> load_splitter(
                    this->total_number_of_particles, this->nprocs, this->myrank);

            // allocate array for preliminary particle data
            std::unique_ptr<particle_rnumber[]> split_particle_state;
            if(load_splitter.getMySize())
            {
                split_particle_state.reset(new particle_rnumber[load_splitter.getMySize()*state_size]);
            }

            // allocate and populate array for preliminary particle indices
            std::unique_ptr<partsize_t[]> split_particle_index;
            if(load_splitter.getMySize()) {
                split_particle_index.reset(new partsize_t[load_splitter.getMySize()]);
            }
            for(partsize_t idx = 0 ; idx < partsize_t(load_splitter.getMySize()) ; idx++){
                split_particle_index[idx] = idx + partsize_t(load_splitter.getMyOffset());
            }

            // allocate and populate array for preliminary particle labels
            std::unique_ptr<partsize_t[]> split_particle_label;
            if(load_splitter.getMySize()) {
                split_particle_label.reset(new partsize_t[load_splitter.getMySize()]);
            }
            for(partsize_t idx = 0 ; idx < partsize_t(load_splitter.getMySize()) ; idx++){
                split_particle_label[idx] = idx + partsize_t(load_splitter.getMyOffset());
            }

            // generate random particle states
            {
                TIMEZONE("particles_input_random::generate random numbers");
                if (state_size == 15)
                    // deformation tensor
                {
                    for (partsize_t idx=0; idx < partsize_t(load_splitter.getMySize()); idx++)
                    {
                        // positions are uniformly distributed within cube
                        for (int cc=0; cc < 3; cc++)
                            split_particle_state[idx*state_size + cc] = udist(rgen);
                        // Q matrix is initially identity
                        split_particle_state[idx*state_size +  3] = particle_rnumber(1.0);
                        split_particle_state[idx*state_size +  4] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  5] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  6] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  7] = particle_rnumber(1.0);
                        split_particle_state[idx*state_size +  8] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size +  9] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 10] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 11] = particle_rnumber(1.0);
                        // log-stretching factors are initially 0
                        split_particle_state[idx*state_size + 12] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 13] = particle_rnumber(0.0);
                        split_particle_state[idx*state_size + 14] = particle_rnumber(0.0);
                    }
                }
                else
                {
                    for (partsize_t idx=0; idx < partsize_t(load_splitter.getMySize()); idx++)
                    {
                        // positions are uniformly distributed within cube
                        for (int cc=0; cc < 3; cc++)
                            split_particle_state[idx*state_size + cc] = udist(rgen);
                        // velocities/orientations are normally distributed
                        // TODO: add option for normalization of orientation vectors etc
                        for (int cc = 3; cc < state_size; cc++)
                            split_particle_state[idx*state_size + cc] = gdist(rgen);
                    }
                }
            }
            this->permute(
                    load_splitter,
                    split_particle_state,
                    split_particle_index,
                    split_particle_label);

        }

        std::vector<hsize_t> getParticleFileLayout()
        {
            return std::vector<hsize_t>({
                    hsize_t(this->getTotalNbParticles())});
        }
};


#endif//PARTICLE_SET_INPUT_RANDOM_HPP
