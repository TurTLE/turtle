/******************************************************************************
*                                                                             *
*  Copyright 2022 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef PARTICLE_SET_INPUT_HDF5_HPP
#define PARTICLE_SET_INPUT_HDF5_HPP

#include <random>
#include "particles/particle_set_input.hpp"

template <class partsize_t, class particle_rnumber, int state_size>
class particle_set_input_hdf5: public particle_set_input<partsize_t, particle_rnumber, state_size>
{
    private:
        const std::string file_name;
        const std::string species_name;
        const int iteration;

        std::vector<hsize_t> file_layout;

    public:
        particle_set_input_hdf5(
                const MPI_Comm in_mpi_comm,
                const std::string& inFileName,
                const std::string& inSpeciesName,
                const int inIteration,
                const particle_rnumber my_spatial_low_limit,
                const particle_rnumber my_spatial_up_limit)
            : particle_set_input<partsize_t, particle_rnumber, state_size>(
                    in_mpi_comm,
                    my_spatial_low_limit,
                    my_spatial_up_limit)
            , file_name(inFileName)
            , species_name(inSpeciesName)
            , iteration(inIteration)
        {
            TIMEZONE("particles_set_input_hdf5::particles_set_input_hdf5");
            hid_t dataset       = H5E_ERROR;
            hid_t particle_file = H5E_ERROR;
            hid_t fspace        = H5E_ERROR;
            hid_t mspace        = H5E_ERROR;
            int ndims           = 0;

            hsize_t *fdims = NULL;
            hsize_t *mdims = NULL;
            hsize_t *foffset = NULL;

            // open file
            hid_t plist_id_par = H5Pcreate(H5P_FILE_ACCESS);
            assert(plist_id_par >= 0);
            {
                int retTest = H5Pset_fapl_mpio(plist_id_par, this->comm, MPI_INFO_NULL);
                variable_used_only_in_assert(retTest);
                assert(retTest >= 0);
            }
            particle_file = H5Fopen(file_name.c_str(), H5F_ACC_RDONLY, plist_id_par);
            assert(particle_file >= 0);
            assert(particle_file != H5E_ERROR);

            // open state dataset
            dataset = H5Dopen(
                    particle_file,
                    (species_name + std::string("/state/") + std::to_string(iteration)).c_str(),
                    H5P_DEFAULT);
            assert(dataset >= 0);
            assert(dataset != H5E_ERROR);
            // read in data space for state dataset
            fspace = H5Dget_space(dataset);
            assert(fspace >= 0);
            assert(fspace != H5E_ERROR);
            // get number of dimensions for state array
            ndims = H5Sget_simple_extent_ndims(fspace);
            fdims = new hsize_t[ndims];
            // read shape of state array
            assert(ndims == H5Sget_simple_extent_dims(fspace, fdims, NULL));
            // confirm the state size is correct
            assert(fdims[ndims-1] == state_size);
            // compute total number of particles, store file layout
            this->total_number_of_particles = 1;
            this->file_layout.resize(ndims-1);
            for (int i=0; i<ndims-1; ++i)
            {
                this->file_layout[i] = fdims[i];
                this->total_number_of_particles *= fdims[i];
            }

            // now we can tell each processor how many particles it should hold
            particles_utils::IntervalSplitter<hsize_t> load_splitter(
                    this->total_number_of_particles, this->nprocs, this->myrank);

            // we can now use a simpler fspace
            H5Sclose(fspace);
            delete[] fdims;
            fdims = new hsize_t[2];
            fdims[0] = this->total_number_of_particles;
            fdims[1] = state_size;
            fspace = H5Screate_simple(2, fdims, NULL);

            // allocate array for preliminary particle data
            std::unique_ptr<particle_rnumber[]> split_particle_state;
            if(load_splitter.getMySize())
            {
                split_particle_state.reset(new particle_rnumber[load_splitter.getMySize()*state_size]);
            }

            // allocate and populate array for preliminary particle indices
            std::unique_ptr<partsize_t[]> split_particle_index;
            if(load_splitter.getMySize()) {
                split_particle_index.reset(new partsize_t[load_splitter.getMySize()]);
            }
            for(partsize_t idx = 0 ; idx < partsize_t(load_splitter.getMySize()) ; idx++){
                split_particle_index[idx] = idx + partsize_t(load_splitter.getMyOffset());
            }

            // allocate and populate array for preliminary particle labels
            std::unique_ptr<partsize_t[]> split_particle_label;
            if(load_splitter.getMySize()) {
                split_particle_label.reset(new partsize_t[load_splitter.getMySize()]);
            }
            for(partsize_t idx = 0 ; idx < partsize_t(load_splitter.getMySize()) ; idx++){
                split_particle_label[idx] = idx + partsize_t(load_splitter.getMyOffset());
            }

            // in memory particle array is flattened, i.e. shaped as "mdims"
            mdims = new hsize_t[2];
            mdims[0] = load_splitter.getMySize();
            mdims[1] = state_size;
            mspace = H5Screate_simple(2, mdims, NULL);

            // file space offset
            foffset = new hsize_t[2];
            foffset[0] = load_splitter.getMyOffset();
            foffset[1] = 0;

            // select local data from file space
            H5Sselect_hyperslab(fspace, H5S_SELECT_SET, foffset, NULL, mdims, NULL);

            // read state data
            H5Dread(
                    dataset,
                    hdf5_tools::hdf5_type_id<particle_rnumber>(),
                    mspace,
                    fspace,
                    H5P_DEFAULT,
                    split_particle_state.get());
            H5Sclose(mspace);
            H5Sclose(fspace);
            H5Dclose(dataset);

            const std::string label_dset_name =
                species_name + std::string("/label/");
            if (H5Lexists(particle_file, label_dset_name.c_str(), H5P_DEFAULT) > 0)
            {
                // open label dataset
                dataset = H5Dopen(
                        particle_file,
                        (label_dset_name + std::to_string(iteration)).c_str(),
                        H5P_DEFAULT);
                if (dataset >= 0 && dataset != H5E_ERROR) {
                    fspace = H5Screate_simple(1, fdims, NULL);
                    mspace = H5Screate_simple(1, mdims, NULL);
                    H5Sselect_hyperslab(fspace, H5S_SELECT_SET, foffset, NULL, mdims, NULL);
                    H5Dread(
                            dataset,
                            hdf5_tools::hdf5_type_id<partsize_t>(),
                            mspace,
                            fspace,
                            H5P_DEFAULT,
                            split_particle_label.get());
                    H5Sclose(mspace);
                    H5Sclose(fspace);
                    H5Dclose(dataset);
                } else {
                    DEBUG_MSG("Could not read labels for species %s and iteration %d; H5Dopen returned %ld\n",
                            species_name.c_str(), iteration, dataset);
                    assert(dataset != H5E_ERROR);
                    assert(dataset >= 0);
                }
            } else {
                    DEBUG_MSG("Didn't find labels for species %s to read. Using default (label = index).\n",
                            species_name.c_str());
            }
            delete[] mdims;
            delete[] fdims;
            H5Fclose(particle_file);

            this->permute(
                    load_splitter,
                    split_particle_state,
                    split_particle_index,
                    split_particle_label);

        }

        std::vector<hsize_t> getParticleFileLayout()
        {
            return this->file_layout;
        }
};


#endif//PARTICLE_SET_INPUT_HDF5_HPP
