/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLE_SOLVER_HPP
#define PARTICLE_SOLVER_HPP

#include "particles/abstract_particle_rhs.hpp"
#include "particles/particles_output_hdf5.hpp"


/** Time-stepping and checkpointing functionality for particle systems.
 *
 * This class implements the universal functionality of a particle solver:
 * time-stepping methods and checkpointing, together with the associated
 * bookkeeping.
 *
 * We rely on predefined functionality of particle set objects, as well as
 * "particle right-hand-side computers".
 *
 * Development note: the `additional_states` member is to be used for
 * Adams-Bashforth time-stepping, not for Runge-Kutta k values.
 * In particular values stored in `additional_states` are used for
 * check-pointing, so there's no sense to allocate this array unless strictly
 * required as for the Adams-Bashforth scheme.
 */

class particle_solver
{
    protected:
        using particle_rnumber = abstract_particle_set::particle_rnumber;
        using partsize_t = abstract_particle_set::partsize_t;

        abstract_particle_set *pset;

        std::vector<std::unique_ptr<particle_rnumber[]>> additional_states;
        const int number_of_additional_states;

        std::string particle_species_name;
        int iteration;

    public:
        particle_solver(
                abstract_particle_set &in_pset,
                const int in_number_of_additional_states):
            pset(&in_pset),
            number_of_additional_states(in_number_of_additional_states)
        {
            this->iteration = 0;
            this->additional_states.resize(number_of_additional_states);
            this->particle_species_name = "tracers0";
        }
        ~particle_solver() noexcept(false){}

        int setIteration(const int i)
        {
            this->iteration = i;
            return EXIT_SUCCESS;
        }
        int getIteration(void)
        {
            return this->iteration;
        }

        std::string getParticleSpeciesName() const
        {
            return this->particle_species_name;
        }
        int setParticleSpeciesName(
                const std::string new_name)
        {
            this->particle_species_name.assign(new_name);
            return EXIT_SUCCESS;
        }

        template <int state_size>
        int writeCheckpoint(
                particles_output_hdf5<partsize_t,
                                      particle_rnumber,
                                      state_size> *particles_output_writer);

        template <int state_size>
        int writeCheckpoint(
                const std::string file_name);

        template <int state_size>
        int writeCheckpointWithLabels(
                const std::string file_name);

        int Euler(
                double dt,
                abstract_particle_rhs &rhs);

        int Heun(
                double dt,
                abstract_particle_rhs &rhs);

        int cRK4(
                double dt,
                abstract_particle_rhs &rhs);
};

#endif//PARTICLE_SOLVER_HPP

