/******************************************************************************
*                                                                             *
*  Copyright 2022 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                                      *
*                                                                             *
******************************************************************************/



#ifndef PARTICLE_SET_INPUT_HPP
#define PARTICLE_SET_INPUT_HPP

#include <mpi.h>
#include <cassert>
#include "particles/abstract_particles_input.hpp"

template <class partsize_t, class particle_rnumber, int state_size>
class particle_set_input: public abstract_particles_input<partsize_t, particle_rnumber>
{
    protected:
        MPI_Comm comm;
        int myrank, nprocs;

        hsize_t total_number_of_particles;
        hsize_t number_rhs;
        partsize_t local_number_of_particles;

        std::unique_ptr<particle_rnumber[]> local_particle_state;
        std::unique_ptr<partsize_t[]> local_particle_index;
        std::unique_ptr<partsize_t[]> local_particle_label;
        std::vector<particle_rnumber> in_spatial_limit_per_proc;
    public:
        ~particle_set_input() noexcept(false){}
        particle_set_input(
                const MPI_Comm in_mpi_comm,
                const particle_rnumber my_spatial_low_limit,
                const particle_rnumber my_spatial_up_limit):
            comm(in_mpi_comm)
        {
            TIMEZONE("particles_input_grid::particles_input_grid");
            assert(state_size >= 3);
            static_assert((std::is_same<particle_rnumber, double>::value ||
                           std::is_same<particle_rnumber, float>::value),
                          "real_number must be double or float");
            AssertMpi(MPI_Comm_rank(comm, &myrank));
            AssertMpi(MPI_Comm_size(comm, &nprocs));
            this->in_spatial_limit_per_proc = BuildLimitsAllProcesses<particle_rnumber>(
                    this->comm,
                    my_spatial_low_limit,
                    my_spatial_up_limit);
            assert(int(in_spatial_limit_per_proc.size()) == nprocs+1);
        }
        int permute(
                particles_utils::IntervalSplitter<hsize_t> load_splitter,
                std::unique_ptr<particle_rnumber[]> &split_particle_state,
                std::unique_ptr<partsize_t[]>       &split_particle_index,
                std::unique_ptr<partsize_t[]>       &split_particle_label)
        {
            // Permute
            std::vector<partsize_t> nb_particles_per_proc(this->nprocs);
            {
                TIMEZONE("particles_input_grid::partition");

                const particle_rnumber spatial_box_offset = in_spatial_limit_per_proc[0];
                const particle_rnumber spatial_box_width =
                    in_spatial_limit_per_proc[this->nprocs] - in_spatial_limit_per_proc[0];

                partsize_t previousOffset = 0;
                for(int idx_proc = 0 ; idx_proc < this->nprocs-1 ; ++idx_proc){
                    const particle_rnumber limitPartitionShifted =
                            in_spatial_limit_per_proc[idx_proc+1] - spatial_box_offset;
                    const partsize_t localOffset = particles_utils::partition_extra<partsize_t, state_size>(
                            &split_particle_state[previousOffset*state_size],
                            partsize_t(load_splitter.getMySize())-previousOffset,
                            [&](const particle_rnumber val[]){
                                    const particle_rnumber shiftPos = val[IDXC_Z] - spatial_box_offset;
                                    const particle_rnumber nbRepeat = floor(shiftPos/spatial_box_width);
                                    const particle_rnumber posInBox = shiftPos - (spatial_box_width*nbRepeat);
                                    return posInBox < limitPartitionShifted;
                            },
                            [&](const partsize_t idx1, const partsize_t idx2){
                                    std::swap(split_particle_index[idx1],
                                              split_particle_index[idx2]);
                                    std::swap(split_particle_label[idx1],
                                              split_particle_label[idx2]);
                            },
                            previousOffset);

                    nb_particles_per_proc[idx_proc] = localOffset;
                    previousOffset += localOffset;
                }
                nb_particles_per_proc[this->nprocs-1] = partsize_t(load_splitter.getMySize()) - previousOffset;
            }

            {
                TIMEZONE("particles_input_grid::exchanger");
                alltoall_exchanger exchanger(
                        this->comm,
                        std::move(nb_particles_per_proc));
                // nb_particles_per_processes cannot be used after due to move

                this->local_number_of_particles = exchanger.getTotalToRecv();

                if(this->local_number_of_particles){
                    this->local_particle_state.reset(new particle_rnumber[exchanger.getTotalToRecv()*state_size]);
                }
                exchanger.alltoallv<particle_rnumber>(
                        split_particle_state.get(),
                        this->local_particle_state.get(),
                        state_size);
                delete[] split_particle_state.release();

                if(this->local_number_of_particles){
                    this->local_particle_index.reset(new partsize_t[exchanger.getTotalToRecv()]);
                }
                exchanger.alltoallv<partsize_t>(
                        split_particle_index.get(),
                        this->local_particle_index.get());
                delete[] split_particle_index.release();

                if(this->local_number_of_particles){
                    this->local_particle_label.reset(new partsize_t[exchanger.getTotalToRecv()]);
                }
                exchanger.alltoallv<partsize_t>(
                        split_particle_label.get(),
                        this->local_particle_label.get());
                delete[] split_particle_label.release();
            }
            return EXIT_SUCCESS;
        }

        partsize_t getTotalNbParticles()
        {
            return this->total_number_of_particles;
        }
        partsize_t getLocalNbParticles()
        {
            return this->local_number_of_particles;
        }
        int getNbRhs()
        {
            return 0;
        }

        std::unique_ptr<particle_rnumber[]> getMyParticles()
        {
            assert(this->local_particle_state != nullptr || this->local_number_of_particles == 0);
            return std::move(this->local_particle_state);
        }

        std::unique_ptr<partsize_t[]> getMyParticlesIndexes()
        {
            assert(this->local_particle_index != nullptr || this->local_number_of_particles == 0);
            return std::move(this->local_particle_index);
        }

        std::unique_ptr<partsize_t[]> getMyParticlesLabels()
        {
            assert(this->local_particle_label != nullptr || this->local_number_of_particles == 0);
            return std::move(this->local_particle_label);
        }

        std::vector<std::unique_ptr<particle_rnumber[]>> getMyRhs()
        {
            return std::move(std::vector<std::unique_ptr<particle_rnumber[]>>());
        }
};


#endif//PARTICLE_SET_INPUT_HPP
