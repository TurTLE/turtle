/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLES_INNER_COMPUTER_HPP
#define PARTICLES_INNER_COMPUTER_HPP

#include <cstring>
#include <cassert>

template <class real_number, class partsize_t>
class particles_inner_computer{
    bool isActive;
    const real_number v0;
    const real_number lambda;
    const real_number lambda1;
    const real_number lambda2;
    const real_number lambda3;

public:
    explicit particles_inner_computer(const real_number inV0):
        isActive(true),
        v0(inV0),
        lambda(0),
        lambda1(0),
        lambda2(0),
        lambda3(0)
    {}
    explicit particles_inner_computer(const real_number inV0, const real_number inLambda):
        isActive(true),
        v0(inV0),
        lambda(inLambda),
        lambda1(0),
        lambda2(0),
        lambda3(0)
    {}
    explicit particles_inner_computer(
            const real_number inV0,
            const real_number inLambda1,
            const real_number inLambda2,
            const real_number inLambda3):
        isActive(true),
        v0(inV0),
        lambda(0),
        lambda1(inLambda1),
        lambda2(inLambda2),
        lambda3(inLambda3)
    {}

    template <int size_particle_positions, int size_particle_rhs>
    void compute_interaction(
            const partsize_t nb_particles,
            const real_number pos_part[],
            real_number rhs_part[]) const;
    // for given orientation and right-hand-side, recompute right-hand-side such
    // that it is perpendicular to the current orientation.
    // this is the job of the Lagrange multiplier terms, hence the
    // "add_Lagrange_multipliers" name of the method.
    template <int size_particle_positions, int size_particle_rhs>
    void add_Lagrange_multipliers(
            const partsize_t nb_particles,
            const real_number pos_part[],
            real_number rhs_part[]) const;
    template <int size_particle_positions, int size_particle_rhs, int size_particle_rhs_extra>
    void compute_interaction_with_extra(
            const partsize_t nb_particles,
            const real_number pos_part[],
            real_number rhs_part[],
            const real_number rhs_part_extra[]) const;
    // meant to be called AFTER executing the time-stepping operation.
    // once the particles have been moved, ensure that the orientation is a unit vector.
    template <int size_particle_positions>
    void enforce_unit_orientation(
            const partsize_t nb_particles,
            real_number pos_part[]) const;

    bool isEnable() const {
        return isActive;
    }

    void setEnable(const bool inIsActive) {
        isActive = inIsActive;
    }
};

#endif

