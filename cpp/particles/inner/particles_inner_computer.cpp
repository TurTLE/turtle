/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "base.hpp"
#include "particles/particles_utils.hpp"
#include "particles/inner/particles_inner_computer.hpp"

#include <cmath>

template <class real_number, class partsize_t>
template <int size_particle_positions, int size_particle_rhs>
void particles_inner_computer<real_number, partsize_t>::compute_interaction(
        const partsize_t nb_particles,
        const real_number pos_part[],
        real_number rhs_part[]) const{
    static_assert(size_particle_positions == 6, "This kernel works only with 6 values for one particle's position");
    static_assert(size_particle_rhs == 6, "This kernel works only with 6 values per particle's rhs");

    #pragma omp parallel for
    for(partsize_t idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
        // Add attr × V0 to the field interpolation
        rhs_part[idx_part*size_particle_rhs + IDXC_X] += pos_part[idx_part*size_particle_positions + 3+IDXC_X]*v0;
        rhs_part[idx_part*size_particle_rhs + IDXC_Y] += pos_part[idx_part*size_particle_positions + 3+IDXC_Y]*v0;
        rhs_part[idx_part*size_particle_rhs + IDXC_Z] += pos_part[idx_part*size_particle_positions + 3+IDXC_Z]*v0;
    }
}

    // for given orientation and right-hand-side, recompute right-hand-side such
    // that it is perpendicular to the current orientation.
    // this is the job of the Lagrange multiplier terms, hence the
    // "add_Lagrange_multipliers" name of the method.
template <>
template <>
void particles_inner_computer<double, long long>::add_Lagrange_multipliers<6,6>(
        const long long nb_particles,
        const double pos_part[],
        double rhs_part[]) const{

        #pragma omp parallel for
        for(long long idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
            const long long idx0 = idx_part*6 + 3;
            const long long idx1 = idx_part*6 + 3;
            // check that orientation is unit vector:
            double orientation_size = std::sqrt(
                    pos_part[idx0+IDXC_X]*pos_part[idx0+IDXC_X] +
                    pos_part[idx0+IDXC_Y]*pos_part[idx0+IDXC_Y] +
                    pos_part[idx0+IDXC_Z]*pos_part[idx0+IDXC_Z]);
            variable_used_only_in_assert(orientation_size);
            assert(orientation_size > 0.99);
            assert(orientation_size < 1.01);
            // I call "rotation" to be the right hand side of the orientation part of the ODE
            // project rotation on orientation:
            double projection = (
                    pos_part[idx0+IDXC_X]*rhs_part[idx1+IDXC_X] +
                    pos_part[idx0+IDXC_Y]*rhs_part[idx1+IDXC_Y] +
                    pos_part[idx0+IDXC_Z]*rhs_part[idx1+IDXC_Z]);

            // now remove parallel bit.
            rhs_part[idx1+IDXC_X] -= pos_part[idx0+IDXC_X]*projection;
            rhs_part[idx1+IDXC_Y] -= pos_part[idx0+IDXC_Y]*projection;
            rhs_part[idx1+IDXC_Z] -= pos_part[idx0+IDXC_Z]*projection;

            // DEBUG
            // sanity check, for debugging purposes
            // compute dot product between orientation and orientation change
            //double dotproduct = (
            //        rhs_part[idx1 + IDXC_X]*pos_part[idx0 + IDXC_X] +
            //        rhs_part[idx1 + IDXC_Y]*pos_part[idx0 + IDXC_Y] +
            //        rhs_part[idx1 + IDXC_Z]*pos_part[idx0 + IDXC_Z]);
            //if (dotproduct > 0.1)
            //{
            //    DEBUG_MSG("dotproduct = %g, projection = %g\n"
            //              "pos_part[%d] = %g, pos_part[%d] = %g, pos_part[%d] = %g\n"
            //              "rhs_part[%d] = %g, rhs_part[%d] = %g, rhs_part[%d] = %g\n",
            //            dotproduct,
            //            projection,
            //            IDXC_X, pos_part[idx0 + IDXC_X],
            //            IDXC_Y, pos_part[idx0 + IDXC_Y],
            //            IDXC_Z, pos_part[idx0 + IDXC_Z],
            //            IDXC_X, rhs_part[idx1 + IDXC_X],
            //            IDXC_Y, rhs_part[idx1 + IDXC_Y],
            //            IDXC_Z, rhs_part[idx1 + IDXC_Z]);
            //    assert(false);
            //}
            //assert(dotproduct <= 0.1);
        }
    }

template <>
template <>
void particles_inner_computer<double, long long>::compute_interaction_with_extra<6,6,3>(
        const long long nb_particles,
        const double pos_part[],
        double rhs_part[],
        const double rhs_part_extra[]) const{
    // call plain compute_interaction first
    compute_interaction<6, 6>(nb_particles, pos_part, rhs_part);

    // now add vorticity term
    #pragma omp parallel for
    for(long long idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
        // Cross product vorticity/orientation
        rhs_part[idx_part*6 + 3+IDXC_X] += 0.5*(rhs_part_extra[idx_part*3 + IDXC_Y]*pos_part[idx_part*6 + 3+IDXC_Z] -
                                               rhs_part_extra[idx_part*3 + IDXC_Z]*pos_part[idx_part*6 + 3+IDXC_Y]);
        rhs_part[idx_part*6 + 3+IDXC_Y] += 0.5*(rhs_part_extra[idx_part*3 + IDXC_Z]*pos_part[idx_part*6 + 3+IDXC_X] -
                                               rhs_part_extra[idx_part*3 + IDXC_X]*pos_part[idx_part*6 + 3+IDXC_Z]);
        rhs_part[idx_part*6 + 3+IDXC_Z] += 0.5*(rhs_part_extra[idx_part*3 + IDXC_X]*pos_part[idx_part*6 + 3+IDXC_Y] -
                                               rhs_part_extra[idx_part*3 + IDXC_Y]*pos_part[idx_part*6 + 3+IDXC_X]);
    }
}

template <> //Work here
template <>
void particles_inner_computer<double, long long>::compute_interaction_with_extra<6,6,9>(
        const long long nb_particles,
        const double pos_part[],
        double rhs_part[],
        const double rhs_part_extra[]) const{
    // call plain compute_interaction first
    compute_interaction<6, 6>(nb_particles, pos_part, rhs_part);
    const double ll2 = lambda*lambda;

    // now add vorticity term
    #pragma omp parallel for
    for(long long idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
        long long idx_part6 = idx_part*6 + 3;
        long long idx_part9 = idx_part*9;
        rhs_part[idx_part6+IDXC_X] += (
                pos_part[idx_part6+IDXC_Z]*(ll2*rhs_part_extra[idx_part9 + IDXC_DZ_X]-rhs_part_extra[idx_part9 + IDXC_DX_Z])
              + pos_part[idx_part6+IDXC_Y]*(ll2*rhs_part_extra[idx_part9 + IDXC_DY_X]-rhs_part_extra[idx_part9 + IDXC_DX_Y])
              + pos_part[idx_part6+IDXC_X]*(ll2-1)*rhs_part_extra[idx_part9 + IDXC_DX_X]) / (ll2+1);
        rhs_part[idx_part6+IDXC_Y] += (
                pos_part[idx_part6+IDXC_X]*(ll2*rhs_part_extra[idx_part9 + IDXC_DX_Y]-rhs_part_extra[idx_part9 + IDXC_DY_X])
              + pos_part[idx_part6+IDXC_Z]*(ll2*rhs_part_extra[idx_part9 + IDXC_DZ_Y]-rhs_part_extra[idx_part9 + IDXC_DY_Z])
              + pos_part[idx_part6+IDXC_Y]*(ll2-1)*rhs_part_extra[idx_part9 + IDXC_DY_Y]) / (ll2+1);
        rhs_part[idx_part6+IDXC_Z] += (
                pos_part[idx_part6+IDXC_Y]*(ll2*rhs_part_extra[idx_part9 + IDXC_DY_Z]-rhs_part_extra[idx_part9 + IDXC_DZ_Y])
              + pos_part[idx_part6+IDXC_X]*(ll2*rhs_part_extra[idx_part9 + IDXC_DX_Z]-rhs_part_extra[idx_part9 + IDXC_DZ_X])
              + pos_part[idx_part6+IDXC_Z]*(ll2-1)*rhs_part_extra[idx_part9 + IDXC_DZ_Z]) / (ll2+1);
    }
}


// meant to be called AFTER executing the time-stepping operation.
// once the particles have been moved, ensure that the orientation is a unit vector.
template <>
template <>
void particles_inner_computer<double, long long>::enforce_unit_orientation<6>(
        const long long nb_particles,
        double pos_part[]) const{
    #pragma omp parallel for
    for(long long idx_part = 0 ; idx_part < nb_particles ; ++idx_part){
        const long long idx0 = idx_part*6 + 3;
        // compute orientation size:
        double orientation_size = std::sqrt(
                pos_part[idx0+IDXC_X]*pos_part[idx0+IDXC_X] +
                pos_part[idx0+IDXC_Y]*pos_part[idx0+IDXC_Y] +
                pos_part[idx0+IDXC_Z]*pos_part[idx0+IDXC_Z]);
        // now renormalize
        pos_part[idx0 + IDXC_X] /= orientation_size;
        pos_part[idx0 + IDXC_Y] /= orientation_size;
        pos_part[idx0 + IDXC_Z] /= orientation_size;
    }
}

template
void particles_inner_computer<double, long long>::compute_interaction<6, 6>(
        const long long nb_particles,
        const double pos_part[],
        double rhs_part[]) const;

