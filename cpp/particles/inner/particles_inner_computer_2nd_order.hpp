/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLES_INNER_COMPUTER_2ND_ORDER_HPP
#define PARTICLES_INNER_COMPUTER_2ND_ORDER_HPP


/** \brief Computes action of Stokes drag on particles.
 *
 * Either 6 state variables (positions + momenta) or 7 state variables (positions + momenta + value of drag coefficient).
 * The case of particles with individual values of the drag coefficient is relevant for the case of plastic collisions,
 * when we do not have a priori knowledge of what the mass/size/drag coefficient of the particle is.
 *
 * `compute_interaction_with_extra` is currently only defined for 6 or 7 state variables.
 *
 */

template <class real_number, class partsize_t>
class particles_inner_computer_2nd_order_Stokes{
    double drag_coefficient;
public:
    template <int size_particle_state, int size_particle_rhs>
    void compute_interaction(const partsize_t number_of_particles, real_number particle_state[], real_number particle_rhs[]) const{
    }

    template <int size_particle_state>
    void enforce_unit_orientation(const partsize_t /*nb_particles*/, real_number /*pos_part*/[]) const{
    }

    template <int size_particle_state, int size_particle_rhs>
    void add_Lagrange_multipliers(const partsize_t /*nb_particles*/, real_number /*pos_part*/[], real_number /*rhs_part*/[]) const{
    }

    template <int size_particle_state, int size_particle_rhs, int size_particle_rhs_extra>
    void compute_interaction_with_extra(
            const partsize_t number_of_particles,
            real_number particle_state[],
            real_number particle_rhs[],
            const real_number sampled_velocity[]) const{
        assert(size_particle_rhs_extra == 3);
        assert(size_particle_state == size_particle_rhs);
        assert((size_particle_state == 6) || (size_particle_state == 7));
        if (size_particle_state == 6){
            #pragma omp parallel for
            for(partsize_t idx_part = 0 ; idx_part < number_of_particles ; ++idx_part){
                particle_rhs[idx_part*size_particle_rhs + IDXC_X] = particle_state[idx_part*size_particle_state + 3 + IDXC_X];
                particle_rhs[idx_part*size_particle_rhs + IDXC_Y] = particle_state[idx_part*size_particle_state + 3 + IDXC_Y];
                particle_rhs[idx_part*size_particle_rhs + IDXC_Z] = particle_state[idx_part*size_particle_state + 3 + IDXC_Z];
                particle_rhs[idx_part*size_particle_rhs + 3 + IDXC_X] = - this->drag_coefficient * (particle_state[idx_part*size_particle_state + 3 + IDXC_X] - sampled_velocity[idx_part*size_particle_rhs_extra + IDXC_X]);
                particle_rhs[idx_part*size_particle_rhs + 3 + IDXC_Y] = - this->drag_coefficient * (particle_state[idx_part*size_particle_state + 3 + IDXC_Y] - sampled_velocity[idx_part*size_particle_rhs_extra + IDXC_Y]);
                particle_rhs[idx_part*size_particle_rhs + 3 + IDXC_Z] = - this->drag_coefficient * (particle_state[idx_part*size_particle_state + 3 + IDXC_Z] - sampled_velocity[idx_part*size_particle_rhs_extra + IDXC_Z]);
            }
        }
        else if (size_particle_state == 7){
            // particle stores its own drag coefficient in the 7th state variable
            #pragma omp parallel for
            for(partsize_t idx_part = 0 ; idx_part < number_of_particles ; ++idx_part){
                particle_rhs[idx_part*size_particle_rhs + IDXC_X] = particle_state[idx_part*size_particle_state + 3 + IDXC_X];
                particle_rhs[idx_part*size_particle_rhs + IDXC_Y] = particle_state[idx_part*size_particle_state + 3 + IDXC_Y];
                particle_rhs[idx_part*size_particle_rhs + IDXC_Z] = particle_state[idx_part*size_particle_state + 3 + IDXC_Z];
                particle_rhs[idx_part*size_particle_rhs + 3 + IDXC_X] = - particle_state[idx_part*size_particle_state + 6] * (particle_state[idx_part*size_particle_state + 3 + IDXC_X] - sampled_velocity[idx_part*size_particle_rhs_extra + IDXC_X]);
                particle_rhs[idx_part*size_particle_rhs + 3 + IDXC_Y] = - particle_state[idx_part*size_particle_state + 6] * (particle_state[idx_part*size_particle_state + 3 + IDXC_Y] - sampled_velocity[idx_part*size_particle_rhs_extra + IDXC_Y]);
                particle_rhs[idx_part*size_particle_rhs + 3 + IDXC_Z] = - particle_state[idx_part*size_particle_state + 6] * (particle_state[idx_part*size_particle_state + 3 + IDXC_Z] - sampled_velocity[idx_part*size_particle_rhs_extra + IDXC_Z]);
            }
        }
    }

    constexpr static bool isEnable() {
        return true;
    }

    void set_drag_coefficient(double mu)
    {
        this->drag_coefficient = mu;
    }

    double get_drag_coefficient()
    {
        return this->drag_coefficient;
    }
};

#endif//PARTICLES_INNER_COMPUTER_2ND_ORDER_HPP

