/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "particles/particle_solver.hpp"

int particle_solver::Euler(
        double dt,
        abstract_particle_rhs &rhs)
    {
        TIMEZONE("particle_solver::Euler");
        assert(this->pset->getStateSize() == rhs.getStateSize());
        // temporary array for storing right-hand-side
        auto *rhs_val = new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())];
        // temporary array for new particle state
        auto *xx = new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())];
        // empty array required by rhs call
        std::vector<std::unique_ptr<particle_rnumber[]>> tvalues;

        // compute right hand side
        rhs(0, *(this->pset), rhs_val, tvalues);


        // compute new particle state
        this->pset->LOOP(
                [&](const partsize_t idx){
                xx[idx] = this->pset->getParticleState()[idx] + dt * rhs_val[idx];
                });

        // update particle set
        //

        //std::vector<std::unique_ptr<particle_rnumber[]>> temporary;
        this->pset->setParticleState(xx);
        this->pset->redistribute(tvalues);

        // ensure new state is consistent with physical model
        rhs.imposeModelConstraints(*(this->pset));

        // deallocate temporary arrays
        delete[] rhs_val;
        delete[] xx;
        return EXIT_SUCCESS;
    }

/** \brief Implementation of the Heun integration method
 *
 * This is a second order Runge-Kutta time-stepping algorithm.
 * We integrate the following first oder ODE:
 * \f[
 *     x'(t) = f(t, x(t)).
 * \f]
 * The initial condition is \f$ x_n \f$ (for time \f$ t_n \f$), and we would
 * like to integrate the equation forward for an interval \f$ h \f$.
 * Then we must apply the following formulas:
 * \f{eqnarray*}{
 *     k_1 &=& f(t_n, x_n) \\
 *     k_2 &=& f(t_n+h, x_n + h k_1) \\
 *     x_{n+1} &=& x_n + \frac{h}{2}(k_1 + k_2)
 * \f}
 *
 */
int particle_solver::Heun(
        double dt,
        abstract_particle_rhs &rhs)
{
    TIMEZONE("particle_solver::Heun");
    assert(this->pset->getStateSize() == rhs.getStateSize());
    // temporary arrays for storing the two right-hand-side values
    // We need to be able to redistribute the initial condition for computations to make sense,
    // so we must put everything in the same vector.
    std::vector<std::unique_ptr<particle_rnumber[]>> tvalues;
    std::unique_ptr<particle_rnumber[]> xx, x0, k1, k2;

    // allocate x0
    x0.reset(new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())]);
    // store initial condition
    this->pset->getParticleState(x0.get());

    tvalues.resize(1);
    tvalues[0].reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    this->pset->copy_state_tofrom(tvalues[0], x0);
    // allocate k1
    k1.reset(new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())]);
    // compute k1
    // possibly change local ordering, but size of particle state remains the same
    rhs(0, *(this->pset), k1.get(), tvalues);
    // copy back initial data to x0
    this->pset->copy_state_tofrom(x0, tvalues[0]);

    // allocate xx
    xx.reset(new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())]);
    // compute xn+h*k1 in particle set
    this->pset->LOOP(
            [&](const partsize_t idx){
            xx[idx] = x0[idx] + dt*k1[idx];
            });
    // copy the temporary state to the particle set, and redistribute
    // we MUST redistribute, because we want to interpolate.
    // we MUST shuffle values according to the same redistribution, otherwise
    // the subsequent computations are meaningless (and can lead to segfaults).
    this->pset->setParticleState(xx.get());
    tvalues.resize(2);
    tvalues[0].reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    tvalues[1].reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    this->pset->copy_state_tofrom(tvalues[0], x0);
    this->pset->copy_state_tofrom(tvalues[1], k1);
    this->pset->redistribute(tvalues);
    // allocate k2
    k2.reset(new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())]);
    // compute k2
    // note the temporal interpolation is at `1`, i.e. end of time-step
    rhs(1, *(this->pset), k2.get(), tvalues);
    // reallocate k0
    x0.reset(new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())]);
    // reallocate k1
    k1.reset(new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())]);
    this->pset->copy_state_tofrom(x0, tvalues[0]);
    this->pset->copy_state_tofrom(k1, tvalues[1]);
    // tvalues no longer required
    tvalues.resize(0);

    // local number of particles may have changed
    xx.reset(new particle_rnumber[this->pset->getLocalNumberOfParticles()*(this->pset->getStateSize())]);
    // compute final state
    this->pset->LOOP(
            [&](const partsize_t idx){
            xx[idx] = x0[idx] + 0.5*dt*(k1[idx] + k2[idx]);
            });
    this->pset->setParticleState(xx.get());
    // redistribute, impose model constraints
    this->pset->redistribute(tvalues);
    rhs.imposeModelConstraints(*(this->pset));

    // and we are done
    return EXIT_SUCCESS;
}

/** \brief Implementation of the classic 4th order Runge Kutta integration method
 *
 * This is a fourth order Runge-Kutta time-stepping algorithm.
 * We integrate the following first oder ODE:
 * \f[
 *     x'(t) = f(t, x(t)).
 * \f]
 * The initial condition is \f$ x_n \f$ (for time \f$ t_n \f$), and we would
 * like to integrate the equation forward for an interval \f$ h \f$.
 * Then we must apply the following formulas:
 * \f{eqnarray*}{
 *     k_1 &=& f(t_n, x_n) \\
 *     k_2 &=& f(t_n+frac{h}{2}, x_n + \frac{h}{2} k_1) \\
 *     k_3 &=& f(t_n+frac{h}{2}, x_n + \frac{h}{2} k_2) \\
 *     k_4 &=& f(t_n+h, x_n + h k_3) \\
 *     x_{n+1} &=& x_n + \frac{h}{6}(k_1 + 2 k_2 + 2 k_3 + k_4)
 * \f}
 *
 */
int particle_solver::cRK4(
        double dt,
        abstract_particle_rhs &rhs)
{
    TIMEZONE("particle_solver::cRK4");
    assert(this->pset->getStateSize() == rhs.getStateSize());
    // temporary arrays for storing the two right-hand-side values
    // We need to be able to redistribute the initial condition for computations to make sense,
    // so we must put everything in the same vector.
    std::vector<std::unique_ptr<particle_rnumber[]>> extra_values;
    std::unique_ptr<particle_rnumber[]> xx, kcurrent;

    // allocate extra_values
    // I use the following convention:
    // extra_values[0] = x0
    // extra_values[1] = k1
    // extra_values[2] = k2
    // extra_values[3] = k3
    // extra_values[4] = k4
    extra_values.resize(5);
    for (int temp_counter = 0; temp_counter < 5; temp_counter++)
    {
        extra_values[temp_counter].reset(new particle_rnumber[
                this->pset->getLocalNumberOfParticles()*
                this->pset->getStateSize()]);
    }

    // store initial condition
    this->pset->getParticleState(extra_values[0].get());

    /*** COMPUTE K1 ***/
    kcurrent.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    // call to rhs will possibly change local ordering, but local number of particles remains the same
    rhs(0, *(this->pset), kcurrent.get(), extra_values);
    // store k1 values
    this->pset->copy_state_tofrom(extra_values[1], kcurrent);

    /*** COMPUTE xn+h*k1/2 ***/
    // allocate xx
    xx.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    this->pset->LOOP(
            [&](const partsize_t idx){
            xx[idx] = extra_values[0][idx] + 0.5*dt*extra_values[1][idx];
            });
    // copy the temporary state to the particle set, and redistribute
    // we MUST redistribute, because we want to interpolate.
    // we MUST shuffle values according to the same redistribution, otherwise
    // the subsequent computations are meaningless.
    // this will possibly change local ordering AND/OR local number of particles
    this->pset->setParticleState(xx.get());
    this->pset->redistribute(extra_values);

    /*** COMPUTE K2 ***/
    kcurrent.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    rhs(0.5, *(this->pset), kcurrent.get(), extra_values);
    this->pset->copy_state_tofrom(extra_values[2], kcurrent);

    /*** COMPUTE xn+h*k2/2 ***/
    xx.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    this->pset->LOOP(
            [&](const partsize_t idx){
            xx[idx] = extra_values[0][idx] + 0.5*dt*extra_values[2][idx];
            });
    this->pset->setParticleState(xx.get());
    this->pset->redistribute(extra_values);

    /*** COMPUTE K3 ***/
    kcurrent.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    rhs(0.5, *(this->pset), kcurrent.get(), extra_values);
    this->pset->copy_state_tofrom(extra_values[3], kcurrent);

    /*** COMPUTE xn+h*k3 ***/
    xx.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    this->pset->LOOP(
            [&](const partsize_t idx){
            xx[idx] = extra_values[0][idx] + dt*extra_values[3][idx];
            });
    this->pset->setParticleState(xx.get());
    this->pset->redistribute(extra_values);

    /*** COMPUTE K4 ***/
    kcurrent.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    rhs(1.0, *(this->pset), kcurrent.get(), extra_values);
    this->pset->copy_state_tofrom(extra_values[4], kcurrent);

    /*** COMPUTE xn+h*(k1 + 2*k2 + 2*k3 + k4)/6 ***/
    xx.reset(new particle_rnumber[
            this->pset->getLocalNumberOfParticles()*
            this->pset->getStateSize()]);
    this->pset->LOOP(
            [&](const partsize_t idx){
            xx[idx] = extra_values[0][idx] +
                dt*(extra_values[1][idx] +
                  2*extra_values[2][idx] +
                  2*extra_values[3][idx] +
                    extra_values[4][idx]) / 6;
            });
    this->pset->setParticleState(xx.get());
    extra_values.resize(0);
    this->pset->redistribute(extra_values);

    // impose model constraints
    rhs.imposeModelConstraints(*(this->pset));

    // and we are done
    return EXIT_SUCCESS;
}


template <int state_size>
int particle_solver::writeCheckpoint(
        particles_output_hdf5<partsize_t, particle_rnumber, state_size> *particles_output_writer)
{
    TIMEZONE("particle_solver::writeCheckpoint to particles_output_hdf5");
    particles_output_writer->update_particle_species_name(this->particle_species_name);
    particles_output_writer->setParticleFileLayout(pset->getParticleFileLayout());
    particles_output_writer->template save<state_size>(
            this->pset->getParticleState(),
            this->additional_states.data(),
            this->pset->getParticleIndices(),
            this->pset->getLocalNumberOfParticles(),
            this->iteration);
    return EXIT_SUCCESS;
}


template <int state_size>
int particle_solver::writeCheckpoint(
        const std::string file_name)
{
    TIMEZONE("particle_solver::writeCheckpoint to file_name");
    this->pset->writeParticleStates<state_size>(
            file_name,
            this->particle_species_name,
            "state",
            this->iteration);
    return EXIT_SUCCESS;
}


template <int state_size>
int particle_solver::writeCheckpointWithLabels(
        const std::string file_name)
{
    TIMEZONE("particle_solver::writeCheckpointWithLabels");
    this->template writeCheckpoint<state_size>(file_name);
    this->pset->writeParticleLabels(
            file_name,
            this->particle_species_name,
            "label",
            this->iteration);
    return EXIT_SUCCESS;
}

template int particle_solver::writeCheckpoint<3>(particles_output_hdf5<partsize_t, particle_rnumber, 3> *);
template int particle_solver::writeCheckpoint<6>(particles_output_hdf5<partsize_t, particle_rnumber, 6> *);
template int particle_solver::writeCheckpoint<7>(particles_output_hdf5<partsize_t, particle_rnumber, 7> *);
template int particle_solver::writeCheckpoint<8>(particles_output_hdf5<partsize_t, particle_rnumber, 8> *);
template int particle_solver::writeCheckpoint<15>(particles_output_hdf5<partsize_t, particle_rnumber, 15> *);

template int particle_solver::writeCheckpoint<3> (const std::string file_name);
template int particle_solver::writeCheckpoint<6> (const std::string file_name);
template int particle_solver::writeCheckpoint<7> (const std::string file_name);
template int particle_solver::writeCheckpoint<8> (const std::string file_name);
template int particle_solver::writeCheckpoint<15>(const std::string file_name);

template int particle_solver::writeCheckpointWithLabels<3> (const std::string file_name);
template int particle_solver::writeCheckpointWithLabels<6> (const std::string file_name);
template int particle_solver::writeCheckpointWithLabels<7> (const std::string file_name);
template int particle_solver::writeCheckpointWithLabels<8> (const std::string file_name);
template int particle_solver::writeCheckpointWithLabels<15>(const std::string file_name);

