/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#include "particles/rhs/deformation_tensor_rhs.hpp"

#include <gsl/gsl_linalg.h>

template <typename rnumber,
          field_backend be,
          temporal_interpolation_type tt>
int deformation_tensor_rhs<rnumber, be, tt>::imposeModelConstraints(
        abstract_particle_set &pset) const
{
    particle_rnumber *current_state = pset.getParticleState();

    gsl_vector * tmp_vector = gsl_vector_alloc(3);
    gsl_matrix_view Q;
    gsl_matrix * Q_tmp = gsl_matrix_alloc(3, 3);
    gsl_matrix * R_tmp = gsl_matrix_alloc(3, 3);

    for (partsize_t idx = 0; idx < pset.getLocalNumberOfParticles(); idx++)
    {
        const partsize_t idx15 = 15*idx;

        // QR decomposition
        Q = gsl_matrix_view_array(current_state+idx15 + 3, 3, 3);
        gsl_linalg_QR_decomp(&Q.matrix, tmp_vector);
        gsl_linalg_QR_unpack(&Q.matrix, tmp_vector, Q_tmp, R_tmp);

        // save results
        gsl_matrix_memcpy(&Q.matrix, Q_tmp);
        current_state[idx15 + 12 + IDXC_X] += log(std::abs(gsl_matrix_get(R_tmp, 0, 0)));
        current_state[idx15 + 12 + IDXC_Y] += log(std::abs(gsl_matrix_get(R_tmp, 1, 1)));
        current_state[idx15 + 12 + IDXC_Z] += log(std::abs(gsl_matrix_get(R_tmp, 2, 2)));
    }
    return EXIT_SUCCESS;
}

template class deformation_tensor_rhs<float, FFTW, NONE>;
template class deformation_tensor_rhs<double, FFTW, NONE>;

