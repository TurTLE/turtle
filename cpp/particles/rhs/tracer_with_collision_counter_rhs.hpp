/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef TRACER_WITH_COLLISION_COUNTER_RHS_HPP
#define TRACER_WITH_COLLISION_COUNTER_RHS_HPP

#include "particles/rhs/tracer_rhs.hpp"
#include "particles/p2p/p2p_ghost_collisions.hpp"


template <typename rnumber,
          field_backend be,
          temporal_interpolation_type tt>
class tracer_with_collision_counter_rhs: public tracer_rhs<rnumber, be, tt>
{
    protected:
        p2p_ghost_collisions<abstract_particle_set::particle_rnumber, abstract_particle_set::partsize_t> p2p_gc;

    public:
        tracer_with_collision_counter_rhs(){}
        ~tracer_with_collision_counter_rhs() noexcept(false){}

        virtual int getStateSize() const
        {
            return 3;
        }

        int operator()(
                double t,
                abstract_particle_set &pset,
                abstract_particle_set::particle_rnumber *result,
                std::vector<std::unique_ptr<abstract_particle_set::particle_rnumber[]>> &additional_data)
        {
            // interpolation adds on top of existing values, so result must be cleared.
            std::fill_n(result, pset.getLocalNumberOfParticles()*3, 0);
            const int interpolation_result = (*(this->velocity))(t, pset, result);
            additional_data.insert(
                    additional_data.begin(),
                    std::unique_ptr<abstract_particle_set::particle_rnumber[]>(
                        new abstract_particle_set::particle_rnumber[pset.getLocalNumberOfParticles()*pset.getStateSize()]));
            // copy rhs values to temporary array
            pset.copy_state_tofrom(
                    additional_data[0].get(),
                    result);
            this->p2p_gc.reset_collision_pairs();
            pset.template applyP2PKernel<
                3,
                p2p_ghost_collisions<abstract_particle_set::particle_rnumber,
                                     abstract_particle_set::partsize_t>>(
                    this->p2p_gc,
                    additional_data);
            // copy shuffled rhs values
            pset.copy_state_tofrom(
                    result,
                    additional_data[0].get());
            // clear temporary array
            additional_data.erase(additional_data.begin());
            return interpolation_result;
        }

        p2p_ghost_collisions<abstract_particle_set::particle_rnumber, abstract_particle_set::partsize_t> &getCollisionCounter()
        {
            return this->p2p_gc;
        }
};

#endif//TRACER_WITH_COLLISION_COUNTER_RHS_HPP

