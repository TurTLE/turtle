/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef DEFORMATION_TENSOR_RHS_HPP
#define DEFORMATION_TENSOR_RHS_HPP

#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/abstract_particle_rhs.hpp"

template <typename rnumber,
          field_backend be,
          temporal_interpolation_type tt>
class deformation_tensor_rhs: public abstract_particle_rhs
{
    private:
        using particle_rnumber = abstract_particle_set::particle_rnumber;
        using partsize_t = abstract_particle_set::partsize_t;

    protected:
        field_tinterpolator<rnumber, be, THREE, tt> *velocity;
        field_tinterpolator<rnumber, be, THREExTHREE, tt> *velocity_gradient;

    public:
        deformation_tensor_rhs(){}
        ~deformation_tensor_rhs() noexcept(false){}

        int getStateSize() const
        {
            // 3 numbers for position
            // 9 numbers for components of deformation tensor
            // 3 numbers to store logarithmic factors
            return 15;
        }

        int setVelocity(field_tinterpolator<rnumber, be, THREE, tt> *in_velocity)
        {
            this->velocity = in_velocity;
            return EXIT_SUCCESS;
        }

        int setVelocityGradient(field_tinterpolator<rnumber, be, THREExTHREE, tt> *in_velocity_gradient)
        {
            this->velocity_gradient = in_velocity_gradient;
            return EXIT_SUCCESS;
        }

        int operator()(
                double t,
                abstract_particle_set &pset,
                particle_rnumber *result,
                std::vector<std::unique_ptr<abstract_particle_set::particle_rnumber[]>> &additional_data)
        {
            std::unique_ptr<double[]> pdata3(new double[3*pset.getLocalNumberOfParticles()]);
            std::unique_ptr<double[]> pdata9(new double[9*pset.getLocalNumberOfParticles()]);
            // reference to current particle state
            particle_rnumber *current_state = pset.getParticleState();
            // interpolation adds on top of existing values, so result arrays must be cleared.
            for (long long int idx_part = 0; idx_part < pset.getLocalNumberOfParticles(); idx_part++)
            {
                std::fill_n(pdata3.get()+3*idx_part, 3, 0);
                std::fill_n(pdata9.get()+9*idx_part, 9, 0);
            }
            // interpolate velocity field
            (*(this->velocity))(t, pset, pdata3.get());
            (*(this->velocity_gradient))(t, pset, pdata9.get());

            // now store right hand side
            for (partsize_t idx = 0; idx < pset.getLocalNumberOfParticles(); idx++)
            {
                // for position right hand side is velocity
                const partsize_t idx15 = 15*idx;
                const partsize_t idx9 = 9*idx;
                const partsize_t idx3 = 3*idx;
                result[idx15 + IDXC_X] = pdata3[idx3 + IDXC_X];
                result[idx15 + IDXC_Y] = pdata3[idx3 + IDXC_Y];
                result[idx15 + IDXC_Z] = pdata3[idx3 + IDXC_Z];

                // ODE of the deformation tensor: (dt F_{ij}) = F_{kj} (du_i/dx_k)
                // We save the matrix F_{ij} = dX_i/dx_j in row-major
                // to be compatible with GSL
                result[idx15 + 3 + IDXC_GSL_XX] = pdata9[idx9+IDXC_DX_X]*current_state[idx15 + 3 + IDXC_GSL_XX]
                                                + pdata9[idx9+IDXC_DY_X]*current_state[idx15 + 3 + IDXC_GSL_YX]
                                                + pdata9[idx9+IDXC_DZ_X]*current_state[idx15 + 3 + IDXC_GSL_ZX];
                result[idx15 + 3 + IDXC_GSL_YX] = pdata9[idx9+IDXC_DX_Y]*current_state[idx15 + 3 + IDXC_GSL_XX]
                                                + pdata9[idx9+IDXC_DY_Y]*current_state[idx15 + 3 + IDXC_GSL_YX]
                                                + pdata9[idx9+IDXC_DZ_Y]*current_state[idx15 + 3 + IDXC_GSL_ZX];
                result[idx15 + 3 + IDXC_GSL_ZX] = pdata9[idx9+IDXC_DX_Z]*current_state[idx15 + 3 + IDXC_GSL_XX]
                                                + pdata9[idx9+IDXC_DY_Z]*current_state[idx15 + 3 + IDXC_GSL_YX]
                                                + pdata9[idx9+IDXC_DZ_Z]*current_state[idx15 + 3 + IDXC_GSL_ZX];

                result[idx15 + 3 + IDXC_GSL_XY] = pdata9[idx9+IDXC_DX_X]*current_state[idx15 + 3 + IDXC_GSL_XY]
                                                + pdata9[idx9+IDXC_DY_X]*current_state[idx15 + 3 + IDXC_GSL_YY]
                                                + pdata9[idx9+IDXC_DZ_X]*current_state[idx15 + 3 + IDXC_GSL_ZY];
                result[idx15 + 3 + IDXC_GSL_YY] = pdata9[idx9+IDXC_DX_Y]*current_state[idx15 + 3 + IDXC_GSL_XY]
                                                + pdata9[idx9+IDXC_DY_Y]*current_state[idx15 + 3 + IDXC_GSL_YY]
                                                + pdata9[idx9+IDXC_DZ_Y]*current_state[idx15 + 3 + IDXC_GSL_ZY];
                result[idx15 + 3 + IDXC_GSL_ZY] = pdata9[idx9+IDXC_DX_Z]*current_state[idx15 + 3 + IDXC_GSL_XY]
                                                + pdata9[idx9+IDXC_DY_Z]*current_state[idx15 + 3 + IDXC_GSL_YY]
                                                + pdata9[idx9+IDXC_DZ_Z]*current_state[idx15 + 3 + IDXC_GSL_ZY];

                result[idx15 + 3 + IDXC_GSL_XZ] = pdata9[idx9+IDXC_DX_X]*current_state[idx15 + 3 + IDXC_GSL_XZ]
                                                + pdata9[idx9+IDXC_DY_X]*current_state[idx15 + 3 + IDXC_GSL_YZ]
                                                + pdata9[idx9+IDXC_DZ_X]*current_state[idx15 + 3 + IDXC_GSL_ZZ];
                result[idx15 + 3 + IDXC_GSL_YZ] = pdata9[idx9+IDXC_DX_Y]*current_state[idx15 + 3 + IDXC_GSL_XZ]
                                                + pdata9[idx9+IDXC_DY_Y]*current_state[idx15 + 3 + IDXC_GSL_YZ]
                                                + pdata9[idx9+IDXC_DZ_Y]*current_state[idx15 + 3 + IDXC_GSL_ZZ];
                result[idx15 + 3 + IDXC_GSL_ZZ] = pdata9[idx9+IDXC_DX_Z]*current_state[idx15 + 3 + IDXC_GSL_XZ]
                                                + pdata9[idx9+IDXC_DY_Z]*current_state[idx15 + 3 + IDXC_GSL_YZ]
                                                + pdata9[idx9+IDXC_DZ_Z]*current_state[idx15 + 3 + IDXC_GSL_ZZ];

                // The last three components contain the logarithmic stretching factors.
                // They are updated by the orthogonalization procedure
                result[idx15 + 12 + IDXC_X] = 0;
                result[idx15 + 12 + IDXC_Y] = 0;
                result[idx15 + 12 + IDXC_Z] = 0;
            }
            // we use `unique_ptr`, so we don't need to explicitly free the memory for pdata3 and pdata9
            return EXIT_SUCCESS;
        }

        int imposeModelConstraints(
                abstract_particle_set &pset) const;
};

#endif//DEFORMATION_TENSOR_RHS_HPP

