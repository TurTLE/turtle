/******************************************************************************
*                                                                             *
*  Copyright 2020 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef TRACER_RHS_HPP
#define TRACER_RHS_HPP

#include "particles/interpolation/field_tinterpolator.hpp"
#include "particles/abstract_particle_rhs.hpp"



template <typename rnumber,
          field_backend be,
          temporal_interpolation_type tt>
class tracer_rhs: public abstract_particle_rhs
{
    protected:
        field_tinterpolator<rnumber, be, THREE, tt> *velocity;

    public:
        tracer_rhs(){}
        ~tracer_rhs() noexcept(false){}

        int getStateSize() const
        {
            return 3;
        }

        int setVelocity(field_tinterpolator<rnumber, be, THREE, tt> *in_velocity)
        {
            this->velocity = in_velocity;
            return EXIT_SUCCESS;
        }

        int operator()(
                double t,
                abstract_particle_set &pset,
                particle_rnumber *result,
                std::vector<std::unique_ptr<abstract_particle_set::particle_rnumber[]>> &additional_data)
        {
            // interpolation adds on top of existing values, so result must be cleared.
            std::fill_n(result, pset.getLocalNumberOfParticles()*3, 0);
            return (*(this->velocity))(t, pset, result);
        }

        int imposeModelConstraints(
                abstract_particle_set &pset) const
        {
            return EXIT_SUCCESS;
        }
};

#endif//TRACER_RHS_HPP

