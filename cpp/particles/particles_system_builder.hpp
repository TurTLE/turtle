/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef PARTICLES_SYSTEM_BUILDER_HPP
#define PARTICLES_SYSTEM_BUILDER_HPP

#include "particles/particles_system.hpp"
#include "particles/particles_input_hdf5.hpp"
#include "particles/interpolation/particles_generic_interp.hpp"
#include "particles/p2p/p2p_computer_empty.hpp"
#include "particles/inner/particles_inner_computer_empty.hpp"

//////////////////////////////////////////////////////////////////////////////
///
/// Double template "for"
///
/// "Template_double_for_if" is used to systematically generate specialized
/// templates for two ranges of template parameters.
/// For the interpolation we have `n` and `m` that designate "number of
/// neighbours" and "smoothness of interpolant".
/// Calling Template_double_for_if is essentially equivalent to having a couple
/// of nested "switch" statements, but without all the repetitive code lines.
///
//////////////////////////////////////////////////////////////////////////////

namespace Template_double_for_if{

template <class RetType,
          class IterType1, IterType1 CurrentIter1,
          class IterType2, const IterType2 CurrentIter2, const IterType2 iterTo2, const IterType2 IterStep2,
          class Func, bool IsNotOver, typename... Args>
struct For2{
    static RetType evaluate(IterType2 value2, Args... args){
        if(CurrentIter2 == value2){
            return std::move(Func::template instanciate<CurrentIter1, CurrentIter2>(args...));
        }
        else{
            return std::move(For2<RetType,
                                        IterType1, CurrentIter1,
                                        IterType2, CurrentIter2+IterStep2, iterTo2, IterStep2,
                                        Func, (CurrentIter2+IterStep2 < iterTo2), Args...>::evaluate(value2, args...));
        }
    }
};

template <class RetType,
          class IterType1, IterType1 CurrentIter1,
          class IterType2, const IterType2 CurrentIter2, const IterType2 iterTo2, const IterType2 IterStep2,
          class Func, typename... Args>
struct For2<RetType,
                  IterType1, CurrentIter1,
                  IterType2, CurrentIter2, iterTo2, IterStep2,
                  Func, false, Args...>{
    static RetType evaluate(IterType2 value2, Args... args){
        std::cout << __FUNCTION__ << "[ERROR] template values for loop 2 " << value2 << " does not exist\n";
        return RetType();
    }
};

template <class RetType,
          class IterType1, const IterType1 CurrentIter1, const IterType1 iterTo1, const IterType1 IterStep1,
          class IterType2, const IterType2 IterFrom2, const IterType2 iterTo2, const IterType2 IterStep2,
          class Func, bool IsNotOver, typename... Args>
struct For1{
    static RetType evaluate(IterType1 value1, IterType2 value2, Args... args){
        if(CurrentIter1 == value1){
            return std::move(For2<RetType,
                                        IterType1, CurrentIter1,
                                        IterType2, IterFrom2, iterTo2, IterStep2,
                                        Func, (IterFrom2<iterTo2), Args...>::evaluate(value2, args...));
        }
        else{
            return std::move(For1<RetType,
                              IterType1, CurrentIter1+IterStep1, iterTo1, IterStep1,
                              IterType2, IterFrom2, iterTo2, IterStep2,
                              Func, (CurrentIter1+IterStep1 < iterTo1), Args...>::evaluate(value1, value2, args...));
        }
    }
};

template <class RetType,
          class IterType1, const IterType1 IterFrom1, const IterType1 iterTo1, const IterType1 IterStep1,
          class IterType2, const IterType2 IterFrom2, const IterType2 iterTo2, const IterType2 IterStep2,
          class Func, typename... Args>
struct For1<RetType,
                IterType1, IterFrom1, iterTo1, IterStep1,
                IterType2, IterFrom2, iterTo2, IterStep2,
                Func, false, Args...>{
    static RetType evaluate(IterType1 value1, IterType2 value2, Args... args){
        std::cout << __FUNCTION__ << "[ERROR] template values for loop 1 " << value1 << " does not exist\n";
        return RetType();
    }
};

template <class RetType,
          class IterType1, const IterType1 IterFrom1, const IterType1 iterTo1, const IterType1 IterStep1,
          class IterType2, const IterType2 IterFrom2, const IterType2 iterTo2, const IterType2 IterStep2,
          class Func, typename... Args>
inline RetType evaluate(IterType1 value1, IterType2 value2, Args... args){
    return std::move(For1<RetType,
            IterType1, IterFrom1, iterTo1, IterStep1,
            IterType2, IterFrom2, iterTo2, IterStep2,
            Func, (IterFrom1<iterTo1), Args...>::evaluate(value1, value2, args...));
}

}


//////////////////////////////////////////////////////////////////////////////
///
/// Builder Functions
///
//////////////////////////////////////////////////////////////////////////////

template <class partsize_t, class field_rnumber, field_backend be, field_components fc, class particles_rnumber, class p2p_computer_class,
          class particles_inner_computer_class, int size_particle_positions, int size_particle_rhs>
struct particles_system_build_container {
    template <const int interpolation_size, const int spline_mode>
    static std::unique_ptr<abstract_particles_system_with_p2p<partsize_t, particles_rnumber, p2p_computer_class>> instanciate(
             const field<field_rnumber, be, fc>* fs_field, // (field object)
             const kspace<be, SMOOTH>* fs_kk, // (kspace object, contains dkx, dky, dkz)
             const int nsteps, // to check coherency between parameters and hdf input file (nb rhs)
             const partsize_t nparticles, // to check coherency between parameters and hdf input file
             const std::string& fname_input, // particles input filename
            const std::string& inDatanameState, const std::string& inDatanameRhs, // input dataset names
             MPI_Comm mpi_comm,
            const int in_current_iteration,
            p2p_computer_class p2p_computer,
            particles_inner_computer_class inner_computer,
            const particles_rnumber cutoff = std::numeric_limits<particles_rnumber>::max()){

        // The size of the field grid (global size) all_size seems
        std::array<size_t,3> field_grid_dim;
        field_grid_dim[IDXC_X] = fs_field->rlayout->sizes[IDXV_X];// nx
        field_grid_dim[IDXC_Y] = fs_field->rlayout->sizes[IDXV_Y];// nx
        field_grid_dim[IDXC_Z] = fs_field->rlayout->sizes[IDXV_Z];// nz

        // The size of the local field grid (the field nodes that belong to current process)
        std::array<size_t,3> local_field_dims;
        local_field_dims[IDXC_X] = fs_field->rlayout->subsizes[IDXV_X];
        local_field_dims[IDXC_Y] = fs_field->rlayout->subsizes[IDXV_Y];
        local_field_dims[IDXC_Z] = fs_field->rlayout->subsizes[IDXV_Z];

        // The offset of the local field grid
        std::array<size_t,3> local_field_offset;
        local_field_offset[IDXC_X] = fs_field->rlayout->starts[IDXV_X];
        local_field_offset[IDXC_Y] = fs_field->rlayout->starts[IDXV_Y];
        local_field_offset[IDXC_Z] = fs_field->rlayout->starts[IDXV_Z];


        // Retreive split from fftw to know processes that have no work
        int my_rank, nb_processes;
        AssertMpi(MPI_Comm_rank(mpi_comm, &my_rank));
        AssertMpi(MPI_Comm_size(mpi_comm, &nb_processes));

        const int split_step = (int(field_grid_dim[IDXC_Z])+nb_processes-1)/nb_processes;
        const int nb_processes_involved = (int(field_grid_dim[IDXC_Z])+split_step-1)/split_step;

        assert((my_rank < nb_processes_involved && local_field_dims[IDXC_Z] != 0)
               || (nb_processes_involved <= my_rank && local_field_dims[IDXC_Z] == 0));
        assert(nb_processes_involved <= int(field_grid_dim[IDXC_Z]));

        // Make the idle processes starting from the limit (and not 0 as set by fftw)
        if(nb_processes_involved <= my_rank){
            local_field_offset[IDXC_Z] = field_grid_dim[IDXC_Z];
        }

        // Ensure that 1D partitioning is used
        {
            assert(local_field_offset[IDXC_X] == 0);
            assert(local_field_offset[IDXC_Y] == 0);
            assert(local_field_dims[IDXC_X] == field_grid_dim[IDXC_X]);
            assert(local_field_dims[IDXC_Y] == field_grid_dim[IDXC_Y]);

            assert(my_rank >= nb_processes_involved || ((my_rank == 0 && local_field_offset[IDXC_Z] == 0)
                   || (my_rank != 0 && local_field_offset[IDXC_Z] != 0)));
            assert(my_rank >= nb_processes_involved || ((my_rank == nb_processes_involved-1 && local_field_offset[IDXC_Z]+local_field_dims[IDXC_Z] == field_grid_dim[IDXC_Z])
                   || (my_rank != nb_processes_involved-1 && local_field_offset[IDXC_Z]+local_field_dims[IDXC_Z] != field_grid_dim[IDXC_Z])));
        }

        // The spatial box size (all particles should be included inside)
        std::array<particles_rnumber,3> spatial_box_width;
        spatial_box_width[IDXC_X] = 4 * acos(0) / (fs_kk->dkx);
        spatial_box_width[IDXC_Y] = 4 * acos(0) / (fs_kk->dky);
        spatial_box_width[IDXC_Z] = 4 * acos(0) / (fs_kk->dkz);

        // Box is in the corner
        std::array<particles_rnumber,3> spatial_box_offset;
        spatial_box_offset[IDXC_X] = 0;
        spatial_box_offset[IDXC_Y] = 0;
        spatial_box_offset[IDXC_Z] = 0;

        // The distance between two field nodes in z
        std::array<particles_rnumber,3> spatial_partition_width;
        spatial_partition_width[IDXC_X] = spatial_box_width[IDXC_X]/particles_rnumber(field_grid_dim[IDXC_X]);
        spatial_partition_width[IDXC_Y] = spatial_box_width[IDXC_Y]/particles_rnumber(field_grid_dim[IDXC_Y]);
        spatial_partition_width[IDXC_Z] = spatial_box_width[IDXC_Z]/particles_rnumber(field_grid_dim[IDXC_Z]);
        // The spatial interval of the current process
        const particles_rnumber my_spatial_low_limit_z = particles_rnumber(local_field_offset[IDXC_Z])*spatial_partition_width[IDXC_Z];
        const particles_rnumber my_spatial_up_limit_z = particles_rnumber(local_field_offset[IDXC_Z]+local_field_dims[IDXC_Z])*spatial_partition_width[IDXC_Z];

        // Create the particles system
        using particles_system_type = particles_system<partsize_t, particles_rnumber, field_rnumber,
                                                       field<field_rnumber, be, fc>,
                                                       particles_generic_interp<particles_rnumber, interpolation_size,spline_mode>,
                                                       interpolation_size,
                                                       size_particle_positions, size_particle_rhs,
                                                       p2p_computer_class,
                                                       particles_inner_computer_class>;
        particles_system_type* part_sys = new particles_system_type(field_grid_dim,
                                               spatial_box_width,
                                               spatial_box_offset,
                                               spatial_partition_width,
                                               my_spatial_low_limit_z,
                                               my_spatial_up_limit_z,
                                               local_field_dims,
                                               local_field_offset,
                                               (*fs_field),
                                               mpi_comm,
                                               nparticles,
                                               cutoff,
                                               p2p_computer,
                                               inner_computer,
                                               in_current_iteration);

        // TODO P2P load particle data too
        // Load particles from hdf5
        particles_input_hdf5<partsize_t,
                             particles_rnumber,
                             size_particle_positions,
                             size_particle_rhs> generator(
                                     mpi_comm,
                                     fname_input,
                                     inDatanameState,
                                     inDatanameRhs,
                                     my_spatial_low_limit_z,
                                     my_spatial_up_limit_z);

        // Ensure parameters match the input file
        if(generator.getNbRhs() != nsteps){
            std::runtime_error(std::string("Nb steps is ") + std::to_string(nsteps)
                               + " in the parameters but " + std::to_string(generator.getNbRhs()) + " in the particles file.");
        }
        // Ensure parameters match the input file
        if(generator.getTotalNbParticles() != nparticles){
            std::runtime_error(std::string("Nb particles is ") + std::to_string(nparticles)
                               + " in the parameters but " + std::to_string(generator.getTotalNbParticles()) + " in the particles file.");
        }

        // Load the particles and move them to the particles system
        part_sys->init(generator);

        assert(part_sys->getNbRhs() == nsteps);

        // store particle file layout
        part_sys->setParticleFileLayout(generator.getParticleFileLayout());

        // Return the created particles system
        return std::unique_ptr<abstract_particles_system_with_p2p<partsize_t, particles_rnumber, p2p_computer_class>>(part_sys);
    }
};


template <class partsize_t, class field_rnumber, field_backend be, field_components fc, class particles_rnumber = double>
inline std::unique_ptr<abstract_particles_system<partsize_t, particles_rnumber>> particles_system_builder(
        const field<field_rnumber, be, fc>* fs_field, // (field object)
        const kspace<be, SMOOTH>* fs_kk, // (kspace object, contains dkx, dky, dkz)
        const int nsteps, // to check coherency between parameters and hdf input file (nb rhs)
        const partsize_t nparticles, // to check coherency between parameters and hdf input file
        const std::string& fname_input, // particles input filename
        const std::string& inDatanameState, const std::string& inDatanameRhs, // input dataset names
        const int interpolation_size,
        const int spline_mode,
        MPI_Comm mpi_comm,
        const int in_current_iteration){
    return Template_double_for_if::evaluate<std::unique_ptr<abstract_particles_system<partsize_t, particles_rnumber>>,
                       int, MIN_INTERPOLATION_NEIGHBOURS, MAX_INTERPOLATION_NEIGHBOURS, 1, // interpolation_size
                       int, MIN_INTERPOLATION_SMOOTHNESS, MAX_INTERPOLATION_SMOOTHNESS, 1, // spline_mode
                       particles_system_build_container<partsize_t, field_rnumber,be,fc,particles_rnumber,
                                                        p2p_computer_empty<particles_rnumber,partsize_t>,
                                                        particles_inner_computer_empty<particles_rnumber,partsize_t>,
                                                        3,3>>(
                           interpolation_size, // template iterator 1
                           spline_mode, // template iterator 2
                           fs_field,fs_kk, nsteps, nparticles, fname_input, inDatanameState, inDatanameRhs, mpi_comm, in_current_iteration,
                           p2p_computer_empty<particles_rnumber,partsize_t>(), particles_inner_computer_empty<particles_rnumber,partsize_t>());
}

template <class partsize_t, class field_rnumber, field_backend be, field_components fc,
          class p2p_computer_class, class particles_inner_computer_class,
          class particles_rnumber = double>
inline std::unique_ptr<abstract_particles_system_with_p2p<partsize_t, particles_rnumber, p2p_computer_class>> particles_system_builder_with_p2p(
        const field<field_rnumber, be, fc>* fs_field, // (field object)
        const kspace<be, SMOOTH>* fs_kk, // (kspace object, contains dkx, dky, dkz)
        const int nsteps, // to check coherency between parameters and hdf input file (nb rhs)
        const partsize_t nparticles, // to check coherency between parameters and hdf input file
        const std::string& fname_input, // particles input filename
        const std::string& inDatanameState, const std::string& inDatanameRhs, // input dataset names
        const int interpolation_size,
        const int spline_mode,
        MPI_Comm mpi_comm,
        const int in_current_iteration,
        p2p_computer_class p2p_computer,
        particles_inner_computer_class inner_computer,
        const particles_rnumber cutoff){
    return Template_double_for_if::evaluate<std::unique_ptr<abstract_particles_system_with_p2p<partsize_t, particles_rnumber, p2p_computer_class>>,
                       int, MIN_INTERPOLATION_NEIGHBOURS, MAX_INTERPOLATION_NEIGHBOURS, 1, // interpolation_size
                       int, MIN_INTERPOLATION_SMOOTHNESS, MAX_INTERPOLATION_SMOOTHNESS, 1, // spline_mode
                       particles_system_build_container<partsize_t, field_rnumber,be,fc,particles_rnumber,
                                                        p2p_computer_class,
                                                        particles_inner_computer_class,
                                                        6,6>>(
                           interpolation_size, // template iterator 1
                           spline_mode, // template iterator 2
                           fs_field,fs_kk, nsteps, nparticles, fname_input, inDatanameState, inDatanameRhs, mpi_comm, in_current_iteration,
                           std::move(p2p_computer), std::move(inner_computer), cutoff);
}


#endif
