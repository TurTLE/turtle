/******************************************************************************
*                                                                             *
*  Copyright 2019 the TurTLE team                                             *
*                                                                             *
*  This file is part of TurTLE.                                               *
*                                                                             *
*  TurTLE is free software: you can redistribute it and/or modify             *
*  it under the terms of the GNU General Public License as published          *
*  by the Free Software Foundation, either version 3 of the License,          *
*  or (at your option) any later version.                                     *
*                                                                             *
*  TurTLE is distributed in the hope that it will be useful,                  *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
*  GNU General Public License for more details.                               *
*                                                                             *
*  You should have received a copy of the GNU General Public License          *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>             *
*                                                                             *
* Contact: Cristian.Lalescu@ds.mpg.de                                         *
*                                                                             *
******************************************************************************/



#ifndef ABSTRACT_PARTICLES_SYSTEM_HPP
#define ABSTRACT_PARTICLES_SYSTEM_HPP

//- Not generic to enable sampling begin
#include "field.hpp"
//- Not generic to enable sampling end

#include <memory>

template <class partsize_t, class real_number>
class abstract_particles_system {
public:
    virtual ~abstract_particles_system() noexcept(false){}

    virtual void delete_particles_from_indexes(const std::vector<partsize_t>& inIndexToDelete) = 0;

    virtual void compute() = 0;

    virtual void compute_p2p() = 0;

    virtual void compute_particles_inner() = 0;

    virtual void enforce_unit_orientation() = 0;

    virtual void add_Lagrange_multipliers() = 0;

    virtual void compute_sphere_particles_inner(const real_number particle_extra_rhs[]) = 0;
    virtual void compute_ellipsoid_particles_inner(const real_number particle_extra_rhs[]) = 0;

    virtual void move(const real_number dt) = 0;

    virtual void redistribute() = 0;

    virtual void inc_step_idx() = 0;

    virtual void shift_rhs_vectors() = 0;

    virtual void completeLoop(const real_number dt) = 0;
    virtual void complete2ndOrderLoop(const real_number dt) = 0;

    virtual void completeLoopWithVorticity(
            const real_number dt,
            const real_number sampled_vorticity[]) = 0;

    virtual void completeLoopWithVelocityGradient(
            const real_number dt,
            const real_number sampled_velocity_gradient[]) = 0;

    virtual const real_number* getParticlesState() const = 0;

    virtual std::unique_ptr<real_number[]> extractParticlesState(const int firstState, const int lastState) const = 0;

    virtual const std::unique_ptr<real_number[]>* getParticlesRhs() const = 0;

    virtual const partsize_t* getParticlesIndexes() const = 0;

    virtual partsize_t getLocalNbParticles() const = 0;

    virtual partsize_t getGlobalNbParticles() const = 0;

    virtual int getNbRhs() const = 0;

    virtual int get_step_idx() const = 0;

    //- Not generic to enable sampling begin
    virtual void sample_compute_field(const field<float, FFTW, ONE>& sample_field,
                                real_number sample_rhs[]) = 0;
    virtual void sample_compute_field(const field<float, FFTW, THREE>& sample_field,
                                real_number sample_rhs[]) = 0;
    virtual void sample_compute_field(const field<float, FFTW, THREExTHREE>& sample_field,
                                real_number sample_rhs[]) = 0;
    virtual void sample_compute_field(const field<double, FFTW, ONE>& sample_field,
                                real_number sample_rhs[]) = 0;
    virtual void sample_compute_field(const field<double, FFTW, THREE>& sample_field,
                                real_number sample_rhs[]) = 0;
    virtual void sample_compute_field(const field<double, FFTW, THREExTHREE>& sample_field,
                                real_number sample_rhs[]) = 0;
    //- Not generic to enable sampling end

    template <typename rnumber, field_backend be, field_components fc>
    void completeLoopWithExtraField(
            const real_number dt,
            const field<rnumber, be, fc>& in_field) {
        static_assert((fc == THREE) || (fc == THREExTHREE), "only THREE or THREExTHREE is supported for now");
        if (fc == THREE)
        {
            std::unique_ptr<real_number[]> extra_rhs(new real_number[getLocalNbParticles()*3]());
            std::fill_n(extra_rhs.get(), 3*getLocalNbParticles(), 0);
            sample_compute_field(in_field, extra_rhs.get());
            completeLoopWithVorticity(dt, extra_rhs.get());
        }
        else if (fc == THREExTHREE)
        {
            std::unique_ptr<real_number[]> extra_rhs(new real_number[getLocalNbParticles()*9]());
            std::fill_n(extra_rhs.get(), 9*getLocalNbParticles(), 0);
            sample_compute_field(in_field, extra_rhs.get());
            completeLoopWithVelocityGradient(dt, extra_rhs.get());
        }
    }

    virtual int setParticleFileLayout(std::vector<hsize_t>) = 0;
    virtual std::vector<hsize_t> getParticleFileLayout() = 0;
};

#endif
