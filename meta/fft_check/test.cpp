#include "main_code.hpp"

#include <random>

/****************************/
// parameters

const int nx = 4096;
const int ny = 8;
const int nz = 4096;

const int nsteps = 100;

/****************************/

int print_plan(fftw_plan &pl)
{
    char *plan_information = fftw_sprint_plan(pl);
    if (myrank == 0)
        DEBUG_MSG("\n\n%s\n\n", plan_information);

    free(plan_information);
    return EXIT_SUCCESS;
}

int test_fft(
        int argc,
        char *argv[],
        bool FPE)
{
    ptrdiff_t nfftw[3] = {nz, ny, nx};
    ptrdiff_t local_n0, local_0_start;
    ptrdiff_t local_n1, local_1_start;

    double *data = NULL;

    fftw_plan c2r_plan;
    fftw_plan r2c_plan;
    unsigned fftw_plan_rigor = FFTW_MEASURE;

    ptrdiff_t local_size = fftw_mpi_local_size_many_transposed(
            3, nfftw, 1,
            FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
            MPI_COMM_WORLD,
            &local_n0, &local_0_start,
            &local_n1, &local_1_start);

    /************/
    /* ALLOCATE */
    /************/
    data = fftw_alloc_real(local_size*2);

    c2r_plan = fftw_mpi_plan_many_dft_c2r(
            3, nfftw, 1,
            FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
            (fftw_complex*)(data),
            data,
            MPI_COMM_WORLD,
            fftw_plan_rigor | FFTW_MPI_TRANSPOSED_IN);

    assert(c2r_plan != NULL);

    r2c_plan = fftw_mpi_plan_many_dft_r2c(
            3, nfftw, 1,
            FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
            data,
            (fftw_complex*)(data),
            MPI_COMM_WORLD,
            fftw_plan_rigor | FFTW_MPI_TRANSPOSED_OUT);

    assert(r2c_plan != NULL);

    DEBUG_MSG("r2c plan representation\n");
    print_plan(r2c_plan);
    DEBUG_MSG("c2r plan representation\n");
    print_plan(c2r_plan);

    // fill up data
    std::random_device rd{};
    std::mt19937 gen{rd()};

    std::normal_distribution<> gaussian;


    for (ptrdiff_t ii = 0; ii < local_size; ii++)
        data[ii] = gaussian(gen);

    // start mpi profiling
    MPI_Pcontrol(5);

    for (ptrdiff_t tt = 0; tt < nsteps; tt++)
    {
        fftw_execute(r2c_plan);
        fftw_execute(c2r_plan);
        #pragma omp parallel for schedule(static)
        for(ptrdiff_t ii = 0; ii < local_size; ii++)
            data[ii] /= nx*ny*nz;
    }

    // stop mpi profiling
    MPI_Pcontrol(-5);


    /************/
    /* FREE     */
    /************/
    fftw_free(data);
    fftw_destroy_plan(c2r_plan);
    fftw_destroy_plan(r2c_plan);

    return EXIT_SUCCESS;
}

int main(int argc,
         char *argv[])
{
    return main_wrapper(argc, argv, true, test_fft);
}

