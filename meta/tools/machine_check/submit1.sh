#! /usr/bin/bash
#SBATCH -J machine_check #Jobname
#SBATCH -o job.%j.out #Job output file
#SBATCH -e job.%j.err #Job error file
#SBATCH --nodes 8 #Number of nodes
#SBATCH --ntasks-per-node 32 #Cores per node
#SBATCH --ntasks-per-core 1 #threads per core
#SBATCH -t 00:05:00 #Requested Walltime

source module_setup.sh

#-------------------------------------------------------------------------------
#Set number of threads (per process)
export OMP_NUM_THREADS=1
export OMP_PLACES=cores
export OMP_PROC_BIND=close

#-------------------------------------------------------------------------------
#Execute
mpirun -np ${SLURM_NTASKS} build/check1 &> output1.out
set ERR = $?

#-------------------------------------------------------------------------------
#Output and clean up
echo 'Job ended!'
exit $ERR
