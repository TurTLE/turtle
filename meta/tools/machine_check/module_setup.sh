#! /bin/bash

# modules to load for gcc compilation
module purge
module load cmake
module load inteloneapi/compiler/2022.0.1
module load inteloneapi/mpi/2021.5.0
module load fftw/3.3.10
module load phdf5/1.12.1
module load python/3.9.9

# PINCHECK library
export PINCHECK_ROOT=${HOME}/.local/

# module-dependent variables
export CC=icc
export CXX=icpc
export MPI_HOME=${I_MPI_ROOT}

export FFTW_BASE=${FFTW_DIR}
export FFTW_ROOT=${FFTW_DIR}

export HDF5_BASE=${HDF5_DIR}
export HDF5_ROOT=${HDF5_DIR}

export PATH=${CUSTOM_INSTALL_PATH}/bin:${PATH}
export CMAKE_PREFIX_PATH=${CUSTOM_INSTALL_PATH}/lib
