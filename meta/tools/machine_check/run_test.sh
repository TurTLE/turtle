source module_setup.sh

rm -rf build
mkdir build
cd build
cmake ..

VERBOSE=1 make

cd ..

sbatch ./submit0.sh
sbatch ./submit1.sh
sbatch ./submit2.sh
