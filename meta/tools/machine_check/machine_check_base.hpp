/**********************************************************************
*                                                                     *
*  Copyright 2022 Max Planck Computing and Data Facility              *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/


#ifndef __MACHINE_CHECK_BASE_HPP__
#define __MACHINE_CHECK_BASE_HPP__

#include <string>
#include <iostream>
#include <mpi.h>
#include <omp.h>

namespace mpi_vars
{
int myrank, nprocs;
}

#ifdef PINCHECK_FOUND
#define PINCHK_USE_MPI
#include <pincheck.hpp>

void print_pinning_info(void)
{
    // obtain string with pinning information on rank 0,
    // ranks >0 get an empty string
    const std::string pinning_info = pincheck::pincheck();
    if (mpi_vars::myrank == 0)
    {
        std::cerr << "### pinning info begin" << std::endl;
        std::cerr << pinning_info;
        std::cerr << "### pinning info end" << std::endl;
    }
}

#else

#define print_pinning_info(...)

#endif

#endif//__MACHINE_CHECK_BASE_HPP__


