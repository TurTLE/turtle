/**********************************************************************
*                                                                     *
*  Copyright 2022 Max Planck Computing and Data Facility              *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#include "machine_check_base.hpp"

#include <cassert>
#include <mpi.h>

int main(
        int argc,
        char *argv[])
{
    int mpiprovided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpiprovided);
    assert(mpiprovided >= MPI_THREAD_FUNNELED);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_vars::myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_vars::nprocs);

    if (mpi_vars::myrank == 0)
    {
        std::cerr <<  "nprocs = " << mpi_vars::nprocs << std::endl;
    }

    print_pinning_info();

    MPI_Finalize();
    return EXIT_SUCCESS;
}

