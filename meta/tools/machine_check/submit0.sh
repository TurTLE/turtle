#! /usr/bin/bash
#SBATCH -J machine_check #Jobname
#SBATCH -o job.%j.out #Job output file
#SBATCH -e job.%j.err #Job error file
#SBATCH --nodes=8 #Number of nodes
#SBATCH --ntasks-per-node=32 #Cores per node
#SBATCH --time=00:05:00 #Requested Walltime

source module_setup.sh

#-------------------------------------------------------------------------------
#Set number of threads (per process)
echo "slurm cpus per task is ${SLURM_CPUS_PER_TASK}"
echo "slurm ntasks is ${SLURM_NTASKS}"
export OMP_NUM_THREADS=1
export OMP_PLACES=cores
export OMP_PROC_BIND=close

#-------------------------------------------------------------------------------
#Execute
mpirun -np ${SLURM_NTASKS} build/check0 &> output0.out
set ERR = $?

#-------------------------------------------------------------------------------
#Output and clean up
echo 'Job ended!'
exit $ERR
