/**********************************************************************
*                                                                     *
*  Copyright 2022 Max Planck Computing and Data Facility              *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: Cristian.Lalescu@mpcdf.mpg.de                              *
*                                                                     *
**********************************************************************/



#include "machine_check_base.hpp"

#include <cassert>
#include <mpi.h>
#include <omp.h>
#include <fftw3-mpi.h>

using namespace mpi_vars;

int main(
        int argc,
        char *argv[])
{
    std::string simname = "test_machine";

    /* initialize MPI environment */
#ifdef NO_FFTWOMP
    MPI_Init(&argc, &argv);
    // turn off MPI profiling for initialization
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    print_pinning_info();

    fftw_mpi_init();
    fftwf_mpi_init();
#else
    int mpiprovided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpiprovided);
    assert(mpiprovided >= MPI_THREAD_FUNNELED);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

    print_pinning_info();

    const int nThreads = omp_get_max_threads();
    if (nThreads > 1){
        fftw_init_threads();
        fftwf_init_threads();
    }
    fftw_mpi_init();
    fftwf_mpi_init();
    if (nThreads > 1){
        fftw_plan_with_nthreads(nThreads);
        fftwf_plan_with_nthreads(nThreads);
    }
#endif

    /* import fftw wisdom */
    if (myrank == 0)
    {
        fftwf_import_wisdom_from_filename(
                (simname + std::string("_fftwf_wisdom.txt")).c_str());
        fftw_import_wisdom_from_filename(
                (simname + std::string("_fftw_wisdom.txt")).c_str());
    }
    fftwf_mpi_broadcast_wisdom(MPI_COMM_WORLD);
    fftw_mpi_broadcast_wisdom(MPI_COMM_WORLD);

    fftwf_set_timelimit(300);
    fftw_set_timelimit(300);

    /* export fftw wisdom */
    fftwf_mpi_gather_wisdom(MPI_COMM_WORLD);
    fftw_mpi_gather_wisdom(MPI_COMM_WORLD);
    MPI_Barrier(MPI_COMM_WORLD);
    if (myrank == 0)
    {
        fftwf_export_wisdom_to_filename(
                (simname + std::string("_fftwf_wisdom.txt")).c_str());
        fftw_export_wisdom_to_filename(
                (simname + std::string("_fftw_wisdom.txt")).c_str());
    }

  /* clean up */
    fftwf_mpi_cleanup();
    fftw_mpi_cleanup();
#ifndef NO_FFTWOMP
    if (nThreads > 1){
        fftw_cleanup_threads();
        fftwf_cleanup_threads();
    }
#endif

    MPI_Finalize();
    return EXIT_SUCCESS;
}

