import numpy as np

def distribute_cores_evenly(
        nprocesses = 4,
        nthreads_per_process = 3,
        total_number_of_cores = 16):
    assert(nprocesses*nthreads_per_process <= total_number_of_cores)

    # first, determine how many total cores we can allocate per process
    max_cores_per_process = total_number_of_cores // nprocesses

    # then spread useful cores evenly throughout the total cores per process
    # start with no cores allocated
    single_process_mask = np.zeros(max_cores_per_process, np.bool)
    # allocate cores evenly
    skip = max_cores_per_process // nthreads_per_process
    for t in range(nthreads_per_process):
        single_process_mask[t*skip] = 1

    single_process_mask = sum(int(single_process_mask[i])*(2**i) for i in range(max_cores_per_process))

    # now create full node mask:
    all_masks = []
    for p in range(nprocesses):
        all_masks.append(single_process_mask*(2**(max_cores_per_process*p)))
    return all_masks

def main():
    """
        The point is, in principle, to generate a submission script from python,
        and change the core allocation masks from within python (thus allowing
        to generate multiple submision scripts at the same time).
        This is why I generate an `export SLURM_CPU_BIND` line, which will be
        specific to each script, but can be used in a generic `srun ${SLURM...`
        line afterwards.
    """
    core_masks = distribute_cores_evenly(
            nprocesses = 8,
            nthreads_per_process = 3,
            total_number_of_cores = 40)
    print('place following line in SLURM script:\n' +
          'export SLURM_CPU_BIND_OPTION="--cpu-bind=verbose,mask_cpu:' +
          ','.join(['0x{0:x}'.format(mm) for mm in core_masks]) + '"\n' +
          'then use `srun ${SLURM_CPU_BIND_OPTION}` in the script to launch the executable')
    return None

if __name__ == '__main__':
    main()

