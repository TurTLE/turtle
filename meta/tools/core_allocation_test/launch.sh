#! /bin/bash

set -e

# number of nodes to use
NN=2
# number of processes per node
NP=16
# number of cores per node --- depends on machine TODO: change as required
CPN=48

CURRENT_WORK_DIR="test_nn${NN}_np${NP}"

rm -rf ${CURRENT_WORK_DIR}
mkdir ${CURRENT_WORK_DIR}
cd ${CURRENT_WORK_DIR}

cp ../test.cpp ./
bash ../compile.sh
cp ../run_test_template.sh ./run_test.sh
sed -i "s/NN/${NN}/" run_test.sh
sed -i "s/NP/${NP}/" run_test.sh
sed -i "s/ONT/$(expr ${CPN} / ${NP})/" run_test.sh

# the following need to be changed according to your setup
module load lrz/default
module load lrztools
module load slurm_setup

sbatch run_test.sh

