/**********************************************************************
*                                                                     *
*  Copyright 2015 Max Planck Institute                                *
*                 for Dynamics and Self-Organization                  *
*                                                                     *
*  This file is part of TurTLE.                                       *
*                                                                     *
*  TurTLE is free software: you can redistribute it and/or modify     *
*  it under the terms of the GNU General Public License as published  *
*  by the Free Software Foundation, either version 3 of the License,  *
*  or (at your option) any later version.                             *
*                                                                     *
*  TurTLE is distributed in the hope that it will be useful,          *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
*  GNU General Public License for more details.                       *
*                                                                     *
*  You should have received a copy of the GNU General Public License  *
*  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>     *
*                                                                     *
* Contact: berenger.bramas@inria.fr                                   *
*                                                                     *
**********************************************************************/

/* Simple code to check MPI/OpenMP process/thread distribution on cluster */


#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <sys/types.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sched.h>

#include <iostream>
#include <vector>
#include <cassert>
#include <sstream>
#include <omp.h>
#include <mpi.h>
#include <algorithm>

std::vector<long int> GetBindingList(){
    std::vector<long int> list;

    cpu_set_t mask;
    CPU_ZERO(&mask);
    pid_t tid = static_cast<pid_t>(syscall(SYS_gettid));
    // Get the affinity
    int retValue = sched_getaffinity(tid, sizeof(mask), &mask);
    assert(retValue == 0);

    for(size_t idx = 0 ; idx < sizeof(long int)*8-1 ; ++idx){
        if(CPU_ISSET(idx, &mask)){
             list.push_back(idx);
        }
    }
    return list;
}

int main(
        int argc,
        char *argv[]){
    MPI_Init(&argc, &argv);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    std::ostringstream stream;

    stream << "[" << world_rank << "] is on node " << (getenv("HOSTNAME")?getenv("HOSTNAME"):"HOSTNAME is null") << "\n";
    {

        const std::vector<long int> cores = GetBindingList();

        stream << "[" << world_rank << "] Available cores for master process = ";

        for(long int core : cores){
            stream << core << "  ";
        }

        stream << "\n";
    }

    #pragma omp parallel
    {
        for(int idxThread = 0 ; idxThread < omp_get_num_threads() ; ++idxThread){
            if(idxThread == omp_get_thread_num()){
                const std::vector<long int> cores = GetBindingList();

                stream << "[" << world_rank << "] Available cores for thread " << omp_get_thread_num() << " = ";

                for(long int core : cores){
                    stream << core << "  ";
                }

                stream << "\n";
            }
            #pragma omp barrier
        }
    }


    if( world_rank == 0){
        std::cout << "==========  Process 0 =========" << std::endl;
        std::cout << stream.str() << std::endl;

        std::vector<char> buffer;
        for(int idxProcess = 1 ; idxProcess < world_size ; ++idxProcess){
            int length = 0;
            MPI_Recv(&length, 1, MPI_INT, idxProcess, idxProcess*3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            buffer.resize(length);
            MPI_Recv(buffer.data(), length, MPI_CHAR, idxProcess, idxProcess*3+2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            buffer.push_back('\0');
            std::cout << "==========  Process " << idxProcess << " =========" << std::endl;
            std::cout << buffer.data() << std::endl;
        }
    }
    else{
        const std::string str = stream.str();
        int length = str.size();
        MPI_Send(&length, 1, MPI_INT, 0, world_rank*3, MPI_COMM_WORLD);
        MPI_Send(str.c_str(), length, MPI_CHAR, 0, world_rank*3+2, MPI_COMM_WORLD);
    }

    MPI_Finalize();

    return 0;
}

