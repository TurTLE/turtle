#!/bin/bash -l
#SBATCH -J test_pinning
#SBATCH -e err_test_pinning_nnNN_npNP
#SBATCH -o out_test_pinning_nnNN_npNP
#SBATCH -D ./
#SBATCH --mail-type=END
#SBATCH --mail-user=INSERT_EMAIL_HERE
#SBATCH --partition=micro
#SBATCH --nodes=NN
#SBATCH --ntasks-per-node=NP
#SBATCH --cpus-per-task=ONT
#SBATCH --time=0:05:00
#SBATCH --no-requeue
#SBATCH --export=NONE
#SBATCH --get-user-env
#SBATCH --account=INSERT_ACCOUNT_TO_BE_CHARGED_HERE

############################################################################
###############################BEGIN########################################
# feel free to rename this file to whatever, but it should load all required modules
source ~/.config/TurTLE/bashrc

export OMP_NUM_THREADS=ONT

echo "Start time is `date`"

echo "==========================================="
echo "srun ./test.exe"
srun ./test.exe

echo "==========================================="
echo "OMP_PLACES=cores mpiexec ./test.exe"
OMP_PLACES=cores mpiexec ./test.exe

echo "==========================================="
echo "OMP_PLACES=cores mpiexec -n $SLURM_NTASKS ./test.exe"
OMP_PLACES=cores mpiexec -n $SLURM_NTASKS ./test.exe

echo "==========================================="
echo "I_MPI_PIN_DOMAIN=omp:compact OMP_PLACES=cores mpiexec -n $SLURM_NTASKS ./test.exe"
I_MPI_PIN_DOMAIN=omp:compact OMP_PLACES=cores mpiexec -n $SLURM_NTASKS ./test.exe

echo "==========================================="
echo "I_MPI_PIN_CELL=core I_MPI_PIN_DOMAIN=omp:compact OMP_PLACES=cores mpiexec -n $SLURM_NTASKS ./test.exe"
I_MPI_PIN_CELL=core I_MPI_PIN_DOMAIN=omp:compact OMP_PLACES=cores mpiexec -n $SLURM_NTASKS ./test.exe

echo "==========================================="
echo "I_MPI_PIN_CELL=core I_MPI_PIN_DOMAIN=omp:compact KMP_AFFINITY=verbose,compact mpiexec -n $SLURM_NTASKS ./test.exe"
I_MPI_PIN_CELL=core I_MPI_PIN_DOMAIN=omp:compact KMP_AFFINITY=verbose,compact mpiexec -n $SLURM_NTASKS ./test.exe

echo "End time is `date`"
exit 0
###############################END###########################################
#############################################################################

