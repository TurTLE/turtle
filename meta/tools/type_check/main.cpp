#include <iostream>
#include <hdf5.h>

int main()
{
    std::cout << "Checking `sizeof` for data types used in TurTLE" << std::endl;
    std::cout << "sizeof(int)" << sizeof(int) << std::endl;
    std::cout << "sizeof(ptrdiff_t)" << sizeof(ptrdiff_t) << std::endl;
    std::cout << "sizeof(hsize_t)" << sizeof(hsize_t) << std::endl;
    return EXIT_SUCCESS;
}

