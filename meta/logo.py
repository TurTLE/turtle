import matplotlib.path
Path = matplotlib.path.Path
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np

MAX_PLANCK_GREEN = (0, 125/255., 122./255)

def typed():
    plt.rcParams.update({
        'text.usetex' : False,
        'font.serif' : ['Gentium Book Basic'],
        'font.sans-serif' : ['DejaVu Sans'],
        'mathtext.fallback' : 'cm',
        })

    f = plt.figure(figsize = (4, 2.25))
    a = f.add_axes([0, 0, 1, 1])

    # T
    a.text(-1.1, 0.1, 'T', fontsize = 160,
            horizontalalignment = 'center',
            fontfamily = 'sans',
            verticalalignment = 'center',
            color = MAX_PLANCK_GREEN,
            )

    # ur
    a.text(0, 0, 'ur', fontsize = 80,
            horizontalalignment = 'center',
            #fontfamily = 'serif',
            fontfamily = 'sans',
            #fontweight = 'bold',
            color = MAX_PLANCK_GREEN,
            )

    # T
    a.text(1.2, 0.1, 'T', fontsize = 160,
            horizontalalignment = 'center',
            fontfamily = 'sans',
            verticalalignment = 'center',
            color = MAX_PLANCK_GREEN,
            )

    # L
    a.text(2.3, 0, 'L', fontsize = 80,
            fontfamily = 'sans',
            horizontalalignment = 'center',
            color = MAX_PLANCK_GREEN,
            )

    # E
    a.text(3.1, 0, 'E', fontsize = 80,
            fontfamily = 'sans',
            horizontalalignment = 'center',
            color = MAX_PLANCK_GREEN,
            )
    a.set_aspect('equal')
    a.set_axis_off()
    a.set_xlim(-2.35, 3.55)
    a.set_ylim(-1., 2.3)
    f.savefig('TurTLE_logo.pdf')
    return None

def drawn_full():
    plt.rcParams.update({
        'text.usetex' : False,
        'font.family' : 'sans-serif',
        'font.sans-serif' : ['Linux Biolinum O'],
        'font.weight' :'bold',
        'mathtext.fallback' : 'cm',
        })

    f = plt.figure(figsize = (4, 2.25))
    a = f.add_axes([0, 0, 1, 1])

    # bottom T
    pp1 = mpatches.PathPatch(
        Path([(-1, 1),
              (-1.2, 1),
              (-2, -0.7),
              (-1.5, -0.7),
              (-0.8, -0.7),
              (-0.8, 1),
              (-1, 1),
              (-1, 1)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc=MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    # top T
    pp1 = mpatches.PathPatch(
        Path([(-2.5, 0),
              (-2, 0.125),
              (-2, 2.1),
              (-0.355, 2.1)] +
             smooth_corner_coords(
                 p0 = (-0.355, 2.1),
                 p1 = ( 0.055, 1.7),
                 angle = np.pi/4) +
             smooth_corner_coords(
                 p0 = ( 0.055, 1.7),
                 p1 = (-0.355, 1.3),
                 angle = -np.pi/4) +
             [(-2, 1.3),
              (-1.5, 0),
              (-2.5, 0),
              (-2.5, 0),
              ],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc = MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    # ur
    a.text(0, 0, 'ur', fontsize = 72,
            horizontalalignment = 'center',
            #verticalalignment = 'center',
            color = MAX_PLANCK_GREEN,
            )

    # bottom T
    pp1 = mpatches.PathPatch(
        Path([(1, 1),
              (1.2, 1),
              (2, -0.7),
              (1.5, -0.7),
              (0.8, -0.7),
              (0.8, 1),
              (1, 1),
              (1, 1)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc=MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    # top T
    pp1 = mpatches.PathPatch(
        Path([
            (0.515, 2.1)] +
             smooth_corner_coords(
                 p0 = (0.515, 2.1),
                 p1 = (0.115, 1.7),
                 angle = 3*np.pi/4) +
             smooth_corner_coords(
                 p0 = (0.115, 1.7),
                 p1 = (0.515, 1.3),
                 angle = -3*np.pi/4) +
             [(0.9, 1.3),
              (1.05, 1.1),
              (1.275, 1.1),
              (1.3, 1.1),
              (1.35, 1.1),
              (1.45, 1.2)] +
             smooth_corner_coords(
                 p0 = (1.45, 1.2),
                 p1 = (1.45, 1.6),
                 angle = 0) +
             [(1.4, 1.65),
              (1., 2.1),
              (0.415, 2.1),
              (0.415, 2.1)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc = MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    # L
    pp1 = mpatches.PathPatch(
        Path([
            (1.5, 1.15)] +
             smooth_corner_coords(
                 p0 = (1.5, 1.15),
                 p1 = (1.8, 1.15),
                 angle = np.pi/2) +
             [(1.8, 1.15),
              (2.15, 0.75),
              (2.15, 0.25),
              (2.45, 0.25),
              (2.47, 0.2),
              (2.6, 0.02),
              (2.75, 0.),
              (1.9, 0.),
              (1.9, 0.25),
              (1.8, 0.75),
              (1.5, 0.95),
              ] +
             smooth_corner_coords(
                 p0 = (1.5, 0.95),
                 p1 = (1.5, 1.15),
                 angle = -np.pi) +
             [(1.5, 1.15)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.LINETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.LINETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc = MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    # E
    pp1 = mpatches.PathPatch(
        Path([
              (2.5, 0.4),
              (2.5, 1.9),
              (3.2, 1.3),
              (3.2, 0.75),
              (2.7, 1.2),
              (2.6, 0.6),
              (3.1, 0.6),
              (2.7, 0.6),
              (2.6, 0.2),
              (3.2, 0.4),
              (3.2, -0.2),
              (2.5, 0.),
              (2.5, 0.4),
              (2.5, 0.4)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc=MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    ## LE
    #pp1 = mpatches.PathPatch(
    #    Path([(1.9, 0),
    #          (1.9, 0.75),
    #          (1.3, 1.5),
    #          (0, 1.5),
    #          (-2, 1.5),
    #          (-1.5, 0),
    #          (-2.5, 0),
    #          (-2, 0.125),
    #          (-2, 2.2),
    #          (0, 2.2),
    #          (1.5, 2.2),
    #          (2.1, 0.8),
    #          (2.1, 0.2),
    #          (2.5, 0.2),
    #          (2.5, 1.9),
    #          (3.2, 1.3),
    #          (3.2, 0.75),
    #          (2.7, 1.2),
    #          (2.6, 0.6),
    #          (3.1, 0.6),
    #          (2.7, 0.6),
    #          (2.6, 0.2),
    #          (3.2, 0.4),
    #          (3.2, -0.2),
    #          (2.5, 0.),
    #          (1.9, 0),
    #          (1.9, 0)],
    #         [Path.MOVETO,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE3,
    #          Path.LINETO,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CURVE4,
    #          Path.CLOSEPOLY]),
    #    #fc=MAX_PLANCK_GREEN,
    #    fc='none',
    #    linestyle = ':',
    #    ec='black',
    #    transform=a.transData)
    #a.add_patch(pp1)

    a.set_aspect('equal')
    a.set_axis_off()
    a.set_xlim(-2.6, 3.3)
    a.set_ylim(-1., 2.3)
    f.savefig('TurTLE_logo.pdf')
    f.savefig('TurTLE_logo.svg')
    return None

def drawn():
    plt.rcParams.update({
        'text.usetex' : False,
        'font.family' : 'sans-serif',
        'font.sans-serif' : ['Gentium Book Basic'],
        'font.weight' :'bold',
        'mathtext.fallback' : 'cm',
        })


    f = plt.figure(figsize = (4, 2.25))
    a = f.add_axes([0, 0, 1, 1])

    # T
    pp1 = mpatches.PathPatch(
        Path([(-1, 1), (-2.5, -1.5), (-0.5, -1.5), (-1, 1), (-1, 1)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc=MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    # ur
    a.text(0, 0, 'ur', fontsize = 80,
            horizontalalignment = 'center',
            #verticalalignment = 'center',
            color = MAX_PLANCK_GREEN,
            )

    # T
    pp1 = mpatches.PathPatch(
        Path([(1, 1), (2.5, -1.5), (0.5, -1.5), (1, 1), (1, 1)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc=MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    # LE
    pp1 = mpatches.PathPatch(
        Path([(1.9, 0),
              (1.9, 0.75),
              (1.3, 1.5),
              (0, 1.5),
              (-2, 1.5),
              (-1.5, 0),
              (-2.5, 0),
              (-2, 0.125),
              (-2, 2.2),
              (0, 2.2),
              (1.5, 2.2),
              (2.1, 0.8),
              (2.1, 0.2),
              (2.5, 0.2),
              (2.5, 1.9),
              (3.2, 1.3),
              (3.2, 0.75),
              (2.7, 1.2),
              (2.6, 0.6),
              (3.1, 0.6),
              (2.7, 0.6),
              (2.6, 0.2),
              (3.2, 0.4),
              (3.2, -0.2),
              (2.5, 0.),
              (1.9, 0),
              (1.9, 0)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE3,
              Path.LINETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc=MAX_PLANCK_GREEN,
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)

    a.set_aspect('equal')
    a.set_axis_off()
    a.set_xlim(-2.6, 3.3)
    a.set_ylim(-1., 2.3)
    f.savefig('TurTLE_logo.pdf')
    return None

def smooth_corner_coords(
        p0 = (-1, 1),
        p1 = (1, -1),
        angle = 0):
    O = ((p0[0] + p1[0])/2,
         (p0[1] + p1[1])/2)
    dP = (p1[0] - p0[0], p1[1] - p0[1])
    radius = 0.5*(dP[0]**2+dP[1]**2)**0.5
    PP = (O[0] + radius*np.cos(angle),
          O[1] + radius*np.sin(angle))
    print(PP)
    p2 = ((p0[0] + PP[0])/2,
          (p0[1] + PP[1])/2)
    p4 = ((p1[0] + PP[0])/2,
          (p1[1] + PP[1])/2)
    p3 = ((p2[0] + p4[0])/2,
          (p2[1] + p4[1])/2)
    return [p2, p2, p3, p4, p4, p1]

def test_corner():
    f = plt.figure(figsize = (4, 2.25))
    a = f.add_axes([0, 0, 1, 1])
    print(
             smooth_corner_coords())

    pp1 = mpatches.PathPatch(
        Path(#[(-1, 1),
             # (0., 1),
             # (0., 1),
             # (0.5, 0.5),
             # (1, 0),
             # (1, 0),
             # (1, -1),
             # (1, -1)],
             [(-1, 1)]+
             smooth_corner_coords(angle = np.pi/4)+
             smooth_corner_coords(p0 = (1, -1), p1 = (-0.5, 1), angle = -3*np.pi/4)+
             [(-1, 1)],
             [Path.MOVETO,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CURVE4,
              Path.CLOSEPOLY]),
        fc='none',
        ec=MAX_PLANCK_GREEN,
        transform=a.transData)
    a.add_patch(pp1)
    a.set_axis_off()

    a.scatter([0, 0, 1, 1], [0, 1, 0, 1])

    a.set_xlim(-2, 2)
    a.set_ylim(-2, 2)
    a.set_aspect('equal')
    f.savefig('tmp.pdf')
    return None

if __name__ == '__main__':
    drawn_full()
    #test_corner()

