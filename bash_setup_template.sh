#! /usr/bin/env bash -l

# Load dependencies here if necessary, e.g. `module load cmake`

# module-dependent variables
export CC=<CC>                # Preferred C compiler, e.g. `gcc`
export CXX=<CXX>              # Preferred C++ compiler, e.g. `g++`

export MPI_HOME=<MPI_HOME>    # MPI installation directory, e.g. ${I_MPI_ROOT}
# see also https://cmake.org/cmake/help/latest/module/FindMPI.html

export PKG_CONFIG_PATH=<FFTW_DIR>/lib/pkgconfig:${PKG_CONFIG_PATH} # for FFTW
# see also `cmake/set_up_FFTW.cmake`

export HDF5_ROOT=<HDF5_DIR>   # Base directory of HDF5
# see also https://cmake.org/cmake/help/latest/module/FindHDF5.html

########################################################################
# OPTIONAL
#
# export GSL_ROOT_DIR=<GLS_DIR> # Base directory of GNU Scientific Library
# see also https://cmake.org/cmake/help/latest/module/FindGSL.html
#
# If this variable is exported, additional functionality is available to
# both TurTLE and projects that use TurTLE as a library.
########################################################################

########################################################################
# OPTIONAL
#
# export PINCHECK_ROOT=<PINCHECK_ROOT>
#
# If this variable is exported, you can check whether TurTLE
# MPI processes and OpenMP threads are pinned properly to hardware threads.
# It is available from https://gitlab.mpcdf.mpg.de/khr/pincheck.
# The pincheck headers need to be placed under `<PINCHECK_ROOT>/include`.
########################################################################

########################################################################
# OPTIONAL
#
# export TURTLE_COMPILATION_FLAGS=<COMPILATION_FLAGS>
#
# We also recommend setting this variable to optimize compilation
# For clusters of unknown architecture it helps to log into
# individual nodes and run the following command:
# gcc -march=native -Q --help=target
########################################################################

########################################################################
# NOTE
#
# For FFTW we rely on PkgConfig (see `cmake/set_up_FFTW.cmake`).
# If this approach does not work on your environment, this file may need
# to be updated; let us know.
########################################################################

# Turtle installation directory
export TURTLE_ROOT=<TURTLE_DIR>
# (library will go under ${TURTLE_ROOT}/lib, headers under ${TURTLE_ROOT}/include, etc.)

# location-dependent variables
export CUSTOM_INSTALL_PATH=${TURTLE_ROOT}
export PATH=${CUSTOM_INSTALL_PATH}/bin:${PATH}
export CMAKE_PREFIX_PATH=${CUSTOM_INSTALL_PATH}/lib
export CMAKE_MODULE_PATH=${CUSTOM_INSTALL_PATH}/lib # needed because
# `set_up_FFTW.cmake` will be placed there, for subsequent use by TurTLE or
# dependent packages.

# activate python virtual environment
source ${TURTLE_ROOT}/bin/activate
