import subprocess
import h5py
import os

if not os.path.exists('test.h5'):
    subprocess.check_call([
        'turtle',
        'DNS',
        'NSVE'])

subprocess.check_call([
    'turtle',
    'PP',
    'bandpass_stats',
    '--no-submit'])

f = h5py.File('test_post.h5', 'r')
print(f['bandpass_stats/parameters/k0list'][...])
print(f['bandpass_stats/parameters/k1list'][...])
print(f['bandpass_stats/parameters/filter_type'][...])
print(f['bandpass_stats/parameters/max_velocity_estimate'][...])
