#include <fftw3-mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

#ifndef FFTW_PLAN_RIGOR

#define FFTW_PLAN_RIGOR FFTW_ESTIMATE

#endif

//#define NO_FFTWOMP

#define NX 36
#define NY 36
#define NZ 12

const int nx = NX;
const int ny = NY;
const int nz = NZ;
const int npoints = NX*NY*NZ;

const double dkx = 1.0;
const double dky = 1.0;
const double dkz = 1.0;

int myrank, nprocs;

int main(
        int argc,
        char *argv[])
{
    ////////////////////////////////////
    /* initialize MPI environment */
#ifdef NO_FFTWOMP
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    fftw_mpi_init();
    fftwf_mpi_init();
    printf("There are %d processes\n", nprocs);
#else
    int mpiprovided;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpiprovided);
    assert(mpiprovided >= MPI_THREAD_FUNNELED);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    const int nThreads = omp_get_max_threads();
    printf("Number of threads for the FFTW = %d\n",
              nThreads);
    if (nThreads > 1){
        fftw_init_threads();
        fftwf_init_threads();
    }
    fftw_mpi_init();
    fftwf_mpi_init();
    printf("There are %d processes and %d threads\n",
              nprocs,
              nThreads);
    if (nThreads > 1){
        fftw_plan_with_nthreads(nThreads);
        fftwf_plan_with_nthreads(nThreads);
    }
#endif

    ////////////////////////////////////
    /* do useful work */

    // declarations
    ptrdiff_t nfftw[3];
    ptrdiff_t tmp_local_size;
    ptrdiff_t local_n0, local_0_start;
    ptrdiff_t local_n1, local_1_start;
    ptrdiff_t local_size;
    ptrdiff_t ix, iy, iz;
    ptrdiff_t jx, jy, jz;
    ptrdiff_t rindex, cindex;
    int cc;
    float *data0, *data;
    fftwf_complex *cdata;
    double L2norm0, L2norm1, L2norm2, L2normk;
    double local_L2norm0, local_L2norm1;
    fftwf_plan c2r_plan, r2c_plan;
    double *kx, *ky, *kz;

    // get sizes
    nfftw[0] = nz;
    nfftw[1] = ny;
    nfftw[2] = nx;
    tmp_local_size =  fftwf_mpi_local_size_many_transposed(
            3, nfftw, 3,
            FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK, MPI_COMM_WORLD,
            &local_n0, &local_0_start,
            &local_n1, &local_1_start);

    local_size = local_n1 * nz * nx * 3 * 2;

    // allocate field
    data = fftwf_alloc_real(
            local_size);
    data0 = fftwf_alloc_real(
            local_size);
    cdata = (fftwf_complex*)(data);

    c2r_plan = fftwf_mpi_plan_many_dft_c2r(
            3, nfftw, 3,
            FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
            cdata,
            data,
            MPI_COMM_WORLD,
            FFTW_PLAN_RIGOR | FFTW_MPI_TRANSPOSED_IN);

    r2c_plan = fftwf_mpi_plan_many_dft_r2c(
            3, nfftw, 3,
            FFTW_MPI_DEFAULT_BLOCK, FFTW_MPI_DEFAULT_BLOCK,
            data,
            cdata,
            MPI_COMM_WORLD,
            FFTW_PLAN_RIGOR | FFTW_MPI_TRANSPOSED_OUT);

    kx = (double*)malloc(sizeof(double)*(nx/2+1));
    ky = (double*)malloc(sizeof(double)*local_n1);
    kz = (double*)malloc(sizeof(double)*nz);

    // generate wavenumbers
    for (jy = 0; jy < local_n1; jy++)
    {
        if (jy + local_1_start <= ny/2)
            ky[jy] = dky*(jy + local_1_start);
        else
            ky[jy] = dky*((jy + local_1_start) - ny); }
    for (jz = 0; jz < nz; jz++)
    {
        if (jz <= nz/2)
            kz[jz] = dkz*jz;
        else
            kz[jz] = dkz*(jz - nz);
    }
    for (jx = 0; jx < nx/2+1; jx++)
    {
        kx[jx] = dkx*jx;
    }

    // fill field with random numbers
    // I'm generating cindex the stupid way, but we can also use
    // cindex = (jy*nz + jz)*(nx/2+1) + jx
    cindex = 0;
    for (jy = 0; jy < local_n1; jy++)
        for (jz = 0; jz < nz; jz++)
        {
            for (jx = 0; jx < nx/2+1; jx++)
            {
                double k2 = (kx[jx]*kx[jx] +
                             ky[jy]*ky[jy] +
                             kz[jz]*kz[jz]);
                if (jx == 0 && (jy + local_1_start) == 0 && jz == 0)
                    k2 = dkx*dkx + dky*dky + dkz*dkz;
                for (cc = 0; cc<3; cc++)
                {
                    cdata[cindex*3+cc][0] = (drand48()-0.5) / sqrt(k2);
                    cdata[cindex*3+cc][1] = (drand48()-0.5) / sqrt(k2);
                }
                cindex++;
            }
        }

    // go back and forth so that the
    // Fourier space representation is properly symmetrized
    fftwf_execute(c2r_plan);
    fftwf_execute(r2c_plan);
    // normalize, compute Fourier space L2 norm
    cindex = 0;
    local_L2norm0 = 0;
    for (jy = 0; jy < local_n1; jy++)
        for (jz = 0; jz < nz; jz++)
        {
            for (cc = 0; cc<3; cc++)
            {
                cdata[cindex*3+cc][0] /= npoints;
                cdata[cindex*3+cc][1] /= npoints;
                local_L2norm0 += (cdata[cindex*3+cc][0]*cdata[cindex*3+cc][0] +
                                  cdata[cindex*3+cc][1]*cdata[cindex*3+cc][1]);
            }
            cindex++;
            for (jx = 1; jx < nx/2+1; jx++)
            {
                for (cc = 0; cc<3; cc++)
                {
                    cdata[cindex*3+cc][0] /= npoints;
                    cdata[cindex*3+cc][1] /= npoints;
                    local_L2norm0 += 2*(cdata[cindex*3+cc][0]*cdata[cindex*3+cc][0] +
                                        cdata[cindex*3+cc][1]*cdata[cindex*3+cc][1]);
                }
                cindex++;
            }
        }
    MPI_Allreduce(
            &local_L2norm0,
            &L2normk,
            1,
            MPI_DOUBLE,
            MPI_SUM,
            MPI_COMM_WORLD);
    L2normk = sqrt(L2normk);

    // go to real space
    fftwf_execute(c2r_plan);

    // rindex = (iz*ny + iy)*(nx+2) + ix
    rindex = 0;
    local_L2norm0 = 0;
    for (iz = 0; iz < local_n0; iz++)
        for (iy = 0; iy < ny; iy++)
        {
            for (ix = 0; ix < nx; ix++)
            {
                for (cc = 0; cc<3; cc++)
                {
                    local_L2norm0 += data[rindex*3+cc]*data[rindex*3+cc];
                }
                rindex++;
            }
            for (ix = nx; ix < nx+2; ix++)
            {
                rindex++;
            }
        }
    MPI_Allreduce(
            &local_L2norm0,
            &L2norm1,
            1,
            MPI_DOUBLE,
            MPI_SUM,
            MPI_COMM_WORLD);
    L2norm1 = sqrt(L2norm1 / npoints);

    //fftwf_execute(r2c_plan);

    //cindex = 0;
    //local_L2norm0 = 0;
    //for (jy = 0; jy < local_n1; jy++)
    //    for (jz = 0; jz < nz; jz++)
    //    {
    //        for (cc = 0; cc<3; cc++)
    //        {
    //            local_L2norm0 += (cdata[cindex*3+cc][0]*cdata[cindex*3+cc][0] +
    //                              cdata[cindex*3+cc][1]*cdata[cindex*3+cc][1]);
    //        }
    //        cindex++;
    //        // I am not adding the energy from mode nx/2 as a matter of principle.
    //        for (jx = 1; jx < nx/2+1; jx++)
    //        {
    //            for (cc = 0; cc<3; cc++)
    //            {
    //                local_L2norm0 += 2*(cdata[cindex*3+cc][0]*cdata[cindex*3+cc][0] +
    //                                    cdata[cindex*3+cc][1]*cdata[cindex*3+cc][1]);
    //            }
    //            cindex++;
    //        }
    //    }
    //MPI_Allreduce(
    //        &local_L2norm0,
    //        &L2normk,
    //        1,
    //        MPI_DOUBLE,
    //        MPI_SUM,
    //        MPI_COMM_WORLD);
    //L2normk = sqrt(L2normk) / (nx*ny*nz);
    //fftwf_execute(c2r_plan);

    //// normalize
    //rindex = 0;
    //local_L2norm0 = 0;
    //local_L2norm1 = 0;
    //for (iz = 0; iz < local_n0; iz++)
    //    for (iy = 0; iy < ny; iy++)
    //    {
    //        for (ix = 0; ix < nx; ix++)
    //        {
    //            for (cc = 0; cc<3; cc++)
    //            {
    //                data[rindex*3+cc] /= (nx*ny*nz);
    //                local_L2norm0 += data[rindex*3+cc]*data[rindex*3+cc];
    //                local_L2norm1 += ((data0[rindex*3+cc] - data[rindex*3+cc])*
    //                                  (data0[rindex*3+cc] - data[rindex*3+cc]));
    //            }
    //            rindex++;
    //        }
    //        for (ix = nx; ix < nx+2; ix++)
    //        {
    //            rindex++;
    //        }
    //    }
    //MPI_Allreduce(
    //        &local_L2norm0,
    //        &L2norm1,
    //        1,
    //        MPI_DOUBLE,
    //        MPI_SUM,
    //        MPI_COMM_WORLD);
    //MPI_Allreduce(
    //        &local_L2norm1,
    //        &L2norm2,
    //        1,
    //        MPI_DOUBLE,
    //        MPI_SUM,
    //        MPI_COMM_WORLD);
    //L2norm1 = sqrt(L2norm1 / (nx*ny*nz));
    //L2norm2 = sqrt(L2norm2 / (nx*ny*nz));

    printf("FFTW_PLAN_RIGOR=%d\n", FFTW_PLAN_RIGOR);
    printf("L2normk = %g, L2norm1 = %g, relative error = %g\n",
            L2normk, L2norm1, fabs(L2normk - L2norm1) / (L2normk));

    // deallocate
    fftwf_destroy_plan(r2c_plan);
    fftwf_destroy_plan(c2r_plan);
    fftwf_free(data);
    fftwf_free(data0);
    free(kx);
    free(ky);
    free(kz);

    ////////////////////////////////////
    /* clean up */
    fftwf_mpi_cleanup();
    fftw_mpi_cleanup();

#ifndef NO_FFTWOMP
    if (nThreads > 1){
        fftw_cleanup_threads();
        fftwf_cleanup_threads();
    }
#endif

    MPI_Finalize();
    return EXIT_SUCCESS;
}

