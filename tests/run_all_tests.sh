#!/bin/bash

set -e

turtle.test_fftw
turtle.test_Parseval
turtle.test_NSVEparticles

turtle DNS static_field_with_ghost_collisions --simname dumb_test_of_ghost_collisions

# test postprocessing
turtle PP field_single_to_double --simname dns_nsveparticles --iter0 32 --iter1 32
turtle PP get_rfields --simname dns_nsveparticles --iter0 0 --iter1 64 --TrS2_on 1
turtle PP joint_acc_vel_stats --simname dns_nsveparticles --iter0 0 --iter1 64
turtle PP resize --simname dns_nsveparticles --new_nx 96 --new_ny 96 --new_nz 96 --new_simname dns_nsveparticles_resized
