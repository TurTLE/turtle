import sys
import numpy as np
import argparse
import os

import TurTLE

def get_DNS_parameters(
        DNS_type = 'A',
        N = 512,
        nnodes = 1,
        nprocesses = 1,
        output_on = False,
        cores_per_node = 16,
        nparticles = int(1e5),
        environment = 'express',
        minutes = '29',
        no_submit = True,
        src_dirname = '/draco/ptmp/clalescu/scaling',
        src_prefix = 'fbL',
        src_iteration = None,
        kMeta = 1.5,
        fftw_plan_rigor = 'FFTW_MEASURE',
        extra_parameters = []):
    simname = (DNS_type + '{0:0>4d}'.format(N))
    if output_on:
        simname = DNS_type + simname
    class_name = 'NSVE'
    if DNS_type != 'A':
        exponent = int(np.log10(nparticles))
        simname += 'p{0}e{1}'.format(
                int(nparticles / 10**exponent),
                exponent)
        class_name += 'particles'
    work_dir = 'nn{0:0>4d}np{1}'.format(nnodes, nprocesses)
    if not output_on:
        class_name += '_no_output'
    src_simname = src_prefix + '_N{0:0>4d}_kMeta{1:.1f}'.format(N, kMeta)
    if type(src_iteration) == type(None):
        assert (N in [1024, 2048, 4096])
        if N == 1024:
            src_iteration = 32*1024
        if N == 2048:
            src_iteration = 20*1024
        if N == 4096:
            src_simname = 'fb3_N2048x2_kMeta1.5'
            src_iteration = 0
    DNS_parameters = [
            class_name,
            '-n', '{0}'.format(N),
            '--np', '{0}'.format(nnodes*nprocesses),
            '--ntpp', '{0}'.format(cores_per_node // nprocesses),
            '--simname', simname,
            '--wd', work_dir,
            '--niter_todo', '12',
            '--niter_out', '12',
            '--niter_stat', '3']
    if N == 4096:
        DNS_parameters += [
                '--precision', 'double']
    # check that source sim exists
    print('looking for ', os.path.join(src_dirname, src_simname + '.h5'))
    assert(os.path.exists(os.path.join(src_dirname, src_simname + '.h5')))
    # check that source checkpoint exists
    dns_src = TurTLE.DNS(simname = src_simname, work_dir = src_dirname)
    dns_src.read_parameters()
    print('looking for ', dns_src.get_checkpoint_fname(iteration = src_iteration))
    assert(os.path.exists(dns_src.get_checkpoint_fname(iteration = src_iteration)))
    DNS_parameters += [
            '--src-wd', src_dirname,
            '--src-simname', src_simname,
            '--src-iteration', '{0}'.format(src_iteration)]
    # hardcode precision:
    if N <= 1024:
        DNS_parameters += ['--precision', 'single']
    else:
        DNS_parameters += ['--precision', 'double']
    # copy parameters from source simulation
    for parameter in ['fk0', 'fk1', 'forcing_type', 'dt', 'dealias_type', 'dkx', 'dky', 'dkz', 'energy', 'famplitude', 'fmode', 'injection_rate', 'nu']:
        DNS_parameters += ['--' + parameter, '{0}'.format(dns_src.parameters[parameter])]
    if DNS_type != 'A':
        DNS_parameters += [
                '--nparticles', '{0}'.format(nparticles)]
        nneighbours = np.where(np.array(
            ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K']) == DNS_type)[0][0]
        if nneighbours < 3:
            smoothness = 1
        else:
            smoothness = 2
        DNS_parameters += [
                '--tracers0_neighbours', '{0}'.format(nneighbours),
                '--tracers0_smoothness', '{0}'.format(smoothness),
                '--niter_part', '6',
                '--cpp_random_particles', '2']
    if no_submit:
        DNS_parameters += ['--no-submit']
    DNS_parameters += ['--environment', environment,
                       '--minutes', '{0}'.format(minutes),
                       '--fftw_plan_rigor', fftw_plan_rigor]
    return simname, work_dir, DNS_parameters + extra_parameters

def main():
        #DNS_type = 'A',
        #N = 512,
        #nnodes = 1,
        #nprocesses = 1,
        #output_on = False,
        #cores_per_node = 16,
        #nparticles = 1e5)
    parser = argparse.ArgumentParser(prog = 'launcher')
    parser.add_argument(
            'DNS_setup',
            choices = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'],
            type = str)
    parser.add_argument(
            '-n',
            type = int,
            dest = 'n',
            default = 32)
    parser.add_argument(
            '--nnodes',
            type = int,
            help = 'how many nodes to use in total',
            dest = 'nnodes',
            default = 1)
    parser.add_argument(
            '--nprocesses',
            type = int,
            help = 'how many MPI processes per node to use',
            dest = 'nprocesses',
            default = 1)
    parser.add_argument(
            '--ncores',
            type = int,
            help = 'how many cores there are per node',
            dest = 'ncores',
            default = 40)
    parser.add_argument(
            '--output-on',
            action = 'store_true',
            dest = 'output_on')
    parser.add_argument(
            '--submit',
            action = 'store_true',
            dest = 'submit')
    parser.add_argument(
            '--nparticles',
            type = int,
            dest = 'nparticles',
            default = int(1e5))
    parser.add_argument(
            '--environment',
            type = str,
            dest = 'environment',
            default = 'test')
    parser.add_argument(
            '--minutes',
            type = int,
            dest = 'minutes',
            default = 29,
            help = 'If environment supports it, this is the requested wall-clock-limit.')
    parser.add_argument(
            '--src-wd',
            type = str,
            dest = 'src_dirname',
            default = '/draco/ptmp/clalescu/scaling')
    parser.add_argument(
            '--src-prefix',
            type = str,
            dest = 'src_prefix',
            default = 'fbL')
    parser.add_argument(
            '--src-kMeta',
            type = float,
            dest = 'src_kMeta',
            default = 1.5)
    parser.add_argument(
            '--src-iteration',
            type = int,
            dest = 'src_iteration',
            default = None)
    parser.add_argument(
            '--profile-with-vtune',
            action = 'store_true',
            dest = 'use_vtune')
    parser.add_argument(
            '--profile-with-aps',
            action = 'store_true',
            dest = 'use_aps')
    opt = parser.parse_args(sys.argv[1:])
    extra_parameters = []
    if opt.use_vtune:
        extra_parameters.append('--profile-with-vtune')
    if opt.use_aps:
        extra_parameters.append('--profile-with-aps')
    simname, work_dir, params = get_DNS_parameters(
            DNS_type = opt.DNS_setup,
            N = opt.n,
            nnodes = opt.nnodes,
            nprocesses = opt.nprocesses,
            output_on = opt.output_on,
            nparticles = opt.nparticles,
            cores_per_node = opt.ncores,
            no_submit = not opt.submit,
            minutes = opt.minutes,
            environment = opt.environment,
            src_dirname = opt.src_dirname,
            src_prefix = opt.src_prefix,
            src_iteration = opt.src_iteration,
            kMeta = opt.src_kMeta,
            extra_parameters = extra_parameters)
    print(work_dir + '/' + simname)
    print(' '.join(params))
    # these following 2 lines actually launch something
    # I'm not passing anything from sys.argv since we don't want to get
    # parameter conflicts after the simname and work_dir have been decided
    if not os.path.exists(work_dir):
        os.makedirs(work_dir)
    c = TurTLE.DNS()
    c.launch(params)
    return None

if __name__ == '__main__':
    main()

