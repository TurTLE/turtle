################################################################################
#                                                                              #
#  Copyright 2019 Max Planck Institute for Dynamics and Self-Organization      #
#                                                                              #
#  This file is part of TurTLE.                                                #
#                                                                              #
#  TurTLE is free software: you can redistribute it and/or modify              #
#  it under the terms of the GNU General Public License as published           #
#  by the Free Software Foundation, either version 3 of the License,           #
#  or (at your option) any later version.                                      #
#                                                                              #
#  TurTLE is distributed in the hope that it will be useful,                   #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with TurTLE.  If not, see <http://www.gnu.org/licenses/>              #
#                                                                              #
# Contact: Cristian.Lalescu@ds.mpg.de                                          #
#                                                                              #
################################################################################


host_info = {'type' : 'pc'}


# template for host_info dictionary is given below:
#                 {'type'        : info_template_type,
#                  'MPI'         : info_template_MPI,
#                  'environment' : info_template_environment,
#                  'deltanprocs' : info_template_deltanprocs,
#                  'mail_address': info_template_mail_address,
#                  'account'     : info_template_account,
#                  'executable_launcher'           : info_template_executable_launcher,
#                  'extra_slurm_lines'             : info_template_extra_slurm_lines,
#                  'explicit_slurm_environment'    : info_template_explicit_slurm_environment,
#                  'use_TurTLE_core_distribution'  : info_use_TurTLE_core_distribution}

# info_template_type can be one of:
# 'pc'            --- jobs run interactively
# 'cluster'       --- cluster with SGE queueing system
# 'SLURM'         --- cluster with SLURM queueing system
# 'IBMLoadLeveler --- cluster with IBM Load Leveler queueing system

# info_template_MPI can be one of:
# 'openmpi'   --- it means mpirun takes "x" as the parameter to set an environment variable
# not defined --- use "env" instead of "x"

# info_template_environment, relevant for clusters,
# is the default queue to which jobs are submitted

# info_template_deltanprocs, relevant for clusters,
# is the number of cores per node

# info_template_mail_address, relevant for clusters,
# is the contact e-mail address placed in the job scripts.

# info_template_account, relevant for some clusters,
# is the name of the account to be budgeted for the job.

# info_template_executable_launcher, relevant for 'SLURM' clusters,
# is the binary that should be called to execute the hybrid MPI/OpenMP code.
# by default it is 'srun', but it may be 'mpiexec' for some machines.

# info_template_extra_slurm_lines, relevant for 'SLURM' clusters,
# is a list of strings, written as extra options within the SLURM file.
# for example something like `#SBATCH --get-user-env`
# Here, also the virtual environment and environment variables need to be loaded,
# e.g. by `source <TURTLE_DIR>/lib/bash_setup_for_TurTLE.sh`

# info_template_explicit_slurm_environment, relevant for `SLURM` clusters,
# is a bool specifying whether the environment must be mentioned explicitly
# in the submission script, or whether the cluster chooses it based on the
# resources requested

# info_use_TurTLE_core_distribution, relevant for `SLURM` clusters,
# is a bool specifying whether TurTLE should request explicit core
# masks for the individual MPI processes.

